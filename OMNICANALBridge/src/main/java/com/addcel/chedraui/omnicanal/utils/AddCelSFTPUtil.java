package com.addcel.chedraui.omnicanal.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.SocketTimeoutException;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.addcel.chedraui.omnicanal.model.vo.AddCelSFTPVO;
import com.addcel.chedraui.omnicanal.services.OmnicanalService;

@Component
public class AddCelSFTPUtil {
	private static final Logger logger = LoggerFactory
			.getLogger(OmnicanalService.class);

	public String makeFile(String fileName, String content, AddCelSFTPVO SFTPVo) {
		String resp = null;
		PrintWriter pw = null;
		// logger.info("Creando el archivo");
		try {
			pw = new PrintWriter(Constantes.PATH + fileName, "UTF-8");
			pw.append(content);
			// logger.info("El archivo se creo con éxito");
		} catch (Exception ex) {
			logger.error("Error al crear el archivo: ", ex);
			resp = "An error was obtained when creating  file: " + fileName + ", Error: " + ex.getMessage();
		} finally {
			if (pw != null) {
				pw.close();
			}
		}
		return resp;
	}

	public String sendFileviaSFTP(String fileName, AddCelSFTPVO SFTPVo) {
		FTPClient client = null;
		File file = null;
		FileInputStream input = null;
		String resp = null;
		logger.info("Archivo preparado para enviar en " + Constantes.PATH + fileName);

		try {
			logger.info("URL: " + SFTPVo.getUrlFTP());
//			logger.info("USER: " + SFTPVo.getUserFTP());
			client = new FTPClient();
			client.setConnectTimeout(30000);
			client.connect(SFTPVo.getUrlFTP());
			logger.info("Seccion conectada con exito");
			boolean login = client.login(SFTPVo.getUserFTP(), SFTPVo.getPassFTP());
			logger.info("Login status: " + login);

			if (login) {
				int replay = client.getReplyCode();
				if (FTPReply.isPositiveCompletion(replay)) {
					file = new File(Constantes.PATH + fileName);
					input = new FileInputStream(file);
					
					client.changeWorkingDirectory(SFTPVo.getPath());
					client.setFileType(FTP.BINARY_FILE_TYPE);
					client.enterLocalPassiveMode();
					
					logger.info("Path Destino: " + SFTPVo.getPath() + file.getName());

					if (!client.storeFile(SFTPVo.getPath() + file.getName(), input)) {
						resp = "Error al escribir archivo en el FTP.";
						logger.info("Subida fallida!");
					}else{
						logger.info("Subió satisfactoriamente el archivo");
					}
				}
			} else {
				resp = "Eror al autenticar en el FTP.";
			}

		} catch (Exception ex) {
			logger.error("Error al conectar FTP: ", ex);
			resp = "Error al conectar FTP: " + SFTPVo.getUrlFTP();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (client != null) {
				try {
					client.logout();
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					client.disconnect();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			logger.info("Desconectado");
		}
		return resp;
	}
	
	
	public String sendFileviaFTPTest(String fileName, AddCelSFTPVO SFTPVo) {
		FTPClient client = null;
		File file = null;
		FileInputStream input = null;
		String resp = null;
//		logger.info("Archivo preparado para enviar en " + Constantes.PATH + fileName);

		try {
			logger.info("URL: " + SFTPVo.getUrlFTP());
//			logger.info("USER: " + SFTPVo.getUserFTP());
			client = new FTPClient();
			client.setConnectTimeout(30000);
			client.connect(SFTPVo.getUrlFTP());
			logger.info("Seccion conectada con exito");
			boolean login = client.login(SFTPVo.getUserFTP(), SFTPVo.getPassFTP());
			logger.info("Login status: " + login);

			if (login) {
				int replay = client.getReplyCode();
				if (FTPReply.isPositiveCompletion(replay)) {
					file = new File(Constantes.PATH + fileName);
					input = new FileInputStream(file);
					
					client.changeWorkingDirectory(SFTPVo.getPath());
					client.setFileType(FTP.BINARY_FILE_TYPE);
					client.enterLocalPassiveMode();
					
					logger.info("Path Destino: " + SFTPVo.getPath() + file.getName());

					if (!client.storeFile(SFTPVo.getPath() + file.getName(), input)) {
						resp = "Error al escribir archivo en el FTP.";
						logger.info("Subida fallida!");
					}else{
						logger.info("Subió satisfactoriamente el archivo");
					}
				}
			} else {
				resp = "Eror al autenticar en el FTP.";
			}
			
		} catch (SocketTimeoutException se) {
			logger.error("Error al conectar FTP: ", se.getMessage());
			resp = "Error al conectar FTP: " + SFTPVo.getUrlFTP();
		} catch (Exception ex) {
			logger.error("Error al conectar FTP: ", ex);
			resp = "Error al conectar FTP: " + SFTPVo.getUrlFTP();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (client != null) {
				try {
					client.logout();
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					client.disconnect();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			logger.info("Desconectado");
		}
		return resp;
	}

	// public String sendFileviaSFTP(String fileName, AddCelSFTPVO SFTPVo) {
	// Session session = null;
	// Channel channel = null;
	// ChannelSftp channelSftp = null;
	// JSch jsch = null;
	// String resp = null;
	// logger.info("Archivo preparado para enviar en " + Constantes.PATH +
	// fileName);
	//
	// try {
	// logger.info("URL: " + SFTPVo.getUrlFTP());
	// logger.info("USER: " + SFTPVo.getUserFTP());
	// jsch = new JSch();
	// session = jsch.getSession(SFTPVo.getUserFTP(), SFTPVo.getUrlFTP(),
	// SFTPVo.getPort());
	// session.setPassword(SFTPVo.getPassFTP());
	// java.util.Properties config = new java.util.Properties();
	// config.put("StrictHostKeyChecking", "no");
	// session.setConfig(config);
	// session.connect();
	//
	// logger.info("Seccion conectada con exito");
	// channel = session.openChannel("ftp");
	// channel.connect();
	//
	// logger.info("Conectado al canal");
	// channelSftp = (ChannelSftp) channel;
	// //channelSftp.cd(SFTPVo.getPath());
	// // logger.info("Recuperando archivo de " + Constantes.PATH + src_file);
	// File srcFile = new File(Constantes.PATH + fileName).getCanonicalFile();
	//
	// logger.info("Empezando transferencia para : " + Constantes.PATH +
	// fileName);
	// channelSftp.put(new FileInputStream(srcFile), SFTPVo.getPath() +
	// srcFile.getName());
	//
	// logger.info("Archivo transferido");
	//
	// } catch (Exception ex) {
	// logger.error("Error al enviar por SFTP: ", ex);
	// resp = "An error was obtained when sending file: " + SFTPVo.getUrlFTP() +
	// ", Error: " + ex.getMessage();
	// } finally {
	// if(session != null){
	// session.disconnect();
	// }
	// logger.info("Desconectado");
	// }
	// return resp;
	// }

}