package com.addcel.chedraui.omnicanal.model.vo;

public class AddCelSFTPVO extends AbstractVO {
	
	private String userFTP;
	private String passFTP;
	private String urlFTP;
	private int port;
	private String path;
	private String urlWS;
	private String codigo;
		
	public String toString(){
		return "userFTP: " + userFTP + ", passFTP: ******, ip_Addr: "  + urlFTP + ", port: " + port + ", path: " + path +", urlWS: " + urlWS;
	}

	public String getUserFTP() {
		return userFTP;
	}

	public void setUserFTP(String userFTP) {
		this.userFTP = userFTP;
	}

	public String getPassFTP() {
		return passFTP;
	}

	public void setPassFTP(String passFTP) {
		this.passFTP = passFTP;
	}

	public String getUrlFTP() {
		return urlFTP;
	}

	public void setUrlFTP(String urlFTP) {
		this.urlFTP = urlFTP;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getUrlWS() {
		return urlWS;
	}

	public void setUrlWS(String urlWS) {
		this.urlWS = urlWS;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

}
