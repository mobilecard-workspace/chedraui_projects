package com.addcel.chedraui.omnicanal.services;

import java.rmi.RemoteException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.axis.AxisFault;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.chedraui.omnicanal.model.mapper.OmnicanalMapper;
import com.addcel.chedraui.omnicanal.model.vo.AbstractVO;
import com.addcel.chedraui.omnicanal.model.vo.AddCelSFTPVO;
import com.addcel.chedraui.omnicanal.model.vo.DatosTransaccion;
import com.addcel.chedraui.omnicanal.model.vo.DatosTransaccionResponse;
import com.addcel.chedraui.omnicanal.utils.AddCelSFTPUtil;
import com.addcel.chedraui.omnicanal.utils.Constantes;
import com.addcel.chedraui.omnicanal.utils.UtilsService;
import com.addcel.ws.clientes.enigma.magento.ConsultaPedidosProxy;
import com.addcel.ws.clientes.enigma.magento.RespuestaConsultarPedido;
import com.addcel.ws.clientes.enigma.magento.RespuestaConsultarPedidoItem;

@Service
public class OmnicanalServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(OmnicanalServiceTest.class);
	
	@Autowired
	private OmnicanalMapper mapper;
	
	@Autowired
	private AddCelSFTPUtil sftpUtil;
	
	@Autowired
	private UtilsService utilService;	
	
	public String procesaTransaccion(String json) {
		DatosTransaccion data = null;
		AbstractVO resp = null;
		String jsonRes = null;
		AddCelSFTPVO sftp = null;
		try{
			logger.debug("Peticion: " + json);
			data = (DatosTransaccion) utilService.jsonToObject(json, DatosTransaccion.class);				
			
			mapper.agregarBitacora(data);
			sftp = mapper.getDataTienda(String.valueOf(Integer.parseInt(data.getStore_sap())));
			
			if(sftp == null ){
				resp = new AbstractVO(-1, "There is no configuration for the branch: " + data.getStore_sap());
			}else{
//				data.setUrlWS(sftp.getUrlWS());
				switch (data.getIndicador()){
				case 1:
					jsonRes = validarPedidoArchivo(data, sftp);
					break;
					
				case 2:
					jsonRes = validarPedidoInfo(data);
					break;
					
				case 3:
				case 4:
				case 5:
					jsonRes = cambiaStatus(data);
					break;
					
				case 6:
					jsonRes = cancelaPedido(data);
					break;
					
				case 7:
					jsonRes = cancelaPedidoParcial(data);
					break;
					
				default:
					resp = new AbstractVO(-2, "Indicator does not exist.");
				};
			}
		}catch(Exception e){
			logger.error("Error General: ", e);
			resp = new AbstractVO(-1, "Ocurrio un error: " + e.getMessage());
		}finally{
			if(jsonRes == null){
				jsonRes = utilService.objectToJson(resp);
			}
			
			try{
				if(data != null){
					resp = (AbstractVO) utilService.jsonToObject(jsonRes, AbstractVO.class);
					mapper.actualizarBitacora(data.getId(), resp.getIdError(), resp.getMsgError().length() <= 150? resp.getMsgError(): resp.getMsgError().substring(0, 149));
				}
			}catch(Exception ex){
				logger.error("Error Update bitacora: ", ex);
			}
		}
		logger.debug("Peticion Respuesta: " + jsonRes);
		
		return jsonRes;		
	}
	
	public String validarPedidoArchivo (DatosTransaccion data, AddCelSFTPVO sftp) {
//		SalesOrderEntity respServ = null;
		RespuestaConsultarPedido respServ = null;
		AbstractVO resp = null;
		String jsonRes = null;
		String orderIncrementId = null;
//		String orderVersionId = null;
		StringBuilder cadFile = new StringBuilder();
		String respMet = null;
		String fileName = null;
		
		try{
			if(data.getOrderIncrementId().indexOf("-") > 0){
//				orderVersionId = data.getOrderIncrementId().substring(data.getOrderIncrementId().indexOf("-") + 1);
				orderIncrementId = data.getOrderIncrementId().substring(0, data.getOrderIncrementId().indexOf("-") - 1);
			}else{
				orderIncrementId = data.getOrderIncrementId(); 
			}
			
//			respServ = new Mage_Api_Model_Server_V2_HandlerPortTypeProxy(data.getUrlWS()).salesOrderInfo(obtenerSessionID(data.getUrlWS()), new BigInteger(orderIncrementId), orderVersionId != null? new BigInteger(orderVersionId): null);
//			
//			if(respServ != null){
//				cadFile.append("0;").append(respServ.getIncrement_id()).append(";").append(respServ.getStore_sap()).append(";").append(respServ.getGrand_total()).append(";\n");
//				
//				if(respServ.getItems() != null){
//					for(SalesOrderItemEntity item: respServ.getItems()){
//						cadFile.append("1;").append(item.getSku()).append(";").append(item.getIs_qty_decimal()).append(";")
//							.append(item.getPrice()).append(";")
//							.append(item.getQty_ordered().substring(0, item.getQty_ordered().indexOf(".")) + 
//									item.getQty_ordered().substring(item.getQty_ordered().indexOf(".") +1, item.getQty_ordered().length() -1))
//							.append(";\n");
//					}
//				}
//				cadFile.append("5;").append("ccsave".equalsIgnoreCase(respServ.getPayment_site())?Constantes.STATUS_NUM_CONTRA_ENTREGA:"vtol".equalsIgnoreCase(respServ.getPayment_site())? Constantes.STATUS_NUM_PAGO_EN_LÍNEA: "00")
//				.append(";").append(respServ.getGrand_total()).append(";");
//			
//				if(respServ.getIncrement_id().length() > 8){
//					fileName = respServ.getIncrement_id().substring(0, respServ.getIncrement_id().length() - 3) + "." +
//								respServ.getIncrement_id().substring(respServ.getIncrement_id().length() - 3);
//				}else{
//					fileName = respServ.getIncrement_id();
//				}
//				
////				cadFile.append("5;").append("2230910391931908391830234\n98209238492809482934802\n2342424324324243").append(";").append("\n0998098098938402984284284").append(";");
////				if(data.getOrderIncrementId().length() > 8){
////					fileName = data.getOrderIncrementId().substring(0, data.getOrderIncrementId().length() - 3) + "." +
////							data.getOrderIncrementId().substring(data.getOrderIncrementId().length() - 3);
////				}else{
////					fileName = data.getOrderIncrementId();
////				}
//
//				respMet = sftpUtil.makeFile(fileName, cadFile.toString(), sftp);
//				if(respMet == null){
//					respMet = sftpUtil.sendFileviaSFTP(fileName, sftp);
//					
//					if(respMet != null){
//						resp = new AbstractVO(-7, respMet);
//					}else{
//						resp = new AbstractVO(0, "Transaction Success.");
//					}
//				}else{
//					resp = new DatosTransaccionResponse(-6, respMet);
//				}
					
			respServ = new ConsultaPedidosProxy().consultar(data.getOrderIncrementId(), obtenerHASH1(data.getOrderIncrementId() + Constantes.KEY_MAGENTO));
			if(respServ != null){
				if("66".equals(respServ.getRespuesta())){
					resp = new AbstractVO(66, "Problema en el Acceso.");
				}else if("01".equals(respServ.getRespuesta())){
					resp = new AbstractVO(1, "No existe el pedido buscado.");
				}else if("02".equals(respServ.getRespuesta())){
					resp = new AbstractVO(2, "El pedido esta en Estatus invalido.");
				}else if("00".equals(respServ.getRespuesta())){
					cadFile.append("0;").append(respServ.getNumeroPedido()).append(";").append(respServ.getTienda()).append(";")
					.append(respServ.getTotal().doubleValue() - respServ.getTotalEnvio().doubleValue()).append(";\n");
					
					if(respServ.getItemsp() != null){
						for(RespuestaConsultarPedidoItem item: respServ.getItemsp()){
							cadFile.append("1;").append(item.getSku()).append(";").append(item.getEsPesable()).append(";")
								.append(item.getPrecio()).append(";")
//								.append(item.getTotalArticulos())
								.append(item.getEsPesable() != null && "0".equals(item.getEsPesable()) ? 
										item.getTotalArticulos().doubleValue()/1000 : item.getTotalArticulos())
								.append(";\n");
						}
					}
					cadFile.append("5;")
					.append("01".equalsIgnoreCase(respServ.getMetodoPago()) ?Constantes.STATUS_NUM_CONTRA_ENTREGA:
						"02".equalsIgnoreCase(respServ.getMetodoPago()) ? Constantes.STATUS_NUM_CREDITO_AMEX:
						"03".equalsIgnoreCase(respServ.getMetodoPago()) ? Constantes.STATUS_NUM_CREDITO_BANCOMER: Constantes.STATUS_NUM_PEDIDO_PAGADO_OTRA_TIENDA)
					.append(";").append(respServ.getTotal().doubleValue() - respServ.getTotalEnvio().doubleValue())
					.append(respServ.getReferencia() != null? respServ.getReferencia(): "").append(";");
				
					if(orderIncrementId.length() > 8){
						fileName = orderIncrementId.substring(0, orderIncrementId.length() - 3) + "." +
								orderIncrementId.substring(orderIncrementId.length() - 3);
					}else{
						fileName = orderIncrementId;
					}
					
//					respMet = sftpUtil.makeFile(fileName, "0;1000000490;0236;130.3000;\\n1;1859501993;0;10.2000;1000;\\n1;1780015637;0;11.6000;1000;\\n1;1090000501;0;20.0000;1000;\\n1;2708412013;0;14.5000;1000;\\n1;2667668982;0;49.0000;1000;\\n1;2120011139;0;25.0000;1000;\\n5;45;130.3000;", sftp);
					respMet = sftpUtil.makeFile(fileName, cadFile.toString(), sftp);
					if(respMet == null){
						respMet = sftpUtil.sendFileviaSFTP(fileName, sftp);
						
						if(respMet != null){
							resp = new AbstractVO(-7, respMet);
						}else{
							resp = new AbstractVO(0, "Transaction Success.");
						}
					}else{
						resp = new AbstractVO(-6, respMet);
					}
				}
			}else{
				resp = new AbstractVO(1, "Requested order not exists.");
			}
		}catch(AxisFault a){
			if(StringUtils.isNumeric(a.getFaultCode().toString())){
				resp = new AbstractVO(Integer.parseInt(a.getFaultCode().toString()), a.getFaultString());
			}else{
				resp = new AbstractVO(-7, a.getFaultString());
			}
		}catch(Exception e){
			logger.error("Error validarPedidoInfo: ", e);
			resp = new AbstractVO(-4, "Error: " + e.getMessage());
		}finally{
			jsonRes = utilService.objectToJson(resp);
		}
		
		return jsonRes;
	}
	
	public String validarPedidoInfo (DatosTransaccion data) {
//		SalesOrderEntity respServ = null;
		RespuestaConsultarPedido respServ = null;
		DatosTransaccionResponse resp = null;
		String jsonRes = null;
//		String orderIncrementId = null;
//		String orderVersionId = null;
		try{
//			if(data.getOrderIncrementId().indexOf("-") > 0){
//				orderVersionId = data.getOrderIncrementId().substring(data.getOrderIncrementId().indexOf("-") + 1);
//				orderIncrementId = data.getOrderIncrementId().substring(0, data.getOrderIncrementId().indexOf("-") -1);
//			}else{
//				orderIncrementId = data.getOrderIncrementId(); 
//			}
			
//			respServ = new Mage_Api_Model_Server_V2_HandlerPortTypeProxy(data.getUrlWS()).salesOrderInfo(obtenerSessionID(data.getUrlWS()), new BigInteger(orderIncrementId), orderVersionId != null? new BigInteger(orderVersionId): null);
//			
//			if(respServ != null){
//				resp = new DatosTransaccionResponse();
//				resp.setMsgError("Transaction Success.");
//				resp.setOrderIncrementId(respServ.getIncrement_id());
//				resp.setStore_org_sap(respServ.getStore_sap());
//				resp.setGrand_total(respServ.getGrand_total());
//				resp.setPayment_type(
//						"ccsave".equalsIgnoreCase(respServ.getPayment_site())?Constantes.STATUS_CONTRA_ENTREGA:"vtol".equalsIgnoreCase(respServ.getPayment_site())? Constantes.STATUS_PAGO_EN_LÍNEA: "NA");
//				resp.setPayment_ref(
//						"ccsave".equalsIgnoreCase(respServ.getPayment_site())?Constantes.STATUS_NUM_CONTRA_ENTREGA:"vtol".equalsIgnoreCase(respServ.getPayment_site())? Constantes.STATUS_NUM_PAGO_EN_LÍNEA: "00");
//				resp.setStatus(respServ.getStatus());
//			}else{
//				resp = new DatosTransaccionResponse(1, "Requested order not exists.");
//			}
			
			respServ = new ConsultaPedidosProxy().consultar(data.getOrderIncrementId(), obtenerHASH1(data.getOrderIncrementId() + Constantes.KEY_MAGENTO));
			
			if(respServ != null){
				if("66".equals(respServ.getRespuesta())){
					resp = new DatosTransaccionResponse(66, "Problema en el Acceso.");
				}else if("01".equals(respServ.getRespuesta())){
					resp = new DatosTransaccionResponse(1, "No existe el pedido buscado.");
				}else if("02".equals(respServ.getRespuesta())){
					resp = new DatosTransaccionResponse(2, "El pedido esta en Estatus invalido.");
				}else if("00".equals(respServ.getRespuesta())){
					resp = new DatosTransaccionResponse();
					resp.setMsgError("Transaction Success.");
					resp.setOrderIncrementId(respServ.getNumeroPedido());
					resp.setStore_org_sap(respServ.getTienda());
					resp.setGrand_total(String.valueOf(respServ.getTotal().doubleValue() - respServ.getTotalEnvio().doubleValue()));
					resp.setPayment_type(
							"01".equalsIgnoreCase(respServ.getMetodoPago())?Constantes.STATUS_CONTRA_ENTREGA:
								("02".equalsIgnoreCase(respServ.getMetodoPago()) || "03".equalsIgnoreCase(respServ.getMetodoPago()))? Constantes.STATUS_PAGO_EN_LÍNEA: Constantes.STATUS_PAGADO_OTRA_TIENDA);
					resp.setPayment_ref(
							"01".equalsIgnoreCase(respServ.getMetodoPago()) ?Constantes.STATUS_NUM_CONTRA_ENTREGA:
							"02".equalsIgnoreCase(respServ.getMetodoPago()) ? Constantes.STATUS_NUM_CREDITO_AMEX:
							"03".equalsIgnoreCase(respServ.getMetodoPago()) ? Constantes.STATUS_NUM_CREDITO_BANCOMER: Constantes.STATUS_NUM_PEDIDO_PAGADO_OTRA_TIENDA);
//					resp.setPayment_ref(respServ.getReferencia());
					resp.setStatus(respServ.getStatus());
				}
			}else{
				resp = new DatosTransaccionResponse(1, "Requested order not exists.");
			}
			
//			resp = new DatosTransaccionResponse();
//			resp.setMsgError("Transaction Success.");
//			resp.setOrderIncrementId(data.getStore_sap());
//			resp.setStore_org_sap(data.getStore_sap());
//			resp.setGrand_total("112345.0");
//			resp.setPayment_type( Constantes.STATUS_PAGO_EN_LÍNEA);
//			resp.setPayment_ref(Constantes.STATUS_NUM_PAGO_EN_LÍNEA);
//			resp.setStatus("proceso_surtido");
			
		}catch(AxisFault a){
			logger.error("Error validarPedidoInfo: ", a);
			if(StringUtils.isNumeric(a.getFaultCode().toString())){
				resp = new DatosTransaccionResponse(Integer.parseInt(a.getFaultCode().toString()), a.getFaultString());
			}else{
				resp = new DatosTransaccionResponse(-7, a.getFaultString());
			}
		}catch(Exception e){
			logger.error("Error validarPedidoInfo: ", e);
			resp = new DatosTransaccionResponse(-4, "Error: " + e.getMessage());
		}finally{
			jsonRes = utilService.objectToJson(resp);
		}
		
		return jsonRes;
	}
	
	public String cambiaStatus (DatosTransaccion data) {
		String json = null;
		AbstractVO resp = null;
		String status = null;
//		boolean respServ = false;
		String respServ = null;
		
		RespuestaConsultarPedido respS = null;
		
		try{
			switch (data.getIndicador()){
			case 3:
				status = Constantes.STATUS_EN_PROCESO_ENTREGA;
				break;
				
			case 4:
				status = Constantes.STATUS_RENDIR_ENTREGA;
				break;
				
			case 5:
				status = Constantes.STATUS_EN_PAGADO;
				break;
			
			default:
				resp = new AbstractVO(-2, "Indicator does not exist.");
			};
			
//			respServ = new Mage_Api_Model_Server_V2_HandlerPortTypeProxy(data.getUrlWS()).salesOrderAddComment(obtenerSessionID(data.getUrlWS()), data.getOrderIncrementId(), status, "", "");
//			
//			if(respServ){
//				resp = new AbstractVO(0, "Transaction Success.");
//			}else{
//				resp = new AbstractVO(1, "An error was obtained on status change.");
//			}
			
			respServ = new ConsultaPedidosProxy().actualizaStatus(data.getOrderIncrementId(), obtenerHASH1(data.getOrderIncrementId() + Constantes.KEY_MAGENTO), status);
			
			if(respServ != null){
				if("66".equals(respServ)){
					resp = new DatosTransaccionResponse(66, "Problema en el Acceso.");
				}else if("01".equals(respServ)){
					resp = new DatosTransaccionResponse(1, "No existe el estatus a actualizar.");
				}else if("00".equals(respServ)){
					resp = new AbstractVO(0, "Transaction Success.");
					
					//Valida tipo de cambio status, si es pago, busca tienda origen
					if(data.getIndicador() == 5){
						respS = new ConsultaPedidosProxy().consultar(data.getOrderIncrementId(), obtenerHASH1(data.getOrderIncrementId() + Constantes.KEY_MAGENTO));
						
						if("66".equals(respS.getRespuesta())){
							resp = new DatosTransaccionResponse(66, "Error en cambio de tienda. Problema en el Acceso.");
						}else if("01".equals(respS.getRespuesta())){
							resp = new DatosTransaccionResponse(1, "Error en cambio de tienda. No existe el pedido buscado.");
						}else if("02".equals(respS.getRespuesta())){
							resp = new DatosTransaccionResponse(2, "Error en cambio de tienda. El pedido esta en Estatus invalido.");
						}else if("00".equals(respS.getRespuesta())){
							
							//Si la tienda origen es diferente a tienda pago, se ahce el cambio de tienda pago
							if(!data.getStore_sap().equalsIgnoreCase(respS.getTienda())){
								respServ = new ConsultaPedidosProxy().actualizaTiendaPago(data.getOrderIncrementId(), obtenerHASH1(data.getOrderIncrementId() + Constantes.KEY_MAGENTO), data.getStore_sap());
								
								if("66".equals(respServ)){
									resp = new DatosTransaccionResponse(66, "Error en cambio de tienda. Problema en el Acceso.");
								}else if("01".equals(respServ)){
									resp = new DatosTransaccionResponse(1, "Error en cambio de tienda. No existe el estatus a actualizar.");
								}else if("00".equals(respServ)){
									resp = new AbstractVO(0, "Transaction Success.");
								}
							}
						}
					}
				}
			}else{
				resp = new AbstractVO(1, "An error was obtained on status change.");
			}
			
		}catch(AxisFault a){
//			logger.error("Error validarPedidoInfo: ", a);
			if(StringUtils.isNumeric(a.getFaultCode().toString())){
				resp = new DatosTransaccionResponse(Integer.parseInt(a.getFaultCode().toString()), a.getFaultString());
			}else{
				resp = new DatosTransaccionResponse(-7, a.getFaultString());
			}
		}catch (Exception e){
			logger.error("Error cambiaStatus: ", e);
			resp = new AbstractVO(-3, "An error was obtained: " + e.getMessage());
		}finally{
			json = utilService.objectToJson(resp);
		}
		return json;
	}
	
	public String cancelaPedido (DatosTransaccion data) {
//		boolean respServ = false;
		AbstractVO resp = null;
		String jsonRes = null;
		
		try{
			resp = new DatosTransaccionResponse(-11, "Indicator is on standby.");
//			respServ = new Mage_Api_Model_Server_V2_HandlerPortTypeProxy(data.getUrlWS()).salesOrderCancel(obtenerSessionID(data.getUrlWS()), data.getOrderIncrementId());
//			
//			if(respServ){
//				resp = new AbstractVO(0, "Transaction Success.");
//			}else{
//				resp = new AbstractVO(1, "An error was obtained on status change.");
//			}
//		}catch(AxisFault a){
////			logger.error("Error validarPedidoInfo: ", a);
//			if(StringUtils.isNumeric(a.getFaultCode().toString())){
//				resp = new DatosTransaccionResponse(Integer.parseInt(a.getFaultCode().toString()), a.getFaultString());
//			}else{
//				resp = new DatosTransaccionResponse(-7, a.getFaultString());
//			}	
		}catch(Exception e){
			logger.error("Error validarPedidoInfo: ", e);
			resp = new AbstractVO(-4, "Error: " + e.getMessage());
		}finally{
			jsonRes = utilService.objectToJson(resp);
		}
		
		return jsonRes;
	}

	public String cancelaPedidoParcial (DatosTransaccion data) {
		String jsonRes = null;
		AbstractVO resp = null;
		try{
			resp = new DatosTransaccionResponse(-10, "Indicator is on standby.");
		
		}catch(Exception e){
			logger.error("Error validarPedidoInfo: ", e);
			resp = new DatosTransaccionResponse(-4, "Error: " + e.getMessage());
		}finally{
			jsonRes = utilService.objectToJson(resp);
		}
		return jsonRes;
	}

	public String obtenerSessionID (String url ) throws RemoteException {
//		return new Mage_Api_Model_Server_V2_HandlerPortTypeProxy(url).login(Constantes.USUARIO, Constantes.PASSWORD);
		return null;
	}
	
	public String obtenerHASH1(String cadena){
		byte[] buffer, digest;
		MessageDigest md = null;
		String hash = "";
		int b = 0;
		try {
			buffer = cadena.getBytes();
			md = MessageDigest.getInstance("SHA1");
			md.update(buffer);
			digest = md.digest();

			for(byte aux : digest) {
				b = aux & 0xff;
				if (Integer.toHexString(b).length() == 1) hash += "0";
					hash += Integer.toHexString(b);
			}
			
			logger.debug("SHA1:  " + hash);
		} catch (NoSuchAlgorithmException e) {
			logger.error("Error in HASH1: ", e);
		}
		return hash;
	}
}
