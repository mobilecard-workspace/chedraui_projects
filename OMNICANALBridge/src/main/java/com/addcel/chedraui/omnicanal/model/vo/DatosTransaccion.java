package com.addcel.chedraui.omnicanal.model.vo;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DatosTransaccion extends AbstractVO {

	private long id;
	private int indicador;
	private String orderIncrementId;
	private String store_sap;
	private String cancel_items_string;
	private List<String> cancel_items;
	private String status;
	
	
	private String urlWS;
	
	public int getIndicador() {
		return indicador;
	}
	public void setIndicador(int indicador) {
		this.indicador = indicador;
	}
	public String getOrderIncrementId() {
		return orderIncrementId;
	}
	public void setOrderIncrementId(String orderIncrementId) {
		this.orderIncrementId = orderIncrementId;
	}
	public String getStore_sap() {
		return store_sap;
	}
	public void setStore_sap(String store_sap) {
		this.store_sap = store_sap;
	}
	public List<String> getCancel_items() {
		return cancel_items;
	}
	public void setCancel_items(List<String> cancel_items) {
		this.cancel_items = cancel_items;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUrlWS() {
		return urlWS;
	}
	public void setUrlWS(String urlWS) {
		this.urlWS = urlWS;
	}
	public String getCancel_items_string() {
		
		if(this.cancel_items != null){
			return cancel_items.toString();
		}else{
			return "";
		}
		
	}
	public void setCancel_items_string(String cancel_items_string) {
		this.cancel_items_string = cancel_items_string;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
}
