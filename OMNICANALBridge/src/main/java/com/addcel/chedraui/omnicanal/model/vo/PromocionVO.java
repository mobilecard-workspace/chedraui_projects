package com.addcel.chedraui.omnicanal.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PromocionVO extends AbstractVO {

	private long promoID;
	private String tipoBeneficio;
	private String promoType;
	private String descripcionPromocion;
	private double porcentajeDescuento;
	private double montoDescuento;
	private double nuevoPrecio;
	private int valorM;
	private int valorN;
	private int cantidadGratis;
	private double porcentajeBonificacion;
	private double montoBonificacion;
	private int tipoCliente;
	private String disparador;
	private long rangoMontoPromocional;
	
	public long getPromoID() {
		return promoID;
	}
	public void setPromoID(long promoID) {
		this.promoID = promoID;
	}
	public String getTipoBeneficio() {
		return tipoBeneficio;
	}
	public void setTipoBeneficio(String tipoBeneficio) {
		this.tipoBeneficio = tipoBeneficio;
	}
	public String getPromoType() {
		return promoType;
	}
	public void setPromoType(String promoType) {
		this.promoType = promoType;
	}
	public String getDescripcionPromocion() {
		return descripcionPromocion;
	}
	public void setDescripcionPromocion(String descripcionPromocion) {
		this.descripcionPromocion = descripcionPromocion;
	}
	public double getPorcentajeDescuento() {
		return porcentajeDescuento;
	}
	public void setPorcentajeDescuento(double porcentajeDescuento) {
		this.porcentajeDescuento = porcentajeDescuento;
	}
	public double getMontoDescuento() {
		return montoDescuento;
	}
	public void setMontoDescuento(double montoDescuento) {
		this.montoDescuento = montoDescuento;
	}
	public double getNuevoPrecio() {
		return nuevoPrecio;
	}
	public void setNuevoPrecio(double nuevoPrecio) {
		this.nuevoPrecio = nuevoPrecio;
	}
	public int getValorM() {
		return valorM;
	}
	public void setValorM(int valorM) {
		this.valorM = valorM;
	}
	public int getValorN() {
		return valorN;
	}
	public void setValorN(int valorN) {
		this.valorN = valorN;
	}
	public int getCantidadGratis() {
		return cantidadGratis;
	}
	public void setCantidadGratis(int cantidadGratis) {
		this.cantidadGratis = cantidadGratis;
	}
	public double getPorcentajeBonificacion() {
		return porcentajeBonificacion;
	}
	public void setPorcentajeBonificacion(double porcentajeBonificacion) {
		this.porcentajeBonificacion = porcentajeBonificacion;
	}
	public double getMontoBonificacion() {
		return montoBonificacion;
	}
	public void setMontoBonificacion(double montoBonificacion) {
		this.montoBonificacion = montoBonificacion;
	}
	public int getTipoCliente() {
		return tipoCliente;
	}
	public void setTipoCliente(int tipoCliente) {
		this.tipoCliente = tipoCliente;
	}
	public String getDisparador() {
		return disparador;
	}
	public void setDisparador(String disparador) {
		this.disparador = disparador;
	}
	public long getRangoMontoPromocional() {
		return rangoMontoPromocional;
	}
	public void setRangoMontoPromocional(long rangoMontoPromocional) {
		this.rangoMontoPromocional = rangoMontoPromocional;
	}
	
	
}
