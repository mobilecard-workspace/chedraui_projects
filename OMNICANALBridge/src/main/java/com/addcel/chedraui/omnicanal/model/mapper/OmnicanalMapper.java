package com.addcel.chedraui.omnicanal.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.chedraui.omnicanal.model.vo.AddCelSFTPVO;
import com.addcel.chedraui.omnicanal.model.vo.DatosTransaccion;
import com.addcel.chedraui.omnicanal.model.vo.PromocionVO;

public interface OmnicanalMapper {
	
	public int agregarBitacora(DatosTransaccion data);
	
	public int actualizarBitacora(@Param(value="id") long id,
								@Param(value="status") int status,
								@Param(value="error") String error);
	
	public AddCelSFTPVO getDataTienda(@Param(value="sucursal") String sucursal);
	
	public List<AddCelSFTPVO> getTiendas();
	
	public PromocionVO getPromociones(String promoId);
	
}
