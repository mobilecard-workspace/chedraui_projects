package com.addcel.chedraui.omnicanal.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DatosTransaccionResponse extends AbstractVO {

	private String orderIncrementId;
	private String store_org_sap;
	private String grand_total;
	private String payment_type;
	private String payment_ref;
	private String status;
	
	public DatosTransaccionResponse(){
	}
	public DatosTransaccionResponse(int idError, String msgError) {
		super(idError, msgError);
	}
	
	
	public String getOrderIncrementId() {
		return orderIncrementId;
	}
	public void setOrderIncrementId(String orderIncrementId) {
		this.orderIncrementId = orderIncrementId;
	}
	public String getStore_org_sap() {
		return store_org_sap;
	}
	public void setStore_org_sap(String store_org_sap) {
		this.store_org_sap = store_org_sap;
	}
	public String getGrand_total() {
		return grand_total;
	}
	public void setGrand_total(String grand_total) {
		this.grand_total = grand_total;
	}
	public String getPayment_type() {
		return payment_type;
	}
	public void setPayment_type(String payment_type) {
		this.payment_type = payment_type;
	}
	public String getPayment_ref() {
		return payment_ref;
	}
	public void setPayment_ref(String payment_ref) {
		this.payment_ref = payment_ref;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
