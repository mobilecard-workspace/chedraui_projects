package com.addcel.chedraui.omnicanal.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.chedraui.omnicanal.model.mapper.OmnicanalMapper;
import com.addcel.chedraui.omnicanal.model.vo.AddCelSFTPVO;
import com.addcel.chedraui.omnicanal.model.vo.DatosTransaccion;
import com.addcel.chedraui.omnicanal.utils.AddCelSFTPUtil;
import com.addcel.chedraui.omnicanal.utils.UtilsService;

@Service
public class ValidacionesService {
	private static final Logger logger = LoggerFactory.getLogger(ValidacionesService.class);
	
	@Autowired
	private OmnicanalMapper mapper;
	
	@Autowired
	private AddCelSFTPUtil sftpUtil;
	
	@Autowired
	private UtilsService utilService;
	
	
	public String procesaTransaccion(String json) {
		DatosTransaccion data = null;
		String jsonRes = null;
		List<AddCelSFTPVO> lstTienda = null;
		AddCelSFTPVO sftp = null;
		StringBuilder cadFile = null;
		String respMet =  null;
		StringBuilder errores = new StringBuilder();
		try{
			lstTienda = mapper.getTiendas();
			
			if(lstTienda != null && lstTienda.size() > 0){
				
				cadFile = new StringBuilder("Archivo de prueba para FTP.");
				respMet = sftpUtil.makeFile("file.temp", cadFile.toString(), sftp);
				if(respMet == null){
				
					for(int i = 0; i < lstTienda.size(); i++){
						sftp = lstTienda.get(i);
						
						respMet = sftpUtil.sendFileviaFTPTest("file.temp", sftp);
						
						errores.append("[  ").append( i ).append( " ] : Tienda: " ).append(sftp.getCodigo())
						.append( ",  IP: " ).append(sftp.getUrlFTP());
						
						if(respMet != null){
							logger.error("[" + i + " ] : "  + respMet);
							errores.append( respMet).append("<br>");
						}else{
							logger.error("[ " + i +  "] : Transacción Exitosa." );
							errores.append( "Exito").append("<br>");
//							resp = new AbstractVO(0, "Transacción Exitosa.");
						}
					}
				
				}else{
					logger.error("[ 0 ] : Error al generar Archivo.");
					errores.append("[ 0 ] : " ).append( "Error al generar Archivo.").append("<br>");
				}
			}
		}catch(Exception e){
			logger.error("Error validarPedidoInfo: ", e);
		}finally{
			jsonRes = "";
		}
		
		return errores.toString();
	}
	
}
