package com.addcel.chedraui.omnicanal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.addcel.chedraui.omnicanal.services.OmnicanalService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class OmnicanalController {
	
//	private static final Logger logger = LoggerFactory.getLogger(OmnicanalController.class);
	
	@Autowired
	private OmnicanalService service;
	
 	@RequestMapping(value = "/services/transactionProcess"/*,method=RequestMethod.POST*/)
	public @ResponseBody String transactionProcess(@RequestParam("param") String json) {
//		logger.debug("Dentro del servicio: /altaUsuario");
		return service.procesaTransaccion(json);
	}
	
}