package com.addcel.chedraui.omnicanal.rompefilas.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.addcel.chedraui.omnicanal.model.vo.AbstractVO;
import com.addcel.ws.clientes.enigma.magento.RespuestaConsultarPedido;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OmniPedidoResponse extends AbstractVO {

	private RespuestaConsultarPedido pedido;

	public OmniPedidoResponse() {
	}

	public OmniPedidoResponse(int idError, String msgError) {
		super(idError, msgError);
	}
	
	public RespuestaConsultarPedido getPedido() {
		return pedido;
	}
	
	public void setPedido(RespuestaConsultarPedido pedido) {
		this.pedido = pedido;
	}

}
