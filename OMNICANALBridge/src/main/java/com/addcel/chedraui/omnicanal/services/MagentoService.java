package com.addcel.chedraui.omnicanal.services;

import java.rmi.RemoteException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.HashMap;

import org.apache.axis.AxisFault;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.addcel.chedraui.omnicanal.exception.AddcelException;
import com.addcel.chedraui.omnicanal.utils.Constantes;
import com.addcel.ws.clientes.enigma.magento.ConsultaPedidosProxy;
import com.addcel.ws.clientes.enigma.magento.RespuestaConsultarPedido;

@Service
public class MagentoService {
	private static final Logger logger = LoggerFactory.getLogger(MagentoService.class);
	
	public RespuestaConsultarPedido consultaPedidoReintentos(String orderIncrementId, String url) throws AddcelException{
		RespuestaConsultarPedido respServ = null;
		
		respServ = consultaPedido(orderIncrementId, url);
		if(respServ == null){

			logger.debug("Intento consulta 2, pedido: " + orderIncrementId);
			respServ = consultaPedido(orderIncrementId, url);
			if(respServ == null){
				throw new AddcelException(11, "Respuesta vacia Magento.");
			}
		}
		return respServ;
	}
	
	public RespuestaConsultarPedido consultaPedido(String orderIncrementId, String url) throws AddcelException{
		RespuestaConsultarPedido respServ = null;
		try{
			respServ = new ConsultaPedidosProxy(url).consultar(orderIncrementId, obtenerHASH1(orderIncrementId + Constantes.KEY_MAGENTO));
			if(respServ != null){
				if("66".equals(respServ.getRespuesta())){
					throw new AddcelException(66, "Problema en el Acceso a Magento.");
				}else if("01".equals(respServ.getRespuesta())){
					throw new AddcelException(1, "No existe el pedido buscado");
				}else if("02".equals(respServ.getRespuesta())){
					throw new AddcelException(2, "El pedido esta en Estatus invalido");
				}else if("00".equals(respServ.getRespuesta()) || "-1".equals(respServ)){
					
				}else{
					throw new AddcelException(16, "Respuesta desconocida Magento: " + respServ.getRespuesta());
				}
//			}else{
//				throw new AddcelException(11, "Respuesta vacia Magento.");
			}
		}catch(AxisFault a){
			if(StringUtils.isNumeric(a.getFaultCode().toString())){
				throw new AddcelException(Integer.parseInt(a.getFaultCode().toString()), "Error desconocido en Magento.");
			}else if(a.getFaultString().contains("java.net.SocketTimeoutException") ){
				throw new AddcelException(17, "Error al leer la respuesta de Magento.");
			}else if(a.getFaultString().contains("java.net.ConnectException") ){
				throw new AddcelException(18, "Error en la conexion con Magento.");
			}else{
				throw new AddcelException(19, "Error desconocido en Magento.");
			}
		}catch(RemoteException e){
			logger.error("Error consultaPedido Magento: ", e);
			throw new AddcelException(-4, "Error: " + e.getMessage());
		}
		
		return respServ;
	}
	
	public String actualizaStatusReintentos (String orderIncrementId, String status, String url) throws AddcelException {
		String respServ = null;
		
		respServ = actualizaStatus(orderIncrementId, status, url);
		if(respServ != null && "-1".equals(respServ)){
			
			logger.debug("Intento cambioStatus 2, pedido: " + orderIncrementId);
			respServ = actualizaStatus(orderIncrementId, status, url);
			if(respServ != null && "-1".equals(respServ)){
				throw new AddcelException(15, "Error interno Magento.");
			}
		}
		return respServ;
	}
	
	public String actualizaStatus (String orderIncrementId, String status, String url) throws AddcelException {
		String respServ = null;
		try{
			respServ = new ConsultaPedidosProxy(url).actualizaStatus(orderIncrementId, obtenerHASH1(orderIncrementId + status + Constantes.KEY_MAGENTO ), status);
			
			logger.debug("Respuesta cambio status: " + respServ);
			if(respServ != null){
				if("66".equals(respServ)){
					throw new AddcelException(66, "Problema en el Acceso a Magento.");
				}else if("01".equals(respServ)){
					throw new AddcelException(3, "No existe el estatus a actualizar: " + status);
//				}else if("02".equals(respServ)){
//					throw new AddcelException(2, "No existe el estatus a actualizar: " + status);
				}else if("00".equals(respServ) || "-1".equals(respServ)){
					
				}else{
					throw new AddcelException(16, "Respuesta desconocida Magento.");
				}
			}else{
				throw new AddcelException(11, "Respuesta vacia Magento.");
			}
		}catch(AxisFault a){
			if(StringUtils.isNumeric(a.getFaultCode().toString())){
				throw new AddcelException(Integer.parseInt(a.getFaultCode().toString()), a.getFaultString());
			}else if(a.getFaultString().contains("java.net.SocketTimeoutException") ){
				throw new AddcelException(-17, "Error al leer la respuesta de Magento.");
			}else if(a.getFaultString().contains("java.net.ConnectException") ){
				throw new AddcelException(-18, "Error en la conexion con Magento.");
			}else{
				throw new AddcelException(-17, "Error desconocido en Magento.");
			}
		}catch (RemoteException e){
			logger.error("Error cambiaStatus Magento: ", e);
			throw new AddcelException(-4, "Error: " + e.getMessage());
		}
		return respServ;
	}
	
	public String actualizaTiendaPagoReintentos (String orderIncrementId, String tiendaPago, String url) throws AddcelException {
		String respServ = null;
		
		respServ = actualizaTiendaPago(orderIncrementId, tiendaPago, url);
		if(respServ != null && "-1".equals(respServ)){
			
			logger.debug("Intento actualizaTiendaPago 2, pedido: " + orderIncrementId);
			respServ = actualizaTiendaPago(orderIncrementId, tiendaPago, url);
			if(respServ != null && "-1".equals(respServ)){
				throw new AddcelException(15, "Error interno Magento.");
			}
		}
		return respServ;
	}
	
	public String actualizaTiendaPago (String orderIncrementId, String tiendaPago, String url) throws AddcelException {
		String respServ = null;
		try{
			respServ = new ConsultaPedidosProxy(url).actualizaTiendaPago(orderIncrementId, obtenerHASH1(orderIncrementId + tiendaPago + Constantes.KEY_MAGENTO), tiendaPago);
			
			logger.debug("Respuesta actualizaTiendaPago status: " + respServ);
			if(respServ != null){
				if("66".equals(respServ)){
					throw new AddcelException(66, "Problema en el Acceso a Magento.");
				}else if("01".equals(respServ)){
					throw new AddcelException(1, "No existe el pedido buscado.");
				}else if("00".equals(respServ) || "-1".equals(respServ)){
					
				}else{
					throw new AddcelException(16, "Respuesta desconocida Magento.");
				}
			}else{
				throw new AddcelException(11, "Respuesta vacia Magento.");
			}
		}catch(AxisFault a){
			if(StringUtils.isNumeric(a.getFaultCode().toString())){
				throw new AddcelException(Integer.parseInt(a.getFaultCode().toString()), a.getFaultString());
			}else if(a.getFaultString().contains("java.net.SocketTimeoutException") ){
				throw new AddcelException(17, "Error al leer la respuesta de Magento.");
			}else if(a.getFaultString().contains("java.net.ConnectException") ){
				throw new AddcelException(18, "Error en la conexion con Magento.");
			}else{
				throw new AddcelException(19, "Error desconocido en Magento.");
			}
		}catch (RemoteException e){
			logger.error("Error cambiaStatus Magento: ", e);
			throw new AddcelException(-4, "Error: " + e.getMessage());
		}
		return respServ;
	}
	
	public String actualizaDevolucionReintentos (String orderIncrementId, String[] items, String url) throws AddcelException {
		String respServ = null;
		
		respServ = actualizaDevolucion(orderIncrementId, items, url);
		if(respServ != null && "-1".equals(respServ)){

			logger.debug("Intento actualizaDevolucion 2, pedido: " + orderIncrementId);
			respServ = actualizaDevolucion(orderIncrementId, items, url);
			if(respServ != null && "-1".equals(respServ)){
				throw new AddcelException(16, "Error interno Magento.");
			}
		}
		return respServ;
	}
	
	public String actualizaDevolucion (String orderIncrementId, String[] items, String url) throws AddcelException {
		String respServ = null;
		String itemsHash = "";
		HashMap<String, Integer> data = new HashMap<String, Integer>();
		Integer[] cantidad = null;
		String[] numero = null;
		try{
			for(String upc: items){
				if(data.containsKey(upc)){
					data.put(upc, data.get(upc) + 1);
				}else{
					data.put(upc, 1);
				}
			}
			
			items = data.keySet().toArray(new String[data.keySet().size()]);
			cantidad = data.values().toArray(new Integer[data.keySet().size()]);
			numero = new String[data.keySet().size()];
			
			for(int i = 0; i < items.length; i++){
				itemsHash += items[i];
				numero[i] = cantidad[i] + "";
			}
			
			respServ = new ConsultaPedidosProxy(url).actualizaDevolucion(
					orderIncrementId, obtenerHASH1(orderIncrementId + itemsHash + Constantes.KEY_MAGENTO ), items, numero);
			
			logger.debug("Respuesta cancelacion parcial: " + respServ);
			if(respServ != null){
				if("66".equals(respServ)){
					throw new AddcelException(66, "Problema en el Acceso a Magento.");
				}else if("01".equals(respServ)){
					throw new AddcelException(1, "No existe el pedido buscado.");
				}else if("02".equals(respServ)){
					throw new AddcelException(2, "El pedido esta en Estatus invalido.");
				}else if("00".equals(respServ) || "-1".equals(respServ)){
					
				}else{
					throw new AddcelException(16, "Respuesta desconocida Magento.");
				}
			}else{
				throw new AddcelException(11, "Respuesta vacia Magento.");
			}
		}catch(AxisFault a){
			if(StringUtils.isNumeric(a.getFaultCode().toString())){
				throw new AddcelException(Integer.parseInt(a.getFaultCode().toString()), a.getFaultString());
			}else if(a.getFaultString().contains("java.net.SocketTimeoutException") ){
				throw new AddcelException(17, "Error al leer la respuesta de Magento.");
			}else if(a.getFaultString().contains("java.net.ConnectException") ){
				throw new AddcelException(18, "Error en la conexion con Magento.");
			}else{
				throw new AddcelException(19, "Error desconocido en Magento.");
			}
		}catch (RemoteException e){
			logger.error("Error cambiaStatus Magento: ", e);
			throw new AddcelException(-4, "Error: " + e.getMessage());
		}
		return respServ;
	}
	
	public String obtenerHASH1(String cadena){
		byte[] buffer, digest;
		MessageDigest md = null;
		String hash = "";
		int b = 0;
		try {
			buffer = cadena.getBytes();
			md = MessageDigest.getInstance("SHA1");
			md.update(buffer);
			digest = md.digest();

			for(byte aux : digest) {
				b = aux & 0xff;
				if (Integer.toHexString(b).length() == 1) hash += "0";
					hash += Integer.toHexString(b);
			}
			
//			logger.debug("SHA1:  " + hash);
		} catch (NoSuchAlgorithmException e) {
			logger.error("Error in HASH1: ", e);
		}
		return hash;
	}
	
	public String formatoMonto(double monto){
		DecimalFormat df = new DecimalFormat("######.00");
		String totalPedido = null;
		try{
			totalPedido = df.format(monto );
			totalPedido = totalPedido.substring(0, totalPedido.indexOf(".")) + 
					totalPedido.substring(totalPedido.indexOf(".") +1, totalPedido.length());
		}catch(Exception e){
			logger.error("Error en formtaMonto: " + e.getMessage());
		}
		return totalPedido;
	}
}
