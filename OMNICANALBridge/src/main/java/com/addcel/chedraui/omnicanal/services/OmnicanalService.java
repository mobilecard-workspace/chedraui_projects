package com.addcel.chedraui.omnicanal.services;

import static com.addcel.chedraui.omnicanal.utils.Constantes.MAX_CUPONES_ITEM_PEDIDO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.chedraui.omnicanal.exception.AddcelException;
import com.addcel.chedraui.omnicanal.model.mapper.OmnicanalMapper;
import com.addcel.chedraui.omnicanal.model.vo.AbstractVO;
import com.addcel.chedraui.omnicanal.model.vo.AddCelSFTPVO;
import com.addcel.chedraui.omnicanal.model.vo.DatosTransaccion;
import com.addcel.chedraui.omnicanal.model.vo.DatosTransaccionResponse;
import com.addcel.chedraui.omnicanal.rompefilas.model.vo.OmniPedidoResponse;
import com.addcel.chedraui.omnicanal.utils.AddCelSFTPUtil;
import com.addcel.chedraui.omnicanal.utils.Constantes;
import com.addcel.chedraui.omnicanal.utils.UtilsService;
import com.addcel.ws.clientes.enigma.magento.RespuestaConsultarPedido;
import com.addcel.ws.clientes.enigma.magento.RespuestaConsultarPedidoCupones;
import com.addcel.ws.clientes.enigma.magento.RespuestaConsultarPedidoItem;
import com.addcel.ws.clientes.enigma.magento.RespuestaConsultarPedidoPromociones;
import com.addcel.ws.clientes.enigma.magento.RespuestaConsultarTiendasPedido;
import com.addcel.ws.clientes.enigma.magento.RespuestaDetallePromosByPedidoItem;
import com.sun.istack.internal.Nullable;

@Service
public class OmnicanalService {
	private static final Logger logger = LoggerFactory.getLogger(OmnicanalService.class);
	
	@Autowired
	private OmnicanalMapper mapper;
	
	@Autowired
	private AddCelSFTPUtil sftpUtil;
	
	@Autowired
	private UtilsService utilService;
	
	@Autowired
	private MagentoService magento;
	
	private static final String PLACEHOLDER = " ";
	
	public String procesaTransaccion(String json) {
		DatosTransaccion data = null;
		AbstractVO resp = null;
		String jsonRes = null;
		AddCelSFTPVO sftp = null;
		try{
			logger.debug("Peticion: " + json);
			data = utilService.jsonToObject(json, DatosTransaccion.class);				
			
//			mapper.agregarBitacora(data);
			
			sftp = mapper.getDataTienda(String.valueOf(Integer.parseInt(data.getStore_sap())));
			
			if(sftp == null ){
				resp = new AbstractVO(-15, "NO existe configuracion para la tienda: " + data.getStore_sap());
			}else{
				data.setUrlWS(sftp.getUrlWS());
//				logger.debug("URL MAGENTO: " + data.getUrlWS());
				
				switch (data.getIndicador()){
				case 1:
				case 101:
					jsonRes = validarPedidoArchivo(data, sftp);
					break;
					
				case 2:
				case 102:
					jsonRes = validarPedidoInfo(data);
					break;
					
				case 3:
				case 4:
				case 5:
				case 6:
				case 103:
				case 104:
				case 105:
				case 106:
					jsonRes = cambiaStatus(data);
					break;
					
				case 7:
				case 107:
					jsonRes = cancelaPedidoParcial(data);
					break;
				case 8:
				case 108:
					jsonRes = cambiaStatus(data);
					break;
				case 100:
					jsonRes = obtenerPedido(data);
					break;
					
				default:
					resp = new AbstractVO(-12, "El Indicador no existe.");
				};
			}
		}catch(Exception e){
			logger.error("Error General: ", e);
			resp = new AbstractVO(-10, "Ocurrio un error: " + e.getMessage());
		}finally{
			if(jsonRes == null){
				jsonRes = utilService.objectToJson(resp);
			}
			
			/*
			 * try{
				if(data != null){
						resp = (AbstractVO) utilService.jsonToObject(jsonRes, AbstractVO.class);
						mapper.actualizarBitacora(data.getId(), resp.getIdError(), resp.getMsgError().length() <= 150? resp.getMsgError(): resp.getMsgError().substring(0, 149));
					}
				}catch(Exception ex){
					logger.error("Error Update bitacora: ", ex);
				}
			*/
		}
		logger.debug("Peticion Respuesta: " + jsonRes);
		
		return jsonRes;		
	}
	
	public String obtenerPedido(DatosTransaccion data) {
		
		RespuestaConsultarPedido respServ = null;
		OmniPedidoResponse resp = null;
		String jsonResp = null;
		
		try {
			respServ = magento.consultaPedidoReintentos(data.getOrderIncrementId(), data.getUrlWS());
			resp = new OmniPedidoResponse();
			resp.setIdError(0);
			resp.setMsgError("Consulta exitosa");
			resp.setPedido(respServ);
		} catch (AddcelException e) {
			// TODO Auto-generated catch block
			resp = new OmniPedidoResponse(e.getId(), e.getMessage());
		} catch (Exception e) {
			resp = new OmniPedidoResponse(-14, "Error: " + e.getMessage());
		} finally {
			jsonResp = utilService.objectToJson(resp);
		}
		
		return jsonResp;
		
	}
	
	public String validarPedidoArchivo (DatosTransaccion data, AddCelSFTPVO sftp) {
		RespuestaConsultarPedido respServ = null;
		RespuestaConsultarPedido respServOriginal = null;
		AbstractVO resp = null;
		String jsonRes = null;
		String orderIncrementId = null;
		StringBuilder cadFile = new StringBuilder();
		String respMet = null;
		String fileName = null;
		//DATOS PADRE
		RespuestaConsultarPedido respServPadre = null;
		
		//RespuestaConsultarPedidoPromociones
		
		try{
			if(data.getOrderIncrementId().indexOf("-") > 0){
				orderIncrementId = data.getOrderIncrementId().substring(0, data.getOrderIncrementId().indexOf("-"));
			}else{
				orderIncrementId = data.getOrderIncrementId();
			}
			
			respServ = magento.consultaPedidoReintentos(data.getOrderIncrementId(), data.getUrlWS());
			
			if(respServ != null) {
					
				if(!data.getOrderIncrementId().equals(orderIncrementId)){
					respServOriginal = magento.consultaPedido(orderIncrementId, data.getUrlWS());
				}
				
				String tiendaPago = null;
				String idPedidoOrigen = null;
				
				if (!utilService.isNullOrEmpty(respServ.getPedidoOrigen()) && !respServ.getPedidoOrigen().equals(respServ.getNumeroPedido())) {
					
					if(respServ.getPedidoOrigen().indexOf("-") > 0){
						idPedidoOrigen = respServ.getPedidoOrigen().substring(0, respServ.getPedidoOrigen().indexOf("-"));
					}else{
						idPedidoOrigen = respServ.getPedidoOrigen();
					}
					
					
					logger.debug("Pedido Origen: " + idPedidoOrigen);
					respServPadre = magento.consultaPedidoReintentos(idPedidoOrigen, data.getUrlWS());
					
					if (respServPadre != null) {
						tiendaPago = respServPadre.getTienda();
						logger.debug("Tienda pedido padre: " + tiendaPago);
					} else {
						logger.error("No se pudo obtener pedido padre");
					}
					
				} else {
					tiendaPago = respServ.getTiendaPago();
				}
				
				cadFile.append("0;").append(respServ.getNumeroPedido()).append(";").append(respServ.getTienda()).append(";")
				.append(magento.formatoMonto(respServ.getTotal().doubleValue())).append(";")
				.append(tiendaPago).append(";").append(respServ.getStatus()).append(";")
				.append(magento.formatoMonto(respServ.getTotalEnvio().doubleValue())).append(";")
				.append(respServ.getMetodoEnvio()).append("\r\n");
				
				if(respServ.getItemsp() != null && respServ.getItemsp().length > 0){
					for(RespuestaConsultarPedidoItem item: respServ.getItemsp()){
						
						if (respServ.getItempTienda()!= null && utilService.isNullOrEmptyPost(item.getTipoProducto(), PLACEHOLDER).equals("P")) {
							
							boolean tieneProximidad = false;
							
							for (RespuestaConsultarTiendasPedido tPedido: respServ.getItempTienda()) {
								
								if (tPedido.getSku() != null && tPedido.getSku().equals(item.getSku())) {

									cadFile.append("1;") //Tipo de Registro
										.append(item.getSku()).append(";") //UPC
										.append(item.getEsPesable()).append(";") //Tipo de producto
										.append(magento.formatoMonto(item.getPrecio().doubleValue())).append(";") //Precio unitario
										.append(tPedido.getTotalArticulos().intValue()).append(";") //Cantidad o peso
										.append(utilService.isNullOrEmptyPost(item.getTipoBeneficio(), PLACEHOLDER)).append(";") //Tipo Beneficio
										.append(utilService.isNullOrEmptyPost(item.getPromoType(), PLACEHOLDER)).append(";") //Promo Type
										.append(utilService.isNullOrEmptyPost(item.getPromoId(), PLACEHOLDER)).append(";") //Promo ID
										.append(utilService.isNullOrEmptyPost(item.getDescripcionPromo(), PLACEHOLDER)).append(";") //Descripción Promoción
										.append(magento.formatoMonto(item.getMontoBeneficio()!= null ? item.getMontoBeneficio().doubleValue(): 0)).append(";") //Monto Beneficio
										.append(utilService.isNullOrEmptyPost(
														item.getPorcentajeDescuentoBonificacion() != null && item.getPorcentajeDescuentoBonificacion().doubleValue() > 0 
															? magento.formatoMonto(item.getPorcentajeDescuentoBonificacion().doubleValue()):"", PLACEHOLDER)).append(";") //1Porcentaje Descuento / Porcentaje Bonificación
										.append(utilService.isNullOrEmptyPost(item.getNumeroTarjeta(), PLACEHOLDER)).append(";") //Numero Tarjeta
										//.append(respServ.getPlazos()).append(";")
										.append(utilService.isNullOrEmptyPost(item.getNumeroAutorizacion(), PLACEHOLDER)).append(";") //Número de Autorización
										.append(utilService.isNullOrEmptyPost(item.getDisparador(), PLACEHOLDER)).append(";") //Disparador
										.append(item.getRangoMontoPromocional() != null ? item.getRangoMontoPromocional():"").append(";") //Rango Monto Promocional
										.append(utilService.isNullOrEmptyPost(tPedido.getTienda(), PLACEHOLDER)).append(";") //Tienda de surtido
										.append(buildDescripcionStr(item.getItemPromo()))
										.append("\r\n");
									
									tieneProximidad = true;
								}
							}
							
							if (!tieneProximidad) {
								cadFile.append("1;")
									.append(item.getSku()).append(";")
									.append(item.getEsPesable()).append(";")
									.append(magento.formatoMonto(item.getPrecio().doubleValue())).append(";")
									.append(item.getEsPesable() != null && "0".equals(item.getEsPesable()) ? 
											item.getTotalArticulos().intValue() : ((int)(item.getTotalArticulos().doubleValue()*1000))).append(";")
									.append(utilService.isNullOrEmptyPost(item.getTipoBeneficio(), PLACEHOLDER)).append(";")
									.append(utilService.isNullOrEmptyPost(item.getPromoType(), PLACEHOLDER)).append(";")
									.append(utilService.isNullOrEmptyPost(item.getPromoId(), PLACEHOLDER)).append(";")
									.append(utilService.isNullOrEmptyPost(item.getDescripcionPromo(), PLACEHOLDER)).append(";")
									.append(magento.formatoMonto(item.getMontoBeneficio()!= null? item.getMontoBeneficio().doubleValue(): 0)).append(";")
									.append(
											utilService.isNullOrEmptyPost(
													item.getPorcentajeDescuentoBonificacion() != null && item.getPorcentajeDescuentoBonificacion().doubleValue() > 0?
															magento.formatoMonto(item.getPorcentajeDescuentoBonificacion().doubleValue()):"", PLACEHOLDER)).append(";")
									.append(utilService.isNullOrEmptyPost(item.getNumeroTarjeta(), PLACEHOLDER)).append(";")
									//.append(respServ.getPlazos()).append(";")
									.append(utilService.isNullOrEmptyPost(item.getNumeroAutorizacion(), PLACEHOLDER)).append(";")
									.append(utilService.isNullOrEmptyPost(item.getDisparador(), PLACEHOLDER)).append(";")
									.append(item.getRangoMontoPromocional() != null ? item.getRangoMontoPromocional():"").append(";")
									.append(utilService.isNullOrEmptyPost(respServ.getTienda(), PLACEHOLDER)).append(";")
									.append(buildDescripcionStr(item.getItemPromo()))
									.append("\r\n");
							}
							
							
						} else {
							
							cadFile.append("1;").append(item.getSku()).append(";").append(item.getEsPesable()).append(";")
							.append(magento.formatoMonto(item.getPrecio().doubleValue())).append(";")
							.append(item.getEsPesable() != null && "0".equals(item.getEsPesable()) ? 
									item.getTotalArticulos().intValue() : ((int)(item.getTotalArticulos().doubleValue()*1000))).append(";")
							.append(utilService.isNullOrEmptyPost(item.getTipoBeneficio(), PLACEHOLDER)).append(";").append(utilService.isNullOrEmptyPost(item.getPromoType(), PLACEHOLDER)).append(";")
							.append(utilService.isNullOrEmptyPost(item.getPromoId(), PLACEHOLDER)).append(";").append(utilService.isNullOrEmptyPost(item.getDescripcionPromo(), PLACEHOLDER)).append(";")
							.append(magento.formatoMonto(item.getMontoBeneficio()!= null? item.getMontoBeneficio().doubleValue(): 0)).append(";")
							.append(
									utilService.isNullOrEmptyPost(
											item.getPorcentajeDescuentoBonificacion() != null && item.getPorcentajeDescuentoBonificacion().doubleValue() > 0? 
													magento.formatoMonto(item.getPorcentajeDescuentoBonificacion().doubleValue()):"", PLACEHOLDER)).append(";")
							.append(utilService.isNullOrEmptyPost(item.getNumeroTarjeta(), PLACEHOLDER)).append(";")
							//.append(respServ.getPlazos()).append(";")
							.append(utilService.isNullOrEmptyPost(item.getNumeroAutorizacion(), PLACEHOLDER)).append(";")
							.append(utilService.isNullOrEmptyPost(item.getDisparador(), PLACEHOLDER)).append(";")
							.append(item.getRangoMontoPromocional() != null ? item.getRangoMontoPromocional():"").append(";")
							.append(utilService.isNullOrEmptyPost(respServ.getTienda(), PLACEHOLDER)).append(";")
							.append(buildDescripcionStr(item.getItemPromo()))
							.append("\r\n");
						}
						
						
					}
				}
				
				cadFile.append("5;")
				.append("01".equalsIgnoreCase(respServ.getMetodoPago()) ?Constantes.STATUS_NUM_CONTRA_ENTREGA:
					"02".equalsIgnoreCase(respServ.getMetodoPago()) ? Constantes.STATUS_NUM_CREDITO_AMEX:
					"03".equalsIgnoreCase(respServ.getMetodoPago()) ? Constantes.STATUS_NUM_CREDITO_BANCOMER: 
					"04".equalsIgnoreCase(respServ.getMetodoPago()) ? Constantes.STATUS_NUM_PEDIDO_PAGADO_OTRA_TIENDA:
					"05".equalsIgnoreCase(respServ.getMetodoPago()) ? Constantes.STATUS_NUM_MONEDERO_CHEDRAUI:
						respServ.getMetodoPago() // Se envía por default el método de pago recibido de MAGENTO
//						Constantes.STATUS_NUM_PEDIDO_PAGADO_OTRA_TIENDA
						).append(";")
				.append(
						!data.getOrderIncrementId().equals(orderIncrementId) && !"01".equalsIgnoreCase(respServ.getMetodoPago())?
							magento.formatoMonto(respServOriginal.getTotal().doubleValue()):
							magento.formatoMonto(respServ.getTotal().doubleValue())
						).append(";")
				.append(utilService.isNullOrEmptyPost(respServ.getReferencia().trim(), PLACEHOLDER)).append(";")
//				.append("02".equalsIgnoreCase(respServ.getMetodoPago()) ||
//				"03".equalsIgnoreCase(respServ.getMetodoPago())? ";02296;" : "; ;") // Se colocaba este texto porque no se obtenía la referencia bancaria
				.append(utilService.isNullOrEmptyPost(respServ.getTarjeta(), PLACEHOLDER)) //Solicitado por Alfonso y Germán de chedraui
//				.append(" ")
				.append("\r\n");
				
				if (respServ.getItempCupon() != null && respServ.getItempCupon().length > 0) {
					  
  					for(RespuestaConsultarPedidoCupones cupon: respServ.getItempCupon()) {
  						if (cupon != null) {
							
  							cadFile.append("6;")
  							.append(utilService.isNullOrEmptyPost(cupon.getNumeroCupon(), PLACEHOLDER)).append(";").append(utilService.isNullOrEmptyPost(cupon.getAccionCupon(), PLACEHOLDER)).append(";")
  							.append(utilService.isNullOrEmptyPost(cupon.getOferta(), PLACEHOLDER)).append(";").append(utilService.isNullOrEmptyPost(cupon.getPromoType(), PLACEHOLDER)).append(";")	
  							.append(utilService.isNullOrEmptyPost(cupon.getMontoCupon(), PLACEHOLDER)).append(";").append(utilService.isNullOrEmptyPost(cupon.getTipoCupon(), PLACEHOLDER)).append(";")
  							.append(utilService.isNullOrEmptyPost(cupon.getEstado(), PLACEHOLDER)).append(";")
  							.append(utilService.isNullOrEmptyPost(cupon.getArticulo(), PLACEHOLDER)).append(";")
  							.append(utilService.isNullOrEmptyPost(cupon.getDescripcionCupon(), PLACEHOLDER))
  							.append("\r\n");
  							
  						}
  					}
  
				}
				
//  				ITERACIÓN SOBRE ARREGLO respServ.getItemProm()
  
  				if (respServ.getItempProm() != null && respServ.getItempProm().length > 0) {
  
  					for(RespuestaConsultarPedidoPromociones promocion: respServ.getItempProm()) {
  						if (promocion != null) {
							
							cadFile.append("7;") //TIPO REGISTRO
							.append(utilService.isNullOrEmptyPost(promocion.getTipoBeneficio(), PLACEHOLDER)).append(";") //TIPO BENEFICIO
							.append(utilService.isNullOrEmptyPost(promocion.getPromoType(), PLACEHOLDER)).append(";") //PROMO TYPE
							.append(utilService.isNullOrEmptyPost(promocion.getPromoId(), PLACEHOLDER)).append(";") //PROMO ID
							.append(utilService.isNullOrEmptyPost(promocion.getDescripcionPromo(), PLACEHOLDER)).append(";") //DESCRIPCION PROMOCION
							.append(magento.formatoMonto(promocion.getMontoBeneficio() != null ? promocion.getMontoBeneficio().doubleValue() : 0)).append(";") //MONTO BENEFICIO
							.append(utilService.isNullOrEmptyPost(
									promocion.getPorcentajeDescuentoBonificacion() != null && promocion.getPorcentajeDescuentoBonificacion().doubleValue() > 0 ?
											magento.formatoMonto(promocion.getPorcentajeDescuentoBonificacion().doubleValue()):"", PLACEHOLDER)).append(";") //Porcentaje Descuento / Porcentaje Bonificación
							.append(utilService.isNullOrEmptyPost(
									utilService.isNullOrEmptyPost(promocion.getNumeroTarjeta(), PLACEHOLDER), PLACEHOLDER)).append(";") //NUMERO TARJETA
							.append(utilService.isNullOrEmptyPost(utilService.isNullOrEmptyPost(promocion.getPlazosPago(), PLACEHOLDER), PLACEHOLDER)).append(";") //PLAZOS DE PAGO
							.append(utilService.isNullOrEmptyPost(promocion.getDisparador(), PLACEHOLDER)).append(";") //DISPARADOR
							.append(promocion.getRangoMontoPromocional() != null ? promocion.getRangoMontoPromocional() : "").append(";") //RANGO MONTO PROMOCIONAL
							.append(utilService.isNullOrEmptyPost(promocion.getNumeroAutorizacion(), PLACEHOLDER)) //NUMERO DE AUTORIZACION
							.append("\r\n");
  							
  						}
  					}
  
				}
 
  				
			
				if(orderIncrementId.length() > 8){
					fileName = orderIncrementId.substring(0, orderIncrementId.length() - 3) + "." +
							orderIncrementId.substring(orderIncrementId.length() - 3);
				}else{
					fileName = orderIncrementId;
				}
				
				respMet = sftpUtil.makeFile(fileName, cadFile.toString(), sftp);
				if(respMet == null){
					respMet = sftpUtil.sendFileviaSFTP(fileName, sftp);
					
					if(respMet != null){
						resp = new AbstractVO(-8, respMet);
					}else{
						resp = new AbstractVO(0, "Transacción Exitosa.");
					}
				}else{
					resp = new AbstractVO(-8, "Error al generar Archivo.");
				}
			}
		} catch(AddcelException e) {
			e.printStackTrace();
			resp = new AbstractVO(e.getId(), e.getMessage());
		} catch(Exception e) {
			logger.error("Error validarPedidoInfo: ", e);
			resp = new AbstractVO(-14, "Error: " + e.getMessage());
		} finally {
			jsonRes = utilService.objectToJson(resp);
		}
		
		return jsonRes;
	}
	
	public String validarPedidoInfo (DatosTransaccion data) {
		RespuestaConsultarPedido respServ = null;
		RespuestaConsultarPedido respServOriginal = null;
		DatosTransaccionResponse resp = null;
		String jsonRes = null;
		double totalCompra = 0;
		
		try{
			respServ = magento.consultaPedidoReintentos(data.getOrderIncrementId(), data.getUrlWS());
			if(respServ != null){
//				logger.debug("Metodo Pago: " + respServ.getMetodoPago());
				if("01".equalsIgnoreCase(respServ.getMetodoPago()) ||
						("04".equalsIgnoreCase(respServ.getMetodoPago()) && 
								Constantes.STATUS_PENDIENTE_PAGO.equalsIgnoreCase(respServ.getStatus()))){
					totalCompra = respServ.getTotal().doubleValue();
				}else{
					// para tipo 02 amex, 03 bancomer y 04 pago en misma tienda
					// se maneja la misma logica
					logger.debug("Contiene guion: " + data.getOrderIncrementId().indexOf("-"));
					if(data.getOrderIncrementId().indexOf("-") > 0){
						respServOriginal = magento.consultaPedido(data.getOrderIncrementId().substring(0, data.getOrderIncrementId().indexOf("-")), data.getUrlWS());
						
						totalCompra = (respServ.getTotal().doubleValue()) -
								(respServOriginal.getTotal().doubleValue());
						if(totalCompra < 0){
							totalCompra = 0;
						}
					}else{
						totalCompra = 0;
					}
				}
				
				resp = new DatosTransaccionResponse();
				resp.setMsgError("Transacción Exitosa.");
				resp.setOrderIncrementId(respServ.getNumeroPedido());
				resp.setStore_org_sap(respServ.getTienda());
				resp.setGrand_total(magento.formatoMonto(totalCompra));
				resp.setPayment_type(
						"01".equalsIgnoreCase(respServ.getMetodoPago())? Constantes.STATUS_CONTRA_ENTREGA:
						("02".equalsIgnoreCase(respServ.getMetodoPago()) || "03".equalsIgnoreCase(respServ.getMetodoPago()))? Constantes.STATUS_PAGO_EN_LÍNEA: 
						"04".equalsIgnoreCase(respServ.getMetodoPago()) ? Constantes.STATUS_PAGADO_OTRA_TIENDA: Constantes.STATUS_PAGADO_OTRA_TIENDA);
				resp.setPayment_ref(
						"01".equalsIgnoreCase(respServ.getMetodoPago()) ? Constantes.STATUS_NUM_CONTRA_ENTREGA:
						"02".equalsIgnoreCase(respServ.getMetodoPago()) ? Constantes.STATUS_NUM_CREDITO_AMEX:
						"03".equalsIgnoreCase(respServ.getMetodoPago()) ? Constantes.STATUS_NUM_CREDITO_BANCOMER: 
						"04".equalsIgnoreCase(respServ.getMetodoPago()) ? Constantes.STATUS_NUM_PEDIDO_PAGADO_OTRA_TIENDA:
						"05".equalsIgnoreCase(respServ.getMetodoPago()) ? Constantes.STATUS_NUM_MONEDERO_CHEDRAUI:
							respServ.getMetodoPago() // Se envía por default el método de pago recibido de MAGENTO
//							Constantes.STATUS_NUM_PEDIDO_PAGADO_OTRA_TIENDA
							);
				resp.setStatus(respServ.getStatus());
			}
		}catch(AddcelException e){
			resp = new DatosTransaccionResponse(e.getId(), e.getMessage());
		}catch(Exception e){
			logger.error("Error validarPedidoInfo: ", e);
			resp = new DatosTransaccionResponse(-14, "Error: " + e.getMessage());
		}finally{
			jsonRes = utilService.objectToJson(resp);
		}
		
		return jsonRes;
	}
	
	public String cambiaStatus (DatosTransaccion data) {
		RespuestaConsultarPedido respS = null;
		String json = null;
		AbstractVO resp = null;
		String status = null;
		String respServ = null;
		
		try{
			if(data.getIndicador() == 5){
				respS = magento.consultaPedidoReintentos(data.getOrderIncrementId(), data.getUrlWS());
				
				if(!data.getStore_sap().equalsIgnoreCase(respS.getTienda())){
					respServ = magento.actualizaTiendaPagoReintentos(data.getOrderIncrementId(), data.getStore_sap(), data.getUrlWS() );
					logger.debug("Respuesta cambio tienda: " + respServ);
				}
			}
			
			switch (data.getIndicador()){
			case 3:
			case 103:
				status = Constantes.STATUS_EN_PROCESO_ENTREGA;
				break;
				
			case 4:
			case 104:
				status = Constantes.STATUS_RENDIR_ENTREGA;
				break;
				
			case 5:
			case 105:
				status = Constantes.STATUS_EN_PAGADO;
				break;
			
			case 6:
			case 106:
				status = Constantes.STATUS_EN_CANCELAR;
				break;
			
			case 8:
			case 108:
				status = data.getStatus();
				break;
				
			default:
				resp = new AbstractVO(-12, "El Indicador no existe.");
			};
			
			magento.actualizaStatusReintentos(data.getOrderIncrementId(), status, data.getUrlWS());
			resp = new AbstractVO(0, "Transacción Exitosa.");
			
		}catch(AddcelException e){
			resp = new AbstractVO(e.getId(), e.getMessage());

		}catch (Exception e){
			logger.error("Error cambiaStatus: ", e);
			resp = new AbstractVO(-14, "An error was obtained: " + e.getMessage());
		}finally{
			json = utilService.objectToJson(resp);
		}
		return json;
	}
	
	public String cancelaPedidoParcial (DatosTransaccion data) {
		String jsonRes = null;
		AbstractVO resp = null;
		String[] arreglo = null;
		
		try{
			arreglo = data.getCancel_items().toArray(new String[data.getCancel_items().size()]);
			magento.actualizaDevolucionReintentos(data.getOrderIncrementId(), arreglo, data.getUrlWS());
			
			magento.actualizaStatusReintentos(data.getOrderIncrementId(), Constantes.STATUS_CANCELADO_PARCIAL, data.getUrlWS());

			resp = new AbstractVO(0, "Transacción Exitosa.");
		}catch(AddcelException e){
			resp = new AbstractVO(e.getId(), e.getMessage());
		}catch(Exception e){
			logger.error("Error validarPedidoInfo: ", e);
			resp = new AbstractVO(-14, "Error: " + e.getMessage());
		}finally{
			jsonRes = utilService.objectToJson(resp);
		}
		return jsonRes;
	}
	
	private String buildDescripcionStr(@Nullable RespuestaDetallePromosByPedidoItem[] arr) {
		
		if (null == arr || 0 == arr.length) {
			logger.info("Arreglo RespuestaDetallePromosByPedidoItem nulo o vacío");
			return " ; ; ; ; ; ; ; ; ; ";
		}
		
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < MAX_CUPONES_ITEM_PEDIDO; i++) {
			
			if (i < arr.length && null != arr[i]) {
				
				sb.append(utilService.isNullOrEmptyPost(arr[i].getPromoId(), PLACEHOLDER)).append(";")
					.append(
						utilService.isNullOrCero(
								arr[i].getMontoBeneficio()) ? PLACEHOLDER:
									magento.formatoMonto(
											utilService.getDoubleValueFromGeneric(
													arr[i].getMontoBeneficio())));
			} else {
				sb.append(" ; ");
			}
	
			
			if (i < (MAX_CUPONES_ITEM_PEDIDO - 1)) {
				sb.append(";");
			}
		}
		
		return sb.toString();
		
	}
	
}
