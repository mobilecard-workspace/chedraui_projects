package com.addcel.chedraui.omnicanal.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AbstractVO {	
	private int idError;
	private String msgError;
	
	public AbstractVO() {}
	
	public AbstractVO(int idError, String msgError) {	
		this.idError = idError;
		this.msgError = msgError;
	}
	public int getIdError() {
		return idError;
	}
	public void setIdError(int idError) {
		this.idError = idError;
	}

	public String getMsgError() {
		return msgError;
	}

	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
		
}
