/**
 * ConsultaPedidosService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.enigma.magento;

public interface ConsultaPedidosService extends javax.xml.rpc.Service {
    public java.lang.String getConsultaPedidosAddress();

    public com.addcel.ws.clientes.enigma.magento.ConsultaPedidos getConsultaPedidos() throws javax.xml.rpc.ServiceException;

    public com.addcel.ws.clientes.enigma.magento.ConsultaPedidos getConsultaPedidos(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
