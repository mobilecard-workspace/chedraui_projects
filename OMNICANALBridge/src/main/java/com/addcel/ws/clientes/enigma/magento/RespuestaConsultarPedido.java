/**
 * RespuestaConsultarPedido.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.enigma.magento;

public class RespuestaConsultarPedido  implements java.io.Serializable {
    private com.addcel.ws.clientes.enigma.magento.RespuestaConsultarPedidoCupones[] itempCupon;

    private com.addcel.ws.clientes.enigma.magento.RespuestaConsultarPedidoPromociones[] itempProm;

    private com.addcel.ws.clientes.enigma.magento.RespuestaConsultarTiendasPedido[] itempTienda;

    private com.addcel.ws.clientes.enigma.magento.RespuestaConsultarPedidoItem[] itemsp;

    private java.lang.String metodoEnvio;

    private java.lang.String metodoPago;

    private java.lang.String numeroPedido;

    private com.addcel.ws.clientes.enigma.magento.hibernate.entities.SalesFlatOrder pedido;

    private java.lang.String pedidoOrigen;

    private java.lang.Short plazos;

    private java.lang.String referencia;

    private java.lang.String respuesta;

    private java.lang.String status;

    private java.lang.String tarjeta;

    private java.lang.String tienda;

    private java.lang.String tiendaPago;

    private java.math.BigDecimal total;

    private java.math.BigDecimal totalEnvio;

    public RespuestaConsultarPedido() {
    }

    public RespuestaConsultarPedido(
           com.addcel.ws.clientes.enigma.magento.RespuestaConsultarPedidoCupones[] itempCupon,
           com.addcel.ws.clientes.enigma.magento.RespuestaConsultarPedidoPromociones[] itempProm,
           com.addcel.ws.clientes.enigma.magento.RespuestaConsultarTiendasPedido[] itempTienda,
           com.addcel.ws.clientes.enigma.magento.RespuestaConsultarPedidoItem[] itemsp,
           java.lang.String metodoEnvio,
           java.lang.String metodoPago,
           java.lang.String numeroPedido,
           com.addcel.ws.clientes.enigma.magento.hibernate.entities.SalesFlatOrder pedido,
           java.lang.String pedidoOrigen,
           java.lang.Short plazos,
           java.lang.String referencia,
           java.lang.String respuesta,
           java.lang.String status,
           java.lang.String tarjeta,
           java.lang.String tienda,
           java.lang.String tiendaPago,
           java.math.BigDecimal total,
           java.math.BigDecimal totalEnvio) {
           this.itempCupon = itempCupon;
           this.itempProm = itempProm;
           this.itempTienda = itempTienda;
           this.itemsp = itemsp;
           this.metodoEnvio = metodoEnvio;
           this.metodoPago = metodoPago;
           this.numeroPedido = numeroPedido;
           this.pedido = pedido;
           this.pedidoOrigen = pedidoOrigen;
           this.plazos = plazos;
           this.referencia = referencia;
           this.respuesta = respuesta;
           this.status = status;
           this.tarjeta = tarjeta;
           this.tienda = tienda;
           this.tiendaPago = tiendaPago;
           this.total = total;
           this.totalEnvio = totalEnvio;
    }


    /**
     * Gets the itempCupon value for this RespuestaConsultarPedido.
     * 
     * @return itempCupon
     */
    public com.addcel.ws.clientes.enigma.magento.RespuestaConsultarPedidoCupones[] getItempCupon() {
        return itempCupon;
    }


    /**
     * Sets the itempCupon value for this RespuestaConsultarPedido.
     * 
     * @param itempCupon
     */
    public void setItempCupon(com.addcel.ws.clientes.enigma.magento.RespuestaConsultarPedidoCupones[] itempCupon) {
        this.itempCupon = itempCupon;
    }


    /**
     * Gets the itempProm value for this RespuestaConsultarPedido.
     * 
     * @return itempProm
     */
    public com.addcel.ws.clientes.enigma.magento.RespuestaConsultarPedidoPromociones[] getItempProm() {
        return itempProm;
    }


    /**
     * Sets the itempProm value for this RespuestaConsultarPedido.
     * 
     * @param itempProm
     */
    public void setItempProm(com.addcel.ws.clientes.enigma.magento.RespuestaConsultarPedidoPromociones[] itempProm) {
        this.itempProm = itempProm;
    }


    /**
     * Gets the itempTienda value for this RespuestaConsultarPedido.
     * 
     * @return itempTienda
     */
    public com.addcel.ws.clientes.enigma.magento.RespuestaConsultarTiendasPedido[] getItempTienda() {
        return itempTienda;
    }


    /**
     * Sets the itempTienda value for this RespuestaConsultarPedido.
     * 
     * @param itempTienda
     */
    public void setItempTienda(com.addcel.ws.clientes.enigma.magento.RespuestaConsultarTiendasPedido[] itempTienda) {
        this.itempTienda = itempTienda;
    }


    /**
     * Gets the itemsp value for this RespuestaConsultarPedido.
     * 
     * @return itemsp
     */
    public com.addcel.ws.clientes.enigma.magento.RespuestaConsultarPedidoItem[] getItemsp() {
        return itemsp;
    }


    /**
     * Sets the itemsp value for this RespuestaConsultarPedido.
     * 
     * @param itemsp
     */
    public void setItemsp(com.addcel.ws.clientes.enigma.magento.RespuestaConsultarPedidoItem[] itemsp) {
        this.itemsp = itemsp;
    }


    /**
     * Gets the metodoEnvio value for this RespuestaConsultarPedido.
     * 
     * @return metodoEnvio
     */
    public java.lang.String getMetodoEnvio() {
        return metodoEnvio;
    }


    /**
     * Sets the metodoEnvio value for this RespuestaConsultarPedido.
     * 
     * @param metodoEnvio
     */
    public void setMetodoEnvio(java.lang.String metodoEnvio) {
        this.metodoEnvio = metodoEnvio;
    }


    /**
     * Gets the metodoPago value for this RespuestaConsultarPedido.
     * 
     * @return metodoPago
     */
    public java.lang.String getMetodoPago() {
        return metodoPago;
    }


    /**
     * Sets the metodoPago value for this RespuestaConsultarPedido.
     * 
     * @param metodoPago
     */
    public void setMetodoPago(java.lang.String metodoPago) {
        this.metodoPago = metodoPago;
    }


    /**
     * Gets the numeroPedido value for this RespuestaConsultarPedido.
     * 
     * @return numeroPedido
     */
    public java.lang.String getNumeroPedido() {
        return numeroPedido;
    }


    /**
     * Sets the numeroPedido value for this RespuestaConsultarPedido.
     * 
     * @param numeroPedido
     */
    public void setNumeroPedido(java.lang.String numeroPedido) {
        this.numeroPedido = numeroPedido;
    }


    /**
     * Gets the pedido value for this RespuestaConsultarPedido.
     * 
     * @return pedido
     */
    public com.addcel.ws.clientes.enigma.magento.hibernate.entities.SalesFlatOrder getPedido() {
        return pedido;
    }


    /**
     * Sets the pedido value for this RespuestaConsultarPedido.
     * 
     * @param pedido
     */
    public void setPedido(com.addcel.ws.clientes.enigma.magento.hibernate.entities.SalesFlatOrder pedido) {
        this.pedido = pedido;
    }


    /**
     * Gets the pedidoOrigen value for this RespuestaConsultarPedido.
     * 
     * @return pedidoOrigen
     */
    public java.lang.String getPedidoOrigen() {
        return pedidoOrigen;
    }


    /**
     * Sets the pedidoOrigen value for this RespuestaConsultarPedido.
     * 
     * @param pedidoOrigen
     */
    public void setPedidoOrigen(java.lang.String pedidoOrigen) {
        this.pedidoOrigen = pedidoOrigen;
    }


    /**
     * Gets the plazos value for this RespuestaConsultarPedido.
     * 
     * @return plazos
     */
    public java.lang.Short getPlazos() {
        return plazos;
    }


    /**
     * Sets the plazos value for this RespuestaConsultarPedido.
     * 
     * @param plazos
     */
    public void setPlazos(java.lang.Short plazos) {
        this.plazos = plazos;
    }


    /**
     * Gets the referencia value for this RespuestaConsultarPedido.
     * 
     * @return referencia
     */
    public java.lang.String getReferencia() {
        return referencia;
    }


    /**
     * Sets the referencia value for this RespuestaConsultarPedido.
     * 
     * @param referencia
     */
    public void setReferencia(java.lang.String referencia) {
        this.referencia = referencia;
    }


    /**
     * Gets the respuesta value for this RespuestaConsultarPedido.
     * 
     * @return respuesta
     */
    public java.lang.String getRespuesta() {
        return respuesta;
    }


    /**
     * Sets the respuesta value for this RespuestaConsultarPedido.
     * 
     * @param respuesta
     */
    public void setRespuesta(java.lang.String respuesta) {
        this.respuesta = respuesta;
    }


    /**
     * Gets the status value for this RespuestaConsultarPedido.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this RespuestaConsultarPedido.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the tarjeta value for this RespuestaConsultarPedido.
     * 
     * @return tarjeta
     */
    public java.lang.String getTarjeta() {
        return tarjeta;
    }


    /**
     * Sets the tarjeta value for this RespuestaConsultarPedido.
     * 
     * @param tarjeta
     */
    public void setTarjeta(java.lang.String tarjeta) {
        this.tarjeta = tarjeta;
    }


    /**
     * Gets the tienda value for this RespuestaConsultarPedido.
     * 
     * @return tienda
     */
    public java.lang.String getTienda() {
        return tienda;
    }


    /**
     * Sets the tienda value for this RespuestaConsultarPedido.
     * 
     * @param tienda
     */
    public void setTienda(java.lang.String tienda) {
        this.tienda = tienda;
    }


    /**
     * Gets the tiendaPago value for this RespuestaConsultarPedido.
     * 
     * @return tiendaPago
     */
    public java.lang.String getTiendaPago() {
        return tiendaPago;
    }


    /**
     * Sets the tiendaPago value for this RespuestaConsultarPedido.
     * 
     * @param tiendaPago
     */
    public void setTiendaPago(java.lang.String tiendaPago) {
        this.tiendaPago = tiendaPago;
    }


    /**
     * Gets the total value for this RespuestaConsultarPedido.
     * 
     * @return total
     */
    public java.math.BigDecimal getTotal() {
        return total;
    }


    /**
     * Sets the total value for this RespuestaConsultarPedido.
     * 
     * @param total
     */
    public void setTotal(java.math.BigDecimal total) {
        this.total = total;
    }


    /**
     * Gets the totalEnvio value for this RespuestaConsultarPedido.
     * 
     * @return totalEnvio
     */
    public java.math.BigDecimal getTotalEnvio() {
        return totalEnvio;
    }


    /**
     * Sets the totalEnvio value for this RespuestaConsultarPedido.
     * 
     * @param totalEnvio
     */
    public void setTotalEnvio(java.math.BigDecimal totalEnvio) {
        this.totalEnvio = totalEnvio;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RespuestaConsultarPedido)) return false;
        RespuestaConsultarPedido other = (RespuestaConsultarPedido) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.itempCupon==null && other.getItempCupon()==null) || 
             (this.itempCupon!=null &&
              java.util.Arrays.equals(this.itempCupon, other.getItempCupon()))) &&
            ((this.itempProm==null && other.getItempProm()==null) || 
             (this.itempProm!=null &&
              java.util.Arrays.equals(this.itempProm, other.getItempProm()))) &&
            ((this.itempTienda==null && other.getItempTienda()==null) || 
             (this.itempTienda!=null &&
              java.util.Arrays.equals(this.itempTienda, other.getItempTienda()))) &&
            ((this.itemsp==null && other.getItemsp()==null) || 
             (this.itemsp!=null &&
              java.util.Arrays.equals(this.itemsp, other.getItemsp()))) &&
            ((this.metodoEnvio==null && other.getMetodoEnvio()==null) || 
             (this.metodoEnvio!=null &&
              this.metodoEnvio.equals(other.getMetodoEnvio()))) &&
            ((this.metodoPago==null && other.getMetodoPago()==null) || 
             (this.metodoPago!=null &&
              this.metodoPago.equals(other.getMetodoPago()))) &&
            ((this.numeroPedido==null && other.getNumeroPedido()==null) || 
             (this.numeroPedido!=null &&
              this.numeroPedido.equals(other.getNumeroPedido()))) &&
            ((this.pedido==null && other.getPedido()==null) || 
             (this.pedido!=null &&
              this.pedido.equals(other.getPedido()))) &&
            ((this.pedidoOrigen==null && other.getPedidoOrigen()==null) || 
             (this.pedidoOrigen!=null &&
              this.pedidoOrigen.equals(other.getPedidoOrigen()))) &&
            ((this.plazos==null && other.getPlazos()==null) || 
             (this.plazos!=null &&
              this.plazos.equals(other.getPlazos()))) &&
            ((this.referencia==null && other.getReferencia()==null) || 
             (this.referencia!=null &&
              this.referencia.equals(other.getReferencia()))) &&
            ((this.respuesta==null && other.getRespuesta()==null) || 
             (this.respuesta!=null &&
              this.respuesta.equals(other.getRespuesta()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.tarjeta==null && other.getTarjeta()==null) || 
             (this.tarjeta!=null &&
              this.tarjeta.equals(other.getTarjeta()))) &&
            ((this.tienda==null && other.getTienda()==null) || 
             (this.tienda!=null &&
              this.tienda.equals(other.getTienda()))) &&
            ((this.tiendaPago==null && other.getTiendaPago()==null) || 
             (this.tiendaPago!=null &&
              this.tiendaPago.equals(other.getTiendaPago()))) &&
            ((this.total==null && other.getTotal()==null) || 
             (this.total!=null &&
              this.total.equals(other.getTotal()))) &&
            ((this.totalEnvio==null && other.getTotalEnvio()==null) || 
             (this.totalEnvio!=null &&
              this.totalEnvio.equals(other.getTotalEnvio())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getItempCupon() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getItempCupon());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getItempCupon(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getItempProm() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getItempProm());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getItempProm(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getItempTienda() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getItempTienda());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getItempTienda(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getItemsp() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getItemsp());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getItemsp(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMetodoEnvio() != null) {
            _hashCode += getMetodoEnvio().hashCode();
        }
        if (getMetodoPago() != null) {
            _hashCode += getMetodoPago().hashCode();
        }
        if (getNumeroPedido() != null) {
            _hashCode += getNumeroPedido().hashCode();
        }
        if (getPedido() != null) {
            _hashCode += getPedido().hashCode();
        }
        if (getPedidoOrigen() != null) {
            _hashCode += getPedidoOrigen().hashCode();
        }
        if (getPlazos() != null) {
            _hashCode += getPlazos().hashCode();
        }
        if (getReferencia() != null) {
            _hashCode += getReferencia().hashCode();
        }
        if (getRespuesta() != null) {
            _hashCode += getRespuesta().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getTarjeta() != null) {
            _hashCode += getTarjeta().hashCode();
        }
        if (getTienda() != null) {
            _hashCode += getTienda().hashCode();
        }
        if (getTiendaPago() != null) {
            _hashCode += getTiendaPago().hashCode();
        }
        if (getTotal() != null) {
            _hashCode += getTotal().hashCode();
        }
        if (getTotalEnvio() != null) {
            _hashCode += getTotalEnvio().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RespuestaConsultarPedido.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "RespuestaConsultarPedido"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itempCupon");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "itempCupon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "RespuestaConsultarPedidoCupones"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itempProm");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "itempProm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "RespuestaConsultarPedidoPromociones"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itempTienda");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "itempTienda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "RespuestaConsultarTiendasPedido"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemsp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "itemsp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "RespuestaConsultarPedidoItem"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("metodoEnvio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "metodoEnvio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("metodoPago");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "metodoPago"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroPedido");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "numeroPedido"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pedido");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "pedido"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "SalesFlatOrder"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pedidoOrigen");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "pedidoOrigen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("plazos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "plazos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referencia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "referencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("respuesta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "respuesta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tarjeta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "tarjeta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tienda");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "tienda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tiendaPago");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "tiendaPago"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("total");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "total"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalEnvio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "totalEnvio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
