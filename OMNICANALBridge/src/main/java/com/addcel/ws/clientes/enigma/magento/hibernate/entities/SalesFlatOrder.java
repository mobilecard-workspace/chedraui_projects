/**
 * SalesFlatOrder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.enigma.magento.hibernate.entities;

public class SalesFlatOrder  implements java.io.Serializable {
    private java.lang.String XForwardedFor;

    private java.math.BigDecimal adjustmentNegative;

    private java.math.BigDecimal adjustmentPositive;

    private java.lang.String appliedRuleIds;

    private java.math.BigDecimal baseAdjustmentNegative;

    private java.math.BigDecimal baseAdjustmentPositive;

    private java.lang.String baseCurrencyCode;

    private java.math.BigDecimal baseDiscountAmount;

    private java.math.BigDecimal baseDiscountCanceled;

    private java.math.BigDecimal baseDiscountInvoiced;

    private java.math.BigDecimal baseDiscountRefunded;

    private java.math.BigDecimal baseGrandTotal;

    private java.math.BigDecimal baseHiddenTaxAmount;

    private java.math.BigDecimal baseHiddenTaxInvoiced;

    private java.math.BigDecimal baseHiddenTaxRefunded;

    private java.math.BigDecimal baseShippingAmount;

    private java.math.BigDecimal baseShippingCanceled;

    private java.math.BigDecimal baseShippingDiscountAmount;

    private java.math.BigDecimal baseShippingHiddenTaxAmnt;

    private java.math.BigDecimal baseShippingInclTax;

    private java.math.BigDecimal baseShippingInvoiced;

    private java.math.BigDecimal baseShippingRefunded;

    private java.math.BigDecimal baseShippingTaxAmount;

    private java.math.BigDecimal baseShippingTaxRefunded;

    private java.math.BigDecimal baseSubtotal;

    private java.math.BigDecimal baseSubtotalCanceled;

    private java.math.BigDecimal baseSubtotalInclTax;

    private java.math.BigDecimal baseSubtotalInvoiced;

    private java.math.BigDecimal baseSubtotalRefunded;

    private java.math.BigDecimal baseTaxAmount;

    private java.math.BigDecimal baseTaxCanceled;

    private java.math.BigDecimal baseTaxInvoiced;

    private java.math.BigDecimal baseTaxRefunded;

    private java.math.BigDecimal baseToGlobalRate;

    private java.math.BigDecimal baseToOrderRate;

    private java.math.BigDecimal baseTotalCanceled;

    private java.math.BigDecimal baseTotalDue;

    private java.math.BigDecimal baseTotalInvoiced;

    private java.math.BigDecimal baseTotalInvoicedCost;

    private java.math.BigDecimal baseTotalOfflineRefunded;

    private java.math.BigDecimal baseTotalOnlineRefunded;

    private java.math.BigDecimal baseTotalPaid;

    private java.math.BigDecimal baseTotalQtyOrdered;

    private java.math.BigDecimal baseTotalRefunded;

    private java.lang.Integer billingAddressId;

    private java.lang.Short canShipPartially;

    private java.lang.Short canShipPartiallyItem;

    private java.lang.String couponCode;

    private java.lang.String couponRuleName;

    private java.lang.String createdAt;

    private java.lang.String customerDob;

    private java.lang.String customerEmail;

    private java.lang.String customerFirstname;

    private java.lang.Integer customerGender;

    private java.lang.Short customerGroupId;

    private java.lang.Integer customerId;

    private java.lang.Short customerIsGuest;

    private java.lang.String customerLastname;

    private java.lang.String customerMiddlename;

    private java.lang.String customerNote;

    private java.lang.Short customerNoteNotify;

    private java.lang.String customerPrefix;

    private java.lang.String customerSuffix;

    private java.lang.String customerTaxvat;

    private java.math.BigDecimal discountAmount;

    private java.math.BigDecimal discountCanceled;

    private java.lang.String discountDescription;

    private java.math.BigDecimal discountInvoiced;

    private java.math.BigDecimal discountRefunded;

    private java.lang.Integer editIncrement;

    private java.lang.Short emailSent;

    private java.lang.Integer entityId;

    private java.lang.String extCustomerId;

    private java.lang.String extOrderId;

    private java.lang.Short forcedShipmentWithInvoice;

    private java.lang.Integer giftMessageId;

    private java.lang.String globalCurrencyCode;

    private java.math.BigDecimal grandTotal;

    private java.math.BigDecimal hiddenTaxAmount;

    private java.math.BigDecimal hiddenTaxInvoiced;

    private java.math.BigDecimal hiddenTaxRefunded;

    private java.lang.String holdBeforeState;

    private java.lang.String holdBeforeStatus;

    private java.lang.String incrementId;

    private java.lang.Short isVirtual;

    private java.lang.String orderCurrencyCode;

    private java.lang.String originalIncrementId;

    private java.lang.Integer paymentAuthExpiration;

    private java.math.BigDecimal paymentAuthorizationAmount;

    private java.lang.Integer paypalIpnCustomerNotified;

    private java.lang.Integer pendingTlog;

    private java.lang.String protectCode;

    private java.lang.Integer quoteAddressId;

    private java.lang.Integer quoteId;

    private java.lang.String relationChildId;

    private java.lang.String relationChildRealId;

    private java.lang.String relationParentId;

    private java.lang.String relationParentRealId;

    private java.lang.String remoteIp;

    private java.lang.Integer shippingAddressId;

    private java.math.BigDecimal shippingAmount;

    private java.math.BigDecimal shippingCanceled;

    private java.lang.String shippingDescription;

    private java.math.BigDecimal shippingDiscountAmount;

    private java.math.BigDecimal shippingHiddenTaxAmount;

    private java.math.BigDecimal shippingInclTax;

    private java.math.BigDecimal shippingInvoiced;

    private java.lang.String shippingMethod;

    private java.math.BigDecimal shippingRefunded;

    private java.math.BigDecimal shippingTaxAmount;

    private java.math.BigDecimal shippingTaxRefunded;

    private java.lang.String state;

    private java.lang.String status;

    private java.lang.String storeCurrencyCode;

    private java.lang.Short storeId;

    private java.lang.String storeName;

    private java.math.BigDecimal storeToBaseRate;

    private java.math.BigDecimal storeToOrderRate;

    private java.math.BigDecimal subtotal;

    private java.math.BigDecimal subtotalCanceled;

    private java.math.BigDecimal subtotalInclTax;

    private java.math.BigDecimal subtotalInvoiced;

    private java.math.BigDecimal subtotalRefunded;

    private java.lang.Integer supplierId;

    private java.math.BigDecimal taxAmount;

    private java.math.BigDecimal taxCanceled;

    private java.math.BigDecimal taxInvoiced;

    private java.math.BigDecimal taxRefunded;

    private java.math.BigDecimal totalCanceled;

    private java.math.BigDecimal totalDue;

    private java.math.BigDecimal totalInvoiced;

    private short totalItemCount;

    private java.math.BigDecimal totalOfflineRefunded;

    private java.math.BigDecimal totalOnlineRefunded;

    private java.math.BigDecimal totalPaid;

    private java.math.BigDecimal totalQtyOrdered;

    private java.math.BigDecimal totalRefunded;

    private java.lang.String updatedAt;

    private java.math.BigDecimal weight;

    public SalesFlatOrder() {
    }

    public SalesFlatOrder(
           java.lang.String XForwardedFor,
           java.math.BigDecimal adjustmentNegative,
           java.math.BigDecimal adjustmentPositive,
           java.lang.String appliedRuleIds,
           java.math.BigDecimal baseAdjustmentNegative,
           java.math.BigDecimal baseAdjustmentPositive,
           java.lang.String baseCurrencyCode,
           java.math.BigDecimal baseDiscountAmount,
           java.math.BigDecimal baseDiscountCanceled,
           java.math.BigDecimal baseDiscountInvoiced,
           java.math.BigDecimal baseDiscountRefunded,
           java.math.BigDecimal baseGrandTotal,
           java.math.BigDecimal baseHiddenTaxAmount,
           java.math.BigDecimal baseHiddenTaxInvoiced,
           java.math.BigDecimal baseHiddenTaxRefunded,
           java.math.BigDecimal baseShippingAmount,
           java.math.BigDecimal baseShippingCanceled,
           java.math.BigDecimal baseShippingDiscountAmount,
           java.math.BigDecimal baseShippingHiddenTaxAmnt,
           java.math.BigDecimal baseShippingInclTax,
           java.math.BigDecimal baseShippingInvoiced,
           java.math.BigDecimal baseShippingRefunded,
           java.math.BigDecimal baseShippingTaxAmount,
           java.math.BigDecimal baseShippingTaxRefunded,
           java.math.BigDecimal baseSubtotal,
           java.math.BigDecimal baseSubtotalCanceled,
           java.math.BigDecimal baseSubtotalInclTax,
           java.math.BigDecimal baseSubtotalInvoiced,
           java.math.BigDecimal baseSubtotalRefunded,
           java.math.BigDecimal baseTaxAmount,
           java.math.BigDecimal baseTaxCanceled,
           java.math.BigDecimal baseTaxInvoiced,
           java.math.BigDecimal baseTaxRefunded,
           java.math.BigDecimal baseToGlobalRate,
           java.math.BigDecimal baseToOrderRate,
           java.math.BigDecimal baseTotalCanceled,
           java.math.BigDecimal baseTotalDue,
           java.math.BigDecimal baseTotalInvoiced,
           java.math.BigDecimal baseTotalInvoicedCost,
           java.math.BigDecimal baseTotalOfflineRefunded,
           java.math.BigDecimal baseTotalOnlineRefunded,
           java.math.BigDecimal baseTotalPaid,
           java.math.BigDecimal baseTotalQtyOrdered,
           java.math.BigDecimal baseTotalRefunded,
           java.lang.Integer billingAddressId,
           java.lang.Short canShipPartially,
           java.lang.Short canShipPartiallyItem,
           java.lang.String couponCode,
           java.lang.String couponRuleName,
           java.lang.String createdAt,
           java.lang.String customerDob,
           java.lang.String customerEmail,
           java.lang.String customerFirstname,
           java.lang.Integer customerGender,
           java.lang.Short customerGroupId,
           java.lang.Integer customerId,
           java.lang.Short customerIsGuest,
           java.lang.String customerLastname,
           java.lang.String customerMiddlename,
           java.lang.String customerNote,
           java.lang.Short customerNoteNotify,
           java.lang.String customerPrefix,
           java.lang.String customerSuffix,
           java.lang.String customerTaxvat,
           java.math.BigDecimal discountAmount,
           java.math.BigDecimal discountCanceled,
           java.lang.String discountDescription,
           java.math.BigDecimal discountInvoiced,
           java.math.BigDecimal discountRefunded,
           java.lang.Integer editIncrement,
           java.lang.Short emailSent,
           java.lang.Integer entityId,
           java.lang.String extCustomerId,
           java.lang.String extOrderId,
           java.lang.Short forcedShipmentWithInvoice,
           java.lang.Integer giftMessageId,
           java.lang.String globalCurrencyCode,
           java.math.BigDecimal grandTotal,
           java.math.BigDecimal hiddenTaxAmount,
           java.math.BigDecimal hiddenTaxInvoiced,
           java.math.BigDecimal hiddenTaxRefunded,
           java.lang.String holdBeforeState,
           java.lang.String holdBeforeStatus,
           java.lang.String incrementId,
           java.lang.Short isVirtual,
           java.lang.String orderCurrencyCode,
           java.lang.String originalIncrementId,
           java.lang.Integer paymentAuthExpiration,
           java.math.BigDecimal paymentAuthorizationAmount,
           java.lang.Integer paypalIpnCustomerNotified,
           java.lang.Integer pendingTlog,
           java.lang.String protectCode,
           java.lang.Integer quoteAddressId,
           java.lang.Integer quoteId,
           java.lang.String relationChildId,
           java.lang.String relationChildRealId,
           java.lang.String relationParentId,
           java.lang.String relationParentRealId,
           java.lang.String remoteIp,
           java.lang.Integer shippingAddressId,
           java.math.BigDecimal shippingAmount,
           java.math.BigDecimal shippingCanceled,
           java.lang.String shippingDescription,
           java.math.BigDecimal shippingDiscountAmount,
           java.math.BigDecimal shippingHiddenTaxAmount,
           java.math.BigDecimal shippingInclTax,
           java.math.BigDecimal shippingInvoiced,
           java.lang.String shippingMethod,
           java.math.BigDecimal shippingRefunded,
           java.math.BigDecimal shippingTaxAmount,
           java.math.BigDecimal shippingTaxRefunded,
           java.lang.String state,
           java.lang.String status,
           java.lang.String storeCurrencyCode,
           java.lang.Short storeId,
           java.lang.String storeName,
           java.math.BigDecimal storeToBaseRate,
           java.math.BigDecimal storeToOrderRate,
           java.math.BigDecimal subtotal,
           java.math.BigDecimal subtotalCanceled,
           java.math.BigDecimal subtotalInclTax,
           java.math.BigDecimal subtotalInvoiced,
           java.math.BigDecimal subtotalRefunded,
           java.lang.Integer supplierId,
           java.math.BigDecimal taxAmount,
           java.math.BigDecimal taxCanceled,
           java.math.BigDecimal taxInvoiced,
           java.math.BigDecimal taxRefunded,
           java.math.BigDecimal totalCanceled,
           java.math.BigDecimal totalDue,
           java.math.BigDecimal totalInvoiced,
           short totalItemCount,
           java.math.BigDecimal totalOfflineRefunded,
           java.math.BigDecimal totalOnlineRefunded,
           java.math.BigDecimal totalPaid,
           java.math.BigDecimal totalQtyOrdered,
           java.math.BigDecimal totalRefunded,
           java.lang.String updatedAt,
           java.math.BigDecimal weight) {
           this.XForwardedFor = XForwardedFor;
           this.adjustmentNegative = adjustmentNegative;
           this.adjustmentPositive = adjustmentPositive;
           this.appliedRuleIds = appliedRuleIds;
           this.baseAdjustmentNegative = baseAdjustmentNegative;
           this.baseAdjustmentPositive = baseAdjustmentPositive;
           this.baseCurrencyCode = baseCurrencyCode;
           this.baseDiscountAmount = baseDiscountAmount;
           this.baseDiscountCanceled = baseDiscountCanceled;
           this.baseDiscountInvoiced = baseDiscountInvoiced;
           this.baseDiscountRefunded = baseDiscountRefunded;
           this.baseGrandTotal = baseGrandTotal;
           this.baseHiddenTaxAmount = baseHiddenTaxAmount;
           this.baseHiddenTaxInvoiced = baseHiddenTaxInvoiced;
           this.baseHiddenTaxRefunded = baseHiddenTaxRefunded;
           this.baseShippingAmount = baseShippingAmount;
           this.baseShippingCanceled = baseShippingCanceled;
           this.baseShippingDiscountAmount = baseShippingDiscountAmount;
           this.baseShippingHiddenTaxAmnt = baseShippingHiddenTaxAmnt;
           this.baseShippingInclTax = baseShippingInclTax;
           this.baseShippingInvoiced = baseShippingInvoiced;
           this.baseShippingRefunded = baseShippingRefunded;
           this.baseShippingTaxAmount = baseShippingTaxAmount;
           this.baseShippingTaxRefunded = baseShippingTaxRefunded;
           this.baseSubtotal = baseSubtotal;
           this.baseSubtotalCanceled = baseSubtotalCanceled;
           this.baseSubtotalInclTax = baseSubtotalInclTax;
           this.baseSubtotalInvoiced = baseSubtotalInvoiced;
           this.baseSubtotalRefunded = baseSubtotalRefunded;
           this.baseTaxAmount = baseTaxAmount;
           this.baseTaxCanceled = baseTaxCanceled;
           this.baseTaxInvoiced = baseTaxInvoiced;
           this.baseTaxRefunded = baseTaxRefunded;
           this.baseToGlobalRate = baseToGlobalRate;
           this.baseToOrderRate = baseToOrderRate;
           this.baseTotalCanceled = baseTotalCanceled;
           this.baseTotalDue = baseTotalDue;
           this.baseTotalInvoiced = baseTotalInvoiced;
           this.baseTotalInvoicedCost = baseTotalInvoicedCost;
           this.baseTotalOfflineRefunded = baseTotalOfflineRefunded;
           this.baseTotalOnlineRefunded = baseTotalOnlineRefunded;
           this.baseTotalPaid = baseTotalPaid;
           this.baseTotalQtyOrdered = baseTotalQtyOrdered;
           this.baseTotalRefunded = baseTotalRefunded;
           this.billingAddressId = billingAddressId;
           this.canShipPartially = canShipPartially;
           this.canShipPartiallyItem = canShipPartiallyItem;
           this.couponCode = couponCode;
           this.couponRuleName = couponRuleName;
           this.createdAt = createdAt;
           this.customerDob = customerDob;
           this.customerEmail = customerEmail;
           this.customerFirstname = customerFirstname;
           this.customerGender = customerGender;
           this.customerGroupId = customerGroupId;
           this.customerId = customerId;
           this.customerIsGuest = customerIsGuest;
           this.customerLastname = customerLastname;
           this.customerMiddlename = customerMiddlename;
           this.customerNote = customerNote;
           this.customerNoteNotify = customerNoteNotify;
           this.customerPrefix = customerPrefix;
           this.customerSuffix = customerSuffix;
           this.customerTaxvat = customerTaxvat;
           this.discountAmount = discountAmount;
           this.discountCanceled = discountCanceled;
           this.discountDescription = discountDescription;
           this.discountInvoiced = discountInvoiced;
           this.discountRefunded = discountRefunded;
           this.editIncrement = editIncrement;
           this.emailSent = emailSent;
           this.entityId = entityId;
           this.extCustomerId = extCustomerId;
           this.extOrderId = extOrderId;
           this.forcedShipmentWithInvoice = forcedShipmentWithInvoice;
           this.giftMessageId = giftMessageId;
           this.globalCurrencyCode = globalCurrencyCode;
           this.grandTotal = grandTotal;
           this.hiddenTaxAmount = hiddenTaxAmount;
           this.hiddenTaxInvoiced = hiddenTaxInvoiced;
           this.hiddenTaxRefunded = hiddenTaxRefunded;
           this.holdBeforeState = holdBeforeState;
           this.holdBeforeStatus = holdBeforeStatus;
           this.incrementId = incrementId;
           this.isVirtual = isVirtual;
           this.orderCurrencyCode = orderCurrencyCode;
           this.originalIncrementId = originalIncrementId;
           this.paymentAuthExpiration = paymentAuthExpiration;
           this.paymentAuthorizationAmount = paymentAuthorizationAmount;
           this.paypalIpnCustomerNotified = paypalIpnCustomerNotified;
           this.pendingTlog = pendingTlog;
           this.protectCode = protectCode;
           this.quoteAddressId = quoteAddressId;
           this.quoteId = quoteId;
           this.relationChildId = relationChildId;
           this.relationChildRealId = relationChildRealId;
           this.relationParentId = relationParentId;
           this.relationParentRealId = relationParentRealId;
           this.remoteIp = remoteIp;
           this.shippingAddressId = shippingAddressId;
           this.shippingAmount = shippingAmount;
           this.shippingCanceled = shippingCanceled;
           this.shippingDescription = shippingDescription;
           this.shippingDiscountAmount = shippingDiscountAmount;
           this.shippingHiddenTaxAmount = shippingHiddenTaxAmount;
           this.shippingInclTax = shippingInclTax;
           this.shippingInvoiced = shippingInvoiced;
           this.shippingMethod = shippingMethod;
           this.shippingRefunded = shippingRefunded;
           this.shippingTaxAmount = shippingTaxAmount;
           this.shippingTaxRefunded = shippingTaxRefunded;
           this.state = state;
           this.status = status;
           this.storeCurrencyCode = storeCurrencyCode;
           this.storeId = storeId;
           this.storeName = storeName;
           this.storeToBaseRate = storeToBaseRate;
           this.storeToOrderRate = storeToOrderRate;
           this.subtotal = subtotal;
           this.subtotalCanceled = subtotalCanceled;
           this.subtotalInclTax = subtotalInclTax;
           this.subtotalInvoiced = subtotalInvoiced;
           this.subtotalRefunded = subtotalRefunded;
           this.supplierId = supplierId;
           this.taxAmount = taxAmount;
           this.taxCanceled = taxCanceled;
           this.taxInvoiced = taxInvoiced;
           this.taxRefunded = taxRefunded;
           this.totalCanceled = totalCanceled;
           this.totalDue = totalDue;
           this.totalInvoiced = totalInvoiced;
           this.totalItemCount = totalItemCount;
           this.totalOfflineRefunded = totalOfflineRefunded;
           this.totalOnlineRefunded = totalOnlineRefunded;
           this.totalPaid = totalPaid;
           this.totalQtyOrdered = totalQtyOrdered;
           this.totalRefunded = totalRefunded;
           this.updatedAt = updatedAt;
           this.weight = weight;
    }


    /**
     * Gets the XForwardedFor value for this SalesFlatOrder.
     * 
     * @return XForwardedFor
     */
    public java.lang.String getXForwardedFor() {
        return XForwardedFor;
    }


    /**
     * Sets the XForwardedFor value for this SalesFlatOrder.
     * 
     * @param XForwardedFor
     */
    public void setXForwardedFor(java.lang.String XForwardedFor) {
        this.XForwardedFor = XForwardedFor;
    }


    /**
     * Gets the adjustmentNegative value for this SalesFlatOrder.
     * 
     * @return adjustmentNegative
     */
    public java.math.BigDecimal getAdjustmentNegative() {
        return adjustmentNegative;
    }


    /**
     * Sets the adjustmentNegative value for this SalesFlatOrder.
     * 
     * @param adjustmentNegative
     */
    public void setAdjustmentNegative(java.math.BigDecimal adjustmentNegative) {
        this.adjustmentNegative = adjustmentNegative;
    }


    /**
     * Gets the adjustmentPositive value for this SalesFlatOrder.
     * 
     * @return adjustmentPositive
     */
    public java.math.BigDecimal getAdjustmentPositive() {
        return adjustmentPositive;
    }


    /**
     * Sets the adjustmentPositive value for this SalesFlatOrder.
     * 
     * @param adjustmentPositive
     */
    public void setAdjustmentPositive(java.math.BigDecimal adjustmentPositive) {
        this.adjustmentPositive = adjustmentPositive;
    }


    /**
     * Gets the appliedRuleIds value for this SalesFlatOrder.
     * 
     * @return appliedRuleIds
     */
    public java.lang.String getAppliedRuleIds() {
        return appliedRuleIds;
    }


    /**
     * Sets the appliedRuleIds value for this SalesFlatOrder.
     * 
     * @param appliedRuleIds
     */
    public void setAppliedRuleIds(java.lang.String appliedRuleIds) {
        this.appliedRuleIds = appliedRuleIds;
    }


    /**
     * Gets the baseAdjustmentNegative value for this SalesFlatOrder.
     * 
     * @return baseAdjustmentNegative
     */
    public java.math.BigDecimal getBaseAdjustmentNegative() {
        return baseAdjustmentNegative;
    }


    /**
     * Sets the baseAdjustmentNegative value for this SalesFlatOrder.
     * 
     * @param baseAdjustmentNegative
     */
    public void setBaseAdjustmentNegative(java.math.BigDecimal baseAdjustmentNegative) {
        this.baseAdjustmentNegative = baseAdjustmentNegative;
    }


    /**
     * Gets the baseAdjustmentPositive value for this SalesFlatOrder.
     * 
     * @return baseAdjustmentPositive
     */
    public java.math.BigDecimal getBaseAdjustmentPositive() {
        return baseAdjustmentPositive;
    }


    /**
     * Sets the baseAdjustmentPositive value for this SalesFlatOrder.
     * 
     * @param baseAdjustmentPositive
     */
    public void setBaseAdjustmentPositive(java.math.BigDecimal baseAdjustmentPositive) {
        this.baseAdjustmentPositive = baseAdjustmentPositive;
    }


    /**
     * Gets the baseCurrencyCode value for this SalesFlatOrder.
     * 
     * @return baseCurrencyCode
     */
    public java.lang.String getBaseCurrencyCode() {
        return baseCurrencyCode;
    }


    /**
     * Sets the baseCurrencyCode value for this SalesFlatOrder.
     * 
     * @param baseCurrencyCode
     */
    public void setBaseCurrencyCode(java.lang.String baseCurrencyCode) {
        this.baseCurrencyCode = baseCurrencyCode;
    }


    /**
     * Gets the baseDiscountAmount value for this SalesFlatOrder.
     * 
     * @return baseDiscountAmount
     */
    public java.math.BigDecimal getBaseDiscountAmount() {
        return baseDiscountAmount;
    }


    /**
     * Sets the baseDiscountAmount value for this SalesFlatOrder.
     * 
     * @param baseDiscountAmount
     */
    public void setBaseDiscountAmount(java.math.BigDecimal baseDiscountAmount) {
        this.baseDiscountAmount = baseDiscountAmount;
    }


    /**
     * Gets the baseDiscountCanceled value for this SalesFlatOrder.
     * 
     * @return baseDiscountCanceled
     */
    public java.math.BigDecimal getBaseDiscountCanceled() {
        return baseDiscountCanceled;
    }


    /**
     * Sets the baseDiscountCanceled value for this SalesFlatOrder.
     * 
     * @param baseDiscountCanceled
     */
    public void setBaseDiscountCanceled(java.math.BigDecimal baseDiscountCanceled) {
        this.baseDiscountCanceled = baseDiscountCanceled;
    }


    /**
     * Gets the baseDiscountInvoiced value for this SalesFlatOrder.
     * 
     * @return baseDiscountInvoiced
     */
    public java.math.BigDecimal getBaseDiscountInvoiced() {
        return baseDiscountInvoiced;
    }


    /**
     * Sets the baseDiscountInvoiced value for this SalesFlatOrder.
     * 
     * @param baseDiscountInvoiced
     */
    public void setBaseDiscountInvoiced(java.math.BigDecimal baseDiscountInvoiced) {
        this.baseDiscountInvoiced = baseDiscountInvoiced;
    }


    /**
     * Gets the baseDiscountRefunded value for this SalesFlatOrder.
     * 
     * @return baseDiscountRefunded
     */
    public java.math.BigDecimal getBaseDiscountRefunded() {
        return baseDiscountRefunded;
    }


    /**
     * Sets the baseDiscountRefunded value for this SalesFlatOrder.
     * 
     * @param baseDiscountRefunded
     */
    public void setBaseDiscountRefunded(java.math.BigDecimal baseDiscountRefunded) {
        this.baseDiscountRefunded = baseDiscountRefunded;
    }


    /**
     * Gets the baseGrandTotal value for this SalesFlatOrder.
     * 
     * @return baseGrandTotal
     */
    public java.math.BigDecimal getBaseGrandTotal() {
        return baseGrandTotal;
    }


    /**
     * Sets the baseGrandTotal value for this SalesFlatOrder.
     * 
     * @param baseGrandTotal
     */
    public void setBaseGrandTotal(java.math.BigDecimal baseGrandTotal) {
        this.baseGrandTotal = baseGrandTotal;
    }


    /**
     * Gets the baseHiddenTaxAmount value for this SalesFlatOrder.
     * 
     * @return baseHiddenTaxAmount
     */
    public java.math.BigDecimal getBaseHiddenTaxAmount() {
        return baseHiddenTaxAmount;
    }


    /**
     * Sets the baseHiddenTaxAmount value for this SalesFlatOrder.
     * 
     * @param baseHiddenTaxAmount
     */
    public void setBaseHiddenTaxAmount(java.math.BigDecimal baseHiddenTaxAmount) {
        this.baseHiddenTaxAmount = baseHiddenTaxAmount;
    }


    /**
     * Gets the baseHiddenTaxInvoiced value for this SalesFlatOrder.
     * 
     * @return baseHiddenTaxInvoiced
     */
    public java.math.BigDecimal getBaseHiddenTaxInvoiced() {
        return baseHiddenTaxInvoiced;
    }


    /**
     * Sets the baseHiddenTaxInvoiced value for this SalesFlatOrder.
     * 
     * @param baseHiddenTaxInvoiced
     */
    public void setBaseHiddenTaxInvoiced(java.math.BigDecimal baseHiddenTaxInvoiced) {
        this.baseHiddenTaxInvoiced = baseHiddenTaxInvoiced;
    }


    /**
     * Gets the baseHiddenTaxRefunded value for this SalesFlatOrder.
     * 
     * @return baseHiddenTaxRefunded
     */
    public java.math.BigDecimal getBaseHiddenTaxRefunded() {
        return baseHiddenTaxRefunded;
    }


    /**
     * Sets the baseHiddenTaxRefunded value for this SalesFlatOrder.
     * 
     * @param baseHiddenTaxRefunded
     */
    public void setBaseHiddenTaxRefunded(java.math.BigDecimal baseHiddenTaxRefunded) {
        this.baseHiddenTaxRefunded = baseHiddenTaxRefunded;
    }


    /**
     * Gets the baseShippingAmount value for this SalesFlatOrder.
     * 
     * @return baseShippingAmount
     */
    public java.math.BigDecimal getBaseShippingAmount() {
        return baseShippingAmount;
    }


    /**
     * Sets the baseShippingAmount value for this SalesFlatOrder.
     * 
     * @param baseShippingAmount
     */
    public void setBaseShippingAmount(java.math.BigDecimal baseShippingAmount) {
        this.baseShippingAmount = baseShippingAmount;
    }


    /**
     * Gets the baseShippingCanceled value for this SalesFlatOrder.
     * 
     * @return baseShippingCanceled
     */
    public java.math.BigDecimal getBaseShippingCanceled() {
        return baseShippingCanceled;
    }


    /**
     * Sets the baseShippingCanceled value for this SalesFlatOrder.
     * 
     * @param baseShippingCanceled
     */
    public void setBaseShippingCanceled(java.math.BigDecimal baseShippingCanceled) {
        this.baseShippingCanceled = baseShippingCanceled;
    }


    /**
     * Gets the baseShippingDiscountAmount value for this SalesFlatOrder.
     * 
     * @return baseShippingDiscountAmount
     */
    public java.math.BigDecimal getBaseShippingDiscountAmount() {
        return baseShippingDiscountAmount;
    }


    /**
     * Sets the baseShippingDiscountAmount value for this SalesFlatOrder.
     * 
     * @param baseShippingDiscountAmount
     */
    public void setBaseShippingDiscountAmount(java.math.BigDecimal baseShippingDiscountAmount) {
        this.baseShippingDiscountAmount = baseShippingDiscountAmount;
    }


    /**
     * Gets the baseShippingHiddenTaxAmnt value for this SalesFlatOrder.
     * 
     * @return baseShippingHiddenTaxAmnt
     */
    public java.math.BigDecimal getBaseShippingHiddenTaxAmnt() {
        return baseShippingHiddenTaxAmnt;
    }


    /**
     * Sets the baseShippingHiddenTaxAmnt value for this SalesFlatOrder.
     * 
     * @param baseShippingHiddenTaxAmnt
     */
    public void setBaseShippingHiddenTaxAmnt(java.math.BigDecimal baseShippingHiddenTaxAmnt) {
        this.baseShippingHiddenTaxAmnt = baseShippingHiddenTaxAmnt;
    }


    /**
     * Gets the baseShippingInclTax value for this SalesFlatOrder.
     * 
     * @return baseShippingInclTax
     */
    public java.math.BigDecimal getBaseShippingInclTax() {
        return baseShippingInclTax;
    }


    /**
     * Sets the baseShippingInclTax value for this SalesFlatOrder.
     * 
     * @param baseShippingInclTax
     */
    public void setBaseShippingInclTax(java.math.BigDecimal baseShippingInclTax) {
        this.baseShippingInclTax = baseShippingInclTax;
    }


    /**
     * Gets the baseShippingInvoiced value for this SalesFlatOrder.
     * 
     * @return baseShippingInvoiced
     */
    public java.math.BigDecimal getBaseShippingInvoiced() {
        return baseShippingInvoiced;
    }


    /**
     * Sets the baseShippingInvoiced value for this SalesFlatOrder.
     * 
     * @param baseShippingInvoiced
     */
    public void setBaseShippingInvoiced(java.math.BigDecimal baseShippingInvoiced) {
        this.baseShippingInvoiced = baseShippingInvoiced;
    }


    /**
     * Gets the baseShippingRefunded value for this SalesFlatOrder.
     * 
     * @return baseShippingRefunded
     */
    public java.math.BigDecimal getBaseShippingRefunded() {
        return baseShippingRefunded;
    }


    /**
     * Sets the baseShippingRefunded value for this SalesFlatOrder.
     * 
     * @param baseShippingRefunded
     */
    public void setBaseShippingRefunded(java.math.BigDecimal baseShippingRefunded) {
        this.baseShippingRefunded = baseShippingRefunded;
    }


    /**
     * Gets the baseShippingTaxAmount value for this SalesFlatOrder.
     * 
     * @return baseShippingTaxAmount
     */
    public java.math.BigDecimal getBaseShippingTaxAmount() {
        return baseShippingTaxAmount;
    }


    /**
     * Sets the baseShippingTaxAmount value for this SalesFlatOrder.
     * 
     * @param baseShippingTaxAmount
     */
    public void setBaseShippingTaxAmount(java.math.BigDecimal baseShippingTaxAmount) {
        this.baseShippingTaxAmount = baseShippingTaxAmount;
    }


    /**
     * Gets the baseShippingTaxRefunded value for this SalesFlatOrder.
     * 
     * @return baseShippingTaxRefunded
     */
    public java.math.BigDecimal getBaseShippingTaxRefunded() {
        return baseShippingTaxRefunded;
    }


    /**
     * Sets the baseShippingTaxRefunded value for this SalesFlatOrder.
     * 
     * @param baseShippingTaxRefunded
     */
    public void setBaseShippingTaxRefunded(java.math.BigDecimal baseShippingTaxRefunded) {
        this.baseShippingTaxRefunded = baseShippingTaxRefunded;
    }


    /**
     * Gets the baseSubtotal value for this SalesFlatOrder.
     * 
     * @return baseSubtotal
     */
    public java.math.BigDecimal getBaseSubtotal() {
        return baseSubtotal;
    }


    /**
     * Sets the baseSubtotal value for this SalesFlatOrder.
     * 
     * @param baseSubtotal
     */
    public void setBaseSubtotal(java.math.BigDecimal baseSubtotal) {
        this.baseSubtotal = baseSubtotal;
    }


    /**
     * Gets the baseSubtotalCanceled value for this SalesFlatOrder.
     * 
     * @return baseSubtotalCanceled
     */
    public java.math.BigDecimal getBaseSubtotalCanceled() {
        return baseSubtotalCanceled;
    }


    /**
     * Sets the baseSubtotalCanceled value for this SalesFlatOrder.
     * 
     * @param baseSubtotalCanceled
     */
    public void setBaseSubtotalCanceled(java.math.BigDecimal baseSubtotalCanceled) {
        this.baseSubtotalCanceled = baseSubtotalCanceled;
    }


    /**
     * Gets the baseSubtotalInclTax value for this SalesFlatOrder.
     * 
     * @return baseSubtotalInclTax
     */
    public java.math.BigDecimal getBaseSubtotalInclTax() {
        return baseSubtotalInclTax;
    }


    /**
     * Sets the baseSubtotalInclTax value for this SalesFlatOrder.
     * 
     * @param baseSubtotalInclTax
     */
    public void setBaseSubtotalInclTax(java.math.BigDecimal baseSubtotalInclTax) {
        this.baseSubtotalInclTax = baseSubtotalInclTax;
    }


    /**
     * Gets the baseSubtotalInvoiced value for this SalesFlatOrder.
     * 
     * @return baseSubtotalInvoiced
     */
    public java.math.BigDecimal getBaseSubtotalInvoiced() {
        return baseSubtotalInvoiced;
    }


    /**
     * Sets the baseSubtotalInvoiced value for this SalesFlatOrder.
     * 
     * @param baseSubtotalInvoiced
     */
    public void setBaseSubtotalInvoiced(java.math.BigDecimal baseSubtotalInvoiced) {
        this.baseSubtotalInvoiced = baseSubtotalInvoiced;
    }


    /**
     * Gets the baseSubtotalRefunded value for this SalesFlatOrder.
     * 
     * @return baseSubtotalRefunded
     */
    public java.math.BigDecimal getBaseSubtotalRefunded() {
        return baseSubtotalRefunded;
    }


    /**
     * Sets the baseSubtotalRefunded value for this SalesFlatOrder.
     * 
     * @param baseSubtotalRefunded
     */
    public void setBaseSubtotalRefunded(java.math.BigDecimal baseSubtotalRefunded) {
        this.baseSubtotalRefunded = baseSubtotalRefunded;
    }


    /**
     * Gets the baseTaxAmount value for this SalesFlatOrder.
     * 
     * @return baseTaxAmount
     */
    public java.math.BigDecimal getBaseTaxAmount() {
        return baseTaxAmount;
    }


    /**
     * Sets the baseTaxAmount value for this SalesFlatOrder.
     * 
     * @param baseTaxAmount
     */
    public void setBaseTaxAmount(java.math.BigDecimal baseTaxAmount) {
        this.baseTaxAmount = baseTaxAmount;
    }


    /**
     * Gets the baseTaxCanceled value for this SalesFlatOrder.
     * 
     * @return baseTaxCanceled
     */
    public java.math.BigDecimal getBaseTaxCanceled() {
        return baseTaxCanceled;
    }


    /**
     * Sets the baseTaxCanceled value for this SalesFlatOrder.
     * 
     * @param baseTaxCanceled
     */
    public void setBaseTaxCanceled(java.math.BigDecimal baseTaxCanceled) {
        this.baseTaxCanceled = baseTaxCanceled;
    }


    /**
     * Gets the baseTaxInvoiced value for this SalesFlatOrder.
     * 
     * @return baseTaxInvoiced
     */
    public java.math.BigDecimal getBaseTaxInvoiced() {
        return baseTaxInvoiced;
    }


    /**
     * Sets the baseTaxInvoiced value for this SalesFlatOrder.
     * 
     * @param baseTaxInvoiced
     */
    public void setBaseTaxInvoiced(java.math.BigDecimal baseTaxInvoiced) {
        this.baseTaxInvoiced = baseTaxInvoiced;
    }


    /**
     * Gets the baseTaxRefunded value for this SalesFlatOrder.
     * 
     * @return baseTaxRefunded
     */
    public java.math.BigDecimal getBaseTaxRefunded() {
        return baseTaxRefunded;
    }


    /**
     * Sets the baseTaxRefunded value for this SalesFlatOrder.
     * 
     * @param baseTaxRefunded
     */
    public void setBaseTaxRefunded(java.math.BigDecimal baseTaxRefunded) {
        this.baseTaxRefunded = baseTaxRefunded;
    }


    /**
     * Gets the baseToGlobalRate value for this SalesFlatOrder.
     * 
     * @return baseToGlobalRate
     */
    public java.math.BigDecimal getBaseToGlobalRate() {
        return baseToGlobalRate;
    }


    /**
     * Sets the baseToGlobalRate value for this SalesFlatOrder.
     * 
     * @param baseToGlobalRate
     */
    public void setBaseToGlobalRate(java.math.BigDecimal baseToGlobalRate) {
        this.baseToGlobalRate = baseToGlobalRate;
    }


    /**
     * Gets the baseToOrderRate value for this SalesFlatOrder.
     * 
     * @return baseToOrderRate
     */
    public java.math.BigDecimal getBaseToOrderRate() {
        return baseToOrderRate;
    }


    /**
     * Sets the baseToOrderRate value for this SalesFlatOrder.
     * 
     * @param baseToOrderRate
     */
    public void setBaseToOrderRate(java.math.BigDecimal baseToOrderRate) {
        this.baseToOrderRate = baseToOrderRate;
    }


    /**
     * Gets the baseTotalCanceled value for this SalesFlatOrder.
     * 
     * @return baseTotalCanceled
     */
    public java.math.BigDecimal getBaseTotalCanceled() {
        return baseTotalCanceled;
    }


    /**
     * Sets the baseTotalCanceled value for this SalesFlatOrder.
     * 
     * @param baseTotalCanceled
     */
    public void setBaseTotalCanceled(java.math.BigDecimal baseTotalCanceled) {
        this.baseTotalCanceled = baseTotalCanceled;
    }


    /**
     * Gets the baseTotalDue value for this SalesFlatOrder.
     * 
     * @return baseTotalDue
     */
    public java.math.BigDecimal getBaseTotalDue() {
        return baseTotalDue;
    }


    /**
     * Sets the baseTotalDue value for this SalesFlatOrder.
     * 
     * @param baseTotalDue
     */
    public void setBaseTotalDue(java.math.BigDecimal baseTotalDue) {
        this.baseTotalDue = baseTotalDue;
    }


    /**
     * Gets the baseTotalInvoiced value for this SalesFlatOrder.
     * 
     * @return baseTotalInvoiced
     */
    public java.math.BigDecimal getBaseTotalInvoiced() {
        return baseTotalInvoiced;
    }


    /**
     * Sets the baseTotalInvoiced value for this SalesFlatOrder.
     * 
     * @param baseTotalInvoiced
     */
    public void setBaseTotalInvoiced(java.math.BigDecimal baseTotalInvoiced) {
        this.baseTotalInvoiced = baseTotalInvoiced;
    }


    /**
     * Gets the baseTotalInvoicedCost value for this SalesFlatOrder.
     * 
     * @return baseTotalInvoicedCost
     */
    public java.math.BigDecimal getBaseTotalInvoicedCost() {
        return baseTotalInvoicedCost;
    }


    /**
     * Sets the baseTotalInvoicedCost value for this SalesFlatOrder.
     * 
     * @param baseTotalInvoicedCost
     */
    public void setBaseTotalInvoicedCost(java.math.BigDecimal baseTotalInvoicedCost) {
        this.baseTotalInvoicedCost = baseTotalInvoicedCost;
    }


    /**
     * Gets the baseTotalOfflineRefunded value for this SalesFlatOrder.
     * 
     * @return baseTotalOfflineRefunded
     */
    public java.math.BigDecimal getBaseTotalOfflineRefunded() {
        return baseTotalOfflineRefunded;
    }


    /**
     * Sets the baseTotalOfflineRefunded value for this SalesFlatOrder.
     * 
     * @param baseTotalOfflineRefunded
     */
    public void setBaseTotalOfflineRefunded(java.math.BigDecimal baseTotalOfflineRefunded) {
        this.baseTotalOfflineRefunded = baseTotalOfflineRefunded;
    }


    /**
     * Gets the baseTotalOnlineRefunded value for this SalesFlatOrder.
     * 
     * @return baseTotalOnlineRefunded
     */
    public java.math.BigDecimal getBaseTotalOnlineRefunded() {
        return baseTotalOnlineRefunded;
    }


    /**
     * Sets the baseTotalOnlineRefunded value for this SalesFlatOrder.
     * 
     * @param baseTotalOnlineRefunded
     */
    public void setBaseTotalOnlineRefunded(java.math.BigDecimal baseTotalOnlineRefunded) {
        this.baseTotalOnlineRefunded = baseTotalOnlineRefunded;
    }


    /**
     * Gets the baseTotalPaid value for this SalesFlatOrder.
     * 
     * @return baseTotalPaid
     */
    public java.math.BigDecimal getBaseTotalPaid() {
        return baseTotalPaid;
    }


    /**
     * Sets the baseTotalPaid value for this SalesFlatOrder.
     * 
     * @param baseTotalPaid
     */
    public void setBaseTotalPaid(java.math.BigDecimal baseTotalPaid) {
        this.baseTotalPaid = baseTotalPaid;
    }


    /**
     * Gets the baseTotalQtyOrdered value for this SalesFlatOrder.
     * 
     * @return baseTotalQtyOrdered
     */
    public java.math.BigDecimal getBaseTotalQtyOrdered() {
        return baseTotalQtyOrdered;
    }


    /**
     * Sets the baseTotalQtyOrdered value for this SalesFlatOrder.
     * 
     * @param baseTotalQtyOrdered
     */
    public void setBaseTotalQtyOrdered(java.math.BigDecimal baseTotalQtyOrdered) {
        this.baseTotalQtyOrdered = baseTotalQtyOrdered;
    }


    /**
     * Gets the baseTotalRefunded value for this SalesFlatOrder.
     * 
     * @return baseTotalRefunded
     */
    public java.math.BigDecimal getBaseTotalRefunded() {
        return baseTotalRefunded;
    }


    /**
     * Sets the baseTotalRefunded value for this SalesFlatOrder.
     * 
     * @param baseTotalRefunded
     */
    public void setBaseTotalRefunded(java.math.BigDecimal baseTotalRefunded) {
        this.baseTotalRefunded = baseTotalRefunded;
    }


    /**
     * Gets the billingAddressId value for this SalesFlatOrder.
     * 
     * @return billingAddressId
     */
    public java.lang.Integer getBillingAddressId() {
        return billingAddressId;
    }


    /**
     * Sets the billingAddressId value for this SalesFlatOrder.
     * 
     * @param billingAddressId
     */
    public void setBillingAddressId(java.lang.Integer billingAddressId) {
        this.billingAddressId = billingAddressId;
    }


    /**
     * Gets the canShipPartially value for this SalesFlatOrder.
     * 
     * @return canShipPartially
     */
    public java.lang.Short getCanShipPartially() {
        return canShipPartially;
    }


    /**
     * Sets the canShipPartially value for this SalesFlatOrder.
     * 
     * @param canShipPartially
     */
    public void setCanShipPartially(java.lang.Short canShipPartially) {
        this.canShipPartially = canShipPartially;
    }


    /**
     * Gets the canShipPartiallyItem value for this SalesFlatOrder.
     * 
     * @return canShipPartiallyItem
     */
    public java.lang.Short getCanShipPartiallyItem() {
        return canShipPartiallyItem;
    }


    /**
     * Sets the canShipPartiallyItem value for this SalesFlatOrder.
     * 
     * @param canShipPartiallyItem
     */
    public void setCanShipPartiallyItem(java.lang.Short canShipPartiallyItem) {
        this.canShipPartiallyItem = canShipPartiallyItem;
    }


    /**
     * Gets the couponCode value for this SalesFlatOrder.
     * 
     * @return couponCode
     */
    public java.lang.String getCouponCode() {
        return couponCode;
    }


    /**
     * Sets the couponCode value for this SalesFlatOrder.
     * 
     * @param couponCode
     */
    public void setCouponCode(java.lang.String couponCode) {
        this.couponCode = couponCode;
    }


    /**
     * Gets the couponRuleName value for this SalesFlatOrder.
     * 
     * @return couponRuleName
     */
    public java.lang.String getCouponRuleName() {
        return couponRuleName;
    }


    /**
     * Sets the couponRuleName value for this SalesFlatOrder.
     * 
     * @param couponRuleName
     */
    public void setCouponRuleName(java.lang.String couponRuleName) {
        this.couponRuleName = couponRuleName;
    }


    /**
     * Gets the createdAt value for this SalesFlatOrder.
     * 
     * @return createdAt
     */
    public java.lang.String getCreatedAt() {
        return createdAt;
    }


    /**
     * Sets the createdAt value for this SalesFlatOrder.
     * 
     * @param createdAt
     */
    public void setCreatedAt(java.lang.String createdAt) {
        this.createdAt = createdAt;
    }


    /**
     * Gets the customerDob value for this SalesFlatOrder.
     * 
     * @return customerDob
     */
    public java.lang.String getCustomerDob() {
        return customerDob;
    }


    /**
     * Sets the customerDob value for this SalesFlatOrder.
     * 
     * @param customerDob
     */
    public void setCustomerDob(java.lang.String customerDob) {
        this.customerDob = customerDob;
    }


    /**
     * Gets the customerEmail value for this SalesFlatOrder.
     * 
     * @return customerEmail
     */
    public java.lang.String getCustomerEmail() {
        return customerEmail;
    }


    /**
     * Sets the customerEmail value for this SalesFlatOrder.
     * 
     * @param customerEmail
     */
    public void setCustomerEmail(java.lang.String customerEmail) {
        this.customerEmail = customerEmail;
    }


    /**
     * Gets the customerFirstname value for this SalesFlatOrder.
     * 
     * @return customerFirstname
     */
    public java.lang.String getCustomerFirstname() {
        return customerFirstname;
    }


    /**
     * Sets the customerFirstname value for this SalesFlatOrder.
     * 
     * @param customerFirstname
     */
    public void setCustomerFirstname(java.lang.String customerFirstname) {
        this.customerFirstname = customerFirstname;
    }


    /**
     * Gets the customerGender value for this SalesFlatOrder.
     * 
     * @return customerGender
     */
    public java.lang.Integer getCustomerGender() {
        return customerGender;
    }


    /**
     * Sets the customerGender value for this SalesFlatOrder.
     * 
     * @param customerGender
     */
    public void setCustomerGender(java.lang.Integer customerGender) {
        this.customerGender = customerGender;
    }


    /**
     * Gets the customerGroupId value for this SalesFlatOrder.
     * 
     * @return customerGroupId
     */
    public java.lang.Short getCustomerGroupId() {
        return customerGroupId;
    }


    /**
     * Sets the customerGroupId value for this SalesFlatOrder.
     * 
     * @param customerGroupId
     */
    public void setCustomerGroupId(java.lang.Short customerGroupId) {
        this.customerGroupId = customerGroupId;
    }


    /**
     * Gets the customerId value for this SalesFlatOrder.
     * 
     * @return customerId
     */
    public java.lang.Integer getCustomerId() {
        return customerId;
    }


    /**
     * Sets the customerId value for this SalesFlatOrder.
     * 
     * @param customerId
     */
    public void setCustomerId(java.lang.Integer customerId) {
        this.customerId = customerId;
    }


    /**
     * Gets the customerIsGuest value for this SalesFlatOrder.
     * 
     * @return customerIsGuest
     */
    public java.lang.Short getCustomerIsGuest() {
        return customerIsGuest;
    }


    /**
     * Sets the customerIsGuest value for this SalesFlatOrder.
     * 
     * @param customerIsGuest
     */
    public void setCustomerIsGuest(java.lang.Short customerIsGuest) {
        this.customerIsGuest = customerIsGuest;
    }


    /**
     * Gets the customerLastname value for this SalesFlatOrder.
     * 
     * @return customerLastname
     */
    public java.lang.String getCustomerLastname() {
        return customerLastname;
    }


    /**
     * Sets the customerLastname value for this SalesFlatOrder.
     * 
     * @param customerLastname
     */
    public void setCustomerLastname(java.lang.String customerLastname) {
        this.customerLastname = customerLastname;
    }


    /**
     * Gets the customerMiddlename value for this SalesFlatOrder.
     * 
     * @return customerMiddlename
     */
    public java.lang.String getCustomerMiddlename() {
        return customerMiddlename;
    }


    /**
     * Sets the customerMiddlename value for this SalesFlatOrder.
     * 
     * @param customerMiddlename
     */
    public void setCustomerMiddlename(java.lang.String customerMiddlename) {
        this.customerMiddlename = customerMiddlename;
    }


    /**
     * Gets the customerNote value for this SalesFlatOrder.
     * 
     * @return customerNote
     */
    public java.lang.String getCustomerNote() {
        return customerNote;
    }


    /**
     * Sets the customerNote value for this SalesFlatOrder.
     * 
     * @param customerNote
     */
    public void setCustomerNote(java.lang.String customerNote) {
        this.customerNote = customerNote;
    }


    /**
     * Gets the customerNoteNotify value for this SalesFlatOrder.
     * 
     * @return customerNoteNotify
     */
    public java.lang.Short getCustomerNoteNotify() {
        return customerNoteNotify;
    }


    /**
     * Sets the customerNoteNotify value for this SalesFlatOrder.
     * 
     * @param customerNoteNotify
     */
    public void setCustomerNoteNotify(java.lang.Short customerNoteNotify) {
        this.customerNoteNotify = customerNoteNotify;
    }


    /**
     * Gets the customerPrefix value for this SalesFlatOrder.
     * 
     * @return customerPrefix
     */
    public java.lang.String getCustomerPrefix() {
        return customerPrefix;
    }


    /**
     * Sets the customerPrefix value for this SalesFlatOrder.
     * 
     * @param customerPrefix
     */
    public void setCustomerPrefix(java.lang.String customerPrefix) {
        this.customerPrefix = customerPrefix;
    }


    /**
     * Gets the customerSuffix value for this SalesFlatOrder.
     * 
     * @return customerSuffix
     */
    public java.lang.String getCustomerSuffix() {
        return customerSuffix;
    }


    /**
     * Sets the customerSuffix value for this SalesFlatOrder.
     * 
     * @param customerSuffix
     */
    public void setCustomerSuffix(java.lang.String customerSuffix) {
        this.customerSuffix = customerSuffix;
    }


    /**
     * Gets the customerTaxvat value for this SalesFlatOrder.
     * 
     * @return customerTaxvat
     */
    public java.lang.String getCustomerTaxvat() {
        return customerTaxvat;
    }


    /**
     * Sets the customerTaxvat value for this SalesFlatOrder.
     * 
     * @param customerTaxvat
     */
    public void setCustomerTaxvat(java.lang.String customerTaxvat) {
        this.customerTaxvat = customerTaxvat;
    }


    /**
     * Gets the discountAmount value for this SalesFlatOrder.
     * 
     * @return discountAmount
     */
    public java.math.BigDecimal getDiscountAmount() {
        return discountAmount;
    }


    /**
     * Sets the discountAmount value for this SalesFlatOrder.
     * 
     * @param discountAmount
     */
    public void setDiscountAmount(java.math.BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }


    /**
     * Gets the discountCanceled value for this SalesFlatOrder.
     * 
     * @return discountCanceled
     */
    public java.math.BigDecimal getDiscountCanceled() {
        return discountCanceled;
    }


    /**
     * Sets the discountCanceled value for this SalesFlatOrder.
     * 
     * @param discountCanceled
     */
    public void setDiscountCanceled(java.math.BigDecimal discountCanceled) {
        this.discountCanceled = discountCanceled;
    }


    /**
     * Gets the discountDescription value for this SalesFlatOrder.
     * 
     * @return discountDescription
     */
    public java.lang.String getDiscountDescription() {
        return discountDescription;
    }


    /**
     * Sets the discountDescription value for this SalesFlatOrder.
     * 
     * @param discountDescription
     */
    public void setDiscountDescription(java.lang.String discountDescription) {
        this.discountDescription = discountDescription;
    }


    /**
     * Gets the discountInvoiced value for this SalesFlatOrder.
     * 
     * @return discountInvoiced
     */
    public java.math.BigDecimal getDiscountInvoiced() {
        return discountInvoiced;
    }


    /**
     * Sets the discountInvoiced value for this SalesFlatOrder.
     * 
     * @param discountInvoiced
     */
    public void setDiscountInvoiced(java.math.BigDecimal discountInvoiced) {
        this.discountInvoiced = discountInvoiced;
    }


    /**
     * Gets the discountRefunded value for this SalesFlatOrder.
     * 
     * @return discountRefunded
     */
    public java.math.BigDecimal getDiscountRefunded() {
        return discountRefunded;
    }


    /**
     * Sets the discountRefunded value for this SalesFlatOrder.
     * 
     * @param discountRefunded
     */
    public void setDiscountRefunded(java.math.BigDecimal discountRefunded) {
        this.discountRefunded = discountRefunded;
    }


    /**
     * Gets the editIncrement value for this SalesFlatOrder.
     * 
     * @return editIncrement
     */
    public java.lang.Integer getEditIncrement() {
        return editIncrement;
    }


    /**
     * Sets the editIncrement value for this SalesFlatOrder.
     * 
     * @param editIncrement
     */
    public void setEditIncrement(java.lang.Integer editIncrement) {
        this.editIncrement = editIncrement;
    }


    /**
     * Gets the emailSent value for this SalesFlatOrder.
     * 
     * @return emailSent
     */
    public java.lang.Short getEmailSent() {
        return emailSent;
    }


    /**
     * Sets the emailSent value for this SalesFlatOrder.
     * 
     * @param emailSent
     */
    public void setEmailSent(java.lang.Short emailSent) {
        this.emailSent = emailSent;
    }


    /**
     * Gets the entityId value for this SalesFlatOrder.
     * 
     * @return entityId
     */
    public java.lang.Integer getEntityId() {
        return entityId;
    }


    /**
     * Sets the entityId value for this SalesFlatOrder.
     * 
     * @param entityId
     */
    public void setEntityId(java.lang.Integer entityId) {
        this.entityId = entityId;
    }


    /**
     * Gets the extCustomerId value for this SalesFlatOrder.
     * 
     * @return extCustomerId
     */
    public java.lang.String getExtCustomerId() {
        return extCustomerId;
    }


    /**
     * Sets the extCustomerId value for this SalesFlatOrder.
     * 
     * @param extCustomerId
     */
    public void setExtCustomerId(java.lang.String extCustomerId) {
        this.extCustomerId = extCustomerId;
    }


    /**
     * Gets the extOrderId value for this SalesFlatOrder.
     * 
     * @return extOrderId
     */
    public java.lang.String getExtOrderId() {
        return extOrderId;
    }


    /**
     * Sets the extOrderId value for this SalesFlatOrder.
     * 
     * @param extOrderId
     */
    public void setExtOrderId(java.lang.String extOrderId) {
        this.extOrderId = extOrderId;
    }


    /**
     * Gets the forcedShipmentWithInvoice value for this SalesFlatOrder.
     * 
     * @return forcedShipmentWithInvoice
     */
    public java.lang.Short getForcedShipmentWithInvoice() {
        return forcedShipmentWithInvoice;
    }


    /**
     * Sets the forcedShipmentWithInvoice value for this SalesFlatOrder.
     * 
     * @param forcedShipmentWithInvoice
     */
    public void setForcedShipmentWithInvoice(java.lang.Short forcedShipmentWithInvoice) {
        this.forcedShipmentWithInvoice = forcedShipmentWithInvoice;
    }


    /**
     * Gets the giftMessageId value for this SalesFlatOrder.
     * 
     * @return giftMessageId
     */
    public java.lang.Integer getGiftMessageId() {
        return giftMessageId;
    }


    /**
     * Sets the giftMessageId value for this SalesFlatOrder.
     * 
     * @param giftMessageId
     */
    public void setGiftMessageId(java.lang.Integer giftMessageId) {
        this.giftMessageId = giftMessageId;
    }


    /**
     * Gets the globalCurrencyCode value for this SalesFlatOrder.
     * 
     * @return globalCurrencyCode
     */
    public java.lang.String getGlobalCurrencyCode() {
        return globalCurrencyCode;
    }


    /**
     * Sets the globalCurrencyCode value for this SalesFlatOrder.
     * 
     * @param globalCurrencyCode
     */
    public void setGlobalCurrencyCode(java.lang.String globalCurrencyCode) {
        this.globalCurrencyCode = globalCurrencyCode;
    }


    /**
     * Gets the grandTotal value for this SalesFlatOrder.
     * 
     * @return grandTotal
     */
    public java.math.BigDecimal getGrandTotal() {
        return grandTotal;
    }


    /**
     * Sets the grandTotal value for this SalesFlatOrder.
     * 
     * @param grandTotal
     */
    public void setGrandTotal(java.math.BigDecimal grandTotal) {
        this.grandTotal = grandTotal;
    }


    /**
     * Gets the hiddenTaxAmount value for this SalesFlatOrder.
     * 
     * @return hiddenTaxAmount
     */
    public java.math.BigDecimal getHiddenTaxAmount() {
        return hiddenTaxAmount;
    }


    /**
     * Sets the hiddenTaxAmount value for this SalesFlatOrder.
     * 
     * @param hiddenTaxAmount
     */
    public void setHiddenTaxAmount(java.math.BigDecimal hiddenTaxAmount) {
        this.hiddenTaxAmount = hiddenTaxAmount;
    }


    /**
     * Gets the hiddenTaxInvoiced value for this SalesFlatOrder.
     * 
     * @return hiddenTaxInvoiced
     */
    public java.math.BigDecimal getHiddenTaxInvoiced() {
        return hiddenTaxInvoiced;
    }


    /**
     * Sets the hiddenTaxInvoiced value for this SalesFlatOrder.
     * 
     * @param hiddenTaxInvoiced
     */
    public void setHiddenTaxInvoiced(java.math.BigDecimal hiddenTaxInvoiced) {
        this.hiddenTaxInvoiced = hiddenTaxInvoiced;
    }


    /**
     * Gets the hiddenTaxRefunded value for this SalesFlatOrder.
     * 
     * @return hiddenTaxRefunded
     */
    public java.math.BigDecimal getHiddenTaxRefunded() {
        return hiddenTaxRefunded;
    }


    /**
     * Sets the hiddenTaxRefunded value for this SalesFlatOrder.
     * 
     * @param hiddenTaxRefunded
     */
    public void setHiddenTaxRefunded(java.math.BigDecimal hiddenTaxRefunded) {
        this.hiddenTaxRefunded = hiddenTaxRefunded;
    }


    /**
     * Gets the holdBeforeState value for this SalesFlatOrder.
     * 
     * @return holdBeforeState
     */
    public java.lang.String getHoldBeforeState() {
        return holdBeforeState;
    }


    /**
     * Sets the holdBeforeState value for this SalesFlatOrder.
     * 
     * @param holdBeforeState
     */
    public void setHoldBeforeState(java.lang.String holdBeforeState) {
        this.holdBeforeState = holdBeforeState;
    }


    /**
     * Gets the holdBeforeStatus value for this SalesFlatOrder.
     * 
     * @return holdBeforeStatus
     */
    public java.lang.String getHoldBeforeStatus() {
        return holdBeforeStatus;
    }


    /**
     * Sets the holdBeforeStatus value for this SalesFlatOrder.
     * 
     * @param holdBeforeStatus
     */
    public void setHoldBeforeStatus(java.lang.String holdBeforeStatus) {
        this.holdBeforeStatus = holdBeforeStatus;
    }


    /**
     * Gets the incrementId value for this SalesFlatOrder.
     * 
     * @return incrementId
     */
    public java.lang.String getIncrementId() {
        return incrementId;
    }


    /**
     * Sets the incrementId value for this SalesFlatOrder.
     * 
     * @param incrementId
     */
    public void setIncrementId(java.lang.String incrementId) {
        this.incrementId = incrementId;
    }


    /**
     * Gets the isVirtual value for this SalesFlatOrder.
     * 
     * @return isVirtual
     */
    public java.lang.Short getIsVirtual() {
        return isVirtual;
    }


    /**
     * Sets the isVirtual value for this SalesFlatOrder.
     * 
     * @param isVirtual
     */
    public void setIsVirtual(java.lang.Short isVirtual) {
        this.isVirtual = isVirtual;
    }


    /**
     * Gets the orderCurrencyCode value for this SalesFlatOrder.
     * 
     * @return orderCurrencyCode
     */
    public java.lang.String getOrderCurrencyCode() {
        return orderCurrencyCode;
    }


    /**
     * Sets the orderCurrencyCode value for this SalesFlatOrder.
     * 
     * @param orderCurrencyCode
     */
    public void setOrderCurrencyCode(java.lang.String orderCurrencyCode) {
        this.orderCurrencyCode = orderCurrencyCode;
    }


    /**
     * Gets the originalIncrementId value for this SalesFlatOrder.
     * 
     * @return originalIncrementId
     */
    public java.lang.String getOriginalIncrementId() {
        return originalIncrementId;
    }


    /**
     * Sets the originalIncrementId value for this SalesFlatOrder.
     * 
     * @param originalIncrementId
     */
    public void setOriginalIncrementId(java.lang.String originalIncrementId) {
        this.originalIncrementId = originalIncrementId;
    }


    /**
     * Gets the paymentAuthExpiration value for this SalesFlatOrder.
     * 
     * @return paymentAuthExpiration
     */
    public java.lang.Integer getPaymentAuthExpiration() {
        return paymentAuthExpiration;
    }


    /**
     * Sets the paymentAuthExpiration value for this SalesFlatOrder.
     * 
     * @param paymentAuthExpiration
     */
    public void setPaymentAuthExpiration(java.lang.Integer paymentAuthExpiration) {
        this.paymentAuthExpiration = paymentAuthExpiration;
    }


    /**
     * Gets the paymentAuthorizationAmount value for this SalesFlatOrder.
     * 
     * @return paymentAuthorizationAmount
     */
    public java.math.BigDecimal getPaymentAuthorizationAmount() {
        return paymentAuthorizationAmount;
    }


    /**
     * Sets the paymentAuthorizationAmount value for this SalesFlatOrder.
     * 
     * @param paymentAuthorizationAmount
     */
    public void setPaymentAuthorizationAmount(java.math.BigDecimal paymentAuthorizationAmount) {
        this.paymentAuthorizationAmount = paymentAuthorizationAmount;
    }


    /**
     * Gets the paypalIpnCustomerNotified value for this SalesFlatOrder.
     * 
     * @return paypalIpnCustomerNotified
     */
    public java.lang.Integer getPaypalIpnCustomerNotified() {
        return paypalIpnCustomerNotified;
    }


    /**
     * Sets the paypalIpnCustomerNotified value for this SalesFlatOrder.
     * 
     * @param paypalIpnCustomerNotified
     */
    public void setPaypalIpnCustomerNotified(java.lang.Integer paypalIpnCustomerNotified) {
        this.paypalIpnCustomerNotified = paypalIpnCustomerNotified;
    }


    /**
     * Gets the pendingTlog value for this SalesFlatOrder.
     * 
     * @return pendingTlog
     */
    public java.lang.Integer getPendingTlog() {
        return pendingTlog;
    }


    /**
     * Sets the pendingTlog value for this SalesFlatOrder.
     * 
     * @param pendingTlog
     */
    public void setPendingTlog(java.lang.Integer pendingTlog) {
        this.pendingTlog = pendingTlog;
    }


    /**
     * Gets the protectCode value for this SalesFlatOrder.
     * 
     * @return protectCode
     */
    public java.lang.String getProtectCode() {
        return protectCode;
    }


    /**
     * Sets the protectCode value for this SalesFlatOrder.
     * 
     * @param protectCode
     */
    public void setProtectCode(java.lang.String protectCode) {
        this.protectCode = protectCode;
    }


    /**
     * Gets the quoteAddressId value for this SalesFlatOrder.
     * 
     * @return quoteAddressId
     */
    public java.lang.Integer getQuoteAddressId() {
        return quoteAddressId;
    }


    /**
     * Sets the quoteAddressId value for this SalesFlatOrder.
     * 
     * @param quoteAddressId
     */
    public void setQuoteAddressId(java.lang.Integer quoteAddressId) {
        this.quoteAddressId = quoteAddressId;
    }


    /**
     * Gets the quoteId value for this SalesFlatOrder.
     * 
     * @return quoteId
     */
    public java.lang.Integer getQuoteId() {
        return quoteId;
    }


    /**
     * Sets the quoteId value for this SalesFlatOrder.
     * 
     * @param quoteId
     */
    public void setQuoteId(java.lang.Integer quoteId) {
        this.quoteId = quoteId;
    }


    /**
     * Gets the relationChildId value for this SalesFlatOrder.
     * 
     * @return relationChildId
     */
    public java.lang.String getRelationChildId() {
        return relationChildId;
    }


    /**
     * Sets the relationChildId value for this SalesFlatOrder.
     * 
     * @param relationChildId
     */
    public void setRelationChildId(java.lang.String relationChildId) {
        this.relationChildId = relationChildId;
    }


    /**
     * Gets the relationChildRealId value for this SalesFlatOrder.
     * 
     * @return relationChildRealId
     */
    public java.lang.String getRelationChildRealId() {
        return relationChildRealId;
    }


    /**
     * Sets the relationChildRealId value for this SalesFlatOrder.
     * 
     * @param relationChildRealId
     */
    public void setRelationChildRealId(java.lang.String relationChildRealId) {
        this.relationChildRealId = relationChildRealId;
    }


    /**
     * Gets the relationParentId value for this SalesFlatOrder.
     * 
     * @return relationParentId
     */
    public java.lang.String getRelationParentId() {
        return relationParentId;
    }


    /**
     * Sets the relationParentId value for this SalesFlatOrder.
     * 
     * @param relationParentId
     */
    public void setRelationParentId(java.lang.String relationParentId) {
        this.relationParentId = relationParentId;
    }


    /**
     * Gets the relationParentRealId value for this SalesFlatOrder.
     * 
     * @return relationParentRealId
     */
    public java.lang.String getRelationParentRealId() {
        return relationParentRealId;
    }


    /**
     * Sets the relationParentRealId value for this SalesFlatOrder.
     * 
     * @param relationParentRealId
     */
    public void setRelationParentRealId(java.lang.String relationParentRealId) {
        this.relationParentRealId = relationParentRealId;
    }


    /**
     * Gets the remoteIp value for this SalesFlatOrder.
     * 
     * @return remoteIp
     */
    public java.lang.String getRemoteIp() {
        return remoteIp;
    }


    /**
     * Sets the remoteIp value for this SalesFlatOrder.
     * 
     * @param remoteIp
     */
    public void setRemoteIp(java.lang.String remoteIp) {
        this.remoteIp = remoteIp;
    }


    /**
     * Gets the shippingAddressId value for this SalesFlatOrder.
     * 
     * @return shippingAddressId
     */
    public java.lang.Integer getShippingAddressId() {
        return shippingAddressId;
    }


    /**
     * Sets the shippingAddressId value for this SalesFlatOrder.
     * 
     * @param shippingAddressId
     */
    public void setShippingAddressId(java.lang.Integer shippingAddressId) {
        this.shippingAddressId = shippingAddressId;
    }


    /**
     * Gets the shippingAmount value for this SalesFlatOrder.
     * 
     * @return shippingAmount
     */
    public java.math.BigDecimal getShippingAmount() {
        return shippingAmount;
    }


    /**
     * Sets the shippingAmount value for this SalesFlatOrder.
     * 
     * @param shippingAmount
     */
    public void setShippingAmount(java.math.BigDecimal shippingAmount) {
        this.shippingAmount = shippingAmount;
    }


    /**
     * Gets the shippingCanceled value for this SalesFlatOrder.
     * 
     * @return shippingCanceled
     */
    public java.math.BigDecimal getShippingCanceled() {
        return shippingCanceled;
    }


    /**
     * Sets the shippingCanceled value for this SalesFlatOrder.
     * 
     * @param shippingCanceled
     */
    public void setShippingCanceled(java.math.BigDecimal shippingCanceled) {
        this.shippingCanceled = shippingCanceled;
    }


    /**
     * Gets the shippingDescription value for this SalesFlatOrder.
     * 
     * @return shippingDescription
     */
    public java.lang.String getShippingDescription() {
        return shippingDescription;
    }


    /**
     * Sets the shippingDescription value for this SalesFlatOrder.
     * 
     * @param shippingDescription
     */
    public void setShippingDescription(java.lang.String shippingDescription) {
        this.shippingDescription = shippingDescription;
    }


    /**
     * Gets the shippingDiscountAmount value for this SalesFlatOrder.
     * 
     * @return shippingDiscountAmount
     */
    public java.math.BigDecimal getShippingDiscountAmount() {
        return shippingDiscountAmount;
    }


    /**
     * Sets the shippingDiscountAmount value for this SalesFlatOrder.
     * 
     * @param shippingDiscountAmount
     */
    public void setShippingDiscountAmount(java.math.BigDecimal shippingDiscountAmount) {
        this.shippingDiscountAmount = shippingDiscountAmount;
    }


    /**
     * Gets the shippingHiddenTaxAmount value for this SalesFlatOrder.
     * 
     * @return shippingHiddenTaxAmount
     */
    public java.math.BigDecimal getShippingHiddenTaxAmount() {
        return shippingHiddenTaxAmount;
    }


    /**
     * Sets the shippingHiddenTaxAmount value for this SalesFlatOrder.
     * 
     * @param shippingHiddenTaxAmount
     */
    public void setShippingHiddenTaxAmount(java.math.BigDecimal shippingHiddenTaxAmount) {
        this.shippingHiddenTaxAmount = shippingHiddenTaxAmount;
    }


    /**
     * Gets the shippingInclTax value for this SalesFlatOrder.
     * 
     * @return shippingInclTax
     */
    public java.math.BigDecimal getShippingInclTax() {
        return shippingInclTax;
    }


    /**
     * Sets the shippingInclTax value for this SalesFlatOrder.
     * 
     * @param shippingInclTax
     */
    public void setShippingInclTax(java.math.BigDecimal shippingInclTax) {
        this.shippingInclTax = shippingInclTax;
    }


    /**
     * Gets the shippingInvoiced value for this SalesFlatOrder.
     * 
     * @return shippingInvoiced
     */
    public java.math.BigDecimal getShippingInvoiced() {
        return shippingInvoiced;
    }


    /**
     * Sets the shippingInvoiced value for this SalesFlatOrder.
     * 
     * @param shippingInvoiced
     */
    public void setShippingInvoiced(java.math.BigDecimal shippingInvoiced) {
        this.shippingInvoiced = shippingInvoiced;
    }


    /**
     * Gets the shippingMethod value for this SalesFlatOrder.
     * 
     * @return shippingMethod
     */
    public java.lang.String getShippingMethod() {
        return shippingMethod;
    }


    /**
     * Sets the shippingMethod value for this SalesFlatOrder.
     * 
     * @param shippingMethod
     */
    public void setShippingMethod(java.lang.String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }


    /**
     * Gets the shippingRefunded value for this SalesFlatOrder.
     * 
     * @return shippingRefunded
     */
    public java.math.BigDecimal getShippingRefunded() {
        return shippingRefunded;
    }


    /**
     * Sets the shippingRefunded value for this SalesFlatOrder.
     * 
     * @param shippingRefunded
     */
    public void setShippingRefunded(java.math.BigDecimal shippingRefunded) {
        this.shippingRefunded = shippingRefunded;
    }


    /**
     * Gets the shippingTaxAmount value for this SalesFlatOrder.
     * 
     * @return shippingTaxAmount
     */
    public java.math.BigDecimal getShippingTaxAmount() {
        return shippingTaxAmount;
    }


    /**
     * Sets the shippingTaxAmount value for this SalesFlatOrder.
     * 
     * @param shippingTaxAmount
     */
    public void setShippingTaxAmount(java.math.BigDecimal shippingTaxAmount) {
        this.shippingTaxAmount = shippingTaxAmount;
    }


    /**
     * Gets the shippingTaxRefunded value for this SalesFlatOrder.
     * 
     * @return shippingTaxRefunded
     */
    public java.math.BigDecimal getShippingTaxRefunded() {
        return shippingTaxRefunded;
    }


    /**
     * Sets the shippingTaxRefunded value for this SalesFlatOrder.
     * 
     * @param shippingTaxRefunded
     */
    public void setShippingTaxRefunded(java.math.BigDecimal shippingTaxRefunded) {
        this.shippingTaxRefunded = shippingTaxRefunded;
    }


    /**
     * Gets the state value for this SalesFlatOrder.
     * 
     * @return state
     */
    public java.lang.String getState() {
        return state;
    }


    /**
     * Sets the state value for this SalesFlatOrder.
     * 
     * @param state
     */
    public void setState(java.lang.String state) {
        this.state = state;
    }


    /**
     * Gets the status value for this SalesFlatOrder.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this SalesFlatOrder.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the storeCurrencyCode value for this SalesFlatOrder.
     * 
     * @return storeCurrencyCode
     */
    public java.lang.String getStoreCurrencyCode() {
        return storeCurrencyCode;
    }


    /**
     * Sets the storeCurrencyCode value for this SalesFlatOrder.
     * 
     * @param storeCurrencyCode
     */
    public void setStoreCurrencyCode(java.lang.String storeCurrencyCode) {
        this.storeCurrencyCode = storeCurrencyCode;
    }


    /**
     * Gets the storeId value for this SalesFlatOrder.
     * 
     * @return storeId
     */
    public java.lang.Short getStoreId() {
        return storeId;
    }


    /**
     * Sets the storeId value for this SalesFlatOrder.
     * 
     * @param storeId
     */
    public void setStoreId(java.lang.Short storeId) {
        this.storeId = storeId;
    }


    /**
     * Gets the storeName value for this SalesFlatOrder.
     * 
     * @return storeName
     */
    public java.lang.String getStoreName() {
        return storeName;
    }


    /**
     * Sets the storeName value for this SalesFlatOrder.
     * 
     * @param storeName
     */
    public void setStoreName(java.lang.String storeName) {
        this.storeName = storeName;
    }


    /**
     * Gets the storeToBaseRate value for this SalesFlatOrder.
     * 
     * @return storeToBaseRate
     */
    public java.math.BigDecimal getStoreToBaseRate() {
        return storeToBaseRate;
    }


    /**
     * Sets the storeToBaseRate value for this SalesFlatOrder.
     * 
     * @param storeToBaseRate
     */
    public void setStoreToBaseRate(java.math.BigDecimal storeToBaseRate) {
        this.storeToBaseRate = storeToBaseRate;
    }


    /**
     * Gets the storeToOrderRate value for this SalesFlatOrder.
     * 
     * @return storeToOrderRate
     */
    public java.math.BigDecimal getStoreToOrderRate() {
        return storeToOrderRate;
    }


    /**
     * Sets the storeToOrderRate value for this SalesFlatOrder.
     * 
     * @param storeToOrderRate
     */
    public void setStoreToOrderRate(java.math.BigDecimal storeToOrderRate) {
        this.storeToOrderRate = storeToOrderRate;
    }


    /**
     * Gets the subtotal value for this SalesFlatOrder.
     * 
     * @return subtotal
     */
    public java.math.BigDecimal getSubtotal() {
        return subtotal;
    }


    /**
     * Sets the subtotal value for this SalesFlatOrder.
     * 
     * @param subtotal
     */
    public void setSubtotal(java.math.BigDecimal subtotal) {
        this.subtotal = subtotal;
    }


    /**
     * Gets the subtotalCanceled value for this SalesFlatOrder.
     * 
     * @return subtotalCanceled
     */
    public java.math.BigDecimal getSubtotalCanceled() {
        return subtotalCanceled;
    }


    /**
     * Sets the subtotalCanceled value for this SalesFlatOrder.
     * 
     * @param subtotalCanceled
     */
    public void setSubtotalCanceled(java.math.BigDecimal subtotalCanceled) {
        this.subtotalCanceled = subtotalCanceled;
    }


    /**
     * Gets the subtotalInclTax value for this SalesFlatOrder.
     * 
     * @return subtotalInclTax
     */
    public java.math.BigDecimal getSubtotalInclTax() {
        return subtotalInclTax;
    }


    /**
     * Sets the subtotalInclTax value for this SalesFlatOrder.
     * 
     * @param subtotalInclTax
     */
    public void setSubtotalInclTax(java.math.BigDecimal subtotalInclTax) {
        this.subtotalInclTax = subtotalInclTax;
    }


    /**
     * Gets the subtotalInvoiced value for this SalesFlatOrder.
     * 
     * @return subtotalInvoiced
     */
    public java.math.BigDecimal getSubtotalInvoiced() {
        return subtotalInvoiced;
    }


    /**
     * Sets the subtotalInvoiced value for this SalesFlatOrder.
     * 
     * @param subtotalInvoiced
     */
    public void setSubtotalInvoiced(java.math.BigDecimal subtotalInvoiced) {
        this.subtotalInvoiced = subtotalInvoiced;
    }


    /**
     * Gets the subtotalRefunded value for this SalesFlatOrder.
     * 
     * @return subtotalRefunded
     */
    public java.math.BigDecimal getSubtotalRefunded() {
        return subtotalRefunded;
    }


    /**
     * Sets the subtotalRefunded value for this SalesFlatOrder.
     * 
     * @param subtotalRefunded
     */
    public void setSubtotalRefunded(java.math.BigDecimal subtotalRefunded) {
        this.subtotalRefunded = subtotalRefunded;
    }


    /**
     * Gets the supplierId value for this SalesFlatOrder.
     * 
     * @return supplierId
     */
    public java.lang.Integer getSupplierId() {
        return supplierId;
    }


    /**
     * Sets the supplierId value for this SalesFlatOrder.
     * 
     * @param supplierId
     */
    public void setSupplierId(java.lang.Integer supplierId) {
        this.supplierId = supplierId;
    }


    /**
     * Gets the taxAmount value for this SalesFlatOrder.
     * 
     * @return taxAmount
     */
    public java.math.BigDecimal getTaxAmount() {
        return taxAmount;
    }


    /**
     * Sets the taxAmount value for this SalesFlatOrder.
     * 
     * @param taxAmount
     */
    public void setTaxAmount(java.math.BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }


    /**
     * Gets the taxCanceled value for this SalesFlatOrder.
     * 
     * @return taxCanceled
     */
    public java.math.BigDecimal getTaxCanceled() {
        return taxCanceled;
    }


    /**
     * Sets the taxCanceled value for this SalesFlatOrder.
     * 
     * @param taxCanceled
     */
    public void setTaxCanceled(java.math.BigDecimal taxCanceled) {
        this.taxCanceled = taxCanceled;
    }


    /**
     * Gets the taxInvoiced value for this SalesFlatOrder.
     * 
     * @return taxInvoiced
     */
    public java.math.BigDecimal getTaxInvoiced() {
        return taxInvoiced;
    }


    /**
     * Sets the taxInvoiced value for this SalesFlatOrder.
     * 
     * @param taxInvoiced
     */
    public void setTaxInvoiced(java.math.BigDecimal taxInvoiced) {
        this.taxInvoiced = taxInvoiced;
    }


    /**
     * Gets the taxRefunded value for this SalesFlatOrder.
     * 
     * @return taxRefunded
     */
    public java.math.BigDecimal getTaxRefunded() {
        return taxRefunded;
    }


    /**
     * Sets the taxRefunded value for this SalesFlatOrder.
     * 
     * @param taxRefunded
     */
    public void setTaxRefunded(java.math.BigDecimal taxRefunded) {
        this.taxRefunded = taxRefunded;
    }


    /**
     * Gets the totalCanceled value for this SalesFlatOrder.
     * 
     * @return totalCanceled
     */
    public java.math.BigDecimal getTotalCanceled() {
        return totalCanceled;
    }


    /**
     * Sets the totalCanceled value for this SalesFlatOrder.
     * 
     * @param totalCanceled
     */
    public void setTotalCanceled(java.math.BigDecimal totalCanceled) {
        this.totalCanceled = totalCanceled;
    }


    /**
     * Gets the totalDue value for this SalesFlatOrder.
     * 
     * @return totalDue
     */
    public java.math.BigDecimal getTotalDue() {
        return totalDue;
    }


    /**
     * Sets the totalDue value for this SalesFlatOrder.
     * 
     * @param totalDue
     */
    public void setTotalDue(java.math.BigDecimal totalDue) {
        this.totalDue = totalDue;
    }


    /**
     * Gets the totalInvoiced value for this SalesFlatOrder.
     * 
     * @return totalInvoiced
     */
    public java.math.BigDecimal getTotalInvoiced() {
        return totalInvoiced;
    }


    /**
     * Sets the totalInvoiced value for this SalesFlatOrder.
     * 
     * @param totalInvoiced
     */
    public void setTotalInvoiced(java.math.BigDecimal totalInvoiced) {
        this.totalInvoiced = totalInvoiced;
    }


    /**
     * Gets the totalItemCount value for this SalesFlatOrder.
     * 
     * @return totalItemCount
     */
    public short getTotalItemCount() {
        return totalItemCount;
    }


    /**
     * Sets the totalItemCount value for this SalesFlatOrder.
     * 
     * @param totalItemCount
     */
    public void setTotalItemCount(short totalItemCount) {
        this.totalItemCount = totalItemCount;
    }


    /**
     * Gets the totalOfflineRefunded value for this SalesFlatOrder.
     * 
     * @return totalOfflineRefunded
     */
    public java.math.BigDecimal getTotalOfflineRefunded() {
        return totalOfflineRefunded;
    }


    /**
     * Sets the totalOfflineRefunded value for this SalesFlatOrder.
     * 
     * @param totalOfflineRefunded
     */
    public void setTotalOfflineRefunded(java.math.BigDecimal totalOfflineRefunded) {
        this.totalOfflineRefunded = totalOfflineRefunded;
    }


    /**
     * Gets the totalOnlineRefunded value for this SalesFlatOrder.
     * 
     * @return totalOnlineRefunded
     */
    public java.math.BigDecimal getTotalOnlineRefunded() {
        return totalOnlineRefunded;
    }


    /**
     * Sets the totalOnlineRefunded value for this SalesFlatOrder.
     * 
     * @param totalOnlineRefunded
     */
    public void setTotalOnlineRefunded(java.math.BigDecimal totalOnlineRefunded) {
        this.totalOnlineRefunded = totalOnlineRefunded;
    }


    /**
     * Gets the totalPaid value for this SalesFlatOrder.
     * 
     * @return totalPaid
     */
    public java.math.BigDecimal getTotalPaid() {
        return totalPaid;
    }


    /**
     * Sets the totalPaid value for this SalesFlatOrder.
     * 
     * @param totalPaid
     */
    public void setTotalPaid(java.math.BigDecimal totalPaid) {
        this.totalPaid = totalPaid;
    }


    /**
     * Gets the totalQtyOrdered value for this SalesFlatOrder.
     * 
     * @return totalQtyOrdered
     */
    public java.math.BigDecimal getTotalQtyOrdered() {
        return totalQtyOrdered;
    }


    /**
     * Sets the totalQtyOrdered value for this SalesFlatOrder.
     * 
     * @param totalQtyOrdered
     */
    public void setTotalQtyOrdered(java.math.BigDecimal totalQtyOrdered) {
        this.totalQtyOrdered = totalQtyOrdered;
    }


    /**
     * Gets the totalRefunded value for this SalesFlatOrder.
     * 
     * @return totalRefunded
     */
    public java.math.BigDecimal getTotalRefunded() {
        return totalRefunded;
    }


    /**
     * Sets the totalRefunded value for this SalesFlatOrder.
     * 
     * @param totalRefunded
     */
    public void setTotalRefunded(java.math.BigDecimal totalRefunded) {
        this.totalRefunded = totalRefunded;
    }


    /**
     * Gets the updatedAt value for this SalesFlatOrder.
     * 
     * @return updatedAt
     */
    public java.lang.String getUpdatedAt() {
        return updatedAt;
    }


    /**
     * Sets the updatedAt value for this SalesFlatOrder.
     * 
     * @param updatedAt
     */
    public void setUpdatedAt(java.lang.String updatedAt) {
        this.updatedAt = updatedAt;
    }


    /**
     * Gets the weight value for this SalesFlatOrder.
     * 
     * @return weight
     */
    public java.math.BigDecimal getWeight() {
        return weight;
    }


    /**
     * Sets the weight value for this SalesFlatOrder.
     * 
     * @param weight
     */
    public void setWeight(java.math.BigDecimal weight) {
        this.weight = weight;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SalesFlatOrder)) return false;
        SalesFlatOrder other = (SalesFlatOrder) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.XForwardedFor==null && other.getXForwardedFor()==null) || 
             (this.XForwardedFor!=null &&
              this.XForwardedFor.equals(other.getXForwardedFor()))) &&
            ((this.adjustmentNegative==null && other.getAdjustmentNegative()==null) || 
             (this.adjustmentNegative!=null &&
              this.adjustmentNegative.equals(other.getAdjustmentNegative()))) &&
            ((this.adjustmentPositive==null && other.getAdjustmentPositive()==null) || 
             (this.adjustmentPositive!=null &&
              this.adjustmentPositive.equals(other.getAdjustmentPositive()))) &&
            ((this.appliedRuleIds==null && other.getAppliedRuleIds()==null) || 
             (this.appliedRuleIds!=null &&
              this.appliedRuleIds.equals(other.getAppliedRuleIds()))) &&
            ((this.baseAdjustmentNegative==null && other.getBaseAdjustmentNegative()==null) || 
             (this.baseAdjustmentNegative!=null &&
              this.baseAdjustmentNegative.equals(other.getBaseAdjustmentNegative()))) &&
            ((this.baseAdjustmentPositive==null && other.getBaseAdjustmentPositive()==null) || 
             (this.baseAdjustmentPositive!=null &&
              this.baseAdjustmentPositive.equals(other.getBaseAdjustmentPositive()))) &&
            ((this.baseCurrencyCode==null && other.getBaseCurrencyCode()==null) || 
             (this.baseCurrencyCode!=null &&
              this.baseCurrencyCode.equals(other.getBaseCurrencyCode()))) &&
            ((this.baseDiscountAmount==null && other.getBaseDiscountAmount()==null) || 
             (this.baseDiscountAmount!=null &&
              this.baseDiscountAmount.equals(other.getBaseDiscountAmount()))) &&
            ((this.baseDiscountCanceled==null && other.getBaseDiscountCanceled()==null) || 
             (this.baseDiscountCanceled!=null &&
              this.baseDiscountCanceled.equals(other.getBaseDiscountCanceled()))) &&
            ((this.baseDiscountInvoiced==null && other.getBaseDiscountInvoiced()==null) || 
             (this.baseDiscountInvoiced!=null &&
              this.baseDiscountInvoiced.equals(other.getBaseDiscountInvoiced()))) &&
            ((this.baseDiscountRefunded==null && other.getBaseDiscountRefunded()==null) || 
             (this.baseDiscountRefunded!=null &&
              this.baseDiscountRefunded.equals(other.getBaseDiscountRefunded()))) &&
            ((this.baseGrandTotal==null && other.getBaseGrandTotal()==null) || 
             (this.baseGrandTotal!=null &&
              this.baseGrandTotal.equals(other.getBaseGrandTotal()))) &&
            ((this.baseHiddenTaxAmount==null && other.getBaseHiddenTaxAmount()==null) || 
             (this.baseHiddenTaxAmount!=null &&
              this.baseHiddenTaxAmount.equals(other.getBaseHiddenTaxAmount()))) &&
            ((this.baseHiddenTaxInvoiced==null && other.getBaseHiddenTaxInvoiced()==null) || 
             (this.baseHiddenTaxInvoiced!=null &&
              this.baseHiddenTaxInvoiced.equals(other.getBaseHiddenTaxInvoiced()))) &&
            ((this.baseHiddenTaxRefunded==null && other.getBaseHiddenTaxRefunded()==null) || 
             (this.baseHiddenTaxRefunded!=null &&
              this.baseHiddenTaxRefunded.equals(other.getBaseHiddenTaxRefunded()))) &&
            ((this.baseShippingAmount==null && other.getBaseShippingAmount()==null) || 
             (this.baseShippingAmount!=null &&
              this.baseShippingAmount.equals(other.getBaseShippingAmount()))) &&
            ((this.baseShippingCanceled==null && other.getBaseShippingCanceled()==null) || 
             (this.baseShippingCanceled!=null &&
              this.baseShippingCanceled.equals(other.getBaseShippingCanceled()))) &&
            ((this.baseShippingDiscountAmount==null && other.getBaseShippingDiscountAmount()==null) || 
             (this.baseShippingDiscountAmount!=null &&
              this.baseShippingDiscountAmount.equals(other.getBaseShippingDiscountAmount()))) &&
            ((this.baseShippingHiddenTaxAmnt==null && other.getBaseShippingHiddenTaxAmnt()==null) || 
             (this.baseShippingHiddenTaxAmnt!=null &&
              this.baseShippingHiddenTaxAmnt.equals(other.getBaseShippingHiddenTaxAmnt()))) &&
            ((this.baseShippingInclTax==null && other.getBaseShippingInclTax()==null) || 
             (this.baseShippingInclTax!=null &&
              this.baseShippingInclTax.equals(other.getBaseShippingInclTax()))) &&
            ((this.baseShippingInvoiced==null && other.getBaseShippingInvoiced()==null) || 
             (this.baseShippingInvoiced!=null &&
              this.baseShippingInvoiced.equals(other.getBaseShippingInvoiced()))) &&
            ((this.baseShippingRefunded==null && other.getBaseShippingRefunded()==null) || 
             (this.baseShippingRefunded!=null &&
              this.baseShippingRefunded.equals(other.getBaseShippingRefunded()))) &&
            ((this.baseShippingTaxAmount==null && other.getBaseShippingTaxAmount()==null) || 
             (this.baseShippingTaxAmount!=null &&
              this.baseShippingTaxAmount.equals(other.getBaseShippingTaxAmount()))) &&
            ((this.baseShippingTaxRefunded==null && other.getBaseShippingTaxRefunded()==null) || 
             (this.baseShippingTaxRefunded!=null &&
              this.baseShippingTaxRefunded.equals(other.getBaseShippingTaxRefunded()))) &&
            ((this.baseSubtotal==null && other.getBaseSubtotal()==null) || 
             (this.baseSubtotal!=null &&
              this.baseSubtotal.equals(other.getBaseSubtotal()))) &&
            ((this.baseSubtotalCanceled==null && other.getBaseSubtotalCanceled()==null) || 
             (this.baseSubtotalCanceled!=null &&
              this.baseSubtotalCanceled.equals(other.getBaseSubtotalCanceled()))) &&
            ((this.baseSubtotalInclTax==null && other.getBaseSubtotalInclTax()==null) || 
             (this.baseSubtotalInclTax!=null &&
              this.baseSubtotalInclTax.equals(other.getBaseSubtotalInclTax()))) &&
            ((this.baseSubtotalInvoiced==null && other.getBaseSubtotalInvoiced()==null) || 
             (this.baseSubtotalInvoiced!=null &&
              this.baseSubtotalInvoiced.equals(other.getBaseSubtotalInvoiced()))) &&
            ((this.baseSubtotalRefunded==null && other.getBaseSubtotalRefunded()==null) || 
             (this.baseSubtotalRefunded!=null &&
              this.baseSubtotalRefunded.equals(other.getBaseSubtotalRefunded()))) &&
            ((this.baseTaxAmount==null && other.getBaseTaxAmount()==null) || 
             (this.baseTaxAmount!=null &&
              this.baseTaxAmount.equals(other.getBaseTaxAmount()))) &&
            ((this.baseTaxCanceled==null && other.getBaseTaxCanceled()==null) || 
             (this.baseTaxCanceled!=null &&
              this.baseTaxCanceled.equals(other.getBaseTaxCanceled()))) &&
            ((this.baseTaxInvoiced==null && other.getBaseTaxInvoiced()==null) || 
             (this.baseTaxInvoiced!=null &&
              this.baseTaxInvoiced.equals(other.getBaseTaxInvoiced()))) &&
            ((this.baseTaxRefunded==null && other.getBaseTaxRefunded()==null) || 
             (this.baseTaxRefunded!=null &&
              this.baseTaxRefunded.equals(other.getBaseTaxRefunded()))) &&
            ((this.baseToGlobalRate==null && other.getBaseToGlobalRate()==null) || 
             (this.baseToGlobalRate!=null &&
              this.baseToGlobalRate.equals(other.getBaseToGlobalRate()))) &&
            ((this.baseToOrderRate==null && other.getBaseToOrderRate()==null) || 
             (this.baseToOrderRate!=null &&
              this.baseToOrderRate.equals(other.getBaseToOrderRate()))) &&
            ((this.baseTotalCanceled==null && other.getBaseTotalCanceled()==null) || 
             (this.baseTotalCanceled!=null &&
              this.baseTotalCanceled.equals(other.getBaseTotalCanceled()))) &&
            ((this.baseTotalDue==null && other.getBaseTotalDue()==null) || 
             (this.baseTotalDue!=null &&
              this.baseTotalDue.equals(other.getBaseTotalDue()))) &&
            ((this.baseTotalInvoiced==null && other.getBaseTotalInvoiced()==null) || 
             (this.baseTotalInvoiced!=null &&
              this.baseTotalInvoiced.equals(other.getBaseTotalInvoiced()))) &&
            ((this.baseTotalInvoicedCost==null && other.getBaseTotalInvoicedCost()==null) || 
             (this.baseTotalInvoicedCost!=null &&
              this.baseTotalInvoicedCost.equals(other.getBaseTotalInvoicedCost()))) &&
            ((this.baseTotalOfflineRefunded==null && other.getBaseTotalOfflineRefunded()==null) || 
             (this.baseTotalOfflineRefunded!=null &&
              this.baseTotalOfflineRefunded.equals(other.getBaseTotalOfflineRefunded()))) &&
            ((this.baseTotalOnlineRefunded==null && other.getBaseTotalOnlineRefunded()==null) || 
             (this.baseTotalOnlineRefunded!=null &&
              this.baseTotalOnlineRefunded.equals(other.getBaseTotalOnlineRefunded()))) &&
            ((this.baseTotalPaid==null && other.getBaseTotalPaid()==null) || 
             (this.baseTotalPaid!=null &&
              this.baseTotalPaid.equals(other.getBaseTotalPaid()))) &&
            ((this.baseTotalQtyOrdered==null && other.getBaseTotalQtyOrdered()==null) || 
             (this.baseTotalQtyOrdered!=null &&
              this.baseTotalQtyOrdered.equals(other.getBaseTotalQtyOrdered()))) &&
            ((this.baseTotalRefunded==null && other.getBaseTotalRefunded()==null) || 
             (this.baseTotalRefunded!=null &&
              this.baseTotalRefunded.equals(other.getBaseTotalRefunded()))) &&
            ((this.billingAddressId==null && other.getBillingAddressId()==null) || 
             (this.billingAddressId!=null &&
              this.billingAddressId.equals(other.getBillingAddressId()))) &&
            ((this.canShipPartially==null && other.getCanShipPartially()==null) || 
             (this.canShipPartially!=null &&
              this.canShipPartially.equals(other.getCanShipPartially()))) &&
            ((this.canShipPartiallyItem==null && other.getCanShipPartiallyItem()==null) || 
             (this.canShipPartiallyItem!=null &&
              this.canShipPartiallyItem.equals(other.getCanShipPartiallyItem()))) &&
            ((this.couponCode==null && other.getCouponCode()==null) || 
             (this.couponCode!=null &&
              this.couponCode.equals(other.getCouponCode()))) &&
            ((this.couponRuleName==null && other.getCouponRuleName()==null) || 
             (this.couponRuleName!=null &&
              this.couponRuleName.equals(other.getCouponRuleName()))) &&
            ((this.createdAt==null && other.getCreatedAt()==null) || 
             (this.createdAt!=null &&
              this.createdAt.equals(other.getCreatedAt()))) &&
            ((this.customerDob==null && other.getCustomerDob()==null) || 
             (this.customerDob!=null &&
              this.customerDob.equals(other.getCustomerDob()))) &&
            ((this.customerEmail==null && other.getCustomerEmail()==null) || 
             (this.customerEmail!=null &&
              this.customerEmail.equals(other.getCustomerEmail()))) &&
            ((this.customerFirstname==null && other.getCustomerFirstname()==null) || 
             (this.customerFirstname!=null &&
              this.customerFirstname.equals(other.getCustomerFirstname()))) &&
            ((this.customerGender==null && other.getCustomerGender()==null) || 
             (this.customerGender!=null &&
              this.customerGender.equals(other.getCustomerGender()))) &&
            ((this.customerGroupId==null && other.getCustomerGroupId()==null) || 
             (this.customerGroupId!=null &&
              this.customerGroupId.equals(other.getCustomerGroupId()))) &&
            ((this.customerId==null && other.getCustomerId()==null) || 
             (this.customerId!=null &&
              this.customerId.equals(other.getCustomerId()))) &&
            ((this.customerIsGuest==null && other.getCustomerIsGuest()==null) || 
             (this.customerIsGuest!=null &&
              this.customerIsGuest.equals(other.getCustomerIsGuest()))) &&
            ((this.customerLastname==null && other.getCustomerLastname()==null) || 
             (this.customerLastname!=null &&
              this.customerLastname.equals(other.getCustomerLastname()))) &&
            ((this.customerMiddlename==null && other.getCustomerMiddlename()==null) || 
             (this.customerMiddlename!=null &&
              this.customerMiddlename.equals(other.getCustomerMiddlename()))) &&
            ((this.customerNote==null && other.getCustomerNote()==null) || 
             (this.customerNote!=null &&
              this.customerNote.equals(other.getCustomerNote()))) &&
            ((this.customerNoteNotify==null && other.getCustomerNoteNotify()==null) || 
             (this.customerNoteNotify!=null &&
              this.customerNoteNotify.equals(other.getCustomerNoteNotify()))) &&
            ((this.customerPrefix==null && other.getCustomerPrefix()==null) || 
             (this.customerPrefix!=null &&
              this.customerPrefix.equals(other.getCustomerPrefix()))) &&
            ((this.customerSuffix==null && other.getCustomerSuffix()==null) || 
             (this.customerSuffix!=null &&
              this.customerSuffix.equals(other.getCustomerSuffix()))) &&
            ((this.customerTaxvat==null && other.getCustomerTaxvat()==null) || 
             (this.customerTaxvat!=null &&
              this.customerTaxvat.equals(other.getCustomerTaxvat()))) &&
            ((this.discountAmount==null && other.getDiscountAmount()==null) || 
             (this.discountAmount!=null &&
              this.discountAmount.equals(other.getDiscountAmount()))) &&
            ((this.discountCanceled==null && other.getDiscountCanceled()==null) || 
             (this.discountCanceled!=null &&
              this.discountCanceled.equals(other.getDiscountCanceled()))) &&
            ((this.discountDescription==null && other.getDiscountDescription()==null) || 
             (this.discountDescription!=null &&
              this.discountDescription.equals(other.getDiscountDescription()))) &&
            ((this.discountInvoiced==null && other.getDiscountInvoiced()==null) || 
             (this.discountInvoiced!=null &&
              this.discountInvoiced.equals(other.getDiscountInvoiced()))) &&
            ((this.discountRefunded==null && other.getDiscountRefunded()==null) || 
             (this.discountRefunded!=null &&
              this.discountRefunded.equals(other.getDiscountRefunded()))) &&
            ((this.editIncrement==null && other.getEditIncrement()==null) || 
             (this.editIncrement!=null &&
              this.editIncrement.equals(other.getEditIncrement()))) &&
            ((this.emailSent==null && other.getEmailSent()==null) || 
             (this.emailSent!=null &&
              this.emailSent.equals(other.getEmailSent()))) &&
            ((this.entityId==null && other.getEntityId()==null) || 
             (this.entityId!=null &&
              this.entityId.equals(other.getEntityId()))) &&
            ((this.extCustomerId==null && other.getExtCustomerId()==null) || 
             (this.extCustomerId!=null &&
              this.extCustomerId.equals(other.getExtCustomerId()))) &&
            ((this.extOrderId==null && other.getExtOrderId()==null) || 
             (this.extOrderId!=null &&
              this.extOrderId.equals(other.getExtOrderId()))) &&
            ((this.forcedShipmentWithInvoice==null && other.getForcedShipmentWithInvoice()==null) || 
             (this.forcedShipmentWithInvoice!=null &&
              this.forcedShipmentWithInvoice.equals(other.getForcedShipmentWithInvoice()))) &&
            ((this.giftMessageId==null && other.getGiftMessageId()==null) || 
             (this.giftMessageId!=null &&
              this.giftMessageId.equals(other.getGiftMessageId()))) &&
            ((this.globalCurrencyCode==null && other.getGlobalCurrencyCode()==null) || 
             (this.globalCurrencyCode!=null &&
              this.globalCurrencyCode.equals(other.getGlobalCurrencyCode()))) &&
            ((this.grandTotal==null && other.getGrandTotal()==null) || 
             (this.grandTotal!=null &&
              this.grandTotal.equals(other.getGrandTotal()))) &&
            ((this.hiddenTaxAmount==null && other.getHiddenTaxAmount()==null) || 
             (this.hiddenTaxAmount!=null &&
              this.hiddenTaxAmount.equals(other.getHiddenTaxAmount()))) &&
            ((this.hiddenTaxInvoiced==null && other.getHiddenTaxInvoiced()==null) || 
             (this.hiddenTaxInvoiced!=null &&
              this.hiddenTaxInvoiced.equals(other.getHiddenTaxInvoiced()))) &&
            ((this.hiddenTaxRefunded==null && other.getHiddenTaxRefunded()==null) || 
             (this.hiddenTaxRefunded!=null &&
              this.hiddenTaxRefunded.equals(other.getHiddenTaxRefunded()))) &&
            ((this.holdBeforeState==null && other.getHoldBeforeState()==null) || 
             (this.holdBeforeState!=null &&
              this.holdBeforeState.equals(other.getHoldBeforeState()))) &&
            ((this.holdBeforeStatus==null && other.getHoldBeforeStatus()==null) || 
             (this.holdBeforeStatus!=null &&
              this.holdBeforeStatus.equals(other.getHoldBeforeStatus()))) &&
            ((this.incrementId==null && other.getIncrementId()==null) || 
             (this.incrementId!=null &&
              this.incrementId.equals(other.getIncrementId()))) &&
            ((this.isVirtual==null && other.getIsVirtual()==null) || 
             (this.isVirtual!=null &&
              this.isVirtual.equals(other.getIsVirtual()))) &&
            ((this.orderCurrencyCode==null && other.getOrderCurrencyCode()==null) || 
             (this.orderCurrencyCode!=null &&
              this.orderCurrencyCode.equals(other.getOrderCurrencyCode()))) &&
            ((this.originalIncrementId==null && other.getOriginalIncrementId()==null) || 
             (this.originalIncrementId!=null &&
              this.originalIncrementId.equals(other.getOriginalIncrementId()))) &&
            ((this.paymentAuthExpiration==null && other.getPaymentAuthExpiration()==null) || 
             (this.paymentAuthExpiration!=null &&
              this.paymentAuthExpiration.equals(other.getPaymentAuthExpiration()))) &&
            ((this.paymentAuthorizationAmount==null && other.getPaymentAuthorizationAmount()==null) || 
             (this.paymentAuthorizationAmount!=null &&
              this.paymentAuthorizationAmount.equals(other.getPaymentAuthorizationAmount()))) &&
            ((this.paypalIpnCustomerNotified==null && other.getPaypalIpnCustomerNotified()==null) || 
             (this.paypalIpnCustomerNotified!=null &&
              this.paypalIpnCustomerNotified.equals(other.getPaypalIpnCustomerNotified()))) &&
            ((this.pendingTlog==null && other.getPendingTlog()==null) || 
             (this.pendingTlog!=null &&
              this.pendingTlog.equals(other.getPendingTlog()))) &&
            ((this.protectCode==null && other.getProtectCode()==null) || 
             (this.protectCode!=null &&
              this.protectCode.equals(other.getProtectCode()))) &&
            ((this.quoteAddressId==null && other.getQuoteAddressId()==null) || 
             (this.quoteAddressId!=null &&
              this.quoteAddressId.equals(other.getQuoteAddressId()))) &&
            ((this.quoteId==null && other.getQuoteId()==null) || 
             (this.quoteId!=null &&
              this.quoteId.equals(other.getQuoteId()))) &&
            ((this.relationChildId==null && other.getRelationChildId()==null) || 
             (this.relationChildId!=null &&
              this.relationChildId.equals(other.getRelationChildId()))) &&
            ((this.relationChildRealId==null && other.getRelationChildRealId()==null) || 
             (this.relationChildRealId!=null &&
              this.relationChildRealId.equals(other.getRelationChildRealId()))) &&
            ((this.relationParentId==null && other.getRelationParentId()==null) || 
             (this.relationParentId!=null &&
              this.relationParentId.equals(other.getRelationParentId()))) &&
            ((this.relationParentRealId==null && other.getRelationParentRealId()==null) || 
             (this.relationParentRealId!=null &&
              this.relationParentRealId.equals(other.getRelationParentRealId()))) &&
            ((this.remoteIp==null && other.getRemoteIp()==null) || 
             (this.remoteIp!=null &&
              this.remoteIp.equals(other.getRemoteIp()))) &&
            ((this.shippingAddressId==null && other.getShippingAddressId()==null) || 
             (this.shippingAddressId!=null &&
              this.shippingAddressId.equals(other.getShippingAddressId()))) &&
            ((this.shippingAmount==null && other.getShippingAmount()==null) || 
             (this.shippingAmount!=null &&
              this.shippingAmount.equals(other.getShippingAmount()))) &&
            ((this.shippingCanceled==null && other.getShippingCanceled()==null) || 
             (this.shippingCanceled!=null &&
              this.shippingCanceled.equals(other.getShippingCanceled()))) &&
            ((this.shippingDescription==null && other.getShippingDescription()==null) || 
             (this.shippingDescription!=null &&
              this.shippingDescription.equals(other.getShippingDescription()))) &&
            ((this.shippingDiscountAmount==null && other.getShippingDiscountAmount()==null) || 
             (this.shippingDiscountAmount!=null &&
              this.shippingDiscountAmount.equals(other.getShippingDiscountAmount()))) &&
            ((this.shippingHiddenTaxAmount==null && other.getShippingHiddenTaxAmount()==null) || 
             (this.shippingHiddenTaxAmount!=null &&
              this.shippingHiddenTaxAmount.equals(other.getShippingHiddenTaxAmount()))) &&
            ((this.shippingInclTax==null && other.getShippingInclTax()==null) || 
             (this.shippingInclTax!=null &&
              this.shippingInclTax.equals(other.getShippingInclTax()))) &&
            ((this.shippingInvoiced==null && other.getShippingInvoiced()==null) || 
             (this.shippingInvoiced!=null &&
              this.shippingInvoiced.equals(other.getShippingInvoiced()))) &&
            ((this.shippingMethod==null && other.getShippingMethod()==null) || 
             (this.shippingMethod!=null &&
              this.shippingMethod.equals(other.getShippingMethod()))) &&
            ((this.shippingRefunded==null && other.getShippingRefunded()==null) || 
             (this.shippingRefunded!=null &&
              this.shippingRefunded.equals(other.getShippingRefunded()))) &&
            ((this.shippingTaxAmount==null && other.getShippingTaxAmount()==null) || 
             (this.shippingTaxAmount!=null &&
              this.shippingTaxAmount.equals(other.getShippingTaxAmount()))) &&
            ((this.shippingTaxRefunded==null && other.getShippingTaxRefunded()==null) || 
             (this.shippingTaxRefunded!=null &&
              this.shippingTaxRefunded.equals(other.getShippingTaxRefunded()))) &&
            ((this.state==null && other.getState()==null) || 
             (this.state!=null &&
              this.state.equals(other.getState()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.storeCurrencyCode==null && other.getStoreCurrencyCode()==null) || 
             (this.storeCurrencyCode!=null &&
              this.storeCurrencyCode.equals(other.getStoreCurrencyCode()))) &&
            ((this.storeId==null && other.getStoreId()==null) || 
             (this.storeId!=null &&
              this.storeId.equals(other.getStoreId()))) &&
            ((this.storeName==null && other.getStoreName()==null) || 
             (this.storeName!=null &&
              this.storeName.equals(other.getStoreName()))) &&
            ((this.storeToBaseRate==null && other.getStoreToBaseRate()==null) || 
             (this.storeToBaseRate!=null &&
              this.storeToBaseRate.equals(other.getStoreToBaseRate()))) &&
            ((this.storeToOrderRate==null && other.getStoreToOrderRate()==null) || 
             (this.storeToOrderRate!=null &&
              this.storeToOrderRate.equals(other.getStoreToOrderRate()))) &&
            ((this.subtotal==null && other.getSubtotal()==null) || 
             (this.subtotal!=null &&
              this.subtotal.equals(other.getSubtotal()))) &&
            ((this.subtotalCanceled==null && other.getSubtotalCanceled()==null) || 
             (this.subtotalCanceled!=null &&
              this.subtotalCanceled.equals(other.getSubtotalCanceled()))) &&
            ((this.subtotalInclTax==null && other.getSubtotalInclTax()==null) || 
             (this.subtotalInclTax!=null &&
              this.subtotalInclTax.equals(other.getSubtotalInclTax()))) &&
            ((this.subtotalInvoiced==null && other.getSubtotalInvoiced()==null) || 
             (this.subtotalInvoiced!=null &&
              this.subtotalInvoiced.equals(other.getSubtotalInvoiced()))) &&
            ((this.subtotalRefunded==null && other.getSubtotalRefunded()==null) || 
             (this.subtotalRefunded!=null &&
              this.subtotalRefunded.equals(other.getSubtotalRefunded()))) &&
            ((this.supplierId==null && other.getSupplierId()==null) || 
             (this.supplierId!=null &&
              this.supplierId.equals(other.getSupplierId()))) &&
            ((this.taxAmount==null && other.getTaxAmount()==null) || 
             (this.taxAmount!=null &&
              this.taxAmount.equals(other.getTaxAmount()))) &&
            ((this.taxCanceled==null && other.getTaxCanceled()==null) || 
             (this.taxCanceled!=null &&
              this.taxCanceled.equals(other.getTaxCanceled()))) &&
            ((this.taxInvoiced==null && other.getTaxInvoiced()==null) || 
             (this.taxInvoiced!=null &&
              this.taxInvoiced.equals(other.getTaxInvoiced()))) &&
            ((this.taxRefunded==null && other.getTaxRefunded()==null) || 
             (this.taxRefunded!=null &&
              this.taxRefunded.equals(other.getTaxRefunded()))) &&
            ((this.totalCanceled==null && other.getTotalCanceled()==null) || 
             (this.totalCanceled!=null &&
              this.totalCanceled.equals(other.getTotalCanceled()))) &&
            ((this.totalDue==null && other.getTotalDue()==null) || 
             (this.totalDue!=null &&
              this.totalDue.equals(other.getTotalDue()))) &&
            ((this.totalInvoiced==null && other.getTotalInvoiced()==null) || 
             (this.totalInvoiced!=null &&
              this.totalInvoiced.equals(other.getTotalInvoiced()))) &&
            this.totalItemCount == other.getTotalItemCount() &&
            ((this.totalOfflineRefunded==null && other.getTotalOfflineRefunded()==null) || 
             (this.totalOfflineRefunded!=null &&
              this.totalOfflineRefunded.equals(other.getTotalOfflineRefunded()))) &&
            ((this.totalOnlineRefunded==null && other.getTotalOnlineRefunded()==null) || 
             (this.totalOnlineRefunded!=null &&
              this.totalOnlineRefunded.equals(other.getTotalOnlineRefunded()))) &&
            ((this.totalPaid==null && other.getTotalPaid()==null) || 
             (this.totalPaid!=null &&
              this.totalPaid.equals(other.getTotalPaid()))) &&
            ((this.totalQtyOrdered==null && other.getTotalQtyOrdered()==null) || 
             (this.totalQtyOrdered!=null &&
              this.totalQtyOrdered.equals(other.getTotalQtyOrdered()))) &&
            ((this.totalRefunded==null && other.getTotalRefunded()==null) || 
             (this.totalRefunded!=null &&
              this.totalRefunded.equals(other.getTotalRefunded()))) &&
            ((this.updatedAt==null && other.getUpdatedAt()==null) || 
             (this.updatedAt!=null &&
              this.updatedAt.equals(other.getUpdatedAt()))) &&
            ((this.weight==null && other.getWeight()==null) || 
             (this.weight!=null &&
              this.weight.equals(other.getWeight())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getXForwardedFor() != null) {
            _hashCode += getXForwardedFor().hashCode();
        }
        if (getAdjustmentNegative() != null) {
            _hashCode += getAdjustmentNegative().hashCode();
        }
        if (getAdjustmentPositive() != null) {
            _hashCode += getAdjustmentPositive().hashCode();
        }
        if (getAppliedRuleIds() != null) {
            _hashCode += getAppliedRuleIds().hashCode();
        }
        if (getBaseAdjustmentNegative() != null) {
            _hashCode += getBaseAdjustmentNegative().hashCode();
        }
        if (getBaseAdjustmentPositive() != null) {
            _hashCode += getBaseAdjustmentPositive().hashCode();
        }
        if (getBaseCurrencyCode() != null) {
            _hashCode += getBaseCurrencyCode().hashCode();
        }
        if (getBaseDiscountAmount() != null) {
            _hashCode += getBaseDiscountAmount().hashCode();
        }
        if (getBaseDiscountCanceled() != null) {
            _hashCode += getBaseDiscountCanceled().hashCode();
        }
        if (getBaseDiscountInvoiced() != null) {
            _hashCode += getBaseDiscountInvoiced().hashCode();
        }
        if (getBaseDiscountRefunded() != null) {
            _hashCode += getBaseDiscountRefunded().hashCode();
        }
        if (getBaseGrandTotal() != null) {
            _hashCode += getBaseGrandTotal().hashCode();
        }
        if (getBaseHiddenTaxAmount() != null) {
            _hashCode += getBaseHiddenTaxAmount().hashCode();
        }
        if (getBaseHiddenTaxInvoiced() != null) {
            _hashCode += getBaseHiddenTaxInvoiced().hashCode();
        }
        if (getBaseHiddenTaxRefunded() != null) {
            _hashCode += getBaseHiddenTaxRefunded().hashCode();
        }
        if (getBaseShippingAmount() != null) {
            _hashCode += getBaseShippingAmount().hashCode();
        }
        if (getBaseShippingCanceled() != null) {
            _hashCode += getBaseShippingCanceled().hashCode();
        }
        if (getBaseShippingDiscountAmount() != null) {
            _hashCode += getBaseShippingDiscountAmount().hashCode();
        }
        if (getBaseShippingHiddenTaxAmnt() != null) {
            _hashCode += getBaseShippingHiddenTaxAmnt().hashCode();
        }
        if (getBaseShippingInclTax() != null) {
            _hashCode += getBaseShippingInclTax().hashCode();
        }
        if (getBaseShippingInvoiced() != null) {
            _hashCode += getBaseShippingInvoiced().hashCode();
        }
        if (getBaseShippingRefunded() != null) {
            _hashCode += getBaseShippingRefunded().hashCode();
        }
        if (getBaseShippingTaxAmount() != null) {
            _hashCode += getBaseShippingTaxAmount().hashCode();
        }
        if (getBaseShippingTaxRefunded() != null) {
            _hashCode += getBaseShippingTaxRefunded().hashCode();
        }
        if (getBaseSubtotal() != null) {
            _hashCode += getBaseSubtotal().hashCode();
        }
        if (getBaseSubtotalCanceled() != null) {
            _hashCode += getBaseSubtotalCanceled().hashCode();
        }
        if (getBaseSubtotalInclTax() != null) {
            _hashCode += getBaseSubtotalInclTax().hashCode();
        }
        if (getBaseSubtotalInvoiced() != null) {
            _hashCode += getBaseSubtotalInvoiced().hashCode();
        }
        if (getBaseSubtotalRefunded() != null) {
            _hashCode += getBaseSubtotalRefunded().hashCode();
        }
        if (getBaseTaxAmount() != null) {
            _hashCode += getBaseTaxAmount().hashCode();
        }
        if (getBaseTaxCanceled() != null) {
            _hashCode += getBaseTaxCanceled().hashCode();
        }
        if (getBaseTaxInvoiced() != null) {
            _hashCode += getBaseTaxInvoiced().hashCode();
        }
        if (getBaseTaxRefunded() != null) {
            _hashCode += getBaseTaxRefunded().hashCode();
        }
        if (getBaseToGlobalRate() != null) {
            _hashCode += getBaseToGlobalRate().hashCode();
        }
        if (getBaseToOrderRate() != null) {
            _hashCode += getBaseToOrderRate().hashCode();
        }
        if (getBaseTotalCanceled() != null) {
            _hashCode += getBaseTotalCanceled().hashCode();
        }
        if (getBaseTotalDue() != null) {
            _hashCode += getBaseTotalDue().hashCode();
        }
        if (getBaseTotalInvoiced() != null) {
            _hashCode += getBaseTotalInvoiced().hashCode();
        }
        if (getBaseTotalInvoicedCost() != null) {
            _hashCode += getBaseTotalInvoicedCost().hashCode();
        }
        if (getBaseTotalOfflineRefunded() != null) {
            _hashCode += getBaseTotalOfflineRefunded().hashCode();
        }
        if (getBaseTotalOnlineRefunded() != null) {
            _hashCode += getBaseTotalOnlineRefunded().hashCode();
        }
        if (getBaseTotalPaid() != null) {
            _hashCode += getBaseTotalPaid().hashCode();
        }
        if (getBaseTotalQtyOrdered() != null) {
            _hashCode += getBaseTotalQtyOrdered().hashCode();
        }
        if (getBaseTotalRefunded() != null) {
            _hashCode += getBaseTotalRefunded().hashCode();
        }
        if (getBillingAddressId() != null) {
            _hashCode += getBillingAddressId().hashCode();
        }
        if (getCanShipPartially() != null) {
            _hashCode += getCanShipPartially().hashCode();
        }
        if (getCanShipPartiallyItem() != null) {
            _hashCode += getCanShipPartiallyItem().hashCode();
        }
        if (getCouponCode() != null) {
            _hashCode += getCouponCode().hashCode();
        }
        if (getCouponRuleName() != null) {
            _hashCode += getCouponRuleName().hashCode();
        }
        if (getCreatedAt() != null) {
            _hashCode += getCreatedAt().hashCode();
        }
        if (getCustomerDob() != null) {
            _hashCode += getCustomerDob().hashCode();
        }
        if (getCustomerEmail() != null) {
            _hashCode += getCustomerEmail().hashCode();
        }
        if (getCustomerFirstname() != null) {
            _hashCode += getCustomerFirstname().hashCode();
        }
        if (getCustomerGender() != null) {
            _hashCode += getCustomerGender().hashCode();
        }
        if (getCustomerGroupId() != null) {
            _hashCode += getCustomerGroupId().hashCode();
        }
        if (getCustomerId() != null) {
            _hashCode += getCustomerId().hashCode();
        }
        if (getCustomerIsGuest() != null) {
            _hashCode += getCustomerIsGuest().hashCode();
        }
        if (getCustomerLastname() != null) {
            _hashCode += getCustomerLastname().hashCode();
        }
        if (getCustomerMiddlename() != null) {
            _hashCode += getCustomerMiddlename().hashCode();
        }
        if (getCustomerNote() != null) {
            _hashCode += getCustomerNote().hashCode();
        }
        if (getCustomerNoteNotify() != null) {
            _hashCode += getCustomerNoteNotify().hashCode();
        }
        if (getCustomerPrefix() != null) {
            _hashCode += getCustomerPrefix().hashCode();
        }
        if (getCustomerSuffix() != null) {
            _hashCode += getCustomerSuffix().hashCode();
        }
        if (getCustomerTaxvat() != null) {
            _hashCode += getCustomerTaxvat().hashCode();
        }
        if (getDiscountAmount() != null) {
            _hashCode += getDiscountAmount().hashCode();
        }
        if (getDiscountCanceled() != null) {
            _hashCode += getDiscountCanceled().hashCode();
        }
        if (getDiscountDescription() != null) {
            _hashCode += getDiscountDescription().hashCode();
        }
        if (getDiscountInvoiced() != null) {
            _hashCode += getDiscountInvoiced().hashCode();
        }
        if (getDiscountRefunded() != null) {
            _hashCode += getDiscountRefunded().hashCode();
        }
        if (getEditIncrement() != null) {
            _hashCode += getEditIncrement().hashCode();
        }
        if (getEmailSent() != null) {
            _hashCode += getEmailSent().hashCode();
        }
        if (getEntityId() != null) {
            _hashCode += getEntityId().hashCode();
        }
        if (getExtCustomerId() != null) {
            _hashCode += getExtCustomerId().hashCode();
        }
        if (getExtOrderId() != null) {
            _hashCode += getExtOrderId().hashCode();
        }
        if (getForcedShipmentWithInvoice() != null) {
            _hashCode += getForcedShipmentWithInvoice().hashCode();
        }
        if (getGiftMessageId() != null) {
            _hashCode += getGiftMessageId().hashCode();
        }
        if (getGlobalCurrencyCode() != null) {
            _hashCode += getGlobalCurrencyCode().hashCode();
        }
        if (getGrandTotal() != null) {
            _hashCode += getGrandTotal().hashCode();
        }
        if (getHiddenTaxAmount() != null) {
            _hashCode += getHiddenTaxAmount().hashCode();
        }
        if (getHiddenTaxInvoiced() != null) {
            _hashCode += getHiddenTaxInvoiced().hashCode();
        }
        if (getHiddenTaxRefunded() != null) {
            _hashCode += getHiddenTaxRefunded().hashCode();
        }
        if (getHoldBeforeState() != null) {
            _hashCode += getHoldBeforeState().hashCode();
        }
        if (getHoldBeforeStatus() != null) {
            _hashCode += getHoldBeforeStatus().hashCode();
        }
        if (getIncrementId() != null) {
            _hashCode += getIncrementId().hashCode();
        }
        if (getIsVirtual() != null) {
            _hashCode += getIsVirtual().hashCode();
        }
        if (getOrderCurrencyCode() != null) {
            _hashCode += getOrderCurrencyCode().hashCode();
        }
        if (getOriginalIncrementId() != null) {
            _hashCode += getOriginalIncrementId().hashCode();
        }
        if (getPaymentAuthExpiration() != null) {
            _hashCode += getPaymentAuthExpiration().hashCode();
        }
        if (getPaymentAuthorizationAmount() != null) {
            _hashCode += getPaymentAuthorizationAmount().hashCode();
        }
        if (getPaypalIpnCustomerNotified() != null) {
            _hashCode += getPaypalIpnCustomerNotified().hashCode();
        }
        if (getPendingTlog() != null) {
            _hashCode += getPendingTlog().hashCode();
        }
        if (getProtectCode() != null) {
            _hashCode += getProtectCode().hashCode();
        }
        if (getQuoteAddressId() != null) {
            _hashCode += getQuoteAddressId().hashCode();
        }
        if (getQuoteId() != null) {
            _hashCode += getQuoteId().hashCode();
        }
        if (getRelationChildId() != null) {
            _hashCode += getRelationChildId().hashCode();
        }
        if (getRelationChildRealId() != null) {
            _hashCode += getRelationChildRealId().hashCode();
        }
        if (getRelationParentId() != null) {
            _hashCode += getRelationParentId().hashCode();
        }
        if (getRelationParentRealId() != null) {
            _hashCode += getRelationParentRealId().hashCode();
        }
        if (getRemoteIp() != null) {
            _hashCode += getRemoteIp().hashCode();
        }
        if (getShippingAddressId() != null) {
            _hashCode += getShippingAddressId().hashCode();
        }
        if (getShippingAmount() != null) {
            _hashCode += getShippingAmount().hashCode();
        }
        if (getShippingCanceled() != null) {
            _hashCode += getShippingCanceled().hashCode();
        }
        if (getShippingDescription() != null) {
            _hashCode += getShippingDescription().hashCode();
        }
        if (getShippingDiscountAmount() != null) {
            _hashCode += getShippingDiscountAmount().hashCode();
        }
        if (getShippingHiddenTaxAmount() != null) {
            _hashCode += getShippingHiddenTaxAmount().hashCode();
        }
        if (getShippingInclTax() != null) {
            _hashCode += getShippingInclTax().hashCode();
        }
        if (getShippingInvoiced() != null) {
            _hashCode += getShippingInvoiced().hashCode();
        }
        if (getShippingMethod() != null) {
            _hashCode += getShippingMethod().hashCode();
        }
        if (getShippingRefunded() != null) {
            _hashCode += getShippingRefunded().hashCode();
        }
        if (getShippingTaxAmount() != null) {
            _hashCode += getShippingTaxAmount().hashCode();
        }
        if (getShippingTaxRefunded() != null) {
            _hashCode += getShippingTaxRefunded().hashCode();
        }
        if (getState() != null) {
            _hashCode += getState().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getStoreCurrencyCode() != null) {
            _hashCode += getStoreCurrencyCode().hashCode();
        }
        if (getStoreId() != null) {
            _hashCode += getStoreId().hashCode();
        }
        if (getStoreName() != null) {
            _hashCode += getStoreName().hashCode();
        }
        if (getStoreToBaseRate() != null) {
            _hashCode += getStoreToBaseRate().hashCode();
        }
        if (getStoreToOrderRate() != null) {
            _hashCode += getStoreToOrderRate().hashCode();
        }
        if (getSubtotal() != null) {
            _hashCode += getSubtotal().hashCode();
        }
        if (getSubtotalCanceled() != null) {
            _hashCode += getSubtotalCanceled().hashCode();
        }
        if (getSubtotalInclTax() != null) {
            _hashCode += getSubtotalInclTax().hashCode();
        }
        if (getSubtotalInvoiced() != null) {
            _hashCode += getSubtotalInvoiced().hashCode();
        }
        if (getSubtotalRefunded() != null) {
            _hashCode += getSubtotalRefunded().hashCode();
        }
        if (getSupplierId() != null) {
            _hashCode += getSupplierId().hashCode();
        }
        if (getTaxAmount() != null) {
            _hashCode += getTaxAmount().hashCode();
        }
        if (getTaxCanceled() != null) {
            _hashCode += getTaxCanceled().hashCode();
        }
        if (getTaxInvoiced() != null) {
            _hashCode += getTaxInvoiced().hashCode();
        }
        if (getTaxRefunded() != null) {
            _hashCode += getTaxRefunded().hashCode();
        }
        if (getTotalCanceled() != null) {
            _hashCode += getTotalCanceled().hashCode();
        }
        if (getTotalDue() != null) {
            _hashCode += getTotalDue().hashCode();
        }
        if (getTotalInvoiced() != null) {
            _hashCode += getTotalInvoiced().hashCode();
        }
        _hashCode += getTotalItemCount();
        if (getTotalOfflineRefunded() != null) {
            _hashCode += getTotalOfflineRefunded().hashCode();
        }
        if (getTotalOnlineRefunded() != null) {
            _hashCode += getTotalOnlineRefunded().hashCode();
        }
        if (getTotalPaid() != null) {
            _hashCode += getTotalPaid().hashCode();
        }
        if (getTotalQtyOrdered() != null) {
            _hashCode += getTotalQtyOrdered().hashCode();
        }
        if (getTotalRefunded() != null) {
            _hashCode += getTotalRefunded().hashCode();
        }
        if (getUpdatedAt() != null) {
            _hashCode += getUpdatedAt().hashCode();
        }
        if (getWeight() != null) {
            _hashCode += getWeight().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SalesFlatOrder.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "SalesFlatOrder"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("XForwardedFor");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "XForwardedFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("adjustmentNegative");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "adjustmentNegative"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("adjustmentPositive");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "adjustmentPositive"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("appliedRuleIds");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "appliedRuleIds"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseAdjustmentNegative");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseAdjustmentNegative"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseAdjustmentPositive");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseAdjustmentPositive"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseDiscountAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseDiscountAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseDiscountCanceled");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseDiscountCanceled"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseDiscountInvoiced");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseDiscountInvoiced"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseDiscountRefunded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseDiscountRefunded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseGrandTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseGrandTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseHiddenTaxAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseHiddenTaxAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseHiddenTaxInvoiced");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseHiddenTaxInvoiced"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseHiddenTaxRefunded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseHiddenTaxRefunded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseShippingAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseShippingAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseShippingCanceled");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseShippingCanceled"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseShippingDiscountAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseShippingDiscountAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseShippingHiddenTaxAmnt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseShippingHiddenTaxAmnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseShippingInclTax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseShippingInclTax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseShippingInvoiced");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseShippingInvoiced"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseShippingRefunded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseShippingRefunded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseShippingTaxAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseShippingTaxAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseShippingTaxRefunded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseShippingTaxRefunded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseSubtotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseSubtotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseSubtotalCanceled");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseSubtotalCanceled"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseSubtotalInclTax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseSubtotalInclTax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseSubtotalInvoiced");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseSubtotalInvoiced"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseSubtotalRefunded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseSubtotalRefunded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseTaxAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseTaxAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseTaxCanceled");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseTaxCanceled"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseTaxInvoiced");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseTaxInvoiced"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseTaxRefunded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseTaxRefunded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseToGlobalRate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseToGlobalRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseToOrderRate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseToOrderRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseTotalCanceled");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseTotalCanceled"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseTotalDue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseTotalDue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseTotalInvoiced");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseTotalInvoiced"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseTotalInvoicedCost");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseTotalInvoicedCost"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseTotalOfflineRefunded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseTotalOfflineRefunded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseTotalOnlineRefunded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseTotalOnlineRefunded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseTotalPaid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseTotalPaid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseTotalQtyOrdered");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseTotalQtyOrdered"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseTotalRefunded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "baseTotalRefunded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("billingAddressId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "billingAddressId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("canShipPartially");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "canShipPartially"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("canShipPartiallyItem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "canShipPartiallyItem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("couponCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "couponCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("couponRuleName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "couponRuleName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdAt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "createdAt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerDob");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "customerDob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerEmail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "customerEmail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerFirstname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "customerFirstname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerGender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "customerGender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerGroupId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "customerGroupId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "customerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerIsGuest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "customerIsGuest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerLastname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "customerLastname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerMiddlename");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "customerMiddlename"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerNote");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "customerNote"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerNoteNotify");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "customerNoteNotify"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerPrefix");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "customerPrefix"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerSuffix");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "customerSuffix"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerTaxvat");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "customerTaxvat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discountAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "discountAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discountCanceled");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "discountCanceled"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discountDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "discountDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discountInvoiced");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "discountInvoiced"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discountRefunded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "discountRefunded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("editIncrement");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "editIncrement"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emailSent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "emailSent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entityId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "entityId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extCustomerId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "extCustomerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extOrderId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "extOrderId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forcedShipmentWithInvoice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "forcedShipmentWithInvoice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftMessageId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "giftMessageId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("globalCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "globalCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("grandTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "grandTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hiddenTaxAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "hiddenTaxAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hiddenTaxInvoiced");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "hiddenTaxInvoiced"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hiddenTaxRefunded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "hiddenTaxRefunded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("holdBeforeState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "holdBeforeState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("holdBeforeStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "holdBeforeStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("incrementId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "incrementId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isVirtual");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "isVirtual"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "orderCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("originalIncrementId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "originalIncrementId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentAuthExpiration");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "paymentAuthExpiration"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentAuthorizationAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "paymentAuthorizationAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paypalIpnCustomerNotified");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "paypalIpnCustomerNotified"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pendingTlog");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "pendingTlog"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("protectCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "protectCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quoteAddressId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "quoteAddressId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quoteId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "quoteId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("relationChildId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "relationChildId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("relationChildRealId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "relationChildRealId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("relationParentId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "relationParentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("relationParentRealId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "relationParentRealId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("remoteIp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "remoteIp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shippingAddressId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "shippingAddressId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shippingAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "shippingAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shippingCanceled");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "shippingCanceled"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shippingDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "shippingDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shippingDiscountAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "shippingDiscountAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shippingHiddenTaxAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "shippingHiddenTaxAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shippingInclTax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "shippingInclTax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shippingInvoiced");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "shippingInvoiced"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shippingMethod");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "shippingMethod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shippingRefunded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "shippingRefunded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shippingTaxAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "shippingTaxAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shippingTaxRefunded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "shippingTaxRefunded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "state"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("storeCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "storeCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("storeId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "storeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("storeName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "storeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("storeToBaseRate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "storeToBaseRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("storeToOrderRate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "storeToOrderRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subtotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "subtotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subtotalCanceled");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "subtotalCanceled"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subtotalInclTax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "subtotalInclTax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subtotalInvoiced");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "subtotalInvoiced"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subtotalRefunded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "subtotalRefunded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("supplierId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "supplierId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "taxAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxCanceled");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "taxCanceled"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxInvoiced");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "taxInvoiced"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxRefunded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "taxRefunded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalCanceled");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "totalCanceled"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalDue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "totalDue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalInvoiced");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "totalInvoiced"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalItemCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "totalItemCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalOfflineRefunded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "totalOfflineRefunded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalOnlineRefunded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "totalOnlineRefunded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalPaid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "totalPaid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalQtyOrdered");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "totalQtyOrdered"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalRefunded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "totalRefunded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updatedAt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "updatedAt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("weight");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.magento.hibernate.entrepids.com", "weight"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
