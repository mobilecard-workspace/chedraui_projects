package com.addcel.ws.clientes.enigma.magento;

public class ConsultaPedidosProxy implements com.addcel.ws.clientes.enigma.magento.ConsultaPedidos {
  private String _endpoint = null;
  private com.addcel.ws.clientes.enigma.magento.ConsultaPedidos consultaPedidos = null;
  
  public ConsultaPedidosProxy() {
    _initConsultaPedidosProxy();
  }
  
  public ConsultaPedidosProxy(String endpoint) {
    _endpoint = endpoint;
    _initConsultaPedidosProxy();
  }
  
  private void _initConsultaPedidosProxy() {
    try {
      consultaPedidos = (new com.addcel.ws.clientes.enigma.magento.ConsultaPedidosServiceLocator()).getConsultaPedidos();
      if (consultaPedidos != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)consultaPedidos)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)consultaPedidos)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (consultaPedidos != null)
      ((javax.xml.rpc.Stub)consultaPedidos)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.ws.clientes.enigma.magento.ConsultaPedidos getConsultaPedidos() {
    if (consultaPedidos == null)
      _initConsultaPedidosProxy();
    return consultaPedidos;
  }
  
  public java.lang.String actualizaDevolucion(java.lang.String numeroPedido, java.lang.String firmaElectronica, java.lang.String[] skus, java.lang.String[] numero) throws java.rmi.RemoteException{
    if (consultaPedidos == null)
      _initConsultaPedidosProxy();
    return consultaPedidos.actualizaDevolucion(numeroPedido, firmaElectronica, skus, numero);
  }
  
  public com.addcel.ws.clientes.enigma.magento.RespuestaConsultarPedido consultar(java.lang.String numeroPedido, java.lang.String firmaElectronica) throws java.rmi.RemoteException{
    if (consultaPedidos == null)
      _initConsultaPedidosProxy();
    return consultaPedidos.consultar(numeroPedido, firmaElectronica);
  }
  
  public java.lang.String actualizaTiendaPago(java.lang.String numeroPedido, java.lang.String firmaElectronica, java.lang.String idTiendaPago) throws java.rmi.RemoteException{
    if (consultaPedidos == null)
      _initConsultaPedidosProxy();
    return consultaPedidos.actualizaTiendaPago(numeroPedido, firmaElectronica, idTiendaPago);
  }
  
  public java.lang.String actualizaStatus(java.lang.String numeroPedido, java.lang.String firmaElectronica, java.lang.String estatus) throws java.rmi.RemoteException{
    if (consultaPedidos == null)
      _initConsultaPedidosProxy();
    return consultaPedidos.actualizaStatus(numeroPedido, firmaElectronica, estatus);
  }
  
  
}