/**
 * RespuestaConsultarPedidoPromociones.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.enigma.magento;

public class RespuestaConsultarPedidoPromociones  implements java.io.Serializable {
    private java.lang.String tipoBeneficio;

    private java.lang.String promoType;

    private java.lang.String promoId;

    private java.lang.String descripcionPromo;

    private java.math.BigDecimal montoBeneficio;

    private java.math.BigDecimal porcentajeDescuentoBonificacion;

    private java.lang.String numeroTarjeta;

    private java.lang.String plazosPago;

    private java.lang.String disparador;

    private java.lang.Integer rangoMontoPromocional;

    private java.lang.String numeroAutorizacion;

    public RespuestaConsultarPedidoPromociones() {
    }

    public RespuestaConsultarPedidoPromociones(
           java.lang.String tipoBeneficio,
           java.lang.String promoType,
           java.lang.String promoId,
           java.lang.String descripcionPromo,
           java.math.BigDecimal montoBeneficio,
           java.math.BigDecimal porcentajeDescuentoBonificacion,
           java.lang.String numeroTarjeta,
           java.lang.String plazosPago,
           java.lang.String disparador,
           java.lang.Integer rangoMontoPromocional,
           java.lang.String numeroAutorizacion) {
           this.tipoBeneficio = tipoBeneficio;
           this.promoType = promoType;
           this.promoId = promoId;
           this.descripcionPromo = descripcionPromo;
           this.montoBeneficio = montoBeneficio;
           this.porcentajeDescuentoBonificacion = porcentajeDescuentoBonificacion;
           this.numeroTarjeta = numeroTarjeta;
           this.plazosPago = plazosPago;
           this.disparador = disparador;
           this.rangoMontoPromocional = rangoMontoPromocional;
           this.numeroAutorizacion = numeroAutorizacion;
    }


    /**
     * Gets the tipoBeneficio value for this RespuestaConsultarPedidoPromociones.
     * 
     * @return tipoBeneficio
     */
    public java.lang.String getTipoBeneficio() {
        return tipoBeneficio;
    }


    /**
     * Sets the tipoBeneficio value for this RespuestaConsultarPedidoPromociones.
     * 
     * @param tipoBeneficio
     */
    public void setTipoBeneficio(java.lang.String tipoBeneficio) {
        this.tipoBeneficio = tipoBeneficio;
    }


    /**
     * Gets the promoType value for this RespuestaConsultarPedidoPromociones.
     * 
     * @return promoType
     */
    public java.lang.String getPromoType() {
        return promoType;
    }


    /**
     * Sets the promoType value for this RespuestaConsultarPedidoPromociones.
     * 
     * @param promoType
     */
    public void setPromoType(java.lang.String promoType) {
        this.promoType = promoType;
    }


    /**
     * Gets the promoId value for this RespuestaConsultarPedidoPromociones.
     * 
     * @return promoId
     */
    public java.lang.String getPromoId() {
        return promoId;
    }


    /**
     * Sets the promoId value for this RespuestaConsultarPedidoPromociones.
     * 
     * @param promoId
     */
    public void setPromoId(java.lang.String promoId) {
        this.promoId = promoId;
    }


    /**
     * Gets the descripcionPromo value for this RespuestaConsultarPedidoPromociones.
     * 
     * @return descripcionPromo
     */
    public java.lang.String getDescripcionPromo() {
        return descripcionPromo;
    }


    /**
     * Sets the descripcionPromo value for this RespuestaConsultarPedidoPromociones.
     * 
     * @param descripcionPromo
     */
    public void setDescripcionPromo(java.lang.String descripcionPromo) {
        this.descripcionPromo = descripcionPromo;
    }


    /**
     * Gets the montoBeneficio value for this RespuestaConsultarPedidoPromociones.
     * 
     * @return montoBeneficio
     */
    public java.math.BigDecimal getMontoBeneficio() {
        return montoBeneficio;
    }


    /**
     * Sets the montoBeneficio value for this RespuestaConsultarPedidoPromociones.
     * 
     * @param montoBeneficio
     */
    public void setMontoBeneficio(java.math.BigDecimal montoBeneficio) {
        this.montoBeneficio = montoBeneficio;
    }


    /**
     * Gets the porcentajeDescuentoBonificacion value for this RespuestaConsultarPedidoPromociones.
     * 
     * @return porcentajeDescuentoBonificacion
     */
    public java.math.BigDecimal getPorcentajeDescuentoBonificacion() {
        return porcentajeDescuentoBonificacion;
    }


    /**
     * Sets the porcentajeDescuentoBonificacion value for this RespuestaConsultarPedidoPromociones.
     * 
     * @param porcentajeDescuentoBonificacion
     */
    public void setPorcentajeDescuentoBonificacion(java.math.BigDecimal porcentajeDescuentoBonificacion) {
        this.porcentajeDescuentoBonificacion = porcentajeDescuentoBonificacion;
    }


    /**
     * Gets the numeroTarjeta value for this RespuestaConsultarPedidoPromociones.
     * 
     * @return numeroTarjeta
     */
    public java.lang.String getNumeroTarjeta() {
        return numeroTarjeta;
    }


    /**
     * Sets the numeroTarjeta value for this RespuestaConsultarPedidoPromociones.
     * 
     * @param numeroTarjeta
     */
    public void setNumeroTarjeta(java.lang.String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }


    /**
     * Gets the plazosPago value for this RespuestaConsultarPedidoPromociones.
     * 
     * @return plazosPago
     */
    public java.lang.String getPlazosPago() {
        return plazosPago;
    }


    /**
     * Sets the plazosPago value for this RespuestaConsultarPedidoPromociones.
     * 
     * @param plazosPago
     */
    public void setPlazosPago(java.lang.String plazosPago) {
        this.plazosPago = plazosPago;
    }


    /**
     * Gets the disparador value for this RespuestaConsultarPedidoPromociones.
     * 
     * @return disparador
     */
    public java.lang.String getDisparador() {
        return disparador;
    }


    /**
     * Sets the disparador value for this RespuestaConsultarPedidoPromociones.
     * 
     * @param disparador
     */
    public void setDisparador(java.lang.String disparador) {
        this.disparador = disparador;
    }


    /**
     * Gets the rangoMontoPromocional value for this RespuestaConsultarPedidoPromociones.
     * 
     * @return rangoMontoPromocional
     */
    public java.lang.Integer getRangoMontoPromocional() {
        return rangoMontoPromocional;
    }


    /**
     * Sets the rangoMontoPromocional value for this RespuestaConsultarPedidoPromociones.
     * 
     * @param rangoMontoPromocional
     */
    public void setRangoMontoPromocional(java.lang.Integer rangoMontoPromocional) {
        this.rangoMontoPromocional = rangoMontoPromocional;
    }


    /**
     * Gets the numeroAutorizacion value for this RespuestaConsultarPedidoPromociones.
     * 
     * @return numeroAutorizacion
     */
    public java.lang.String getNumeroAutorizacion() {
        return numeroAutorizacion;
    }


    /**
     * Sets the numeroAutorizacion value for this RespuestaConsultarPedidoPromociones.
     * 
     * @param numeroAutorizacion
     */
    public void setNumeroAutorizacion(java.lang.String numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RespuestaConsultarPedidoPromociones)) return false;
        RespuestaConsultarPedidoPromociones other = (RespuestaConsultarPedidoPromociones) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.tipoBeneficio==null && other.getTipoBeneficio()==null) || 
             (this.tipoBeneficio!=null &&
              this.tipoBeneficio.equals(other.getTipoBeneficio()))) &&
            ((this.promoType==null && other.getPromoType()==null) || 
             (this.promoType!=null &&
              this.promoType.equals(other.getPromoType()))) &&
            ((this.promoId==null && other.getPromoId()==null) || 
             (this.promoId!=null &&
              this.promoId.equals(other.getPromoId()))) &&
            ((this.descripcionPromo==null && other.getDescripcionPromo()==null) || 
             (this.descripcionPromo!=null &&
              this.descripcionPromo.equals(other.getDescripcionPromo()))) &&
            ((this.montoBeneficio==null && other.getMontoBeneficio()==null) || 
             (this.montoBeneficio!=null &&
              this.montoBeneficio.equals(other.getMontoBeneficio()))) &&
            ((this.porcentajeDescuentoBonificacion==null && other.getPorcentajeDescuentoBonificacion()==null) || 
             (this.porcentajeDescuentoBonificacion!=null &&
              this.porcentajeDescuentoBonificacion.equals(other.getPorcentajeDescuentoBonificacion()))) &&
            ((this.numeroTarjeta==null && other.getNumeroTarjeta()==null) || 
             (this.numeroTarjeta!=null &&
              this.numeroTarjeta.equals(other.getNumeroTarjeta()))) &&
            ((this.plazosPago==null && other.getPlazosPago()==null) || 
             (this.plazosPago!=null &&
              this.plazosPago.equals(other.getPlazosPago()))) &&
            ((this.disparador==null && other.getDisparador()==null) || 
             (this.disparador!=null &&
              this.disparador.equals(other.getDisparador()))) &&
            ((this.rangoMontoPromocional==null && other.getRangoMontoPromocional()==null) || 
             (this.rangoMontoPromocional!=null &&
              this.rangoMontoPromocional.equals(other.getRangoMontoPromocional()))) &&
            ((this.numeroAutorizacion==null && other.getNumeroAutorizacion()==null) || 
             (this.numeroAutorizacion!=null &&
              this.numeroAutorizacion.equals(other.getNumeroAutorizacion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTipoBeneficio() != null) {
            _hashCode += getTipoBeneficio().hashCode();
        }
        if (getPromoType() != null) {
            _hashCode += getPromoType().hashCode();
        }
        if (getPromoId() != null) {
            _hashCode += getPromoId().hashCode();
        }
        if (getDescripcionPromo() != null) {
            _hashCode += getDescripcionPromo().hashCode();
        }
        if (getMontoBeneficio() != null) {
            _hashCode += getMontoBeneficio().hashCode();
        }
        if (getPorcentajeDescuentoBonificacion() != null) {
            _hashCode += getPorcentajeDescuentoBonificacion().hashCode();
        }
        if (getNumeroTarjeta() != null) {
            _hashCode += getNumeroTarjeta().hashCode();
        }
        if (getPlazosPago() != null) {
            _hashCode += getPlazosPago().hashCode();
        }
        if (getDisparador() != null) {
            _hashCode += getDisparador().hashCode();
        }
        if (getRangoMontoPromocional() != null) {
            _hashCode += getRangoMontoPromocional().hashCode();
        }
        if (getNumeroAutorizacion() != null) {
            _hashCode += getNumeroAutorizacion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RespuestaConsultarPedidoPromociones.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "RespuestaConsultarPedidoPromociones"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoBeneficio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "tipoBeneficio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("promoType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "promoType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("promoId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "promoId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descripcionPromo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "descripcionPromo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("montoBeneficio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "montoBeneficio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("porcentajeDescuentoBonificacion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "porcentajeDescuentoBonificacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroTarjeta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "numeroTarjeta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("plazosPago");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "plazosPago"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("disparador");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "disparador"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rangoMontoPromocional");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "rangoMontoPromocional"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroAutorizacion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "numeroAutorizacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
