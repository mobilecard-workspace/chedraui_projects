/**
 * RespuestaConsultarPedidoItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.enigma.magento;

public class RespuestaConsultarPedidoItem  implements java.io.Serializable {
    private java.lang.String esPesable;

    private java.math.BigDecimal montoBeneficio;

    private java.lang.String numeroAutorizacion;

    private java.lang.String numeroTarjeta;

    private java.math.BigDecimal precio;

    private java.lang.String promoId;

    private java.lang.String sku;

    private java.lang.String tiendaDestino;

    private java.lang.String tipoEnvio;

    private java.lang.String tipoProducto;

    private java.math.BigDecimal totalArticulos;

    private java.lang.String tipoBeneficio;

    private java.lang.String promoType;

    private java.lang.String descripcionPromo;

    private java.math.BigDecimal porcentajeDescuentoBonificacion;

    private java.lang.String disparador;

    private java.lang.Integer rangoMontoPromocional;

    private java.lang.String plazosPago;

    private com.addcel.ws.clientes.enigma.magento.RespuestaDetallePromosByPedidoItem[] itemPromo;

    public RespuestaConsultarPedidoItem() {
    }

    public RespuestaConsultarPedidoItem(
           java.lang.String esPesable,
           java.math.BigDecimal montoBeneficio,
           java.lang.String numeroAutorizacion,
           java.lang.String numeroTarjeta,
           java.math.BigDecimal precio,
           java.lang.String promoId,
           java.lang.String sku,
           java.lang.String tiendaDestino,
           java.lang.String tipoEnvio,
           java.lang.String tipoProducto,
           java.math.BigDecimal totalArticulos,
           java.lang.String tipoBeneficio,
           java.lang.String promoType,
           java.lang.String descripcionPromo,
           java.math.BigDecimal porcentajeDescuentoBonificacion,
           java.lang.String disparador,
           java.lang.Integer rangoMontoPromocional,
           java.lang.String plazosPago,
           com.addcel.ws.clientes.enigma.magento.RespuestaDetallePromosByPedidoItem[] itemPromo) {
           this.esPesable = esPesable;
           this.montoBeneficio = montoBeneficio;
           this.numeroAutorizacion = numeroAutorizacion;
           this.numeroTarjeta = numeroTarjeta;
           this.precio = precio;
           this.promoId = promoId;
           this.sku = sku;
           this.tiendaDestino = tiendaDestino;
           this.tipoEnvio = tipoEnvio;
           this.tipoProducto = tipoProducto;
           this.totalArticulos = totalArticulos;
           this.tipoBeneficio = tipoBeneficio;
           this.promoType = promoType;
           this.descripcionPromo = descripcionPromo;
           this.porcentajeDescuentoBonificacion = porcentajeDescuentoBonificacion;
           this.disparador = disparador;
           this.rangoMontoPromocional = rangoMontoPromocional;
           this.plazosPago = plazosPago;
           this.itemPromo = itemPromo;
    }


    /**
     * Gets the esPesable value for this RespuestaConsultarPedidoItem.
     * 
     * @return esPesable
     */
    public java.lang.String getEsPesable() {
        return esPesable;
    }


    /**
     * Sets the esPesable value for this RespuestaConsultarPedidoItem.
     * 
     * @param esPesable
     */
    public void setEsPesable(java.lang.String esPesable) {
        this.esPesable = esPesable;
    }


    /**
     * Gets the montoBeneficio value for this RespuestaConsultarPedidoItem.
     * 
     * @return montoBeneficio
     */
    public java.math.BigDecimal getMontoBeneficio() {
        return montoBeneficio;
    }


    /**
     * Sets the montoBeneficio value for this RespuestaConsultarPedidoItem.
     * 
     * @param montoBeneficio
     */
    public void setMontoBeneficio(java.math.BigDecimal montoBeneficio) {
        this.montoBeneficio = montoBeneficio;
    }


    /**
     * Gets the numeroAutorizacion value for this RespuestaConsultarPedidoItem.
     * 
     * @return numeroAutorizacion
     */
    public java.lang.String getNumeroAutorizacion() {
        return numeroAutorizacion;
    }


    /**
     * Sets the numeroAutorizacion value for this RespuestaConsultarPedidoItem.
     * 
     * @param numeroAutorizacion
     */
    public void setNumeroAutorizacion(java.lang.String numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }


    /**
     * Gets the numeroTarjeta value for this RespuestaConsultarPedidoItem.
     * 
     * @return numeroTarjeta
     */
    public java.lang.String getNumeroTarjeta() {
        return numeroTarjeta;
    }


    /**
     * Sets the numeroTarjeta value for this RespuestaConsultarPedidoItem.
     * 
     * @param numeroTarjeta
     */
    public void setNumeroTarjeta(java.lang.String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }


    /**
     * Gets the precio value for this RespuestaConsultarPedidoItem.
     * 
     * @return precio
     */
    public java.math.BigDecimal getPrecio() {
        return precio;
    }


    /**
     * Sets the precio value for this RespuestaConsultarPedidoItem.
     * 
     * @param precio
     */
    public void setPrecio(java.math.BigDecimal precio) {
        this.precio = precio;
    }


    /**
     * Gets the promoId value for this RespuestaConsultarPedidoItem.
     * 
     * @return promoId
     */
    public java.lang.String getPromoId() {
        return promoId;
    }


    /**
     * Sets the promoId value for this RespuestaConsultarPedidoItem.
     * 
     * @param promoId
     */
    public void setPromoId(java.lang.String promoId) {
        this.promoId = promoId;
    }


    /**
     * Gets the sku value for this RespuestaConsultarPedidoItem.
     * 
     * @return sku
     */
    public java.lang.String getSku() {
        return sku;
    }


    /**
     * Sets the sku value for this RespuestaConsultarPedidoItem.
     * 
     * @param sku
     */
    public void setSku(java.lang.String sku) {
        this.sku = sku;
    }


    /**
     * Gets the tiendaDestino value for this RespuestaConsultarPedidoItem.
     * 
     * @return tiendaDestino
     */
    public java.lang.String getTiendaDestino() {
        return tiendaDestino;
    }


    /**
     * Sets the tiendaDestino value for this RespuestaConsultarPedidoItem.
     * 
     * @param tiendaDestino
     */
    public void setTiendaDestino(java.lang.String tiendaDestino) {
        this.tiendaDestino = tiendaDestino;
    }


    /**
     * Gets the tipoEnvio value for this RespuestaConsultarPedidoItem.
     * 
     * @return tipoEnvio
     */
    public java.lang.String getTipoEnvio() {
        return tipoEnvio;
    }


    /**
     * Sets the tipoEnvio value for this RespuestaConsultarPedidoItem.
     * 
     * @param tipoEnvio
     */
    public void setTipoEnvio(java.lang.String tipoEnvio) {
        this.tipoEnvio = tipoEnvio;
    }


    /**
     * Gets the tipoProducto value for this RespuestaConsultarPedidoItem.
     * 
     * @return tipoProducto
     */
    public java.lang.String getTipoProducto() {
        return tipoProducto;
    }


    /**
     * Sets the tipoProducto value for this RespuestaConsultarPedidoItem.
     * 
     * @param tipoProducto
     */
    public void setTipoProducto(java.lang.String tipoProducto) {
        this.tipoProducto = tipoProducto;
    }


    /**
     * Gets the totalArticulos value for this RespuestaConsultarPedidoItem.
     * 
     * @return totalArticulos
     */
    public java.math.BigDecimal getTotalArticulos() {
        return totalArticulos;
    }


    /**
     * Sets the totalArticulos value for this RespuestaConsultarPedidoItem.
     * 
     * @param totalArticulos
     */
    public void setTotalArticulos(java.math.BigDecimal totalArticulos) {
        this.totalArticulos = totalArticulos;
    }


    /**
     * Gets the tipoBeneficio value for this RespuestaConsultarPedidoItem.
     * 
     * @return tipoBeneficio
     */
    public java.lang.String getTipoBeneficio() {
        return tipoBeneficio;
    }


    /**
     * Sets the tipoBeneficio value for this RespuestaConsultarPedidoItem.
     * 
     * @param tipoBeneficio
     */
    public void setTipoBeneficio(java.lang.String tipoBeneficio) {
        this.tipoBeneficio = tipoBeneficio;
    }


    /**
     * Gets the promoType value for this RespuestaConsultarPedidoItem.
     * 
     * @return promoType
     */
    public java.lang.String getPromoType() {
        return promoType;
    }


    /**
     * Sets the promoType value for this RespuestaConsultarPedidoItem.
     * 
     * @param promoType
     */
    public void setPromoType(java.lang.String promoType) {
        this.promoType = promoType;
    }


    /**
     * Gets the descripcionPromo value for this RespuestaConsultarPedidoItem.
     * 
     * @return descripcionPromo
     */
    public java.lang.String getDescripcionPromo() {
        return descripcionPromo;
    }


    /**
     * Sets the descripcionPromo value for this RespuestaConsultarPedidoItem.
     * 
     * @param descripcionPromo
     */
    public void setDescripcionPromo(java.lang.String descripcionPromo) {
        this.descripcionPromo = descripcionPromo;
    }


    /**
     * Gets the porcentajeDescuentoBonificacion value for this RespuestaConsultarPedidoItem.
     * 
     * @return porcentajeDescuentoBonificacion
     */
    public java.math.BigDecimal getPorcentajeDescuentoBonificacion() {
        return porcentajeDescuentoBonificacion;
    }


    /**
     * Sets the porcentajeDescuentoBonificacion value for this RespuestaConsultarPedidoItem.
     * 
     * @param porcentajeDescuentoBonificacion
     */
    public void setPorcentajeDescuentoBonificacion(java.math.BigDecimal porcentajeDescuentoBonificacion) {
        this.porcentajeDescuentoBonificacion = porcentajeDescuentoBonificacion;
    }


    /**
     * Gets the disparador value for this RespuestaConsultarPedidoItem.
     * 
     * @return disparador
     */
    public java.lang.String getDisparador() {
        return disparador;
    }


    /**
     * Sets the disparador value for this RespuestaConsultarPedidoItem.
     * 
     * @param disparador
     */
    public void setDisparador(java.lang.String disparador) {
        this.disparador = disparador;
    }


    /**
     * Gets the rangoMontoPromocional value for this RespuestaConsultarPedidoItem.
     * 
     * @return rangoMontoPromocional
     */
    public java.lang.Integer getRangoMontoPromocional() {
        return rangoMontoPromocional;
    }


    /**
     * Sets the rangoMontoPromocional value for this RespuestaConsultarPedidoItem.
     * 
     * @param rangoMontoPromocional
     */
    public void setRangoMontoPromocional(java.lang.Integer rangoMontoPromocional) {
        this.rangoMontoPromocional = rangoMontoPromocional;
    }


    /**
     * Gets the plazosPago value for this RespuestaConsultarPedidoItem.
     * 
     * @return plazosPago
     */
    public java.lang.String getPlazosPago() {
        return plazosPago;
    }


    /**
     * Sets the plazosPago value for this RespuestaConsultarPedidoItem.
     * 
     * @param plazosPago
     */
    public void setPlazosPago(java.lang.String plazosPago) {
        this.plazosPago = plazosPago;
    }


    /**
     * Gets the itemPromo value for this RespuestaConsultarPedidoItem.
     * 
     * @return itemPromo
     */
    public com.addcel.ws.clientes.enigma.magento.RespuestaDetallePromosByPedidoItem[] getItemPromo() {
        return itemPromo;
    }


    /**
     * Sets the itemPromo value for this RespuestaConsultarPedidoItem.
     * 
     * @param itemPromo
     */
    public void setItemPromo(com.addcel.ws.clientes.enigma.magento.RespuestaDetallePromosByPedidoItem[] itemPromo) {
        this.itemPromo = itemPromo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RespuestaConsultarPedidoItem)) return false;
        RespuestaConsultarPedidoItem other = (RespuestaConsultarPedidoItem) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.esPesable==null && other.getEsPesable()==null) || 
             (this.esPesable!=null &&
              this.esPesable.equals(other.getEsPesable()))) &&
            ((this.montoBeneficio==null && other.getMontoBeneficio()==null) || 
             (this.montoBeneficio!=null &&
              this.montoBeneficio.equals(other.getMontoBeneficio()))) &&
            ((this.numeroAutorizacion==null && other.getNumeroAutorizacion()==null) || 
             (this.numeroAutorizacion!=null &&
              this.numeroAutorizacion.equals(other.getNumeroAutorizacion()))) &&
            ((this.numeroTarjeta==null && other.getNumeroTarjeta()==null) || 
             (this.numeroTarjeta!=null &&
              this.numeroTarjeta.equals(other.getNumeroTarjeta()))) &&
            ((this.precio==null && other.getPrecio()==null) || 
             (this.precio!=null &&
              this.precio.equals(other.getPrecio()))) &&
            ((this.promoId==null && other.getPromoId()==null) || 
             (this.promoId!=null &&
              this.promoId.equals(other.getPromoId()))) &&
            ((this.sku==null && other.getSku()==null) || 
             (this.sku!=null &&
              this.sku.equals(other.getSku()))) &&
            ((this.tiendaDestino==null && other.getTiendaDestino()==null) || 
             (this.tiendaDestino!=null &&
              this.tiendaDestino.equals(other.getTiendaDestino()))) &&
            ((this.tipoEnvio==null && other.getTipoEnvio()==null) || 
             (this.tipoEnvio!=null &&
              this.tipoEnvio.equals(other.getTipoEnvio()))) &&
            ((this.tipoProducto==null && other.getTipoProducto()==null) || 
             (this.tipoProducto!=null &&
              this.tipoProducto.equals(other.getTipoProducto()))) &&
            ((this.totalArticulos==null && other.getTotalArticulos()==null) || 
             (this.totalArticulos!=null &&
              this.totalArticulos.equals(other.getTotalArticulos()))) &&
            ((this.tipoBeneficio==null && other.getTipoBeneficio()==null) || 
             (this.tipoBeneficio!=null &&
              this.tipoBeneficio.equals(other.getTipoBeneficio()))) &&
            ((this.promoType==null && other.getPromoType()==null) || 
             (this.promoType!=null &&
              this.promoType.equals(other.getPromoType()))) &&
            ((this.descripcionPromo==null && other.getDescripcionPromo()==null) || 
             (this.descripcionPromo!=null &&
              this.descripcionPromo.equals(other.getDescripcionPromo()))) &&
            ((this.porcentajeDescuentoBonificacion==null && other.getPorcentajeDescuentoBonificacion()==null) || 
             (this.porcentajeDescuentoBonificacion!=null &&
              this.porcentajeDescuentoBonificacion.equals(other.getPorcentajeDescuentoBonificacion()))) &&
            ((this.disparador==null && other.getDisparador()==null) || 
             (this.disparador!=null &&
              this.disparador.equals(other.getDisparador()))) &&
            ((this.rangoMontoPromocional==null && other.getRangoMontoPromocional()==null) || 
             (this.rangoMontoPromocional!=null &&
              this.rangoMontoPromocional.equals(other.getRangoMontoPromocional()))) &&
            ((this.plazosPago==null && other.getPlazosPago()==null) || 
             (this.plazosPago!=null &&
              this.plazosPago.equals(other.getPlazosPago()))) &&
            ((this.itemPromo==null && other.getItemPromo()==null) || 
             (this.itemPromo!=null &&
              java.util.Arrays.equals(this.itemPromo, other.getItemPromo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEsPesable() != null) {
            _hashCode += getEsPesable().hashCode();
        }
        if (getMontoBeneficio() != null) {
            _hashCode += getMontoBeneficio().hashCode();
        }
        if (getNumeroAutorizacion() != null) {
            _hashCode += getNumeroAutorizacion().hashCode();
        }
        if (getNumeroTarjeta() != null) {
            _hashCode += getNumeroTarjeta().hashCode();
        }
        if (getPrecio() != null) {
            _hashCode += getPrecio().hashCode();
        }
        if (getPromoId() != null) {
            _hashCode += getPromoId().hashCode();
        }
        if (getSku() != null) {
            _hashCode += getSku().hashCode();
        }
        if (getTiendaDestino() != null) {
            _hashCode += getTiendaDestino().hashCode();
        }
        if (getTipoEnvio() != null) {
            _hashCode += getTipoEnvio().hashCode();
        }
        if (getTipoProducto() != null) {
            _hashCode += getTipoProducto().hashCode();
        }
        if (getTotalArticulos() != null) {
            _hashCode += getTotalArticulos().hashCode();
        }
        if (getTipoBeneficio() != null) {
            _hashCode += getTipoBeneficio().hashCode();
        }
        if (getPromoType() != null) {
            _hashCode += getPromoType().hashCode();
        }
        if (getDescripcionPromo() != null) {
            _hashCode += getDescripcionPromo().hashCode();
        }
        if (getPorcentajeDescuentoBonificacion() != null) {
            _hashCode += getPorcentajeDescuentoBonificacion().hashCode();
        }
        if (getDisparador() != null) {
            _hashCode += getDisparador().hashCode();
        }
        if (getRangoMontoPromocional() != null) {
            _hashCode += getRangoMontoPromocional().hashCode();
        }
        if (getPlazosPago() != null) {
            _hashCode += getPlazosPago().hashCode();
        }
        if (getItemPromo() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getItemPromo());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getItemPromo(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RespuestaConsultarPedidoItem.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "RespuestaConsultarPedidoItem"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("esPesable");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "esPesable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("montoBeneficio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "montoBeneficio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroAutorizacion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "numeroAutorizacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroTarjeta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "numeroTarjeta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("precio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "precio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("promoId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "promoId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sku");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "sku"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tiendaDestino");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "tiendaDestino"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoEnvio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "tipoEnvio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoProducto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "tipoProducto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalArticulos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "totalArticulos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoBeneficio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "tipoBeneficio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("promoType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "promoType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descripcionPromo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "descripcionPromo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("porcentajeDescuentoBonificacion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "porcentajeDescuentoBonificacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("disparador");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "disparador"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rangoMontoPromocional");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "rangoMontoPromocional"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("plazosPago");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "plazosPago"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemPromo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "itemPromo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "RespuestaDetallePromosByPedidoItem"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "item"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
