/**
 * RespuestaConsultarPedidoCupones.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.enigma.magento;

public class RespuestaConsultarPedidoCupones  implements java.io.Serializable {
    private java.lang.String numeroCupon;

    private java.lang.String accionCupon;

    private java.lang.String oferta;

    private java.lang.String promoType;

    private java.lang.String montoCupon;

    private java.lang.String tipoCupon;

    private java.lang.String estado;

    private java.lang.String articulo;

    private java.lang.String descripcionCupon;

    public RespuestaConsultarPedidoCupones() {
    }

    public RespuestaConsultarPedidoCupones(
           java.lang.String numeroCupon,
           java.lang.String accionCupon,
           java.lang.String oferta,
           java.lang.String promoType,
           java.lang.String montoCupon,
           java.lang.String tipoCupon,
           java.lang.String estado,
           java.lang.String articulo,
           java.lang.String descripcionCupon) {
           this.numeroCupon = numeroCupon;
           this.accionCupon = accionCupon;
           this.oferta = oferta;
           this.promoType = promoType;
           this.montoCupon = montoCupon;
           this.tipoCupon = tipoCupon;
           this.estado = estado;
           this.articulo = articulo;
           this.descripcionCupon = descripcionCupon;
    }


    /**
     * Gets the numeroCupon value for this RespuestaConsultarPedidoCupones.
     * 
     * @return numeroCupon
     */
    public java.lang.String getNumeroCupon() {
        return numeroCupon;
    }


    /**
     * Sets the numeroCupon value for this RespuestaConsultarPedidoCupones.
     * 
     * @param numeroCupon
     */
    public void setNumeroCupon(java.lang.String numeroCupon) {
        this.numeroCupon = numeroCupon;
    }


    /**
     * Gets the accionCupon value for this RespuestaConsultarPedidoCupones.
     * 
     * @return accionCupon
     */
    public java.lang.String getAccionCupon() {
        return accionCupon;
    }


    /**
     * Sets the accionCupon value for this RespuestaConsultarPedidoCupones.
     * 
     * @param accionCupon
     */
    public void setAccionCupon(java.lang.String accionCupon) {
        this.accionCupon = accionCupon;
    }


    /**
     * Gets the oferta value for this RespuestaConsultarPedidoCupones.
     * 
     * @return oferta
     */
    public java.lang.String getOferta() {
        return oferta;
    }


    /**
     * Sets the oferta value for this RespuestaConsultarPedidoCupones.
     * 
     * @param oferta
     */
    public void setOferta(java.lang.String oferta) {
        this.oferta = oferta;
    }


    /**
     * Gets the promoType value for this RespuestaConsultarPedidoCupones.
     * 
     * @return promoType
     */
    public java.lang.String getPromoType() {
        return promoType;
    }


    /**
     * Sets the promoType value for this RespuestaConsultarPedidoCupones.
     * 
     * @param promoType
     */
    public void setPromoType(java.lang.String promoType) {
        this.promoType = promoType;
    }


    /**
     * Gets the montoCupon value for this RespuestaConsultarPedidoCupones.
     * 
     * @return montoCupon
     */
    public java.lang.String getMontoCupon() {
        return montoCupon;
    }


    /**
     * Sets the montoCupon value for this RespuestaConsultarPedidoCupones.
     * 
     * @param montoCupon
     */
    public void setMontoCupon(java.lang.String montoCupon) {
        this.montoCupon = montoCupon;
    }


    /**
     * Gets the tipoCupon value for this RespuestaConsultarPedidoCupones.
     * 
     * @return tipoCupon
     */
    public java.lang.String getTipoCupon() {
        return tipoCupon;
    }


    /**
     * Sets the tipoCupon value for this RespuestaConsultarPedidoCupones.
     * 
     * @param tipoCupon
     */
    public void setTipoCupon(java.lang.String tipoCupon) {
        this.tipoCupon = tipoCupon;
    }


    /**
     * Gets the estado value for this RespuestaConsultarPedidoCupones.
     * 
     * @return estado
     */
    public java.lang.String getEstado() {
        return estado;
    }


    /**
     * Sets the estado value for this RespuestaConsultarPedidoCupones.
     * 
     * @param estado
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }


    /**
     * Gets the articulo value for this RespuestaConsultarPedidoCupones.
     * 
     * @return articulo
     */
    public java.lang.String getArticulo() {
        return articulo;
    }


    /**
     * Sets the articulo value for this RespuestaConsultarPedidoCupones.
     * 
     * @param articulo
     */
    public void setArticulo(java.lang.String articulo) {
        this.articulo = articulo;
    }


    /**
     * Gets the descripcionCupon value for this RespuestaConsultarPedidoCupones.
     * 
     * @return descripcionCupon
     */
    public java.lang.String getDescripcionCupon() {
        return descripcionCupon;
    }


    /**
     * Sets the descripcionCupon value for this RespuestaConsultarPedidoCupones.
     * 
     * @param descripcionCupon
     */
    public void setDescripcionCupon(java.lang.String descripcionCupon) {
        this.descripcionCupon = descripcionCupon;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RespuestaConsultarPedidoCupones)) return false;
        RespuestaConsultarPedidoCupones other = (RespuestaConsultarPedidoCupones) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.numeroCupon==null && other.getNumeroCupon()==null) || 
             (this.numeroCupon!=null &&
              this.numeroCupon.equals(other.getNumeroCupon()))) &&
            ((this.accionCupon==null && other.getAccionCupon()==null) || 
             (this.accionCupon!=null &&
              this.accionCupon.equals(other.getAccionCupon()))) &&
            ((this.oferta==null && other.getOferta()==null) || 
             (this.oferta!=null &&
              this.oferta.equals(other.getOferta()))) &&
            ((this.promoType==null && other.getPromoType()==null) || 
             (this.promoType!=null &&
              this.promoType.equals(other.getPromoType()))) &&
            ((this.montoCupon==null && other.getMontoCupon()==null) || 
             (this.montoCupon!=null &&
              this.montoCupon.equals(other.getMontoCupon()))) &&
            ((this.tipoCupon==null && other.getTipoCupon()==null) || 
             (this.tipoCupon!=null &&
              this.tipoCupon.equals(other.getTipoCupon()))) &&
            ((this.estado==null && other.getEstado()==null) || 
             (this.estado!=null &&
              this.estado.equals(other.getEstado()))) &&
            ((this.articulo==null && other.getArticulo()==null) || 
             (this.articulo!=null &&
              this.articulo.equals(other.getArticulo()))) &&
            ((this.descripcionCupon==null && other.getDescripcionCupon()==null) || 
             (this.descripcionCupon!=null &&
              this.descripcionCupon.equals(other.getDescripcionCupon())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNumeroCupon() != null) {
            _hashCode += getNumeroCupon().hashCode();
        }
        if (getAccionCupon() != null) {
            _hashCode += getAccionCupon().hashCode();
        }
        if (getOferta() != null) {
            _hashCode += getOferta().hashCode();
        }
        if (getPromoType() != null) {
            _hashCode += getPromoType().hashCode();
        }
        if (getMontoCupon() != null) {
            _hashCode += getMontoCupon().hashCode();
        }
        if (getTipoCupon() != null) {
            _hashCode += getTipoCupon().hashCode();
        }
        if (getEstado() != null) {
            _hashCode += getEstado().hashCode();
        }
        if (getArticulo() != null) {
            _hashCode += getArticulo().hashCode();
        }
        if (getDescripcionCupon() != null) {
            _hashCode += getDescripcionCupon().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RespuestaConsultarPedidoCupones.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "RespuestaConsultarPedidoCupones"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroCupon");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "numeroCupon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accionCupon");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "accionCupon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oferta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "oferta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("promoType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "promoType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("montoCupon");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "montoCupon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoCupon");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "tipoCupon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "estado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("articulo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "articulo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descripcionCupon");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "descripcionCupon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
