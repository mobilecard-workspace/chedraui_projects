/**
 * RespuestaDetallePromosByPedidoItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.enigma.magento;

public class RespuestaDetallePromosByPedidoItem  implements java.io.Serializable {
    private java.lang.String promoId;

    private java.math.BigDecimal montoBeneficio;

    public RespuestaDetallePromosByPedidoItem() {
    }

    public RespuestaDetallePromosByPedidoItem(
           java.lang.String promoId,
           java.math.BigDecimal montoBeneficio) {
           this.promoId = promoId;
           this.montoBeneficio = montoBeneficio;
    }


    /**
     * Gets the promoId value for this RespuestaDetallePromosByPedidoItem.
     * 
     * @return promoId
     */
    public java.lang.String getPromoId() {
        return promoId;
    }


    /**
     * Sets the promoId value for this RespuestaDetallePromosByPedidoItem.
     * 
     * @param promoId
     */
    public void setPromoId(java.lang.String promoId) {
        this.promoId = promoId;
    }


    /**
     * Gets the montoBeneficio value for this RespuestaDetallePromosByPedidoItem.
     * 
     * @return montoBeneficio
     */
    public java.math.BigDecimal getMontoBeneficio() {
        return montoBeneficio;
    }


    /**
     * Sets the montoBeneficio value for this RespuestaDetallePromosByPedidoItem.
     * 
     * @param montoBeneficio
     */
    public void setMontoBeneficio(java.math.BigDecimal montoBeneficio) {
        this.montoBeneficio = montoBeneficio;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RespuestaDetallePromosByPedidoItem)) return false;
        RespuestaDetallePromosByPedidoItem other = (RespuestaDetallePromosByPedidoItem) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.promoId==null && other.getPromoId()==null) || 
             (this.promoId!=null &&
              this.promoId.equals(other.getPromoId()))) &&
            ((this.montoBeneficio==null && other.getMontoBeneficio()==null) || 
             (this.montoBeneficio!=null &&
              this.montoBeneficio.equals(other.getMontoBeneficio())));
        __equalsCalc = null;
        return _equals;
    }
    
    @Override
    public String toString() {
    	// TODO Auto-generated method stub
    	return "{\"promoId\":\""+promoId+"\",\"montoBeneficio\":"+montoBeneficio.doubleValue()+"}";
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPromoId() != null) {
            _hashCode += getPromoId().hashCode();
        }
        if (getMontoBeneficio() != null) {
            _hashCode += getMontoBeneficio().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RespuestaDetallePromosByPedidoItem.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "RespuestaDetallePromosByPedidoItem"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("promoId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "promoId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("montoBeneficio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://services.magento.entrepids.com", "montoBeneficio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
