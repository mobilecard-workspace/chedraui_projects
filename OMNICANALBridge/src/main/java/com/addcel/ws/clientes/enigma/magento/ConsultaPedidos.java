/**
 * ConsultaPedidos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.enigma.magento;

public interface ConsultaPedidos extends java.rmi.Remote {
    public java.lang.String actualizaDevolucion(java.lang.String numeroPedido, java.lang.String firmaElectronica, java.lang.String[] skus, java.lang.String[] numero) throws java.rmi.RemoteException;
    public com.addcel.ws.clientes.enigma.magento.RespuestaConsultarPedido consultar(java.lang.String numeroPedido, java.lang.String firmaElectronica) throws java.rmi.RemoteException;
    public java.lang.String actualizaTiendaPago(java.lang.String numeroPedido, java.lang.String firmaElectronica, java.lang.String idTiendaPago) throws java.rmi.RemoteException;
    public java.lang.String actualizaStatus(java.lang.String numeroPedido, java.lang.String firmaElectronica, java.lang.String estatus) throws java.rmi.RemoteException;
}
