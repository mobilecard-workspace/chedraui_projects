package com.addcel.chedraui.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.chedraui.model.mapper.AdmonMapper;
import com.addcel.chedraui.model.mapper.PortalMapper;
import com.addcel.chedraui.model.vo.AbstractVO;
import com.addcel.chedraui.model.vo.admon.LoginVO;
import com.addcel.chedraui.model.vo.admon.TiendaVO;
import com.addcel.chedraui.model.vo.admon.UsuarioVO;
import com.addcel.chedraui.model.vo.portal.SearchDataResponse;
import com.addcel.chedraui.model.vo.portal.TiendaDataRequest;
import com.addcel.chedraui.model.vo.portal.UsuariosDataRequest;
import com.addcel.chedraui.model.vo.response.LoginRespuesta;
import com.addcel.chedraui.utils.AddCelGenericMail;
import com.addcel.chedraui.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;

@Service
public class ChedrauiPortalService {
	private static final Logger logger = LoggerFactory.getLogger(ChedrauiPortalService.class);
	
	@Autowired
	private PortalMapper mapperPortal;
	
	@Autowired
	private AdmonMapper mapperAdm;
	
	@Autowired
	private UtilsService utilService;	
	
	public ModelAndView loginFinal( LoginVO login, ModelMap modelo) {
		ModelAndView mav = null;
		LoginRespuesta lr = null;
			
		try{
			logger.debug("Iniciar session login: {}",login.getLogin() );
			
			LoginRespuesta user = mapperAdm.loginUsuario(0, null, null, null, login.getLogin(), 0, 0);	
			login.setPassword(AddcelCrypto.encryptPassword(login.getPassword()));
			
			if(user == null){			
				lr = new LoginRespuesta(1, "Usuario o Contraseña incorrectos.");
				
			}else if(user.getStatus() == 3){
				lr = new LoginRespuesta(3, "Usuario bloqueado, favor de contactar con su Supervisor");
				logger.info("Usuario bloqueado id ==> {}", login.getLogin());
				
			}else if(user.getStatus() == 99){
				lr = new LoginRespuesta(4, "Se requiere cambio de contraseña, favor de contactar con su Supervisor.");
				logger.info("Usuario dado de baja id ==> {}", login.getLogin());
				
			}else if(user.getStatus() != 1 && user.getStatus() != 99){
				lr = new LoginRespuesta(4, "Status no valido, favor de contactar con su Supervisor.");
				logger.info("Status no valido id ==> {}", login.getLogin());
				
			}	else {
				if(user.getStatus() == 1 ){								
					if(user.getPassword().equals(login.password)){
						mapperAdm.updateIntentosStatus(user.getIdUsuario(), 0, "contadorLogin");//Reset loginCount=0
						lr = user;
						lr.setMensajeError("Usuario valido.");
					}else{												
						int contador = user.getLoginCount()+1;						
						if((user.getRole().getIdRol() == 101 && contador > 7) || (user.getRole().getIdRol() == 102 && contador > 5)){
							//se difine el numero maximo de intentos por rol trae el numero maximo de intentos por rol
//								logger.debug("intentos ==> {} = max_login ==> {}",contador,user.getIdTienda());
							//bloquear usuario
							mapperAdm.updateIntentosStatus(user.getIdUsuario(), 3, "status");  //Status 3 = bloqueado
							mapperAdm.updateIntentosStatus(user.getIdUsuario(), 0, "contador"); //Reset loginCount = 0		
							
							lr = new LoginRespuesta(4, "El usuario ha sido bloqueado, favor de contactar con su Supervisor.");
							logger.info("# de intentos superado. El usuario con id ==> {} ha sido bloqueado ",user.getIdUsuario());
						}else{							
							mapperAdm.updateIntentosStatus(user.getIdUsuario(), contador, "contador");//incrementa el contador							
							lr = new LoginRespuesta(5, "Contraseña incorrecta, intento: " + contador +
									" de " + (user.getRole().getIdRol() == 101?"7":"5"));
						}
					}									
				}
			}	
			
			if(lr.getIdError() == 0){
				mav = new ModelAndView("portal/welcome");
				//Se sube a session el objeto Usuario
				modelo.put("usuario", lr);
				mav.addObject("usuario", lr);
				
			}else{
				login.setPassword("");
				mav = new ModelAndView("portal/login", "usuario", login);
				mav.addObject("mensaje", lr.getMensajeError());
			}
			
		}catch(Exception e){
			logger.error("Ocurrio un error general en el servicio /loginFinal: {}", e );
			mav = new ModelAndView("error");
			mav.addObject("mensaje", "Ocurrio un error general: " + e.getMessage());
		}
		return mav;	
	}
	
	public  ModelAndView tiendaAltaFinal( TiendaVO tienda, ModelMap modelo ) {
		ModelAndView mav = null;
		AbstractVO resp = null;
		List<TiendaVO> tiendas = null;
		LoginRespuesta user = null;
		try{
			user = (LoginRespuesta) modelo.get("usuario");
			mav = new ModelAndView("portal/tiendaAlta", "tienda", tienda);
			
			if(user.getRole().getIdRol() == 100 || user.getRole().getIdRol() == 101){
				tiendas = mapperPortal.buscarTienda(0, tienda.getCodigo(), null);
				
				if(tiendas != null && tiendas.size() > 0){
					resp = new AbstractVO(201, "El numero de Codigo de Tienda ya existe, codigo: " + tienda.getCodigo() );
					
				}else {
					tiendas = mapperPortal.buscarTienda(0, null, tienda.getDescripcion());
					
					if(tiendas != null && tiendas.size() > 0){
						resp = new AbstractVO(201, "El Nombre de Tienda ya existe, nombre: " + tienda.getDescripcion() );
						
					}else {
						mapperPortal.guardarTienda(tienda);
						if(tienda.getIdTienda() == 0){
							resp = new AbstractVO(0, "Ocurrio un error al guardar la Tienda en BD.");
							
						}else{
							logger.debug("IdTienda: {}", tienda.getIdTienda());
							resp = new AbstractVO(0, "Tienda guardada exitosamente.");
							mav = new ModelAndView("portal/tiendaAlta", "tienda", new TiendaVO());
						}
					}
				}
			}else{
				resp = new AbstractVO(0, "El usuario no tiene permisos para dar de Alta Tiendas.");
			}
			
		}catch(Exception e){
			mav = new ModelAndView("error");
			logger.error("Ocurrio un error general en el servicio /portal/busquedaTienda: {}", e );
			mav.addObject("mensaje", "Ocurrio un error al guardar la Tienda: " + e.getMessage() );
		}finally{
			mav.addObject("mensaje", resp.getMensajeError());
		}
		return mav;	
	}
	
	public String buscarTiendasPortal(TiendaDataRequest busquedaTienda, ModelMap modelo ){				
		SearchDataResponse resp = new SearchDataResponse();
		String json = null;
		LoginRespuesta user = null;
			
		try {
			user = (LoginRespuesta) modelo.get("usuario");
			if(user.getRole().getIdRol() == 100 ){
				busquedaTienda.setIdTienda(0);
			}else if(user.getRole().getIdRol() == 101 || user.getRole().getIdRol() == 102 ){
				busquedaTienda.setIdTienda(user.getTienda().getIdTienda());
			}else{
				busquedaTienda.setIdTienda(-1);
			}
			
				
			busquedaTienda.setNombreT(busquedaTienda.getNombreT()!= null && !"".equals(busquedaTienda.getNombreT())?busquedaTienda.getNombreT()+"%":"");
			busquedaTienda.setUrlT(busquedaTienda.getUrlT()!= null && !"".equals(busquedaTienda.getUrlT())?busquedaTienda.getUrlT()+"%":"");
			
			resp.setData(mapperPortal.filtroTiendas(busquedaTienda));
			
			if(resp.getData() != null && resp.getData().size() > 0){
				resp.setTotalRecords(resp.getData().size() +1);
				resp.setCurPage(1);

			}else{
				resp.setTotalRecords(resp.getData().size() +1);
				resp.setCurPage(1);
				resp.setData(new ArrayList<TiendaVO>());
				resp.getData().add(new TiendaVO());
				((TiendaVO)resp.getData().get(0)).setIdTienda(-1001);
				((TiendaVO)resp.getData().get(0)).setDescripcion("No existen tiendas, favor de validar filtros.");
			}
			
//			}else{
//				resp.setTotalRecords(0);
//				resp.setCurPage(1);
//				resp.setData(new ArrayList<PaymentData>());
//				resp.getData().add(new PaymentData());
//				resp.getData().get(0).setIdBitacora(-1);
//				resp.getData().get(0).setStatus("");
//				resp.getData().get(0).setCompany(ConstantesEuropaWeb.ERROR_NO_SESSION);
//			}
			
		}catch (Exception pe) {
			logger.error("Ocurrio un error general al buscar tiendas desde el portal: {}", pe.getMessage());
			resp.setTotalRecords(0);
			resp.setCurPage(1);
			resp.setData(new ArrayList<TiendaVO>());
			resp.getData().add(new TiendaVO());
			((TiendaVO)resp.getData().get(0)).setIdTienda(-1000);
			((TiendaVO)resp.getData().get(0)).setDescripcion("Error en busqueda tiendas: " + pe.getMessage());
		}finally{
			json = utilService.objectToJson(resp);
		}
		return json;
	}
	
	public String actualizarTienda(TiendaVO tienda, ModelMap modelo ){				
		AbstractVO resp = new AbstractVO();
		List<TiendaVO> tiendas = null;
		String json = null;
		LoginRespuesta user = null;
		
		try {
			user = (LoginRespuesta) modelo.get("usuario");
			
			if(user.getRole().getIdRol() == 100 || user.getRole().getIdRol() == 101){
				tiendas = mapperPortal.buscarTienda(tienda.getIdTienda(), null, null);
				
				if(tiendas != null && tiendas.size() > 0){
					mapperPortal.actualizarTienda(tienda);
					resp = new AbstractVO(0, "Tienda actualizada correctamente.");
				}else{
					resp = new AbstractVO(-1, "La tienda no existe, favor de validar la informacion.");
				}
			}else{
				resp = new AbstractVO(-1, "Usuario no tiene permisos para modificar la tienda.");
			}
			
			
		}catch (Exception pe) {
			logger.error("Ocurrio un error general al buscar tiendas desde el portal: {}", pe.getMessage());
			resp = new AbstractVO(-1, "Error al actuliazar tienda: " + pe.getMessage());
		}finally{
			json = utilService.objectToJson(resp);
		}
		return json;
	}
	
	public String buscarUsuariosPortal(UsuariosDataRequest busquedaUsuario, ModelMap modelo ){				
		SearchDataResponse resp = new SearchDataResponse();
		String json = null;
		LoginRespuesta user = null;
			
		try {
			user = (LoginRespuesta) modelo.get("usuario");
			if(user.getRole().getIdRol() == 100 ){
				busquedaUsuario.setIdUsuarioT(0);
				busquedaUsuario.setIdSupervisorT(0);
			}else if(user.getRole().getIdRol() == 101){
				busquedaUsuario.setIdUsuarioT(user.getIdUsuario());
				busquedaUsuario.setIdSupervisorT(user.getIdSupervisor());
			}else if(user.getRole().getIdRol() == 102 ){
				busquedaUsuario.setIdUsuarioT(user.getIdUsuario());
			}else{
				busquedaUsuario.setIdUsuarioT(-1);
			}
			
			busquedaUsuario.setLoginT(busquedaUsuario.getLoginT()!= null && !"".equals(busquedaUsuario.getLoginT())? busquedaUsuario.getLoginT()+"%": "");
			busquedaUsuario.setNombreT(busquedaUsuario.getNombreT()!= null && !"".equals(busquedaUsuario.getNombreT())? busquedaUsuario.getNombreT()+"%": "");
			busquedaUsuario.setaPaternoT(busquedaUsuario.getaPaternoT()!= null && !"".equals(busquedaUsuario.getaPaternoT())? busquedaUsuario.getaPaternoT()+"%": "");
//			busquedaUsuario.setaMaterno(busquedaUsuario.getaMaterno()!= null && !"".equals(busquedaUsuario.getaMaterno())?busquedaUsuario.getaMaterno()+"%":"");
			busquedaUsuario.setaMaternoT(null);
			
			resp.setData(mapperPortal.filtroUsuarios(busquedaUsuario));
			
			if(resp.getData() != null && resp.getData().size() > 0){
				resp.setTotalRecords(resp.getData().size() +1);
				resp.setCurPage(1);

			}else{
				resp.setTotalRecords(resp.getData().size() +1);
				resp.setCurPage(1);
				resp.setData(new ArrayList<UsuarioVO>());
				resp.getData().add(new UsuarioVO());
				((UsuarioVO)resp.getData().get(0)).setIdUsuario(-1001);
				((UsuarioVO)resp.getData().get(0)).setNombre("No existen Usuarios, favor de validar filtros.");
			}
			
//			}else{
//				resp.setTotalRecords(0);
//				resp.setCurPage(1);
//				resp.setData(new ArrayList<PaymentData>());
//				resp.getData().add(new PaymentData());
//				resp.getData().get(0).setIdBitacora(-1);
//				resp.getData().get(0).setStatus("");
//				resp.getData().get(0).setCompany(ConstantesEuropaWeb.ERROR_NO_SESSION);
//			}
			
		}catch (Exception pe) {
			logger.error("Ocurrio un error general al buscar tiendas desde el portal: {}", pe.getMessage());
			resp.setTotalRecords(0);
			resp.setCurPage(1);
			resp.setData(new ArrayList<UsuarioVO>());
			resp.getData().add(new UsuarioVO());
			((UsuarioVO)resp.getData().get(0)).setIdUsuario(-1001);
			((UsuarioVO)resp.getData().get(0)).setNombre("Error en busqueda tiendas: " + pe.getMessage());
		}finally{
			json = utilService.objectToJson(resp);
		}
		return json;
	}
	
	public String actualizarUsuario(UsuarioVO usuario, ModelMap modelo ){				
		AbstractVO resp = null;
//		List<TiendaVO> tiendas = null;
		String json = null;
		LoginRespuesta user = null;
		
		try {
			user = (LoginRespuesta) modelo.get("usuario");
			
			if(user.getRole().getIdRol() == 100 || user.getRole().getIdRol() == 101){
				usuario.setPassword(null);
//				tiendas = mapperPortal.buscarTienda(tienda.getIdTienda(), null, null);
				
//				if(tiendas != null && tiendas.size() > 0){
					mapperPortal.actualizarUsuario(usuario);
					resp = new AbstractVO(0, "Usuario actualizado correctamente.");
//				}else{
//					resp = new AbstractVO(-1, "La tienda no existe, favor de validar la informacion.");
//				}
			}else{
				resp = new AbstractVO(-1, "Usuario no tiene permisos para modificar datos.");
			}
			
			
		}catch (Exception pe) {
			logger.error("Ocurrio un error general al buscar tiendas desde el portal: {}", pe.getMessage());
			resp = new AbstractVO(-1, "Error al actuliazar tienda: " + pe.getMessage());
		}finally{
			json = utilService.objectToJson(resp);
		}
		return json;
	}
	
	public String actualizarPassword(UsuarioVO usuario, ModelMap modelo ){				
		AbstractVO resp = null;
//		List<TiendaVO> tiendas = null;
		String json = null;
		LoginRespuesta user = null;
		LoginRespuesta usuarioVO = null; 
		String password = null;
		try {
			user = (LoginRespuesta) modelo.get("usuario");
			
			if(user.getRole().getIdRol() == 100 || user.getRole().getIdRol() == 101){
				usuario.setPassword(null);
				resp = new AbstractVO(0, "Usuario actualizado correctamente.");
				
				password = generaPassword();
				usuario.setPassword(AddcelCrypto.encryptPassword(password));
				
				usuarioVO = mapperAdm.loginUsuario(usuario.getIdUsuario(), null, null, null, null, 0, 0); 
				
				if(usuarioVO != null && usuarioVO.getStatus() == 1){
					usuario.setNombre(usuarioVO.getNombre());
					usuario.setApellido(usuarioVO.getApellido());
					usuario.setMaterno(usuarioVO.getMaterno());
					usuario.setStatus(99);
					usuario.setPassword(AddcelCrypto.encryptPassword(password));
					int updateResult = mapperPortal.actualizarUsuario(usuario);
					logger.info("Usuario actualizado con resultado ==> {}", updateResult);
					
					if (StringUtils.isEmpty(usuario.getMail())) {

						logger.error("Correo vacío");
						usuario.setMail(usuarioVO.getMail());
					}
					
					AddCelGenericMail.sendMail(utilService.objectToJson(AddCelGenericMail.generatedResetPassword(usuario, password)));
					resp = new AbstractVO(0, "Password actualizado correctamente.");
					logger.info("Usuario guardado correctamente con id ==> {} ", usuario.getIdUsuario());
					
				}else if(usuarioVO != null && usuarioVO.getStatus() != 1){
					resp = new AbstractVO(-2, "Usuario no activo, favor de cambiar el estatus antes del reset de contraseña.");
				}else{
					resp = new AbstractVO(-3, "Usuario no existe, favor de validar.");
				}
			}else{
				resp = new AbstractVO(-1, "Usuario no tiene permisos para modificar datos.");
			}
			
			
		}catch (Exception pe) {
			logger.error("Ocurrio un error general al reset password desde el portal: {}", pe.getMessage());
			resp = new AbstractVO(-1, "Error al reset Contraseña: " + pe.getMessage());
		}finally{
			json = utilService.objectToJson(resp);
		}
		return json;
	}
	
	public String generaPassword() {
		String sPwd = (random(5869452) + random(5869452) + random(5869452)+ random(5869452)).substring(0, 10);
		logger.info("Password: {}",sPwd);
		return sPwd;
	}
	
	public String random(int n) {
		java.util.Random rand = new java.util.Random();
		int x = rand.nextInt(n);
		return "" + x;
	}	
}
