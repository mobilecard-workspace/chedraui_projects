package com.addcel.chedraui.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.addcel.chedraui.model.mapper.AdmonMapper;
import com.addcel.chedraui.model.mapper.ChedrauiMapper;
import com.addcel.chedraui.model.vo.AbstractVO;
import com.addcel.chedraui.model.vo.BusquedaPagoResponse;
import com.addcel.chedraui.model.vo.TokenVO;
import com.addcel.chedraui.model.vo.admon.LoginVO;
import com.addcel.chedraui.model.vo.admon.TiendaVO;
import com.addcel.chedraui.model.vo.admon.UsuarioVO;
import com.addcel.chedraui.model.vo.request.RequestBusquedaPagos;
import com.addcel.chedraui.model.vo.response.LoginRespuesta;
import com.addcel.chedraui.utils.AddCelGenericMail;
import com.addcel.chedraui.utils.ConstantesChedraui;
import com.addcel.chedraui.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;

@Service
public class ChedrauiService {
	private static final Logger logger = LoggerFactory.getLogger(ChedrauiService.class);
	
	@Autowired
	private ChedrauiMapper mapper;
	
	@Autowired
	private AdmonMapper mapperAdm;
	
	@Autowired
	private UtilsService utilService;	
	
	public String login(String jsonEnc) {
		LoginRespuesta lr = null;
		String jsonResEnc=null;
//		String json = ;
		try{
			LoginVO login = (LoginVO) utilService.jsonToObject(AddcelCrypto.decryptSensitive(jsonEnc), LoginVO.class);				
			login.setPassword(AddcelCrypto.encryptPassword(login.getPassword()));
			LoginRespuesta user = mapperAdm.loginUsuario(0, null, null, null, login.getLogin(), 0, 0);	
			
			if(user == null){			
				lr = new LoginRespuesta(1, "Usuario o Password incorrectos.");
				
			}else if(user.getStatus() == 3){
				lr = new LoginRespuesta(3, "Usuario bloqueado, favor de contactar con su Supervisor");
				logger.info("Usuario bloqueado id ==> {}", login.getLogin());
				
//			}else if(user.getStatus() == 4){
//				lr = new LoginRespuesta(4, "Se requiere cambio de contraseña");
//				logger.info("Usuario dado de baja id ==> {}", login.getLogin());
//				
			}else if(user.getStatus() != 1 && user.getStatus() != 99){
				lr = new LoginRespuesta(4, "Status no valido, favor de contactar con su Supervisor.");
				logger.info("Status no valido id ==> {}", login.getLogin());
				
			}	else {
				if(user.getStatus() == 1 || user.getStatus() == 99){								
					if(user.getPassword().equals(login.password)){
//							lr = mapper.loginUsuario(user.getIdUser());
						mapperAdm.updateIntentosStatus(user.getIdUsuario(), 0, "contadorLogin");//Reset loginCount=0
						lr = user;
						lr.setIdError(0);
						lr.setMensajeError("Usuario valido.");
					}else{												
						int contador = user.getLoginCount()+1;						
						lr = new LoginRespuesta();
						if((user.getRole().getIdRol() == 101 && 7 <= contador) || (user.getRole().getIdRol() == 102 && 5 <= contador)){
							//se difine el numero maximo de intentos por rol trae el numero maximo de intentos por rol
//								logger.debug("intentos ==> {} = max_login ==> {}",contador,user.getIdTienda());
							//bloquear usuario
							mapperAdm.updateIntentosStatus(user.getIdUsuario(), 3, "status");  //Status 3 = bloqueado
							mapperAdm.updateIntentosStatus(user.getIdUsuario(), 0, "contador"); //Reset loginCount = 0		
							
							lr = new LoginRespuesta(4, "El usuario ha sido bloqueado, favor de contactar con su Supervisor.");
							logger.info("# de intentos superado. El usuario con id ==> {} ha sido bloqueado ",user.getIdUsuario());
						}else{							
							mapperAdm.updateIntentosStatus(user.getIdUsuario(), contador, "contador");//incrementa el contador							
							lr = new LoginRespuesta(5, "Contraseña incorrecta, intento: " + contador +
									" de " + (user.getRole().getIdRol() == 101?"7":"5"));
						}
					}									
				}
			}	
				
		}catch(Exception e){
			lr = new LoginRespuesta(-1, "Ocurrio un error: " + e.getMessage());
		}finally{
			jsonResEnc=AddcelCrypto.encryptSensitive("1239829201",utilService.objectToJson(lr));
		}
		
		return jsonResEnc;		
	}
	
	public String guardarUsuario(String jsonEnc) {
		AbstractVO res = null;
		List<TiendaVO> tiendas = null;
		String jsonResEnc=null;
		String password = null;
		try {
			password = generaPassword();
			
			UsuarioVO usuario = (UsuarioVO) utilService.jsonToObject(AddcelCrypto.decryptSensitive(jsonEnc), UsuarioVO.class);
			usuario.setPassword(AddcelCrypto.encryptPassword(password));
			
			mapperAdm.guardarUsuario(usuario);
			tiendas = mapperAdm.listaTiendas(usuario.getIdTienda(), null); 
			
			AddCelGenericMail.sendMail(utilService.objectToJson(AddCelGenericMail.generatedMailAltaUsuario(usuario, password, tiendas.get(0).getDescripcion())));
			res = new AbstractVO(0, "Usuario dado de alta correctamente.");
			logger.info("Usuario guardado correctamente con id ==> {} ", usuario.getIdUsuario());
		} catch (DuplicateKeyException pe) {				
			res = new AbstractVO(1, "El login de usuario ya existe.");
			logger.error("Nombre de usuario duplicado: {}", pe.getMessage());
		}catch (Exception pe) {			
			res = new AbstractVO(1, "Error al registrar usuario: " + pe.getMessage());
			logger.error("Error al insertar usuario: {}", pe.getMessage());
		}finally{
			jsonResEnc=AddcelCrypto.encryptSensitive("1239829201",utilService.objectToJson(res));
		}
		return jsonResEnc;
	}
	
	public String actualizarUsuario(String jsonEnc){				
		
		AbstractVO res = new AbstractVO();
		String jsonResEnc=null;
		UsuarioVO usuario = null;
//		if(usuario.getIdUsuario() != usuario.getIdSupervisor()){
			try{
				usuario=(UsuarioVO) utilService.jsonToObject(AddcelCrypto.decryptSensitive(jsonEnc), UsuarioVO.class);
				
				mapperAdm.actualizarUsuario(usuario);
				res.setIdError(0);
				res.setMensajeError("Actualizacion de usuario correcta.");
				logger.info("Usuario actualizado: id {}",usuario.getIdUsuario());
			}catch(Exception e){
				res.setIdError(1);
				res.setMensajeError("Error al actualizar Usuario: " + e.getMessage());
				logger.error("Error al actualizar usuario con id ==> {}. Error\n {}",usuario.getIdUsuario(),e);
			}finally{
				jsonResEnc=AddcelCrypto.encryptSensitive("1239829201",utilService.objectToJson(res));
			}
//		}else{
//			res.setIdError(2);
//			res.setMensajeError("Operacíon no permitida");
//		}
		
		return jsonResEnc;
	}
	
	public String actualizarPassword(String jsonEnc){				
		AbstractVO res = null;
		String jsonResEnc = null;
		UsuarioVO usuario = null;
		LoginRespuesta user = null;
		
		try{
			usuario = (UsuarioVO) utilService.jsonToObject(AddcelCrypto.decryptSensitive(jsonEnc), UsuarioVO.class);
			usuario.setPassword(AddcelCrypto.encryptPassword(usuario.getPassword()));
			usuario.setPasswordNuevo(AddcelCrypto.encryptPassword(usuario.getPasswordNuevo()));
			user = mapperAdm.loginUsuario(usuario.getIdUsuario(), null, null, null, null, 0, 0);
			
			if(user == null){
				res = new AbstractVO(1, "Usuario no existe.");
				
			}else if(!user.getPassword().equals(usuario.getPassword())){
				res = new AbstractVO(2, "Contraseña incorrecta.");
				
			}else{
				usuario.setNombre(null);
				usuario.setApellido(null);
				usuario.setMaterno(null);
				usuario.setMail(null);
				usuario.setIdSupervisor(0);
				usuario.setIdTienda(0);
				usuario.setStatus(1);
				usuario.setPassword(usuario.getPasswordNuevo());
				
				mapperAdm.actualizarUsuario(usuario);
				res = new AbstractVO(0, "Contraseña actualizada correctamente.");
				
				logger.info("Usuario actualizado contraseña: id {}",usuario.getIdUsuario());
			}
		}catch(Exception e){
			res = new AbstractVO(-1, "Error al actualizar Usuario: " + e.getMessage());
			logger.error("Error al actualizar usuario con id ==> {}. Error\n {}", usuario.getIdUsuario(), e);
		}finally{
			jsonResEnc=AddcelCrypto.encryptSensitive("1239829201",utilService.objectToJson(res));
		}
		
		return jsonResEnc;
	}
	
	public String getTiendas(String jsonEnc){				
		
		UsuarioVO usuario = null;
		List<TiendaVO> tiendas = null;
		try {
			usuario=(UsuarioVO) utilService.jsonToObject(AddcelCrypto.decryptHard(jsonEnc), UsuarioVO.class);
			tiendas = mapperAdm.listaTiendas(usuario.getIdTienda(), null); 
			
			if(tiendas != null && tiendas.size() > 0){
				jsonEnc="{\"idError\":0,\"mensajeError\":\"Exito.\", \"tiendas\": " + utilService.objectToJson(tiendas) + "}";
			}else{
				jsonEnc="{\"idError\":1,\"mensajeError\":\"No existen Tiendas.\", \"tiendas\": null}";
			}
			
		}catch(Exception e){
			jsonEnc="{\"idError\":2,\"mensajeError\":\"Error al buscar tiendas. " + e.getMessage() +"\", \"tiendas\": null}";
			logger.error("Error al buscar tiendas Error\n {}", e);
		}finally{
			jsonEnc=AddcelCrypto.encryptHard(jsonEnc);
		}
		
		return jsonEnc;
	}
	
	
	public String getRoles(String json){				
		UsuarioVO usuario = null;
		try{
			usuario = (UsuarioVO) utilService.jsonToObject(AddcelCrypto.decryptHard(json), UsuarioVO.class);
//			if(!"userPrueba".equals(usuario.getUsuario()) || !"passwordPrueba".equals(usuario.getPassword())){
				json="{\"idError\":2,\"mensajeError\":\"Exito.\", \"roles\": [{ \"idRol\": 101, \"descripcion\": \"Supervisor\" }, { \"idRol\": 102, \"descripcion\": \"Vendedor\" }]}";
//			}else{
//				json="{\"token\":\"" + AddcelCrypto.encrypt(ConstantesChedraui.KEY_TOKEN, mapper.getFechaActual()) + "\",\"idError\":0,\"mensajeError\":\"\"}";
//			}
		}catch(Exception e){
			logger.error("Ocurrio un error al generar el Token: {}", e.getMessage());
			json = "{\"idError\":1,\"mensajeError\":\"Ocurrio un error al generar el Token.\"}";
		}finally{
			json = AddcelCrypto.encryptHard(json);
		}
		return json;
	}
	
	public String generaToken(String json){				
		TokenVO tokenVO = null;
		try{
			tokenVO = (TokenVO) utilService.jsonToObject(AddcelCrypto.decryptHard(json), TokenVO.class);
			if(!"userPrueba".equals(tokenVO.getUsuario()) || !"passwordPrueba".equals(tokenVO.getPassword())){
				json="{\"idError\":2,\"mensajeError\":\"No tiene permisos para consumir este servicio.\"}";
			}else{
				json="{\"token\":\"" + AddcelCrypto.encrypt(ConstantesChedraui.KEY_TOKEN, mapper.getFechaActual()) + "\",\"idError\":0,\"mensajeError\":\"\"}";
			}
		}catch(Exception e){
			logger.error("Ocurrio un error al generar el Token: {}", e.getMessage());
			json = "{\"idError\":1,\"mensajeError\":\"Ocurrio un error al generar el Token.\"}";
		}finally{
			json = AddcelCrypto.encryptHard(json);
		}
		return json;
	}

	public String generaPassword() {
		String sPwd = (random(5869452) + random(5869452) + random(5869452)+ random(5869452)).substring(0, 10);
		logger.info("Password: {}",sPwd);
		return sPwd;
	}
	
	public String random(int n) {
		java.util.Random rand = new java.util.Random();
		int x = rand.nextInt(n);
		return "" + x;
	}		

	public String busquedaPagos(String data) {
		List<BusquedaPagoResponse> lista = null;
		RequestBusquedaPagos request = null;
		String json = null;

		try{
			request = 	(RequestBusquedaPagos) utilService.jsonToObject(AddcelCrypto.decryptHard(data), RequestBusquedaPagos.class);
			lista = mapper.getDetalle(request);
			
			if(lista == null || lista.size() == 0){
				json = "{\"idError\":1,\"mensajeError\":\"NO existen Registros.\"}";
			}else{
				json = "{\"idError\":0,\"mensajeError\":\"\", \"pagos\": " + utilService.objectToJson( lista ) +  "}";
			}
			
		}catch(Exception e){
			logger.error("Ocurrio un error en busqueda de pagos: {}", e);
			json = "{\"idError\":2,\"mensajeError\":\"Ocurrio un error: " + e.getMessage() + " .\"}";
		}finally{
			json = AddcelCrypto.encryptHard( json ); 
		}
		
		return json;
	}
	
	public String reenvioRecibo(String data) {
		List<BusquedaPagoResponse> detalleVO = null;
		RequestBusquedaPagos request = null;
		String json = null;
		try{
			request = (RequestBusquedaPagos) utilService.jsonToObject(AddcelCrypto.decryptHard(data), RequestBusquedaPagos.class);
			if(request.getIdUsuario() == null){
				json = "{\"idError\":2,\"mensajeError\":\"Falta el parametro idUsuario.\"}";
			}else if(request.getReferenciaMC() == null){
				json = "{\"idError\":3,\"mensajeError\":\"Falta el parametro ReferenciaMC.\"}";
			}else{
				
				detalleVO =  mapper.getDetalle(request);
				
				if(detalleVO != null && detalleVO.size() >0){
					logger.info("Se obtuvieron datos, inicia proceso de reenvio de correo.");
					AddCelGenericMail.sendMail(
							utilService.objectToJson(AddCelGenericMail.generatedMail(detalleVO.get(0))));
					json = "{\"idError\":0,\"mensajeError\":\"Recibo reenviado correctamente.\"}";
				}else{
					logger.error("ID_BITACORA: " + request.getReferenciaMC() + ", No se obtuvieron datos para inicia proceso de reenvio del recibo.");
					json = "{\"idError\":1,\"mensajeError\":\"No se obtuvieron datos para inicia el proceso de reenvio del recibo.\"}";
				}
			}
			
		}catch(Exception e){
			logger.error("Ocurrio un error al reenviar el recibo: {}", e.getMessage());
			json = "{\"idError\":2,\"mensajeError\":\"Ocurrio un error al reenviar el recibo.\"}";
		}finally{
			json = AddcelCrypto.encryptHard(json);
		}
		return json;	
	}
}
