package com.addcel.chedraui.services;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.chedraui.model.mapper.BitacorasMapper;
import com.addcel.chedraui.model.mapper.ChedrauiMapper;
import com.addcel.chedraui.model.vo.BusquedaPagoResponse;
import com.addcel.chedraui.model.vo.DatosPago;
import com.addcel.chedraui.model.vo.DatosPagoProductos;
import com.addcel.chedraui.model.vo.DatosPagoResponse;
import com.addcel.chedraui.model.vo.TProveedorVO;
import com.addcel.chedraui.model.vo.admon.UsuarioVO;
import com.addcel.chedraui.model.vo.pagos.ProsaPagoVO;
import com.addcel.chedraui.model.vo.pagos.TBitacoraProsaVO;
import com.addcel.chedraui.model.vo.pagos.TBitacoraVO;
import com.addcel.chedraui.utils.AddCelGenericMail;
import com.addcel.chedraui.utils.ConstantesChedraui;
import com.addcel.chedraui.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;

@Service
public class ChedrauiPagosService {
	private static final Logger logger = LoggerFactory.getLogger(ChedrauiPagosService.class);
	
	@Autowired 
	private ChedrauiMapper mapper;
	
	@Autowired
	private UtilsService utilService;
	
	@Autowired
	private BitacorasMapper bm;
	
	
	private static final String patron = "ddhhmmss";
	private static final String patronCom = "yyyy/MM/dd HH:mm:ss";
	private static final SimpleDateFormat formatoCom = new SimpleDateFormat(patronCom);
	private static final SimpleDateFormat formato = new SimpleDateFormat(patron);

	public String pagoProductos(String jsonEnc){		
		DatosPagoResponse respuesta = null;
		DatosPago datosPago = null;
		String json = null;
		
		try{
			datosPago = (DatosPago) utilService.jsonToObject(AddcelCrypto.decryptSensitive(jsonEnc), DatosPago.class);
			
			respuesta = validarDataPagos(datosPago);
			
			if(respuesta == null){
				TProveedorVO tproveedor = mapper.getTProveedor();
				
				datosPago.setAfiliacion(tproveedor.getAfiliacion());
				datosPago.setComision(tproveedor.getComision());
				datosPago.setMonto(0);
				
				int index = 0;
				for(DatosPagoProductos producto: datosPago.getProductos()){
					producto.setIndex(index++);
					datosPago.setMonto(datosPago.getMonto() + producto.getMonto());
				}
				datosPago.setTotal(datosPago.getMonto() + datosPago.getComision());
				
				//Insetar Bitacoras Prosa y detalle						
				insertaBitacoras(datosPago);
				
				respuesta = enviarPagoVisa(datosPago);
			}
		}catch(Exception e){
			
		}finally{
			if(respuesta != null){
				json = AddcelCrypto.encryptSensitive(formato.format(new Date()), utilService.objectToJson(respuesta));
			}
		}
		
		return json;
	}
	
	private DatosPagoResponse enviarPagoVisa(DatosPago datosPago){
		DatosPagoResponse respuesta = null;
		ProsaPagoVO prosaPagoVO = null;
		String concepto = null;
		try{
			
			StringBuffer data = new StringBuffer() 
					.append( "card="      ).append( URLEncoder.encode(datosPago.getTarjeta(), "UTF-8") )
					.append( "&vigencia=" ).append( URLEncoder.encode(datosPago.getVigencia(), "UTF-8") )
					.append( "&nombre="   ).append( URLEncoder.encode(datosPago.getNombre() != null? datosPago.getNombre(): "", "UTF-8") )
					.append( "&cvv2="     ).append( URLEncoder.encode(datosPago.getCvv2(), "UTF-8") )
					.append( "&monto="    ).append( URLEncoder.encode(datosPago.getTotal() + "", "UTF-8") )
					.append( "&afiliacion=" ).append( URLEncoder.encode(datosPago.getAfiliacion(), "UTF-8") );
//						.append( "&moneda="   ).append( URLEncoder.encode("MXN", "UTF-8")) ;
					
			logger.info("Envío de datos Grupo Richard VISA: {}", data);
			
			String sb = new UtilsService().peticionHttpsUrlParams(ConstantesChedraui.URL_AUT_PROSA, data.toString());
			
			prosaPagoVO = (ProsaPagoVO) utilService.jsonToObject(sb.toString(), ProsaPagoVO.class);
			
			if(prosaPagoVO != null){
				respuesta = new DatosPagoResponse();
				respuesta.setReferenciaMC(datosPago.getIdBitacora());
				respuesta.setNumAutorizacion(prosaPagoVO.getAutorizacion());
				respuesta.setFecha(formatoCom.format(new Date()));
//				respuesta.setTransaccion(String.valueOf(datosPago.getIdBitacora()));
//				datosPago.setMsg(prosaPagoVO.getMsg());
				
				if(prosaPagoVO.isAuthorized()){
					concepto = "EXITO PAGO PRODUCTOS CHEDRAUI VISA AUTORIZADA";
					respuesta.setStatus(1);
					respuesta.setMensajeError("Se realizo el pago exitosamente..");
					respuesta.setTotal(datosPago.getTotal());
					respuesta.setMonto(datosPago.getMonto());
					respuesta.setComision(datosPago.getComision());
					respuesta.setSucursal(datosPago.getSucursal());
					respuesta.setProductos(datosPago.getProductos());
					AddCelGenericMail.sendMail(utilService.objectToJson(AddCelGenericMail.generatedMail(new BusquedaPagoResponse(datosPago, respuesta))));
					
				}else if(prosaPagoVO.isRejected()){
					concepto = "ERROR PAGO PRODUCTOS CHEDRAUI VISA RECHAZADA";
					respuesta.setStatus(0);
					respuesta.setIdError(Integer.parseInt(
						prosaPagoVO.getClaveRechazo()!= null && !"".equals(prosaPagoVO.getClaveRechazo())? prosaPagoVO.getClaveRechazo(): "1"));
					respuesta.setMensajeError(prosaPagoVO.getDescripcionRechazo());
					
				}else if(prosaPagoVO.isProsaError()){
					concepto = "ERROR PAGO PRODUCTOS CHEDRAUI VISA ERROR";
					respuesta.setStatus(0);
					respuesta.setIdError(Integer.parseInt(
							prosaPagoVO.getClaveRechazo()!= null && !"".equals(prosaPagoVO.getClaveRechazo())? prosaPagoVO.getClaveRechazo(): "1"));
					respuesta.setMensajeError("El pago fue rechazado: " + prosaPagoVO.getDescripcionRechazo());
					
//					AddCelGenericMail.sendMail(utilService.objectToJson(AddCelGenericMail.generatedMailError(datosPago,"ERROR")));
				}
				
				updateBitacoras(respuesta, concepto);
			}
		}catch(Exception e){
			respuesta = new DatosPagoResponse();
			logger.error("Ocurrio un error durante el llamado al banco: {}", e);
			respuesta.setStatus(100);
			respuesta.setMensajeError("Ocurrio un error durante el pago al banco");
			updateBitacoras(respuesta, "ERROR PAGO GRUPO RICHARD VISA GENERAL");
		}
		return respuesta;
	}
	
	public DatosPagoResponse validarDataPagos(DatosPago datosPago){		
		DatosPagoResponse resp = null;
		List<UsuarioVO> usuarioOriginal = null;
		Integer diferencia = null;
		try {
//			usuarioOriginal = usuarioMapper.validarUsuario(0,  datosPago.getIdUsuario(), null, null);
//
//			if(usuarioOriginal == null || (usuarioOriginal != null && usuarioOriginal.size() == 0)){
//				resp = new DatosPagoResponse(101, "No existe el usuario.");
//			}else if(!usuarioOriginal.get(0).getPassword().equals(datosPago.getPassword())){
//				resp = new DatosPagoResponse(102, "Password incorrecto.");
//			}else if(usuarioOriginal.get(0).getStatus() != 1){
//				resp = new DatosPagoResponse(111, "El usuario no está activo.");
//			}else if(usuarioOriginal.get(0).getStatusTienda() != 1){
//				resp = new DatosPagoResponse(112, "La tienda no está activa.");
//			}else if(!usuarioOriginal.get(0).getImei().equals(datosPago.getImei() )){
//				resp = new DatosPagoResponse(103, "El usuario \"" + usuarioOriginal.get(0).getLogin() +"\" no puede realizar una compra en este equipo.");
//			}else 
			if(datosPago.getToken() == null){
				resp = new DatosPagoResponse(50, "El parametro TOKEN no puede ser NULL.");
				logger.error("El parametro TOKEN no puede ser NULL");
			}else{
				datosPago.setToken(AddcelCrypto.decryptJson(ConstantesChedraui.KEY_TOKEN, datosPago.getToken()));
				logger.info("token ==> {}", datosPago.getToken());
				diferencia = mapper.diferenciaFechaSeg(datosPago.getToken());

				try{
//					if(diferencia == null || diferencia > 31){
					if(false){
						resp = new DatosPagoResponse(51, "La Transacción no es válida, por favor intentar nuevamente.");
						logger.error("La Transacción ya no es válida");
					}else if(datosPago.getTarjeta() == null){
						resp = new DatosPagoResponse(52, "El número de tarjeta es obligatorio.");
					}else if(datosPago.getVigencia() == null){
						resp = new DatosPagoResponse(52, "La vigencia es obligatoria.");
					}else if(datosPago.getCvv2() == null){
						resp = new DatosPagoResponse(52, "El número CVV2 es obligatorio.");
					}else if(datosPago.getTotal() == 0){
						resp = new DatosPagoResponse(50, "El Monto es obligatorio.");
//					}else{
//						logger.debug("Validaciones correctas.");
					}
				}catch(Exception e){
					resp = new DatosPagoResponse(51, "La Transacción no es válida, por favor intentar nuevamente.");
					logger.error("La Transacción ya no es válida: {}", e.getMessage());
				}
			}
		}catch(Exception e){
			resp = new DatosPagoResponse();
			resp.setMensajeError("Ocurrio un error en las Validaciones de Datos: " + e.getMessage());
			logger.error("Ocurrio un error al validar datos de pago: {}", e);
		}
		return resp;
	}	
	
	private long insertaBitacoras(DatosPago datosPago) throws Exception{		
		TBitacoraVO tbitacora = null;
		TBitacoraProsaVO tbitacoraProsa = null;
//		String tarjeta = null;
		try{
			tbitacora = new TBitacoraVO(
					datosPago.getIdUsuario(),
					"PAGO PRODUCTOS CHEDRAUI", 
					datosPago.getImei(),
					"PAGO PRODUCTOS CHEDRAUI",
					AddcelCrypto.encryptTarjeta(datosPago.getTarjeta()),
					datosPago.getModelo(), 
					datosPago.getSoftware(), 
					datosPago.getModelo(),
					datosPago.getWkey(),
					datosPago.getTotal() + ""
					);
			
			if(datosPago.getTarjeta().length() == 15){
				datosPago.setTarjetaFinal(datosPago.getTarjeta().substring(11));
			}else{
				datosPago.setTarjetaFinal(datosPago.getTarjeta().substring(12));
			}
			
			logger.debug("Insertar en t_bitacora...");
			bm.agregarBitacora(tbitacora);
			
			logger.debug("idBitacora: {}" , tbitacora.getIdBitacora());
			datosPago.setIdBitacora(tbitacora.getIdBitacora());
			tbitacoraProsa = new TBitacoraProsaVO( tbitacora, datosPago.getMonto(), datosPago.getComision());
			
			logger.debug("Insertar en t_bitacoraProsa...");
			bm.agregarBitacoraProsa(tbitacoraProsa);
			
			logger.debug("Insertar en CHEDAUI_detalle...");
			bm.agregarDetalle(datosPago);
			
			logger.debug("Insertar en CHEDAUI_detalle_productos...");
			bm.agregarDetalleProductos(datosPago);
		
		
		}catch(Exception e){
			tbitacora.setIdBitacora(0);
			logger.error("Error general al insertar en bitacoras: {}", e);
			throw new Exception(e);
		}
		
		return tbitacora.getIdBitacora();
	}
	
	private void updateBitacoras(DatosPagoResponse datosPagoResp, String concepto){
		TBitacoraVO tbitacora = new TBitacoraVO();
		tbitacora.setIdBitacora(datosPagoResp.getReferenciaMC());
		tbitacora.setBitStatus(datosPagoResp.getStatus());
		tbitacora.setBitCodigoError(datosPagoResp.getIdError());
		tbitacora.setBitConcepto(concepto);
		tbitacora.setBitNoAutorizacion(datosPagoResp.getNumAutorizacion());
		
		logger.debug("Actualizando t_bitacora...");
		bm.actualizarBitacora(tbitacora);
		
		if(datosPagoResp.getNumAutorizacion() != null && !datosPagoResp.getNumAutorizacion().equals("")){
			logger.debug("Actualizando t_bitacora_prosa...");
			bm.actualizarBitacoraProsa(tbitacora);
		}
		
		
		logger.debug("Actualizando CHEDRAUI_detalle...");
		bm.updateDetalle(datosPagoResp.getReferenciaMC(),  
				datosPagoResp.getNumAutorizacion(), datosPagoResp.getStatus(), datosPagoResp.getMonto(), 
				datosPagoResp.getComision());
	}
}
