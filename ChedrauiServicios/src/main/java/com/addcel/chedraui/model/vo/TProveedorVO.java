package com.addcel.chedraui.model.vo;

public class TProveedorVO {
	private long idProveedor;
	private String afiliacion;
	private String maxTransaccion;
	private double comision;
	
	public long getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(long idProveedor) {
		this.idProveedor = idProveedor;
	}
	public String getAfiliacion() {
		return afiliacion;
	}
	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}
	public String getMaxTransaccion() {
		return maxTransaccion;
	}
	public void setMaxTransaccion(String maxTransaccion) {
		this.maxTransaccion = maxTransaccion;
	}
	public double getComision() {
		return comision;
	}
	public void setComision(double comision) {
		this.comision = comision;
	}
	
	
}
