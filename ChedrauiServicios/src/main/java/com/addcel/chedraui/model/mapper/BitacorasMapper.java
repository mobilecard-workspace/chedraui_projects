package com.addcel.chedraui.model.mapper;

import org.apache.ibatis.annotations.Param;

import com.addcel.chedraui.model.vo.DatosPago;
import com.addcel.chedraui.model.vo.pagos.TBitacoraProsaVO;
import com.addcel.chedraui.model.vo.pagos.TBitacoraVO;

public interface BitacorasMapper {
public int agregarBitacora(TBitacoraVO tbitacora);
	
	public int agregarBitacoraProsa(TBitacoraProsaVO tbitacoraProsa);
	
	public int actualizarBitacora(TBitacoraVO tbitacora);
	
	public int actualizarBitacoraProsa(TBitacoraVO tbitacoraProsa);
	
	public int agregarDetalle(DatosPago datosPago);
	
	public int agregarDetalleProductos(DatosPago datosPago);
	
	public int updateDetalle(
									@Param(value="idBitacora") long idBitacora,
									@Param(value="numAutorizacion") String numAutorizacion,
									@Param(value="status") int status,
									@Param(value="cargo") double cargo,
									@Param(value="comision") double comision
									);
}
