package com.addcel.chedraui.model.vo.response;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.addcel.chedraui.model.vo.AbstractVO;
import com.addcel.chedraui.model.vo.admon.RoleVO;
import com.addcel.chedraui.model.vo.admon.TiendaVO;

@JsonIgnoreProperties(ignoreUnknown=true)
public class LoginRespuesta extends AbstractVO{	
	private int idUsuario;
	private int idSupervisor;
    private String login;
    private String password;
    
    private TiendaVO tienda;
    private RoleVO role;

    private String nombre;
    private String apellido;
    private String materno;
    private String mail;
    
    private int status;
    private int loginCount;
    private int isPortal;
    
    public LoginRespuesta() {	
	}
    public LoginRespuesta(int idError, String mensajeError) {	
    	super(idError, mensajeError);
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public int getIdSupervisor() {
		return idSupervisor;
	}

	public void setIdSupervisor(int idSupervisor) {
		this.idSupervisor = idSupervisor;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public TiendaVO getTienda() {
		return tienda;
	}

	public void setTienda(TiendaVO tienda) {
		this.tienda = tienda;
	}

	public RoleVO getRole() {
		return role;
	}

	public void setRole(RoleVO role) {
		this.role = role;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getMaterno() {
		return materno;
	}

	public void setMaterno(String materno) {
		this.materno = materno;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getLoginCount() {
		return loginCount;
	}

	public void setLoginCount(int loginCount) {
		this.loginCount = loginCount;
	}
	public int getIsPortal() {
		return isPortal;
	}
	public void setIsPortal(int isPortal) {
		this.isPortal = isPortal;
	}
}
