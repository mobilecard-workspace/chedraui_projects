package com.addcel.chedraui.model.vo.portal;

public class TiendaDataRequest extends AbstractBusquedaRequest{

	private int idTienda;
	private String codigoT;
	private String nombreT;
	private String urlT;
	private int posMobile;
	private int rompeFilas;
	public String getCodigoT() {
		return codigoT;
	}
	public void setCodigoT(String codigoT) {
		this.codigoT = codigoT;
	}
	public String getNombreT() {
		return nombreT;
	}
	public void setNombreT(String nombreT) {
		this.nombreT = nombreT;
	}
	public String getUrlT() {
		return urlT;
	}
	public void setUrlT(String urlT) {
		this.urlT = urlT;
	}
	public int getPosMobile() {
		return posMobile;
	}
	public void setPosMobile(int posMobile) {
		this.posMobile = posMobile;
	}
	public int getRompeFilas() {
		return rompeFilas;
	}
	public void setRompeFilas(int rompeFilas) {
		this.rompeFilas = rompeFilas;
	}
	public int getIdTienda() {
		return idTienda;
	}
	public void setIdTienda(int idTienda) {
		this.idTienda = idTienda;
	}
	
}
