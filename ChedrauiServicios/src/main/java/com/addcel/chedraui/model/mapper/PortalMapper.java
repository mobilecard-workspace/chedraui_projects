package com.addcel.chedraui.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.chedraui.model.vo.admon.TiendaVO;
import com.addcel.chedraui.model.vo.admon.UsuarioVO;
import com.addcel.chedraui.model.vo.portal.TiendaDataRequest;
import com.addcel.chedraui.model.vo.portal.UsuariosDataRequest;

public interface PortalMapper {
	
	public int guardarTienda(TiendaVO tienda);

	public List<TiendaVO> buscarTienda(
			@Param(value = "idTienda") int idTienda,
			@Param(value = "codigo") String codigo,
			@Param(value = "descripcion") String descripcion);
	
	
	public List<TiendaVO> filtroTiendas(TiendaDataRequest busquedaTienda);
	
	public List<UsuarioVO> filtroUsuarios(UsuariosDataRequest busquedaUsuario);
	
	public int actualizarUsuario(UsuarioVO usuario);

	public int actualizarTienda(TiendaVO tienda);

	
	
	
}
