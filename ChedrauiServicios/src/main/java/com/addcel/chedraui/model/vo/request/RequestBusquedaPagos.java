package com.addcel.chedraui.model.vo.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class RequestBusquedaPagos  {
	
	private String idRol;
	private String idUsuario;
	private int tipoBusqueda;
	private String referenciaMC;
	public String getIdRol() {
		return idRol;
	}
	public void setIdRol(String idRol) {
		this.idRol = idRol;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getTipoBusqueda() {
		return tipoBusqueda;
	}
	public void setTipoBusqueda(int tipoBusqueda) {
		this.tipoBusqueda = tipoBusqueda;
	}
	public String getReferenciaMC() {
		return referenciaMC;
	}
	public void setReferenciaMC(String referenciaMC) {
		this.referenciaMC = referenciaMC;
	}

	
}