package com.addcel.chedraui.model.vo.admon;

public class TiendaVO {
	private int idTienda;
	private String descripcion;
	private int status;
	
	private int posMobile;
	private int rompeFilas;
	
	private String codigo;
	private String urlFTP;
	private String userFTP;
	private String passFTP;
	
	public int getIdTienda() {
		return idTienda;
	}
	public void setIdTienda(int idTienda) {
		this.idTienda = idTienda;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getUrlFTP() {
		return urlFTP;
	}
	public void setUrlFTP(String urlFTP) {
		this.urlFTP = urlFTP;
	}
	public String getUserFTP() {
		return userFTP;
	}
	public void setUserFTP(String userFTP) {
		this.userFTP = userFTP;
	}
	public String getPassFTP() {
		return passFTP;
	}
	public void setPassFTP(String passFTP) {
		this.passFTP = passFTP;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public int getPosMobile() {
		return posMobile;
	}
	public void setPosMobile(int posMobile) {
		this.posMobile = posMobile;
	}
	public int getRompeFilas() {
		return rompeFilas;
	}
	public void setRompeFilas(int rompeFilas) {
		this.rompeFilas = rompeFilas;
	}
}
