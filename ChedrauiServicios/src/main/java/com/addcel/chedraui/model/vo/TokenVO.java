package com.addcel.chedraui.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class TokenVO {
	private int idComercio;
	private String usuario;
	private String password;
	public int getIdComercio() {
		return idComercio;
	}
	public void setIdComercio(int idComercio) {
		this.idComercio = idComercio;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
		
}
