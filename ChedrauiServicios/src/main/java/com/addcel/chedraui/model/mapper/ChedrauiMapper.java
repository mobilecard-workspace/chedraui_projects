package com.addcel.chedraui.model.mapper;

import java.util.List;

import com.addcel.chedraui.model.vo.BusquedaPagoResponse;
import com.addcel.chedraui.model.vo.TProveedorVO;
import com.addcel.chedraui.model.vo.request.RequestBusquedaPagos;

public interface ChedrauiMapper {
	
	public String getFechaActual();

	public Integer diferenciaFechaSeg(String fechaToken);

	public int getIdUser(int idUser);

	public List<BusquedaPagoResponse> getDetalle(RequestBusquedaPagos busquedaPagos);

	public TProveedorVO getTProveedor();
	
}