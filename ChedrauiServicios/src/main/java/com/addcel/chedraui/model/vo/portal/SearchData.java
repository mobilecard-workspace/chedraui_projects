package com.addcel.chedraui.model.vo.portal;

import java.util.HashMap;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class SearchData {

	private int idBitacora;
	private int idCompany;
	private String company;
	private String descProduct;
	private String fullName;
	private String mail;
	private String cardNumber;
	private String total;
	private String parametro;
	private int idStatus;
	private String status;
	private String moneda;
	private String numAutorizacion;
	private String numRefund;
	private String claveError;
	private String descError;
	private String fecha;
	
	private String chargeBack = "";
	private String chargeBackNot = "";
	
	public SearchData(){
	}
	
	public SearchData(HashMap<String, Object> data){
		if(data != null){
			this.idBitacora = Integer.parseInt(data.get("idBitacora") + "");
			this.idCompany = (Integer) data.get("idCompany");
			this.company = (String) data.get("company");
			this.descProduct = (String) data.get("descProduct");
			this.fullName = (String) data.get("fullName");
			this.mail = (String) data.get("mail");
			this.cardNumber = (String) data.get("cardNumber");
			this.total = String.valueOf(data.get("total"));
			this.parametro = (String) data.get("parametro");
			this.idStatus = (Integer) data.get("idStatus");
			this.status = (String) data.get("status");
			this.moneda = (String) data.get("moneda");
			this.numAutorizacion = (String) data.get("numAutorizacion");
			this.claveError = (String) data.get("claveError");
			this.descError = (String) data.get("descError");
			this.fecha = (String) data.get("fecha");
		}
	}

	public int getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}

	public int getIdCompany() {
		return idCompany;
	}

	public void setIdCompany(int idCompany) {
		this.idCompany = idCompany;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getDescProduct() {
		return descProduct;
	}

	public void setDescProduct(String descProduct) {
		this.descProduct = descProduct;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getParametro() {
		return parametro;
	}

	public void setParametro(String parametro) {
		this.parametro = parametro;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getClaveError() {
		return claveError;
	}

	public void setClaveError(String claveError) {
		this.claveError = claveError;
	}

	public String getDescError() {
		return descError;
	}

	public void setDescError(String descError) {
		this.descError = descError;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNumAutorizacion() {
		return numAutorizacion;
	}

	public void setNumAutorizacion(String numAutorizacion) {
		this.numAutorizacion = numAutorizacion;
	}

	public String getChargeBack() {
		return chargeBack;
	}

	public void setChargeBack(String chargeBack) {
		this.chargeBack = chargeBack;
	}

	public String getChargeBackNot() {
		return chargeBackNot;
	}

	public void setChargeBackNot(String chargeBackNot) {
		this.chargeBackNot = chargeBackNot;
	}

	public int getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(int idStatus) {
		this.idStatus = idStatus;
	}

	public String getNumRefund() {
		return numRefund;
	}

	public void setNumRefund(String numRefund) {
		this.numRefund = numRefund;
	}
		
}
