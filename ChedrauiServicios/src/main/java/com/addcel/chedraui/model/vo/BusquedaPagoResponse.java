package com.addcel.chedraui.model.vo;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BusquedaPagoResponse {

	private long referenciaMC;
	private String idUsuario;
	private String email;
	private String nombre;
	private double total;
	private double monto;
	private double comision;
	private String sucursal;
	
	private List<DatosPagoProductos> productos;
		
	private String tarjeta;
	private int tipoTarjeta;
	
	private String numAutorizacion;
	private String fecha;
	
	public BusquedaPagoResponse(){
		
	}
	
	public BusquedaPagoResponse(DatosPago datosPago, DatosPagoResponse respuesta){
		this.referenciaMC = datosPago.getIdBitacora();
		this.idUsuario = datosPago.getIdUsuario();
		this.email = datosPago.getEmail();
		this.numAutorizacion = respuesta.getNumAutorizacion();
		this.monto = datosPago.getMonto();
		this.comision = datosPago.getComision();
		this.total = datosPago.getTotal();
//		this.status = datosPago.get;
		this.fecha = respuesta.getFecha();
		this.sucursal = datosPago.getSucursal();
		this.productos = datosPago.getProductos();
	}
	
	public long getReferenciaMC() {
		return referenciaMC;
	}
	public void setReferenciaMC(long referenciaMC) {
		this.referenciaMC = referenciaMC;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public double getComision() {
		return comision;
	}
	public void setComision(double comision) {
		this.comision = comision;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public List<DatosPagoProductos> getProductos() {
		return productos;
	}
	public void setProductos(List<DatosPagoProductos> productos) {
		this.productos = productos;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public int getTipoTarjeta() {
		return tipoTarjeta;
	}
	public void setTipoTarjeta(int tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
	public String getNumAutorizacion() {
		return numAutorizacion;
	}
	public void setNumAutorizacion(String numAutorizacion) {
		this.numAutorizacion = numAutorizacion;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

}
