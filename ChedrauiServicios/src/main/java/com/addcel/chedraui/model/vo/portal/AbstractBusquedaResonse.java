package com.addcel.chedraui.model.vo.portal;

import java.util.List;

public class AbstractBusquedaResonse {	
	private int totalRecords;
	private int curPage;
	private List<Object> data;
	
	public int getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}
	public int getCurPage() {
		return curPage;
	}
	public void setCurPage(int curPage) {
		this.curPage = curPage;
	}
	public List<Object> getData() {
		return data;
	}
	public void setData(List<Object> data) {
		this.data = data;
	}
	
}
