package com.addcel.chedraui.model.vo.pagos;

public class TBitacoraProsaVO{	
	private long idBitacoraProsa;
	private long idBitacora;
	private String idUsuario;
	private String tarjeta;
	private String transaccion;
	private String autorizacion;
	private String fecha;
	private String bit_hora;
	private String concepto;
	private double cargo;
	private double comision;
	private String cx;
	private String cy;
	
	public TBitacoraProsaVO(){};
	
	public TBitacoraProsaVO(TBitacoraVO tbitacora, double cargo, double comision ){
		this.idBitacora = tbitacora.getIdBitacora();
		this.idUsuario = tbitacora.getIdUsuario();
		this.tarjeta = tbitacora.getTarjetaCompra();
//		this.transaccion = tbitacora.gett;
//		this.autorizacion = tbitacora.get;
		this.concepto = tbitacora.getBitConcepto();
		this.cargo = cargo;
		this.comision = comision;
		this.cx = "0";
		this.cy = "0";
	}
	
	public TBitacoraProsaVO( int idBitacora,
		 String idUsuario,
		 String tarjeta,
		 String transaccion,
		 String autorizacion,
		 String concepto,
		 double cargo,
		 double comision,
		 String cx,
		 String cy){
		
		this.idBitacora = idBitacora;
		this.idUsuario = idUsuario;
		this.tarjeta = tarjeta;
		this.transaccion = transaccion;
		this.autorizacion = autorizacion;
		this.concepto = concepto;
		this.cargo = cargo;
		this.comision = comision;
		this.cx = cx;
		this.cy = cy;
	}
	
	public long getIdBitacoraProsa() {
		return idBitacoraProsa;
	}
	public void setIdBitacoraProsa(long idBitacoraProsa) {
		this.idBitacoraProsa = idBitacoraProsa;
	}
	public long getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public String getTransaccion() {
		return transaccion;
	}
	public void setTransaccion(String transaccion) {
		this.transaccion = transaccion;
	}
	public String getAutorizacion() {
		return autorizacion;
	}
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getBit_hora() {
		return bit_hora;
	}
	public void setBit_hora(String bit_hora) {
		this.bit_hora = bit_hora;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public double getCargo() {
		return cargo;
	}
	public void setCargo(double cargo) {
		this.cargo = cargo;
	}
	public double getComision() {
		return comision;
	}
	public void setComision(double comision) {
		this.comision = comision;
	}
	public String getCx() {
		return cx;
	}
	public void setCx(String cx) {
		this.cx = cx;
	}
	public String getCy() {
		return cy;
	}
	public void setCy(String cy) {
		this.cy = cy;
	}
	
		
}
