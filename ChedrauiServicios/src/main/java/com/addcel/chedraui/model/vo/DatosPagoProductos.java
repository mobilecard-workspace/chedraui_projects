package com.addcel.chedraui.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.addcel.chedraui.model.vo.AbstractVO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DatosPagoProductos {

	private int index;
	private String descripcion;
	private String marca;
	private String modeloProd;
	private String idProducto;
	private double monto;
	
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModeloProd() {
		return modeloProd;
	}
	public void setModeloProd(String modeloProd) {
		this.modeloProd = modeloProd;
	}
	public String getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	
}
