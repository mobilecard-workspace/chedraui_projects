package com.addcel.chedraui.model.vo;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DatosPagoResponse extends AbstractVO {

	private long referenciaMC;
	private String numAutorizacion;
	private double monto;
	private double comision;
	private double total;
	
	private int status;
	private String fecha;
	private String sucursal;
	private List<DatosPagoProductos> productos;
	
	public DatosPagoResponse(){
	}
	
	public DatosPagoResponse(int idError, String mensajeError){
		super(idError, mensajeError);
	}

	public long getReferenciaMC() {
		return referenciaMC;
	}

	public void setReferenciaMC(long referenciaMC) {
		this.referenciaMC = referenciaMC;
	}

	public String getNumAutorizacion() {
		return numAutorizacion;
	}

	public void setNumAutorizacion(String numAutorizacion) {
		this.numAutorizacion = numAutorizacion;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public String getSucursal() {
		return sucursal;
	}

	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	public List<DatosPagoProductos> getProductos() {
		return productos;
	}

	public void setProductos(List<DatosPagoProductos> productos) {
		this.productos = productos;
	}
		
}
