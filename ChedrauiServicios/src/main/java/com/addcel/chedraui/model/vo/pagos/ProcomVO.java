package com.addcel.chedraui.model.vo.pagos;

public class ProcomVO {
	// Atributos obligatorios
	private String total;
	private String currency;
	private String address;
	private String orderId;
	private String merchant;
	private String store;
	private String term;
	private String digest;
	private String urlBack;
	
	private int idError;
	private String mensajeError;
	
	public ProcomVO(int idError, String mensajeError) {
		super();
		this.idError = idError;
		this.mensajeError = mensajeError;
	}

	public ProcomVO(String total, String currency, String address,
			String orderId, String merchant, String store, String term,
			String digest, String urlBack, String usuario, String idTramite) {
		super();
		this.total = total;
		this.currency = currency;
		this.address = address;
		this.orderId = orderId;
		this.merchant = merchant;
		this.store = store;
		this.term = term;
		this.digest = digest;
		this.urlBack = urlBack;	
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getDigest() {
		return digest;
	}

	public void setDigest(String digest) {
		this.digest = digest;
	}

	public String getUrlBack() {
		return urlBack;
	}

	public void setUrlBack(String urlBack) {
		this.urlBack = urlBack;
	}

	public int getIdError() {
		return idError;
	}

	public void setIdError(int idError) {
		this.idError = idError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}	


}
