package com.addcel.chedraui.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.chedraui.model.vo.admon.TiendaVO;
import com.addcel.chedraui.model.vo.admon.UsuarioVO;
import com.addcel.chedraui.model.vo.response.LoginRespuesta;

public interface AdmonMapper {
	
	public int guardarUsuario(UsuarioVO usuario);

	public LoginRespuesta loginUsuario(@Param(value = "idUsuario") int idUsuario,
			@Param(value = "nombre") String nombre,
			@Param(value = "paterno") String paterno,
			@Param(value = "materno") String materno,
			@Param(value = "login") String login,
			@Param(value = "idSupervisor") int idSupervisor,
			@Param(value = "idTienda") int idTienda);

	public List<TiendaVO> listaTiendas(
			@Param(value = "idTienda") int idTienda,
			@Param(value = "descripcion") String descripcion);

	public int actualizarUsuario(UsuarioVO usuario);

	public List<UsuarioVO> getVendor(@Param(value = "idSupervisor") String nombre,
			@Param(value = "idUsuario") String paterno,
			@Param(value = "idTienda") String materno,
			@Param(value = "login") String login);

	public UsuarioVO userIntentosLogin(@Param(value = "login") String login);

	public int updateIntentosStatus(@Param(value = "idUsuario") int idUsuario,
			@Param(value = "valor") int valor,
			@Param(value = "update") String update);
	
	
	public String getAfiliacion(@Param(value = "idProveedor")int idProveedor);
	
	public String getParametro(@Param(value = "clave")String clave);
	
	public Double getTipoCambio(@Param(value = "idProveedor")int idProveedor);
}
