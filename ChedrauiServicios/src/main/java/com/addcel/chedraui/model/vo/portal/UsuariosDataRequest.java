package com.addcel.chedraui.model.vo.portal;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class UsuariosDataRequest extends AbstractBusquedaRequest{

	private int idUsuarioT;
	private int idSupervisorT;
	private String loginT;
	private String nombreT;
	private String aPaternoT;
	private String aMaternoT;
	private int statusT;
	public int getIdUsuarioT() {
		return idUsuarioT;
	}
	public void setIdUsuarioT(int idUsuarioT) {
		this.idUsuarioT = idUsuarioT;
	}
	public int getIdSupervisorT() {
		return idSupervisorT;
	}
	public void setIdSupervisorT(int idSupervisorT) {
		this.idSupervisorT = idSupervisorT;
	}
	public String getLoginT() {
		return loginT;
	}
	public void setLoginT(String loginT) {
		this.loginT = loginT;
	}
	public String getNombreT() {
		return nombreT;
	}
	public void setNombreT(String nombreT) {
		this.nombreT = nombreT;
	}
	public String getaPaternoT() {
		return aPaternoT;
	}
	public void setaPaternoT(String aPaternoT) {
		this.aPaternoT = aPaternoT;
	}
	public String getaMaternoT() {
		return aMaternoT;
	}
	public void setaMaternoT(String aMaternoT) {
		this.aMaternoT = aMaternoT;
	}
	public int getStatusT() {
		return statusT;
	}
	public void setStatusT(int statusT) {
		this.statusT = statusT;
	}
		
}
