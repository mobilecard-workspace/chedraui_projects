package com.addcel.chedraui.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChedrauiDetalleVO {
	
	private long referenciaMC;
	private String idUsuario;
	private String email;
	private String descripcion;
	private String sucursal;
	private String marca;
	private String modeloProd;
	private String idProducto;
	private double monto;
	private double comicion;
	private double total;
	
	private String tarjeta;
	private String numAutorizacion;
	private String fecha;
	public long getReferenciaMC() {
		return referenciaMC;
	}
	public void setReferenciaMC(long referenciaMC) {
		this.referenciaMC = referenciaMC;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModeloProd() {
		return modeloProd;
	}
	public void setModeloProd(String modeloProd) {
		this.modeloProd = modeloProd;
	}
	public String getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public double getComicion() {
		return comicion;
	}
	public void setComicion(double comicion) {
		this.comicion = comicion;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public String getNumAutorizacion() {
		return numAutorizacion;
	}
	public void setNumAutorizacion(String numAutorizacion) {
		this.numAutorizacion = numAutorizacion;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

}
