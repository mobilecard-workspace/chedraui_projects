package com.addcel.chedraui.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.chedraui.model.vo.BusquedaPagoResponse;
import com.addcel.chedraui.model.vo.CorreoVO;
import com.addcel.chedraui.model.vo.DatosPagoProductos;
import com.addcel.chedraui.model.vo.admon.UsuarioVO;
import com.addcel.utils.Utilerias;

import crypto.Crypto;

public class AddCelGenericMail {
	private static final Logger logger = LoggerFactory.getLogger(AddCelGenericMail.class);
	
	//private static final String urlString = "http://50.57.192.214:8080/MailSenderAddcel/enviaCorreoAddcel";
	private static final String urlString = "http://localhost:8080/MailSenderAddcel/enviaCorreoAddcel";
	
	private static final String HTML_DOBY = 
			"<!DOCTYPE html> <html> <head> <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\"> <style type=\"text/css\"> * { padding: 0; margin: 0; }  body { font-size: 62.5%; background-color: rgb(255, 255, 255); font-family: verdana, arial, sans-serif; }  a:link { color: #000000; font-weight: bold; }  a:active { color: #000000; font-weight: bold; }  a:visited { color: #000000; /*text-decoration: none;*/ font-weight: bold; }  a:hover { color: #FF0000; /*text-decoration: underline;*/ font-weight: bold; }  .page-container-add { width: 500px; margin: 0px auto; margin-top: 10px; margin-bottom: 10px; border: solid 1px rgb(150, 150, 150); font-size: 1.0em; }  /* HEADER */ .header-add { width: 500px; height: 144px; font-family: \"trebuchet ms\", arial, sans-serif; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .main-content-add { display: inline; /*Fix IE floating margin bug*/; float: left; width: 440px; margin: 20px 30px 10px 30px; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  /* MAIN CONTENT */ .main-content-add h1.pagetitle-add { margin: 0 0 0.4em 0; padding: 0 0 2px 0; font-family: Verdana, Geneva, sans-serif; color: #000000; font-weight: bold; font-size: 180%; }  .main-content-add p { margin: 0 0 1.0em 0; line-height: 150%; font-size: 120%; text-align: justify; color: #000000; }  .main-content-add table { clear: both; width: 100%; margin: 0 0 0 0; table-layout: fixed; border-collapse: collapse; empty-cells: show; background-color: rgb(233, 232, 244); text-align: justify; }  .main-content-add table td { height: 2em; padding: 2px 5px 2px 5px; border-left: solid 2px rgb(255, 255, 255); border-right: solid 2px rgb(255, 255, 255); border-top: solid 2px rgb(255, 255, 255); border-bottom: solid 2px rgb(255, 255, 255); background-color: rgb(225, 225, 225); /* text-align: center; */ font-weight: normal; color: #000000; font-size: 11px; }  /*  FOOTER SECTION  */ .footer-add { clear: both; width: 500px; /* height: 6em; */ padding: 5px 0 0 0; background: rgb(225, 225, 225); font-size: 9px; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .footer-add p { line-height: 150%; text-align: center; color: rgb(125, 125, 125); font-weight: bold; font-size: 9px; } </style> </head> <body> <div class=\"page-container-add\"> <div class=\"header-add\"> <img src=\"cid:identifierCID00\"> </div> <br>  <div class=\"main-content-add\">  <h1 class=\"pagetitle-add\">Estimado(a): #NOMBRE#</h1>  <p> Su compra desde la aplicaci&oacute;n <strong> Chedraui Virtual M&oacute;vil</strong> se ha realizado con &eacute;xito por medio de la plataforma <strong>MOBILECARD</strong>. </p> <br> <p align=center>El Pago ha sido efectuada con &eacute;xito.</p> <br> <table align=\"center\" cellpadding=\"2\"> <tbody> <tr> <td>Fecha y Hora:</td> <td>#FECHA#</td> </tr> <tr> <td>Monto:</td> <td>$ #MONTO# MXN</td> </tr> <tr> <td>Comisi&oacute;n:</td> <td>$ #COMISION# MXN</td> </tr> <tr> <td>Cargo aplicado:</td> <td>$ #CARGO# MXN</td> </tr> <tr> <td>Numero Referencia MC:</td> <td>#REFERENCIAMC#</td> </tr> <tr> <td>Autorizaci&oacute;n bancaria:</td> <td>#AUTORIZACION#</td> </tr> </tbody> </table> <BR> <div style=\"text-align: center; font-family: 'Courier New', Courier, monospace;\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#E6E6FA\" style=\"width: 100%; text-align: center; font-family: Courier New, Courier, monospace; font-size: 10pt; table-layout: auto;\"> <tr> <td style=\"border-width:0\" colspan=\"3\"><b>#DIRECCION#</b></td> </tr> <tr> <td style=\"border-width:0\" colspan=\"3\">#FECHAHORA#</td> </tr> <tr> <td style=\"border-width:0\" colspan=\"3\"> ====================================================== <br /> VENTA <br /> ====================================================== <br /> </td> </tr> <tr> <td style=\"width: 50%; border-width:0;\" >DESCRIPCION</td> <td style=\"width: 30%; border-width:0;\">CODIGO</td> <td style=\"width: 20%; border-width:0;\">MONTO</td> </tr> #TICKET# <tr> <td style=\"border-width:0\" colspan=\"3\"> ====================================================== <br /> PDV: #PUNTOVENTA# </td> </tr> </table> </div> <p>&nbsp;</p> <p align=center> <font size=-1>¡Gracias por comprar con <strong> Chedraui Virtual M&oacute;vil</strong>!</font> </p>  <p> Para dudas y/o aclaraciones puedes contactarnos a trav&eacute;s de los telefonos +52 (55) 55403124 o escribenos a <a href=mailto:soporte@addcel.com>soporte@addcel.com</a> </p> <p> Tambien puedes visitarnos en nuestra p&aacute;gina web: <a href=\"https://www.mobilecard.mx\" title=\"Visistar pagina MobilieCard\">www.mobilecard.mx</a> </p>  <h3>&nbsp;</h3>  </div> <div class=\"footer-add\"> <p>Nota. Este correo es de car&aacute;ter informativo, no es necesario que responda al mismo.</p> <br /> <p>Chedraui &copy; 2015 | Todos los derechos reservados.</p> <p>Powered by Addcel.</p> </div> </div> </body> </html>";
	
	private static final String HTML_DOBY_ALTA_USUARIO = 
			"<!DOCTYPE html> <html> <head> <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\"> <style type=\"text/css\"> * { padding: 0; margin: 0; }  body { font-size: 62.5%; background-color: rgb(255, 255, 255); font-family: verdana, arial, sans-serif; }  a:link { color: #000000; font-weight: bold; }  a:active { color: #000000; font-weight: bold; }  a:visited { color: #000000; /*text-decoration: none;*/ font-weight: bold; }  a:hover { color: #FF0000; /*text-decoration: underline;*/ font-weight: bold; }  .page-container-add { width: 500px; margin: 0px auto; margin-top: 10px; margin-bottom: 10px; border: solid 1px rgb(150, 150, 150); font-size: 1.0em; }  /* HEADER */ .header-add { width: 500px; height: 144px; font-family: \"trebuchet ms\", arial, sans-serif; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .main-content-add { display: inline; /*Fix IE floating margin bug*/; float: left; width: 440px; margin: 20px 30px 10px 30px; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  /* MAIN CONTENT */ .main-content-add h1.pagetitle-add { margin: 0 0 0.4em 0; padding: 0 0 2px 0; font-family: Verdana, Geneva, sans-serif; color: #000000; font-weight: bold; font-size: 180%; }  .main-content-add p { margin: 0 0 1.0em 0; line-height: 150%; font-size: 120%; text-align: justify; color: #000000; }  .main-content-add table { clear: both; width: 100%; margin: 0 0 0 0; table-layout: fixed; border-collapse: collapse; empty-cells: show; background-color: rgb(233, 232, 244); text-align: justify; }  .main-content-add table td { height: 2em; padding: 2px 5px 2px 5px; border-left: solid 2px rgb(255, 255, 255); border-right: solid 2px rgb(255, 255, 255); border-top: solid 2px rgb(255, 255, 255); border-bottom: solid 2px rgb(255, 255, 255); background-color: rgb(225, 225, 225); /* text-align: center; */ font-weight: normal; color: #000000; font-size: 11px; }  /*  FOOTER SECTION  */ .footer-add { clear: both; width: 500px; /* height: 6em; */ padding: 5px 0 0 0; background: rgb(225, 225, 225); font-size: 9px; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .footer-add p { line-height: 150%; text-align: center; color: rgb(125, 125, 125); font-weight: bold; font-size: 9px; } </style> </head> <body> <div class=\"page-container-add\"> <div class=\"header-add\"> <img src=\"cid:identifierCID00\"> </div> <br>  <div class=\"main-content-add\">  <h1 class=\"pagetitle-add\">Estimado(a): #NOMBRE#</h1>  <p> Para poder usar la aplicaci&oacute;n <strong> Chedraui Virtual M&oacute;vil</strong> sus datos de ingreso son los siguientes. </p> <br> <p> Por seguridad, la primera vez que ingrese a la aplicaci&oacute;n se le solicitara cambiar su contraseña. </p> <br> <table align=\"center\" cellpadding=\"2\"> <tbody> <tr> <td>Usuario:</td> <td>#USUARIO#</td> </tr> <tr> <td>Contraseña:</td> <td>#PASSWORD#</td> </tr> <tr> <td>Role:</td> <td>#ROLE#</td> </tr> <tr> <td>Tienda:</td> <td>#TIENDA#</td> </tr>  </tbody> </table> <BR>  <p>&nbsp;</p>  <h3>&nbsp;</h3>  </div> <div class=\"footer-add\"> <p>Nota. Este correo es de car&aacute;ter informativo, no es necesario que responda al mismo.</p> <br /> <p>Chedraui &copy; 2015 | Todos los derechos reservados.</p> <p>Powered by Addcel.</p> </div> </div> </body> </html> ";
	
	private static final String HTML_DOBY_RESET_PASS = 
			"<!DOCTYPE html> <html> <head> <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\"> <style type=\"text/css\"> * { padding: 0; margin: 0; }  body { font-size: 62.5%; background-color: rgb(255, 255, 255); font-family: verdana, arial, sans-serif; }  a:link { color: #000000; font-weight: bold; }  a:active { color: #000000; font-weight: bold; }  a:visited { color: #000000; /*text-decoration: none;*/ font-weight: bold; }  a:hover { color: #FF0000; /*text-decoration: underline;*/ font-weight: bold; }  .page-container-add { width: 500px; margin: 0px auto; margin-top: 10px; margin-bottom: 10px; border: solid 1px rgb(150, 150, 150); font-size: 1.0em; } /* HEADER */ .header-add { width: 500px; height: 144px; font-family: \"trebuchet ms\", arial, sans-serif; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .main-content-add { display: inline; /*Fix IE floating margin bug*/; float: left; width: 440px; margin: 20px 30px 10px 30px; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; } /* MAIN CONTENT */ .main-content-add h1.pagetitle-add { margin: 0 0 0.4em 0; padding: 0 0 2px 0; font-family: Verdana, Geneva, sans-serif; color: #000000; font-weight: bold; font-size: 180%; }  .main-content-add p { margin: 0 0 1.0em 0; line-height: 150%; font-size: 120%; text-align: justify; color: #000000; }  .main-content-add table { clear: both; width: 100%; margin: 0 0 0 0; table-layout: fixed; border-collapse: collapse; empty-cells: show; background-color: rgb(233, 232, 244); text-align: justify; }  .main-content-add table td { height: 2em; padding: 2px 5px 2px 5px; border-left: solid 2px rgb(255, 255, 255); border-right: solid 2px rgb(255, 255, 255); border-top: solid 2px rgb(255, 255, 255); border-bottom: solid 2px rgb(255, 255, 255); background-color: rgb(225, 225, 225); /* text-align: center; */ font-weight: normal; color: #000000; font-size: 11px; } /*  FOOTER SECTION  */ .footer-add { clear: both; width: 500px; /* height: 6em; */ padding: 5px 0 0 0; background: rgb(225, 225, 225); font-size: 9px; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .footer-add p { line-height: 150%; text-align: center; color: rgb(125, 125, 125); font-weight: bold; font-size: 9px; } </style> </head> <body> <div class=\"page-container-add\"> <div class=\"header-add\"> <img src=\"cid:identifierCID00\"> </div> <br> <div class=\"main-content-add\"> <h1 class=\"pagetitle-add\">Estimado(a): #NOMBRE#</h1> <p> Se a solicitado un reset de su contraseña de la aplicaci&oacute;n <strong> Chedraui Virtual M&oacute;vil</strong> sus datos de ingreso son los siguientes. </p> <br> <p>Por seguridad, la siguiente vez que ingrese a la aplicaci&oacute;n se le solicitara cambiar su contraseña.</p> <br> <table align=\"center\" cellpadding=\"2\"> <tbody> <tr> <td>Nueva Contraseña:</td> <td>#PASSWORD#</td> </tr> </tbody> </table> <BR> <p>&nbsp;</p> <h3>&nbsp;</h3> </div> <div class=\"footer-add\"> <p>Nota. Este correo es de car&aacute;ter informativo, no es necesario que responda al mismo.</p> <br /> <p>Chedraui &copy; 2015 | Todos los derechos reservados.</p> <p>Powered by Addcel.</p> </div> </div> </body> </html> ";

	public static CorreoVO generatedMail(BusquedaPagoResponse detalle){
		logger.info("Genera objeto para envio de mail: " + detalle.getEmail());
		
		CorreoVO correo = new CorreoVO();
		StringBuilder sbCampos = null;
		try{
			
			sbCampos = new StringBuilder();
            for(DatosPagoProductos campo: detalle.getProductos()){
            	sbCampos.append("<tr>");
            	sbCampos.append("<td style=\"border-width:0; text-align: left;\">");
            	sbCampos.append(campo.getDescripcion().length()> 20?campo.getDescripcion().toUpperCase().substring(0, 20): campo.getDescripcion().toUpperCase());
            	sbCampos.append("</td>");
            	sbCampos.append("<td style=\"border-width:0; text-align: left;\">");
            	sbCampos.append(campo.getIdProducto());
            	sbCampos.append("</td>");
            	sbCampos.append("<td style=\"border-width:0; text-align: left;\">");
            	sbCampos.append(espacios(Utilerias.formatoImporteMon(campo.getMonto()), 9));
            	sbCampos.append("</td>");
            }
            
            sbCampos.append("<tr><td style=\"border-width:0; text-align: left;\"></td>");
        	sbCampos.append("<td style=\"border-width:0; text-align: left;\">TOTAL:</td>");
        	sbCampos.append("<td style=\"border-width:0; text-align: left;\">");
        	sbCampos.append(espacios(Utilerias.formatoImporteMon(detalle.getTotal()), 9));
        	sbCampos.append("</td>");
			
	//		String[] attachments = {src_file};
			String body = HTML_DOBY.toString();
			body = body.replaceAll("#NOMBRE#", detalle.getNombre() != null ? detalle.getNombre() : "");
			body = body.replaceAll("#MONTO#", Utilerias.formatoImporteMon( detalle.getMonto()));
			body = body.replaceAll("#COMISION#", Utilerias.formatoImporteMon( detalle.getComision()));
			body = body.replaceAll("#CARGO#", Utilerias.formatoImporteMon( detalle.getTotal()));
			body = body.replaceAll("#FECHA#", detalle.getFecha()!= null ? detalle.getFecha().substring(0, 19) : "");
			body = body.replaceAll("#AUTORIZACION#", detalle.getNumAutorizacion()!= null ? detalle.getNumAutorizacion() : "");
			body = body.replaceAll("#REFERENCIAMC#", detalle.getReferenciaMC() + "");
			
			body = body.replaceAll("#DIRECCION#", "<strong>CHEDRAUI<BR>Mexico, D.F.<BR><BR></strong>");
			body = body.replaceAll("#PUNTOVENTA#", detalle.getSucursal() != null ? detalle.getSucursal() : "");
			body = body.replaceAll("#FECHAHORA#", "<strong>www.chedraui.com.mx</strong><BR>" + detalle.getFecha());
			
			body = body.replaceAll("#TICKET#", sbCampos.toString().replaceAll("\\$", "\\\\\\$"));
			
			String from = "no-reply@addcel.com";
			String subject = "Acuse Pago CHEDRAUI - Referencia: " + detalle.getReferenciaMC();
			String[] to = {detalle.getEmail()};
			String[] cid = {"/usr/java/resources/images/Chedraui/logo_chedraui.png"};
	
	//		correo.setAttachments(attachments);
			correo.setBcc(new String[]{"elmans8@gmail.com"});
			correo.setCc(new String[]{});
			correo.setCid(cid);
			correo.setBody(body);
			correo.setFrom(from);
			correo.setSubject(subject);
			correo.setTo(to);
			
//			sendMail(correo);
//			logger.info("Fin proceso de envio email");
		}catch(Exception e){
			correo = null;
			logger.error("Ocurrio un error al enviar el email ", e);
		}
		return correo;
	}
	
	
	public static CorreoVO generatedMailAltaUsuario(UsuarioVO usuario, String password, String tienda){
		logger.info("Genera objeto para envio de mail: " + usuario.getMail());
		
		CorreoVO correo = new CorreoVO();
		try{
			
			String body = HTML_DOBY_ALTA_USUARIO.toString();
			body = body.replaceAll("#NOMBRE#", usuario.getNombre() != null ? usuario.getNombre() : "");
			body = body.replaceAll("#USUARIO#", usuario.getLogin());
			body = body.replaceAll("#PASSWORD#", password);
			body = body.replaceAll("#ROLE#", usuario.getIdRol() == 101? "Supervisor": "Vendedor");
			body = body.replaceAll("#TIENDA#", Crypto.replaceAcentoHTML(tienda));
			
			String from = "no-reply@addcel.com";
			String subject = "Alta Usuario - Chedraui Virtual Movil";
			String[] to = {usuario.getMail()};
			String[] cid = {"/usr/java/resources/images/Chedraui/logo_chedraui.png"};
	
	//		correo.setAttachments(attachments);
			correo.setBcc(new String[]{"elmans8@gmail.com"});
			correo.setCc(new String[]{});
			correo.setCid(cid);
			correo.setBody(body);
			correo.setFrom(from);
			correo.setSubject(subject);
			correo.setTo(to);
			
//			sendMail(correo);
//			logger.info("Fin proceso de envio email");
		}catch(Exception e){
			correo = null;
			logger.error("Ocurrio un error al enviar el email ", e);
		}
		return correo;
	}
	
	public static CorreoVO generatedResetPassword(UsuarioVO usuario, String password){
		logger.info("Genera objeto para envio de mail: " + usuario.getMail());
		
		CorreoVO correo = new CorreoVO();
		try{
			
			String body = HTML_DOBY_RESET_PASS.toString();
			body = body.replaceAll("#NOMBRE#", usuario.getNombre() != null ? usuario.getNombre() : "");
//			body = body.replaceAll("#USUARIO#", usuario.getLogin());
			body = body.replaceAll("#PASSWORD#", password);
//			body = body.replaceAll("#ROLE#", usuario.getIdRol() == 101? "Supervisor": "Vendedor");
//			body = body.replaceAll("#TIENDA#", Crypto.replaceAcentoHTML(tienda));
			
			String from = "no-reply@addcel.com";
			String subject = "Reset Contraseña - Chedraui Virtual Movil";
			String[] to = {usuario.getMail()};
			String[] cid = {"/usr/java/resources/images/Chedraui/logo_chedraui.png"};
	
	//		correo.setAttachments(attachments);
			correo.setBcc(new String[]{"elmans8@gmail.com"});
			correo.setCc(new String[]{});
			correo.setCid(cid);
			correo.setBody(body);
			correo.setFrom(from);
			correo.setSubject(subject);
			correo.setTo(to);
			
//			sendMail(correo);
//			logger.info("Fin proceso de envio email");
		}catch(Exception e){
			correo = null;
			logger.error("Ocurrio un error al enviar el email ", e);
		}
		return correo;
	}
	
	private static String espacios(String campo, int numero){
		StringBuilder respuesta = new StringBuilder();
		for(int i = 0; i < numero - campo.length(); i++){
			respuesta.append("&nbsp;");
		}
		
		return "$&nbsp;" + respuesta + campo;
	}

	public static void sendMail(String data) {
		String line = null;
		StringBuilder sb = new StringBuilder();
		try {
			logger.info("Iniciando proceso de envio email. ");
			logger.info("data = " + data);

			URL url = new URL(urlString);
			logger.info("Conectando con " + urlString);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("Accept", "application/json");
			urlConnection.setRequestMethod("POST");

			OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
			writter.write(data);
			writter.flush();

			logger.info("Datos enviados, esperando respuesta");

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					urlConnection.getInputStream()));

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}

			logger.info("Respuesta del servidor " + sb.toString());
		} catch (Exception ex) {
			logger.error("Error en: sendMail, al enviar el email ", ex);
		}
	}
}
