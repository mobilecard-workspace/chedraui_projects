package com.addcel.chedraui.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.chedraui.model.mapper.AdmonMapper;
import com.addcel.chedraui.model.mapper.PortalMapper;
import com.addcel.chedraui.model.vo.admon.LoginVO;
import com.addcel.chedraui.model.vo.admon.TiendaVO;
import com.addcel.chedraui.model.vo.admon.UsuarioVO;
import com.addcel.chedraui.model.vo.portal.TiendaDataRequest;
import com.addcel.chedraui.model.vo.portal.UsuariosDataRequest;
import com.addcel.chedraui.services.ChedrauiPortalService;

/**
 * Handles requests for the application home page.
 */
@Controller
@SessionAttributes({"usuario"})
public class ChedrauiPortalController {
	
	private static final Logger logger = LoggerFactory.getLogger(ChedrauiPortalController.class);
	
	@Autowired
	private AdmonMapper mapperAdm;
	
	@Autowired
	private PortalMapper mapperPortal;
	
	@Autowired
	private ChedrauiPortalService servPortal;
	
	@RequestMapping(value = "/")
	public ModelAndView login(ModelMap modelo) {	
		logger.debug("Dentro del servicio: /");
		ModelAndView mav = null;
		try{
			logger.debug("Iniciar pantalla, login" );
			mav = new ModelAndView("portal/login", "usuario", new LoginVO());
		}catch(Exception e){
			mav = new ModelAndView("portal/error");
			mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
		}
		return mav;	
	}
	
	@RequestMapping(value = "/portal/login")
	public ModelAndView login(@ModelAttribute("login") LoginVO login, ModelMap modelo) {	
		logger.debug("Dentro del servicio: /login");
		ModelAndView mav = null;
		try{
			logger.debug("Iniciar pantalla, login" );
			mav = new ModelAndView("portal/login", "usuario", new LoginVO());
		}catch(Exception e){
			mav = new ModelAndView("portal/error");
			mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
		}
		
		return mav;	
	}
	
	@RequestMapping(value = "/portal/login-final", method=RequestMethod.POST)
	public ModelAndView loginFinal( @ModelAttribute("login") LoginVO login, ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/loginFinal");
		return servPortal.loginFinal(login, modelo);	
	}
	
	@RequestMapping(value = "/portal/logout", method = {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView logout(SessionStatus status, ModelMap modelo) {
		ModelAndView mav = null;
		try{
			status.setComplete();
			modelo.remove("usuario");
			logger.debug("Cerrando session y redireccionando a la pagina de login." );
			mav = new ModelAndView("portal/login", "usuario", new LoginVO());
		}catch(Exception e){
			logger.error("Ocurrio un error general al cerrar session: {}", e );
			mav = new ModelAndView("error");
			mav.addObject("mensaje", "Ocurrio un error general: " + e.getMessage());
		}

		return mav;
	}
	
	@RequestMapping(value = "/portal/tiendaAlta", method = {RequestMethod.POST})
	public ModelAndView tiendaAlta( ModelMap modelo ) {
		logger.debug("Dentro del servicio: /portal/tiendaAlta");
		ModelAndView mav = null;
		try{
			mav = new ModelAndView("portal/tiendaAlta");
				
		}catch(Exception e){
			mav = new ModelAndView("error");
			logger.error("Ocurrio un error general en el servicio /portal/busquedaTienda: {}", e );
		}
		return mav;	
	}
	
	@RequestMapping(value = "/portal/tiendaAlta-final", method = {RequestMethod.POST})
	public ModelAndView tiendaAltaFinal(@ModelAttribute("tienda") TiendaVO tienda, ModelMap modelo ) {
		logger.debug("Dentro del servicio: /portal/tiendaAltaFinal");
		return servPortal.tiendaAltaFinal(tienda, modelo);	
	}
	
	@RequestMapping(value = "/portal/tiendasBusqueda", method = {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView tiendasBusqueda( ModelMap modelo ) {
		logger.debug("Dentro del servicio: /portal/tiendasBusqueda");
		ModelAndView mav = null;
		try{
			mav = new ModelAndView("portal/tiendasBusqueda");
			
//			if(usuario.getRol() == 1){
//				mav.addObject("companias", webService.buscarCompanias());
//			}
		}catch(Exception e){
			mav = new ModelAndView("error");
			logger.error("Ocurrio un error general en el servicio /portal/busquedaTienda: {}", e );
		}
		return mav;	
	}
	
	@RequestMapping(value = "/portal/tiendasFiltro", method = {RequestMethod.POST, RequestMethod.GET})
	public @ResponseBody String tiendasFiltro(@ModelAttribute("SearchDataRequest") TiendaDataRequest filtroTiendas, ModelMap modelo){
		String json = null;
		try{
			json = servPortal.buscarTiendasPortal(filtroTiendas, modelo);
			
		}catch(Exception e){
			logger.error("Ocurrio un error general en el servicio /portal/busquedaTienda: {}", e );
		}
		return json;	
	}
	
	
	@RequestMapping(value = "/portal/json/actualizarTienda", method = RequestMethod.POST)
	public @ResponseBody String recordUpdate(@RequestBody TiendaVO tienda, ModelMap modelo){
		String json = null;
		try{
			json = servPortal.actualizarTienda(tienda, modelo);
			
		}catch(Exception e){
			logger.error("Ocurrio un error general en el servicio /portal/json/actualizarTienda: {}", e );
		}
		return json;	
	}
	
	@RequestMapping(value = "/portal/usuariosBusqueda", method = {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView usuariosBusqueda( ModelMap modelo ) {
		logger.debug("Dentro del servicio: /portal/usuariosBusqueda");
		ModelAndView mav = null;
		try{
			mav = new ModelAndView("portal/usuariosBusqueda");
			
//			if(usuario.getRol() == 1){
//				mav.addObject("companias", webService.buscarCompanias());
//			}
		}catch(Exception e){
			mav = new ModelAndView("error");
			logger.error("Ocurrio un error general en el servicio /portal/usuariosBusqueda: {}", e );
		}
		return mav;	
	}
	
	@RequestMapping(value = "/portal/usuariosFiltro", method = {RequestMethod.POST, RequestMethod.GET})
	public @ResponseBody String usuariosFiltro(@ModelAttribute("SearchDataRequest") UsuariosDataRequest busquedaUsuario, ModelMap modelo){
		String json = null;
		try{
			json = servPortal.buscarUsuariosPortal(busquedaUsuario, modelo);
			
		}catch(Exception e){
			logger.error("Ocurrio un error general en el servicio /portal/usuariosFiltro: {}", e );
		}
		return json;	
	}
	
	
	@RequestMapping(value = "/portal/json/actualizarUsuario", method = RequestMethod.POST)
	public @ResponseBody String recordUsuarioUpdate(@RequestBody UsuarioVO usuario, ModelMap modelo){
		String json = null;
		try{
			json = servPortal.actualizarUsuario(usuario, modelo);
			
		}catch(Exception e){
			logger.error("Ocurrio un error general en el servicio /portal/json/actualizarUsuario: {}", e );
		}
		return json;	
	}
	
	@RequestMapping(value = "/portal/json/actualizarPassword", method = RequestMethod.POST)
	public @ResponseBody String actualizarPassword(@RequestBody UsuarioVO usuario, ModelMap modelo){
		String json = null;
		try{
			json = servPortal.actualizarPassword(usuario, modelo);
			
		}catch(Exception e){
			logger.error("Ocurrio un error general en el servicio /portal/json/actualizarUsuario: {}", e );
		}
		return json;	
	}
	
//	@RequestMapping(value = "/portal/exportSearchTXT", method = {RequestMethod.POST, RequestMethod.GET})
//	public void exportSearchTXT(@ModelAttribute("PaymentDataRequest") SearchDataRequest filtroTiendas, ModelMap modelo, HttpServletRequest request, HttpServletResponse response){
//		try{
//			webService.exportarBusquedaTXT(filtroTiendas, (UsuarioVO) modelo.get("usuario"), request, response);
//			
//		}catch(Exception e){
//			logger.error("Ocurrio un error general en el servicio /portal/busquedaTienda: {}", e );
//		}
//	}
	
//	@RequestMapping(value = "/portal/busquedaTransaccion", method = {RequestMethod.POST, RequestMethod.GET})
//	public ModelAndView busquedaTransaccion( ModelMap modelo ) {
//		logger.debug("Dentro del servicio: /portal/busquedaTienda");
//		ModelAndView mav = null;
//		UsuarioVO usuario = null;
//		try{
//			usuario = (UsuarioVO) modelo.get("usuario");
//			mav = webService.validarSessionController(usuario);
//			if(mav == null){
//				logger.debug("Inicia pantalla , portal/busquedaTienda." );
//				mav = new ModelAndView("portal/busquedaTransaccion");
//				
//				mav.addObject("usuario", usuario);
//				
//				if(usuario.getRol() == 1){
//					mav.addObject("companias", webService.buscarCompanias());
//				}
//			}
//		}catch(Exception e){
//			mav = new ModelAndView("error");
//			logger.error("Ocurrio un error general en el servicio /portal/busquedaTienda: {}", e );
//		}
//		return mav;	
//	}
//	
//	@RequestMapping(value = "/portal/filtroTransaccion", method = {RequestMethod.POST, RequestMethod.GET})
//	public @ResponseBody String filtroTransaccion(@ModelAttribute("PaymentDataRequest") PaymentDataRequest filtroTransaccion, ModelMap modelo){
//		String json = null;
//		try{
//			json = webService.busquedaTransaccion(filtroTransaccion, (UsuarioVO) modelo.get("usuario"));
//			
//		}catch(Exception e){
//			logger.error("Ocurrio un error general en el servicio /portal/busquedaTienda: {}", e );
//		}
//		return json;	
//	}
//	
//	
//	@RequestMapping(value = "/portal/recordUpdate", method = RequestMethod.POST)
//	public @ResponseBody String recordUpdate(@RequestBody PaymentData paymentData, ModelMap modelo){
//		String json = null;
//		try{
//			json = webService.actulizarRegistro(paymentData, (UsuarioVO) modelo.get("usuario"));
//			
//		}catch(Exception e){
//			logger.error("Ocurrio un error general en el servicio /portal/recordUpdate: {}", e );
//		}
//		return json;	
//	}
//	
//	@RequestMapping(value = "/portal/exportSearchTXT", method = {RequestMethod.POST, RequestMethod.GET})
//	public void exportSearchTXT(@ModelAttribute("PaymentDataRequest") PaymentDataRequest filtroTransaccion, ModelMap modelo, HttpServletRequest request, HttpServletResponse response){
//		try{
//			webService.exportarBusquedaTXT(filtroTransaccion, (UsuarioVO) modelo.get("usuario"), request, response);
//			
//		}catch(Exception e){
//			logger.error("Ocurrio un error general en el servicio /portal/busquedaTienda: {}", e );
//		}
//	}
	
}