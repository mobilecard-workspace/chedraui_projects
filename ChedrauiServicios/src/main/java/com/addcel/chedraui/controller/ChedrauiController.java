package com.addcel.chedraui.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.addcel.chedraui.services.ChedrauiPagosService;
import com.addcel.chedraui.services.ChedrauiService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class ChedrauiController {
	
	private static final Logger logger = LoggerFactory.getLogger(ChedrauiController.class);
	
	@Autowired
	private ChedrauiService service;
	
	@Autowired
	private ChedrauiPagosService chedrauiService;
	
	
	@RequestMapping(value = "/altaUsuario"/*,method=RequestMethod.POST*/)
	public @ResponseBody String altaUsuario(@RequestParam("json") String jsonEnc) {
		logger.debug("Dentro del servicio: /altaUsuario");
		return service.guardarUsuario(jsonEnc);
	}
	
	@RequestMapping(value = "/cambioUsuario"/*,method=RequestMethod.POST*/)
	public @ResponseBody String cambioUsuario(@RequestParam("json") String jsonEnc) {
		logger.debug("Dentro del servicio: /cambioUsuario");
		return service.actualizarUsuario(jsonEnc);
	}
	
	@RequestMapping(value = "/cambioPassword"/*,method=RequestMethod.POST*/)
	public @ResponseBody String cambioPassword(@RequestParam("json") String jsonEnc) {
		logger.debug("Dentro del servicio: /cambioPassword");
		return service.actualizarPassword(jsonEnc);
	}
	
	@RequestMapping(value = "/login"/*,method=RequestMethod.POST*/)
	public @ResponseBody String login(@RequestParam("json") String jsonEnc) {
		logger.debug("Dentro del servicio: /login");
		return service.login(jsonEnc);
	}
	
	@RequestMapping(value = "/getTiendas"/*,method=RequestMethod.POST*/)
	public @ResponseBody String getTiendas(@RequestParam("json") String jsonEnc) {
		logger.debug("Dentro del servicio: /getTiendas");
		return service.getTiendas(jsonEnc);
	}
	
	@RequestMapping(value = "/getRoles"/*,method=RequestMethod.POST*/)
	public @ResponseBody String getRoles(@RequestParam("json") String jsonEnc) {
		logger.debug("Dentro del servicio: /getRoles");
		return service.getRoles(jsonEnc);
	}
	
	@RequestMapping(value = "/getToken"/*,method=RequestMethod.POST*/)
	public @ResponseBody String getToken(@RequestParam("json") String jsonEnc) {
		logger.debug("Dentro del servicio: /getToken");
		return service.generaToken(jsonEnc);
	}
			
	@RequestMapping(value = "/pagoProductos"/*,method=RequestMethod.POST*/)
	public @ResponseBody String pagoProductos(@RequestParam("json") String jsonEnc) {	
		logger.info("Dentro del servicio: /pago-visa");
		return chedrauiService.pagoProductos(jsonEnc);	
	}
	
	@RequestMapping(value = "/busquedaPagos"/*,method=RequestMethod.POST*/) 
	public @ResponseBody String busquedaPagos(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: /busquedaPagos");
		return service.busquedaPagos(data);
	}
	
	@RequestMapping(value = "/reenvioRecibo"/*,method=RequestMethod.POST*/) 
	public @ResponseBody String reenvioRec(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: /reenvioRecibo");
		return service.reenvioRecibo(data);
	}
}