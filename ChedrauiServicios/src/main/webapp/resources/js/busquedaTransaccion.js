$(function() {
	
	$( "#startDate" ).datepicker({
		changeMonth: true, 
		changeYear: true,
		/*minDate: "-6m",*/
		maxDate: "+0d",
		defaultDate: new Date(),
		showButtonPanel: true,
		dateFormat: "yy-mm-dd"
	});
	
	$( "#endDate" ).datepicker({
		changeMonth: true, 
		changeYear: true,
		/*minDate: "-6m",*/
		maxDate: "+0d",
		defaultDate: -1,
		showButtonPanel: true,
		dateFormat: "yy-mm-dd"
	});
	
	$("#exportTXT").click(function(){
		$(location).attr('href', '/AddcelEuropaWeb/portal/exportSearchTXT?' + $("#currentSearch").val());
	});
	
	/************************************************************
	 * 
	 */
	
	       //define colModel
     var colM = [{ title: "ID Reference", width: 70, dataType: "string", dataIndx: "idBitacora" },
                 { title: "Company", width: 100, dataType: "string", dataIndx: "company" },
                 { title: "Status", width: 70, dataType: "string",  dataIndx: "status", 
                	 render: function (ui) {
                		var rowData = ui.rowData; 
                        if (rowData["status"] == 'Approved') {
                            return "<span style='color:green;'>" + rowData["status"] + "</span>";
                        }
                        else if (rowData["status"] == 'Disputed' || rowData["status"] == 'ChargeBack') {
                        	return "<span style='color:magenta;'>" + rowData["status"] + "</span>";
                        }
                        else if (rowData["status"] == 'Rejected') {
                        	return "<span style='color:red;'>" + rowData["status"] + "</span>";
                        }
                        else {
                        	return "<span style='color:blue;'><strong>" + rowData["status"] + "</strong></span>";
                        }
                     }},
                 { title: "Customer Name", width: 100, dataType: "string",  dataIndx: "fullName" },
                 { title: "Card", width: 50, dataType: "string",  dataIndx: "cardNumber", align:"right"  },
                 { title: "Total", width: 70, dataType: "string",  dataIndx: "total", align:"right"  },
                 { title: "Mon", width: 35, dataType: "string",  dataIndx: "moneda", align:"center"},
                 { title: "Num Autho", width: 60, dataType: "string",  dataIndx: "numAutorizacion", align:"right" },
                 { title: "ARN", width: 70, dataType: "string",  dataIndx: "numRefund", align:"right" },
                 { title: "Date", width: 120, dataType: "string",  dataIndx: "fecha" },
                 { title: "Cod Error", width: 30, dataType: "string",  dataIndx: "claveError" },
                 { title: "Des Error", width: 120, dataType: "string",  dataIndx: "descError" },
                 { title: "Desc Product", width: 200, dataType: "string", dataIndx: "descProduct" },
                 { title: "Parameter", width: 200, dataType: "string", dataIndx: "parametro" },
                 { title: "ChargeBack", width: 70, dataType: "string", dataIndx: "chargeBack", 
                	 render: function (ui) {
                 		var rowData = ui.rowData; 
                         if (rowData["chargeBack"] != '') {
                             return "<a href='javascript:void(0)'><span>" + rowData["chargeBack"] + "</span></a>";
                         }
                      }, editable: false},
                 { title: "ChargeBack Can", width: 70, dataType: "string", dataIndx: "chargeBackNot" , 
                	 render: function (ui) {
                  		var rowData = ui.rowData; 
                          if (rowData["chargeBackNot"] != '') {
                              return "<a href='javascript:void(0)' ><span>" + rowData["chargeBackNot"] + "</span></a>";
                          }
                       }, editable: false}
                 ];
     
     //define dataModel
     var dataModel = {
         location: "remote",
         sorting: "remote",
         paging: "local",
         dataType: "JSON",
         method: "POST",
         curPage: 1,
         rPP: 20,
         sortIndx: 0,
         sortDir: "up",
         rPPOptions: [20, 50, 100],
         filterIndx: "",
         filterValue: "",
         getUrl: function () {
             var sortDir = (this.sortDir == "up") ? "asc" : "desc";
             /*var sort = ['idBitacora', 'company', 'status', 'fullName', 'cardNumber', 'total', 'moneda',
                          'claveError', 'descError', 'descProduct', 'parametro'];*/
             var queryString = "curPage=" + this.curPage + "&recordsPerPage=" + this.rPP + 
                 "&sortBy=" + this.sortIndx + "&dir=" + sortDir
                 //+ "&tipoBusqueda=" + $("input[name='tipoBusqueda']:checked").val()
                 + "&status=" + $("input[name='status']:checked").val();
             if($("#company").val() != null){
            	 queryString = queryString + "&idCompany=" + $("#company").val();
             }
             if($("#startDate").val() != null){
            	 queryString = queryString + "&tipoBusqueda=0&startDate=" + $("#startDate").val();
             }else{
            	 queryString = queryString +"&tipoBusqueda=" + $("input[name='tipoBusqueda']:checked").val();
             }
             if($("#endDate").val() != null){
            	 queryString = queryString + "&endDate=" + $("#endDate").val();
             }
        	 queryString = queryString + "&reference=" + $("#reference").val();
        	 queryString = queryString + "&numAutho=" + $("#numAutho").val();
        	 
        	 $("#currentSearch").val(queryString);
        	 
             var obj = { url: "/AddcelEuropaWeb/portal/filtroTransaccion", data: queryString };
             return obj;
         },
         getData: function (dataJSON) {
             return { curPage: dataJSON.curPage, totalRecords: dataJSON.totalRecords, data: dataJSON.data };
         }
     };
     
     var obj = { 
    		 dataModel: dataModel,
    	     colModel: colM,
    		 width: 984, 
    		 height: 600, 
    		 title: "Grid Transacctions", 
    		 flexHeight: false, 
    		 flexWidth: false,
    		 topVisible: false, 
    		 bottomVisible: true,
    		 columnBorders: true, 
    		 rowBorders: true, 
    		 oddRowsHighlight: true, 
    		 numberCell: false, 
    		 scrollModel: {horizontal: true}, 
    		 resizable: false, 
    		 draggable: false,
    		 editable: true
    		 //freezeCols: 3
    	};
     
    var $grid = $("#grid_json").pqGrid(obj);
    
    $("#searchButton").click(function(){
    	$grid.pqGrid("refreshDataAndView");
	});
    
    /*$grid.pqGrid("option", $.paramquery.pqGrid.regional["es"]);
    $grid.find(".pq-pager").pqPager("option", $.paramquery.pqPager.regional["es"]);
	*/
	/*************************************************************
	 * Fin Busqueda de Transaccion
	 */
    
    $grid.on( "pqgridcellclick", function( event, ui ) {
    	
    	if(ui.colIndx != null){
    		if (ui.colIndx == 14) {
    			updateChargeBack(event, ui, 3 );
            }else if (ui.colIndx == 15 ) {
            	updateChargeBack(event, ui, 4);
            }
    	}
    } );
    
    
    function updateChargeBack(event, ui, status ){
    	var rowData = ui.dataModel.data;
		//$('input[type="submit"]').attr('disabled','disabled');
	
		if( (status == 3 && rowData[ui.rowIndx]["chargeBack"] == '') ||
				(status == 4 && rowData[ui.rowIndx]["chargeBackNot"] == '') ){
			return;
		}
		
    	var dataJson = {
    			"idBitacora" : rowData[ui.rowIndx]["idBitacora"] ,
    			"idStatus": ( rowData[ui.rowIndx]["chargeBack"] == 'Notif Dispute'? 3:
    						  rowData[ui.rowIndx]["chargeBack"] == 'Finish ChargeBack' && status == 3? 4:
    					      rowData[ui.rowIndx]["chargeBackNot"] == 'Cancel Dispute'  && status == 4? 1:0)
    	};
    	var mensage = dataJson.idStatus == 3? 'Notification Dispute':
						dataJson.idStatus == 4? 'ChargeBack':
							dataJson.idStatus == 1? 'Approved':'Unknown';
    	
    	var caja1 = $('<div title="System Message"> Are you sure to change the status to: ' + mensage +'</div>');
		caja1.dialog({
			dialogClass: "alert",
			modal: true,
			resizable: false,
			draggable: false,
			buttons: [ {
				text: "OK",
				click: function() {
					$( this ).dialog( "close" );
					$.ajax({
						 url: '/AddcelEuropaWeb/portal/recordUpdate',
						 type: 'POST',
						 data: JSON.stringify(dataJson),
						 dataType: 'json',
						 contentType: "application/json; charset=utf-8",
						 mimeType: 'application/json; charset=utf-8',
						 async: false,    //Cross-domain requests and dataType: "jsonp" requests do not support synchronous operation
			             cache: false,    //This will force requested pages not to be cached by the browser          
			             processData:false, //To avoid making query String instead of JSON
						 statusCode: {
							 404: function() {
							 alert( "Pagina no encontrada." );
							 }
						 },
						 success: function (data) {
							//$('input[type="submit"]').removeAttr('disabled');

						 	var caja2 = $('<div title="' + (data.idError == 0?'System Message' :' Error' ) + '">' + data.mensajeError +'</div>');
							caja2.dialog({
								dialogClass: "alert",
								modal: true,
								resizable: false,
								draggable: false,
								buttons: [ {
									text: "OK",
									click: function() {
										if(data.idError == ID_ERROR_SESSION ){
											$(location).attr('href', '/AddcelEuropaWeb/');
										}else if(data.idError == 0){
									 		$grid.pqGrid("refreshDataAndView");
										}
										$( this ).dialog( "close" );
									}
								}]
						    });
						 	//$( "#cargando" ).dialog( "close" );
						 },
						 error: function (jqXHR, textStatus, errorThrown) {
							//$( "#cargando" ).dialog( "close" );
						 	alert('An error has occured!! \njqXHR: ' + jqXHR +
								 	'\ntextStatus: ' +  textStatus +
								 	'\nerrorThrown: ' + errorThrown);
						 }
					}); 
				}
			}, {
				text: "Cancel",
				click: function() {
					$( this ).dialog( "close" );
				}
			}]
	    });
    }
});