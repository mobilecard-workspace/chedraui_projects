$(function() {
	
	$("#exportTXT").click(function(){
		if($("#currentSearch").val() != ''){
			$(location).attr('href', '/ChedrauiServicios/portal/exportSearchTXT?' + $("#currentSearch").val());
		}
	});
	
	validActualizar = $("#actualizarForm").validate({
        rules :{
        	descripcion : {
                required  : true,
                minlength : 3, //para validar campo con minimo 3 caracteres
                maxlength : 45  //para validar campo con maximo 10 caracteres
            },
            codigo : {
            	required :true,
            	digits   : true,
                maxlength : 4
            },
            userFTP : {
            	required :true,
            	minlength : 3,
                maxlength : 20,
            },
            passFTP : {
            	required :true,
            	minlength : 3,
                maxlength : 20,
            },
            urlFTP: {
            	required :true,
            	minlength : 6,
                maxlength : 20,
            },
            posMobile: {
            	digits   : true,
            	maxlength : 1,
            },
            rompeFilas : {
            	digits   : true,
            	maxlength : 1,
            }
        }
    });
	
	/************************************************************
	 * 
	 */
	
	       //define colModel
     var colM = [{ title: "ID Tienda", width: 70, dataType: "string", dataIndx: "idTienda" },
                 { title: "Nombre", width: 150, dataType: "string", dataIndx: "descripcion" },
                 { title: "Codigo", width: 100, dataType: "string",  dataIndx: "codigo" },
                 { title: "Estatus", width: 70, dataType: "string",  dataIndx: "status", 
                	 render: function (ui) {
                		var rowData = ui.rowData; 
                        if (rowData["status"] == '1') {
                            return "<span style='color:green;'>Activo</span>";
                        }
                        else {
                        	return "<span style='color:red;'>Desactivo</span>";
                        }
                     }},
                 { title: "Editar", width: 70, dataType: "string",  dataIndx: "status", 
                	 render: function (ui) {
                		 var rowData = ui.rowData; 
                         if (rowData["chargeBack"] != '') {
                             return "<a href='javascript:void(0)'><span>Editar...</span></a>";
                         }
                      }, editable: false},
                 { title: "URL FTP", width: 150, dataType: "string",  dataIndx: "urlFTP"  },
                 { title: "USER FTP", width: 80, dataType: "string",  dataIndx: "userFTP"  },
                 { title: "PASS TP", width: 80, dataType: "string",  dataIndx: "passFTP" },
                 { title: "POS MOBILE", width: 70, dataType: "string",  dataIndx: "posMobile",  
                	 render: function (ui) {
                 		var rowData = ui.rowData; 
                         if (rowData["posMobile"] == '1') {
                             return "<span style='color:green;'>Activo</span>";
                         }
                         else {
                         	return "<span style='color:red;'>Desactivo</span>";
                         }
                      }},
                 { title: "ROMPE FILAS", width: 70, dataType: "string",  dataIndx: "rompeFilas",  
                	 render: function (ui) {
                 		var rowData = ui.rowData; 
                         if (rowData["rompeFilas"] == '1') {
                        	    return "<span style='color:green;'>Activo</span>";
                         }
                         else {
                         	return "<span style='color:red;'>Desactivo</span>";
                         }
                      }},
                 ];
     
     //define dataModel
     var dataModel = {
         location: "remote",
         sorting: "remote",
         paging: "local",
         dataType: "JSON",
         method: "POST",
         curPage: 1,
         rPP: 20,
         sortIndx: 0,
         sortDir: "up",
         rPPOptions: [20, 50, 100],
         filterIndx: "",
         filterValue: "",
         getUrl: function () {
             var sortDir = (this.sortDir == "up") ? "asc" : "desc";
             /*var sort = ['idBitacora', 'company', 'status', 'fullName', 'cardNumber', 'total', 'moneda',
                          'claveError', 'descError', 'descProduct', 'parametro'];*/
             var queryString = "curPage=" + this.curPage + "&recordsPerPage=" + this.rPP + 
                 "&sortBy=" + this.sortIndx + 
                 "&dir=" + sortDir +
                 "&codigoT=" + $("#codigoT").val() +
             	 "&nombreT=" + $("#nombreT").val() +
        	 	 "&urlT=" + $("#urlT").val() +
        	 	 "&posMobile=" + ($("#posMobile:checked").val()? "1":"0") +
        	 	 "&rompeFilas=" + ($("#rompeFilas:checked").val()? "1":"0") ;
             
        	 $("#currentSearch").val(queryString);
        	 
             var obj = { url: "/ChedrauiServicios/portal/tiendasFiltro", data: queryString };
             return obj;
         },
         getData: function (dataJSON) {
             return { curPage: dataJSON.curPage, totalRecords: dataJSON.totalRecords, data: dataJSON.data };
         }
     };
     
     var obj = { 
    		 dataModel: dataModel,
    	     colModel: colM,
    		 width: 984, 
    		 height: 500, 
    		 title: "Grid Transacctions", 
    		 flexHeight: false, 
    		 flexWidth: false,
    		 topVisible: false, 
    		 bottomVisible: true,
    		 columnBorders: true, 
    		 rowBorders: true, 
    		 oddRowsHighlight: true, 
    		 numberCell: false, 
    		 scrollModel: {horizontal: true}, 
    		 resizable: false, 
    		 draggable: false,
    		 editable: true
//    		 freezeCols: 4
    	};
     
    var $grid = $("#grid_json").pqGrid(obj);
    
    $("#searchButton").click(function(){
    	$grid.pqGrid("refreshDataAndView");
	});
    
    $grid.pqGrid("option", $.paramquery.pqGrid.regional["es"]);
    $grid.find(".pq-pager").pqPager("option", $.paramquery.pqPager.regional["es"]);
    
    /*$grid.pqGrid("option", $.paramquery.pqGrid.regional["es"]);
    $grid.find(".pq-pager").pqPager("option", $.paramquery.pqPager.regional["es"]);
	*/
	/*************************************************************
	 * Fin Busqueda de Transaccion
	 */
    
    $grid.on( "pqgridcellclick", function( event, ui ) {
    	
    	if(ui.colIndx != null){
    		if (ui.colIndx == 4) {
    			actalizarTienda(event, ui );
            }
    	}
    } );
    
    function actalizarTienda(event, ui ){
    	
    	var rowData = ui.dataModel.data; 
    	
    	if(rowData[ui.rowIndx]["idTienda"] == -1001){
    		return true;
    	}
    	
		validActualizar.resetForm();//remove error class on name elements and clear history
		$( "#mensaje" ).text("");
		$( 'input[type="text"]').removeAttr('class');
		
		$( "#idTienda" ).val( rowData[ui.rowIndx]["idTienda"] );
		$( "#descripcion" ).val( rowData[ui.rowIndx]["descripcion"] );
		$( "#codigo" ).val( rowData[ui.rowIndx]["codigo"] );
		$( "#userFTP" ).val( rowData[ui.rowIndx]["userFTP"] );
		$( "#passFTP" ).val( rowData[ui.rowIndx]["passFTP"] );
		$( "#urlFTP" ).val( rowData[ui.rowIndx]["urlFTP"] );
		$( "#posMobile" ).prop("checked", rowData[ui.rowIndx]["posMobile"] == 1?true: false);
		$( "#rompeFilas" ).prop("checked", (rowData[ui.rowIndx]["rompeFilas"] == 1)?true:false);
		
		$( "#container1" ).dialog( "open" );	
		
    }
    
    function updateStore(event, ui, status ){
    	$('#actualizarForm').submit(function(event) {
			//$('input[type="submit"]').attr('disabled','disabled');
		
	    	var dataJson = {
	    			"idTienda" : $( "#idTienda" ).val( ) ,
	    			"descripcion" : $( "#descripcion" ).val( ) ,
	    			"codigo" : $( "#codigo" ).val( ) ,
	    			"userFTP" : $( "#userFTP" ).val( ) ,
	    			"passFTP" : $( "#passFTP" ).val( ) ,
	    			"urlFTP" : $( "#urlFTP" ).val( ) ,
	    			"posMobile" : $("#posMobile").prop('checked')? "1":"0" ,
	    			"rompeFilas" : $("#rompeFilas").prop('checked')? "1":"0"
	    	};
	    	
	    	alert("json : " + dataJson + "\nJSON.stringify: " + JSON.stringify(dataJson));
	    	
	    	$.ajax({
				 url: '/ChedrauiServicios/portal/json/actualizarTienda',
				 type: 'POST',
				 data: JSON.stringify(dataJson),
				 dataType: 'json',
				 contentType: "application/json; charset=utf-8",
				 mimeType: 'application/json; charset=utf-8',
				 async: false,    //Cross-domain requests and dataType: "jsonp" requests do not support synchronous operation
	             cache: false,    //This will force requested pages not to be cached by the browser          
	             processData:false, //To avoid making query String instead of JSON
				 statusCode: {
					 404: function() {
					 alert( "Pagina no encontrada." );
					 }
				 },
				 success: function (data) {
					//$('input[type="submit"]').removeAttr('disabled');

				 	var caja2 = $('<div title="' + (data.idError == 0?'Mensaje del Sistema' :' Error' ) + '">' + data.mensajeError +'</div>');
					caja2.dialog({
						dialogClass: "alert",
						modal: true,
						resizable: false,
						draggable: false,
						buttons: [ {
							text: "OK",
							click: function() {
								if(data.idError == ID_ERROR_SESSION ){
									$(location).attr('href', '/ChedrauiServicios/portal/login');
								}else if(data.idError == 0){
							 		$grid.pqGrid("refreshDataAndView");
								}
								$( this ).dialog( "close" );
								$( "#container1" ).dialog( "close" );	
							}
						}]
				    });
				 	//$( "#cargando" ).dialog( "close" );
				 },
				 error: function (jqXHR, textStatus, errorThrown) {
					//$( "#cargando" ).dialog( "close" );
				 	alert('An error has occured!! \njqXHR: ' + jqXHR +
						 	'\ntextStatus: ' +  textStatus +
						 	'\nerrorThrown: ' + errorThrown);
				 }
			});
	    	
			event.preventDefault(); //STOP default action
			event.stopImmediatePropagation();
		    return false;
		});
		
		$("#actualizarForm").submit(); //SUBMIT FORM
    }
    
    $( "#container1" ).dialog({
		dialogClass: "no-close",
		modal: true,
		resizable: false,
		draggable: false,
		//closeOnEscape: false,
		autoOpen: false,
		height: 550,
		width: 850,
		buttons: [ {
			text: "Aceptar",
			click: function() {
				updateStore();
			}
		},{
			text: "Cancelar",
			click: function() {
				$( this ).dialog( "close" );
			}
		}]
	});
});