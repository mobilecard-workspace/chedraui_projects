

$(function() {
	
	$( "#cargando" ).dialog({
		dialogClass: "no-close",
		modal: true,
		resizable: false,
		draggable: false,
		//closeOnEscape: false,
		autoOpen: false
	});
	
	function iniciaCarga(){
		$('#menuForm').submit();
		$("#cargando" ).dialog( "open" );
		$('input[type="submit"]').attr('disabled','disabled');
	}

	$(document).ajaxStart(function() {
		$('input[type="submit"]').attr('disabled','disabled');
		$( "#cargando" ).dialog( "open" );
	});
	
	$(document).ajaxStop(function() {
		$( "#cargando" ).dialog( "close" );
		$('input[type="submit"]').removeAttr('disabled');
	});
	
	$("#menuInicio").click(function(){
		$('#menuForm').attr("action", "welcome");
		return false;
	});
	
	
	$("#menuAltaTienda").click(function(){
		$('#menuForm').attr("action", "tiendaAlta");
		iniciaCarga();
		return false;
	});
	
	$("#menuBusquedaTiendas").click(function(){
		$('#menuForm').attr("action", "tiendasBusqueda");
		iniciaCarga();
		return false;
	});
	
	$("#menuBusquedaUsuarios").click(function(){
		$('#menuForm').attr("action", "usuariosBusqueda");
		iniciaCarga();
		return false;
	});

	$("#menuLogout").click(function(){
		$('#menuForm').attr("action", "logout");
		iniciaCarga();
		return false;
	});
	

	$( "input[type=submit], button" )
		.button()
		.click(function( event ) {
		event.preventDefault();
		return false;
	});
	
	$( "a" )
		.click(function( event ) {
		event.preventDefault();
		return false;
	});
	
});