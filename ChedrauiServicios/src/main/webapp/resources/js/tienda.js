$(function() {
	
	$("#altaForm").validate({
        rules :{
        	descripcion : {
                required  : true,
                minlength : 3, //para validar campo con minimo 3 caracteres
                maxlength : 45  //para validar campo con maximo 10 caracteres
            },
            codigo : {
            	required :true,
            	digits   : true,
                maxlength : 4
            },
            userFTP : {
            	required :true,
            	minlength : 3,
                maxlength : 20,
            },
            passFTP : {
            	required :true,
            	minlength : 3,
                maxlength : 20,
            },
            urlFTP: {
            	required :true,
            	minlength : 6,
                maxlength : 20,
            },
            posMobile: {
            	digits   : true,
            	maxlength : 1,
            },
            rompeFilas : {
            	digits   : true,
            	maxlength : 1,
            }
        }
    });


	$("#btnAlta").click(function(){
		$('#altaForm').submit(function(event) {
			if(!$( this ).valid()){
				$('input[type="submit"]').removeAttr('disabled');
				event.preventDefault(); //STOP default action
				event.stopImmediatePropagation();
			    return false;
			}else{
				$("#posMobile").val($("#posMobile:checked").val()? "1":"0");
				$("#rompeFilas").val($("#rompeFilas:checked").val()? "1":"0");
				$( "#cargando" ).dialog( "open" );
			}
			
		});
		$("#altaForm").submit(); //SUBMIT FORM
			
	}); 
	
});