$(function() {
	
	
	validActualizar = $("#actualizarForm").validate({
		rules :{
        	nombre : {
                required  : true,
                maxlength : 45 
            },
            apellido : {
            	required :true,
                maxlength : 45
            },
            materno : {
                maxlength : 45
            },
            mail: {
                required : true,
                email   : true,
                maxlength : 45
            },
            telefono: {
            	required :true,
            	digits   : true,
                maxlength : 10,
            }
        }
       
    });

	
	/************************************************************
	 * 
	 */
	
	       //define colModel
     var colM = [{ title: "ID Usuario", width: 70, dataType: "string", dataIndx: "idUsuario" },
                 { title: "Usuario", width: 80, dataType: "string",  dataIndx: "login" },
                 { title: "Status", width: 60, dataType: "string",  dataIndx: "status", 
                	 render: function (ui) {
                		var rowData = ui.rowData; 
            			if (rowData["status"] == 1) {
                            return "<span style='color:green;'>Activo</span>";
            			}else if (rowData["status"] == 2) {
                        	return "<span style='color:red;'>Desactivo</span>";
                        }else if (rowData["status"] == 3) {
                            return "<span style='color:red;'>Bloqueado</span>";
                        }else if (rowData["status"] == 99) {
                        	return "<span style='color:blue;'>Cam. Contraseña</span>";
                        }else {
                            return "<span style='color:red;'></span>";
                        }
                     }},
                 { title: "Editar", width: 60, dataType: "string",  dataIndx: "status", 
                	 render: function (ui) {
                		 return "<a href='javascript:void(0)'><span>Editar...</span></a>";
                     }, editable: false},
                 { title: "Nombre", width: 100, dataType: "string",  dataIndx: "nombre"  },
                 { title: "A. Paterno", width: 80, dataType: "string",  dataIndx: "apellido"  },
                 { title: "A. Materno", width: 80, dataType: "string",  dataIndx: "materno" },
                 { title: "Email", width: 150, dataType: "string",  dataIndx: "mail" },
                 { title: "Supervisor", width: 80, dataType: "string",  dataIndx: "idSupervisor" },
                 { title: "Tienda", width: 100, dataType: "string",  dataIndx: "tienda" },
                 { title: "Role", width: 70, dataType: "string",  dataIndx: "rol" }
                 ];
	         
     
     //define dataModel
     var dataModel = {
         location: "remote",
         sorting: "remote",
         paging: "local",
         dataType: "JSON",
         method: "POST",
         curPage: 1,
         rPP: 20,
         sortIndx: 0,
         sortDir: "up",
         rPPOptions: [20, 50, 100],
         filterIndx: "",
         filterValue: "",
         getUrl: function () {
        	 var sortDir = (this.sortDir == "up") ? "asc" : "desc";
             var queryString = "curPage=" + this.curPage + "&recordsPerPage=" + this.rPP + 
	             "&sortBy=" + this.sortIndx + 
	             "&dir=" + sortDir +
                 "&idUsuarioT=" + ($("#idUsuarioT").val() != ''? $("#idUsuarioT").val(): '0') +
                 "&loginT=" + $("#loginT").val() +
                 "&nombreT=" + $("#nombreT").val() +
                 "&aPaternoT=" + $("#aPaternoT").val() +
        	 	 "&statusT=" + $("#statusT:checked").val();
             
             var obj = { url: "/ChedrauiServicios/portal/usuariosFiltro", data: queryString };
             return obj;
         },
         getData: function (dataJSON) {
             return { curPage: dataJSON.curPage, totalRecords: dataJSON.totalRecords, data: dataJSON.data };
         }
     };
     
     var obj = { 
    		 dataModel: dataModel,
    	     colModel: colM,
    		 width: 984, 
    		 height: 500, 
    		 title: "Grid Usuarios", 
    		 flexHeight: false, 
    		 flexWidth: false,
    		 topVisible: false, 
    		 bottomVisible: true,
    		 columnBorders: true, 
    		 rowBorders: true, 
    		 oddRowsHighlight: true, 
    		 numberCell: false, 
    		 scrollModel: {horizontal: true}, 
    		 resizable: false, 
    		 draggable: false,
    		 editable: true,
    		 freezeCols: 3
    	};
     
    var $grid = $("#grid_json").pqGrid(obj);
    
    $("#searchButton").click(function(){
    	$grid.pqGrid("refreshDataAndView");
	});
    
    $grid.pqGrid("option", $.paramquery.pqGrid.regional["es"]);
    $grid.find(".pq-pager").pqPager("option", $.paramquery.pqPager.regional["es"]);
	
	/*************************************************************
	 * Fin Busuqeda de Kits
	 */
    
    $grid.on( "pqgridcellclick", function( event, ui ) {
    	
    	if(ui.colIndx != null){
    		if (ui.colIndx == 3 ) {
    			mostrarDatosUsuario(event, ui );
            }else if (ui.colIndx == 4 ) {
            	
            }
    	}
    } );
    
    
    function mostrarDatosUsuario(event, ui ){
    	
    	var rowData = ui.dataModel.data; 
    	
		validActualizar.resetForm();//remove error class on name elements and clear history
		$( "#mensaje" ).text("");
		$('input[type="text"]').removeAttr('class');
		$('input[type="password"]').removeAttr('class');
		
		$( "#idUsuario" ).val( rowData[ui.rowIndx]["idUsuario"] );
		$( "#idUsuarioD" ).text( rowData[ui.rowIndx]["idUsuario"] );
		$( "#loginD" ).text( rowData[ui.rowIndx]["login"] );
		$( "#nombre" ).val( rowData[ui.rowIndx]["nombre"] );
		$( "#apellido" ).val( rowData[ui.rowIndx]["apellido"] );
		$( "#materno" ).val( rowData[ui.rowIndx]["materno"] );
		$( "#mail" ).val( rowData[ui.rowIndx]["mail"] );
//		$( "#telefono" ).val( rowData[ui.rowIndx]["telefono"] );
		
		if(rowData[ui.rowIndx]["status"] == 1){
			$('input:radio[name=statusAC]:nth(0)').attr('checked',true);
		}else if(rowData[ui.rowIndx]["status"] == 2){
			$('input:radio[name=statusAC]:nth(1)').attr('checked',true);
		}else if(rowData[ui.rowIndx]["status"] == 3){
			$('input:radio[name=statusAC]:nth(2)').attr('checked',true);
		}else if(rowData[ui.rowIndx]["status"] == 99){
			$('input:radio[name=statusAC]:nth(3)').attr('checked',true);
		}	
		
		$( "#container1" ).dialog( "open" );	
		
    }
    
    function actalizarUsuario( ){
    	
    	$('#actualizarForm').submit(function(event) {
			$('input[type="submit"]').attr('disabled','disabled');
			
			if(!$( this ).valid()){
				$('input[type="submit"]').removeAttr('disabled');
				event.preventDefault(); //STOP default action
			}else{
    	
		    	//var rowData = ui.dataModel.data; 
		    	var dataJson = {
		    			"idUsuario" : $( "#idUsuario" ).val(  ) ,
		    			"nombre": $( "#nombre" ).val(  ),
		    			"apellido": $( "#apellido" ).val(  ),
		    			"materno": $( "#materno" ).val(  ),
		    			"mail": $( "#mail" ).val(  ),
		    			"status": $( "input[name=statusAC]:checked" ).val(  )
		    	};
		    	
		    	$.ajax({
					 url: '/ChedrauiServicios/portal/json/actualizarUsuario',
					 type: 'POST',
					 data: JSON.stringify(dataJson),
					 dataType: 'json',
					 contentType: "application/json; charset=utf-8",
					 mimeType: 'application/json; charset=utf-8',
					 async: false,    //Cross-domain requests and dataType: "jsonp" requests do not support synchronous operation
		             cache: false,    //This will force requested pages not to be cached by the browser          
		             processData:false, //To avoid making query String instead of JSON
					 statusCode: {
						 404: function() {
						 alert( "Pagina no encontrada." );
						 }
					 },
					 success: function (data) {
						$('input[type="submit"]').removeAttr('disabled');
		
					 	var caja2 = $('<div title="' + (data.idError == 0?'Mensaje del Sistema' :' Error' ) + '">' + data.mensajeError +'</div>');
						caja2.dialog({
							dialogClass: "alert",
							modal: true,
							resizable: false,
							draggable: false,
							buttons: [ {
								text: "Aceptar",
								click: function() {
									if(data.idError == ID_ERROR_SESSION ){
										$(location).attr('href', '/ChedrauiServicios/portal/login');
									}else if(data.idError == 0){
								 		$grid.pqGrid("refreshDataAndView");
								        $( "#container1" ).dialog( "close" );
									}
									$( this ).dialog( "close" );
								}
							}]
					    });
					 	//$( "#cargando" ).dialog( "close" );
					 },
					 error: function (jqXHR, textStatus, errorThrown) {
						//$( "#cargando" ).dialog( "close" );
					 	alert('An error has occured!! \njqXHR: ' + jqXHR +
							 	'\ntextStatus: ' +  textStatus +
							 	'\nerrorThrown: ' + errorThrown);
					 }
				}); 
			}
			
			event.preventDefault(); //STOP default action
			event.stopImmediatePropagation();
		    return false;
    	});
    	
    	$("#actualizarForm").submit(); //SUBMIT FORM
    }
    
    $("#resetButton").click(function(){
    	
    	var caja2 = $('<div title="Mesaje">Por favor CONFIRMAR el Reset de Contraseña?</div>');
		caja2.dialog({
			dialogClass: "alert",
			modal: true,
			resizable: false,
			draggable: false,
			buttons: [ {
				text: "Aceptar",
				click: function() {
					var dataJson = {
			    			"idUsuario" : $( "#idUsuario" ).val(  ) ,
			    			"status": $( "input[name=statusAC]:checked" ).val(  )
			    	};
					$( this ).dialog( "close" );
			    	$.ajax({
						 url: '/ChedrauiServicios/portal/json/actualizarPassword',
						 type: 'POST',
						 data: JSON.stringify(dataJson),
						 dataType: 'json',
						 contentType: "application/json; charset=utf-8",
						 mimeType: 'application/json; charset=utf-8',
						 async: false,    //Cross-domain requests and dataType: "jsonp" requests do not support synchronous operation
			             cache: false,    //This will force requested pages not to be cached by the browser          
			             processData:false, //To avoid making query String instead of JSON
						 statusCode: {
							 404: function() {
							 alert( "Pagina no encontrada." );
							 }
						 },
						 success: function (data) {
							$('input[type="submit"]').removeAttr('disabled');
			
							var caja2 = $('<div title="' + (data.idError == 0?'Mensaje del Sistema' :' Error' ) + '">' + data.mensajeError +'</div>');
							caja2.dialog({
								dialogClass: "alert",
								modal: true,
								resizable: false,
								draggable: false,
								buttons: [ {
									text: "Aceptar",
									click: function() {
										if(data.idError == ID_ERROR_SESSION ){
											$(location).attr('href', '/ChedrauiServicios/portal/login');
										}else if(data.idError == 0){
									 		$grid.pqGrid("refreshDataAndView");
									        $( "#container1" ).dialog( "close" );
										}
										$( this ).dialog( "close" );
									}
								}]
						    });
						 	//$( "#cargando" ).dialog( "close" );
						 },
						 error: function (jqXHR, textStatus, errorThrown) {
							//$( "#cargando" ).dialog( "close" );
						 	alert('An error has occured!! \njqXHR: ' + jqXHR +
								 	'\ntextStatus: ' +  textStatus +
								 	'\nerrorThrown: ' + errorThrown);
						 }
					}); 
					}
				},
				{
					text: "Cancelar",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
	}); 
    
    $( "#container1" ).dialog({
		dialogClass: "no-close",
		modal: true,
		resizable: false,
		draggable: false,
		//closeOnEscape: false,
		autoOpen: false,
		height: 550,
		width: 850,
		buttons: [ {
			text: "Aceptar",
			click: function() {
				actalizarUsuario();
			}
		},{
			text: "Cancelar",
			click: function() {
				$( this ).dialog( "close" );
			}
		}]
	});

    
});