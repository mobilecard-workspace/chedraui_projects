	<div class="main-content" id="bienvenida" style="height: 300px">
		</br>
		<h3 class="pagetitle">Bienvenido: ${usuario.nombre} ${usuario.apellido}</h3>

		<p>
			Gracias por usar el Portal de Administrativo de Chedraui Movil POS...</strong>.
		</p>
		</br>
		<table align="center" cellpadding="2" style="width: 500px;">
			<tbody>
				<tr>
					<td>Usuario:</td>
					<td><strong>${usuario.login}</strong></td>
				</tr>
				<tr>
					<td>Role:</td>
					<td><strong>${usuario.role.descripcion}</strong></td>
				</tr>
				<tr>
					<td>Tienda:</td>
					<td><strong>${usuario.tienda.descripcion}</strong></td>
				</tr>
			</tbody>
		</table>
		</br>
		<p>
			Para preguntas y / o aclaraciones
			favor de contactarnos via telefonica +52 (55) 55403124 
			o si lo prefiere escribirnos a <a href=mailto:soporte@addcel.com>soporte@addcel.com</a>
		</p>

		<h3>&nbsp;</h3>
	</div>	
