<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		prefix="c" %>

<!-- Meta Tags -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Pragma" content="no-cache" >
<meta http-equiv="expires" content="-1" >

<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
 	
%>
<link href="<c:url value="/resources/css/pqgrid.min.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/pqgrid.css"/>" rel="stylesheet">

<script src="<c:url value="/resources/js/sistema/pqgrid.min.js"/>"></script>
<script src="<c:url value="/resources/js/sistema/pq-localize-es.js"/>"></script>
<script src="<c:url value="/resources/js/tiendasBusqueda.js"/>"></script>

	<div id="container" >

		<form id="busqueda" name=""busqueda"" class="mobilecard topLabel page"
			autocomplete="on" method="post" 
			action="${pageContext.request.contextPath}/">

			<!-- <div id="header" class="info">
				<label class="desc" style="font-size: 16px"> Transaction Search </label>
			</div> -->
			<ul>
				<li class="complex notranslate      " >
					<label id="lblErrorPago" class="error" > 
						${mensaje}
					</label>
				</li>
				<li class="complex notranslate  " >
					<table align="left" cellpadding="2" cellspacing="2" >
                        <tbody>
							<tr>
                        		<td>
                        			<label>Codigo: </label>
                        		</td>
                        		<td colspan="4">
                        			<input type="text" id="codigoT" name="codigoT" maxleng="8" size="30">
                        		</td>
                        	</tr>
                        	<tr>
                        		<td>
                        			<label>Nombre: </label>
                        		</td>
                        		<td colspan="4">
                        			<input type="text" id="nombreT" name="nombreT" maxleng="45" size="30">
                        		</td>
                        	</tr>
                        	<tr>
                        		<td>
                        			<label>URL FTP: </label>
                        		</td>
                        		<td colspan="4">
                        			<input type="text" id="urlT" name="urlT" maxleng="20" size="30">
                        		</td>
                        	</tr>
                        	<tr>
                        		<td>
                        			<label>POS MObile: </label>
                        		</td>
                        		<td colspan="4">
                        			<input type="checkbox" id="posMobile" name="posMobile" value="1" checked>
                        		</td>
                        	</tr>
                        	<tr>
                        		<td>
                        			<label>Rompe Filas: </label>
                        		</td>
                        		<td colspan="4">
                        			<input type="checkbox" id="rompeFilas" name="rompeFilas" value="1" checked>
                        		</td>
                        	</tr>
                        	<tr>
                        		<td colspan="3">
                        			<input id="searchButton" name="searchButton" type="submit" value="Buscar Tiendas" style="width: 150px"/>
                        		</td>
                        		<td colspan="1">
                        			<a href="#" title="Export Search to TXT File" id="exportTXT" name="exportTXT" width="80px" >
                        				<p width="80px" >Exportar Busqueda: <img src="<c:url value="/resources/images/txt_24x24.png"/>" align="absmiddle" style="float:left" /></p></a>
                        		</td>
                        	</tr>
                        </tbody>
                	</table>
				</li>
			</ul>
			<input type="hidden" id="currentSearch" name="currentSearch" value=""/>
			<div id="grid_json" style="aling: center;"></div>
			<br>
		</form>
		
	</div>
	<!--container-->
	
	<div id="container1" style="display: none;">

		<form id="actualizarForm" name="actualizarForm" class="conteiner1 topLabel page"
			autocomplete="on" method="post" style="width: 95%"
			action="${pageContext.request.contextPath}/portal/actualizarTienda">

			<div id="header" class="info">
				<label class="desc" style="font-size: 16px"> Actualizar Tienda </label>
				<label class="desc" style="font-size: 12px"> Se deben de ingresar los siguientes datos. </label>
			</div>
			
			<input type="hidden" id="idTienda" name="idTienda" />
			
			<ul>
				<input type="hidden" id="idTienda" name="idTienda" />
				<li id="foliError" class="complex notranslate      " >
					<label id="lblErrorPago" class="error" > 
						${mensaje}
					</label>
				</li>
				<li id="foli0" class="complex notranslate      ">
					<label class="desc" > Nombre Tienda 
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip">
							<input id="descripcion" name="descripcion" />
							<label >Nombre </label>
						</span>
						<span class="right country"> 
							<input id="codigo" name="codigo" />
							<label >Codigo Tienda </label>
						</span> 
					</div>
				</li>
				
				<li id="foli1" class="complex notranslate      ">
					<label class="desc" > Datos FTP 
						<span class="req">*</span>
					</label>
					<div>
				</li>

				<li id="foli3" class="complex notranslate      ">
					<div>
						<span class="left zip"> 
							<input id="userFTP" name="userFTP"  /> 
							<label > Usuario FTP <span class="req">*</span></label>
						</span> 
						<span class="right country"> 
							<input id="passFTP" name="passFTP"  /> 
							<label >Contraseña FTP <span class="req">*</span></label>
						</span>
					</div>
					<div>
						<span class="left zip"> 
							<input id="urlFTP" name="urlFTP" /> 
							<label >URL / IP FTP <span class="req">*</span></label>
						</span>
						<span class="right country"> 
					</div>
				</li>
				
				<li id="foli0" class="complex notranslate      ">
					<label class="desc" > Control de Aplicaciones 
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip">
							<input type="checkbox" id="posMobile" name="posMobile" value="1" > POS Mobile</input>
							<label >Tienda puede usar... </label>
						</span>
						<span class="right country"> 
							<input type="checkbox" id="rompeFilas" name="rompeFilas" value="1" > Rompe Filas</input>
							<label >Tienda puede usar... </label>
						</span> 
					</div>
				</li>
			</ul>
			
			<!-- <div align="center" class="buttons "  style="height: 60px;">
				<input id="saveButton" name="saveButton" type="submit" value="Alta" style="width: 150px"/>
				<input id="cleanButton" name="cleanButton" type="submit" value="Limpiar" style="width: 150px"/>
			</div> -->
		</form>
		
	</div>


