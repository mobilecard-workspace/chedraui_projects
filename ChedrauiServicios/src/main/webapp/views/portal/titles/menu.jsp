<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		prefix="c" %>


<div id="menu">
	<form id="menuForm" name="menuForm" method="post" >
	</form>
	<ul id="nav">
		<li><a id="menuInicio" href="#">Inicio</a></li>
		<c:if test="${usuario.role.idRol == 100 || usuario.role.idRol == 101 }"> 
			<li><a href="javascript:void(0)" onclick="return false;">Tiendas</a>
				<ul>
					<li><a id="menuAltaTienda" href="#" >Alta</a></li>
					<li><a id="menuBusquedaTiendas" href="#">Busqueda</a></li>
				</ul>
			</li>
			<%-- <li><a href="javascript:void(0)">KIT's</a>
				<ul>
					<li><a id="menuAltaKit" href="#">Alta</a></li>
					<li><a id="menuBusquedaKits" href="#">Busqueda</a></li>
				</ul>
			</li> --%>
		</c:if>
		<li><a id="aHrefCamTarj" href="#">Usuarios</a>
			<ul>
				<li><a id="menuBusquedaUsuarios" href="#">Busqueda</a></li>
			</ul>
		</li>
		<li><a id="menuTransacciones" href="#">Transacciones</a></li>
		<li><a id="menuLogout" href="#">Cerrar Session</a></li>
	</ul>
</div>