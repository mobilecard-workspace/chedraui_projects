<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		prefix="c" %>
<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
 	
%>

<link href="<c:url value="/resources/css/pqgrid.min.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/pqgrid.css"/>" rel="stylesheet">

<script src="<c:url value="/resources/js/sistema/pqgrid.min.js"/>"></script>
<script src="<c:url value="/resources/js/sistema/pq-localize-es.js"/>"></script>
<script src="<c:url value="/resources/js/kitsBusqueda-onComercio.js"/>"></script>

<STYLE type="text/css">
	div.pq-grid-toolbar-search
	{
	    text-align:left;
	}
	div.pq-grid-toolbar-search *
	{
	    margin:1px 5px 1px 0px;
	    vertical-align:middle;      
	}
	div.pq-grid-toolbar-search .pq-separator
	{
	   margin-left:10px;  
	   margin-right:10px;  
	}
	div.pq-grid-toolbar-search select
	{
	    height:18px;   
	    position:relative;
	}
	div.pq-grid-toolbar-search input.pq-filter-txt
	{
	    width:180px;border:1px solid #b5b8c8;       
	    height:16px;
	    padding:0px 5px;       
	}
</STYLE>

	<div id="container" >

		<form id="altaForm" name="altaForm" class="oncomercio topLabel page"
			autocomplete="on" method="post" modelAttribute="tienda"
			action="${pageContext.request.contextPath}/portal/altaTiendaFinal">

			<div id="header" class="info">
				<label class="desc" style="font-size: 16px"> Busqueda de kits </label>
				<label class="desc" style="font-size: 12px"> Para realizar la busqueda presione la tecla 'Enter'. </label>
			</div>
			
			<ul>
				<li id="foliError" class="complex notranslate      " >
					<label id="lblErrorPago" class="error" > 
						${mensaje}
					</label>
				</li>
			</ul>
			
			<div id="grid_json"></div>
			
			<!-- <div align="center" class="buttons "  style="height: 60px;">
				<input id="saveButton" name="saveButton" type="submit" value="Alta" style="width: 150px"/>
				<input id="cleanButton" name="cleanButton" type="submit" value="Limpiar" style="width: 150px"/>
			</div> -->
			<br>
		</form>
		
	</div>
	<!--container-->
	
	<div id="container1" style="display: none;">

		<form id="actualizarForm" name="actualizarForm" class="conteiner1 topLabel page"
			autocomplete="on" method="post"
			action="${pageContext.request.contextPath}/portal/actualizarTienda">

			<div id="header" class="info">
				<label class="desc" style="font-size: 16px"> Actualizar Kit </label>
				<label class="desc" style="font-size: 12px"> Se deben de ingresar los siguientes datos. </label>
			</div>
			
			<input type="hidden" id="idKit" name="idKit" />
			<input type="hidden" id="version" name="version" />
			
			<ul>
				<li id="foliError" class="complex notranslate      " >
					<label id="lblErrorPago" class="error" > 
						${mensaje}
					</label>
				</li>
				
				<li id="foli1" class="complex notranslate      ">
					<label class="desc" > CARNET Actual 
					</label>
					<div>
						<label class="desc" id="carnetActual"> CARNET Actual 
					</label>
 
					</div>
				</li>

				<li id="foli1" class="complex notranslate      ">
					<label class="desc" > CARNET 
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip">
							<input id="carnet" name="carnet" maxlength="16"/>
							<label >CARNET <span class="req">*</span></label>
						</span>
						<span class="right country"> 
							<input id="vigencia" name="vigencia" readonly="true" />
							<label >Vigencia <span class="req">*</span></label>
						</span> 
					</div>
				</li>
				<li id="foli1" class="complex notranslate      ">
					<div>
						<span class="left zip">
							<input id="nombreTitular" name="nombreTitular" maxlength="100"/>
							<label >Nombre Titular </label>
						</span>
						<span class="right country"> 
							<input id="folio" name="folio" maxlength="20"/>
							<label >Folio <span class="req">*</span></label>
						</span> 
					</div>
				</li>
				<li id="foli2" class="complex notranslate      ">
				<label class="desc" > IMEI 
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip">
							<input id="imei" name="imei" /> 
							<label >IMEI </label>
						</span>
						<span class="right country"> 
							<input id="imeiConf" name="imeiConf" maxlength="50" /> 
							<label >Confirmar IMEI <span class="req">*</span></label>
						</span> 
					</div>
				</li>

				<li id="foli3" class="complex notranslate      ">
				<label class="desc" > DISPOSITIVO 
					</label>
					<div>
						<span class="left zip"> 
							<input id="dispositivo" name="dispositivo"  maxlength="30"/> 
							<label >Dispositivo </label>
						</span> 
						<span class="right country"> 
							<input id="dispositivoConf" name="dispositivoConf"  /> 
							<label >Confirmar Dispositivo </label>
						</span>
					</div>
				</li>
				<li id="foli3" class="complex notranslate      ">
					<label class="desc" > SIM 
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip"> 
							<input id="sim" name="sim"  maxlength="20"/> 
							<label >SIM <span class="req">*</span></label>
						</span> 
						<span class="right country"> 
							<input id="simConf" name="simConf"  maxlength="20"/> 
							<label >Confirmar SIM <span class="req">*</span></label>
						</span>
					</div>
				</li>
				<li id="foli3" class="complex notranslate      ">
					<label class="desc" > TELEFONO 
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip"> 
							<input id="telefono" name="telefono"  maxlength="10"/> 
							<label >Telefono <span class="req">*</span></label>
						</span> 
						<span class="right country"> 
							<input id="telefonoConf" name="telefonoConf"  maxlength="10"/> 
							<label >Confirmar Telefono <span class="req">*</span></label>
						</span>
					</div>
				</li>
				
			</ul>
			
			<!-- <div align="center" class="buttons "  style="height: 60px;">
				<input id="saveButton" name="saveButton" type="submit" value="Alta" style="width: 150px"/>
				<input id="cleanButton" name="cleanButton" type="submit" value="Limpiar" style="width: 150px"/>
			</div> -->
		</form>
		
	</div>


