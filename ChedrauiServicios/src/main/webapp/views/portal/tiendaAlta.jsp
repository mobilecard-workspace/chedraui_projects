<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		prefix="c" %>
<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
 	
%>

<script src="<c:url value="/resources/js/tienda.js"/>"></script>

	<div id="container" >

		<form id="altaForm" name="altaForm" class="oncomercio topLabel page"
			autocomplete="on" method="post" modelAttribute="tienda"
			action="${pageContext.request.contextPath}/portal/tiendaAlta-final">

			<div id="header" class="info">
				<label class="desc" style="font-size: 16px"> Alta Tienda </label>
				<label class="desc" style="font-size: 12px"> Se deben de ingresar los siguientes datos. </label>
			</div>
			
			<ul>
				<li id="foliError" class="complex notranslate      " >
					<label id="lblErrorPago" class="error" > 
						${mensaje}
					</label>
				</li>
				<li id="foli0" class="complex notranslate      ">
					<label class="desc" > Nombre Tienda 
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip">
							<input id="descripcion" name="descripcion" />
							<label >Nombre </label>
						</span>
						<span class="right country"> 
							<input id="codigo" name="codigo" />
							<label >Codigo Tienda </label>
						</span> 
					</div>
				</li>
				
				<li id="foli1" class="complex notranslate      ">
					<label class="desc" > Datos FTP 
						<span class="req">*</span>
					</label>
					<div>
				</li>

				<li id="foli3" class="complex notranslate      ">
					<div>
						<span class="left zip"> 
							<input id="userFTP" name="userFTP"  /> 
							<label > Usuario FTP <span class="req">*</span></label>
						</span> 
						<span class="right country"> 
							<input id="passFTP" name="passFTP"  /> 
							<label >Contraseña FTP <span class="req">*</span></label>
						</span>
					</div>
					<div>
						<span class="left zip"> 
							<input id="urlFTP" name="urlFTP" /> 
							<label >URL / IP FTP <span class="req">*</span></label>
						</span>
						<span class="right country"> 
					</div>
				</li>
				
				<li id="foli0" class="complex notranslate      ">
					<label class="desc" > Control de Aplicaciones 
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip">
							<input type="checkbox" id="posMobile" name="posMobile" value="1" > POS Mobile</input>
							<label >Tienda puede usar... </label>
						</span>
						<span class="right country"> 
							<input type="checkbox" id="rompeFilas" name="rompeFilas" value="1" > Rompe Filas</input>
							<label >Tienda puede usar... </label>
						</span> 
					</div>
				</li>
			</ul>
			
			<div align="center" class="buttons "  style="height: 60px;">
				<input id="btnAlta" name="btnAlta" type="submit" value="Alta" style="width: 150px"/>
				<!-- <input id="cleanButton" name="cleanButton" type="submit" value="Limpiar" style="width: 150px"/> -->
			</div>
		</form>
		
	</div>
	<!--container-->


