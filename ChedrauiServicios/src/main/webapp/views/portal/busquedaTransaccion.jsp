<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		prefix="c" %>

<!-- Meta Tags -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Pragma" content="no-cache" >
<meta http-equiv="expires" content="-1" >

<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
 	
%>
<link href="<c:url value="/resources/css/pqgrid.min.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/pqgrid.css"/>" rel="stylesheet">

<script src="<c:url value="/resources/js/sistema/pqgrid.min.js"/>"></script>
<script src="<c:url value="/resources/js/busquedaTransaccion.js"/>"></script>


	<div id="container" >

		<form id="busqueda" name=""busqueda"" class="mobilecard topLabel page"
			autocomplete="on" method="post" 
			action="${pageContext.request.contextPath}/">

			<!-- <div id="header" class="info">
				<label class="desc" style="font-size: 16px"> Transaction Search </label>
			</div> -->
			<input type="hidden" id="currentSearch" name="currentSearch" value=""/>
			<ul>
				<li class="complex notranslate      " >
					<label id="lblErrorPago" class="error" > 
						${mensaje}
					</label>
				</li>
				<li class="complex notranslate  " >
					<table align="left" cellpadding="2" cellspacing="2" >
                        <tbody>
                        	<tr>
                        		<c:if test="${usuario == null || usuario.rol == 0 }">
	                        		<td>
	                        			<input type="radio" name="tipoBusqueda" value="1" checked><label>Dia Actual </label>
	                        		</td>
	                        		<td>
	                        			<input type="radio" name="tipoBusqueda" value="2"><label>Semana Actual </label>
	                        		</td>
	                        		<td>
	                        			<input type="radio" name="tipoBusqueda" value="3"><label>Mes Actual </label>
	                        		</td>
	                        		<td>
	                        			<input type="radio" name="tipoBusqueda" value="4"><label>Mes Anterior </label>
	                        		</td>
	                        		<td>
	                        			<input type="radio" name="tipoBusqueda" value="5"><label>2 Meses Anteriores </label>
	                        		</td>
	                        		<td>
	                        		</td>
                        		</c:if>
                        		<c:if test="${usuario != null && usuario.rol == 1}">
                        			<td>
	                        			<label>Start Date  </label>
	                        		</td>
	                        		<td>
	                        			<input id="startDate" name="startDate" readonly />
	                        		</td>
	                        		<td>
	                        			<label>End Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
	                        		</td>
	                        		<td>
	                        			<input id="endDate" name="endDate" readonly />
	                        		</td>
	                        		<td>
	                        		</td>
	                        		<td>
	                        		</td>
                        		</c:if>
                        	</tr>
                        	<tr>
                        		<td>
                        			<input type="radio" name="status" value="-1" checked><label>Todos </label>
                        		</td>
                        		<td>
                        			<input type="radio" name="status" value="1"><label>Exitos </label>
                        		</td>
                        		<td>
                        			<input type="radio" name="status" value="0"><label>Error </label>
                        		</td>
                        	</tr>
                        	<c:if test="${usuario != null && usuario.rol == 1 && companias != null}">
                        		<tr>
                        			<td>
	                        			<label>Company </label>
	                        		</td>
	                        		<td colspan="3">
	                        			<select name="company" id="company"  >
					              			<option value="0" >--   All companies   --</option>
		                        			<c:forEach items="${companias}" var="company">
										       <option value="${company.value}">${company.label}</option>
										   </c:forEach>
									   </select>
	                        		</td>
	                        	</tr>
							</c:if>
							<tr>
                        		<td>
                        			<label>Referencia MC: </label>
                        		</td>
                        		<td colspan="4">
                        			<input type="text" id="reference" name="reference" maxleng="8">
                        		</td>
                        	</tr>
                        	<tr>
                        		<td>
                        			<label>Numero de Autorizacion: </label>
                        		</td>
                        		<td colspan="4">
                        			<input type="text" id="numAutho" name="numAutho" maxleng="8">
                        		</td>
                        	</tr>
                        	<tr>
                        		<td colspan="3">
                        			<input id="searchButton" name="searchButton" type="submit" value="Transaction Search" style="width: 150px"/>
                        		</td>
                        		<td colspan="1">
                        			<a href="#" title="Export Search to TXT File" id="exportTXT" name="exportTXT" width="80px" >
                        				<p width="80px" >Exportar Busqueda: <img src="<c:url value="/resources/images/txt_24x24.png"/>" align="absmiddle" style="float:left" /></p></a>
                        		</td>
                        	</tr>
                        </tbody>
                	</table>
				</li>
			</ul>
			<div id="grid_json" style="aling: center;"></div>
			<br>
		</form>
		
	</div>
	<!--container-->
	

