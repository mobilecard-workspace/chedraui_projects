<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
 	
%>

<script src="<c:url value="/resources/js/kit-onComercio.js"/>"></script>

	<div id="container" >

		<form:form  id="altaForm" name="altaForm" class="oncomercio topLabel page"
			autocomplete="off" method="post" modelAttribute="kit"
			action="${pageContext.request.contextPath}/portal/altaKitFinal">

			<div id="header" class="info">
				<label class="desc" style="font-size: 16px"> Alta Kit </label>
				<label class="desc" style="font-size: 12px"> Se deben de ingresar los siguientes datos. </label>
			</div>
			
			<ul>
				<li id="foliError" class="complex notranslate      " >
					<label id="lblErrorPago" class="error" > 
						${mensaje}
					</label>
				</li>

				<li id="foli1" class="complex notranslate      ">
					<label class="desc" > CARNET 
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip">
							<form:input id="carnet" path="carnet" maxlength="16"/>
							<label >CARNET <span class="req">*</span></label>
						</span>
						<span class="right country"> 
							<form:input id="vigencia" path="vigencia" readonly="true" />
							<label >Vigencia <span class="req">*</span></label>
						</span> 
					</div>
				</li>
				<li id="foli1" class="complex notranslate      ">
					<div>
						<span class="left zip">
							<form:input id="nombreTitular" path="nombreTitular" maxlength="100"/>
							<label >Nombre Titular </label>
						</span>
						<span class="right country"> 
							<form:input id="folio" path="folio" maxlength="20" />
							<label >Folio <span class="req">*</span></label>
						</span> 
					</div>
				</li>
				<li id="foli2" class="complex notranslate      ">
					<label class="desc" > IMEI 
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip">
							<form:input id="imei" path="imei" /> 
							<label >IMEI </label>
						</span>
						<span class="right country"> 
							<form:input id="imeiConf" path="imeiConf" maxlength="50" /> 
							<label >Confirmar IMEI <span class="req">*</span></label>
						</span> 
					</div>
				</li>

				<li id="foli3" class="complex notranslate      ">
					<label class="desc" > DISPOSITIVO 
					</label>
					<div>
						<span class="left zip"> 
							<form:input id="dispositivo" path="dispositivo"  maxlength="30"/> 
							<label >Dispositivo </label>
						</span> 
						<span class="right country"> 
							<form:input id="dispositivoConf" path="dispositivoConf"  /> 
							<label >Confirmar Dispositivo </label>
						</span>
					</div>
				</li>
				<li id="foli3" class="complex notranslate      ">
					<label class="desc" > SIM 
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip"> 
							<form:input id="sim" path="sim"  maxlength="20"/> 
							<label >SIM <span class="req">*</span></label>
						</span> 
						<span class="right country"> 
							<form:input id="simConf" path="simConf"  maxlength="20"/> 
							<label >Confirmar SIM <span class="req">*</span></label>
						</span>
					</div>
				</li>
				<li id="foli3" class="complex notranslate      ">
					<label class="desc" > TELEFONO 
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip"> 
							<form:input id="telefono" path="telefono" maxlength="10"/> 
							<label >Telefono <span class="req">*</span></label>
						</span> 
						<span class="right country"> 
							<form:input id="telefonoConf" path="telefonoConf"  maxlength="10"/> 
							<label >Confirmar Telefono <span class="req">*</span></label>
						</span>
					</div>
				</li>
			</ul>
			
			<div align="center" class="buttons "  style="height: 60px;">
				<input id="saveButton" path="saveButton" type="submit" value="Alta" style="width: 150px"/>
				<input id="cleanButton" path="cleanButton" type="submit" value="Limpiar" style="width: 150px"/>
			</div>
		</form:form >
		
	</div>
	<!--container-->


