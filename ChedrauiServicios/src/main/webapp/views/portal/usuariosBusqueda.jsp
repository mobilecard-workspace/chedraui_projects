<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		prefix="c" %>
<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
 	
%>

<link href="<c:url value="/resources/css/pqgrid.min.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/pqgrid.css"/>" rel="stylesheet">

<script src="<c:url value="/resources/js/sistema/pqgrid.min.js"/>"></script>
<script src="<c:url value="/resources/js/sistema/pq-localize-es.js"/>"></script>
<script src="<c:url value="/resources/js/usuariosBusqueda.js"/>"></script>

	<div id="container" >

		<form id="busqueda" name=""busqueda"" class="mobilecard topLabel page"
			autocomplete="on" method="post" 
			action="${pageContext.request.contextPath}/">

			<div id="header" class="info">
				<label class="desc" style="font-size: 12px"> Busqueda de Usuarios </label>
			</div>
			
			<ul>
				<li id="foliError" class="complex notranslate      " >
					<label id="lblErrorPago" class="error" > 
						${mensaje}
					</label>
				</li>
				<li class="complex notranslate  " >
					<table align="left" cellpadding="2" cellspacing="2" >
                        <tbody>
							<tr>
                        		<td>
                        			<label>Id Usuario: </label>
                        		</td>
                        		<td colspan="4">
                        			<input type="text" id="idUsuarioT" name="idUsuarioT" maxleng="6" size="30">
                        		</td>
                        	</tr>
                        	<tr>
                        		<td>
                        			<label>Login: </label>
                        		</td>
                        		<td colspan="4">
                        			<input type="text" id="loginT" name="loginT" maxleng="45" size="30">
                        		</td>
                        	</tr>
                        	<tr>
                        		<td>
                        			<label>Nombre: </label>
                        		</td>
                        		<td colspan="4">
                        			<input type="text" id="nombreT" name="nombreT" maxleng="40" size="30">
                        		</td>
                        	</tr>
                        	<tr>
                        		<td>
                        			<label>Apellido Paterno: </label>
                        		</td>
                        		<td colspan="4">
                        			<input type="text" id="aPaternoT" name="aPaternoT" maxleng="45" size="30">
                        		</td>
                        	</tr>
                        	<tr>
                        		<td colspan="4">
                        			<table align="left" cellpadding="2" cellspacing="2" >
				                        <tbody>
				                        	<tr>
				                        		<td >
				                        			<input type="radio" id="statusT" name="statusT" value="0" checked>
				                        		</td>
				                        		<td>
				                        			<label>Todos </label>
				                        		</td>
				                        		<td >
				                        			<input type="radio" id="statusT" name="statusT" value="1" >
				                        		</td>
				                        		<td>
				                        			<label>Activo </label>
				                        		</td>
				                        		<td >
				                        			<input type="radio" id="statusT" name="statusT" value="2" >
				                        		</td>
				                        		<td>
				                        			<label>Desactivo </label>
				                        		</td>
				                        		<td >
				                        			<input type="radio" id="statusT" name="statusT" value="3" >
				                        		</td>
				                        		<td>
				                        			<label>Bloqueado </label>
				                        		</td>
				                        	</tr>
				                        </tbody>
				                	</table>
                        		</td>
                        	</tr>
                        	<tr>
                        		<td colspan="3">
                        			<input id="searchButton" name="searchButton" type="submit" value="Buscar Usuarios" style="width: 150px"/>
                        		</td>
                        		<td colspan="1">
                        			<a href="#" title="Export Search to TXT File" id="exportTXT" name="exportTXT" width="80px" >
                        				<p width="80px" >Exportar Busqueda: <img src="<c:url value="/resources/images/txt_24x24.png"/>" align="absmiddle" style="float:left" /></p></a>
                        		</td>
                        	</tr>
                        </tbody>
                	</table>
				</li>
			</ul>
			<input type="hidden" id="currentSearch" name="currentSearch" value=""/>
			<div id="grid_json" style="aling: center;"></div>
			<br>
		</form>
		
	</div>
	<!--container-->
	
	<div id="container1" style="display: none;">

		<form id="actualizarForm" name="actualizarForm" class="conteiner1 topLabel page"
			autocomplete="on" method="post" style="width: 95%"
			action="${pageContext.request.contextPath}/portal/actualizaUsuario">

			<div id="header" class="info">
				<label class="desc" style="font-size: 12px"> Editar Usuario </label>
				<label class="desc" style="font-size: 12px"> Se deben de ingresar los siguientes datos. </label>
			</div>
			
			<input type="hidden" id="idUsuario" name="idUsuario" />
			<input type="hidden" id="login" name="login" />
			
			<ul>
				<li id="foliError" class="complex notranslate      " >
					<label id="lblErrorPago" class="error" > 
						${mensaje}
					</label>
				</li>
				
				<li id="foli1" class="complex notranslate      ">
					<label class="desc" > Usuario 
					</label>
					<span class="left zip">
						<label class="desc" id="loginD"> </label>
						<label >Usuario </label>
					</span>
					<span class="right country"> 
						<label class="desc" id="idUsuarioD"> </label>
						<label >ID Usuario </label>
					</span> 
				</li>
				<li id="foli1" class="complex notranslate      ">
					<label class="desc" > Nombre Completo
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip">
							<input id="nombre" name="nombre" maxlength="45"/>
							<label >Nombre <span class="req">*</span></label>
						</span>
						<span class="right country"> 
							<input id="apellido" name="apellido" maxlength="45"/>
							<label >Apellido <span class="req">*</span></label>
						</span> 
					</div>
				</li>
				<li id="foli2" class="complex notranslate      ">
					<div>
						<span class="left zip">
							<input id="materno" name="materno" maxlength="45"/> 
							<label >Materno </label>
						</span>
					</div>
				</li>

				<li id="foli3" class="complex notranslate      ">
					<label class="desc" > Correo Electronico 
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip"> 
							<input id="mail" name="mail"  maxlength="45"/> 
							<label >Email <span class="req">*</span></label>
						</span> 
						<span class="right country"> 
							<table align="left" cellpadding="2" cellspacing="2" >
		                        <tbody>
		                        	<tr>
		                        		<td >
		                        			<input type="radio" id="statusAC" name="statusAC" value="1" >
		                        		</td>
		                        		<td>
		                        			<label>Activo </label>
		                        		</td>
		                        		<td >
		                        			<input type="radio" id="statusAC" name="statusAC" value="2" >
		                        		</td>
		                        		<td>
		                        			<label>Desactivo </label>
		                        		</td>
		                        		<td >
		                        			<input type="radio" id="statusAC" name="statusAC" value="3" >
		                        		</td>
		                        		<td>
		                        			<label>Bloqueado </label>
		                        		</td>
		                        		<td >
		                        			<input type="radio" id="statusAC" name="statusAC" value="99" >
		                        		</td>
		                        		<td>
		                        			<label>Cam. Contraseña </label>
		                        		</td>
		                        	</tr>
		                        </tbody>
		                	</table>
						</span>
					</div>
				</li>
				<li id="foli1" class="complex notranslate      ">
					<label class="desc" > Reset Contraseña
					</label>
					<div>
						<span class="full zip">
							<input id="resetButton" name="resetButton" type="submit" value="Reset Contraseña" style="width: 200px"/>
							<label >Reinicio de contraseña, se enviara un Email al correo registrado con una contraseña temporal, la siguiente vez que ingrese el usuario tendrá que cambiar su contraseña temporal. </label>
						</span>
					</div>
				</li>
			</ul>
			
			<!-- <div align="center" class="buttons "  style="height: 60px;">
				<input id="saveButton" name="saveButton" type="submit" value="Alta" style="width: 150px"/>
				<input id="cleanButton" name="cleanButton" type="submit" value="Limpiar" style="width: 150px"/>
			</div> -->
		</form>
		
	</div>


