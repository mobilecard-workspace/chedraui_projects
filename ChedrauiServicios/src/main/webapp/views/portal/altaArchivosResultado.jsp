<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
 	
%>

<STYLE type="text/css">
	div.pq-grid-toolbar-search
	{
	    text-align:left;
	}
	div.pq-grid-toolbar-search *
	{
	    margin:1px 5px 1px 0px;
	    vertical-align:middle;      
	}
	div.pq-grid-toolbar-search .pq-separator
	{
	   margin-left:10px;  
	   margin-right:10px;  
	}
	div.pq-grid-toolbar-search select
	{
	    height:18px;   
	    position:relative;
	}
	div.pq-grid-toolbar-search input.pq-filter-txt
	{
	    width:180px;border:1px solid #b5b8c8;       
	    height:16px;
	    padding:0px 5px;       
	}
</STYLE>

	<div id="main-content" class="main-content">

			<div id="header" class="info">
				<label class="desc" style="font-size: 16px"> Resultado. </label>
				<label class="desc" style="font-size: 12px"> Los Kits quedaron distribuidos de la siguiente forma, tambien puede descargar el archivo generado.
			</div>
			<br/>
			<label class="error" > 
				${mensaje}
			</label>
			<br/>
			<br/>
			<label class="desc" id="title9" for="Field9"> Archivo : <a id="altaArchivosRecuperar" href="${pageContext.request.contextPath}/portal/altaArchivosRecuperar?nombreArchivo=${archivo}" ><strong>${archivo}</strong></a>
			</label>
			<br/>
			<div style="width: 840px; height: 500px; overflow: auto; border: 0px solid #CCC;">
				<table align="center" cellpadding="2" style="width: auto;">
					<thead>
						<tr>
							<th style="width: 100px">Id Tienda</th>
							<th style="width: 200px">Nombre Tienda</td>
							<th style="width: 100px">Id Kit</td>
							<th>Carnet</th>
							<!-- <th>Vigencia</td> -->
							<th>Folio</th>
							<th>IMEI</td>
							<th>Dispositivo</td>
							<th>SIM</td>
							<th>Telefono</td>
							<th>Calle</th>
							<th>Num Ext</td>
							<th>Num Int</th>
							<th>Colonia</td>
							<th>CP</th>
							<th>Ciudad</td>
							<th>Estado</th>
							<th>Latitud</td>
							<th>Longitud</th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${combos != null}">
			               <c:forEach items="${combos}" var="combo">
						       <tr>
									<td>${combo.idTienda}</td>
									<td>${combo.nombre}</td>
									<td>${combo.idKit}</td>
									<td>${combo.mascara}</td>
									<%-- <td>${combo.vigencia}</td> --%>
									<td>${combo.folio}</td>
									<td></td>
									<td>${combo.imei}</td>
									<td>${combo.dispositivo}</td>
									<td>${combo.sim}</td>
									<td>${combo.telefono}</td>
									<td>${combo.calle}</td>
									<td>${combo.numExt}</td>
									<td>${combo.numInt}</td>
									<td>${combo.colonia}</td>
									<td>${combo.cp}</td>
									<td>${combo.ciudad}</td>
									<td>${combo.estado}</td>
									<td>${combo.cx}</td>
									<td>${combo.cy}</td>
								</tr>
						   </c:forEach>
					   </c:if>
					</tbody>
				</table>
			</div>
			<br/>
			<div align="center"  style="height: 50px;">
				<!-- <input id="procesarButton" name="procesarButton" type="submit" value="Procesar Archivos" style="width: 150px"/> -->
				<!-- <input id="resetSubmit" name="resetSubmit" type="submit" value="Reset Password" style="width: 150px"/> -->
			</div>
		
	</div>
	
	