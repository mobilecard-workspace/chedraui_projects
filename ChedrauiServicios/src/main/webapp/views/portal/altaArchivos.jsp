<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
 	
%>

<link href="<c:url value="/resources/css/jquery.nicefileinput.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/pqgrid.min.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/pqgrid.css"/>" rel="stylesheet">

<script src="<c:url value="/resources/js/sistema/jquery.nicefileinput.js"/>"></script>
<script src="<c:url value="/resources/js/kitArchivo-onComercio.js"/>"></script>

<STYLE type="text/css">
	div.pq-grid-toolbar-search
	{
	    text-align:left;
	}
	div.pq-grid-toolbar-search *
	{
	    margin:1px 5px 1px 0px;
	    vertical-align:middle;      
	}
	div.pq-grid-toolbar-search .pq-separator
	{
	   margin-left:10px;  
	   margin-right:10px;  
	}
	div.pq-grid-toolbar-search select
	{
	    height:18px;   
	    position:relative;
	}
	div.pq-grid-toolbar-search input.pq-filter-txt
	{
	    width:180px;border:1px solid #b5b8c8;       
	    height:16px;
	    padding:0px 5px;       
	}
</STYLE>

	<div id="container" >

		<form:form id="kitArchivoForm" name="kitArchivoForm" class="oncomercio topLabel page"
			autocomplete="off" method="post" modelAttribute="listaArchivos" enctype="multipart/form-data"
			action="${pageContext.request.contextPath}/portal/altaArchivosProcesar">

			<div id="header" class="info">
				<label class="desc" style="font-size: 16px"> Proceso de Alta de Tiendas y Kit's por archivos </label>
				<label class="desc" style="font-size: 12px">
					Se tienen las siguientes combinaciones posibles:
					<br>
					1. Ingresar archivo Tiendas.<br>
					2. Ingresar archivos Kit's (Tablets, CARNET, Dispositivo, SIM's).<br>
					3. Ingresar los 5 archivos, alta de Tiendas y Kits.<br> 
			</div>

			<ul>
				<li id="foliError" class="complex notranslate      " >
					<label class="error" > 
						${mensaje}
					</label>
				</li>
				<br/>
				<li id="foli0" class="complex notranslate      ">
					<label class="desc" id="title9" for="Field9"> Archivo Tiendas 
						<span id="req_9" class="req">*</span>
					</label>
					<div>
							<input name="archivos[0]" type="file" class="nicefileinput nice"/>
					</div>
				</li>
				<br/>
				<li id="foli0" class="complex notranslate      ">
					<label class="desc" id="title9" for="Field9"> Archivo Tablets o Terminales 
						<span id="req_9" class="req">*</span>
					</label>
					<div>
							<input name="archivos[1]" type="file" class="nicefileinput nice"/>
					</div>
				</li>
				<br/>
				<li id="foli1" class="complex notranslate      ">
					<label class="desc" id="title9" for="Field9"> Archivo CARNET o Tarjetas
						<span id="req_9" class="req">*</span>
					</label>
					<div>
							<input name="archivos[2]" type="file" class="nicefileinput nice"/>
					</div>
				</li>
				<br/>
				<li id="foli1" class="complex notranslate      ">
					<label class="desc" id="title9" for="Field9"> Archivo Dispositivos o Lector de Tarjetas
						<span id="req_9" class="req">*</span>
					</label>
					<div>
							<input name="archivos[3]" type="file" class="nicefileinput nice"/>
					</div>
				</li>
				<br/>
				<li id="foli1" class="complex notranslate      ">
					<label class="desc" id="title9" for="Field9"> Archivo SIM's y Numero Telefono
						<span id="req_9" class="req">*</span>
					</label>
					<div>
							<input name="archivos[4]" type="file" class="nicefileinput nice"/>
					</div>
				</li>
			</ul>
			<br/>
			<div align="center"  style="height: 50px;">
				<input id="procesarButton" name="procesarButton" type="submit" value="Procesar Archivos" style="width: 150px"/>
				<!-- <input id="resetSubmit" name="resetSubmit" type="submit" value="Reset Password" style="width: 150px"/> -->
			</div>
		</form:form>
		
	</div>
	
	