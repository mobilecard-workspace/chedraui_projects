package addcel.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.webkit.DateSorter;

public final class AddcelDateUtils {
	
	private static List<Integer> vigenciaAnios;
	private static Calendar calendar;
	
	private AddcelDateUtils() {
	}
	
	public static synchronized List<Integer> getVigenciaAnios() {
		if (vigenciaAnios == null) {
			
			int anio = getCalendar().get(Calendar.YEAR);
			vigenciaAnios = new ArrayList<Integer>();
			
			int amount = anio + 7;
			
			for (int i = anio; i < amount; i++) {
				vigenciaAnios.add(i);
			}
		}
		
		return vigenciaAnios;
	}
	
	public static synchronized boolean validaVigencia(String mes, int anio) {
		int mesInt = Integer.parseInt(mes) - 1;
		int calendarYear = getCalendar().get(Calendar.YEAR);
		
		if (anio < calendarYear) {
			return false;
		} else if (anio == calendarYear) {
			if (mesInt < getCalendar().get(Calendar.MONTH)) {
				return false;
			}
		}
		
		return true;
		
	}
	
	public static synchronized int getAnioVigenciaIndex(int anioVigencia) {
		int anioActual = getCalendar().get(Calendar.YEAR);
		return anioVigencia - anioActual;
	}
	
	private static synchronized Calendar getCalendar() {
		if (calendar == null) {
			calendar = Calendar.getInstance();
		}
		
		return calendar;
			
	}
	
	public static synchronized int compareDateToToday(Date date) {
		
		if (date == null)
			return -10000;
		
		Calendar dateCalendar = Calendar.getInstance();
		dateCalendar.setTime(date);
		int dateDay = dateCalendar.get(Calendar.DAY_OF_MONTH);
		int today = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
		
		if (dateDay > today)
			return 1;
		if (dateDay == today)
			return 0;
		if (dateDay < today)
			return -1;
		
		return -10000;
	}
	
	public static synchronized long getFirstOfWeek() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
		c.clear(Calendar.MINUTE);
		c.clear(Calendar.SECOND);
		c.clear(Calendar.MILLISECOND);

		// get start of this week in milliseconds
		c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek());
		
		return c.getTimeInMillis();
		
		
	}
	
	public static synchronized long getFirstOfMonth() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
		cal.clear(Calendar.MINUTE);
		cal.clear(Calendar.SECOND);
		cal.clear(Calendar.MILLISECOND);

		// get start of the month
		cal.set(Calendar.DAY_OF_MONTH, 1);
		
		return cal.getTimeInMillis();
	}

}
