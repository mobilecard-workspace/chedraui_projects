package addcel.util;

import android.app.ProgressDialog;
import android.content.Context;

public class ProgressDialogFactory {
	
	private static ProgressDialog instance;
	
	public static synchronized ProgressDialog get(Context context, int msg) {
		return get(context, msg, true, false);
	}
	
	public static synchronized ProgressDialog get(Context context, int msg, 
			boolean indeterminate) {
		return get(context, msg, indeterminate, false);
	}
	
	public static synchronized ProgressDialog get(Context context, int msg, 
			boolean indeterminate, boolean cancellable) {
		instance = new ProgressDialog(context);
		instance.setMessage(context.getText(msg));
		
		return instance;
	}
	

}
