package addcel.util;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;

public final class DeviceUtils {
	
	private static String imei;
	private static String swVersion;
	private static String model;
	private static String tipo;
	private static DisplayMetrics metrics;
	private static int width;
	private static int height;
	
	
	private DeviceUtils() {
		
	}

	/**
	 * 
	 * @param ctx Context desde donde se ejecutará el método.
	 * @return El imei del dispositivo, en caso de que no lo encuentre el id del dispositivo.
	 */
	public static synchronized String getIMEI(Context ctx){
		
		if (TextUtils.isEmpty(imei)) {
			try{
				imei = ((TelephonyManager)ctx.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
				if (TextUtils.isEmpty(imei)){
					imei = Settings.System.getString(ctx.getContentResolver(), Settings.Secure.ANDROID_ID);
				}
			}catch(Exception e){
				imei = Settings.System.getString(ctx.getContentResolver(), Settings.Secure.ANDROID_ID);
				Log.e("getImei",e.getMessage());
			}
		}
		
		return imei;
	}
	
	/**
	 * 
	 * @return La versión de software del dispositivo
	 */
	public static synchronized String getSWVersion(){
		if (TextUtils.isEmpty(swVersion))
			swVersion = Build.VERSION.RELEASE;
		
		return swVersion;
	}
	
	/**
	 * 
	 * @return Modelo del dispositivo.
	 */
	public static synchronized String getModel(){
		if (TextUtils.isEmpty(model))
			model = Build.MODEL;
		
		return model;
	}
	
	/**
	 * 
	 * @return Obtener tipo del dispositivo.
	 */
	public static synchronized String getTipo(){
		if (TextUtils.isEmpty(tipo))
			tipo = Build.MANUFACTURER;
		
		return tipo;
	}
	
	/**
	 * 
	 * @param context Context desde el que se ejecuta el método
	 * @return Una sola instancia del objeto DisplayMetrics del dispositivo
	 */
	private static synchronized DisplayMetrics getMetrics(Context context) {
		if (metrics == null) {
			
			metrics = new DisplayMetrics();
			WindowManager mgr = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE));
			mgr.getDefaultDisplay();
			mgr.getDefaultDisplay().getMetrics(metrics);
		}
		
		return metrics;
	}
	
	/**
	 * 
	 * @param con Context desde donde se ejecuta el método.
	 * @return Ancho de pantalla
	 */
	public static synchronized int getWidth(Context con) {
		if (width <= 0)
			width = getMetrics(con).widthPixels;
		
		return width;
	}
	
	/**
	 * 
	 * @param con Context desde donde se ejecuta el método.
	 * @return Alto de pantalla
	 */
	public static synchronized int getHeight(Context con) {
		if (height <= 0)
			height = getMetrics(con).heightPixels;
		
		return height;
	}
}
