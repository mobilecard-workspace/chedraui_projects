package addcel.util;

import org.addcel.android.chedrauipos.R;

import android.content.Context;

import com.squareup.phrase.Phrase;

public final class ErrorUtil {
	
	private static CharSequence sequence;
	
	private ErrorUtil() {
		
	}
	
	public static synchronized CharSequence vacio(Context con, int campo) {
		
		sequence = Phrase.from(con, R.string.error_vacio).put("campo", con.getText(campo)).format();
		
		return sequence;
	}
	
	public static synchronized CharSequence noValido(Context con, int dato) {
		sequence = Phrase.from(con, R.string.error_valido).put("dato", con.getText(dato)).format();
		
		return sequence;
	}
	
	public static synchronized CharSequence errorServicio(Context con, String servicio) {
		sequence = Phrase.from(con, R.string.error_servicio).put("servicio", servicio).format();
		
		return sequence;
	}
	
	public static synchronized CharSequence errorLista(Context con, String lista) {
		sequence = Phrase.from(con, R.string.error_lista).put("lista", lista).format();
		
		return sequence;
	}
}
