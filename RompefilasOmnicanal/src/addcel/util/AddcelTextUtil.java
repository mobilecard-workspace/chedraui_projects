package addcel.util;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.w3c.dom.Text;

import android.text.TextUtils;
import android.util.Log;

public final class AddcelTextUtil {

	private AddcelTextUtil() {

	}

	private static byte[] auxArr;

	private static Locale MX_LOCALE = new Locale("es", "MX");

	public static synchronized Locale getMexLocale() {

		if (null == MX_LOCALE)
			MX_LOCALE = new Locale("es", "MX");

		return MX_LOCALE;
	}

	/**
	 * Verifica si un texto dado, es correo o no
	 * 
	 * @return boolean.
	 */
	public static synchronized boolean esCorreo(String correo) {
		return (correo
				.matches("^[_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@+([_a-zA-Z0-9-]+\\.)*[a-zA-Z0-9-]{2,200}\\.[a-zA-Z]{2,6}$"));
	}

	public static synchronized String parsePass(String pass) {
		int len = pass.length();
		String key = "";

		for (int i = 0; i < 32 / len; i++) {
			key += pass;
		}

		int carry = 0;
		while (key.length() < 32) {
			key += pass.charAt(carry);
			carry++;
		}
		return key;
	}

	public static synchronized String reverse(String chain) {
		String nuevo = "";

		for (int i = chain.length() - 1; i >= 0; i--) {
			nuevo += chain.charAt(i);
		}

		return nuevo;
	}

	public static synchronized String mergeStr(String first, String second) {
		String result = "";
		String other = AddcelTextUtil.reverse(second);

		result += (Integer.toString(other.length()).length() < 2) ? "0"
				+ other.length() : Integer.toString(other.length());

		String sub1 = first.substring(0, 19);
		String sub2 = first.substring(19, first.length());

		result += sub1;

		for (int i = 0; i < other.length(); i += 2) {
			int offset = ((i + 2) <= other.length()) ? (i + 2) : (i + 1);
			String next = other.substring(i, offset);

			result += next;

			result += sub2.substring(0, 2);

			sub2 = sub2.substring(2);
		}
		result += sub2;

		return result;
	}

	public static synchronized String formatDate(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
		return format.format(date);
	}

	public static synchronized String formatDateNoSpace(Date date) {

		SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy", Locale.US);
		return format.format(date);
	}

	public static synchronized String formatDateWithSlash(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd", Locale.US);
		return format.format(date);

	}

	public static synchronized String formatDateForShowing(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
		return format.format(date);
	}

	public static synchronized String formatDateTimeForShowing(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm",
				Locale.US);
		return format.format(date);
	}

	public static synchronized String formatTimeForShowing(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("hh:mm", Locale.US);
		return format.format(date);
	}

	public static synchronized String formatDateVitamedica(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy", Locale.US);
		return format.format(date);
	}

	public static synchronized String toVitaDate(String date) {
		Log.i("Text", date);
		Date d = getSimpleDateFromString(date);
		return formatDateVitamedica(d);
	}

	public static synchronized String formatTimer(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("dd hh:mm:ss", Locale.US);
		return format.format(date);
	}

	public static synchronized String formatCurrency(double cantidad) {

		NumberFormat formatter = NumberFormat
				.getCurrencyInstance(getMexLocale());
		formatter.setMaximumFractionDigits(2);

		String moneyString = formatter.format(cantidad);

		return moneyString;
	}
	
	public static synchronized String formatCurrency(BigDecimal cantidad) {
		NumberFormat formatter = NumberFormat
				.getCurrencyInstance(getMexLocale());
		formatter.setMaximumFractionDigits(2);

		String moneyString = formatter.format(cantidad);

		return moneyString;
	}

	public static synchronized Integer[] getYearsArray() {
		Calendar c = Calendar.getInstance();
		Integer[] years = new Integer[10];

		int i = 0;
		int year = c.get(Calendar.YEAR);

		while (i < 10) {

			years[i] = year;
			year++;
			i++;
		}

		Log.i("years array", Arrays.toString(years));

		return years;

	}

	public static synchronized String trimMonth(int month) {
		month = month + 1;

		if (month < 10) {
			return "0" + Integer.toString(month);
		} else {
			return Integer.toString(month);
		}
	}

	public static synchronized String trimYear(int year) {
		return Integer.toString(year).substring(2, 4);
	}

	public static synchronized String getVigencia(int month, int year) {
		return trimMonth(month) + "/" + year;
	}

	public static synchronized Date getDateFromString(String s) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",
				Locale.US);
		try {
			Date fecha = format.parse(s);
			Log.i("getDateFromString", fecha.toString());
			return fecha;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return null;
		}

	}

	public static synchronized Date getDateFromStringWithMS(String s) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S",
				Locale.US);
		try {
			Date fecha = format.parse(s);
			Log.i("getDateFromString", fecha.toString());
			return fecha;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return null;
		}

	}

	public static synchronized String getStringFromDateWithMS(Date d) {

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S",
				Locale.US);
		return format.format(d);
	}

	public static synchronized Date getSimpleDateFromString(String s) {

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

		try {
			Date fecha = format.parse(s);
			Log.d("getSimpleDateFromString", fecha.toString());
			return fecha;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return null;
		}

	}

	public static synchronized Date getSimpleDateFromStringWithSlash(String s) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd", Locale.US);
		try {
			Date fecha = format.parse(s);
			Log.d("getSimpleDateFromStringWithSlash", fecha.toString());
			return fecha;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	public static synchronized Date getDateFromETNString(String s) {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd", Locale.US);
		try {
			Date fecha = format.parse(s);
			Log.d("getDateFromETNString", fecha.toString());
			return fecha;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	public static synchronized Date getEtnDateFromStringWithSlash(String s) {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
		try {
			Date fecha = format.parse(s);
			Log.d("getSimpleDateFromStringWithSlash", fecha.toString());
			return fecha;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	public static synchronized String addParamsToUrl(String url,
			List<BasicNameValuePair> params) {
		if (!url.endsWith("?"))
			url += "?";

		String paramString = URLEncodedUtils.format(params, "utf-8");

		url += paramString;

		return url;
	}

	public static synchronized String getKey() {
		return String.valueOf(new Date().getTime()).substring(0, 8);

	}

	public static synchronized String getPeriodosConPipe(List<String> list) {
		String listString = "";

		List<String> subList = list.subList(0, list.size() - 1);

		for (String periodo : subList) {
			listString = listString + periodo + "|";
		}

		return listString + list.get(list.size() - 1);
	}
	
	public static synchronized String makePlaceholders(int len) {
	    if (len < 1) {
	        // It will lead to an invalid query anyway ..
	        throw new RuntimeException("No placeholders");
	    } else {
	        StringBuilder sb = new StringBuilder(len * 2 - 1);
	        sb.append("?");
	        for (int i = 1; i < len; i++) {
	            sb.append(",?");
	        }
	        return sb.toString();
	    }
	}

	/**
	 * Convierte un String a arreglo de bytes UTF-8 (byte[])
	 * 
	 * @param str String a convertir
	 * @return
	 */
	public static synchronized byte[] toByteArray(String paramName, String str) {
		StringBuilder sb = new StringBuilder();
		
		if (!TextUtils.isEmpty(paramName)) {
			sb.append(paramName).append("=").append(str);
		} else {
			sb.append(str);
		}
		
		auxArr = sb.toString().getBytes(Charset.defaultCharset());

		return auxArr;
	}

	public static synchronized String byteArrToString(byte[] bytes) {
		return new String(bytes, Charset.defaultCharset());
	}

	/**
	 * 
	 * @param card
	 *            Número de tarjeta bancaria
	 * @return Tarjeta enmascarada de la forma XXXX XXXX XXXX ####
	 */
	public static synchronized String maskCard(String card) {

		StringBuilder sb = new StringBuilder();

		int i = 1; // Iniciamos índice en 1 para poder hacer modulo 4 sobre el
					// índice del String de la
					// tarjeta

		while (i <= card.length()) {

			if (i <= card.length() - 4) {
				sb.append("X");
			} else {
				sb.append(card.charAt(i - 1)); // Manejamos índice convencional
												// para llamar el método.
			}

			if (i < card.length() && i % 4 == 0)
				sb.append(" ");

			i++;
		}

		return sb.toString();
	}

	public static synchronized String buildJSPaymentString(String nombre,
			String tarjeta, String tipo, int mes, int anio) {

		String tag = "buildJSPaymentString";

		Log.d(tag, nombre);
		Log.d(tag, maskCard(tarjeta));
		Log.d(tag, tipo);
		Log.d(tag, String.valueOf(mes));
		Log.d(tag, String.valueOf(anio));

		int formattedMes = mes - 1;
		int formattedAnio = anio - Calendar.getInstance().get(Calendar.YEAR);

		StringBuilder builder = new StringBuilder(
				"javascript:document.getElementsByName('cc_name')[0].value='")
				.append(nombre)
				.append("';")
				.append("document.getElementsByName('cc_number')[0].value='")
				.append(tarjeta)
				.append("';")
				.append("document.getElementsByName('cc_type')[0].value='")
				.append(tipo)
				.append("';")
				.append("document.getElementsByName('_cc_expmonth')[0].selectedIndex=")
				.append(formattedMes)
				.append(";")
				.append("document.getElementsByName('_cc_expyear')[0].selectedIndex=")
				.append(formattedAnio).append(";");

		return builder.toString();

	}

	public static synchronized String buildJSAmexPaymentString(String nombre) {

		String tag = "buildJSAmexPaymentString";
		Log.d(tag, nombre);

		return new StringBuilder(
				"javascript:document.getElementsByName('cc_name')[0].value='")
				.append(nombre).append("';").toString();
	}

}
