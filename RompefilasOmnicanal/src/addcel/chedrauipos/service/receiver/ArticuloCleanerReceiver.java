package addcel.chedrauipos.service.receiver;

import java.util.Calendar;

import addcel.chedrauipos.service.ArticulosCleanerService;
import addcel.receiver.BootReceiver;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

public class ArticuloCleanerReceiver extends WakefulBroadcastReceiver {
	
	private AlarmManager aManager;
	private PendingIntent aIntent;

	private static final String TAG = ArticuloCleanerReceiver.class
			.getSimpleName();
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		Log.d(TAG, "Recibe señal para lanzar servicio.");
		Intent service = new Intent(context, ArticulosCleanerService.class);
		startWakefulService(context, service);
	}
	
	public void setAlarm(Context context) {
		// TODO Auto-generated method stub
		aManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(context, ArticuloCleanerReceiver.class);
		aIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		
		/**************Implementación para QA*****************/
		// Set the alarm's trigger time to 2:00 a.m.
//		calendar.set(Calendar.HOUR_OF_DAY, 13);
//		calendar.set(Calendar.MINUTE, 15);
//
//		aManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
//				calendar.getTimeInMillis(), AlarmManager.INTERVAL_HOUR, aIntent);
		/******************************************************/
		
		/**************Implementación para producción*****************/
		// Set the alarm's trigger time to 2:00 a.m.
		calendar.set(Calendar.HOUR_OF_DAY, 2);
		calendar.set(Calendar.MINUTE, 0);

		aManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
				calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, aIntent);
		/******************************************************/

		ComponentName receiver = new ComponentName(context, BootReceiver.class);
		PackageManager pm = context.getPackageManager();

		pm.setComponentEnabledSetting(receiver,
				PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
				PackageManager.DONT_KILL_APP);

	}

	public void cancelAlarm(Context context) {
		// If the alarm has been set, cancel it.
		if (aManager != null) {
			aManager.cancel(aIntent);
		}

		// Disable {@code SampleBootReceiver} so that it doesn't automatically
		// restart the
		// alarm when the device is rebooted.
		ComponentName receiver = new ComponentName(context, BootReceiver.class);
		PackageManager pm = context.getPackageManager();

		pm.setComponentEnabledSetting(receiver,
				PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
				PackageManager.DONT_KILL_APP);
	}

}
