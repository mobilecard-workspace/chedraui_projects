package addcel.chedrauipos.service.receiver;

import addcel.chedrauipos.service.ActualizacionPeriodicaService;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

public class ActualizacionPeriodicaReceiver extends WakefulBroadcastReceiver {
	
	private AlarmManager aManager;
	private PendingIntent aIntent;

	private static final String TAG = ActualizacionPeriodicaReceiver.class.getSimpleName();


	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		Log.i(TAG, "Recibe señal para lanzar servicio.");
		Intent service = new Intent(context, ActualizacionPeriodicaService.class);
		startWakefulService(context, service);
	}
	
	public void setAlarm(Context context) {
		
		aManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(context, ActualizacionPeriodicaReceiver.class);
		aIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

		/*         
		 *  // Wake up the device to fire the alarm in 30 minutes, and every 30 minutes
         * // after that.
         */
        aManager.setRepeating(AlarmManager.ELAPSED_REALTIME, 
                 AlarmManager.INTERVAL_HALF_HOUR, //REGRESAR A MEDIA HR
                 AlarmManager.INTERVAL_HALF_HOUR, aIntent);
        
        Log.d(TAG, "Actualizacion periodica programada exitosamente.");
    }

	 public void cancelAlarm(Context context) {
	        // If the alarm has been set, cancel it.
	        if (aManager!= null) {
	            aManager.cancel(aIntent);

	            Log.d(TAG, "Actualizacion periodica cancelada exitosamente.");
	        } else {

	            Log.d(TAG, "No existe actualización.");
	        }
	    }

}
