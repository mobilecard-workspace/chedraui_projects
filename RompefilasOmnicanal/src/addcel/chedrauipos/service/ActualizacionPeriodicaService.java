package addcel.chedrauipos.service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.addcel.android.chedrauipos.AppStatus;
import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.ServicesStatus;
import org.addcel.android.chedrauipos.Url;
import org.joda.time.DateTime;

import addcel.chedrauipos.model.Articulo;
import addcel.chedrauipos.model.factory.ArticuloFactory;
import addcel.chedrauipos.service.receiver.ActualizacionPeriodicaReceiver;
import addcel.chedrauipos.util.ChedrauiTextUtil;
import addcel.chedrauipos.util.FileTransferClient;
import addcel.chedrauipos.util.SftpTransferClient;
import addcel.otto.BusFactory;
import addcel.util.NetworkUtil;
import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import com.activeandroid.ActiveAndroid;

public class ActualizacionPeriodicaService extends IntentService {
	
	private FileTransferClient client;
	private String myPath;
	private String serverPath;
	
	private static final String TAG = ActualizacionPeriodicaService
			.class.getSimpleName();

	public ActualizacionPeriodicaService() {
		super("ActualizacionPeriodicaService");
		// TODO Auto-generated constructor stub
		Log.d(TAG, "Inicializando service");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		Log.d(TAG, "Entrando a onHandleIntent");
		// preferences = new SessionManager(this);
		ServicesStatus.setStatus(ServicesStatus.ACTUALIZACION, true);
		if (NetworkUtil.isOnline(this)) {
			if (AppStatus.get(this).getCodigo() != null) {
				this.client = SftpTransferClient.get( "50.57.192.214", "rmuniz", "xZZWj4GFzTZa");
				
				this.myPath = getFilesDir()
						+ "/"
						+ ChedrauiTextUtil.buildStockFileName(AppStatus.get(this).getCodigo(), "UPD");
				this.serverPath = Url.DESCARGA;
				int actualizado = get();
				Log.d(TAG, "Termino proceso de descarga de archivo");
				if (actualizado == 0) {
					int update = update(myPath);
					switch (update) {
					case 0:
						reportServiceSuccess(new DateTime());
						break;

					default:
						reportServiceError(getString(R.string.error_insert_masivo));
						break;
					}
				} else {
					reportServiceError(getString(R.string.error_descarga_masiva));
				}
			} else {
				reportServiceError(getString(R.string.error_sucursal));
			}
		} else {
			reportServiceError(getString(R.string.error_conexion));
		}
		
		/*
		 * Marcamos que el servicio ya no se encuentra activo y señalamos que se
		 * terminó de ejecutar el servicio
		 */
		ServicesStatus.setStatus(ServicesStatus.ACTUALIZACION, false);
		ActualizacionPeriodicaReceiver.completeWakefulIntent(intent);
	}
	
	private int get() {
		if (((SftpTransferClient) client).connect()) {
			return client.getFile(myPath, serverPath);
		} else {
			return -10000;
		}
	}
	
	private void reportServiceError(final String error) {
		Handler h = new Handler(Looper.getMainLooper());
		h.post(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				BusFactory.get().post(error);
			}
		});
		
	}
	
	private void reportServiceSuccess(final Object o) {
		Handler h = new Handler(Looper.getMainLooper());
		h.post(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				BusFactory.get().post(o);
			}
		});
	}

	private int update(String myPath) {

		FileInputStream _ims = null;
		BufferedReader reader = null;
		ActiveAndroid.beginTransaction();
		try {
			_ims = new FileInputStream(myPath);

			reader = new BufferedReader(new InputStreamReader(_ims, "UTF8"));
			String line;
			while ((line = reader.readLine()) != null) {
				String[] lineSplit = TextUtils.split(line, "\\|");
				Articulo a = Articulo.getArticulo(Long.parseLong(lineSplit[0]));
				if (a == null) {
					Articulo newArt = ArticuloFactory.get(lineSplit);
					long id = newArt.save();
					Log.i(TAG, "Artículo creado: " + id);
				} else {
					a.setDescripcion(lineSplit[4]);
					a.setFlat(lineSplit[8]);
					a.setPrecio(Double.parseDouble(lineSplit[9]));
					a.setIva(lineSplit[10]);
					a.setIeps(lineSplit[11]);
					a.setPromo(lineSplit[12]);
					a.setDescuento(Double.parseDouble(lineSplit[13]));
					a.save();
					Log.i(TAG, "Artículo actualizado: " + a.getId());
				}
			}
			ActiveAndroid.setTransactionSuccessful();
			return 0;

		} catch (UnsupportedEncodingException e) {
			Log.e(TAG, IOException.class.getSimpleName(), e);
			return -10000;
		} catch (IOException ex) {
			// handle exception
			Log.e(TAG, IOException.class.getSimpleName(), ex);
			return -10000;
		}

		finally {
			ActiveAndroid.endTransaction();
			
			try {
				_ims.close();
			} catch (IOException e) {
				Log.e(TAG, IOException.class.getSimpleName(), e);
			}
		}
	}

}
