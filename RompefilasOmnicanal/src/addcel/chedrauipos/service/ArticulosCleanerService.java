package addcel.chedrauipos.service;

import org.addcel.android.chedrauipos.AppStatus;
import org.addcel.android.chedrauipos.ServicesStatus;

import addcel.chedrauipos.model.Articulo;
import addcel.chedrauipos.service.receiver.ActualizacionPeriodicaReceiver;
import addcel.chedrauipos.service.receiver.ArticuloCleanerReceiver;
import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

public class ArticulosCleanerService extends IntentService {
	
	private ActualizacionPeriodicaReceiver pAlarm = new ActualizacionPeriodicaReceiver();
	private AppStatus status;
	
	private static final String TAG = ArticulosCleanerService.class.getSimpleName();

	public ArticulosCleanerService() {
		super(TAG);
		// TODO Auto-generated constructor stub
		Log.d(TAG, "Inicializando service.");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		Log.i(TAG, "Entrando a onHandleIntent");
		ServicesStatus.setStatus(ServicesStatus.CLEANER, true);
		try {
			cleanDatabase();
			
			status = AppStatus.get(this);
			status.setFechaActualizacion(null);
			status.setUpdated(false);
			
			// Cancelamos lanzamiento de actualizaciones periódicas.
			pAlarm.cancelAlarm(this);
		} catch (Exception e) {
			Log.e(TAG, Exception.class.getSimpleName(), e);
		} finally {
			ServicesStatus.setStatus(ServicesStatus.CLEANER, false);
			ArticuloCleanerReceiver.completeWakefulIntent(intent);
		}
	}
	
	private void cleanDatabase() throws Exception {
		Articulo.deleteAll();
	}

}
