package addcel.chedrauipos.util;

import addcel.chedrauipos.model.Articulo;

/**
 * Interfaz que especifica métodos para adecuar los códigos de barra 
 * y artículos a la lógica de negocio de chedraui
 * @author carlosgs
 *
 */
public interface BarcodeProcessor {
	/**
	 * Modifica barcode capturado para buscarlo en base de datos
	 * @param barcode
	 * @return
	 */
	public String getUpc(String barcode);
	/**
	 * Modifica artículo obtenido para su correcto procesamiento en el POS
	 * @param barcode
	 * @param articulo
	 * @return
	 */
	public Articulo processArticulo(String barcode, Articulo articulo);
}
