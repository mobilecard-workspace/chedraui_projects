package addcel.chedrauipos.util;

import java.io.IOException;

import addcel.chedrauipos.model.Ticket;
import addcel.util.AddcelTextUtil;
import android.bluetooth.BluetoothDevice;

import com.zebra.sdk.comm.BluetoothConnectionInsecure;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.graphics.ZebraImageFactory;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;

public final class BarcodePrinterUtil {
	
	private BarcodePrinterUtil() {
		
	}

	public static synchronized int printBarcode(BluetoothDevice device, Ticket ticket){
		
		String barcodeStr = "";
		
		try {
			 Connection connection = new BluetoothConnectionInsecure(device.getAddress());
//			 Connection connection = ConnectionFactory.get(device.getAddress());
             connection.open();
             ZebraPrinter printer = ZebraPrinterFactory.getInstance(connection);
             int x = 0;
             int y = 0;
             connection.write("! UTILITIES\r\nIN-MILLIMETERS\r\nSETFF 10 2\r\nPRINT\r\n".getBytes());
             printer.printImage(ZebraImageFactory.getImage(ticket.getBarcode()), x, y, 0, 0, false);
             String bcStr = barcodeStr + "\n\n";
             connection.write(bcStr.getBytes());
             String importe = "TOTAL M.N.: " + AddcelTextUtil.formatCurrency(ticket.getTotal()) + "\n\n\n";
             connection.write(importe.getBytes());
             connection.close();
		     return 0;
		 } catch(ConnectionException e) {
			 return -1;
		 } catch(ZebraPrinterLanguageUnknownException e) {
			 return -1;
		 } catch(IOException e) {
			 return -1;
		 }
	}
}
