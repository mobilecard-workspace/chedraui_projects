package addcel.chedrauipos.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;

import addcel.chedrauipos.omnicanal.Constantes;
import addcel.chedrauipos.omnicanal.vo.PedidoResponse;
import addcel.chedrauipos.omnicanal.vo.RespuestaConsultarTiendasPedido;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

public final class FileGenerationUtil {
	
	private static FileGenerationUtil instance;
	
	private static final String TAG = FileGenerationUtil.class.getSimpleName();

	private FileGenerationUtil() {
		
	}
	
	public static synchronized FileGenerationUtil get() {
		if (instance == null)
			instance = new FileGenerationUtil();
		
		return instance;
	}
	
	
	public String generaArchivoRF(Context con, String barCode, String fileString) {
		
//		A partir de la posición 2 es donde empieza el consecutivo y 
//		tomas 1 digito del precio
		String consecutivoArchivo = barCode.substring(2, 7);
		
		String archivo = "MTL" + consecutivoArchivo + ".dat";
		
		Log.d(TAG, "Generando archivo MTL: " + archivo);
		
		File mtlFile = new File(con.getFilesDir(), archivo);
		
//		Para el caso en que ya se reinicio el consecutivo de tickets (9999 a 9001) 
//		y pudiera generarse un mtl ya existente
		if(mtlFile.exists()){
			Log.d(TAG, "El archivo: " + archivo + " ya existia, por lo que sera borrado "
					+ "para no afectar la transaccion actual");
			mtlFile.delete();
		}
		
		try {
			
			FileWriter fw = new FileWriter(mtlFile);
			BufferedWriter bw = new BufferedWriter(fw);
			
			bw.write(fileString);
			bw.flush();
			bw.close();
			fw.close();
		
		} catch (IOException e) {
			Log.e(TAG, "IOException, ", e);
		} catch(Exception e){
			Log.e(TAG, "Exception, ", e);
		}
		
		return archivo;
	}
	
	public String generaArchivoOC(Context con, PedidoResponse pedido) {
		String fileName = null;
		
		if(pedido.getOrderIncrementId().length() > 8){
			fileName = pedido.getOrderIncrementId().substring(0, pedido.getOrderIncrementId().length() - 3) + "." +
					pedido.getOrderIncrementId().substring(pedido.getOrderIncrementId().length() - 3);
		}else{
			fileName = pedido.getOrderIncrementId();
		}
		
		File mtlFile = new File(con.getFilesDir(), fileName);
		
//		Para el caso en que ya se reinicio el consecutivo de tickets (9999 a 9001) 
//		y pudiera generarse un mtl ya existente
		if(mtlFile.exists()){
			Log.d(TAG, "El archivo: " + fileName + " ya existia, por lo que sera borrado "
					+ "para no afectar la transaccion actual");
			mtlFile.delete();
		}
		
		try {
			
			FileWriter fw = new FileWriter(mtlFile);
			BufferedWriter bw = new BufferedWriter(fw);
			
			bw.write(buildOmnicanalFileString(pedido));
			bw.flush();
			bw.close();
			fw.close();
		
		} catch (IOException e) {
			Log.e(TAG, "IOException, ", e);
		} catch(Exception e){
			Log.e(TAG, "Exception, ", e);
		}
		
		return fileName;
	}
	
	public String buildOmnicanalFileString(PedidoResponse respServ){
		
		StringBuilder cadFile = new StringBuilder();
		addcel.chedrauipos.omnicanal.vo.RespuestaConsultarPedidoItem itemPedido;
		addcel.chedrauipos.omnicanal.vo.RespuestaConsultarPedidoPromociones promocionPedido;
		addcel.chedrauipos.omnicanal.vo.PromocionVO promo;
		
		cadFile.append("0;").append(respServ.getPedido().getNumeroPedido()).append(";").append(respServ.getPedido().getTienda()).append(";")
		.append(formatoMonto(respServ.getPedido().getTotal().doubleValue())).append(";")
		.append(respServ.getTiendaPadre()).append(";").append(respServ.getPedido().getStatus()).append(";")
		.append(formatoMonto(respServ.getPedido().getTotalEnvio().doubleValue())).append(";")
		.append(respServ.getPedido().getMetodoEnvio()).append("\r\n");
		
		if(respServ.getPedido().getItemsp() != null){
			for(int i = 0; i < respServ.getPedido().getItemsp().length; i++){
				
				itemPedido = respServ.getPedido().getItemsp()[i];
				
				promo = respServ.getPromosArticulos().get(i);
				if(promo == null){
					promo = new addcel.chedrauipos.omnicanal.vo.PromocionVO();
				}
				
				if (respServ.getPedido().getItempTienda()!= null) {
					
					boolean tieneProximidad = false;
					
					for (RespuestaConsultarTiendasPedido tPedido: respServ.getPedido().getItempTienda()) {
						
						if (tPedido.getSku() != null && tPedido.getSku().equals(itemPedido.getSku())) {

							cadFile.append("1;").append(itemPedido.getSku()).append(";").append(itemPedido.getEsPesable()).append(";")
							.append(formatoMonto(itemPedido.getPrecio().doubleValue())).append(";")
							.append(tPedido.getTotalArticulos().intValue()).append(";")
							.append(isEmptyPost(promo.getTipoBeneficio())).append(";").append(isEmptyPost(promo.getPromoType())).append(";")
							.append(isEmptyPost(itemPedido.getPromoId())).append(";").append(isEmptyPost(promo.getDescripcionPromocion())).append(";")
							.append(formatoMonto(itemPedido.getMontoBeneficio()!= null? itemPedido.getMontoBeneficio().doubleValue(): 0)).append(";")
							.append(isEmptyPost(
									promo.getPorcentajeDescuento() > 0? formatoMonto(promo.getPorcentajeDescuento()):
									promo.getPorcentajeBonificacion() > 0? formatoMonto(promo.getPorcentajeBonificacion()):"")).append(";")
							.append(isEmptyPost(itemPedido.getNumeroTarjeta())).append(";")
							//.append(respServ.getPlazos()).append(";")
							.append(isEmptyPost(itemPedido.getNumeroAutorizacion())).append(";")
							.append(isEmptyPost(promo.getDisparador())).append(";").append(promo.getRangoMontoPromocional()).append(";")
							.append(isEmptyPost(tPedido.getTienda()))
							.append("\r\n");
							
							tieneProximidad = true;
						}
					}
					
					if (!tieneProximidad) {
						cadFile.append("1;").append(itemPedido.getSku()).append(";").append(itemPedido.getEsPesable()).append(";")
						.append(formatoMonto(itemPedido.getPrecio().doubleValue())).append(";")
						.append(itemPedido.getEsPesable() != null && "0".equals(itemPedido.getEsPesable()) ? 
								itemPedido.getTotalArticulos().intValue() : ((int)(itemPedido.getTotalArticulos().doubleValue()*1000))).append(";")
						.append(isEmptyPost(promo.getTipoBeneficio())).append(";").append(isEmptyPost(promo.getPromoType())).append(";")
						.append(isEmptyPost(itemPedido.getPromoId())).append(";").append(isEmptyPost(promo.getDescripcionPromocion())).append(";")
						.append(formatoMonto(itemPedido.getMontoBeneficio()!= null? itemPedido.getMontoBeneficio().doubleValue(): 0)).append(";")
						.append(isEmptyPost(
								promo.getPorcentajeDescuento() > 0? formatoMonto(promo.getPorcentajeDescuento()):
								promo.getPorcentajeBonificacion() > 0? formatoMonto(promo.getPorcentajeBonificacion()):"")).append(";")
						.append(isEmptyPost(itemPedido.getNumeroTarjeta())).append(";")
						//.append(respServ.getPlazos()).append(";")
						.append(isEmptyPost(itemPedido.getNumeroAutorizacion())).append(";")
						.append(isEmptyPost(promo.getDisparador())).append(";").append(promo.getRangoMontoPromocional()).append(";")
						.append(isEmptyPost(respServ.getPedido().getTienda()))
						.append("\r\n");
					}
					
					
				} else {
					
					cadFile.append("1;").append(itemPedido.getSku()).append(";").append(itemPedido.getEsPesable()).append(";")
					.append(formatoMonto(itemPedido.getPrecio().doubleValue())).append(";")
					.append(itemPedido.getEsPesable() != null && "0".equals(itemPedido.getEsPesable()) ? 
							itemPedido.getTotalArticulos().intValue() : ((int)(itemPedido.getTotalArticulos().doubleValue()*1000))).append(";")
					.append(isEmptyPost(promo.getTipoBeneficio())).append(";").append(isEmptyPost(promo.getPromoType())).append(";")
					.append(isEmptyPost(itemPedido.getPromoId())).append(";").append(isEmptyPost(promo.getDescripcionPromocion())).append(";")
					.append(formatoMonto(itemPedido.getMontoBeneficio()!= null? itemPedido.getMontoBeneficio().doubleValue(): 0)).append(";")
					.append(isEmptyPost(
							promo.getPorcentajeDescuento() > 0? formatoMonto(promo.getPorcentajeDescuento()):
							promo.getPorcentajeBonificacion() > 0? formatoMonto(promo.getPorcentajeBonificacion()):"")).append(";")
					.append(isEmptyPost(itemPedido.getNumeroTarjeta())).append(";")
					//.append(respServ.getPlazos()).append(";")
					.append(isEmptyPost(itemPedido.getNumeroAutorizacion())).append(";")
					.append(isEmptyPost(promo.getDisparador())).append(";").append(promo.getRangoMontoPromocional()).append(";")
					.append(isEmptyPost(respServ.getPedido().getTienda()))
					.append("\r\n");
				}
			}
		}
		
		cadFile.append("5;")
		.append("01".equalsIgnoreCase(respServ.getPedido().getMetodoPago()) ? Constantes.STATUS_NUM_CONTRA_ENTREGA:
			"02".equalsIgnoreCase(respServ.getPedido().getMetodoPago()) ? Constantes.STATUS_NUM_CREDITO_AMEX:
			"03".equalsIgnoreCase(respServ.getPedido().getMetodoPago()) ? Constantes.STATUS_NUM_CREDITO_BANCOMER: 
			"04".equalsIgnoreCase(respServ.getPedido().getMetodoPago()) ? Constantes.STATUS_NUM_PEDIDO_PAGADO_OTRA_TIENDA:
			"05".equalsIgnoreCase(respServ.getPedido().getMetodoPago()) ? Constantes.STATUS_NUM_MONEDERO_CHEDRAUI:
				Constantes.STATUS_NUM_PEDIDO_PAGADO_OTRA_TIENDA)
		.append(";").append(
				!respServ.getPedido().getNumeroPedido().equals(respServ.getOrderIncrementId()) && !"01".equalsIgnoreCase(respServ.getPedido().getMetodoPago())?
					formatoMonto(respServ.getPedidoOriginal().getTotal().doubleValue()):
					formatoMonto(respServ.getPedido().getTotal().doubleValue())
				)
//			.append(respServ.getReferencia() != null? ";" + respServ.getReferencia(): "").append("");
		.append("02".equalsIgnoreCase(respServ.getPedido().getMetodoPago()) || "03".equalsIgnoreCase(respServ.getPedido().getMetodoPago())? ";02296;" : "; ;")
		.append(isEmptyPost(respServ.getPedido().getTarjeta())) //Solicitado por Alfonso y Germán de chedraui
//		.append(" ")
		.append("\r\n");
		
//		.append("6;")
//		.append(isEmptyPost(respServ.getNumeroCupon())).append(";").append(isEmptyPost(respServ.getAccionCupon())).append(";")
//		.append(isEmptyPost(respServ.getOferta())).append(";").append(isEmptyPost(respServ.getPromoType())).append(";")	
//		.append(isEmptyPost(respServ.getMontoCupon())).append(";").append(isEmptyPost(respServ.getTipoCupon())).append(";")
//		.append(isEmptyPost(respServ.getEstado()))
//		.append(isEmptyPost(" "))
//		.append("\r\n");
		
		
//			ITERACIÓN SOBRE ARREGLO respServ.getItemProm()

		if (respServ.getPedido().getItempProm() != null) {
	
			for(int i = 0; i < respServ.getPedido().getItempProm().length; i++) {
				promocionPedido = respServ.getPedido().getItempProm()[i];
				promo = respServ.getPromosGenerales().get(i);
				
				if(promo == null){
					promo = new addcel.chedrauipos.omnicanal.vo.PromocionVO();
				}
				
				cadFile.append("7;")
				.append(isEmptyPost(promo.getTipoBeneficio())).append(";").append(isEmptyPost(promo.getPromoType())).append(";")
				.append(isEmptyPost(promocionPedido.getPromoId())).append(";").append(isEmptyPost(promo.getDescripcionPromocion())).append(";")
				.append(formatoMonto(promocionPedido.getMontoBeneficio() != null ? promocionPedido.getMontoBeneficio().doubleValue() : 0)).append(";")
				.append(isEmptyPost(
				      promo.getPorcentajeDescuento() > 0 ? formatoMonto(promo.getPorcentajeDescuento()):
				      promo.getPorcentajeBonificacion() > 0 ? formatoMonto(promo.getPorcentajeBonificacion()):"")).append(";")
				.append(isEmptyPost(isEmptyPost(promocionPedido.getNumeroTarjeta()))).append(";")
				.append(isEmptyPost(isEmptyPost(promocionPedido.getPlazosPago()))).append(";")
				.append(isEmptyPost(promo.getDisparador())).append(";").append(promo.getRangoMontoPromocional()).append(";")
				.append(isEmptyPost(promocionPedido.getNumeroAutorizacion()))
				.append("\r\n");
					
			}
		}
		
		return cadFile.toString();
	}
		
		
	
	private String isEmptyPost(String str) {
		if (TextUtils.isEmpty(str)) {
			return " ";
		}
		
		return str;
	}
	
	private String formatoMonto(double monto){
		DecimalFormat df = new DecimalFormat("######.00");
		String totalPedido = null;
		try{
			totalPedido = df.format(monto );
			totalPedido = totalPedido.substring(0, totalPedido.indexOf(".")) + 
					totalPedido.substring(totalPedido.indexOf(".") +1, totalPedido.length());
		}catch(Exception e){
			Log.e(TAG, "Error en formatoMonto", e);
		}
		return totalPedido;
	}
}
