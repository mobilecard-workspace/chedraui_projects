package addcel.chedrauipos.util;


import org.addcel.android.chedrauipos.AppStatus;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

public final class BarcodeGenerationUtil {

	public static final int MAXIMO_CONSECUTIVO = 9999;
	public static final int INICIO_CONSECUTIVO = 9001;
	private static final String[] REPLACABLE = {".", ",",};
	private static final String[] REPLACES = {"", ""};
	
	private static BarcodeGenerationUtil instance;
	private static final String TAG = BarcodeGenerationUtil.class.getSimpleName();
	
	private BarcodeGenerationUtil() {
		
	}
	
	public static synchronized BarcodeGenerationUtil get() {
		if (instance == null) {
			instance = new BarcodeGenerationUtil();
		}
		
		return instance;
	}
	
	//el argumento monto es la cantidad de la transacción sin punto decimal
	public String generaCodigoBarrasTicket(String consecutivo, String monto){
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("28");
		
		sb.append(consecutivo);
		monto = TextUtils.replace(monto, REPLACABLE, REPLACES).toString();
//				monto.replace(".", "");
		Log.d(TAG, "La longitud del monto: " + monto + " es: " + monto.length());
		//El monto esta formado por 6 caracteres, por lo que si tienes un precio de 55.52, tienes que agregar dos ceros al inicio
		if(monto.length() == 4){
			monto = "00" + monto;
		}
		//El monto esta formado por 6 caracteres, por lo que si tienes un precio de 555.52, tienes que agregar un cero al inicio
		else if(monto.length() == 5){
			monto = "0" + monto;
		}
		
		sb.append(monto);
		
		sb.append(EAN13.get().generateVerDigit(sb.toString()));
		
		
		return sb.toString();
		
	}
	
	//Metodo para generar el numero de serie correspondiente al ticket en turno
	public String generaNSTicket(Context _con, int _consecutivo){
			
		Integer consecutivo = Integer.valueOf(_consecutivo);
		consecutivo++;
		
		Log.d(TAG,"Número de consecutivo para el ticket actual: " + consecutivo);
		
		if(consecutivo > MAXIMO_CONSECUTIVO){
			consecutivo = INICIO_CONSECUTIVO;
			Log.d(TAG,"Reiniciando consecutivo de tickets a: " + consecutivo);
		}
		
//			AUMENTAMOS CONSECUTIVO EN PREFERENCIAS
		AppStatus.get(_con).setConsecutivo(consecutivo);
		
		
		return consecutivo.toString();		
	}
}
