package addcel.chedrauipos.util;

public final class ChedrauiTextUtil {

	private static String str;

	private ChedrauiTextUtil() {

	}

	public static synchronized String buildStockFileName(String tienda,
			String prefix) {
		
		int tiendaLen = tienda.length();
		
		switch (tiendaLen) {
		case 1:
			str = prefix + "000" + tienda + ".txt";
			break;
		case 2:
			str = prefix + "00" + tienda + ".txt";
			break;

		case 3:
			str = prefix + "0" + tienda + ".txt";
			break;

		default:
			str = prefix + tienda + ".txt";
			break;
		}

		return str;
	}
}
