package addcel.chedrauipos.util;

import android.util.Log;


public final class BarcodeProcessorFactory {
	
	public enum ProcessorType {
		DEFAULT, PESABLE, NO_PROCESS, 
//		OMNICANAL
		;
	}

	private static BarcodeProcessor processor;
	private static final String TAG = BarcodeProcessorFactory.class.getSimpleName();
	
	public static synchronized void set(BarcodeProcessor p) {
		BarcodeProcessorFactory.processor = p;
	}
	
	public static synchronized BarcodeProcessor get(ProcessorType t) {
		switch (t) {
		case DEFAULT:
			processor = new DefaultBarcodeProcessor();
			break;

		case PESABLE:
			processor = new PesableBarcodeProcessor();
			break;
		case NO_PROCESS:
			processor = new BarcodeNotProcessor();
			break;
//		case OMNICANAL:
//			processor = new OmnicanalBarcodeProcessor();
//			break;
		default:
			break;
		}

		Log.d(TAG, "Se generó procesador " + t.name());
		return processor;
	}

}
