package addcel.chedrauipos.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.net.ftp.FTPClient;

import android.util.Log;
/**
 * Implementación de {@code FileTransferClient} orientada a FTP
 * @author carlosgs
 *
 */
public class FtpTransferClient implements FileTransferClient {
	
	private String host;
	private String login;
	private String password;
	private int port;
	private FTPClient client;
	
	private static FtpTransferClient instance;
	
	private static final String TAG = FtpTransferClient.class.getSimpleName();

	public FtpTransferClient() {
		client = new FTPClient();
	}
	
	public FtpTransferClient(String host, String login, String pass) {
		// TODO Auto-generated constructor stub
		this();
		this.host = host;
		this.login = login;
		this.password = pass;
		this.port = 21;
	}
	
	public static synchronized FtpTransferClient get(String host, String login, String pass) {
		
		if (instance == null) {
			instance = new FtpTransferClient();
		}
		
		instance.host = host;
		instance.login = login;
		instance.password = pass;
		instance.port = 21;
		
		return instance;
	}
	
	public static synchronized FtpTransferClient getQA() {
		return get("50.57.192.214", "chedraui", "8dntseBoBo");
	}
	
	public String getHost() {
		return host;
	}

	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}

	public int getPort() {
		return port;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
	@Override
	public boolean connect() {
		try {
			Log.d(TAG, "HOST: "+ host);
			client.connect(InetAddress.getByName(host), port);
			return client.login(login, password);
		} catch (IOException e) {
			e.printStackTrace();
			Log.e(TAG, "connect", e);
			return false;
		}
	}
	

	@Override
	public int getFile(String myPath, String serverPath) {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		sb.append("ftp://").append(login).append(":").append(password)
				.append("@").append(host).append(serverPath);

		String urlStr = sb.toString();
		Log.d(TAG, "El archivo se descargará  de: " + urlStr);

		try {
			URL url = new URL(urlStr);
			URLConnection conn = url.openConnection();
			InputStream inputStream = conn.getInputStream();

			FileOutputStream outputStream = new FileOutputStream(myPath);

			byte[] buffer = new byte[4096];
			int bytesRead = -1;
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, bytesRead);
			}

			outputStream.close();
			inputStream.close();
			Log.d(TAG, "Archivo descargado correctamente");
			return 0;

		} catch (IOException ex) {
			Log.e(TAG, "Error en descarga.", ex);
			return -10000;
		}
	}

	@Override
	public int sendFile(String myPath, String serverPath) {
		// TODO Auto-generated method stub
		try {
			FileInputStream srcFileStream = new FileInputStream(myPath);
			Log.d(TAG, serverPath);
			boolean status = client.storeFile(serverPath, srcFileStream);
			Log.d("OrderStatus", String.valueOf(status));
			srcFileStream.close();
			return status ? 0 : -10000;

		} catch (Exception e) {
			e.printStackTrace();
			return -10000;
		} finally {
			disconnect();
		}
	}

	@Override
	public boolean disconnect() {
		// TODO Auto-generated method stub
		try {
			if (client.isConnected()) {
				client.logout();
				client.disconnect();
				return true;
			} else {
				return false;
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

}
