package addcel.chedrauipos.util;

import addcel.chedrauipos.model.Articulo;
import android.text.TextUtils;

public class DefaultBarcodeProcessor implements BarcodeProcessor {

	@Override
	public String getUpc(String barcode) {
		// TODO Auto-generated method stub
		
		if (!TextUtils.isEmpty(barcode)) {
			if (TextUtils.indexOf(barcode, "0") == 0) {
				barcode = TextUtils.substring(barcode, 1, barcode.length());
			}
			return TextUtils.substring(barcode, 0, barcode.length() - 1);
		} else {
			return "";
		}
	}

	@Override
	public Articulo processArticulo(String barcode, Articulo articulo) {
		// TODO Auto-generated method stub
		if (articulo != null) {
			articulo.setMontoTotal(articulo.getPrecio() * 1);
		}
			
		return articulo;
	}

}
