package addcel.chedrauipos.util;

/**
 * Interfaz que especifica métodos para transferencia de archivos
 * @author carlosgs
 *	
 */
public interface FileTransferClient {
	
	/**
	 * Abre conexión con servidor.
	 * @return {@code true} si la conexión con servidor fue exitosa.
	 */
	public boolean connect();

	/**
	 * Descarga archivo desde servidor
	 * @param myPath Ruta en dispositivo donde se depositará archivo
	 * @param serverPath Ruta en servidor desde donde se descarga archivo.
	 * @return 0 si la descarga es exitosa
	 */
	public int getFile(String myPath, String serverPath);

	/**
	 * Deposita archivo en servidor.
	 * @param myPath Ruta en dispositivo donde se encuentra archivo
	 * @param serverPath Ruta en servidor donde se deposita archivo.
	 * @return 0 si el deposito es exitoso.
	 */
	public int sendFile(String myPath, String serverPath);

	/**
	 * Cierra la conexión con servidor.
	 * @return {@code true} si el proceso de desconexión es exitoso.
	 */
	public boolean disconnect();
}
