package addcel.chedrauipos.util;

import java.util.EnumMap;
import java.util.Map;

import android.graphics.Bitmap;
import android.util.Log;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

public final class BarcodeUtil {
	
	private static final int TRANSPARENT = 0xFFFFFFFF;
	private static final int BLACK = 0xFF000000;
	
	private static final String TAG = BarcodeUtil.class.getName();
	
	private BarcodeUtil() {
		
	}

	private static Bitmap encodeAsBitmap(
			String contents, BarcodeFormat format, int img_width, int img_height)
					throws WriterException {
		String contentsToEncode = contents;
		
		if (contentsToEncode == null) {
		    return null; 
		} 
		
		Map<EncodeHintType, Object> hints = null;
		String encoding = guessAppropriateEncoding(contentsToEncode);
		
		if (encoding != null) {
		    hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
		    hints.put(EncodeHintType.CHARACTER_SET, encoding);
		} 
		
		MultiFormatWriter writer = new MultiFormatWriter();
		BitMatrix result;
		
		try { 
		    result = writer.encode(
		    		contentsToEncode, format, img_width, img_height, hints);
		} catch (IllegalArgumentException iae) {
		    // Unsupported format 
		        return null; 
		} 
		
	    int width = result.getWidth();
	    int height = result.getHeight();
	    int[] pixels = new int[width * height];
	    
	    for (int y = 0; y < height; y++) {
	        int offset = y * width;
	        
	        for (int x = 0; x < width; x++) {
	        	pixels[offset + x] = result.get(x, y) ? BLACK : TRANSPARENT;
	        } 
	    } 
		 
	    Bitmap bitmap = Bitmap.createBitmap(
	    		width, height, Bitmap.Config.ARGB_8888);
		bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
		
		return bitmap;
    } 
		 
		    
	public static String guessAppropriateEncoding(CharSequence contents) {
		    // Very crude at the moment 
		for (int i = 0; i < contents.length(); i++) {
			
		    if (contents.charAt(i) > 0xFF) {
		    	return "UTF-8"; 
		    } 
		} 
		
		return null; 
	} 
	
	public static Bitmap generateBarcode(String _barcode,
			BarcodeFormat _f, int _width) {
		// TODO Auto-generated method stub
		Log.d(TAG, "Intentando generar barcode para String: " + _barcode);
	    try { 
	    	 
	        Bitmap bitmap = encodeAsBitmap(_barcode, _f, _width, _width / 2);
	        Log.d(TAG, "Barcode generado exitosamente");
	        return bitmap;
	 
	    } catch (WriterException e) {
	        Log.e(TAG, "WriterException", e);
	        Log.d(TAG, "Falló generación del barcode para String: " + _barcode);
	        return null;
	    } catch (Exception e) {
	    	Log.e(TAG, "Excepción general", e);
	        Log.d(TAG, "Falló generación del barcode para String: " + _barcode);
	        return null;
	    }
	 
	}
}
