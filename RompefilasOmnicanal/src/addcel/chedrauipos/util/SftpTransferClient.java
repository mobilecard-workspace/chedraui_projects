package addcel.chedrauipos.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import android.util.Log;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

/**
 * Implementación de {@code FileTransferClient} orientada a SFTP
 * @author carlosgs
 *
 */
public class SftpTransferClient implements FileTransferClient {

	private JSch channel;
	private ChannelSftp sftpCh;
	private Session session;
	private String host;
	private String login;
	private String password;
	private int port;
	
	private static final String TAG = SftpTransferClient.class.getName();
	private static SftpTransferClient instance;
	
	private SftpTransferClient() {
		// TODO Auto-generated constructor stub
		channel = new JSch();
	}
	
	private SftpTransferClient(String host, String login, String pass) {
		this();
		this.host = host;
		this.login = login;
		this.password = pass;
		this.port = 22;
	}
	
	public static synchronized SftpTransferClient get(String host, String login, String pass) {
		if (instance == null)
			instance = new SftpTransferClient();
		
		instance.host = host;
		instance.login = login;
		instance.password = pass;
		instance.port = 22;
		
		return instance;
	}
	
	@Override
	public boolean connect() {
		// TODO Auto-generated method stub
		try {
			session = channel.getSession(login, host, port);
			session.setPassword(password);
			session.setConfig("StrictHostKeyChecking", "no");
			session.connect();
			if (session.isConnected()) {
				Log.i(TAG, "Conexión Exitosa");
				sftpCh = (ChannelSftp) session.openChannel("sftp");
				sftpCh.connect();
				return sftpCh.isConnected();
			} else {
				return false;
			}
			
		} catch (JSchException e) {
			Log.e(TAG, JSchException.class.getSimpleName(), e);
			return false;
		}
	}

	@Override
	public int getFile(String myPath, String serverPath) {
		// TODO Auto-generated method stub
		try {
			File file = new File(myPath);
            FileOutputStream srcFileStream = new FileOutputStream(file);
            sftpCh.cd(serverPath);
            sftpCh.get(file.getName(), srcFileStream);
            srcFileStream.close();
            
            return 0;
		} catch (Exception e) {
            Log.e(TAG, Exception.class.getSimpleName(), e);
			return -10000;
		} finally {
			disconnect();
		}
	}

	@Override
	public int sendFile(String myPath, String serverPath) {
		// TODO Auto-generated method stub
		try {  
        	File file = new File(myPath);
            FileInputStream srcFileStream = new FileInputStream(file);  
            sftpCh.cd(serverPath);
            sftpCh.put(srcFileStream, file.getName());
            srcFileStream.close();
            return 0;
             
        } catch (Exception e) {  
             Log.e(TAG, Exception.class.getSimpleName(), e);
             return -10000;
        } finally {
        	disconnect();
        }
	}

	@Override
	public boolean disconnect() {
		// TODO Auto-generated method stub
		sftpCh.disconnect();
		session.disconnect();
		
		return true;
	}

}
