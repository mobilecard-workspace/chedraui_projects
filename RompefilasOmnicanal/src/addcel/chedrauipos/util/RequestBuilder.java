package addcel.chedrauipos.util;


public final class RequestBuilder {
	
	private static String str;
	private static StringBuilder sb;
	
	private RequestBuilder() {
		
	}
	
	public static synchronized String loginRequest(String login, String password) {
		sb = new StringBuilder();
		
		sb.append("{")
			.append("\"login\":").append("\"").append(login).append("\"").append(",")
			.append("\"password\":").append("\"").append(password).append("\"")
			.append("}");
		
		str = sb.toString();
		
		return str;
	}
	
	public static synchronized String passwordRequest(int idUsuario, CharSequence password,
			CharSequence passwordNuevo) {
		
		sb = new StringBuilder();
		
		sb.append("{")
		.append("\"password\":").append("\"").append(password).append("\"").append(",")
		.append("\"passwordNuevo\":").append("\"").append(passwordNuevo).append("\"").append(",")
		.append("\"idUsuario\":").append(idUsuario)
		.append("}");
	
		str = sb.toString();
	
		return str;
	}
	
	public static synchronized String catalogRequest(int idUsuario) {
		sb = new StringBuilder();
		sb.append("{\"idUsuario\":").append(idUsuario).append("}");
		
		str = sb.toString();
		
		return str;
	}
}
