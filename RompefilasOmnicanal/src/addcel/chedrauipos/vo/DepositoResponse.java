package addcel.chedrauipos.vo;

import addcel.chedrauipos.model.Ticket;

public class DepositoResponse extends DefaultResponse {
	
	private Ticket ticket;
	
	public DepositoResponse() {
		super();
		setIdError(-10000);
		setMensajeError("Error en depósito");
	}
	
	public Ticket getTicket() {
		return ticket;
	}
	
	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

}
