package addcel.chedrauipos.vo;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class Tienda {
	private int idTienda;
	private String descripcion;
	private int status;
	private int posMobile;
	private int rompeFilas;
	private String codigo;
	@SerializedName("urlFTP")
	private String urlFtp;
	@SerializedName("userFTP")
	private String userFtp;
	@SerializedName("passFTP")
	private String passFtp;

	public int getIdTienda() {
		return this.idTienda;
	}

	public void setIdTienda(int idTienda) {
		this.idTienda = idTienda;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getPosMobile() {
		return this.posMobile;
	}

	public void setPosMobile(int posMobile) {
		this.posMobile = posMobile;
	}

	public int getRompeFilas() {
		return this.rompeFilas;
	}

	public void setRompeFilas(int rompeFilas) {
		this.rompeFilas = rompeFilas;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getUrlFtp() {
		return this.urlFtp;
	}

	public void setUrlFtp(String urlFtp) {
		this.urlFtp = urlFtp;
	}

	public String getUserFTP() {
		return this.userFtp;
	}

	public void setUserFtp(String userFtp) {
		this.userFtp = userFtp;
	}

	public String getPassFtp() {
		return this.passFtp;
	}

	public void setPassFtp(String passFtp) {
		this.passFtp = passFtp;
	}
	
	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		if (o instanceof Tienda) {
			
			Tienda comp = (Tienda) o;
			
			if (idTienda == comp.getIdTienda() && 
					TextUtils.equals(codigo, comp.getCodigo())) {
				return true;
			} else {
				return false;
			}
			
		} else {
			return false;
		}
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.codigo + " - " + this.descripcion;
	}
}
