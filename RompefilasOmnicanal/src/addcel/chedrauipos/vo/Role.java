package addcel.chedrauipos.vo;

public class Role {

	private int idRol;
	private String descripcion;
	
	public Role() {
		
	}
	
	public Role(int idRol, String descripcion) {
		this.idRol = idRol;
		this.descripcion = descripcion;
	}

	public int getIdRol() {
		return this.idRol;
	}

	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		if (o instanceof Role) {
			
			Role comp = (Role) o;
			
			if (idRol == comp.getIdRol())
				return true;
			else
				return false;
		}
		
		return false;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.descripcion;
	}
}
