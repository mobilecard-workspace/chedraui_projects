package addcel.chedrauipos.vo;

import java.util.List;


public class TiendasResponse extends DefaultResponse {
	
	private List<Tienda> tiendas;
	
	public List<Tienda> getTiendas() {
		return tiendas;
	}
	
	public void setTiendas(List<Tienda> tiendas) {
		this.tiendas = tiendas;
	}
}
