package addcel.chedrauipos.vo;

import java.util.List;

public class RolesResponse extends DefaultResponse {
	
	private List<Role> roles;
	
	public List<Role> getRoles() {
		return roles;
	}
	
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

}
