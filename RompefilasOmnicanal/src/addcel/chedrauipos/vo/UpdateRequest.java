package addcel.chedrauipos.vo;

public class UpdateRequest {

	private int idUsuario;
	private int idSupervisor;
	private String login;
	private String password;
	private String nombre;
	private String apellido;
	private String materno;
	private String mail;
	private int idTienda;
	private int idRol;

	public UpdateRequest() {
		// TODO Auto-generated constructor stub
	}

	public UpdateRequest(LoginResponse resp) {
		idUsuario = resp.getIdUsuario();
		idSupervisor = resp.getIdSupervisor();
		login = resp.getLogin();
		password = resp.getPassword();
		nombre = resp.getNombre();
		apellido = resp.getApellido();
		materno = resp.getMaterno();
		mail = resp.getMail();
		idTienda = resp.getTienda().getIdTienda();
		idRol = resp.getRole().getIdRol();
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public int getIdSupervisor() {
		return idSupervisor;
	}

	public void setIdSupervisor(int idSupervisor) {
		this.idSupervisor = idSupervisor;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getMaterno() {
		return materno;
	}

	public void setMaterno(String materno) {
		this.materno = materno;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public int getIdTienda() {
		return idTienda;
	}

	public void setIdTienda(int idTienda) {
		this.idTienda = idTienda;
	}

	public int getIdRol() {
		return idRol;
	}

	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}

}
