package addcel.chedrauipos.model;

import java.util.List;

import org.joda.time.DateTime;

import addcel.util.AddcelTextUtil;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

@Table(name = "ticket")
public class Ticket extends Model {

	@Column(name = "consecutivo")
	private String consecutivo;
	@Column(name = "barcode")
	private String barcode;
	@Column(name = "file_string")
	private String fileString;
	@Column(name = "fecha")
	private long fecha;
	@Column(name = "procesado")
	private boolean procesado;
	@Column(name = "total")
	private double total;

	public Ticket() {
		super();
	}

	public String getConsecutivo() {
		return consecutivo;
	}

	public String getBarcode() {
		return barcode;
	}

	public String getFileString() {
		return fileString;
	}

	public long getFecha() {
		return fecha;
	}

	public boolean isProcesado() {
		return procesado;
	}

	public double getTotal() {
		return total;
	}

	public void setConsecutivo(String consecutivo) {
		this.consecutivo = consecutivo;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public void setFileString(String fileString) {
		this.fileString = fileString;
	}

	public void setFecha(long fecha) {
		this.fecha = fecha;
	}

	public void setProcesado(boolean procesado) {
		this.procesado = procesado;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public static synchronized List<Ticket> getAll() {
		return new Select().from(Ticket.class).execute();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		
		sb.append("Total: ").append(AddcelTextUtil.formatCurrency(total))
			.append("\nFecha: ").append(new DateTime(fecha).toString("dd/MM/yyyy HH:mm:ss"));

		return sb.toString();
	}

}
