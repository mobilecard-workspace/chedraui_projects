package addcel.chedrauipos.model;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;

import addcel.gson.GsonFactory;
import addcel.util.AddcelTextUtil;
import android.text.TextUtils;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;

@Table(name = "articulo")
public class Articulo extends Model {

	@Column(index = true, unique = true)
	private long upc;
	@Column(name = "tienda")
	private String tienda;
	@Column(name = "sku")
	private String sku;
	@Column(name = "unidad")
	private String unidad;
	@Column(name = "descripcion")
	private String descripcion;
	@Column(name = "proveedor")
	private String proveedor;
	@Column(name = "marca")
	private String marca;
	@Column(name = "grupo")
	private String grupo;
	@Column(name = "flat")
	private String flat;
	@Column(name = "precio")
	private double precio;
	@Column(name = "iva")
	private String iva;
	@Column(name = "ieps")
	private String ieps;
	@Column(name = "promo")
	private String promo;
	@Column(name = "descuento")
	private double descuento;

	/**
	 * ATRIBUTOS AUXILIARES PARA PROCESAR ARTICULOS PESABLES, NO SON CAMPOS DE
	 * LA TABLA
	 */
	private double peso;
	private double montoTotal;
	
	/**
	 * ATRIBUTOS AUXILIARES PARA PROCESAR ARTÍCULOS OMNICANAL, NO SON CAMPOS DE 
	 * LA TABLA
	 */
	protected boolean seleccionado;
	protected boolean omnicanal;

	private static String UPD_STR = "articulo.tienda = ?, articulo.sku = ?, articulo.unidad = ?,"
									+ " articulo.descripcion = ?, articulo.proveedor = ?, articulo.marca = ?,"
									+ " articulo.grupo = ?, articulo.flat = ?, articulo.precio = ?,"
									+ " articulo.iva = ?, articulo.ieps = ?, articulo.promo = ?,"
									+ " articulo.descuento = ?";

	public Articulo() {
		super();
	}

	public long getUpc() {
		return upc;
	}

	public void setUpc(long upc) {
		this.upc = upc;
	}

	public String getTienda() {
		return tienda;
	}

	public void setTienda(String tienda) {
		this.tienda = tienda;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public String getFlat() {
		return flat;
	}

	public void setFlat(String flat) {
		this.flat = flat;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String getIva() {
		return iva;
	}

	public void setIva(String iva) {
		this.iva = iva;
	}

	public String getIeps() {
		return ieps;
	}

	public void setIeps(String ieps) {
		this.ieps = ieps;
	}

	public String getPromo() {
		return promo;
	}

	public void setPromo(String promo) {
		this.promo = promo;
	}

	public double getDescuento() {
		return descuento;
	}

	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(double montoTotal) {
		this.montoTotal = montoTotal;
	}
	
	public boolean isOmnicanal() {
		return omnicanal;
	}
	
	public void setOmnicanal(boolean omnicanal) {
		this.omnicanal = omnicanal;
	}
	
	public boolean isSeleccionado() {
		return seleccionado;
	}
	
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		sb.append(descripcion).append("\n").append(precio);

		return sb.toString();
	}

	public static synchronized double getTotal(List<Articulo> prodList) {
		double total = 0.0;
		for (Articulo producto : prodList)
			total += (producto.getMontoTotal());

		return total;
	}

	public static synchronized JSONArray buildJSONArray(List<Articulo> articulos) {

		JSONArray array = new JSONArray();

		for (Articulo articulo : articulos) {
			array.put(GsonFactory.get().toJson(articulo));
		}

		return array;

	}

	public static synchronized String getTotalString(List<Articulo> prodList) {
		return AddcelTextUtil.formatCurrency(getTotal(prodList)).replace("$", "");
	}
	
	public static synchronized String getOmnicanalTotalString(List<Articulo> prodList, 
			double shippingAmount) {
		double total = getTotal(prodList) + shippingAmount;
		return AddcelTextUtil.formatCurrency(total).replace("$", "");
	}
	
	public static String getFileString(List<Articulo> prodList) {
		Date hoy = Calendar.getInstance().getTime();
		
		DecimalFormat df = new DecimalFormat("0.00");
		DecimalFormat df1 = new DecimalFormat("0.000");
		
		StringBuffer sb = new StringBuffer();
		
		for (Articulo articulo : prodList) {
			
			//Departamento
			sb.append(articulo.getGrupo() + ";");
			//Código de barras a 12 digitos
			if(Long.toString(articulo.getUpc()).length() < 12) {
				sb.append(StringUtils.leftPad(Long.toString(articulo.getUpc()), 12, "0") + ";");
			} else {
				sb.append(articulo.getUpc() + ";");
			}
			//Tipo de producto unitario = 0
			if (TextUtils.equals(articulo.getUnidad(), "ST"))
				sb.append("0;");
			else
				sb.append("1;");
			
			String precio = df.format(Double.valueOf(articulo.getPrecio()));
			precio = precio.replace(".", ""); //PRECIO SIN PUNTO DECIMAL
			sb.append(precio + ";");
			if (TextUtils.equals(articulo.getUnidad(), "ST")) {
				int cantidad = 1; //AGREGAR ATRIBUTO CANTIDAD
				sb.append(cantidad * 1000 + ";");
			} else {
				String pesoStr = df1.format(Double.valueOf(articulo.getPeso())); //FORMATEAR PESO A 0.000
				pesoStr = pesoStr.replace(".", ""); //ELIMINAR PUNTO DECIMAL
				sb.append(pesoStr + ";");
			}
			
			String subTotal = df.format(Double.valueOf(articulo.getMontoTotal()));
			subTotal = subTotal.replace(".", "");
			sb.append(subTotal + ";");
			
			SimpleDateFormat sf1 = new SimpleDateFormat("hh:mm:ss", Locale.getDefault());
			sb.append(sf1.format(new Date()) + ";");
			
			SimpleDateFormat sf2 = new SimpleDateFormat("dd/MM/yy", Locale.getDefault());
			sb.append(sf2.format(hoy) + "\r\n");
		}
		
		return sb.toString();
		
	}

	// QUERIES

	/**
	 * Query para obtener un artículo en específico.
	 * 
	 * @param upc {@code long} a buscar en base de datos
	 * @return {@code Articulo} asociado al upc
	 */
	public static synchronized Articulo getArticulo(long upc) {
		return new Select().from(Articulo.class).where("articulo.upc = ?", upc)
				.executeSingle();
	}

	public static synchronized void updateArticulo(long upc, String... line) {

		String tienda = line[1];
		String sku = line[2];
		String unidad = line[3];
		String descripcion = line[4];
		String proveedor = line[5];
		String marca = line[6];
		String grupo = line[7];
		String flat = line[8];
		double precio = Double.parseDouble(line[9]);
		String iva = line[10];
		String ieps = line[11];
		String promo = line[12];
		double descuento = Double.parseDouble(line[13]);

		new Update(Articulo.class)
				.set(UPD_STR, tienda, sku, unidad, descripcion, proveedor,
						marca, grupo, flat, precio, iva, ieps, promo, descuento)
				.where("articulo.upc = ?", upc).execute();
	}

	public static synchronized void deleteAll() throws Exception {
		new Delete().from(Articulo.class).execute();
	}

}
