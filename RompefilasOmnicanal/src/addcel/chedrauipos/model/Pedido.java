package addcel.chedrauipos.model;

import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.squareup.phrase.Phrase;

@Table(name = "pedido")
public class Pedido extends Model {

	@Column(index = true, unique = true)
	private String numPedido;
	@Column(name = "tienda")
	private String tienda;
	@Column(name = "status")
	private String status;

	public Pedido() {
		super();
	}

	public String getNumPedido() {
		return numPedido;
	}

	public void setNumPedido(String numPedido) {
		this.numPedido = numPedido;
	}

	public String getTienda() {
		return tienda;
	}

	public void setTienda(String tienda) {
		this.tienda = tienda;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return Phrase.from("Id: {id} \nSucursal: {tienda} \nStatus: {status}")
				.put("id", numPedido).put("tienda", tienda)
				.put("status", status).format().toString();
	}
	
	public static synchronized Pedido get(String numPedido) {
		return new Select().from(Pedido.class).where("pedido.numPedido = ?", numPedido)
				.executeSingle();
	}
	
	public static synchronized List<Pedido> getAll() {
		return new Select().from(Pedido.class).execute();
	}
	
	public static synchronized void setStatus(String numPedido, String status) {
		new Update(Pedido.class).set("pedido.status ?", status)
								.where("articulo.numPedido = ?", numPedido)
								.execute();;
	}
	
	public static synchronized void deleteAll() throws Exception {
		new Delete().from(Pedido.class).execute();
	}

}
