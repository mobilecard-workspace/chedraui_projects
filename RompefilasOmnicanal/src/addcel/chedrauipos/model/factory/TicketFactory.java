package addcel.chedrauipos.model.factory;

import org.joda.time.DateTime;

import addcel.chedrauipos.model.Ticket;

public final class TicketFactory {
	
	private static Ticket instance;
	private static final String TAG = TicketFactory.class.getSimpleName();
	
	private TicketFactory() {
		
	}
	
	public static synchronized Ticket get(String barcode, String consecutivo, double total) {
		instance = new Ticket();
		
		instance.setBarcode(barcode);
		instance.setConsecutivo(consecutivo);
		instance.setFecha(DateTime.now().getMillis());
		instance.setTotal(total);
		
		return instance;
	}

}
