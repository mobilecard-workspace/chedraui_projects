package addcel.chedrauipos.model.factory;

import addcel.chedrauipos.model.Articulo;
import android.text.TextUtils;
import android.util.Log;

public final class ArticuloFactory {
	
	private static Articulo instance;
	private static final String TAG = ArticuloFactory.class.getName();
	
	private ArticuloFactory() {
		
	}
	
	public static synchronized Articulo get(String... line) {
		instance = new Articulo();
		
		String upc = TextUtils.isEmpty(line[0])? "No Info" : line[0];
		String tienda = TextUtils.isEmpty(line[1])? "No Info" : line[1];
		String articulo = TextUtils.isEmpty(line[2])? "No Info" : line[2];
		String um = TextUtils.isEmpty(line[3])? "No Info" : line[3];
		String descripcion = TextUtils.isEmpty(line[4])? "No Info" : line[4];
		String proveedor = TextUtils.isEmpty(line[5])? "No Info" : line[5];
		String marca = TextUtils.isEmpty(line[6])? "No Info" : line[6];
		String departamento = TextUtils.isEmpty(line[7])? "No Info" : line[7];
		String pesable = TextUtils.isEmpty(line[8])? "No Info" : line[8];
		String precio = TextUtils.isEmpty(line[9])? "No Info" : line[9];
		String iva = TextUtils.isEmpty(line[10])? "No Info" : line[10];
		String ieps = TextUtils.isEmpty(line[11])? "No Info" : line[11];
		String promo = TextUtils.isEmpty(line[12])? "No Info" : line[12];
		String descuento = TextUtils.isEmpty(line[13])? "No Info" : line[13];
		
		try {
			
			long upcL = Long.parseLong(upc);
			double precioD = Double.parseDouble(precio);
			double descuentoD = Double.parseDouble(descuento);
			
			instance.setUpc(upcL);
			instance.setTienda(tienda);
			instance.setSku(articulo);
			instance.setUnidad(um);
			instance.setDescripcion(descripcion);
			instance.setProveedor(proveedor);
			instance.setMarca(marca);
			instance.setGrupo(departamento);
			instance.setFlat(pesable);
			instance.setPrecio(precioD);
			instance.setIva(iva);
			instance.setIeps(ieps);
			instance.setPromo(promo);
			instance.setDescuento(descuentoD);

			return instance;
			
		} catch (NumberFormatException e) {
			Log.e(TAG, NumberFormatException.class.getName(), e);
		} catch (Exception e1) {
			Log.e(TAG, Exception.class.getName(), e1);
		}
		
		return null;
	}

}
