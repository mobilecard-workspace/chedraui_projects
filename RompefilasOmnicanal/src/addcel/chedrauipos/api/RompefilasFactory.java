package addcel.chedrauipos.api;

import org.addcel.android.crypto.AddcelCrypto;
import org.addcel.android.crypto.Cifrados;

import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.http.Body;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;
import addcel.gson.GsonFactory;
import android.util.Log;

public final class RompefilasFactory {
	
	private static Rompefilas instance;
	
	private RompefilasFactory() {
		// TODO Auto-generated constructor stub
	}
	
	public static synchronized Rompefilas get() {
		if (instance == null) {
			
			RestAdapter.Log log = new RestAdapter.Log() {
				
				@Override
				public void log(String arg0) {
					// TODO Auto-generated method stub
					Log.d(Rompefilas.class.getSimpleName(), arg0);
				}
			};
			
			RestAdapter adapter = new RestAdapter.Builder().setLog(log)
					.setLogLevel(LogLevel.FULL).setEndpoint("http://50.57.192.214:8080").build();
			
			instance = adapter.create(Rompefilas.class);
		}
		
		return instance;
	}
	
	
	private static synchronized String translate(TypedInput body, Cifrados cifrado) {
		
		String resp = new String(((TypedByteArray) body).getBytes());
		
		switch (cifrado) {
		case HARD:
			return AddcelCrypto.decryptHard(resp);
			
		case SENSITIVE:
			
			return AddcelCrypto.decryptSensitive(resp);

		default:
			return resp;
		}
	}
	
	public static synchronized String buildRequest(Object o, Cifrados cifrado) {
		String request = GsonFactory.get().toJson(o);
		switch (cifrado) {
		case HARD:
			
			return AddcelCrypto.encryptHard(request);

		case SENSITIVE:
			
			return AddcelCrypto.encryptSensitive(AddcelCrypto.getKey(), request);
			
		default:
			return request;
		}
	}
	
	public static synchronized <T> T buildResponse(TypedInput body, Cifrados cifrado, Class<T> type) 
			throws Exception {
		
		String response = translate(body, cifrado);
		return GsonFactory.get().fromJson(response, type);
	}
	
	

}
