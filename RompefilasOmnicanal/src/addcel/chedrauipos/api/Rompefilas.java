package addcel.chedrauipos.api;

import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

public interface Rompefilas {
	
	@FormUrlEncoded
	@POST("/ChedrauiServicios/getTiendas")
	public Response getTiendas(@Field("json")String request) throws Exception;

	@FormUrlEncoded
	@POST("/ChedrauiServicios/getRoles")
	public Response getRoles(@Field("json") String request) throws Exception;
	
	@FormUrlEncoded
	@POST("/ChedrauiServicios/cambioUsuario")
	public Response updateUsuario(@Field("json") String request) throws Exception;
}
