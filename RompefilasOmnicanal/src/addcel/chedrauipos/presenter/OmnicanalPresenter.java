package addcel.chedrauipos.presenter;

import java.util.ArrayList;
import java.util.List;

import org.addcel.android.chedrauipos.AppStatus;
import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.UserSession;

import com.squareup.phrase.Phrase;

import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import addcel.chedrauipos.model.Articulo;
import addcel.chedrauipos.omnicanal.api.OmnicanalRest;
import addcel.chedrauipos.omnicanal.vo.AbstractVO;
import addcel.chedrauipos.omnicanal.vo.DatosTransaccion;
import addcel.chedrauipos.omnicanal.vo.PedidoResponse;
import addcel.chedrauipos.omnicanal.vo.RespuestaConsultarPedidoItem;
import addcel.chedrauipos.ui.callback.OmnicanalCallback;
import addcel.chedrauipos.util.BarcodeProcessor;
import addcel.chedrauipos.util.BarcodeProcessorFactory;
import addcel.chedrauipos.util.BarcodeProcessorFactory.ProcessorType;
import addcel.chedrauipos.util.FileGenerationUtil;
import addcel.chedrauipos.util.FileTransferClient;
import addcel.chedrauipos.util.FtpTransferClient;
import addcel.util.ErrorUtil;
import addcel.util.ProgressDialogFactory;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class OmnicanalPresenter {
	
	private Context context;
	private PedidoResponse pedido;
	private List<RespuestaConsultarPedidoItem> itemsPedido;
	private List<String> itemsAEliminar;
	private OmnicanalCallback callback;
	private ProgressDialog pDialog;
	private FileTransferClient transferClient;
	private BarcodeProcessor processor;
	private int statusCantidad[];
	private Builder alertBuilder;
	private AlertDialog cancelacionParcialDialog, cancelarDialog;
	
	private static OmnicanalRest restClient;
	private static final String TAG = OmnicanalPresenter.class.getSimpleName();
	
	public OmnicanalPresenter(Context context, OmnicanalCallback callback) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.pedido = null;
		this.itemsPedido = new ArrayList<RespuestaConsultarPedidoItem>();
		this.itemsAEliminar = new ArrayList<String>();
		this.callback = callback;
		this.transferClient = 
				FtpTransferClient.getQA();
//				FtpTransferClient.get(UserSession.get().getLogin().getTienda().getUrlFtp(), 
//													UserSession.get().getLogin().getTienda().getUserFTP(),
//													UserSession.get().getLogin().getTienda().getPassFtp());
		
		this.pDialog = ProgressDialogFactory.get(this.context, R.string.msg_pdialog);
	}
	
	public PedidoResponse getPedido() {
		return pedido;
	}
	
	public void setPedido(PedidoResponse pedido) {
		this.pedido = pedido;
	}
	
	public List<RespuestaConsultarPedidoItem> getItemsPedido() {
		return itemsPedido;
	}
	
	public void setItemsPedido(List<RespuestaConsultarPedidoItem> itemsPedido) {
		if (!this.itemsPedido.isEmpty())
			this.itemsPedido.clear();
			
		this.itemsPedido.addAll(itemsPedido);
	}
	
	public List<String> getItemsAEliminar() {
		return itemsAEliminar;
	}
	
	public void setItemsAEliminar(List<String> itemsAEliminar) {
		this.itemsAEliminar = itemsAEliminar;
	}
	
	/**
	 * 
	 * @return {@code int[]} que contiene la cantidad 
	 * de busquedas exitosas sobre cada item del pedido.
	 */
	public int[] getStatusCantidad() {
		return statusCantidad;
	}
	
	/**
	 * Asigna un valor a la variable {@code statusCantidad}
	 * @param statusCantidad
	 */
	public void setStatusCantidad(int[] statusCantidad) {
		this.statusCantidad = statusCantidad;
	}
	

	public CharSequence buildMensajeCantidad(int cantidad, RespuestaConsultarPedidoItem item) {
		CharSequence mensaje = null;
		int faltante = item.getTotalArticulos().intValue() - cantidad;
		
		if (faltante == 1)
			mensaje = Phrase.from(context, R.string.txt_articulo_faltante)
				.put("sku", item.getSku()).format();
		else
			mensaje = Phrase.from(context, R.string.txt_articulos_faltantes)
			.put("cantidad", faltante)
			.put("sku", item.getSku()).format();
				
		return mensaje;
	}
	
	public AlertDialog buildPedidoDialog() {
		
		final View view = LayoutInflater.from(context).inflate(
				R.layout.view_pedido, null);
		
		alertBuilder = new Builder(context)
		.setMessage(R.string.txt_msg_pedido)
		.setView(view)
		.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				EditText text = (EditText) view.findViewById(R.id.text_pedido);
				if (!TextUtils.isEmpty(text.getText()) && text.length() <= 12) {
					String pedido = text.getText().toString().trim();
					text.setText("");
					
					DatosTransaccion request = DatosTransaccion.get
							(100, pedido,
									AppStatus.get(context).getCodigoForOmnicanal());
					
					lanzarOperacion(request);
				} else {
					text.setText("");
					Toast.makeText(context,
							ErrorUtil.noValido(context, R.string.txt_hint_pedido),
							Toast.LENGTH_SHORT).show();
				}
			}
		})
		.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				EditText text = (EditText) view.findViewById(R.id.text_pedido);
				text.setText("");
				dialog.cancel();
			}
		});
		
		return alertBuilder.create();
	}
	
	public AlertDialog getCancelarDialog() {
		
		if (cancelarDialog == null) {
		
			alertBuilder = new Builder(context)
			.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					DatosTransaccion d = DatosTransaccion.get(600,
							pedido.getPedido().getNumeroPedido(),
							AppStatus.get(context).getCodigo());
	//				DESARROLLO
					callback.onStatusCancelado(AbstractVO.getDummySuccess());
					
	//				PRODUCCIÓN
//					lanzarOperacion(d);
				}
			})
			.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.cancel();
				}
			});
		
			cancelarDialog = alertBuilder.create();
		}
		
		return cancelarDialog;
	}
	
	public AlertDialog getCancelarParcialDialog() {
		
		if (cancelacionParcialDialog == null) {
			
			alertBuilder = new Builder(context)
			.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					DatosTransaccion d = DatosTransaccion.get(107,
							pedido.getPedido().getNumeroPedido(),
							AppStatus.get(context).getCodigo(),
							itemsAEliminar);
					
	//				DESARROLLO
					callback.onCancelacionParcial(AbstractVO.getDummySuccess());
					
	//				PRODUCCIÓN
//					lanzarOperacion(d);
				}
			})
			.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					itemsAEliminar.clear();
					alterDeleteSize(itemsAEliminar.size());
					dialog.cancel();
				}
			});
			
			cancelacionParcialDialog = alertBuilder.create();
		}
		
		CharSequence msg = Phrase.from(
				context, R.string.txt_cancel_dialog_msg)
				.put("articulos", itemsAEliminar.toString())
				.format();
		
		cancelacionParcialDialog.setMessage(msg);
		
		return cancelacionParcialDialog;
	}
	
	public AlertDialog buildTipoCancelacionDialog() {
		
		
		alertBuilder = new Builder(context)
		.setMessage(R.string.txt_cancelacion_tipo_msg)
		.setPositiveButton(R.string.txt_cancelacion_parcial, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				getCancelarParcialDialog().show();
			}
		})
		.setNegativeButton(R.string.txt_cancelacion_total, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				getCancelarDialog().show();
			}
		})
		.setOnCancelListener(new DialogInterface.OnCancelListener() {
			
			@Override
			public void onCancel(DialogInterface dialog) {
				// TODO Auto-generated method stub
				dialog.cancel();
				itemsAEliminar.clear();
				alterDeleteSize(itemsAEliminar.size());
			}
		});
		
		return alertBuilder.create();
	}
	
	public void alterDeleteSize(int newSize) {
		callback.onDeleteListModified(newSize);
	}
	
	public void lanzarOperacion(DatosTransaccion data) {
		switch (data.getIndicador()) {
		case 100:
			consultarPedidoAsync(data);
			break;
		case 300:
		case 600:
			cambiarStatusPedidoAsync(data);
			break;
		case 700:
			cancelacionParcialPedidoAsync(data);
			break;

		default:
			break;
		}
	}
	
	private int depositarArchivo() {
		
		int result = -404;
		
		if (pedido != null && itemsPedido.size() > 0) {
			
			String archivo = FileGenerationUtil.get().generaArchivoOC(context, pedido);
		
			if (transferClient.connect()) {
				result = transferClient.sendFile(context.getFilesDir() + "/" + archivo,
						"/home/chedraui/version/" + archivo);
				
//				result = transferClient.sendFile(context.getFilesDir() + "/" + archivo,
//						"C:/OMNI/" + archivo);
			}
		}
		
		return result;
	}
	
	public void depositarArchivoAsync() {
		
		new AsyncTask<Void, Void, Integer>() {
			
			protected void onPreExecute() {
				pDialog.show();
			};

			@Override
			protected Integer doInBackground(Void... params) {
				// TODO Auto-generated method stub
				return depositarArchivo();
			}
			
			protected void onPostExecute(Integer result) {
				pDialog.dismiss();
				callback.onArchivoDepositado(result);
			};
			
		}.execute();
	}
	
	public void consultarPedidoAsync(final DatosTransaccion pedido) {
		
		new AsyncTask<Void, Void, PedidoResponse>() {
			
			protected void onPreExecute() {
				pDialog.show();
			};

			@Override
			protected PedidoResponse doInBackground(Void... params) {
				// TODO Auto-generated method stub
				try {
					return getRestClient().getPedido(pedido);
				} catch (Exception e) {
					Log.e(TAG, "consultarPedidoAsync", e);
					return null;
				}
			}
			
			protected void onPostExecute(PedidoResponse result) {
				pDialog.dismiss();
				callback.onPedidoConsultado(result);
			};
			
		}.execute();
	}
	
	private long[] consultarUpcsPedido() {
		long[] upcs = new long[itemsPedido.size()];
		
		for (int i = 0; i < itemsPedido.size(); i++) {

			Articulo articulo = consultarArticulo(itemsPedido.get(i).getSku(), true);
			
			if (articulo != null) {
				upcs[i] = articulo.getUpc();
				String descripcion = ! TextUtils.isEmpty(articulo.getDescripcion()) 
										? articulo.getDescripcion() : "";
				itemsPedido.get(i).setDescripcion(descripcion);
			}
		}
		
		return upcs;
	}
	
	public void consultarUpcsPedidoAsync() {
		
		new AsyncTask<Void, Void, long[]>() {
			
			protected void onPreExecute() {
				pDialog.show();
			};

			@Override
			protected long[] doInBackground(Void... params) {
				// TODO Auto-generated method stub
				return consultarUpcsPedido();
			}
			
			protected void onPostExecute(long[] result) {
				pDialog.dismiss();
				callback.onUpcsConsultados(result);
			};
			
		}.execute();
	}
	
	public String processBarcode(String upc) {
		Log.d(TAG, "UPC antes de procesar: " + upc);
		int len = upc.length();
		Log.d(TAG, "Longitud UPC: " + len);
		switch (len) {
		case 18:
			if (TextUtils.indexOf(upc, "23") == 0 || TextUtils.indexOf(upc, "25") == 0) {
				processor = BarcodeProcessorFactory.get(ProcessorType.PESABLE);
			} else {
				processor = BarcodeProcessorFactory.get(ProcessorType.DEFAULT);
			}
			break;

		default:
			if (TextUtils.indexOf(upc, "99999") == 0) {
				processor = BarcodeProcessorFactory.get(ProcessorType.NO_PROCESS);
			} else {
				processor = BarcodeProcessorFactory.get(ProcessorType.DEFAULT);
			}
			break;
		}
		
		return processor.getUpc(upc);
	}
	
	private Articulo consultarArticulo(String upcItemPedido, boolean esItemOmnicanal) {
		Articulo a = null;
		try {
			Log.d(TAG, "Buscando: " + upcItemPedido);
			if (!esItemOmnicanal) {
				upcItemPedido = processBarcode(upcItemPedido);
				Log.d(TAG, "UPC procesado a: " + upcItemPedido);
			}
			
			long upc = Long.parseLong(upcItemPedido);
			a = Articulo.getArticulo(upc);
		} catch (NumberFormatException e) {
			Log.e(TAG, "consultarArticulo", e);
		}
		
		return a;
	}
	
	public void consultarArticuloAsync(final String upcItemPedido, final boolean esItemOmnicanal) {
		if (pedido != null) {
			new AsyncTask<Void, Void, Articulo>() {
				
				protected void onPreExecute() {
					pDialog.show();
				};
	
				@Override
				protected Articulo doInBackground(Void... params) {
					// TODO Auto-generated method stub
					return consultarArticulo(upcItemPedido, esItemOmnicanal);
				}
				
				protected void onPostExecute(Articulo result) {
					pDialog.dismiss();
					callback.onArticuloConsultado(result);
				};
			}.execute();
		} else {
			Toast.makeText(context, R.string.error_pedido_nulo, Toast.LENGTH_SHORT).show();
		}
	}
	
	public void cambiarStatusPedidoAsync(final DatosTransaccion datos) {
		new AsyncTask<Void, Void, AbstractVO>() {
			
			protected void onPreExecute() {
				pDialog.show();
			};

			@Override
			protected AbstractVO doInBackground(Void... params) {
				// TODO Auto-generated method stub
				try {
					return getRestClient().setStatus(datos);
				} catch (Exception e) {
					return null;
				}
			}
			
			protected void onPostExecute(AbstractVO result) {
				pDialog.dismiss();
				callback.onStatusModificado(result);
			};
			
		}.execute();
	}
	
	public void cancelarPedidoAsync(final DatosTransaccion datos) {
		new AsyncTask<Void, Void, AbstractVO>() {
			
			protected void onPreExecute() {
				pDialog.show();
			};

			@Override
			protected AbstractVO doInBackground(Void... params) {
				// TODO Auto-generated method stub
				try {
					return getRestClient().setStatus(datos);
				} catch (Exception e) {
					return null;
				}
			}
			
			protected void onPostExecute(AbstractVO result) {
				pDialog.dismiss();
				callback.onStatusModificado(result);
			};
			
		}.execute();
	}
	
	public void addItemToDeleteList(int position) {
		RespuestaConsultarPedidoItem item = itemsPedido.get(position);
		callback.onItemAddedToDeleteList(item);
		
	}
	
	public void cancelacionParcialPedidoAsync(final DatosTransaccion datos) {
		new AsyncTask<Void, Void, AbstractVO>() {
			
			protected void onPreExecute() {
				pDialog.show();
			};

			@Override
			protected AbstractVO doInBackground(Void... params) {
				// TODO Auto-generated method stub
				try {
					return getRestClient().partialCancelPedido(datos);
				} catch (Exception e) {
					return null;
				}
			}
			
			protected void onPostExecute(AbstractVO result) {
				pDialog.dismiss();
				callback.onCancelacionParcial(result);;
			};
			
		}.execute();
	}
	
	public static synchronized OmnicanalRest getRestClient() {
		if (restClient == null) {
			
			RestAdapter.Log log = new RestAdapter.Log() {
				
				@Override
				public void log(String arg0) {
					// TODO Auto-generated method stub
					Log.d(OmnicanalRest.class.getSimpleName(), arg0);
				}
			};
			
			RestAdapter adapter = new RestAdapter.Builder().setLog(log)
					.setLogLevel(LogLevel.FULL).setEndpoint(OmnicanalRest.DEV).build();
			
			restClient = adapter.create(OmnicanalRest.class);
		}
		
		return restClient;
	}
	
	private int printReceipt() {
		return -1;
	}
	
	public void printReceiptAsync() {
		new AsyncTask<Void, Void, Integer>() {
			
			@Override
			protected void onPreExecute() {
				pDialog.show();
			};
			
			@Override
			protected Integer doInBackground(Void... params) {
				return printReceipt();
			};
			
			@Override
			protected void onPostExecute(Integer result) {
				pDialog.dismiss();
				callback.onTicketImpreso(result);
			};
			
		}.execute();
	}
	

}
