package addcel.chedrauipos.presenter;

import java.util.List;

import org.addcel.android.chedrauipos.UserSession;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.widget.Spinner;

public class PrinterConfPresenter {
	
	private Context context;
	private List<BluetoothDevice> devices;
	
	public PrinterConfPresenter(Context context, List<BluetoothDevice> devices) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.devices = devices;
	}
	
	private int getInitialDevicePosition(BluetoothDevice device) {
		if (device != null) {
			for (int i = 0; i < devices.size(); i++) {
				if (device.equals(devices.get(i)))
					return i;
			}
		}
		
		return -1000;
	}
	
	public void setInitialDevicePosition(BluetoothDevice device, Spinner spinner) {
		int position = getInitialDevicePosition(UserSession.get().getImpresora());
		if (position > 0) 
			spinner.setSelection(position);
	}
	
	public List<BluetoothDevice> getDevices() {
		return devices;
	}
	
	public void setDevices(List<BluetoothDevice> devices) {
		if (!this.devices.isEmpty())
			this.devices.clear();
		
		this.devices.addAll(devices);
	}

}
