package addcel.chedrauipos.presenter;

import java.util.ArrayList;
import java.util.List;

import org.addcel.android.chedrauipos.UserSession;
import org.addcel.android.crypto.AddcelCrypto;
import org.addcel.android.crypto.Cifrados;

import com.activeandroid.util.Log;

import retrofit.client.Response;
import addcel.chedrauipos.api.RompefilasFactory;
import addcel.chedrauipos.ui.callback.DatosCallback;
import addcel.chedrauipos.util.RequestBuilder;
import addcel.chedrauipos.vo.DefaultResponse;
import addcel.chedrauipos.vo.LoginResponse;
import addcel.chedrauipos.vo.Role;
import addcel.chedrauipos.vo.RolesResponse;
import addcel.chedrauipos.vo.Tienda;
import addcel.chedrauipos.vo.TiendasResponse;
import addcel.chedrauipos.vo.UpdateRequest;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

public class DatosPresenter {
	
	private Context context;
	private DatosCallback callback;
	private List<Tienda> tiendas;
	private List<Role> roles;
	private ProgressDialog pDialog;
	private LoginResponse data;
	
	private DatosPresenter() {
		// TODO Auto-generated constructor stub
	}
	
	public DatosPresenter(Context context, DatosCallback callback) {
		this();
		this.context = context;
		this.callback = callback;
		tiendas = new ArrayList<Tienda>();
		roles = new ArrayList<Role>();
		data = UserSession.get().getLogin();
		
	}
	
	public ProgressDialog getpDialog(CharSequence msg) {
		if (pDialog == null) {
			pDialog = new ProgressDialog(context);
			pDialog.setIndeterminate(true);
			pDialog.setCancelable(false);
		}
		
		if (!TextUtils.isEmpty(msg))
			pDialog.setMessage(msg);
		
		return pDialog;
	}
	
	public ProgressDialog getpDialog() {
		return getpDialog(null);
	}
	
	public LoginResponse getData() {
		return data;
	}
	
	public void setData(LoginResponse data) {
		this.data = data;
	}
	
	public List<Tienda> getTiendas() {
		return tiendas;
	}
	
	public void setTiendas(List<Tienda> tiendas) {
		if (!this.tiendas.isEmpty())
			this.tiendas.clear();
		
		this.tiendas.addAll(tiendas);
	}
	
	public void loadTiendasAsync() {
		
		new AsyncTask<Void, Void, List<Tienda>>() {
			
			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				getpDialog("Obteniendo sucursales...").show();;
			}

			@Override
			protected List<Tienda> doInBackground(Void... params) {
				// TODO Auto-generated method stub
				
				if (UserSession.get().getTiendas().isEmpty()) {
				
					try {
						String request = AddcelCrypto.encryptHard(
								RequestBuilder.catalogRequest(UserSession.get().getLogin().getIdUsuario()));
						Response r =  RompefilasFactory.get().getTiendas(request);
						
						TiendasResponse resp = RompefilasFactory.buildResponse(r.getBody(), Cifrados.HARD,
								TiendasResponse.class);
						
						switch (resp.getIdError()) {
						case 0:
							UserSession.get().setTiendas(resp.getTiendas());
							return UserSession.get().getTiendas();
	
						default:
							return null;
						}
						
					} catch (Exception e) {
						Log.e(DatosPresenter.class.getSimpleName(), "Error al obtener tiendas", e);
					}
				
					return null;
				} else {
					return UserSession.get().getTiendas();
				}
					
			}
			
			@Override
			protected void onPostExecute(List<Tienda> result) {
				// TODO Auto-generated method stub
				getpDialog().dismiss();
				callback.onTiendasReceived(result);
			}
			
		}.execute();
	}
	
	public List<Role> getRoles() {
		return roles;
	}
	
	public void setRoles(List<Role> roles) {
		if (!this.roles.isEmpty())
			this.roles.clear();
		
		this.roles.addAll(roles);
	}
	
	public void loadRolesAsync() {
		
		new AsyncTask<Void, Void, List<Role>>() {
			
			protected void onPreExecute() {
				getpDialog("Obteniendo Roles de usuario...").show();
			};
			
			@Override
			protected List<Role> doInBackground(Void... params) {
				// TODO Auto-generated method stub
				
				List<Role> result = new ArrayList<Role>();
				
				if (UserSession.get().getRoles().isEmpty()) {
					
					try {
						String request = AddcelCrypto.encryptHard(
								RequestBuilder.catalogRequest(UserSession.get().getLogin().getIdUsuario()));
						Response r =  RompefilasFactory.get().getRoles(request);
						
						RolesResponse resp = RompefilasFactory.buildResponse(r.getBody(), Cifrados.HARD,
								RolesResponse.class);
						
						switch (resp.getIdError()) {
						case 0:
						case 2:
							UserSession.get().setRoles(resp.getRoles());
							Log.d("Roles en sesión: " + UserSession.get().getRoles().toString());
							result.addAll(resp.getRoles());
							break;
						default:
							break;
						}
						
					} catch (Exception e) {
						Log.e(DatosPresenter.class.getSimpleName(), "Error al obtener roles", e);
					}
				
					return result;
					
				} else {
					result.addAll(UserSession.get().getRoles()); 
					Log.d("Roles en sesión: " + UserSession.get().getRoles().toString());
					return result;
				}
			}
			
			protected void onPostExecute(java.util.List<Role> result) {
				getpDialog().dismiss();
				
				if (!result.isEmpty())
					if (data.getRole().getIdRol() == 100) {
						if (!data.getRole().equals(result.get(0))) {
							result.add(0, new Role(100, "ROOT"));
							Log.d("Roles en sesión: " + UserSession.get().getRoles().toString());
						}
					}
				
				callback.onRolesReceived(result);
			};
			
		}.execute();
	}
	
	public int getSelectedTiendaIndex(Tienda tienda) {
		for (int i = 0; i < tiendas.size(); i++) {
			if (tiendas.get(i).equals(tienda))
				return i;
		}
		
		return 0;
	}
	
	public int getSelectedRoleIndex(Role role) {
		for (int i = 0; i < roles.size(); i++) {
			if (roles.get(i).equals(role))
				return i;
		}
		
		return 0;
	}
	
	public void actualizarDatosAsync(String login, String email, String nombre,
			String paterno, String materno, Tienda tienda, Role role) {
		
		data.setLogin(login);
		data.setMail(email);
		data.setNombre(nombre);
		data.setApellido(paterno);
		data.setMaterno(materno);
		data.setTienda(tienda);
		data.setRole(role);
		
		UpdateRequest req = new UpdateRequest(data);
		
		final String request = RompefilasFactory.buildRequest(req, Cifrados.SENSITIVE);
		
		new AsyncTask<Void, Void, DefaultResponse>() {
			
			protected void onPreExecute() {
				getpDialog("Editando datos de usuario").show();
			};

			@Override
			protected DefaultResponse doInBackground(Void... params) {
				// TODO Auto-generated method stub
				try {
					Response r = RompefilasFactory.get().updateUsuario(request);
					
					return RompefilasFactory.buildResponse(
							r.getBody(), Cifrados.SENSITIVE, DefaultResponse.class);
					
				} catch (Exception e) {
					Log.e(DatosPresenter.class.getSimpleName(), "Error al editar usuario", e);
				}
				
				return null;
			}
			
			protected void onPostExecute(DefaultResponse result) {
				getpDialog().dismiss();
				callback.onDataUpdated(result, data);
			};
			
		}.execute();
	
	}

}
