package addcel.chedrauipos.presenter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import addcel.chedrauipos.ui.callback.MenuCallback;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;

public class MenuPresenter {
	
	private Context context;
	private MenuCallback callback;
	
	public MenuPresenter(Context context, MenuCallback callback) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.callback = callback;
	}
	
	private boolean validaBluetooth(BluetoothAdapter adapter) {
		
		if (adapter == null)
			return false;
		
		return true;
	}
	
	private List<BluetoothDevice> getListaBt() {
		BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
		
		if (!validaBluetooth(adapter))
			return null;
		
		Set<BluetoothDevice> deviceSet = adapter.getBondedDevices();
		
		return new ArrayList<BluetoothDevice>(deviceSet);
	}
	
	public void getBtDevices() {
		callback.onBtDevicesObtained(getListaBt());
	}
	
	

}
