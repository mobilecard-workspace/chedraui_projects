package addcel.chedrauipos.presenter;

import java.util.List;

import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.Url;
import org.addcel.android.chedrauipos.UserSession;
import org.addcel.android.crypto.Cifrados;

import addcel.chedrauipos.ui.callback.RegistroCallback;
import addcel.chedrauipos.util.RequestBuilder;
import addcel.chedrauipos.vo.DefaultResponse;
import addcel.chedrauipos.vo.Role;
import addcel.chedrauipos.vo.RolesResponse;
import addcel.chedrauipos.vo.Tienda;
import addcel.chedrauipos.vo.TiendasResponse;
import addcel.chedrauipos.vo.UserInsertRequest;
import addcel.gson.GsonFactory;
import addcel.http.Client;
import addcel.http.ClientFactory;
import addcel.util.ProgressDialogFactory;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

public class RegistroController {
	
	private Context context;
	private Client client;
	private RegistroCallback callback;
	private ProgressDialog pDialog;
	private List<Tienda> tiendas;
	private List<Role> roles;
	private String catalogRequest;
	
	public RegistroController(Context context, RegistroCallback callback) {
		this.context = context;
		this.callback = callback;
		this.pDialog = ProgressDialogFactory.get(this.context, R.string.msg_pdialog);
		tiendas = UserSession.get().getTiendas();
		roles = UserSession.get().getRoles();
		catalogRequest = RequestBuilder.catalogRequest(UserSession.get().getLogin().getIdUsuario());
		
	}
	
	public List<Tienda> getTiendas() {
		return tiendas;
	}
	
	public void setTiendas(List<Tienda> tiendas) {
		this.tiendas = tiendas;
	}
	
	public List<Role> getRoles() {
		return roles;
	}
	
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	public void getTiendasAsync() {
		
		if (tiendas.isEmpty()) {
		
			new AsyncTask<Void, Void, String>() {
				
				@Override
				protected void onPreExecute() {
					// TODO Auto-generated method stub
					if (!pDialog.isShowing())
						pDialog.show();
				}
	
				@Override
				protected String doInBackground(Void... params) {
					// TODO Auto-generated method stub
					client = ClientFactory.get(Cifrados.HARD, Url.TIENDA_GET, null, catalogRequest);
					return client.getData();
				}
				
				protected void onPostExecute(String result) {
					// TODO Auto-generated method stub
					if (pDialog.isShowing())
						pDialog.dismiss();
					
					if (TextUtils.isEmpty(result))
						callback.onTiendasReceived(null);
					else {
						TiendasResponse response = GsonFactory.get()
								.fromJson(result, TiendasResponse.class);
						callback.onTiendasReceived(response);
					}
				}
			}.execute();
		}
	}
	
	public void getRolesAsync() {
		
		if (roles.isEmpty()) {
			
			new AsyncTask<Void, Void, String>() {
				
				@Override
				protected void onPreExecute() {
					// TODO Auto-generated method stub
					if (!pDialog.isShowing())
						pDialog.show();
				}
	
				@Override
				protected String doInBackground(Void... params) {
					// TODO Auto-generated method stub
					client = ClientFactory.get(Cifrados.HARD, Url.ROLE_GET, null, catalogRequest);
					return client.getData();
				}
				
				protected void onPostExecute(String result) {
					// TODO Auto-generated method stub
					if (pDialog.isShowing())
						pDialog.dismiss();
					
					if (TextUtils.isEmpty(result)) {
						callback.onRolesReceived(null);
					} else {
						RolesResponse response = GsonFactory.get()
								.fromJson(result, RolesResponse.class);
						callback.onRolesReceived(response);
					}
				}
			}.execute();
		}
	}
	
	public void registrar(final UserInsertRequest request) {
		new AsyncTask<Void, Void, String>() {
			
			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				if (!pDialog.isShowing())
					pDialog.show();
			}

			@Override
			protected String doInBackground(Void... params) {
				// TODO Auto-generated method stub
				String param = GsonFactory.get().toJson(request);
				client = ClientFactory.get(Cifrados.SENSITIVE, Url.USER_INSERT, null, param);
				return client.getData();
			}
			
			@Override
			protected void onPostExecute(String result) {
				// TODO Auto-generated method stub
				if (pDialog.isShowing())
					pDialog.dismiss();
				
				if (TextUtils.isEmpty(result))
					callback.onRegistroFinished(null);
				else {
					DefaultResponse response = GsonFactory.get()
							.fromJson(result, DefaultResponse.class);
					callback.onRegistroFinished(response);
				}
					
			}
		}.execute();
	}

}
