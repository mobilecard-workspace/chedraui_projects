package addcel.chedrauipos.presenter;

import java.util.ArrayList;
import java.util.List;

import org.addcel.android.chedrauipos.AppStatus;
import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.UserSession;

import addcel.chedrauipos.model.Articulo;
import addcel.chedrauipos.model.Ticket;
import addcel.chedrauipos.model.factory.TicketFactory;
import addcel.chedrauipos.util.BarcodeGenerationUtil;
import addcel.chedrauipos.util.BarcodePrinterUtil;
import addcel.chedrauipos.util.BarcodeProcessor;
import addcel.chedrauipos.util.BarcodeProcessorFactory;
import addcel.chedrauipos.util.BarcodeProcessorFactory.ProcessorType;
import addcel.chedrauipos.util.FileGenerationUtil;
import addcel.chedrauipos.util.FileTransferClient;
import addcel.chedrauipos.util.FtpTransferClient;
import addcel.chedrauipos.vo.DepositoResponse;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

/**
 * Controller que maneja l&oacute;gica de negocio de {@code RompefilasActivity}
 * @author carlosgs
 *
 */
public class RompefilasPresenter {
	
	private Context context;
	private List<Articulo> articulos;
	private FileTransferClient client;
	private BarcodeProcessor barcodeProcessor;
	
	private static final String TAG  = RompefilasPresenter.class.getSimpleName();
	
	public RompefilasPresenter(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.articulos = new ArrayList<Articulo>();
		
		/*
		 * CLIENTE PRODUCTIVO
		 * this.client = FtpTransferClient.get(
		 *		UserSession.get().getLogin().getTienda().getUrlFtp(),
		 *		UserSession.get().getLogin().getTienda().getUserFTP(),
		 *		UserSession.get().getLogin().getTienda().getPassFtp());
		 */
		
		//CLIENTE PRUEBAS
		this.client = FtpTransferClient.getQA();
	}
	
	public List<Articulo> getArticulos() {
		return articulos;
	}
	
	public void setArticulos(List<Articulo> articulos) {
		this.articulos = articulos;
	}
	
	public FileTransferClient getClient() {
		return client;
	}
	
	public void setClient(FileTransferClient client) {
		this.client = client;
	}
	
	private String processBarcode(String upc) {
		int len = upc.length();
		switch (len) {
		case 18:
			if (TextUtils.indexOf(upc, "23") == 0 || TextUtils.indexOf(upc, "25") == 0) {
				barcodeProcessor = BarcodeProcessorFactory.get(ProcessorType.PESABLE);
			} else {
				barcodeProcessor = BarcodeProcessorFactory.get(ProcessorType.DEFAULT);
			}
			break;

		default:
			if (TextUtils.indexOf(upc, "99999") == 0)
				barcodeProcessor = BarcodeProcessorFactory.get(ProcessorType.NO_PROCESS);
			else
				barcodeProcessor = BarcodeProcessorFactory.get(ProcessorType.DEFAULT);
			
			break;
		}
		
		return barcodeProcessor.getUpc(upc);
	}
	
	public void deleteAllArticulos() {
		articulos.clear();
	}
	
	public boolean searchArticulo(String upc) {
		Log.d(TAG, "Buscando: " + upc);
		upc = processBarcode(upc.trim());
		Log.d(TAG, "Después de procesar: " + upc);
		
		long upcLong = Long.parseLong(upc);
		Articulo articulo = Articulo.getArticulo(upcLong);
		
		if (articulo != null) {
			articulo = barcodeProcessor.processArticulo(upc, articulo);
			articulos.add(articulo);
			return true;
		}
		
		return false;
	}
	
	public DepositoResponse depositFile() {
		
		DepositoResponse response = new DepositoResponse();
		
		String consecutivo = BarcodeGenerationUtil.get().generaNSTicket(context,
				AppStatus.get(context).getConsecutivo());
		String barcode = BarcodeGenerationUtil.get().generaCodigoBarrasTicket(
				consecutivo, Articulo.getTotalString(articulos));
		String fileString = Articulo.getFileString(articulos);
		
		if (!TextUtils.isEmpty(fileString)) {
			String fileName = FileGenerationUtil.get().generaArchivoRF(context,
					barcode, fileString);
			
			Ticket t = TicketFactory.get(barcode, consecutivo, Articulo.getTotal(articulos));
			
			int result = -10000;
			String mensajeError = "Error";
			
			if (client.connect()) {
				
				result = client.sendFile(context.getFilesDir() + "/" + fileName,
						"/home/chedraui/version/" + fileName);
				
//				result = client.sendFile(context.getFilesDir() + "/" + fileName,
//				"C:TOLEDO/" + fileName);
			}
			
			switch (result) {
			case 0:
				mensajeError = context.getString(R.string.exito_deposito);
				t.setProcesado(true);
				break;

			default:
				mensajeError = context.getString(R.string.error_deposito);
				t.setProcesado(false);
				t.setFileString(fileString);
				break;
			}
			
			t.save();
			
			response.setIdError(result);
			response.setMensajeError(mensajeError);
			response.setTicket(t);
		}
		
		return response;
	}
	
	public int printReceipt(Ticket t) {
		
		BluetoothDevice device = UserSession.get().getImpresora();
		
		if (null != device) {
			try {
				Log.d(TAG, device.getAddress());
				
				Log.d(TAG, "Intentando imprimir barcode.");
				return BarcodePrinterUtil.printBarcode(device, t);
	 
	        } catch (Exception e) {
	              // Handle communications error here.
	          Log.e(TAG, Exception.class.getSimpleName(), e);
	          return -1;
	        }
		} else {
			Log.d("setBluetooth", "No se cargó dispositivo BT.");
			return -1;
		}
	}
}
