package addcel.chedrauipos.presenter;

import java.util.List;

import org.addcel.android.chedrauipos.R;
import org.joda.time.DateTime;

import com.squareup.phrase.Phrase;

import addcel.chedrauipos.model.Ticket;
import addcel.chedrauipos.util.FileGenerationUtil;
import addcel.chedrauipos.util.FileTransferClient;
import addcel.chedrauipos.util.FtpTransferClient;
import addcel.chedrauipos.vo.DepositoResponse;
import addcel.util.AddcelTextUtil;
import android.content.Context;
import android.text.TextUtils;

public class TicketController {

	private List<Ticket> tickets;
	private FileTransferClient client;

	public TicketController() {
		// TODO Auto-generated constructor stub
		this.client = FtpTransferClient.getQA(); // CAMBIAR POR PRODUCCION
		/*
		 * CLIENTE PRODUCTIVO this.client = FtpTransferClient.get(
		 * UserSession.get().getLogin().getTienda().getUrlFtp(),
		 * UserSession.get().getLogin().getTienda().getUserFTP(),
		 * UserSession.get().getLogin().getTienda().getPassFtp());
		 */
		this.tickets = Ticket.getAll();

	}

	public List<Ticket> getTickets() {
		return tickets;
	}

	public void setTickets(List<Ticket> tickets) {
		this.tickets = tickets;
	}

	public CharSequence getTicketDescripcion(Context context, int position) {
		Ticket t = tickets.get(position);
		CharSequence pagado = t.isProcesado() ? "pagado" : "por pagar";
		CharSequence status = t.isProcesado() ? "Procesado" : "No procesado";

		return Phrase
				.from(context, R.string.txt_ticket_desc)
				.put("pagado", pagado)
				.put("total", AddcelTextUtil.formatCurrency(t.getTotal()))
				.put("fecha",
						new DateTime(t.getFecha())
								.toString("dd/MM/yyyy HH:mm:ss"))
				.put("status", status).format();
	}

	public DepositoResponse depositFile(Context context, int position) {

		DepositoResponse response = new DepositoResponse();
		Ticket t = tickets.get(position);
		String barcode = t.getBarcode();
		String fileString = t.getFileString();

		if (!TextUtils.isEmpty(fileString)) {
			String fileName = FileGenerationUtil.get().generaArchivoRF(context,
					barcode, fileString);

			int result = -10000;
			String mensajeError = "Error";

			if (client.connect()) {

				result = client.sendFile(
						context.getFilesDir() + "/" + fileName,
						"/home/chedraui/version/" + fileName);

				// result = client.sendFile(context.getFilesDir() + "/" +
				// fileName,
				// "C:TOLEDO/" + fileName);
			}

			switch (result) {
			case 0:
				mensajeError = context.getString(R.string.exito_deposito);
				t.setProcesado(true);
				break;

			default:
				mensajeError = context.getString(R.string.error_deposito);
				t.setProcesado(false);
				t.setFileString(fileString);
				break;
			}

			t.save();
			tickets.set(position, t);
			response.setIdError(result);
			response.setMensajeError(mensajeError);
			response.setTicket(t);
		}

		return response;
	}

}
