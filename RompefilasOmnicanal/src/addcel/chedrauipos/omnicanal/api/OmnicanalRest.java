package addcel.chedrauipos.omnicanal.api;

import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import addcel.chedrauipos.omnicanal.vo.AbstractVO;
import addcel.chedrauipos.omnicanal.vo.DatosTransaccion;
import addcel.chedrauipos.omnicanal.vo.PedidoResponse;

public interface OmnicanalRest {
	
	public static final String DEV = "http://50.57.192.214:8080/OMNICANALBridge/";
	public static final String PROD = "https://www.mobilecard.mx:8443/OMNICANALBridge/";
	
	@POST("/services/mobile/setStatus")
	AbstractVO setStatus(@Body DatosTransaccion json) throws Exception;
	
	@POST("/services/mobile/partialCancelPedido")
	AbstractVO partialCancelPedido(@Body DatosTransaccion json) throws Exception;
	
	@POST("/services/mobile/getPedido")
	PedidoResponse getPedido(@Body DatosTransaccion json) throws Exception;
	

}
