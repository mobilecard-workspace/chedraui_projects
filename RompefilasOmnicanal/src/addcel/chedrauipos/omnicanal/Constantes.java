package addcel.chedrauipos.omnicanal;

public class Constantes {
	
	public static final String PATH = "/usr/java/resources/files/chedraui/omnicanal/";
	
	public static final String STATUS_EN_PROCESO_ENTREGA = "proceso_entrega";
	public static final String STATUS_RENDIR_ENTREGA= "entregado";
	public static final String STATUS_EN_PAGADO = "pending";
	public static final String STATUS_EN_CANCELAR = "cancelado_total";
	public static final String STATUS_CANCELADO_PARCIAL = "cancelado_parcial";
	public static final String STATUS_PENDIENTE_PAGO = "pending_payment";
	public static final String STATUS_EN_PROCESO_SURTIDO = "proceso_surtido";
	
	public static final String STATUS_NUM_CREDITO_AMEX = "45";
	public static final String STATUS_NUM_CREDITO_BANCOMER = "46";
	public static final String STATUS_NUM_DEBITO_BANCOMER = "47";
	public static final String STATUS_NUM_MONEDERO_CHEDRAUI = "47";
	public static final String STATUS_NUM_CONTRA_ENTREGA = "66";
	public static final String STATUS_NUM_PEDIDO_PAGADO_OTRA_TIENDA = "67";
	
	
	public static final String STATUS_PAGO_EN_LÍNEA = "PAGO EN LINEA";
	public static final String STATUS_CONTRA_ENTREGA = "CONTRA ENTREGA";
	public static final String STATUS_PAGADO_OTRA_TIENDA = "PAGADO EN OTRA TIENDA";
}
