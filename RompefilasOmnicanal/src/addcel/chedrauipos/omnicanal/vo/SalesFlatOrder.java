/**
 * SalesFlatOrder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package addcel.chedrauipos.omnicanal.vo;

import java.math.BigDecimal;
import java.util.Calendar;

import com.google.gson.annotations.Expose;

public class SalesFlatOrder implements java.io.Serializable {
	@Expose
	private String XForwardedFor;
	@Expose
	private BigDecimal adjustmentNegative;
	@Expose
	private BigDecimal adjustmentPositive;
	@Expose
	private String appliedRuleIds;
	@Expose
	private BigDecimal baseAdjustmentNegative;
	@Expose
	private BigDecimal baseAdjustmentPositive;
	@Expose
	private String baseCurrencyCode;
	@Expose
	private BigDecimal baseDiscountAmount;
	@Expose
	private BigDecimal baseDiscountCanceled;
	@Expose
	private BigDecimal baseDiscountInvoiced;
	@Expose
	private BigDecimal baseDiscountRefunded;
	@Expose
	private BigDecimal baseGrandTotal;
	@Expose
	private BigDecimal baseHiddenTaxAmount;
	@Expose
	private BigDecimal baseHiddenTaxInvoiced;
	@Expose
	private BigDecimal baseHiddenTaxRefunded;
	@Expose
	private BigDecimal baseShippingAmount;
	@Expose
	private BigDecimal baseShippingCanceled;
	@Expose
	private BigDecimal baseShippingDiscountAmount;
	@Expose
	private BigDecimal baseShippingHiddenTaxAmnt;
	@Expose
	private BigDecimal baseShippingInclTax;
	@Expose
	private BigDecimal baseShippingInvoiced;
	@Expose
	private BigDecimal baseShippingRefunded;
	@Expose
	private BigDecimal baseShippingTaxAmount;
	@Expose
	private BigDecimal baseShippingTaxRefunded;
	@Expose
	private BigDecimal baseSubtotal;
	@Expose
	private BigDecimal baseSubtotalCanceled;
	@Expose
	private BigDecimal baseSubtotalInclTax;
	@Expose
	private BigDecimal baseSubtotalInvoiced;
	@Expose
	private BigDecimal baseSubtotalRefunded;
	@Expose
	private BigDecimal baseTaxAmount;
	@Expose
	private BigDecimal baseTaxCanceled;
	@Expose
	private BigDecimal baseTaxInvoiced;
	@Expose
	private BigDecimal baseTaxRefunded;
	@Expose
	private BigDecimal baseToGlobalRate;
	@Expose
	private BigDecimal baseToOrderRate;
	@Expose
	private BigDecimal baseTotalCanceled;
	@Expose
	private BigDecimal baseTotalDue;
	@Expose
	private BigDecimal baseTotalInvoiced;
	@Expose
	private BigDecimal baseTotalInvoicedCost;
	@Expose
	private BigDecimal baseTotalOfflineRefunded;
	@Expose
	private BigDecimal baseTotalOnlineRefunded;
	@Expose
	private BigDecimal baseTotalPaid;
	@Expose
	private BigDecimal baseTotalQtyOrdered;
	@Expose
	private BigDecimal baseTotalRefunded;
	@Expose
	private Integer billingAddressId;
	@Expose
	private Short canShipPartially;
	@Expose
	private Short canShipPartiallyItem;
	@Expose
	private String couponCode;
	@Expose
	private String couponRuleName;
	@Expose
	private String customerEmail;
	@Expose
	private String customerFirstname;
	@Expose
	private Integer customerGender;
	@Expose
	private Short customerGroupId;
	@Expose
	private Integer customerId;
	@Expose
	private Short customerIsGuest;
	@Expose
	private String customerLastname;
	@Expose
	private String customerMiddlename;
	@Expose
	private String customerNote;
	@Expose
	private Short customerNoteNotify;
	@Expose
	private String customerPrefix;
	@Expose
	private String customerSuffix;
	@Expose
	private String customerTaxvat;
	@Expose
	private BigDecimal discountAmount;
	@Expose
	private BigDecimal discountCanceled;
	@Expose
	private String discountDescription;
	@Expose
	private BigDecimal discountInvoiced;
	@Expose
	private BigDecimal discountRefunded;
	@Expose
	private Integer editIncrement;
	@Expose
	private Short emailSent;
	@Expose
	private Integer entityId;
	@Expose
	private String extCustomerId;
	@Expose
	private String extOrderId;
	@Expose
	private Short forcedShipmentWithInvoice;
	@Expose
	private Integer giftMessageId;
	@Expose
	private String globalCurrencyCode;
	@Expose
	private BigDecimal grandTotal;
	@Expose
	private BigDecimal hiddenTaxAmount;
	@Expose
	private BigDecimal hiddenTaxInvoiced;
	@Expose
	private BigDecimal hiddenTaxRefunded;
	@Expose
	private String holdBeforeState;
	@Expose
	private String holdBeforeStatus;
	@Expose
	private String incrementId;
	@Expose
	private Short isVirtual;
	@Expose
	private String orderCurrencyCode;
	@Expose
	private String originalIncrementId;
	@Expose
	private Integer paymentAuthExpiration;
	@Expose
	private BigDecimal paymentAuthorizationAmount;
	@Expose
	private Integer paypalIpnCustomerNotified;
	@Expose
	private Integer pendingTlog;
	@Expose
	private String protectCode;
	@Expose
	private Integer quoteAddressId;
	@Expose
	private Integer quoteId;
	@Expose
	private String relationChildId;
	@Expose
	private String relationChildRealId;
	@Expose
	private String relationParentId;
	@Expose
	private String relationParentRealId;
	@Expose
	private String remoteIp;
	@Expose
	private Integer shippingAddressId;
	@Expose
	private BigDecimal shippingAmount;
	@Expose
	private BigDecimal shippingCanceled;
	@Expose
	private String shippingDescription;
	@Expose
	private BigDecimal shippingDiscountAmount;
	@Expose
	private BigDecimal shippingHiddenTaxAmount;
	@Expose
	private BigDecimal shippingInclTax;
	@Expose
	private BigDecimal shippingInvoiced;
	@Expose
	private String shippingMethod;
	@Expose
	private BigDecimal shippingRefunded;
	@Expose
	private BigDecimal shippingTaxAmount;
	@Expose
	private BigDecimal shippingTaxRefunded;
	@Expose
	private String state;
	@Expose
	private String status;
	@Expose
	private String storeCurrencyCode;
	@Expose
	private Short storeId;
	@Expose
	private String storeName;
	@Expose
	private BigDecimal storeToBaseRate;
	@Expose
	private BigDecimal storeToOrderRate;
	@Expose
	private BigDecimal subtotal;
	@Expose
	private BigDecimal subtotalCanceled;
	@Expose
	private BigDecimal subtotalInclTax;
	@Expose
	private BigDecimal subtotalInvoiced;
	@Expose
	private BigDecimal subtotalRefunded;
	@Expose
	private Integer supplierId;
	@Expose
	private BigDecimal taxAmount;
	@Expose
	private BigDecimal taxCanceled;
	@Expose
	private BigDecimal taxInvoiced;
	@Expose
	private BigDecimal taxRefunded;
	@Expose
	private BigDecimal totalCanceled;
	@Expose
	private BigDecimal totalDue;
	@Expose
	private BigDecimal totalInvoiced;
	@Expose
	private short totalItemCount;
	@Expose
	private BigDecimal totalOfflineRefunded;
	@Expose
	private BigDecimal totalOnlineRefunded;
	@Expose
	private BigDecimal totalPaid;
	@Expose
	private BigDecimal totalQtyOrdered;
	@Expose
	private BigDecimal totalRefunded;
	@Expose
	private BigDecimal weight;

	public SalesFlatOrder() {
	}

	public SalesFlatOrder(String XForwardedFor, BigDecimal adjustmentNegative,
			BigDecimal adjustmentPositive, String appliedRuleIds,
			BigDecimal baseAdjustmentNegative,
			BigDecimal baseAdjustmentPositive, String baseCurrencyCode,
			BigDecimal baseDiscountAmount, BigDecimal baseDiscountCanceled,
			BigDecimal baseDiscountInvoiced, BigDecimal baseDiscountRefunded,
			BigDecimal baseGrandTotal, BigDecimal baseHiddenTaxAmount,
			BigDecimal baseHiddenTaxInvoiced, BigDecimal baseHiddenTaxRefunded,
			BigDecimal baseShippingAmount, BigDecimal baseShippingCanceled,
			BigDecimal baseShippingDiscountAmount,
			BigDecimal baseShippingHiddenTaxAmnt,
			BigDecimal baseShippingInclTax, BigDecimal baseShippingInvoiced,
			BigDecimal baseShippingRefunded, BigDecimal baseShippingTaxAmount,
			BigDecimal baseShippingTaxRefunded, BigDecimal baseSubtotal,
			BigDecimal baseSubtotalCanceled, BigDecimal baseSubtotalInclTax,
			BigDecimal baseSubtotalInvoiced, BigDecimal baseSubtotalRefunded,
			BigDecimal baseTaxAmount, BigDecimal baseTaxCanceled,
			BigDecimal baseTaxInvoiced, BigDecimal baseTaxRefunded,
			BigDecimal baseToGlobalRate, BigDecimal baseToOrderRate,
			BigDecimal baseTotalCanceled, BigDecimal baseTotalDue,
			BigDecimal baseTotalInvoiced, BigDecimal baseTotalInvoicedCost,
			BigDecimal baseTotalOfflineRefunded,
			BigDecimal baseTotalOnlineRefunded, BigDecimal baseTotalPaid,
			BigDecimal baseTotalQtyOrdered, BigDecimal baseTotalRefunded,
			Integer billingAddressId, Short canShipPartially,
			Short canShipPartiallyItem, String couponCode,
			String couponRuleName, String customerEmail,
			String customerFirstname, Integer customerGender,
			Short customerGroupId, Integer customerId, Short customerIsGuest,
			String customerLastname, String customerMiddlename,
			String customerNote, Short customerNoteNotify,
			String customerPrefix, String customerSuffix,
			String customerTaxvat, BigDecimal discountAmount,
			BigDecimal discountCanceled, String discountDescription,
			BigDecimal discountInvoiced, BigDecimal discountRefunded,
			Integer editIncrement, Short emailSent, Integer entityId,
			String extCustomerId, String extOrderId,
			Short forcedShipmentWithInvoice, Integer giftMessageId,
			String globalCurrencyCode, BigDecimal grandTotal,
			BigDecimal hiddenTaxAmount, BigDecimal hiddenTaxInvoiced,
			BigDecimal hiddenTaxRefunded, String holdBeforeState,
			String holdBeforeStatus, String incrementId, Short isVirtual,
			String orderCurrencyCode, String originalIncrementId,
			Integer paymentAuthExpiration,
			BigDecimal paymentAuthorizationAmount,
			Integer paypalIpnCustomerNotified, Integer pendingTlog,
			String protectCode, Integer quoteAddressId, Integer quoteId,
			String relationChildId, String relationChildRealId,
			String relationParentId, String relationParentRealId,
			String remoteIp, Integer shippingAddressId,
			BigDecimal shippingAmount, BigDecimal shippingCanceled,
			String shippingDescription, BigDecimal shippingDiscountAmount,
			BigDecimal shippingHiddenTaxAmount, BigDecimal shippingInclTax,
			BigDecimal shippingInvoiced, String shippingMethod,
			BigDecimal shippingRefunded, BigDecimal shippingTaxAmount,
			BigDecimal shippingTaxRefunded, String state, String status,
			String storeCurrencyCode, Short storeId, String storeName,
			BigDecimal storeToBaseRate, BigDecimal storeToOrderRate,
			BigDecimal subtotal, BigDecimal subtotalCanceled,
			BigDecimal subtotalInclTax, BigDecimal subtotalInvoiced,
			BigDecimal subtotalRefunded, Integer supplierId,
			BigDecimal taxAmount, BigDecimal taxCanceled,
			BigDecimal taxInvoiced, BigDecimal taxRefunded,
			BigDecimal totalCanceled, BigDecimal totalDue,
			BigDecimal totalInvoiced, short totalItemCount,
			BigDecimal totalOfflineRefunded, BigDecimal totalOnlineRefunded,
			BigDecimal totalPaid, BigDecimal totalQtyOrdered,
			BigDecimal totalRefunded, BigDecimal weight) {
		this.XForwardedFor = XForwardedFor;
		this.adjustmentNegative = adjustmentNegative;
		this.adjustmentPositive = adjustmentPositive;
		this.appliedRuleIds = appliedRuleIds;
		this.baseAdjustmentNegative = baseAdjustmentNegative;
		this.baseAdjustmentPositive = baseAdjustmentPositive;
		this.baseCurrencyCode = baseCurrencyCode;
		this.baseDiscountAmount = baseDiscountAmount;
		this.baseDiscountCanceled = baseDiscountCanceled;
		this.baseDiscountInvoiced = baseDiscountInvoiced;
		this.baseDiscountRefunded = baseDiscountRefunded;
		this.baseGrandTotal = baseGrandTotal;
		this.baseHiddenTaxAmount = baseHiddenTaxAmount;
		this.baseHiddenTaxInvoiced = baseHiddenTaxInvoiced;
		this.baseHiddenTaxRefunded = baseHiddenTaxRefunded;
		this.baseShippingAmount = baseShippingAmount;
		this.baseShippingCanceled = baseShippingCanceled;
		this.baseShippingDiscountAmount = baseShippingDiscountAmount;
		this.baseShippingHiddenTaxAmnt = baseShippingHiddenTaxAmnt;
		this.baseShippingInclTax = baseShippingInclTax;
		this.baseShippingInvoiced = baseShippingInvoiced;
		this.baseShippingRefunded = baseShippingRefunded;
		this.baseShippingTaxAmount = baseShippingTaxAmount;
		this.baseShippingTaxRefunded = baseShippingTaxRefunded;
		this.baseSubtotal = baseSubtotal;
		this.baseSubtotalCanceled = baseSubtotalCanceled;
		this.baseSubtotalInclTax = baseSubtotalInclTax;
		this.baseSubtotalInvoiced = baseSubtotalInvoiced;
		this.baseSubtotalRefunded = baseSubtotalRefunded;
		this.baseTaxAmount = baseTaxAmount;
		this.baseTaxCanceled = baseTaxCanceled;
		this.baseTaxInvoiced = baseTaxInvoiced;
		this.baseTaxRefunded = baseTaxRefunded;
		this.baseToGlobalRate = baseToGlobalRate;
		this.baseToOrderRate = baseToOrderRate;
		this.baseTotalCanceled = baseTotalCanceled;
		this.baseTotalDue = baseTotalDue;
		this.baseTotalInvoiced = baseTotalInvoiced;
		this.baseTotalInvoicedCost = baseTotalInvoicedCost;
		this.baseTotalOfflineRefunded = baseTotalOfflineRefunded;
		this.baseTotalOnlineRefunded = baseTotalOnlineRefunded;
		this.baseTotalPaid = baseTotalPaid;
		this.baseTotalQtyOrdered = baseTotalQtyOrdered;
		this.baseTotalRefunded = baseTotalRefunded;
		this.billingAddressId = billingAddressId;
		this.canShipPartially = canShipPartially;
		this.canShipPartiallyItem = canShipPartiallyItem;
		this.couponCode = couponCode;
		this.couponRuleName = couponRuleName;
		this.customerEmail = customerEmail;
		this.customerFirstname = customerFirstname;
		this.customerGender = customerGender;
		this.customerGroupId = customerGroupId;
		this.customerId = customerId;
		this.customerIsGuest = customerIsGuest;
		this.customerLastname = customerLastname;
		this.customerMiddlename = customerMiddlename;
		this.customerNote = customerNote;
		this.customerNoteNotify = customerNoteNotify;
		this.customerPrefix = customerPrefix;
		this.customerSuffix = customerSuffix;
		this.customerTaxvat = customerTaxvat;
		this.discountAmount = discountAmount;
		this.discountCanceled = discountCanceled;
		this.discountDescription = discountDescription;
		this.discountInvoiced = discountInvoiced;
		this.discountRefunded = discountRefunded;
		this.editIncrement = editIncrement;
		this.emailSent = emailSent;
		this.entityId = entityId;
		this.extCustomerId = extCustomerId;
		this.extOrderId = extOrderId;
		this.forcedShipmentWithInvoice = forcedShipmentWithInvoice;
		this.giftMessageId = giftMessageId;
		this.globalCurrencyCode = globalCurrencyCode;
		this.grandTotal = grandTotal;
		this.hiddenTaxAmount = hiddenTaxAmount;
		this.hiddenTaxInvoiced = hiddenTaxInvoiced;
		this.hiddenTaxRefunded = hiddenTaxRefunded;
		this.holdBeforeState = holdBeforeState;
		this.holdBeforeStatus = holdBeforeStatus;
		this.incrementId = incrementId;
		this.isVirtual = isVirtual;
		this.orderCurrencyCode = orderCurrencyCode;
		this.originalIncrementId = originalIncrementId;
		this.paymentAuthExpiration = paymentAuthExpiration;
		this.paymentAuthorizationAmount = paymentAuthorizationAmount;
		this.paypalIpnCustomerNotified = paypalIpnCustomerNotified;
		this.pendingTlog = pendingTlog;
		this.protectCode = protectCode;
		this.quoteAddressId = quoteAddressId;
		this.quoteId = quoteId;
		this.relationChildId = relationChildId;
		this.relationChildRealId = relationChildRealId;
		this.relationParentId = relationParentId;
		this.relationParentRealId = relationParentRealId;
		this.remoteIp = remoteIp;
		this.shippingAddressId = shippingAddressId;
		this.shippingAmount = shippingAmount;
		this.shippingCanceled = shippingCanceled;
		this.shippingDescription = shippingDescription;
		this.shippingDiscountAmount = shippingDiscountAmount;
		this.shippingHiddenTaxAmount = shippingHiddenTaxAmount;
		this.shippingInclTax = shippingInclTax;
		this.shippingInvoiced = shippingInvoiced;
		this.shippingMethod = shippingMethod;
		this.shippingRefunded = shippingRefunded;
		this.shippingTaxAmount = shippingTaxAmount;
		this.shippingTaxRefunded = shippingTaxRefunded;
		this.state = state;
		this.status = status;
		this.storeCurrencyCode = storeCurrencyCode;
		this.storeId = storeId;
		this.storeName = storeName;
		this.storeToBaseRate = storeToBaseRate;
		this.storeToOrderRate = storeToOrderRate;
		this.subtotal = subtotal;
		this.subtotalCanceled = subtotalCanceled;
		this.subtotalInclTax = subtotalInclTax;
		this.subtotalInvoiced = subtotalInvoiced;
		this.subtotalRefunded = subtotalRefunded;
		this.supplierId = supplierId;
		this.taxAmount = taxAmount;
		this.taxCanceled = taxCanceled;
		this.taxInvoiced = taxInvoiced;
		this.taxRefunded = taxRefunded;
		this.totalCanceled = totalCanceled;
		this.totalDue = totalDue;
		this.totalInvoiced = totalInvoiced;
		this.totalItemCount = totalItemCount;
		this.totalOfflineRefunded = totalOfflineRefunded;
		this.totalOnlineRefunded = totalOnlineRefunded;
		this.totalPaid = totalPaid;
		this.totalQtyOrdered = totalQtyOrdered;
		this.totalRefunded = totalRefunded;
		this.weight = weight;
	}

	/**
	 * Gets the XForwardedFor value for this SalesFlatOrder.
	 * 
	 * @return XForwardedFor
	 */
	public String getXForwardedFor() {
		return XForwardedFor;
	}

	/**
	 * Sets the XForwardedFor value for this SalesFlatOrder.
	 * 
	 * @param XForwardedFor
	 */
	public void setXForwardedFor(String XForwardedFor) {
		this.XForwardedFor = XForwardedFor;
	}

	/**
	 * Gets the adjustmentNegative value for this SalesFlatOrder.
	 * 
	 * @return adjustmentNegative
	 */
	public BigDecimal getAdjustmentNegative() {
		return adjustmentNegative;
	}

	/**
	 * Sets the adjustmentNegative value for this SalesFlatOrder.
	 * 
	 * @param adjustmentNegative
	 */
	public void setAdjustmentNegative(BigDecimal adjustmentNegative) {
		this.adjustmentNegative = adjustmentNegative;
	}

	/**
	 * Gets the adjustmentPositive value for this SalesFlatOrder.
	 * 
	 * @return adjustmentPositive
	 */
	public BigDecimal getAdjustmentPositive() {
		return adjustmentPositive;
	}

	/**
	 * Sets the adjustmentPositive value for this SalesFlatOrder.
	 * 
	 * @param adjustmentPositive
	 */
	public void setAdjustmentPositive(BigDecimal adjustmentPositive) {
		this.adjustmentPositive = adjustmentPositive;
	}

	/**
	 * Gets the appliedRuleIds value for this SalesFlatOrder.
	 * 
	 * @return appliedRuleIds
	 */
	public String getAppliedRuleIds() {
		return appliedRuleIds;
	}

	/**
	 * Sets the appliedRuleIds value for this SalesFlatOrder.
	 * 
	 * @param appliedRuleIds
	 */
	public void setAppliedRuleIds(String appliedRuleIds) {
		this.appliedRuleIds = appliedRuleIds;
	}

	/**
	 * Gets the baseAdjustmentNegative value for this SalesFlatOrder.
	 * 
	 * @return baseAdjustmentNegative
	 */
	public BigDecimal getBaseAdjustmentNegative() {
		return baseAdjustmentNegative;
	}

	/**
	 * Sets the baseAdjustmentNegative value for this SalesFlatOrder.
	 * 
	 * @param baseAdjustmentNegative
	 */
	public void setBaseAdjustmentNegative(BigDecimal baseAdjustmentNegative) {
		this.baseAdjustmentNegative = baseAdjustmentNegative;
	}

	/**
	 * Gets the baseAdjustmentPositive value for this SalesFlatOrder.
	 * 
	 * @return baseAdjustmentPositive
	 */
	public BigDecimal getBaseAdjustmentPositive() {
		return baseAdjustmentPositive;
	}

	/**
	 * Sets the baseAdjustmentPositive value for this SalesFlatOrder.
	 * 
	 * @param baseAdjustmentPositive
	 */
	public void setBaseAdjustmentPositive(BigDecimal baseAdjustmentPositive) {
		this.baseAdjustmentPositive = baseAdjustmentPositive;
	}

	/**
	 * Gets the baseCurrencyCode value for this SalesFlatOrder.
	 * 
	 * @return baseCurrencyCode
	 */
	public String getBaseCurrencyCode() {
		return baseCurrencyCode;
	}

	/**
	 * Sets the baseCurrencyCode value for this SalesFlatOrder.
	 * 
	 * @param baseCurrencyCode
	 */
	public void setBaseCurrencyCode(String baseCurrencyCode) {
		this.baseCurrencyCode = baseCurrencyCode;
	}

	/**
	 * Gets the baseDiscountAmount value for this SalesFlatOrder.
	 * 
	 * @return baseDiscountAmount
	 */
	public BigDecimal getBaseDiscountAmount() {
		return baseDiscountAmount;
	}

	/**
	 * Sets the baseDiscountAmount value for this SalesFlatOrder.
	 * 
	 * @param baseDiscountAmount
	 */
	public void setBaseDiscountAmount(BigDecimal baseDiscountAmount) {
		this.baseDiscountAmount = baseDiscountAmount;
	}

	/**
	 * Gets the baseDiscountCanceled value for this SalesFlatOrder.
	 * 
	 * @return baseDiscountCanceled
	 */
	public BigDecimal getBaseDiscountCanceled() {
		return baseDiscountCanceled;
	}

	/**
	 * Sets the baseDiscountCanceled value for this SalesFlatOrder.
	 * 
	 * @param baseDiscountCanceled
	 */
	public void setBaseDiscountCanceled(BigDecimal baseDiscountCanceled) {
		this.baseDiscountCanceled = baseDiscountCanceled;
	}

	/**
	 * Gets the baseDiscountInvoiced value for this SalesFlatOrder.
	 * 
	 * @return baseDiscountInvoiced
	 */
	public BigDecimal getBaseDiscountInvoiced() {
		return baseDiscountInvoiced;
	}

	/**
	 * Sets the baseDiscountInvoiced value for this SalesFlatOrder.
	 * 
	 * @param baseDiscountInvoiced
	 */
	public void setBaseDiscountInvoiced(BigDecimal baseDiscountInvoiced) {
		this.baseDiscountInvoiced = baseDiscountInvoiced;
	}

	/**
	 * Gets the baseDiscountRefunded value for this SalesFlatOrder.
	 * 
	 * @return baseDiscountRefunded
	 */
	public BigDecimal getBaseDiscountRefunded() {
		return baseDiscountRefunded;
	}

	/**
	 * Sets the baseDiscountRefunded value for this SalesFlatOrder.
	 * 
	 * @param baseDiscountRefunded
	 */
	public void setBaseDiscountRefunded(BigDecimal baseDiscountRefunded) {
		this.baseDiscountRefunded = baseDiscountRefunded;
	}

	/**
	 * Gets the baseGrandTotal value for this SalesFlatOrder.
	 * 
	 * @return baseGrandTotal
	 */
	public BigDecimal getBaseGrandTotal() {
		return baseGrandTotal;
	}

	/**
	 * Sets the baseGrandTotal value for this SalesFlatOrder.
	 * 
	 * @param baseGrandTotal
	 */
	public void setBaseGrandTotal(BigDecimal baseGrandTotal) {
		this.baseGrandTotal = baseGrandTotal;
	}

	/**
	 * Gets the baseHiddenTaxAmount value for this SalesFlatOrder.
	 * 
	 * @return baseHiddenTaxAmount
	 */
	public BigDecimal getBaseHiddenTaxAmount() {
		return baseHiddenTaxAmount;
	}

	/**
	 * Sets the baseHiddenTaxAmount value for this SalesFlatOrder.
	 * 
	 * @param baseHiddenTaxAmount
	 */
	public void setBaseHiddenTaxAmount(BigDecimal baseHiddenTaxAmount) {
		this.baseHiddenTaxAmount = baseHiddenTaxAmount;
	}

	/**
	 * Gets the baseHiddenTaxInvoiced value for this SalesFlatOrder.
	 * 
	 * @return baseHiddenTaxInvoiced
	 */
	public BigDecimal getBaseHiddenTaxInvoiced() {
		return baseHiddenTaxInvoiced;
	}

	/**
	 * Sets the baseHiddenTaxInvoiced value for this SalesFlatOrder.
	 * 
	 * @param baseHiddenTaxInvoiced
	 */
	public void setBaseHiddenTaxInvoiced(BigDecimal baseHiddenTaxInvoiced) {
		this.baseHiddenTaxInvoiced = baseHiddenTaxInvoiced;
	}

	/**
	 * Gets the baseHiddenTaxRefunded value for this SalesFlatOrder.
	 * 
	 * @return baseHiddenTaxRefunded
	 */
	public BigDecimal getBaseHiddenTaxRefunded() {
		return baseHiddenTaxRefunded;
	}

	/**
	 * Sets the baseHiddenTaxRefunded value for this SalesFlatOrder.
	 * 
	 * @param baseHiddenTaxRefunded
	 */
	public void setBaseHiddenTaxRefunded(BigDecimal baseHiddenTaxRefunded) {
		this.baseHiddenTaxRefunded = baseHiddenTaxRefunded;
	}

	/**
	 * Gets the baseShippingAmount value for this SalesFlatOrder.
	 * 
	 * @return baseShippingAmount
	 */
	public BigDecimal getBaseShippingAmount() {
		return baseShippingAmount;
	}

	/**
	 * Sets the baseShippingAmount value for this SalesFlatOrder.
	 * 
	 * @param baseShippingAmount
	 */
	public void setBaseShippingAmount(BigDecimal baseShippingAmount) {
		this.baseShippingAmount = baseShippingAmount;
	}

	/**
	 * Gets the baseShippingCanceled value for this SalesFlatOrder.
	 * 
	 * @return baseShippingCanceled
	 */
	public BigDecimal getBaseShippingCanceled() {
		return baseShippingCanceled;
	}

	/**
	 * Sets the baseShippingCanceled value for this SalesFlatOrder.
	 * 
	 * @param baseShippingCanceled
	 */
	public void setBaseShippingCanceled(BigDecimal baseShippingCanceled) {
		this.baseShippingCanceled = baseShippingCanceled;
	}

	/**
	 * Gets the baseShippingDiscountAmount value for this SalesFlatOrder.
	 * 
	 * @return baseShippingDiscountAmount
	 */
	public BigDecimal getBaseShippingDiscountAmount() {
		return baseShippingDiscountAmount;
	}

	/**
	 * Sets the baseShippingDiscountAmount value for this SalesFlatOrder.
	 * 
	 * @param baseShippingDiscountAmount
	 */
	public void setBaseShippingDiscountAmount(
			BigDecimal baseShippingDiscountAmount) {
		this.baseShippingDiscountAmount = baseShippingDiscountAmount;
	}

	/**
	 * Gets the baseShippingHiddenTaxAmnt value for this SalesFlatOrder.
	 * 
	 * @return baseShippingHiddenTaxAmnt
	 */
	public BigDecimal getBaseShippingHiddenTaxAmnt() {
		return baseShippingHiddenTaxAmnt;
	}

	/**
	 * Sets the baseShippingHiddenTaxAmnt value for this SalesFlatOrder.
	 * 
	 * @param baseShippingHiddenTaxAmnt
	 */
	public void setBaseShippingHiddenTaxAmnt(
			BigDecimal baseShippingHiddenTaxAmnt) {
		this.baseShippingHiddenTaxAmnt = baseShippingHiddenTaxAmnt;
	}

	/**
	 * Gets the baseShippingInclTax value for this SalesFlatOrder.
	 * 
	 * @return baseShippingInclTax
	 */
	public BigDecimal getBaseShippingInclTax() {
		return baseShippingInclTax;
	}

	/**
	 * Sets the baseShippingInclTax value for this SalesFlatOrder.
	 * 
	 * @param baseShippingInclTax
	 */
	public void setBaseShippingInclTax(BigDecimal baseShippingInclTax) {
		this.baseShippingInclTax = baseShippingInclTax;
	}

	/**
	 * Gets the baseShippingInvoiced value for this SalesFlatOrder.
	 * 
	 * @return baseShippingInvoiced
	 */
	public BigDecimal getBaseShippingInvoiced() {
		return baseShippingInvoiced;
	}

	/**
	 * Sets the baseShippingInvoiced value for this SalesFlatOrder.
	 * 
	 * @param baseShippingInvoiced
	 */
	public void setBaseShippingInvoiced(BigDecimal baseShippingInvoiced) {
		this.baseShippingInvoiced = baseShippingInvoiced;
	}

	/**
	 * Gets the baseShippingRefunded value for this SalesFlatOrder.
	 * 
	 * @return baseShippingRefunded
	 */
	public BigDecimal getBaseShippingRefunded() {
		return baseShippingRefunded;
	}

	/**
	 * Sets the baseShippingRefunded value for this SalesFlatOrder.
	 * 
	 * @param baseShippingRefunded
	 */
	public void setBaseShippingRefunded(BigDecimal baseShippingRefunded) {
		this.baseShippingRefunded = baseShippingRefunded;
	}

	/**
	 * Gets the baseShippingTaxAmount value for this SalesFlatOrder.
	 * 
	 * @return baseShippingTaxAmount
	 */
	public BigDecimal getBaseShippingTaxAmount() {
		return baseShippingTaxAmount;
	}

	/**
	 * Sets the baseShippingTaxAmount value for this SalesFlatOrder.
	 * 
	 * @param baseShippingTaxAmount
	 */
	public void setBaseShippingTaxAmount(BigDecimal baseShippingTaxAmount) {
		this.baseShippingTaxAmount = baseShippingTaxAmount;
	}

	/**
	 * Gets the baseShippingTaxRefunded value for this SalesFlatOrder.
	 * 
	 * @return baseShippingTaxRefunded
	 */
	public BigDecimal getBaseShippingTaxRefunded() {
		return baseShippingTaxRefunded;
	}

	/**
	 * Sets the baseShippingTaxRefunded value for this SalesFlatOrder.
	 * 
	 * @param baseShippingTaxRefunded
	 */
	public void setBaseShippingTaxRefunded(BigDecimal baseShippingTaxRefunded) {
		this.baseShippingTaxRefunded = baseShippingTaxRefunded;
	}

	/**
	 * Gets the baseSubtotal value for this SalesFlatOrder.
	 * 
	 * @return baseSubtotal
	 */
	public BigDecimal getBaseSubtotal() {
		return baseSubtotal;
	}

	/**
	 * Sets the baseSubtotal value for this SalesFlatOrder.
	 * 
	 * @param baseSubtotal
	 */
	public void setBaseSubtotal(BigDecimal baseSubtotal) {
		this.baseSubtotal = baseSubtotal;
	}

	/**
	 * Gets the baseSubtotalCanceled value for this SalesFlatOrder.
	 * 
	 * @return baseSubtotalCanceled
	 */
	public BigDecimal getBaseSubtotalCanceled() {
		return baseSubtotalCanceled;
	}

	/**
	 * Sets the baseSubtotalCanceled value for this SalesFlatOrder.
	 * 
	 * @param baseSubtotalCanceled
	 */
	public void setBaseSubtotalCanceled(BigDecimal baseSubtotalCanceled) {
		this.baseSubtotalCanceled = baseSubtotalCanceled;
	}

	/**
	 * Gets the baseSubtotalInclTax value for this SalesFlatOrder.
	 * 
	 * @return baseSubtotalInclTax
	 */
	public BigDecimal getBaseSubtotalInclTax() {
		return baseSubtotalInclTax;
	}

	/**
	 * Sets the baseSubtotalInclTax value for this SalesFlatOrder.
	 * 
	 * @param baseSubtotalInclTax
	 */
	public void setBaseSubtotalInclTax(BigDecimal baseSubtotalInclTax) {
		this.baseSubtotalInclTax = baseSubtotalInclTax;
	}

	/**
	 * Gets the baseSubtotalInvoiced value for this SalesFlatOrder.
	 * 
	 * @return baseSubtotalInvoiced
	 */
	public BigDecimal getBaseSubtotalInvoiced() {
		return baseSubtotalInvoiced;
	}

	/**
	 * Sets the baseSubtotalInvoiced value for this SalesFlatOrder.
	 * 
	 * @param baseSubtotalInvoiced
	 */
	public void setBaseSubtotalInvoiced(BigDecimal baseSubtotalInvoiced) {
		this.baseSubtotalInvoiced = baseSubtotalInvoiced;
	}

	/**
	 * Gets the baseSubtotalRefunded value for this SalesFlatOrder.
	 * 
	 * @return baseSubtotalRefunded
	 */
	public BigDecimal getBaseSubtotalRefunded() {
		return baseSubtotalRefunded;
	}

	/**
	 * Sets the baseSubtotalRefunded value for this SalesFlatOrder.
	 * 
	 * @param baseSubtotalRefunded
	 */
	public void setBaseSubtotalRefunded(BigDecimal baseSubtotalRefunded) {
		this.baseSubtotalRefunded = baseSubtotalRefunded;
	}

	/**
	 * Gets the baseTaxAmount value for this SalesFlatOrder.
	 * 
	 * @return baseTaxAmount
	 */
	public BigDecimal getBaseTaxAmount() {
		return baseTaxAmount;
	}

	/**
	 * Sets the baseTaxAmount value for this SalesFlatOrder.
	 * 
	 * @param baseTaxAmount
	 */
	public void setBaseTaxAmount(BigDecimal baseTaxAmount) {
		this.baseTaxAmount = baseTaxAmount;
	}

	/**
	 * Gets the baseTaxCanceled value for this SalesFlatOrder.
	 * 
	 * @return baseTaxCanceled
	 */
	public BigDecimal getBaseTaxCanceled() {
		return baseTaxCanceled;
	}

	/**
	 * Sets the baseTaxCanceled value for this SalesFlatOrder.
	 * 
	 * @param baseTaxCanceled
	 */
	public void setBaseTaxCanceled(BigDecimal baseTaxCanceled) {
		this.baseTaxCanceled = baseTaxCanceled;
	}

	/**
	 * Gets the baseTaxInvoiced value for this SalesFlatOrder.
	 * 
	 * @return baseTaxInvoiced
	 */
	public BigDecimal getBaseTaxInvoiced() {
		return baseTaxInvoiced;
	}

	/**
	 * Sets the baseTaxInvoiced value for this SalesFlatOrder.
	 * 
	 * @param baseTaxInvoiced
	 */
	public void setBaseTaxInvoiced(BigDecimal baseTaxInvoiced) {
		this.baseTaxInvoiced = baseTaxInvoiced;
	}

	/**
	 * Gets the baseTaxRefunded value for this SalesFlatOrder.
	 * 
	 * @return baseTaxRefunded
	 */
	public BigDecimal getBaseTaxRefunded() {
		return baseTaxRefunded;
	}

	/**
	 * Sets the baseTaxRefunded value for this SalesFlatOrder.
	 * 
	 * @param baseTaxRefunded
	 */
	public void setBaseTaxRefunded(BigDecimal baseTaxRefunded) {
		this.baseTaxRefunded = baseTaxRefunded;
	}

	/**
	 * Gets the baseToGlobalRate value for this SalesFlatOrder.
	 * 
	 * @return baseToGlobalRate
	 */
	public BigDecimal getBaseToGlobalRate() {
		return baseToGlobalRate;
	}

	/**
	 * Sets the baseToGlobalRate value for this SalesFlatOrder.
	 * 
	 * @param baseToGlobalRate
	 */
	public void setBaseToGlobalRate(BigDecimal baseToGlobalRate) {
		this.baseToGlobalRate = baseToGlobalRate;
	}

	/**
	 * Gets the baseToOrderRate value for this SalesFlatOrder.
	 * 
	 * @return baseToOrderRate
	 */
	public BigDecimal getBaseToOrderRate() {
		return baseToOrderRate;
	}

	/**
	 * Sets the baseToOrderRate value for this SalesFlatOrder.
	 * 
	 * @param baseToOrderRate
	 */
	public void setBaseToOrderRate(BigDecimal baseToOrderRate) {
		this.baseToOrderRate = baseToOrderRate;
	}

	/**
	 * Gets the baseTotalCanceled value for this SalesFlatOrder.
	 * 
	 * @return baseTotalCanceled
	 */
	public BigDecimal getBaseTotalCanceled() {
		return baseTotalCanceled;
	}

	/**
	 * Sets the baseTotalCanceled value for this SalesFlatOrder.
	 * 
	 * @param baseTotalCanceled
	 */
	public void setBaseTotalCanceled(BigDecimal baseTotalCanceled) {
		this.baseTotalCanceled = baseTotalCanceled;
	}

	/**
	 * Gets the baseTotalDue value for this SalesFlatOrder.
	 * 
	 * @return baseTotalDue
	 */
	public BigDecimal getBaseTotalDue() {
		return baseTotalDue;
	}

	/**
	 * Sets the baseTotalDue value for this SalesFlatOrder.
	 * 
	 * @param baseTotalDue
	 */
	public void setBaseTotalDue(BigDecimal baseTotalDue) {
		this.baseTotalDue = baseTotalDue;
	}

	/**
	 * Gets the baseTotalInvoiced value for this SalesFlatOrder.
	 * 
	 * @return baseTotalInvoiced
	 */
	public BigDecimal getBaseTotalInvoiced() {
		return baseTotalInvoiced;
	}

	/**
	 * Sets the baseTotalInvoiced value for this SalesFlatOrder.
	 * 
	 * @param baseTotalInvoiced
	 */
	public void setBaseTotalInvoiced(BigDecimal baseTotalInvoiced) {
		this.baseTotalInvoiced = baseTotalInvoiced;
	}

	/**
	 * Gets the baseTotalInvoicedCost value for this SalesFlatOrder.
	 * 
	 * @return baseTotalInvoicedCost
	 */
	public BigDecimal getBaseTotalInvoicedCost() {
		return baseTotalInvoicedCost;
	}

	/**
	 * Sets the baseTotalInvoicedCost value for this SalesFlatOrder.
	 * 
	 * @param baseTotalInvoicedCost
	 */
	public void setBaseTotalInvoicedCost(BigDecimal baseTotalInvoicedCost) {
		this.baseTotalInvoicedCost = baseTotalInvoicedCost;
	}

	/**
	 * Gets the baseTotalOfflineRefunded value for this SalesFlatOrder.
	 * 
	 * @return baseTotalOfflineRefunded
	 */
	public BigDecimal getBaseTotalOfflineRefunded() {
		return baseTotalOfflineRefunded;
	}

	/**
	 * Sets the baseTotalOfflineRefunded value for this SalesFlatOrder.
	 * 
	 * @param baseTotalOfflineRefunded
	 */
	public void setBaseTotalOfflineRefunded(BigDecimal baseTotalOfflineRefunded) {
		this.baseTotalOfflineRefunded = baseTotalOfflineRefunded;
	}

	/**
	 * Gets the baseTotalOnlineRefunded value for this SalesFlatOrder.
	 * 
	 * @return baseTotalOnlineRefunded
	 */
	public BigDecimal getBaseTotalOnlineRefunded() {
		return baseTotalOnlineRefunded;
	}

	/**
	 * Sets the baseTotalOnlineRefunded value for this SalesFlatOrder.
	 * 
	 * @param baseTotalOnlineRefunded
	 */
	public void setBaseTotalOnlineRefunded(BigDecimal baseTotalOnlineRefunded) {
		this.baseTotalOnlineRefunded = baseTotalOnlineRefunded;
	}

	/**
	 * Gets the baseTotalPaid value for this SalesFlatOrder.
	 * 
	 * @return baseTotalPaid
	 */
	public BigDecimal getBaseTotalPaid() {
		return baseTotalPaid;
	}

	/**
	 * Sets the baseTotalPaid value for this SalesFlatOrder.
	 * 
	 * @param baseTotalPaid
	 */
	public void setBaseTotalPaid(BigDecimal baseTotalPaid) {
		this.baseTotalPaid = baseTotalPaid;
	}

	/**
	 * Gets the baseTotalQtyOrdered value for this SalesFlatOrder.
	 * 
	 * @return baseTotalQtyOrdered
	 */
	public BigDecimal getBaseTotalQtyOrdered() {
		return baseTotalQtyOrdered;
	}

	/**
	 * Sets the baseTotalQtyOrdered value for this SalesFlatOrder.
	 * 
	 * @param baseTotalQtyOrdered
	 */
	public void setBaseTotalQtyOrdered(BigDecimal baseTotalQtyOrdered) {
		this.baseTotalQtyOrdered = baseTotalQtyOrdered;
	}

	/**
	 * Gets the baseTotalRefunded value for this SalesFlatOrder.
	 * 
	 * @return baseTotalRefunded
	 */
	public BigDecimal getBaseTotalRefunded() {
		return baseTotalRefunded;
	}

	/**
	 * Sets the baseTotalRefunded value for this SalesFlatOrder.
	 * 
	 * @param baseTotalRefunded
	 */
	public void setBaseTotalRefunded(BigDecimal baseTotalRefunded) {
		this.baseTotalRefunded = baseTotalRefunded;
	}

	/**
	 * Gets the billingAddressId value for this SalesFlatOrder.
	 * 
	 * @return billingAddressId
	 */
	public Integer getBillingAddressId() {
		return billingAddressId;
	}

	/**
	 * Sets the billingAddressId value for this SalesFlatOrder.
	 * 
	 * @param billingAddressId
	 */
	public void setBillingAddressId(Integer billingAddressId) {
		this.billingAddressId = billingAddressId;
	}

	/**
	 * Gets the canShipPartially value for this SalesFlatOrder.
	 * 
	 * @return canShipPartially
	 */
	public Short getCanShipPartially() {
		return canShipPartially;
	}

	/**
	 * Sets the canShipPartially value for this SalesFlatOrder.
	 * 
	 * @param canShipPartially
	 */
	public void setCanShipPartially(Short canShipPartially) {
		this.canShipPartially = canShipPartially;
	}

	/**
	 * Gets the canShipPartiallyItem value for this SalesFlatOrder.
	 * 
	 * @return canShipPartiallyItem
	 */
	public Short getCanShipPartiallyItem() {
		return canShipPartiallyItem;
	}

	/**
	 * Sets the canShipPartiallyItem value for this SalesFlatOrder.
	 * 
	 * @param canShipPartiallyItem
	 */
	public void setCanShipPartiallyItem(Short canShipPartiallyItem) {
		this.canShipPartiallyItem = canShipPartiallyItem;
	}

	/**
	 * Gets the couponCode value for this SalesFlatOrder.
	 * 
	 * @return couponCode
	 */
	public String getCouponCode() {
		return couponCode;
	}

	/**
	 * Sets the couponCode value for this SalesFlatOrder.
	 * 
	 * @param couponCode
	 */
	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	/**
	 * Gets the couponRuleName value for this SalesFlatOrder.
	 * 
	 * @return couponRuleName
	 */
	public String getCouponRuleName() {
		return couponRuleName;
	}

	/**
	 * Sets the couponRuleName value for this SalesFlatOrder.
	 * 
	 * @param couponRuleName
	 */
	public void setCouponRuleName(String couponRuleName) {
		this.couponRuleName = couponRuleName;
	}

	/**
	 * Gets the customerEmail value for this SalesFlatOrder.
	 * 
	 * @return customerEmail
	 */
	public String getCustomerEmail() {
		return customerEmail;
	}

	/**
	 * Sets the customerEmail value for this SalesFlatOrder.
	 * 
	 * @param customerEmail
	 */
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	/**
	 * Gets the customerFirstname value for this SalesFlatOrder.
	 * 
	 * @return customerFirstname
	 */
	public String getCustomerFirstname() {
		return customerFirstname;
	}

	/**
	 * Sets the customerFirstname value for this SalesFlatOrder.
	 * 
	 * @param customerFirstname
	 */
	public void setCustomerFirstname(String customerFirstname) {
		this.customerFirstname = customerFirstname;
	}

	/**
	 * Gets the customerGender value for this SalesFlatOrder.
	 * 
	 * @return customerGender
	 */
	public Integer getCustomerGender() {
		return customerGender;
	}

	/**
	 * Sets the customerGender value for this SalesFlatOrder.
	 * 
	 * @param customerGender
	 */
	public void setCustomerGender(Integer customerGender) {
		this.customerGender = customerGender;
	}

	/**
	 * Gets the customerGroupId value for this SalesFlatOrder.
	 * 
	 * @return customerGroupId
	 */
	public Short getCustomerGroupId() {
		return customerGroupId;
	}

	/**
	 * Sets the customerGroupId value for this SalesFlatOrder.
	 * 
	 * @param customerGroupId
	 */
	public void setCustomerGroupId(Short customerGroupId) {
		this.customerGroupId = customerGroupId;
	}

	/**
	 * Gets the customerId value for this SalesFlatOrder.
	 * 
	 * @return customerId
	 */
	public Integer getCustomerId() {
		return customerId;
	}

	/**
	 * Sets the customerId value for this SalesFlatOrder.
	 * 
	 * @param customerId
	 */
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	/**
	 * Gets the customerIsGuest value for this SalesFlatOrder.
	 * 
	 * @return customerIsGuest
	 */
	public Short getCustomerIsGuest() {
		return customerIsGuest;
	}

	/**
	 * Sets the customerIsGuest value for this SalesFlatOrder.
	 * 
	 * @param customerIsGuest
	 */
	public void setCustomerIsGuest(Short customerIsGuest) {
		this.customerIsGuest = customerIsGuest;
	}

	/**
	 * Gets the customerLastname value for this SalesFlatOrder.
	 * 
	 * @return customerLastname
	 */
	public String getCustomerLastname() {
		return customerLastname;
	}

	/**
	 * Sets the customerLastname value for this SalesFlatOrder.
	 * 
	 * @param customerLastname
	 */
	public void setCustomerLastname(String customerLastname) {
		this.customerLastname = customerLastname;
	}

	/**
	 * Gets the customerMiddlename value for this SalesFlatOrder.
	 * 
	 * @return customerMiddlename
	 */
	public String getCustomerMiddlename() {
		return customerMiddlename;
	}

	/**
	 * Sets the customerMiddlename value for this SalesFlatOrder.
	 * 
	 * @param customerMiddlename
	 */
	public void setCustomerMiddlename(String customerMiddlename) {
		this.customerMiddlename = customerMiddlename;
	}

	/**
	 * Gets the customerNote value for this SalesFlatOrder.
	 * 
	 * @return customerNote
	 */
	public String getCustomerNote() {
		return customerNote;
	}

	/**
	 * Sets the customerNote value for this SalesFlatOrder.
	 * 
	 * @param customerNote
	 */
	public void setCustomerNote(String customerNote) {
		this.customerNote = customerNote;
	}

	/**
	 * Gets the customerNoteNotify value for this SalesFlatOrder.
	 * 
	 * @return customerNoteNotify
	 */
	public Short getCustomerNoteNotify() {
		return customerNoteNotify;
	}

	/**
	 * Sets the customerNoteNotify value for this SalesFlatOrder.
	 * 
	 * @param customerNoteNotify
	 */
	public void setCustomerNoteNotify(Short customerNoteNotify) {
		this.customerNoteNotify = customerNoteNotify;
	}

	/**
	 * Gets the customerPrefix value for this SalesFlatOrder.
	 * 
	 * @return customerPrefix
	 */
	public String getCustomerPrefix() {
		return customerPrefix;
	}

	/**
	 * Sets the customerPrefix value for this SalesFlatOrder.
	 * 
	 * @param customerPrefix
	 */
	public void setCustomerPrefix(String customerPrefix) {
		this.customerPrefix = customerPrefix;
	}

	/**
	 * Gets the customerSuffix value for this SalesFlatOrder.
	 * 
	 * @return customerSuffix
	 */
	public String getCustomerSuffix() {
		return customerSuffix;
	}

	/**
	 * Sets the customerSuffix value for this SalesFlatOrder.
	 * 
	 * @param customerSuffix
	 */
	public void setCustomerSuffix(String customerSuffix) {
		this.customerSuffix = customerSuffix;
	}

	/**
	 * Gets the customerTaxvat value for this SalesFlatOrder.
	 * 
	 * @return customerTaxvat
	 */
	public String getCustomerTaxvat() {
		return customerTaxvat;
	}

	/**
	 * Sets the customerTaxvat value for this SalesFlatOrder.
	 * 
	 * @param customerTaxvat
	 */
	public void setCustomerTaxvat(String customerTaxvat) {
		this.customerTaxvat = customerTaxvat;
	}

	/**
	 * Gets the discountAmount value for this SalesFlatOrder.
	 * 
	 * @return discountAmount
	 */
	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	/**
	 * Sets the discountAmount value for this SalesFlatOrder.
	 * 
	 * @param discountAmount
	 */
	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	/**
	 * Gets the discountCanceled value for this SalesFlatOrder.
	 * 
	 * @return discountCanceled
	 */
	public BigDecimal getDiscountCanceled() {
		return discountCanceled;
	}

	/**
	 * Sets the discountCanceled value for this SalesFlatOrder.
	 * 
	 * @param discountCanceled
	 */
	public void setDiscountCanceled(BigDecimal discountCanceled) {
		this.discountCanceled = discountCanceled;
	}

	/**
	 * Gets the discountDescription value for this SalesFlatOrder.
	 * 
	 * @return discountDescription
	 */
	public String getDiscountDescription() {
		return discountDescription;
	}

	/**
	 * Sets the discountDescription value for this SalesFlatOrder.
	 * 
	 * @param discountDescription
	 */
	public void setDiscountDescription(String discountDescription) {
		this.discountDescription = discountDescription;
	}

	/**
	 * Gets the discountInvoiced value for this SalesFlatOrder.
	 * 
	 * @return discountInvoiced
	 */
	public BigDecimal getDiscountInvoiced() {
		return discountInvoiced;
	}

	/**
	 * Sets the discountInvoiced value for this SalesFlatOrder.
	 * 
	 * @param discountInvoiced
	 */
	public void setDiscountInvoiced(BigDecimal discountInvoiced) {
		this.discountInvoiced = discountInvoiced;
	}

	/**
	 * Gets the discountRefunded value for this SalesFlatOrder.
	 * 
	 * @return discountRefunded
	 */
	public BigDecimal getDiscountRefunded() {
		return discountRefunded;
	}

	/**
	 * Sets the discountRefunded value for this SalesFlatOrder.
	 * 
	 * @param discountRefunded
	 */
	public void setDiscountRefunded(BigDecimal discountRefunded) {
		this.discountRefunded = discountRefunded;
	}

	/**
	 * Gets the editIncrement value for this SalesFlatOrder.
	 * 
	 * @return editIncrement
	 */
	public Integer getEditIncrement() {
		return editIncrement;
	}

	/**
	 * Sets the editIncrement value for this SalesFlatOrder.
	 * 
	 * @param editIncrement
	 */
	public void setEditIncrement(Integer editIncrement) {
		this.editIncrement = editIncrement;
	}

	/**
	 * Gets the emailSent value for this SalesFlatOrder.
	 * 
	 * @return emailSent
	 */
	public Short getEmailSent() {
		return emailSent;
	}

	/**
	 * Sets the emailSent value for this SalesFlatOrder.
	 * 
	 * @param emailSent
	 */
	public void setEmailSent(Short emailSent) {
		this.emailSent = emailSent;
	}

	/**
	 * Gets the entityId value for this SalesFlatOrder.
	 * 
	 * @return entityId
	 */
	public Integer getEntityId() {
		return entityId;
	}

	/**
	 * Sets the entityId value for this SalesFlatOrder.
	 * 
	 * @param entityId
	 */
	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}

	/**
	 * Gets the extCustomerId value for this SalesFlatOrder.
	 * 
	 * @return extCustomerId
	 */
	public String getExtCustomerId() {
		return extCustomerId;
	}

	/**
	 * Sets the extCustomerId value for this SalesFlatOrder.
	 * 
	 * @param extCustomerId
	 */
	public void setExtCustomerId(String extCustomerId) {
		this.extCustomerId = extCustomerId;
	}

	/**
	 * Gets the extOrderId value for this SalesFlatOrder.
	 * 
	 * @return extOrderId
	 */
	public String getExtOrderId() {
		return extOrderId;
	}

	/**
	 * Sets the extOrderId value for this SalesFlatOrder.
	 * 
	 * @param extOrderId
	 */
	public void setExtOrderId(String extOrderId) {
		this.extOrderId = extOrderId;
	}

	/**
	 * Gets the forcedShipmentWithInvoice value for this SalesFlatOrder.
	 * 
	 * @return forcedShipmentWithInvoice
	 */
	public Short getForcedShipmentWithInvoice() {
		return forcedShipmentWithInvoice;
	}

	/**
	 * Sets the forcedShipmentWithInvoice value for this SalesFlatOrder.
	 * 
	 * @param forcedShipmentWithInvoice
	 */
	public void setForcedShipmentWithInvoice(Short forcedShipmentWithInvoice) {
		this.forcedShipmentWithInvoice = forcedShipmentWithInvoice;
	}

	/**
	 * Gets the giftMessageId value for this SalesFlatOrder.
	 * 
	 * @return giftMessageId
	 */
	public Integer getGiftMessageId() {
		return giftMessageId;
	}

	/**
	 * Sets the giftMessageId value for this SalesFlatOrder.
	 * 
	 * @param giftMessageId
	 */
	public void setGiftMessageId(Integer giftMessageId) {
		this.giftMessageId = giftMessageId;
	}

	/**
	 * Gets the globalCurrencyCode value for this SalesFlatOrder.
	 * 
	 * @return globalCurrencyCode
	 */
	public String getGlobalCurrencyCode() {
		return globalCurrencyCode;
	}

	/**
	 * Sets the globalCurrencyCode value for this SalesFlatOrder.
	 * 
	 * @param globalCurrencyCode
	 */
	public void setGlobalCurrencyCode(String globalCurrencyCode) {
		this.globalCurrencyCode = globalCurrencyCode;
	}

	/**
	 * Gets the grandTotal value for this SalesFlatOrder.
	 * 
	 * @return grandTotal
	 */
	public BigDecimal getGrandTotal() {
		return grandTotal;
	}

	/**
	 * Sets the grandTotal value for this SalesFlatOrder.
	 * 
	 * @param grandTotal
	 */
	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}

	/**
	 * Gets the hiddenTaxAmount value for this SalesFlatOrder.
	 * 
	 * @return hiddenTaxAmount
	 */
	public BigDecimal getHiddenTaxAmount() {
		return hiddenTaxAmount;
	}

	/**
	 * Sets the hiddenTaxAmount value for this SalesFlatOrder.
	 * 
	 * @param hiddenTaxAmount
	 */
	public void setHiddenTaxAmount(BigDecimal hiddenTaxAmount) {
		this.hiddenTaxAmount = hiddenTaxAmount;
	}

	/**
	 * Gets the hiddenTaxInvoiced value for this SalesFlatOrder.
	 * 
	 * @return hiddenTaxInvoiced
	 */
	public BigDecimal getHiddenTaxInvoiced() {
		return hiddenTaxInvoiced;
	}

	/**
	 * Sets the hiddenTaxInvoiced value for this SalesFlatOrder.
	 * 
	 * @param hiddenTaxInvoiced
	 */
	public void setHiddenTaxInvoiced(BigDecimal hiddenTaxInvoiced) {
		this.hiddenTaxInvoiced = hiddenTaxInvoiced;
	}

	/**
	 * Gets the hiddenTaxRefunded value for this SalesFlatOrder.
	 * 
	 * @return hiddenTaxRefunded
	 */
	public BigDecimal getHiddenTaxRefunded() {
		return hiddenTaxRefunded;
	}

	/**
	 * Sets the hiddenTaxRefunded value for this SalesFlatOrder.
	 * 
	 * @param hiddenTaxRefunded
	 */
	public void setHiddenTaxRefunded(BigDecimal hiddenTaxRefunded) {
		this.hiddenTaxRefunded = hiddenTaxRefunded;
	}

	/**
	 * Gets the holdBeforeState value for this SalesFlatOrder.
	 * 
	 * @return holdBeforeState
	 */
	public String getHoldBeforeState() {
		return holdBeforeState;
	}

	/**
	 * Sets the holdBeforeState value for this SalesFlatOrder.
	 * 
	 * @param holdBeforeState
	 */
	public void setHoldBeforeState(String holdBeforeState) {
		this.holdBeforeState = holdBeforeState;
	}

	/**
	 * Gets the holdBeforeStatus value for this SalesFlatOrder.
	 * 
	 * @return holdBeforeStatus
	 */
	public String getHoldBeforeStatus() {
		return holdBeforeStatus;
	}

	/**
	 * Sets the holdBeforeStatus value for this SalesFlatOrder.
	 * 
	 * @param holdBeforeStatus
	 */
	public void setHoldBeforeStatus(String holdBeforeStatus) {
		this.holdBeforeStatus = holdBeforeStatus;
	}

	/**
	 * Gets the incrementId value for this SalesFlatOrder.
	 * 
	 * @return incrementId
	 */
	public String getIncrementId() {
		return incrementId;
	}

	/**
	 * Sets the incrementId value for this SalesFlatOrder.
	 * 
	 * @param incrementId
	 */
	public void setIncrementId(String incrementId) {
		this.incrementId = incrementId;
	}

	/**
	 * Gets the isVirtual value for this SalesFlatOrder.
	 * 
	 * @return isVirtual
	 */
	public Short getIsVirtual() {
		return isVirtual;
	}

	/**
	 * Sets the isVirtual value for this SalesFlatOrder.
	 * 
	 * @param isVirtual
	 */
	public void setIsVirtual(Short isVirtual) {
		this.isVirtual = isVirtual;
	}

	/**
	 * Gets the orderCurrencyCode value for this SalesFlatOrder.
	 * 
	 * @return orderCurrencyCode
	 */
	public String getOrderCurrencyCode() {
		return orderCurrencyCode;
	}

	/**
	 * Sets the orderCurrencyCode value for this SalesFlatOrder.
	 * 
	 * @param orderCurrencyCode
	 */
	public void setOrderCurrencyCode(String orderCurrencyCode) {
		this.orderCurrencyCode = orderCurrencyCode;
	}

	/**
	 * Gets the originalIncrementId value for this SalesFlatOrder.
	 * 
	 * @return originalIncrementId
	 */
	public String getOriginalIncrementId() {
		return originalIncrementId;
	}

	/**
	 * Sets the originalIncrementId value for this SalesFlatOrder.
	 * 
	 * @param originalIncrementId
	 */
	public void setOriginalIncrementId(String originalIncrementId) {
		this.originalIncrementId = originalIncrementId;
	}

	/**
	 * Gets the paymentAuthExpiration value for this SalesFlatOrder.
	 * 
	 * @return paymentAuthExpiration
	 */
	public Integer getPaymentAuthExpiration() {
		return paymentAuthExpiration;
	}

	/**
	 * Sets the paymentAuthExpiration value for this SalesFlatOrder.
	 * 
	 * @param paymentAuthExpiration
	 */
	public void setPaymentAuthExpiration(Integer paymentAuthExpiration) {
		this.paymentAuthExpiration = paymentAuthExpiration;
	}

	/**
	 * Gets the paymentAuthorizationAmount value for this SalesFlatOrder.
	 * 
	 * @return paymentAuthorizationAmount
	 */
	public BigDecimal getPaymentAuthorizationAmount() {
		return paymentAuthorizationAmount;
	}

	/**
	 * Sets the paymentAuthorizationAmount value for this SalesFlatOrder.
	 * 
	 * @param paymentAuthorizationAmount
	 */
	public void setPaymentAuthorizationAmount(
			BigDecimal paymentAuthorizationAmount) {
		this.paymentAuthorizationAmount = paymentAuthorizationAmount;
	}

	/**
	 * Gets the paypalIpnCustomerNotified value for this SalesFlatOrder.
	 * 
	 * @return paypalIpnCustomerNotified
	 */
	public Integer getPaypalIpnCustomerNotified() {
		return paypalIpnCustomerNotified;
	}

	/**
	 * Sets the paypalIpnCustomerNotified value for this SalesFlatOrder.
	 * 
	 * @param paypalIpnCustomerNotified
	 */
	public void setPaypalIpnCustomerNotified(Integer paypalIpnCustomerNotified) {
		this.paypalIpnCustomerNotified = paypalIpnCustomerNotified;
	}

	/**
	 * Gets the pendingTlog value for this SalesFlatOrder.
	 * 
	 * @return pendingTlog
	 */
	public Integer getPendingTlog() {
		return pendingTlog;
	}

	/**
	 * Sets the pendingTlog value for this SalesFlatOrder.
	 * 
	 * @param pendingTlog
	 */
	public void setPendingTlog(Integer pendingTlog) {
		this.pendingTlog = pendingTlog;
	}

	/**
	 * Gets the protectCode value for this SalesFlatOrder.
	 * 
	 * @return protectCode
	 */
	public String getProtectCode() {
		return protectCode;
	}

	/**
	 * Sets the protectCode value for this SalesFlatOrder.
	 * 
	 * @param protectCode
	 */
	public void setProtectCode(String protectCode) {
		this.protectCode = protectCode;
	}

	/**
	 * Gets the quoteAddressId value for this SalesFlatOrder.
	 * 
	 * @return quoteAddressId
	 */
	public Integer getQuoteAddressId() {
		return quoteAddressId;
	}

	/**
	 * Sets the quoteAddressId value for this SalesFlatOrder.
	 * 
	 * @param quoteAddressId
	 */
	public void setQuoteAddressId(Integer quoteAddressId) {
		this.quoteAddressId = quoteAddressId;
	}

	/**
	 * Gets the quoteId value for this SalesFlatOrder.
	 * 
	 * @return quoteId
	 */
	public Integer getQuoteId() {
		return quoteId;
	}

	/**
	 * Sets the quoteId value for this SalesFlatOrder.
	 * 
	 * @param quoteId
	 */
	public void setQuoteId(Integer quoteId) {
		this.quoteId = quoteId;
	}

	/**
	 * Gets the relationChildId value for this SalesFlatOrder.
	 * 
	 * @return relationChildId
	 */
	public String getRelationChildId() {
		return relationChildId;
	}

	/**
	 * Sets the relationChildId value for this SalesFlatOrder.
	 * 
	 * @param relationChildId
	 */
	public void setRelationChildId(String relationChildId) {
		this.relationChildId = relationChildId;
	}

	/**
	 * Gets the relationChildRealId value for this SalesFlatOrder.
	 * 
	 * @return relationChildRealId
	 */
	public String getRelationChildRealId() {
		return relationChildRealId;
	}

	/**
	 * Sets the relationChildRealId value for this SalesFlatOrder.
	 * 
	 * @param relationChildRealId
	 */
	public void setRelationChildRealId(String relationChildRealId) {
		this.relationChildRealId = relationChildRealId;
	}

	/**
	 * Gets the relationParentId value for this SalesFlatOrder.
	 * 
	 * @return relationParentId
	 */
	public String getRelationParentId() {
		return relationParentId;
	}

	/**
	 * Sets the relationParentId value for this SalesFlatOrder.
	 * 
	 * @param relationParentId
	 */
	public void setRelationParentId(String relationParentId) {
		this.relationParentId = relationParentId;
	}

	/**
	 * Gets the relationParentRealId value for this SalesFlatOrder.
	 * 
	 * @return relationParentRealId
	 */
	public String getRelationParentRealId() {
		return relationParentRealId;
	}

	/**
	 * Sets the relationParentRealId value for this SalesFlatOrder.
	 * 
	 * @param relationParentRealId
	 */
	public void setRelationParentRealId(String relationParentRealId) {
		this.relationParentRealId = relationParentRealId;
	}

	/**
	 * Gets the remoteIp value for this SalesFlatOrder.
	 * 
	 * @return remoteIp
	 */
	public String getRemoteIp() {
		return remoteIp;
	}

	/**
	 * Sets the remoteIp value for this SalesFlatOrder.
	 * 
	 * @param remoteIp
	 */
	public void setRemoteIp(String remoteIp) {
		this.remoteIp = remoteIp;
	}

	/**
	 * Gets the shippingAddressId value for this SalesFlatOrder.
	 * 
	 * @return shippingAddressId
	 */
	public Integer getShippingAddressId() {
		return shippingAddressId;
	}

	/**
	 * Sets the shippingAddressId value for this SalesFlatOrder.
	 * 
	 * @param shippingAddressId
	 */
	public void setShippingAddressId(Integer shippingAddressId) {
		this.shippingAddressId = shippingAddressId;
	}

	/**
	 * Gets the shippingAmount value for this SalesFlatOrder.
	 * 
	 * @return shippingAmount
	 */
	public BigDecimal getShippingAmount() {
		return shippingAmount;
	}

	/**
	 * Sets the shippingAmount value for this SalesFlatOrder.
	 * 
	 * @param shippingAmount
	 */
	public void setShippingAmount(BigDecimal shippingAmount) {
		this.shippingAmount = shippingAmount;
	}

	/**
	 * Gets the shippingCanceled value for this SalesFlatOrder.
	 * 
	 * @return shippingCanceled
	 */
	public BigDecimal getShippingCanceled() {
		return shippingCanceled;
	}

	/**
	 * Sets the shippingCanceled value for this SalesFlatOrder.
	 * 
	 * @param shippingCanceled
	 */
	public void setShippingCanceled(BigDecimal shippingCanceled) {
		this.shippingCanceled = shippingCanceled;
	}

	/**
	 * Gets the shippingDescription value for this SalesFlatOrder.
	 * 
	 * @return shippingDescription
	 */
	public String getShippingDescription() {
		return shippingDescription;
	}

	/**
	 * Sets the shippingDescription value for this SalesFlatOrder.
	 * 
	 * @param shippingDescription
	 */
	public void setShippingDescription(String shippingDescription) {
		this.shippingDescription = shippingDescription;
	}

	/**
	 * Gets the shippingDiscountAmount value for this SalesFlatOrder.
	 * 
	 * @return shippingDiscountAmount
	 */
	public BigDecimal getShippingDiscountAmount() {
		return shippingDiscountAmount;
	}

	/**
	 * Sets the shippingDiscountAmount value for this SalesFlatOrder.
	 * 
	 * @param shippingDiscountAmount
	 */
	public void setShippingDiscountAmount(BigDecimal shippingDiscountAmount) {
		this.shippingDiscountAmount = shippingDiscountAmount;
	}

	/**
	 * Gets the shippingHiddenTaxAmount value for this SalesFlatOrder.
	 * 
	 * @return shippingHiddenTaxAmount
	 */
	public BigDecimal getShippingHiddenTaxAmount() {
		return shippingHiddenTaxAmount;
	}

	/**
	 * Sets the shippingHiddenTaxAmount value for this SalesFlatOrder.
	 * 
	 * @param shippingHiddenTaxAmount
	 */
	public void setShippingHiddenTaxAmount(BigDecimal shippingHiddenTaxAmount) {
		this.shippingHiddenTaxAmount = shippingHiddenTaxAmount;
	}

	/**
	 * Gets the shippingInclTax value for this SalesFlatOrder.
	 * 
	 * @return shippingInclTax
	 */
	public BigDecimal getShippingInclTax() {
		return shippingInclTax;
	}

	/**
	 * Sets the shippingInclTax value for this SalesFlatOrder.
	 * 
	 * @param shippingInclTax
	 */
	public void setShippingInclTax(BigDecimal shippingInclTax) {
		this.shippingInclTax = shippingInclTax;
	}

	/**
	 * Gets the shippingInvoiced value for this SalesFlatOrder.
	 * 
	 * @return shippingInvoiced
	 */
	public BigDecimal getShippingInvoiced() {
		return shippingInvoiced;
	}

	/**
	 * Sets the shippingInvoiced value for this SalesFlatOrder.
	 * 
	 * @param shippingInvoiced
	 */
	public void setShippingInvoiced(BigDecimal shippingInvoiced) {
		this.shippingInvoiced = shippingInvoiced;
	}

	/**
	 * Gets the shippingMethod value for this SalesFlatOrder.
	 * 
	 * @return shippingMethod
	 */
	public String getShippingMethod() {
		return shippingMethod;
	}

	/**
	 * Sets the shippingMethod value for this SalesFlatOrder.
	 * 
	 * @param shippingMethod
	 */
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	/**
	 * Gets the shippingRefunded value for this SalesFlatOrder.
	 * 
	 * @return shippingRefunded
	 */
	public BigDecimal getShippingRefunded() {
		return shippingRefunded;
	}

	/**
	 * Sets the shippingRefunded value for this SalesFlatOrder.
	 * 
	 * @param shippingRefunded
	 */
	public void setShippingRefunded(BigDecimal shippingRefunded) {
		this.shippingRefunded = shippingRefunded;
	}

	/**
	 * Gets the shippingTaxAmount value for this SalesFlatOrder.
	 * 
	 * @return shippingTaxAmount
	 */
	public BigDecimal getShippingTaxAmount() {
		return shippingTaxAmount;
	}

	/**
	 * Sets the shippingTaxAmount value for this SalesFlatOrder.
	 * 
	 * @param shippingTaxAmount
	 */
	public void setShippingTaxAmount(BigDecimal shippingTaxAmount) {
		this.shippingTaxAmount = shippingTaxAmount;
	}

	/**
	 * Gets the shippingTaxRefunded value for this SalesFlatOrder.
	 * 
	 * @return shippingTaxRefunded
	 */
	public BigDecimal getShippingTaxRefunded() {
		return shippingTaxRefunded;
	}

	/**
	 * Sets the shippingTaxRefunded value for this SalesFlatOrder.
	 * 
	 * @param shippingTaxRefunded
	 */
	public void setShippingTaxRefunded(BigDecimal shippingTaxRefunded) {
		this.shippingTaxRefunded = shippingTaxRefunded;
	}

	/**
	 * Gets the state value for this SalesFlatOrder.
	 * 
	 * @return state
	 */
	public String getState() {
		return state;
	}

	/**
	 * Sets the state value for this SalesFlatOrder.
	 * 
	 * @param state
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Gets the status value for this SalesFlatOrder.
	 * 
	 * @return status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status value for this SalesFlatOrder.
	 * 
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the storeCurrencyCode value for this SalesFlatOrder.
	 * 
	 * @return storeCurrencyCode
	 */
	public String getStoreCurrencyCode() {
		return storeCurrencyCode;
	}

	/**
	 * Sets the storeCurrencyCode value for this SalesFlatOrder.
	 * 
	 * @param storeCurrencyCode
	 */
	public void setStoreCurrencyCode(String storeCurrencyCode) {
		this.storeCurrencyCode = storeCurrencyCode;
	}

	/**
	 * Gets the storeId value for this SalesFlatOrder.
	 * 
	 * @return storeId
	 */
	public Short getStoreId() {
		return storeId;
	}

	/**
	 * Sets the storeId value for this SalesFlatOrder.
	 * 
	 * @param storeId
	 */
	public void setStoreId(Short storeId) {
		this.storeId = storeId;
	}

	/**
	 * Gets the storeName value for this SalesFlatOrder.
	 * 
	 * @return storeName
	 */
	public String getStoreName() {
		return storeName;
	}

	/**
	 * Sets the storeName value for this SalesFlatOrder.
	 * 
	 * @param storeName
	 */
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	/**
	 * Gets the storeToBaseRate value for this SalesFlatOrder.
	 * 
	 * @return storeToBaseRate
	 */
	public BigDecimal getStoreToBaseRate() {
		return storeToBaseRate;
	}

	/**
	 * Sets the storeToBaseRate value for this SalesFlatOrder.
	 * 
	 * @param storeToBaseRate
	 */
	public void setStoreToBaseRate(BigDecimal storeToBaseRate) {
		this.storeToBaseRate = storeToBaseRate;
	}

	/**
	 * Gets the storeToOrderRate value for this SalesFlatOrder.
	 * 
	 * @return storeToOrderRate
	 */
	public BigDecimal getStoreToOrderRate() {
		return storeToOrderRate;
	}

	/**
	 * Sets the storeToOrderRate value for this SalesFlatOrder.
	 * 
	 * @param storeToOrderRate
	 */
	public void setStoreToOrderRate(BigDecimal storeToOrderRate) {
		this.storeToOrderRate = storeToOrderRate;
	}

	/**
	 * Gets the subtotal value for this SalesFlatOrder.
	 * 
	 * @return subtotal
	 */
	public BigDecimal getSubtotal() {
		return subtotal;
	}

	/**
	 * Sets the subtotal value for this SalesFlatOrder.
	 * 
	 * @param subtotal
	 */
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	/**
	 * Gets the subtotalCanceled value for this SalesFlatOrder.
	 * 
	 * @return subtotalCanceled
	 */
	public BigDecimal getSubtotalCanceled() {
		return subtotalCanceled;
	}

	/**
	 * Sets the subtotalCanceled value for this SalesFlatOrder.
	 * 
	 * @param subtotalCanceled
	 */
	public void setSubtotalCanceled(BigDecimal subtotalCanceled) {
		this.subtotalCanceled = subtotalCanceled;
	}

	/**
	 * Gets the subtotalInclTax value for this SalesFlatOrder.
	 * 
	 * @return subtotalInclTax
	 */
	public BigDecimal getSubtotalInclTax() {
		return subtotalInclTax;
	}

	/**
	 * Sets the subtotalInclTax value for this SalesFlatOrder.
	 * 
	 * @param subtotalInclTax
	 */
	public void setSubtotalInclTax(BigDecimal subtotalInclTax) {
		this.subtotalInclTax = subtotalInclTax;
	}

	/**
	 * Gets the subtotalInvoiced value for this SalesFlatOrder.
	 * 
	 * @return subtotalInvoiced
	 */
	public BigDecimal getSubtotalInvoiced() {
		return subtotalInvoiced;
	}

	/**
	 * Sets the subtotalInvoiced value for this SalesFlatOrder.
	 * 
	 * @param subtotalInvoiced
	 */
	public void setSubtotalInvoiced(BigDecimal subtotalInvoiced) {
		this.subtotalInvoiced = subtotalInvoiced;
	}

	/**
	 * Gets the subtotalRefunded value for this SalesFlatOrder.
	 * 
	 * @return subtotalRefunded
	 */
	public BigDecimal getSubtotalRefunded() {
		return subtotalRefunded;
	}

	/**
	 * Sets the subtotalRefunded value for this SalesFlatOrder.
	 * 
	 * @param subtotalRefunded
	 */
	public void setSubtotalRefunded(BigDecimal subtotalRefunded) {
		this.subtotalRefunded = subtotalRefunded;
	}

	/**
	 * Gets the supplierId value for this SalesFlatOrder.
	 * 
	 * @return supplierId
	 */
	public Integer getSupplierId() {
		return supplierId;
	}

	/**
	 * Sets the supplierId value for this SalesFlatOrder.
	 * 
	 * @param supplierId
	 */
	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}

	/**
	 * Gets the taxAmount value for this SalesFlatOrder.
	 * 
	 * @return taxAmount
	 */
	public BigDecimal getTaxAmount() {
		return taxAmount;
	}

	/**
	 * Sets the taxAmount value for this SalesFlatOrder.
	 * 
	 * @param taxAmount
	 */
	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	/**
	 * Gets the taxCanceled value for this SalesFlatOrder.
	 * 
	 * @return taxCanceled
	 */
	public BigDecimal getTaxCanceled() {
		return taxCanceled;
	}

	/**
	 * Sets the taxCanceled value for this SalesFlatOrder.
	 * 
	 * @param taxCanceled
	 */
	public void setTaxCanceled(BigDecimal taxCanceled) {
		this.taxCanceled = taxCanceled;
	}

	/**
	 * Gets the taxInvoiced value for this SalesFlatOrder.
	 * 
	 * @return taxInvoiced
	 */
	public BigDecimal getTaxInvoiced() {
		return taxInvoiced;
	}

	/**
	 * Sets the taxInvoiced value for this SalesFlatOrder.
	 * 
	 * @param taxInvoiced
	 */
	public void setTaxInvoiced(BigDecimal taxInvoiced) {
		this.taxInvoiced = taxInvoiced;
	}

	/**
	 * Gets the taxRefunded value for this SalesFlatOrder.
	 * 
	 * @return taxRefunded
	 */
	public BigDecimal getTaxRefunded() {
		return taxRefunded;
	}

	/**
	 * Sets the taxRefunded value for this SalesFlatOrder.
	 * 
	 * @param taxRefunded
	 */
	public void setTaxRefunded(BigDecimal taxRefunded) {
		this.taxRefunded = taxRefunded;
	}

	/**
	 * Gets the totalCanceled value for this SalesFlatOrder.
	 * 
	 * @return totalCanceled
	 */
	public BigDecimal getTotalCanceled() {
		return totalCanceled;
	}

	/**
	 * Sets the totalCanceled value for this SalesFlatOrder.
	 * 
	 * @param totalCanceled
	 */
	public void setTotalCanceled(BigDecimal totalCanceled) {
		this.totalCanceled = totalCanceled;
	}

	/**
	 * Gets the totalDue value for this SalesFlatOrder.
	 * 
	 * @return totalDue
	 */
	public BigDecimal getTotalDue() {
		return totalDue;
	}

	/**
	 * Sets the totalDue value for this SalesFlatOrder.
	 * 
	 * @param totalDue
	 */
	public void setTotalDue(BigDecimal totalDue) {
		this.totalDue = totalDue;
	}

	/**
	 * Gets the totalInvoiced value for this SalesFlatOrder.
	 * 
	 * @return totalInvoiced
	 */
	public BigDecimal getTotalInvoiced() {
		return totalInvoiced;
	}

	/**
	 * Sets the totalInvoiced value for this SalesFlatOrder.
	 * 
	 * @param totalInvoiced
	 */
	public void setTotalInvoiced(BigDecimal totalInvoiced) {
		this.totalInvoiced = totalInvoiced;
	}

	/**
	 * Gets the totalItemCount value for this SalesFlatOrder.
	 * 
	 * @return totalItemCount
	 */
	public short getTotalItemCount() {
		return totalItemCount;
	}

	/**
	 * Sets the totalItemCount value for this SalesFlatOrder.
	 * 
	 * @param totalItemCount
	 */
	public void setTotalItemCount(short totalItemCount) {
		this.totalItemCount = totalItemCount;
	}

	/**
	 * Gets the totalOfflineRefunded value for this SalesFlatOrder.
	 * 
	 * @return totalOfflineRefunded
	 */
	public BigDecimal getTotalOfflineRefunded() {
		return totalOfflineRefunded;
	}

	/**
	 * Sets the totalOfflineRefunded value for this SalesFlatOrder.
	 * 
	 * @param totalOfflineRefunded
	 */
	public void setTotalOfflineRefunded(BigDecimal totalOfflineRefunded) {
		this.totalOfflineRefunded = totalOfflineRefunded;
	}

	/**
	 * Gets the totalOnlineRefunded value for this SalesFlatOrder.
	 * 
	 * @return totalOnlineRefunded
	 */
	public BigDecimal getTotalOnlineRefunded() {
		return totalOnlineRefunded;
	}

	/**
	 * Sets the totalOnlineRefunded value for this SalesFlatOrder.
	 * 
	 * @param totalOnlineRefunded
	 */
	public void setTotalOnlineRefunded(BigDecimal totalOnlineRefunded) {
		this.totalOnlineRefunded = totalOnlineRefunded;
	}

	/**
	 * Gets the totalPaid value for this SalesFlatOrder.
	 * 
	 * @return totalPaid
	 */
	public BigDecimal getTotalPaid() {
		return totalPaid;
	}

	/**
	 * Sets the totalPaid value for this SalesFlatOrder.
	 * 
	 * @param totalPaid
	 */
	public void setTotalPaid(BigDecimal totalPaid) {
		this.totalPaid = totalPaid;
	}

	/**
	 * Gets the totalQtyOrdered value for this SalesFlatOrder.
	 * 
	 * @return totalQtyOrdered
	 */
	public BigDecimal getTotalQtyOrdered() {
		return totalQtyOrdered;
	}

	/**
	 * Sets the totalQtyOrdered value for this SalesFlatOrder.
	 * 
	 * @param totalQtyOrdered
	 */
	public void setTotalQtyOrdered(BigDecimal totalQtyOrdered) {
		this.totalQtyOrdered = totalQtyOrdered;
	}

	/**
	 * Gets the totalRefunded value for this SalesFlatOrder.
	 * 
	 * @return totalRefunded
	 */
	public BigDecimal getTotalRefunded() {
		return totalRefunded;
	}

	/**
	 * Sets the totalRefunded value for this SalesFlatOrder.
	 * 
	 * @param totalRefunded
	 */
	public void setTotalRefunded(BigDecimal totalRefunded) {
		this.totalRefunded = totalRefunded;
	}

	/**
	 * Gets the weight value for this SalesFlatOrder.
	 * 
	 * @return weight
	 */
	public BigDecimal getWeight() {
		return weight;
	}

	/**
	 * Sets the weight value for this SalesFlatOrder.
	 * 
	 * @param weight
	 */
	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

	private Object __equalsCalc = null;

	public synchronized boolean equals(Object obj) {
		if (!(obj instanceof SalesFlatOrder))
			return false;
		SalesFlatOrder other = (SalesFlatOrder) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.XForwardedFor == null && other.getXForwardedFor() == null) || (this.XForwardedFor != null && this.XForwardedFor
						.equals(other.getXForwardedFor())))
				&& ((this.adjustmentNegative == null && other
						.getAdjustmentNegative() == null) || (this.adjustmentNegative != null && this.adjustmentNegative
						.equals(other.getAdjustmentNegative())))
				&& ((this.adjustmentPositive == null && other
						.getAdjustmentPositive() == null) || (this.adjustmentPositive != null && this.adjustmentPositive
						.equals(other.getAdjustmentPositive())))
				&& ((this.appliedRuleIds == null && other.getAppliedRuleIds() == null) || (this.appliedRuleIds != null && this.appliedRuleIds
						.equals(other.getAppliedRuleIds())))
				&& ((this.baseAdjustmentNegative == null && other
						.getBaseAdjustmentNegative() == null) || (this.baseAdjustmentNegative != null && this.baseAdjustmentNegative
						.equals(other.getBaseAdjustmentNegative())))
				&& ((this.baseAdjustmentPositive == null && other
						.getBaseAdjustmentPositive() == null) || (this.baseAdjustmentPositive != null && this.baseAdjustmentPositive
						.equals(other.getBaseAdjustmentPositive())))
				&& ((this.baseCurrencyCode == null && other
						.getBaseCurrencyCode() == null) || (this.baseCurrencyCode != null && this.baseCurrencyCode
						.equals(other.getBaseCurrencyCode())))
				&& ((this.baseDiscountAmount == null && other
						.getBaseDiscountAmount() == null) || (this.baseDiscountAmount != null && this.baseDiscountAmount
						.equals(other.getBaseDiscountAmount())))
				&& ((this.baseDiscountCanceled == null && other
						.getBaseDiscountCanceled() == null) || (this.baseDiscountCanceled != null && this.baseDiscountCanceled
						.equals(other.getBaseDiscountCanceled())))
				&& ((this.baseDiscountInvoiced == null && other
						.getBaseDiscountInvoiced() == null) || (this.baseDiscountInvoiced != null && this.baseDiscountInvoiced
						.equals(other.getBaseDiscountInvoiced())))
				&& ((this.baseDiscountRefunded == null && other
						.getBaseDiscountRefunded() == null) || (this.baseDiscountRefunded != null && this.baseDiscountRefunded
						.equals(other.getBaseDiscountRefunded())))
				&& ((this.baseGrandTotal == null && other.getBaseGrandTotal() == null) || (this.baseGrandTotal != null && this.baseGrandTotal
						.equals(other.getBaseGrandTotal())))
				&& ((this.baseHiddenTaxAmount == null && other
						.getBaseHiddenTaxAmount() == null) || (this.baseHiddenTaxAmount != null && this.baseHiddenTaxAmount
						.equals(other.getBaseHiddenTaxAmount())))
				&& ((this.baseHiddenTaxInvoiced == null && other
						.getBaseHiddenTaxInvoiced() == null) || (this.baseHiddenTaxInvoiced != null && this.baseHiddenTaxInvoiced
						.equals(other.getBaseHiddenTaxInvoiced())))
				&& ((this.baseHiddenTaxRefunded == null && other
						.getBaseHiddenTaxRefunded() == null) || (this.baseHiddenTaxRefunded != null && this.baseHiddenTaxRefunded
						.equals(other.getBaseHiddenTaxRefunded())))
				&& ((this.baseShippingAmount == null && other
						.getBaseShippingAmount() == null) || (this.baseShippingAmount != null && this.baseShippingAmount
						.equals(other.getBaseShippingAmount())))
				&& ((this.baseShippingCanceled == null && other
						.getBaseShippingCanceled() == null) || (this.baseShippingCanceled != null && this.baseShippingCanceled
						.equals(other.getBaseShippingCanceled())))
				&& ((this.baseShippingDiscountAmount == null && other
						.getBaseShippingDiscountAmount() == null) || (this.baseShippingDiscountAmount != null && this.baseShippingDiscountAmount
						.equals(other.getBaseShippingDiscountAmount())))
				&& ((this.baseShippingHiddenTaxAmnt == null && other
						.getBaseShippingHiddenTaxAmnt() == null) || (this.baseShippingHiddenTaxAmnt != null && this.baseShippingHiddenTaxAmnt
						.equals(other.getBaseShippingHiddenTaxAmnt())))
				&& ((this.baseShippingInclTax == null && other
						.getBaseShippingInclTax() == null) || (this.baseShippingInclTax != null && this.baseShippingInclTax
						.equals(other.getBaseShippingInclTax())))
				&& ((this.baseShippingInvoiced == null && other
						.getBaseShippingInvoiced() == null) || (this.baseShippingInvoiced != null && this.baseShippingInvoiced
						.equals(other.getBaseShippingInvoiced())))
				&& ((this.baseShippingRefunded == null && other
						.getBaseShippingRefunded() == null) || (this.baseShippingRefunded != null && this.baseShippingRefunded
						.equals(other.getBaseShippingRefunded())))
				&& ((this.baseShippingTaxAmount == null && other
						.getBaseShippingTaxAmount() == null) || (this.baseShippingTaxAmount != null && this.baseShippingTaxAmount
						.equals(other.getBaseShippingTaxAmount())))
				&& ((this.baseShippingTaxRefunded == null && other
						.getBaseShippingTaxRefunded() == null) || (this.baseShippingTaxRefunded != null && this.baseShippingTaxRefunded
						.equals(other.getBaseShippingTaxRefunded())))
				&& ((this.baseSubtotal == null && other.getBaseSubtotal() == null) || (this.baseSubtotal != null && this.baseSubtotal
						.equals(other.getBaseSubtotal())))
				&& ((this.baseSubtotalCanceled == null && other
						.getBaseSubtotalCanceled() == null) || (this.baseSubtotalCanceled != null && this.baseSubtotalCanceled
						.equals(other.getBaseSubtotalCanceled())))
				&& ((this.baseSubtotalInclTax == null && other
						.getBaseSubtotalInclTax() == null) || (this.baseSubtotalInclTax != null && this.baseSubtotalInclTax
						.equals(other.getBaseSubtotalInclTax())))
				&& ((this.baseSubtotalInvoiced == null && other
						.getBaseSubtotalInvoiced() == null) || (this.baseSubtotalInvoiced != null && this.baseSubtotalInvoiced
						.equals(other.getBaseSubtotalInvoiced())))
				&& ((this.baseSubtotalRefunded == null && other
						.getBaseSubtotalRefunded() == null) || (this.baseSubtotalRefunded != null && this.baseSubtotalRefunded
						.equals(other.getBaseSubtotalRefunded())))
				&& ((this.baseTaxAmount == null && other.getBaseTaxAmount() == null) || (this.baseTaxAmount != null && this.baseTaxAmount
						.equals(other.getBaseTaxAmount())))
				&& ((this.baseTaxCanceled == null && other.getBaseTaxCanceled() == null) || (this.baseTaxCanceled != null && this.baseTaxCanceled
						.equals(other.getBaseTaxCanceled())))
				&& ((this.baseTaxInvoiced == null && other.getBaseTaxInvoiced() == null) || (this.baseTaxInvoiced != null && this.baseTaxInvoiced
						.equals(other.getBaseTaxInvoiced())))
				&& ((this.baseTaxRefunded == null && other.getBaseTaxRefunded() == null) || (this.baseTaxRefunded != null && this.baseTaxRefunded
						.equals(other.getBaseTaxRefunded())))
				&& ((this.baseToGlobalRate == null && other
						.getBaseToGlobalRate() == null) || (this.baseToGlobalRate != null && this.baseToGlobalRate
						.equals(other.getBaseToGlobalRate())))
				&& ((this.baseToOrderRate == null && other.getBaseToOrderRate() == null) || (this.baseToOrderRate != null && this.baseToOrderRate
						.equals(other.getBaseToOrderRate())))
				&& ((this.baseTotalCanceled == null && other
						.getBaseTotalCanceled() == null) || (this.baseTotalCanceled != null && this.baseTotalCanceled
						.equals(other.getBaseTotalCanceled())))
				&& ((this.baseTotalDue == null && other.getBaseTotalDue() == null) || (this.baseTotalDue != null && this.baseTotalDue
						.equals(other.getBaseTotalDue())))
				&& ((this.baseTotalInvoiced == null && other
						.getBaseTotalInvoiced() == null) || (this.baseTotalInvoiced != null && this.baseTotalInvoiced
						.equals(other.getBaseTotalInvoiced())))
				&& ((this.baseTotalInvoicedCost == null && other
						.getBaseTotalInvoicedCost() == null) || (this.baseTotalInvoicedCost != null && this.baseTotalInvoicedCost
						.equals(other.getBaseTotalInvoicedCost())))
				&& ((this.baseTotalOfflineRefunded == null && other
						.getBaseTotalOfflineRefunded() == null) || (this.baseTotalOfflineRefunded != null && this.baseTotalOfflineRefunded
						.equals(other.getBaseTotalOfflineRefunded())))
				&& ((this.baseTotalOnlineRefunded == null && other
						.getBaseTotalOnlineRefunded() == null) || (this.baseTotalOnlineRefunded != null && this.baseTotalOnlineRefunded
						.equals(other.getBaseTotalOnlineRefunded())))
				&& ((this.baseTotalPaid == null && other.getBaseTotalPaid() == null) || (this.baseTotalPaid != null && this.baseTotalPaid
						.equals(other.getBaseTotalPaid())))
				&& ((this.baseTotalQtyOrdered == null && other
						.getBaseTotalQtyOrdered() == null) || (this.baseTotalQtyOrdered != null && this.baseTotalQtyOrdered
						.equals(other.getBaseTotalQtyOrdered())))
				&& ((this.baseTotalRefunded == null && other
						.getBaseTotalRefunded() == null) || (this.baseTotalRefunded != null && this.baseTotalRefunded
						.equals(other.getBaseTotalRefunded())))
				&& ((this.billingAddressId == null && other
						.getBillingAddressId() == null) || (this.billingAddressId != null && this.billingAddressId
						.equals(other.getBillingAddressId())))
				&& ((this.canShipPartially == null && other
						.getCanShipPartially() == null) || (this.canShipPartially != null && this.canShipPartially
						.equals(other.getCanShipPartially())))
				&& ((this.canShipPartiallyItem == null && other
						.getCanShipPartiallyItem() == null) || (this.canShipPartiallyItem != null && this.canShipPartiallyItem
						.equals(other.getCanShipPartiallyItem())))
				&& ((this.couponCode == null && other.getCouponCode() == null) || (this.couponCode != null && this.couponCode
						.equals(other.getCouponCode())))
				&& ((this.couponRuleName == null && other.getCouponRuleName() == null) || (this.couponRuleName != null && this.couponRuleName
						.equals(other.getCouponRuleName())))
				&& ((this.customerEmail == null && other.getCustomerEmail() == null) || (this.customerEmail != null && this.customerEmail
						.equals(other.getCustomerEmail())))
				&& ((this.customerFirstname == null && other
						.getCustomerFirstname() == null) || (this.customerFirstname != null && this.customerFirstname
						.equals(other.getCustomerFirstname())))
				&& ((this.customerGender == null && other.getCustomerGender() == null) || (this.customerGender != null && this.customerGender
						.equals(other.getCustomerGender())))
				&& ((this.customerGroupId == null && other.getCustomerGroupId() == null) || (this.customerGroupId != null && this.customerGroupId
						.equals(other.getCustomerGroupId())))
				&& ((this.customerId == null && other.getCustomerId() == null) || (this.customerId != null && this.customerId
						.equals(other.getCustomerId())))
				&& ((this.customerIsGuest == null && other.getCustomerIsGuest() == null) || (this.customerIsGuest != null && this.customerIsGuest
						.equals(other.getCustomerIsGuest())))
				&& ((this.customerLastname == null && other
						.getCustomerLastname() == null) || (this.customerLastname != null && this.customerLastname
						.equals(other.getCustomerLastname())))
				&& ((this.customerMiddlename == null && other
						.getCustomerMiddlename() == null) || (this.customerMiddlename != null && this.customerMiddlename
						.equals(other.getCustomerMiddlename())))
				&& ((this.customerNote == null && other.getCustomerNote() == null) || (this.customerNote != null && this.customerNote
						.equals(other.getCustomerNote())))
				&& ((this.customerNoteNotify == null && other
						.getCustomerNoteNotify() == null) || (this.customerNoteNotify != null && this.customerNoteNotify
						.equals(other.getCustomerNoteNotify())))
				&& ((this.customerPrefix == null && other.getCustomerPrefix() == null) || (this.customerPrefix != null && this.customerPrefix
						.equals(other.getCustomerPrefix())))
				&& ((this.customerSuffix == null && other.getCustomerSuffix() == null) || (this.customerSuffix != null && this.customerSuffix
						.equals(other.getCustomerSuffix())))
				&& ((this.customerTaxvat == null && other.getCustomerTaxvat() == null) || (this.customerTaxvat != null && this.customerTaxvat
						.equals(other.getCustomerTaxvat())))
				&& ((this.discountAmount == null && other.getDiscountAmount() == null) || (this.discountAmount != null && this.discountAmount
						.equals(other.getDiscountAmount())))
				&& ((this.discountCanceled == null && other
						.getDiscountCanceled() == null) || (this.discountCanceled != null && this.discountCanceled
						.equals(other.getDiscountCanceled())))
				&& ((this.discountDescription == null && other
						.getDiscountDescription() == null) || (this.discountDescription != null && this.discountDescription
						.equals(other.getDiscountDescription())))
				&& ((this.discountInvoiced == null && other
						.getDiscountInvoiced() == null) || (this.discountInvoiced != null && this.discountInvoiced
						.equals(other.getDiscountInvoiced())))
				&& ((this.discountRefunded == null && other
						.getDiscountRefunded() == null) || (this.discountRefunded != null && this.discountRefunded
						.equals(other.getDiscountRefunded())))
				&& ((this.editIncrement == null && other.getEditIncrement() == null) || (this.editIncrement != null && this.editIncrement
						.equals(other.getEditIncrement())))
				&& ((this.emailSent == null && other.getEmailSent() == null) || (this.emailSent != null && this.emailSent
						.equals(other.getEmailSent())))
				&& ((this.entityId == null && other.getEntityId() == null) || (this.entityId != null && this.entityId
						.equals(other.getEntityId())))
				&& ((this.extCustomerId == null && other.getExtCustomerId() == null) || (this.extCustomerId != null && this.extCustomerId
						.equals(other.getExtCustomerId())))
				&& ((this.extOrderId == null && other.getExtOrderId() == null) || (this.extOrderId != null && this.extOrderId
						.equals(other.getExtOrderId())))
				&& ((this.forcedShipmentWithInvoice == null && other
						.getForcedShipmentWithInvoice() == null) || (this.forcedShipmentWithInvoice != null && this.forcedShipmentWithInvoice
						.equals(other.getForcedShipmentWithInvoice())))
				&& ((this.giftMessageId == null && other.getGiftMessageId() == null) || (this.giftMessageId != null && this.giftMessageId
						.equals(other.getGiftMessageId())))
				&& ((this.globalCurrencyCode == null && other
						.getGlobalCurrencyCode() == null) || (this.globalCurrencyCode != null && this.globalCurrencyCode
						.equals(other.getGlobalCurrencyCode())))
				&& ((this.grandTotal == null && other.getGrandTotal() == null) || (this.grandTotal != null && this.grandTotal
						.equals(other.getGrandTotal())))
				&& ((this.hiddenTaxAmount == null && other.getHiddenTaxAmount() == null) || (this.hiddenTaxAmount != null && this.hiddenTaxAmount
						.equals(other.getHiddenTaxAmount())))
				&& ((this.hiddenTaxInvoiced == null && other
						.getHiddenTaxInvoiced() == null) || (this.hiddenTaxInvoiced != null && this.hiddenTaxInvoiced
						.equals(other.getHiddenTaxInvoiced())))
				&& ((this.hiddenTaxRefunded == null && other
						.getHiddenTaxRefunded() == null) || (this.hiddenTaxRefunded != null && this.hiddenTaxRefunded
						.equals(other.getHiddenTaxRefunded())))
				&& ((this.holdBeforeState == null && other.getHoldBeforeState() == null) || (this.holdBeforeState != null && this.holdBeforeState
						.equals(other.getHoldBeforeState())))
				&& ((this.holdBeforeStatus == null && other
						.getHoldBeforeStatus() == null) || (this.holdBeforeStatus != null && this.holdBeforeStatus
						.equals(other.getHoldBeforeStatus())))
				&& ((this.incrementId == null && other.getIncrementId() == null) || (this.incrementId != null && this.incrementId
						.equals(other.getIncrementId())))
				&& ((this.isVirtual == null && other.getIsVirtual() == null) || (this.isVirtual != null && this.isVirtual
						.equals(other.getIsVirtual())))
				&& ((this.orderCurrencyCode == null && other
						.getOrderCurrencyCode() == null) || (this.orderCurrencyCode != null && this.orderCurrencyCode
						.equals(other.getOrderCurrencyCode())))
				&& ((this.originalIncrementId == null && other
						.getOriginalIncrementId() == null) || (this.originalIncrementId != null && this.originalIncrementId
						.equals(other.getOriginalIncrementId())))
				&& ((this.paymentAuthExpiration == null && other
						.getPaymentAuthExpiration() == null) || (this.paymentAuthExpiration != null && this.paymentAuthExpiration
						.equals(other.getPaymentAuthExpiration())))
				&& ((this.paymentAuthorizationAmount == null && other
						.getPaymentAuthorizationAmount() == null) || (this.paymentAuthorizationAmount != null && this.paymentAuthorizationAmount
						.equals(other.getPaymentAuthorizationAmount())))
				&& ((this.paypalIpnCustomerNotified == null && other
						.getPaypalIpnCustomerNotified() == null) || (this.paypalIpnCustomerNotified != null && this.paypalIpnCustomerNotified
						.equals(other.getPaypalIpnCustomerNotified())))
				&& ((this.pendingTlog == null && other.getPendingTlog() == null) || (this.pendingTlog != null && this.pendingTlog
						.equals(other.getPendingTlog())))
				&& ((this.protectCode == null && other.getProtectCode() == null) || (this.protectCode != null && this.protectCode
						.equals(other.getProtectCode())))
				&& ((this.quoteAddressId == null && other.getQuoteAddressId() == null) || (this.quoteAddressId != null && this.quoteAddressId
						.equals(other.getQuoteAddressId())))
				&& ((this.quoteId == null && other.getQuoteId() == null) || (this.quoteId != null && this.quoteId
						.equals(other.getQuoteId())))
				&& ((this.relationChildId == null && other.getRelationChildId() == null) || (this.relationChildId != null && this.relationChildId
						.equals(other.getRelationChildId())))
				&& ((this.relationChildRealId == null && other
						.getRelationChildRealId() == null) || (this.relationChildRealId != null && this.relationChildRealId
						.equals(other.getRelationChildRealId())))
				&& ((this.relationParentId == null && other
						.getRelationParentId() == null) || (this.relationParentId != null && this.relationParentId
						.equals(other.getRelationParentId())))
				&& ((this.relationParentRealId == null && other
						.getRelationParentRealId() == null) || (this.relationParentRealId != null && this.relationParentRealId
						.equals(other.getRelationParentRealId())))
				&& ((this.remoteIp == null && other.getRemoteIp() == null) || (this.remoteIp != null && this.remoteIp
						.equals(other.getRemoteIp())))
				&& ((this.shippingAddressId == null && other
						.getShippingAddressId() == null) || (this.shippingAddressId != null && this.shippingAddressId
						.equals(other.getShippingAddressId())))
				&& ((this.shippingAmount == null && other.getShippingAmount() == null) || (this.shippingAmount != null && this.shippingAmount
						.equals(other.getShippingAmount())))
				&& ((this.shippingCanceled == null && other
						.getShippingCanceled() == null) || (this.shippingCanceled != null && this.shippingCanceled
						.equals(other.getShippingCanceled())))
				&& ((this.shippingDescription == null && other
						.getShippingDescription() == null) || (this.shippingDescription != null && this.shippingDescription
						.equals(other.getShippingDescription())))
				&& ((this.shippingDiscountAmount == null && other
						.getShippingDiscountAmount() == null) || (this.shippingDiscountAmount != null && this.shippingDiscountAmount
						.equals(other.getShippingDiscountAmount())))
				&& ((this.shippingHiddenTaxAmount == null && other
						.getShippingHiddenTaxAmount() == null) || (this.shippingHiddenTaxAmount != null && this.shippingHiddenTaxAmount
						.equals(other.getShippingHiddenTaxAmount())))
				&& ((this.shippingInclTax == null && other.getShippingInclTax() == null) || (this.shippingInclTax != null && this.shippingInclTax
						.equals(other.getShippingInclTax())))
				&& ((this.shippingInvoiced == null && other
						.getShippingInvoiced() == null) || (this.shippingInvoiced != null && this.shippingInvoiced
						.equals(other.getShippingInvoiced())))
				&& ((this.shippingMethod == null && other.getShippingMethod() == null) || (this.shippingMethod != null && this.shippingMethod
						.equals(other.getShippingMethod())))
				&& ((this.shippingRefunded == null && other
						.getShippingRefunded() == null) || (this.shippingRefunded != null && this.shippingRefunded
						.equals(other.getShippingRefunded())))
				&& ((this.shippingTaxAmount == null && other
						.getShippingTaxAmount() == null) || (this.shippingTaxAmount != null && this.shippingTaxAmount
						.equals(other.getShippingTaxAmount())))
				&& ((this.shippingTaxRefunded == null && other
						.getShippingTaxRefunded() == null) || (this.shippingTaxRefunded != null && this.shippingTaxRefunded
						.equals(other.getShippingTaxRefunded())))
				&& ((this.state == null && other.getState() == null) || (this.state != null && this.state
						.equals(other.getState())))
				&& ((this.status == null && other.getStatus() == null) || (this.status != null && this.status
						.equals(other.getStatus())))
				&& ((this.storeCurrencyCode == null && other
						.getStoreCurrencyCode() == null) || (this.storeCurrencyCode != null && this.storeCurrencyCode
						.equals(other.getStoreCurrencyCode())))
				&& ((this.storeId == null && other.getStoreId() == null) || (this.storeId != null && this.storeId
						.equals(other.getStoreId())))
				&& ((this.storeName == null && other.getStoreName() == null) || (this.storeName != null && this.storeName
						.equals(other.getStoreName())))
				&& ((this.storeToBaseRate == null && other.getStoreToBaseRate() == null) || (this.storeToBaseRate != null && this.storeToBaseRate
						.equals(other.getStoreToBaseRate())))
				&& ((this.storeToOrderRate == null && other
						.getStoreToOrderRate() == null) || (this.storeToOrderRate != null && this.storeToOrderRate
						.equals(other.getStoreToOrderRate())))
				&& ((this.subtotal == null && other.getSubtotal() == null) || (this.subtotal != null && this.subtotal
						.equals(other.getSubtotal())))
				&& ((this.subtotalCanceled == null && other
						.getSubtotalCanceled() == null) || (this.subtotalCanceled != null && this.subtotalCanceled
						.equals(other.getSubtotalCanceled())))
				&& ((this.subtotalInclTax == null && other.getSubtotalInclTax() == null) || (this.subtotalInclTax != null && this.subtotalInclTax
						.equals(other.getSubtotalInclTax())))
				&& ((this.subtotalInvoiced == null && other
						.getSubtotalInvoiced() == null) || (this.subtotalInvoiced != null && this.subtotalInvoiced
						.equals(other.getSubtotalInvoiced())))
				&& ((this.subtotalRefunded == null && other
						.getSubtotalRefunded() == null) || (this.subtotalRefunded != null && this.subtotalRefunded
						.equals(other.getSubtotalRefunded())))
				&& ((this.supplierId == null && other.getSupplierId() == null) || (this.supplierId != null && this.supplierId
						.equals(other.getSupplierId())))
				&& ((this.taxAmount == null && other.getTaxAmount() == null) || (this.taxAmount != null && this.taxAmount
						.equals(other.getTaxAmount())))
				&& ((this.taxCanceled == null && other.getTaxCanceled() == null) || (this.taxCanceled != null && this.taxCanceled
						.equals(other.getTaxCanceled())))
				&& ((this.taxInvoiced == null && other.getTaxInvoiced() == null) || (this.taxInvoiced != null && this.taxInvoiced
						.equals(other.getTaxInvoiced())))
				&& ((this.taxRefunded == null && other.getTaxRefunded() == null) || (this.taxRefunded != null && this.taxRefunded
						.equals(other.getTaxRefunded())))
				&& ((this.totalCanceled == null && other.getTotalCanceled() == null) || (this.totalCanceled != null && this.totalCanceled
						.equals(other.getTotalCanceled())))
				&& ((this.totalDue == null && other.getTotalDue() == null) || (this.totalDue != null && this.totalDue
						.equals(other.getTotalDue())))
				&& ((this.totalInvoiced == null && other.getTotalInvoiced() == null) || (this.totalInvoiced != null && this.totalInvoiced
						.equals(other.getTotalInvoiced())))
				&& this.totalItemCount == other.getTotalItemCount()
				&& ((this.totalOfflineRefunded == null && other
						.getTotalOfflineRefunded() == null) || (this.totalOfflineRefunded != null && this.totalOfflineRefunded
						.equals(other.getTotalOfflineRefunded())))
				&& ((this.totalOnlineRefunded == null && other
						.getTotalOnlineRefunded() == null) || (this.totalOnlineRefunded != null && this.totalOnlineRefunded
						.equals(other.getTotalOnlineRefunded())))
				&& ((this.totalPaid == null && other.getTotalPaid() == null) || (this.totalPaid != null && this.totalPaid
						.equals(other.getTotalPaid())))
				&& ((this.totalQtyOrdered == null && other.getTotalQtyOrdered() == null) || (this.totalQtyOrdered != null && this.totalQtyOrdered
						.equals(other.getTotalQtyOrdered())))
				&& ((this.totalRefunded == null && other.getTotalRefunded() == null) || (this.totalRefunded != null && this.totalRefunded
						.equals(other.getTotalRefunded())))
				&& ((this.weight == null && other.getWeight() == null) || (this.weight != null && this.weight
						.equals(other.getWeight())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getXForwardedFor() != null) {
			_hashCode += getXForwardedFor().hashCode();
		}
		if (getAdjustmentNegative() != null) {
			_hashCode += getAdjustmentNegative().hashCode();
		}
		if (getAdjustmentPositive() != null) {
			_hashCode += getAdjustmentPositive().hashCode();
		}
		if (getAppliedRuleIds() != null) {
			_hashCode += getAppliedRuleIds().hashCode();
		}
		if (getBaseAdjustmentNegative() != null) {
			_hashCode += getBaseAdjustmentNegative().hashCode();
		}
		if (getBaseAdjustmentPositive() != null) {
			_hashCode += getBaseAdjustmentPositive().hashCode();
		}
		if (getBaseCurrencyCode() != null) {
			_hashCode += getBaseCurrencyCode().hashCode();
		}
		if (getBaseDiscountAmount() != null) {
			_hashCode += getBaseDiscountAmount().hashCode();
		}
		if (getBaseDiscountCanceled() != null) {
			_hashCode += getBaseDiscountCanceled().hashCode();
		}
		if (getBaseDiscountInvoiced() != null) {
			_hashCode += getBaseDiscountInvoiced().hashCode();
		}
		if (getBaseDiscountRefunded() != null) {
			_hashCode += getBaseDiscountRefunded().hashCode();
		}
		if (getBaseGrandTotal() != null) {
			_hashCode += getBaseGrandTotal().hashCode();
		}
		if (getBaseHiddenTaxAmount() != null) {
			_hashCode += getBaseHiddenTaxAmount().hashCode();
		}
		if (getBaseHiddenTaxInvoiced() != null) {
			_hashCode += getBaseHiddenTaxInvoiced().hashCode();
		}
		if (getBaseHiddenTaxRefunded() != null) {
			_hashCode += getBaseHiddenTaxRefunded().hashCode();
		}
		if (getBaseShippingAmount() != null) {
			_hashCode += getBaseShippingAmount().hashCode();
		}
		if (getBaseShippingCanceled() != null) {
			_hashCode += getBaseShippingCanceled().hashCode();
		}
		if (getBaseShippingDiscountAmount() != null) {
			_hashCode += getBaseShippingDiscountAmount().hashCode();
		}
		if (getBaseShippingHiddenTaxAmnt() != null) {
			_hashCode += getBaseShippingHiddenTaxAmnt().hashCode();
		}
		if (getBaseShippingInclTax() != null) {
			_hashCode += getBaseShippingInclTax().hashCode();
		}
		if (getBaseShippingInvoiced() != null) {
			_hashCode += getBaseShippingInvoiced().hashCode();
		}
		if (getBaseShippingRefunded() != null) {
			_hashCode += getBaseShippingRefunded().hashCode();
		}
		if (getBaseShippingTaxAmount() != null) {
			_hashCode += getBaseShippingTaxAmount().hashCode();
		}
		if (getBaseShippingTaxRefunded() != null) {
			_hashCode += getBaseShippingTaxRefunded().hashCode();
		}
		if (getBaseSubtotal() != null) {
			_hashCode += getBaseSubtotal().hashCode();
		}
		if (getBaseSubtotalCanceled() != null) {
			_hashCode += getBaseSubtotalCanceled().hashCode();
		}
		if (getBaseSubtotalInclTax() != null) {
			_hashCode += getBaseSubtotalInclTax().hashCode();
		}
		if (getBaseSubtotalInvoiced() != null) {
			_hashCode += getBaseSubtotalInvoiced().hashCode();
		}
		if (getBaseSubtotalRefunded() != null) {
			_hashCode += getBaseSubtotalRefunded().hashCode();
		}
		if (getBaseTaxAmount() != null) {
			_hashCode += getBaseTaxAmount().hashCode();
		}
		if (getBaseTaxCanceled() != null) {
			_hashCode += getBaseTaxCanceled().hashCode();
		}
		if (getBaseTaxInvoiced() != null) {
			_hashCode += getBaseTaxInvoiced().hashCode();
		}
		if (getBaseTaxRefunded() != null) {
			_hashCode += getBaseTaxRefunded().hashCode();
		}
		if (getBaseToGlobalRate() != null) {
			_hashCode += getBaseToGlobalRate().hashCode();
		}
		if (getBaseToOrderRate() != null) {
			_hashCode += getBaseToOrderRate().hashCode();
		}
		if (getBaseTotalCanceled() != null) {
			_hashCode += getBaseTotalCanceled().hashCode();
		}
		if (getBaseTotalDue() != null) {
			_hashCode += getBaseTotalDue().hashCode();
		}
		if (getBaseTotalInvoiced() != null) {
			_hashCode += getBaseTotalInvoiced().hashCode();
		}
		if (getBaseTotalInvoicedCost() != null) {
			_hashCode += getBaseTotalInvoicedCost().hashCode();
		}
		if (getBaseTotalOfflineRefunded() != null) {
			_hashCode += getBaseTotalOfflineRefunded().hashCode();
		}
		if (getBaseTotalOnlineRefunded() != null) {
			_hashCode += getBaseTotalOnlineRefunded().hashCode();
		}
		if (getBaseTotalPaid() != null) {
			_hashCode += getBaseTotalPaid().hashCode();
		}
		if (getBaseTotalQtyOrdered() != null) {
			_hashCode += getBaseTotalQtyOrdered().hashCode();
		}
		if (getBaseTotalRefunded() != null) {
			_hashCode += getBaseTotalRefunded().hashCode();
		}
		if (getBillingAddressId() != null) {
			_hashCode += getBillingAddressId().hashCode();
		}
		if (getCanShipPartially() != null) {
			_hashCode += getCanShipPartially().hashCode();
		}
		if (getCanShipPartiallyItem() != null) {
			_hashCode += getCanShipPartiallyItem().hashCode();
		}
		if (getCouponCode() != null) {
			_hashCode += getCouponCode().hashCode();
		}
		if (getCouponRuleName() != null) {
			_hashCode += getCouponRuleName().hashCode();
		}
		if (getCustomerEmail() != null) {
			_hashCode += getCustomerEmail().hashCode();
		}
		if (getCustomerFirstname() != null) {
			_hashCode += getCustomerFirstname().hashCode();
		}
		if (getCustomerGender() != null) {
			_hashCode += getCustomerGender().hashCode();
		}
		if (getCustomerGroupId() != null) {
			_hashCode += getCustomerGroupId().hashCode();
		}
		if (getCustomerId() != null) {
			_hashCode += getCustomerId().hashCode();
		}
		if (getCustomerIsGuest() != null) {
			_hashCode += getCustomerIsGuest().hashCode();
		}
		if (getCustomerLastname() != null) {
			_hashCode += getCustomerLastname().hashCode();
		}
		if (getCustomerMiddlename() != null) {
			_hashCode += getCustomerMiddlename().hashCode();
		}
		if (getCustomerNote() != null) {
			_hashCode += getCustomerNote().hashCode();
		}
		if (getCustomerNoteNotify() != null) {
			_hashCode += getCustomerNoteNotify().hashCode();
		}
		if (getCustomerPrefix() != null) {
			_hashCode += getCustomerPrefix().hashCode();
		}
		if (getCustomerSuffix() != null) {
			_hashCode += getCustomerSuffix().hashCode();
		}
		if (getCustomerTaxvat() != null) {
			_hashCode += getCustomerTaxvat().hashCode();
		}
		if (getDiscountAmount() != null) {
			_hashCode += getDiscountAmount().hashCode();
		}
		if (getDiscountCanceled() != null) {
			_hashCode += getDiscountCanceled().hashCode();
		}
		if (getDiscountDescription() != null) {
			_hashCode += getDiscountDescription().hashCode();
		}
		if (getDiscountInvoiced() != null) {
			_hashCode += getDiscountInvoiced().hashCode();
		}
		if (getDiscountRefunded() != null) {
			_hashCode += getDiscountRefunded().hashCode();
		}
		if (getEditIncrement() != null) {
			_hashCode += getEditIncrement().hashCode();
		}
		if (getEmailSent() != null) {
			_hashCode += getEmailSent().hashCode();
		}
		if (getEntityId() != null) {
			_hashCode += getEntityId().hashCode();
		}
		if (getExtCustomerId() != null) {
			_hashCode += getExtCustomerId().hashCode();
		}
		if (getExtOrderId() != null) {
			_hashCode += getExtOrderId().hashCode();
		}
		if (getForcedShipmentWithInvoice() != null) {
			_hashCode += getForcedShipmentWithInvoice().hashCode();
		}
		if (getGiftMessageId() != null) {
			_hashCode += getGiftMessageId().hashCode();
		}
		if (getGlobalCurrencyCode() != null) {
			_hashCode += getGlobalCurrencyCode().hashCode();
		}
		if (getGrandTotal() != null) {
			_hashCode += getGrandTotal().hashCode();
		}
		if (getHiddenTaxAmount() != null) {
			_hashCode += getHiddenTaxAmount().hashCode();
		}
		if (getHiddenTaxInvoiced() != null) {
			_hashCode += getHiddenTaxInvoiced().hashCode();
		}
		if (getHiddenTaxRefunded() != null) {
			_hashCode += getHiddenTaxRefunded().hashCode();
		}
		if (getHoldBeforeState() != null) {
			_hashCode += getHoldBeforeState().hashCode();
		}
		if (getHoldBeforeStatus() != null) {
			_hashCode += getHoldBeforeStatus().hashCode();
		}
		if (getIncrementId() != null) {
			_hashCode += getIncrementId().hashCode();
		}
		if (getIsVirtual() != null) {
			_hashCode += getIsVirtual().hashCode();
		}
		if (getOrderCurrencyCode() != null) {
			_hashCode += getOrderCurrencyCode().hashCode();
		}
		if (getOriginalIncrementId() != null) {
			_hashCode += getOriginalIncrementId().hashCode();
		}
		if (getPaymentAuthExpiration() != null) {
			_hashCode += getPaymentAuthExpiration().hashCode();
		}
		if (getPaymentAuthorizationAmount() != null) {
			_hashCode += getPaymentAuthorizationAmount().hashCode();
		}
		if (getPaypalIpnCustomerNotified() != null) {
			_hashCode += getPaypalIpnCustomerNotified().hashCode();
		}
		if (getPendingTlog() != null) {
			_hashCode += getPendingTlog().hashCode();
		}
		if (getProtectCode() != null) {
			_hashCode += getProtectCode().hashCode();
		}
		if (getQuoteAddressId() != null) {
			_hashCode += getQuoteAddressId().hashCode();
		}
		if (getQuoteId() != null) {
			_hashCode += getQuoteId().hashCode();
		}
		if (getRelationChildId() != null) {
			_hashCode += getRelationChildId().hashCode();
		}
		if (getRelationChildRealId() != null) {
			_hashCode += getRelationChildRealId().hashCode();
		}
		if (getRelationParentId() != null) {
			_hashCode += getRelationParentId().hashCode();
		}
		if (getRelationParentRealId() != null) {
			_hashCode += getRelationParentRealId().hashCode();
		}
		if (getRemoteIp() != null) {
			_hashCode += getRemoteIp().hashCode();
		}
		if (getShippingAddressId() != null) {
			_hashCode += getShippingAddressId().hashCode();
		}
		if (getShippingAmount() != null) {
			_hashCode += getShippingAmount().hashCode();
		}
		if (getShippingCanceled() != null) {
			_hashCode += getShippingCanceled().hashCode();
		}
		if (getShippingDescription() != null) {
			_hashCode += getShippingDescription().hashCode();
		}
		if (getShippingDiscountAmount() != null) {
			_hashCode += getShippingDiscountAmount().hashCode();
		}
		if (getShippingHiddenTaxAmount() != null) {
			_hashCode += getShippingHiddenTaxAmount().hashCode();
		}
		if (getShippingInclTax() != null) {
			_hashCode += getShippingInclTax().hashCode();
		}
		if (getShippingInvoiced() != null) {
			_hashCode += getShippingInvoiced().hashCode();
		}
		if (getShippingMethod() != null) {
			_hashCode += getShippingMethod().hashCode();
		}
		if (getShippingRefunded() != null) {
			_hashCode += getShippingRefunded().hashCode();
		}
		if (getShippingTaxAmount() != null) {
			_hashCode += getShippingTaxAmount().hashCode();
		}
		if (getShippingTaxRefunded() != null) {
			_hashCode += getShippingTaxRefunded().hashCode();
		}
		if (getState() != null) {
			_hashCode += getState().hashCode();
		}
		if (getStatus() != null) {
			_hashCode += getStatus().hashCode();
		}
		if (getStoreCurrencyCode() != null) {
			_hashCode += getStoreCurrencyCode().hashCode();
		}
		if (getStoreId() != null) {
			_hashCode += getStoreId().hashCode();
		}
		if (getStoreName() != null) {
			_hashCode += getStoreName().hashCode();
		}
		if (getStoreToBaseRate() != null) {
			_hashCode += getStoreToBaseRate().hashCode();
		}
		if (getStoreToOrderRate() != null) {
			_hashCode += getStoreToOrderRate().hashCode();
		}
		if (getSubtotal() != null) {
			_hashCode += getSubtotal().hashCode();
		}
		if (getSubtotalCanceled() != null) {
			_hashCode += getSubtotalCanceled().hashCode();
		}
		if (getSubtotalInclTax() != null) {
			_hashCode += getSubtotalInclTax().hashCode();
		}
		if (getSubtotalInvoiced() != null) {
			_hashCode += getSubtotalInvoiced().hashCode();
		}
		if (getSubtotalRefunded() != null) {
			_hashCode += getSubtotalRefunded().hashCode();
		}
		if (getSupplierId() != null) {
			_hashCode += getSupplierId().hashCode();
		}
		if (getTaxAmount() != null) {
			_hashCode += getTaxAmount().hashCode();
		}
		if (getTaxCanceled() != null) {
			_hashCode += getTaxCanceled().hashCode();
		}
		if (getTaxInvoiced() != null) {
			_hashCode += getTaxInvoiced().hashCode();
		}
		if (getTaxRefunded() != null) {
			_hashCode += getTaxRefunded().hashCode();
		}
		if (getTotalCanceled() != null) {
			_hashCode += getTotalCanceled().hashCode();
		}
		if (getTotalDue() != null) {
			_hashCode += getTotalDue().hashCode();
		}
		if (getTotalInvoiced() != null) {
			_hashCode += getTotalInvoiced().hashCode();
		}
		_hashCode += getTotalItemCount();
		if (getTotalOfflineRefunded() != null) {
			_hashCode += getTotalOfflineRefunded().hashCode();
		}
		if (getTotalOnlineRefunded() != null) {
			_hashCode += getTotalOnlineRefunded().hashCode();
		}
		if (getTotalPaid() != null) {
			_hashCode += getTotalPaid().hashCode();
		}
		if (getTotalQtyOrdered() != null) {
			_hashCode += getTotalQtyOrdered().hashCode();
		}
		if (getTotalRefunded() != null) {
			_hashCode += getTotalRefunded().hashCode();
		}
		if (getWeight() != null) {
			_hashCode += getWeight().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}
}
