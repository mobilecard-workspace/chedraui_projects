/**
 * RespuestaConsultarPedidoPromociones.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package addcel.chedrauipos.omnicanal.vo;

import java.math.BigDecimal;

public class RespuestaConsultarPedidoPromociones  implements java.io.Serializable {
    private BigDecimal montoBeneficio;

    private String numeroAutorizacion;

    private String numeroTarjeta;

    private String plazosPago;

    private String promoId;

    private String tipoEnvio;

    public RespuestaConsultarPedidoPromociones() {
    }

    public RespuestaConsultarPedidoPromociones(
           BigDecimal montoBeneficio,
           String numeroAutorizacion,
           String numeroTarjeta,
           String plazosPago,
           String promoId,
           String tipoEnvio) {
           this.montoBeneficio = montoBeneficio;
           this.numeroAutorizacion = numeroAutorizacion;
           this.numeroTarjeta = numeroTarjeta;
           this.plazosPago = plazosPago;
           this.promoId = promoId;
           this.tipoEnvio = tipoEnvio;
    }


    /**
     * Gets the montoBeneficio value for this RespuestaConsultarPedidoPromociones.
     * 
     * @return montoBeneficio
     */
    public BigDecimal getMontoBeneficio() {
        return montoBeneficio;
    }


    /**
     * Sets the montoBeneficio value for this RespuestaConsultarPedidoPromociones.
     * 
     * @param montoBeneficio
     */
    public void setMontoBeneficio(BigDecimal montoBeneficio) {
        this.montoBeneficio = montoBeneficio;
    }


    /**
     * Gets the numeroAutorizacion value for this RespuestaConsultarPedidoPromociones.
     * 
     * @return numeroAutorizacion
     */
    public String getNumeroAutorizacion() {
        return numeroAutorizacion;
    }


    /**
     * Sets the numeroAutorizacion value for this RespuestaConsultarPedidoPromociones.
     * 
     * @param numeroAutorizacion
     */
    public void setNumeroAutorizacion(String numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }


    /**
     * Gets the numeroTarjeta value for this RespuestaConsultarPedidoPromociones.
     * 
     * @return numeroTarjeta
     */
    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }


    /**
     * Sets the numeroTarjeta value for this RespuestaConsultarPedidoPromociones.
     * 
     * @param numeroTarjeta
     */
    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }


    /**
     * Gets the plazosPago value for this RespuestaConsultarPedidoPromociones.
     * 
     * @return plazosPago
     */
    public String getPlazosPago() {
        return plazosPago;
    }


    /**
     * Sets the plazosPago value for this RespuestaConsultarPedidoPromociones.
     * 
     * @param plazosPago
     */
    public void setPlazosPago(String plazosPago) {
        this.plazosPago = plazosPago;
    }


    /**
     * Gets the promoId value for this RespuestaConsultarPedidoPromociones.
     * 
     * @return promoId
     */
    public String getPromoId() {
        return promoId;
    }


    /**
     * Sets the promoId value for this RespuestaConsultarPedidoPromociones.
     * 
     * @param promoId
     */
    public void setPromoId(String promoId) {
        this.promoId = promoId;
    }


    /**
     * Gets the tipoEnvio value for this RespuestaConsultarPedidoPromociones.
     * 
     * @return tipoEnvio
     */
    public String getTipoEnvio() {
        return tipoEnvio;
    }


    /**
     * Sets the tipoEnvio value for this RespuestaConsultarPedidoPromociones.
     * 
     * @param tipoEnvio
     */
    public void setTipoEnvio(String tipoEnvio) {
        this.tipoEnvio = tipoEnvio;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof RespuestaConsultarPedidoPromociones)) return false;
        RespuestaConsultarPedidoPromociones other = (RespuestaConsultarPedidoPromociones) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.montoBeneficio==null && other.getMontoBeneficio()==null) || 
             (this.montoBeneficio!=null &&
              this.montoBeneficio.equals(other.getMontoBeneficio()))) &&
            ((this.numeroAutorizacion==null && other.getNumeroAutorizacion()==null) || 
             (this.numeroAutorizacion!=null &&
              this.numeroAutorizacion.equals(other.getNumeroAutorizacion()))) &&
            ((this.numeroTarjeta==null && other.getNumeroTarjeta()==null) || 
             (this.numeroTarjeta!=null &&
              this.numeroTarjeta.equals(other.getNumeroTarjeta()))) &&
            ((this.plazosPago==null && other.getPlazosPago()==null) || 
             (this.plazosPago!=null &&
              this.plazosPago.equals(other.getPlazosPago()))) &&
            ((this.promoId==null && other.getPromoId()==null) || 
             (this.promoId!=null &&
              this.promoId.equals(other.getPromoId()))) &&
            ((this.tipoEnvio==null && other.getTipoEnvio()==null) || 
             (this.tipoEnvio!=null &&
              this.tipoEnvio.equals(other.getTipoEnvio())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMontoBeneficio() != null) {
            _hashCode += getMontoBeneficio().hashCode();
        }
        if (getNumeroAutorizacion() != null) {
            _hashCode += getNumeroAutorizacion().hashCode();
        }
        if (getNumeroTarjeta() != null) {
            _hashCode += getNumeroTarjeta().hashCode();
        }
        if (getPlazosPago() != null) {
            _hashCode += getPlazosPago().hashCode();
        }
        if (getPromoId() != null) {
            _hashCode += getPromoId().hashCode();
        }
        if (getTipoEnvio() != null) {
            _hashCode += getTipoEnvio().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }
}
