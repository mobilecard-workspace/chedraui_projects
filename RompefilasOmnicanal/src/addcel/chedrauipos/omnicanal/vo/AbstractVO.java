package addcel.chedrauipos.omnicanal.vo;

public class AbstractVO {
	private int idError;
	private String msgError;
	
	private static AbstractVO instance;

	public AbstractVO() {
	}

	public AbstractVO(int idError, String msgError) {
		this.idError = idError;
		this.msgError = msgError;
	}
	
	public static synchronized AbstractVO getDummySuccess() {
		if (instance == null)
			instance = new AbstractVO(0, "Operación Exitosa");
		
		return instance;
	}

	public int getIdError() {
		return idError;
	}

	public void setIdError(int idError) {
		this.idError = idError;
	}

	public String getMsgError() {
		return msgError;
	}

	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
}
