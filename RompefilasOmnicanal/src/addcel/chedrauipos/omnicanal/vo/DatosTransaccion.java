package addcel.chedrauipos.omnicanal.vo;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class DatosTransaccion {

	private int indicador;
	private String orderIncrementId;
	@SerializedName("store_sap")
	private String storeSap;
	@SerializedName("cancel_items")
	private List<String> cancelItems;
	private String status;

	private static DatosTransaccion instance;

	private DatosTransaccion() {

	}

	/**
	 * Factory para construir {@code DatosTransaccion}
	 * 
	 * @param indicador
	 *            Operaci&oacute;n a ejecutar
	 * @param orderIncrementId
	 *            N&uacute;mero de pedido
	 * @param storeSap
	 *            Sucursal chedraui formato <i>0000</i>
	 * @param status
	 *            Status que se asignara al pedido
	 * @param cancelItems
	 *            Lista de upc / sku a cancelar
	 * @return {@code DatosTransaccion} inicializado.
	 */
	private static synchronized DatosTransaccion get(int indicador,
			String orderIncrementId, String storeSap, String status,
			List<String> cancelItems) {
		if (instance == null)
			instance = new DatosTransaccion();

		instance.indicador = indicador;
		instance.orderIncrementId = orderIncrementId;
		instance.storeSap = storeSap;

		if (status == null)
			instance.status = null;
		else
			instance.status = status;

		if (cancelItems == null)
			instance.cancelItems = null;
		else
			instance.cancelItems = cancelItems;

		return instance;
	}

	/**
	 * Factory para construir {@code DatosTransaccion} espec&iacute;ca para
	 * cancelaci&oacute;n parcial
	 * 
	 * @param indicador
	 *            Operaci&oacute;n a ejecutar
	 * @param orderIncrementId
	 *            N&uacute;mero de pedido
	 * @param storeSap
	 *            Sucursal chedraui formato <i>0000</i>
	 * @param cancelItems
	 *            Lista de upc / sku a cancelar
	 * @return {@code DatosTransaccion} inicializado.
	 */
	public static synchronized DatosTransaccion get(int indicador,
			String orderIncrementId, String storeSap, List<String> cancelItems) {
		return get(indicador, orderIncrementId, storeSap, null, cancelItems);
	}

	/**
	 * Factory para construir {@code DatosTransaccion} espec&iacute;ca para
	 * reverso de status
	 * 
	 * @param indicador
	 *            Operaci&oacute;n a ejecutar
	 * @param orderIncrementId
	 *            N&uacute;mero de pedido
	 * @param storeSap
	 *            Sucursal chedraui formato <i>0000</i>
	 * @param status
	 *            Status que se asignara al pedido
	 * @return {@code DatosTransaccion} inicializado.
	 */
	public static synchronized DatosTransaccion get(int indicador,
			String orderIncrementId, String storeSap, String status) {
		return get(indicador, orderIncrementId, storeSap, status, null);
	}

	/**
	 * Factory para construir {@code DatosTransaccion} funciona para obtener
	 * pedido, cambio de status y cancelaci&oacute;n de pedido
	 * 
	 * @param indicador
	 *            Operaci&oacute;n a ejecutar
	 * @param orderIncrementId
	 *            N&uacute;mero de pedido
	 * @param storeSap
	 *            Sucursal chedraui formato <i>0000</i>
	 * @return {@code DatosTransaccion} inicializado.
	 */
	public static synchronized DatosTransaccion get(int indicador,
			String orderIncrementId, String storeSap) {
		return get(indicador, orderIncrementId, storeSap, null, null);
	}

	public int getIndicador() {
		return indicador;
	}

	public void setIndicador(int indicador) {
		this.indicador = indicador;
	}

	public String getOrderIncrementId() {
		return orderIncrementId;
	}

	public void setOrderIncrementId(String orderIncrementId) {
		this.orderIncrementId = orderIncrementId;
	}

	public String getStoreSap() {
		return storeSap;
	}

	public void setStoreSap(String storeSap) {
		this.storeSap = storeSap;
	}

	public List<String> getCancelItems() {
		return cancelItems;
	}

	public void setCancelItems(List<String> cancelItems) {
		this.cancelItems = cancelItems;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
