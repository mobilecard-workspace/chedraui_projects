/**
 * RespuestaConsultarPedidoItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package addcel.chedrauipos.omnicanal.vo;

import java.math.BigDecimal;

import com.google.gson.annotations.Expose;

public class RespuestaConsultarPedidoItem implements java.io.Serializable {
	@Expose
	private String esPesable;
	@Expose
	private BigDecimal montoBeneficio;
	@Expose
	private String numeroAutorizacion;
	@Expose
	private String numeroTarjeta;
	@Expose
	private BigDecimal precio;
	@Expose
	private String promoId;
	@Expose
	private String sku;
	@Expose
	private String tiendaDestino;
	@Expose
	private String tipoEnvio;
	@Expose
	private String tipoProducto;
	@Expose
	private BigDecimal totalArticulos;

	private boolean seleccionado;
	
	private String descripcion;

	public RespuestaConsultarPedidoItem() {
	}

	public RespuestaConsultarPedidoItem(String esPesable,
			BigDecimal montoBeneficio, String numeroAutorizacion,
			String numeroTarjeta, BigDecimal precio, String promoId,
			String sku, String tiendaDestino, String tipoEnvio,
			String tipoProducto, BigDecimal totalArticulos) {
		this.esPesable = esPesable;
		this.montoBeneficio = montoBeneficio;
		this.numeroAutorizacion = numeroAutorizacion;
		this.numeroTarjeta = numeroTarjeta;
		this.precio = precio;
		this.promoId = promoId;
		this.sku = sku;
		this.tiendaDestino = tiendaDestino;
		this.tipoEnvio = tipoEnvio;
		this.tipoProducto = tipoProducto;
		this.totalArticulos = totalArticulos;
	}

	/**
	 * Gets the esPesable value for this RespuestaConsultarPedidoItem.
	 * 
	 * @return esPesable
	 */
	public String getEsPesable() {
		return esPesable;
	}

	/**
	 * Sets the esPesable value for this RespuestaConsultarPedidoItem.
	 * 
	 * @param esPesable
	 */
	public void setEsPesable(String esPesable) {
		this.esPesable = esPesable;
	}

	/**
	 * Gets the montoBeneficio value for this RespuestaConsultarPedidoItem.
	 * 
	 * @return montoBeneficio
	 */
	public BigDecimal getMontoBeneficio() {
		return montoBeneficio;
	}

	/**
	 * Sets the montoBeneficio value for this RespuestaConsultarPedidoItem.
	 * 
	 * @param montoBeneficio
	 */
	public void setMontoBeneficio(BigDecimal montoBeneficio) {
		this.montoBeneficio = montoBeneficio;
	}

	/**
	 * Gets the numeroAutorizacion value for this RespuestaConsultarPedidoItem.
	 * 
	 * @return numeroAutorizacion
	 */
	public String getNumeroAutorizacion() {
		return numeroAutorizacion;
	}

	/**
	 * Sets the numeroAutorizacion value for this RespuestaConsultarPedidoItem.
	 * 
	 * @param numeroAutorizacion
	 */
	public void setNumeroAutorizacion(String numeroAutorizacion) {
		this.numeroAutorizacion = numeroAutorizacion;
	}

	/**
	 * Gets the numeroTarjeta value for this RespuestaConsultarPedidoItem.
	 * 
	 * @return numeroTarjeta
	 */
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	/**
	 * Sets the numeroTarjeta value for this RespuestaConsultarPedidoItem.
	 * 
	 * @param numeroTarjeta
	 */
	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	/**
	 * Gets the precio value for this RespuestaConsultarPedidoItem.
	 * 
	 * @return precio
	 */
	public BigDecimal getPrecio() {
		return precio;
	}

	/**
	 * Sets the precio value for this RespuestaConsultarPedidoItem.
	 * 
	 * @param precio
	 */
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	/**
	 * Gets the promoId value for this RespuestaConsultarPedidoItem.
	 * 
	 * @return promoId
	 */
	public String getPromoId() {
		return promoId;
	}

	/**
	 * Sets the promoId value for this RespuestaConsultarPedidoItem.
	 * 
	 * @param promoId
	 */
	public void setPromoId(String promoId) {
		this.promoId = promoId;
	}

	/**
	 * Gets the sku value for this RespuestaConsultarPedidoItem.
	 * 
	 * @return sku
	 */
	public String getSku() {
		return sku;
	}

	/**
	 * Sets the sku value for this RespuestaConsultarPedidoItem.
	 * 
	 * @param sku
	 */
	public void setSku(String sku) {
		this.sku = sku;
	}

	/**
	 * Gets the tiendaDestino value for this RespuestaConsultarPedidoItem.
	 * 
	 * @return tiendaDestino
	 */
	public String getTiendaDestino() {
		return tiendaDestino;
	}

	/**
	 * Sets the tiendaDestino value for this RespuestaConsultarPedidoItem.
	 * 
	 * @param tiendaDestino
	 */
	public void setTiendaDestino(String tiendaDestino) {
		this.tiendaDestino = tiendaDestino;
	}

	/**
	 * Gets the tipoEnvio value for this RespuestaConsultarPedidoItem.
	 * 
	 * @return tipoEnvio
	 */
	public String getTipoEnvio() {
		return tipoEnvio;
	}

	/**
	 * Sets the tipoEnvio value for this RespuestaConsultarPedidoItem.
	 * 
	 * @param tipoEnvio
	 */
	public void setTipoEnvio(String tipoEnvio) {
		this.tipoEnvio = tipoEnvio;
	}

	/**
	 * Gets the tipoProducto value for this RespuestaConsultarPedidoItem.
	 * 
	 * @return tipoProducto
	 */
	public String getTipoProducto() {
		return tipoProducto;
	}

	/**
	 * Sets the tipoProducto value for this RespuestaConsultarPedidoItem.
	 * 
	 * @param tipoProducto
	 */
	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

	/**
	 * Gets the totalArticulos value for this RespuestaConsultarPedidoItem.
	 * 
	 * @return totalArticulos
	 */
	public BigDecimal getTotalArticulos() {
		return totalArticulos;
	}

	/**
	 * Sets the totalArticulos value for this RespuestaConsultarPedidoItem.
	 * 
	 * @param totalArticulos
	 */
	public void setTotalArticulos(BigDecimal totalArticulos) {
		this.totalArticulos = totalArticulos;
	}
	
	public boolean isSeleccionado() {
		return seleccionado;
	}
	
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	private Object __equalsCalc = null;

	public synchronized boolean equals(Object obj) {
		if (!(obj instanceof RespuestaConsultarPedidoItem))
			return false;
		RespuestaConsultarPedidoItem other = (RespuestaConsultarPedidoItem) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.esPesable == null && other.getEsPesable() == null) || (this.esPesable != null && this.esPesable
						.equals(other.getEsPesable())))
				&& ((this.montoBeneficio == null && other.getMontoBeneficio() == null) || (this.montoBeneficio != null && this.montoBeneficio
						.equals(other.getMontoBeneficio())))
				&& ((this.numeroAutorizacion == null && other
						.getNumeroAutorizacion() == null) || (this.numeroAutorizacion != null && this.numeroAutorizacion
						.equals(other.getNumeroAutorizacion())))
				&& ((this.numeroTarjeta == null && other.getNumeroTarjeta() == null) || (this.numeroTarjeta != null && this.numeroTarjeta
						.equals(other.getNumeroTarjeta())))
				&& ((this.precio == null && other.getPrecio() == null) || (this.precio != null && this.precio
						.equals(other.getPrecio())))
				&& ((this.promoId == null && other.getPromoId() == null) || (this.promoId != null && this.promoId
						.equals(other.getPromoId())))
				&& ((this.sku == null && other.getSku() == null) || (this.sku != null && this.sku
						.equals(other.getSku())))
				&& ((this.tiendaDestino == null && other.getTiendaDestino() == null) || (this.tiendaDestino != null && this.tiendaDestino
						.equals(other.getTiendaDestino())))
				&& ((this.tipoEnvio == null && other.getTipoEnvio() == null) || (this.tipoEnvio != null && this.tipoEnvio
						.equals(other.getTipoEnvio())))
				&& ((this.tipoProducto == null && other.getTipoProducto() == null) || (this.tipoProducto != null && this.tipoProducto
						.equals(other.getTipoProducto())))
				&& ((this.totalArticulos == null && other.getTotalArticulos() == null) || (this.totalArticulos != null && this.totalArticulos
						.equals(other.getTotalArticulos())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getEsPesable() != null) {
			_hashCode += getEsPesable().hashCode();
		}
		if (getMontoBeneficio() != null) {
			_hashCode += getMontoBeneficio().hashCode();
		}
		if (getNumeroAutorizacion() != null) {
			_hashCode += getNumeroAutorizacion().hashCode();
		}
		if (getNumeroTarjeta() != null) {
			_hashCode += getNumeroTarjeta().hashCode();
		}
		if (getPrecio() != null) {
			_hashCode += getPrecio().hashCode();
		}
		if (getPromoId() != null) {
			_hashCode += getPromoId().hashCode();
		}
		if (getSku() != null) {
			_hashCode += getSku().hashCode();
		}
		if (getTiendaDestino() != null) {
			_hashCode += getTiendaDestino().hashCode();
		}
		if (getTipoEnvio() != null) {
			_hashCode += getTipoEnvio().hashCode();
		}
		if (getTipoProducto() != null) {
			_hashCode += getTipoProducto().hashCode();
		}
		if (getTotalArticulos() != null) {
			_hashCode += getTotalArticulos().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}
}
