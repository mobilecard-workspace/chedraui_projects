/**
 * RespuestaConsultarPedido.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package addcel.chedrauipos.omnicanal.vo;

public class RespuestaConsultarPedido  implements java.io.Serializable {
    private java.lang.String accionCupon;

    private java.lang.String estado;

    private RespuestaConsultarPedidoPromociones[] itempProm;

    private RespuestaConsultarTiendasPedido[] itempTienda;

    private RespuestaConsultarPedidoItem[] itemsp;

    private java.lang.String metodoEnvio;

    private java.lang.String metodoPago;

    private java.lang.String montoCupon;

    private java.lang.String numeroCupon;

    private java.lang.String numeroPedido;

    private java.lang.String oferta;

    private SalesFlatOrder pedido;

    private java.lang.String pedidoOrigen;

    private java.lang.Short plazos;

    private java.lang.String promoType;

    private java.lang.String referencia;

    private java.lang.String respuesta;

    private java.lang.String status;

    private java.lang.String tarjeta;

    private java.lang.String tienda;

    private java.lang.String tiendaPago;

    private java.lang.String tipoCupon;

    private java.math.BigDecimal total;

    private java.math.BigDecimal totalEnvio;

    private java.lang.String xml;

    public RespuestaConsultarPedido() {
    }

    public RespuestaConsultarPedido(
           java.lang.String accionCupon,
           java.lang.String estado,
           RespuestaConsultarPedidoPromociones[] itempProm,
           RespuestaConsultarTiendasPedido[] itempTienda,
           RespuestaConsultarPedidoItem[] itemsp,
           java.lang.String metodoEnvio,
           java.lang.String metodoPago,
           java.lang.String montoCupon,
           java.lang.String numeroCupon,
           java.lang.String numeroPedido,
           java.lang.String oferta,
           SalesFlatOrder pedido,
           java.lang.String pedidoOrigen,
           java.lang.Short plazos,
           java.lang.String promoType,
           java.lang.String referencia,
           java.lang.String respuesta,
           java.lang.String status,
           java.lang.String tarjeta,
           java.lang.String tienda,
           java.lang.String tiendaPago,
           java.lang.String tipoCupon,
           java.math.BigDecimal total,
           java.math.BigDecimal totalEnvio,
           java.lang.String xml) {
           this.accionCupon = accionCupon;
           this.estado = estado;
           this.itempProm = itempProm;
           this.itempTienda = itempTienda;
           this.itemsp = itemsp;
           this.metodoEnvio = metodoEnvio;
           this.metodoPago = metodoPago;
           this.montoCupon = montoCupon;
           this.numeroCupon = numeroCupon;
           this.numeroPedido = numeroPedido;
           this.oferta = oferta;
           this.pedido = pedido;
           this.pedidoOrigen = pedidoOrigen;
           this.plazos = plazos;
           this.promoType = promoType;
           this.referencia = referencia;
           this.respuesta = respuesta;
           this.status = status;
           this.tarjeta = tarjeta;
           this.tienda = tienda;
           this.tiendaPago = tiendaPago;
           this.tipoCupon = tipoCupon;
           this.total = total;
           this.totalEnvio = totalEnvio;
           this.xml = xml;
    }


    /**
     * Gets the accionCupon value for this RespuestaConsultarPedido.
     * 
     * @return accionCupon
     */
    public java.lang.String getAccionCupon() {
        return accionCupon;
    }


    /**
     * Sets the accionCupon value for this RespuestaConsultarPedido.
     * 
     * @param accionCupon
     */
    public void setAccionCupon(java.lang.String accionCupon) {
        this.accionCupon = accionCupon;
    }


    /**
     * Gets the estado value for this RespuestaConsultarPedido.
     * 
     * @return estado
     */
    public java.lang.String getEstado() {
        return estado;
    }


    /**
     * Sets the estado value for this RespuestaConsultarPedido.
     * 
     * @param estado
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }


    /**
     * Gets the itempProm value for this RespuestaConsultarPedido.
     * 
     * @return itempProm
     */
    public RespuestaConsultarPedidoPromociones[] getItempProm() {
        return itempProm;
    }


    /**
     * Sets the itempProm value for this RespuestaConsultarPedido.
     * 
     * @param itempProm
     */
    public void setItempProm(RespuestaConsultarPedidoPromociones[] itempProm) {
        this.itempProm = itempProm;
    }


    /**
     * Gets the itempTienda value for this RespuestaConsultarPedido.
     * 
     * @return itempTienda
     */
    public RespuestaConsultarTiendasPedido[] getItempTienda() {
        return itempTienda;
    }


    /**
     * Sets the itempTienda value for this RespuestaConsultarPedido.
     * 
     * @param itempTienda
     */
    public void setItempTienda(RespuestaConsultarTiendasPedido[] itempTienda) {
        this.itempTienda = itempTienda;
    }


    /**
     * Gets the itemsp value for this RespuestaConsultarPedido.
     * 
     * @return itemsp
     */
    public RespuestaConsultarPedidoItem[] getItemsp() {
        return itemsp;
    }


    /**
     * Sets the itemsp value for this RespuestaConsultarPedido.
     * 
     * @param itemsp
     */
    public void setItemsp(RespuestaConsultarPedidoItem[] itemsp) {
        this.itemsp = itemsp;
    }


    /**
     * Gets the metodoEnvio value for this RespuestaConsultarPedido.
     * 
     * @return metodoEnvio
     */
    public java.lang.String getMetodoEnvio() {
        return metodoEnvio;
    }


    /**
     * Sets the metodoEnvio value for this RespuestaConsultarPedido.
     * 
     * @param metodoEnvio
     */
    public void setMetodoEnvio(java.lang.String metodoEnvio) {
        this.metodoEnvio = metodoEnvio;
    }


    /**
     * Gets the metodoPago value for this RespuestaConsultarPedido.
     * 
     * @return metodoPago
     */
    public java.lang.String getMetodoPago() {
        return metodoPago;
    }


    /**
     * Sets the metodoPago value for this RespuestaConsultarPedido.
     * 
     * @param metodoPago
     */
    public void setMetodoPago(java.lang.String metodoPago) {
        this.metodoPago = metodoPago;
    }


    /**
     * Gets the montoCupon value for this RespuestaConsultarPedido.
     * 
     * @return montoCupon
     */
    public java.lang.String getMontoCupon() {
        return montoCupon;
    }


    /**
     * Sets the montoCupon value for this RespuestaConsultarPedido.
     * 
     * @param montoCupon
     */
    public void setMontoCupon(java.lang.String montoCupon) {
        this.montoCupon = montoCupon;
    }


    /**
     * Gets the numeroCupon value for this RespuestaConsultarPedido.
     * 
     * @return numeroCupon
     */
    public java.lang.String getNumeroCupon() {
        return numeroCupon;
    }


    /**
     * Sets the numeroCupon value for this RespuestaConsultarPedido.
     * 
     * @param numeroCupon
     */
    public void setNumeroCupon(java.lang.String numeroCupon) {
        this.numeroCupon = numeroCupon;
    }


    /**
     * Gets the numeroPedido value for this RespuestaConsultarPedido.
     * 
     * @return numeroPedido
     */
    public java.lang.String getNumeroPedido() {
        return numeroPedido;
    }


    /**
     * Sets the numeroPedido value for this RespuestaConsultarPedido.
     * 
     * @param numeroPedido
     */
    public void setNumeroPedido(java.lang.String numeroPedido) {
        this.numeroPedido = numeroPedido;
    }


    /**
     * Gets the oferta value for this RespuestaConsultarPedido.
     * 
     * @return oferta
     */
    public java.lang.String getOferta() {
        return oferta;
    }


    /**
     * Sets the oferta value for this RespuestaConsultarPedido.
     * 
     * @param oferta
     */
    public void setOferta(java.lang.String oferta) {
        this.oferta = oferta;
    }


    /**
     * Gets the pedido value for this RespuestaConsultarPedido.
     * 
     * @return pedido
     */
    public SalesFlatOrder getPedido() {
        return pedido;
    }


    /**
     * Sets the pedido value for this RespuestaConsultarPedido.
     * 
     * @param pedido
     */
    public void setPedido(SalesFlatOrder pedido) {
        this.pedido = pedido;
    }


    /**
     * Gets the pedidoOrigen value for this RespuestaConsultarPedido.
     * 
     * @return pedidoOrigen
     */
    public java.lang.String getPedidoOrigen() {
        return pedidoOrigen;
    }


    /**
     * Sets the pedidoOrigen value for this RespuestaConsultarPedido.
     * 
     * @param pedidoOrigen
     */
    public void setPedidoOrigen(java.lang.String pedidoOrigen) {
        this.pedidoOrigen = pedidoOrigen;
    }


    /**
     * Gets the plazos value for this RespuestaConsultarPedido.
     * 
     * @return plazos
     */
    public java.lang.Short getPlazos() {
        return plazos;
    }


    /**
     * Sets the plazos value for this RespuestaConsultarPedido.
     * 
     * @param plazos
     */
    public void setPlazos(java.lang.Short plazos) {
        this.plazos = plazos;
    }


    /**
     * Gets the promoType value for this RespuestaConsultarPedido.
     * 
     * @return promoType
     */
    public java.lang.String getPromoType() {
        return promoType;
    }


    /**
     * Sets the promoType value for this RespuestaConsultarPedido.
     * 
     * @param promoType
     */
    public void setPromoType(java.lang.String promoType) {
        this.promoType = promoType;
    }


    /**
     * Gets the referencia value for this RespuestaConsultarPedido.
     * 
     * @return referencia
     */
    public java.lang.String getReferencia() {
        return referencia;
    }


    /**
     * Sets the referencia value for this RespuestaConsultarPedido.
     * 
     * @param referencia
     */
    public void setReferencia(java.lang.String referencia) {
        this.referencia = referencia;
    }


    /**
     * Gets the respuesta value for this RespuestaConsultarPedido.
     * 
     * @return respuesta
     */
    public java.lang.String getRespuesta() {
        return respuesta;
    }


    /**
     * Sets the respuesta value for this RespuestaConsultarPedido.
     * 
     * @param respuesta
     */
    public void setRespuesta(java.lang.String respuesta) {
        this.respuesta = respuesta;
    }


    /**
     * Gets the status value for this RespuestaConsultarPedido.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this RespuestaConsultarPedido.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the tarjeta value for this RespuestaConsultarPedido.
     * 
     * @return tarjeta
     */
    public java.lang.String getTarjeta() {
        return tarjeta;
    }


    /**
     * Sets the tarjeta value for this RespuestaConsultarPedido.
     * 
     * @param tarjeta
     */
    public void setTarjeta(java.lang.String tarjeta) {
        this.tarjeta = tarjeta;
    }


    /**
     * Gets the tienda value for this RespuestaConsultarPedido.
     * 
     * @return tienda
     */
    public java.lang.String getTienda() {
        return tienda;
    }


    /**
     * Sets the tienda value for this RespuestaConsultarPedido.
     * 
     * @param tienda
     */
    public void setTienda(java.lang.String tienda) {
        this.tienda = tienda;
    }


    /**
     * Gets the tiendaPago value for this RespuestaConsultarPedido.
     * 
     * @return tiendaPago
     */
    public java.lang.String getTiendaPago() {
        return tiendaPago;
    }


    /**
     * Sets the tiendaPago value for this RespuestaConsultarPedido.
     * 
     * @param tiendaPago
     */
    public void setTiendaPago(java.lang.String tiendaPago) {
        this.tiendaPago = tiendaPago;
    }


    /**
     * Gets the tipoCupon value for this RespuestaConsultarPedido.
     * 
     * @return tipoCupon
     */
    public java.lang.String getTipoCupon() {
        return tipoCupon;
    }


    /**
     * Sets the tipoCupon value for this RespuestaConsultarPedido.
     * 
     * @param tipoCupon
     */
    public void setTipoCupon(java.lang.String tipoCupon) {
        this.tipoCupon = tipoCupon;
    }


    /**
     * Gets the total value for this RespuestaConsultarPedido.
     * 
     * @return total
     */
    public java.math.BigDecimal getTotal() {
        return total;
    }


    /**
     * Sets the total value for this RespuestaConsultarPedido.
     * 
     * @param total
     */
    public void setTotal(java.math.BigDecimal total) {
        this.total = total;
    }


    /**
     * Gets the totalEnvio value for this RespuestaConsultarPedido.
     * 
     * @return totalEnvio
     */
    public java.math.BigDecimal getTotalEnvio() {
        return totalEnvio;
    }


    /**
     * Sets the totalEnvio value for this RespuestaConsultarPedido.
     * 
     * @param totalEnvio
     */
    public void setTotalEnvio(java.math.BigDecimal totalEnvio) {
        this.totalEnvio = totalEnvio;
    }


    /**
     * Gets the xml value for this RespuestaConsultarPedido.
     * 
     * @return xml
     */
    public java.lang.String getXml() {
        return xml;
    }


    /**
     * Sets the xml value for this RespuestaConsultarPedido.
     * 
     * @param xml
     */
    public void setXml(java.lang.String xml) {
        this.xml = xml;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RespuestaConsultarPedido)) return false;
        RespuestaConsultarPedido other = (RespuestaConsultarPedido) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.accionCupon==null && other.getAccionCupon()==null) || 
             (this.accionCupon!=null &&
              this.accionCupon.equals(other.getAccionCupon()))) &&
            ((this.estado==null && other.getEstado()==null) || 
             (this.estado!=null &&
              this.estado.equals(other.getEstado()))) &&
            ((this.itempProm==null && other.getItempProm()==null) || 
             (this.itempProm!=null &&
              java.util.Arrays.equals(this.itempProm, other.getItempProm()))) &&
            ((this.itempTienda==null && other.getItempTienda()==null) || 
             (this.itempTienda!=null &&
              java.util.Arrays.equals(this.itempTienda, other.getItempTienda()))) &&
            ((this.itemsp==null && other.getItemsp()==null) || 
             (this.itemsp!=null &&
              java.util.Arrays.equals(this.itemsp, other.getItemsp()))) &&
            ((this.metodoEnvio==null && other.getMetodoEnvio()==null) || 
             (this.metodoEnvio!=null &&
              this.metodoEnvio.equals(other.getMetodoEnvio()))) &&
            ((this.metodoPago==null && other.getMetodoPago()==null) || 
             (this.metodoPago!=null &&
              this.metodoPago.equals(other.getMetodoPago()))) &&
            ((this.montoCupon==null && other.getMontoCupon()==null) || 
             (this.montoCupon!=null &&
              this.montoCupon.equals(other.getMontoCupon()))) &&
            ((this.numeroCupon==null && other.getNumeroCupon()==null) || 
             (this.numeroCupon!=null &&
              this.numeroCupon.equals(other.getNumeroCupon()))) &&
            ((this.numeroPedido==null && other.getNumeroPedido()==null) || 
             (this.numeroPedido!=null &&
              this.numeroPedido.equals(other.getNumeroPedido()))) &&
            ((this.oferta==null && other.getOferta()==null) || 
             (this.oferta!=null &&
              this.oferta.equals(other.getOferta()))) &&
            ((this.pedido==null && other.getPedido()==null) || 
             (this.pedido!=null &&
              this.pedido.equals(other.getPedido()))) &&
            ((this.pedidoOrigen==null && other.getPedidoOrigen()==null) || 
             (this.pedidoOrigen!=null &&
              this.pedidoOrigen.equals(other.getPedidoOrigen()))) &&
            ((this.plazos==null && other.getPlazos()==null) || 
             (this.plazos!=null &&
              this.plazos.equals(other.getPlazos()))) &&
            ((this.promoType==null && other.getPromoType()==null) || 
             (this.promoType!=null &&
              this.promoType.equals(other.getPromoType()))) &&
            ((this.referencia==null && other.getReferencia()==null) || 
             (this.referencia!=null &&
              this.referencia.equals(other.getReferencia()))) &&
            ((this.respuesta==null && other.getRespuesta()==null) || 
             (this.respuesta!=null &&
              this.respuesta.equals(other.getRespuesta()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.tarjeta==null && other.getTarjeta()==null) || 
             (this.tarjeta!=null &&
              this.tarjeta.equals(other.getTarjeta()))) &&
            ((this.tienda==null && other.getTienda()==null) || 
             (this.tienda!=null &&
              this.tienda.equals(other.getTienda()))) &&
            ((this.tiendaPago==null && other.getTiendaPago()==null) || 
             (this.tiendaPago!=null &&
              this.tiendaPago.equals(other.getTiendaPago()))) &&
            ((this.tipoCupon==null && other.getTipoCupon()==null) || 
             (this.tipoCupon!=null &&
              this.tipoCupon.equals(other.getTipoCupon()))) &&
            ((this.total==null && other.getTotal()==null) || 
             (this.total!=null &&
              this.total.equals(other.getTotal()))) &&
            ((this.totalEnvio==null && other.getTotalEnvio()==null) || 
             (this.totalEnvio!=null &&
              this.totalEnvio.equals(other.getTotalEnvio()))) &&
            ((this.xml==null && other.getXml()==null) || 
             (this.xml!=null &&
              this.xml.equals(other.getXml())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAccionCupon() != null) {
            _hashCode += getAccionCupon().hashCode();
        }
        if (getEstado() != null) {
            _hashCode += getEstado().hashCode();
        }
        if (getItempProm() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getItempProm());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getItempProm(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getItempTienda() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getItempTienda());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getItempTienda(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getItemsp() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getItemsp());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getItemsp(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMetodoEnvio() != null) {
            _hashCode += getMetodoEnvio().hashCode();
        }
        if (getMetodoPago() != null) {
            _hashCode += getMetodoPago().hashCode();
        }
        if (getMontoCupon() != null) {
            _hashCode += getMontoCupon().hashCode();
        }
        if (getNumeroCupon() != null) {
            _hashCode += getNumeroCupon().hashCode();
        }
        if (getNumeroPedido() != null) {
            _hashCode += getNumeroPedido().hashCode();
        }
        if (getOferta() != null) {
            _hashCode += getOferta().hashCode();
        }
        if (getPedido() != null) {
            _hashCode += getPedido().hashCode();
        }
        if (getPedidoOrigen() != null) {
            _hashCode += getPedidoOrigen().hashCode();
        }
        if (getPlazos() != null) {
            _hashCode += getPlazos().hashCode();
        }
        if (getPromoType() != null) {
            _hashCode += getPromoType().hashCode();
        }
        if (getReferencia() != null) {
            _hashCode += getReferencia().hashCode();
        }
        if (getRespuesta() != null) {
            _hashCode += getRespuesta().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getTarjeta() != null) {
            _hashCode += getTarjeta().hashCode();
        }
        if (getTienda() != null) {
            _hashCode += getTienda().hashCode();
        }
        if (getTiendaPago() != null) {
            _hashCode += getTiendaPago().hashCode();
        }
        if (getTipoCupon() != null) {
            _hashCode += getTipoCupon().hashCode();
        }
        if (getTotal() != null) {
            _hashCode += getTotal().hashCode();
        }
        if (getTotalEnvio() != null) {
            _hashCode += getTotalEnvio().hashCode();
        }
        if (getXml() != null) {
            _hashCode += getXml().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }
}
