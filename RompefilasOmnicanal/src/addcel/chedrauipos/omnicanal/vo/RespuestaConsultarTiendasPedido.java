/**
 * RespuestaConsultarTiendasPedido.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package addcel.chedrauipos.omnicanal.vo;

public class RespuestaConsultarTiendasPedido  implements java.io.Serializable {
    private java.lang.String sku;

    private java.lang.String tienda;

    private java.math.BigDecimal totalArticulos;

    public RespuestaConsultarTiendasPedido() {
    }

    public RespuestaConsultarTiendasPedido(
           java.lang.String sku,
           java.lang.String tienda,
           java.math.BigDecimal totalArticulos) {
           this.sku = sku;
           this.tienda = tienda;
           this.totalArticulos = totalArticulos;
    }


    /**
     * Gets the sku value for this RespuestaConsultarTiendasPedido.
     * 
     * @return sku
     */
    public java.lang.String getSku() {
        return sku;
    }


    /**
     * Sets the sku value for this RespuestaConsultarTiendasPedido.
     * 
     * @param sku
     */
    public void setSku(java.lang.String sku) {
        this.sku = sku;
    }


    /**
     * Gets the tienda value for this RespuestaConsultarTiendasPedido.
     * 
     * @return tienda
     */
    public java.lang.String getTienda() {
        return tienda;
    }


    /**
     * Sets the tienda value for this RespuestaConsultarTiendasPedido.
     * 
     * @param tienda
     */
    public void setTienda(java.lang.String tienda) {
        this.tienda = tienda;
    }


    /**
     * Gets the totalArticulos value for this RespuestaConsultarTiendasPedido.
     * 
     * @return totalArticulos
     */
    public java.math.BigDecimal getTotalArticulos() {
        return totalArticulos;
    }


    /**
     * Sets the totalArticulos value for this RespuestaConsultarTiendasPedido.
     * 
     * @param totalArticulos
     */
    public void setTotalArticulos(java.math.BigDecimal totalArticulos) {
        this.totalArticulos = totalArticulos;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RespuestaConsultarTiendasPedido)) return false;
        RespuestaConsultarTiendasPedido other = (RespuestaConsultarTiendasPedido) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sku==null && other.getSku()==null) || 
             (this.sku!=null &&
              this.sku.equals(other.getSku()))) &&
            ((this.tienda==null && other.getTienda()==null) || 
             (this.tienda!=null &&
              this.tienda.equals(other.getTienda()))) &&
            ((this.totalArticulos==null && other.getTotalArticulos()==null) || 
             (this.totalArticulos!=null &&
              this.totalArticulos.equals(other.getTotalArticulos())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSku() != null) {
            _hashCode += getSku().hashCode();
        }
        if (getTienda() != null) {
            _hashCode += getTienda().hashCode();
        }
        if (getTotalArticulos() != null) {
            _hashCode += getTotalArticulos().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

}
