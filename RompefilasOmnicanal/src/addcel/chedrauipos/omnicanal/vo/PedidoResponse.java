package addcel.chedrauipos.omnicanal.vo;

import java.util.List;

public class PedidoResponse extends AbstractVO {
	
	private String orderIncrementId;
	private RespuestaConsultarPedido pedido;
	private RespuestaConsultarPedido pedidoOriginal;
	private String tiendaPadre;
	private List<PromocionVO> promosArticulos;
	private List<PromocionVO> promosGenerales;
	
	public PedidoResponse() {
	}

	public PedidoResponse(int idError, String msgError) {
		super(idError, msgError);
	}
	
	public String getOrderIncrementId() {
		return orderIncrementId;
	}
	
	public void setOrderIncrementId(String orderIncrementId) {
		this.orderIncrementId = orderIncrementId;
	}

	public RespuestaConsultarPedido getPedido() {
		return pedido;
	}

	public void setPedido(RespuestaConsultarPedido pedido) {
		this.pedido = pedido;
	}

	public RespuestaConsultarPedido getPedidoOriginal() {
		return pedidoOriginal;
	}

	public void setPedidoOriginal(RespuestaConsultarPedido pedidoOriginal) {
		this.pedidoOriginal = pedidoOriginal;
	}

	public String getTiendaPadre() {
		return tiendaPadre;
	}

	public void setTiendaPadre(String tiendaPadre) {
		this.tiendaPadre = tiendaPadre;
	}

	public List<PromocionVO> getPromosArticulos() {
		return promosArticulos;
	}

	public void setPromosArticulos(List<PromocionVO> promosArticulos) {
		this.promosArticulos = promosArticulos;
	}

	public List<PromocionVO> getPromosGenerales() {
		return promosGenerales;
	}

	public void setPromosGenerales(List<PromocionVO> promosGenerales) {
		this.promosGenerales = promosGenerales;
	}
	

}
