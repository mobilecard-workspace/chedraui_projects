package addcel.chedrauipos.ui;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.List;

import org.addcel.android.chedrauipos.AppStatus;
import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.ServicesStatus;
import org.addcel.android.chedrauipos.Url;
import org.addcel.android.chedrauipos.UserSession;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import addcel.chedrauipos.model.Articulo;
import addcel.chedrauipos.model.factory.ArticuloFactory;
import addcel.chedrauipos.presenter.MenuPresenter;
import addcel.chedrauipos.service.receiver.ActualizacionPeriodicaReceiver;
import addcel.chedrauipos.ui.callback.MenuCallback;
import addcel.chedrauipos.ui.dialog.LoginDialog;
import addcel.chedrauipos.ui.dialog.PasswordDialog;
import addcel.chedrauipos.ui.dialog.PrinterConfDialog;
import addcel.chedrauipos.util.ChedrauiTextUtil;
import addcel.chedrauipos.util.FileTransferClient;
import addcel.chedrauipos.util.SftpTransferClient;
import addcel.chedrauipos.vo.DefaultResponse;
import addcel.chedrauipos.vo.LoginResponse;
import addcel.gson.GsonFactory;
import addcel.http.HttpListener;
import addcel.http.HttpListenerFactory;
import addcel.otto.BusFactory;
import addcel.util.DeviceUtils;
import addcel.util.ErrorUtil;
import addcel.version.Version;
import addcel.version.VersionProcessingServiceFactory;
import addcel.version.VersionStatus;
import addcel.version.VersionUtil;
import addcel.version.dialog.VersionDialogFactory;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import com.activeandroid.ActiveAndroid;
import com.squareup.otto.Subscribe;
import com.squareup.phrase.Phrase;

public class MenuActivity extends Activity implements MenuCallback{
	
	@InjectView(R.id.view_actualizacion) 
	TextView actualizacionView;
	
	private MenuPresenter presenter;
	private LoginDialog lDialog;
	private PasswordDialog pDialog;
	private PrinterConfDialog printConfDialog;
	private HttpListener listener = HttpListenerFactory.get(new LoginListener());
	private static AlertDialog tipoVentaDialog;
	private final Context context = this;
	private AppStatus status;
	private ActualizacionPeriodicaReceiver alarm = new ActualizacionPeriodicaReceiver();
	
	
	private static final String TAG = MenuActivity.class.getSimpleName();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		ButterKnife.inject(this);
		presenter = new MenuPresenter(this, this);
		status = AppStatus.get(context);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		BusFactory.get().register(context);
		
		LocalDate today = new LocalDate();
		LocalDate update = AppStatus.get(context).getFechaActualizacion().toLocalDate();
		if (update.isBefore(today)) {
			alarm.cancelAlarm(context);
			status.setUpdated(false);
		} else {
			status.setUpdated(true);
			actualizacionView.setText(
					Phrase.from(context, R.string.actualizacion)
						.put("fecha", status.getFechaActualizacion().
								toString("dd/MM/yyyy hh:mm:ss")).format());
		}
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		BusFactory.get().unregister(context);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = new MenuInflater(context);
		inflater.inflate(R.menu.menu, menu);

		if (UserSession.get().getLogin() != null) {

			MenuItem it = menu.findItem(R.id.a_notlogged);
			it.setVisible(false);
			MenuItem logged = menu.findItem(R.id.a_logged);
			Menu m = logged.getSubMenu();

			if (UserSession.get().getLogin().getRole().getIdRol() == 102) {
				m.findItem(R.id.a_registro).setVisible(false);
				m.findItem(R.id.a_password).setVisible(false);
				m.findItem(R.id.a_datos).setVisible(false);
				m.findItem(R.id.a_settings).setVisible(false);
			}

		} else {
			MenuItem it = menu.findItem(R.id.a_logged);
			it.setVisible(false);
		}

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();

		switch (id) {
		case R.id.a_login:
			if (UserSession.get().getLogin() != null) {

			}
			getLDialog(context, listener).show();
			invalidateOptionsMenu();
			break;
		case R.id.a_registro:
			startActivity(new Intent(context, RegistroActivity.class));
			break;
		case R.id.a_logout:
			UserSession.get().logout();
			// SessionTimerBroadcastService.setFlagCerrarSesion();
			invalidateOptionsMenu();
			break;
		case R.id.a_datos:
			startActivity(new Intent(context, DatosActivity.class));
			break;
		case R.id.a_password:
			getPDialog(context, HttpListenerFactory.get(new PasswordListener())).show();
			break;
//		case R.id.a_consultar:
//			break;
		case R.id.a_tickets:
			if (UserSession.get().isLoggedIn())
				startActivity(new Intent(context, TicketActivity.class));
			else
				Toast.makeText(context, R.string.error_login_ticket, Toast.LENGTH_SHORT)
				.show();
			break;
		
		case R.id.a_printer:
			if (UserSession.get().isLoggedIn())
				presenter.getBtDevices();
			else
				Toast.makeText(context, R.string.error_login_ticket, Toast.LENGTH_SHORT)
				.show();
			break;

		case R.id.a_settings:
			if (UserSession.get().getLogin() != null) {
				// SessionTimerBroadcastService.setFlagNoCerrarSesion();
			}
			startActivity(new Intent(Settings.ACTION_SETTINGS));
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		if (UserSession.get().getLogin() != null) {
			menu.getItem(0).setVisible(false);
		} else {
			menu.getItem(1).setVisible(false);
		}

		return super.onPrepareOptionsMenu(menu);
	}
	
	public LoginDialog getLDialog(Context context, HttpListener listener) {
		if (lDialog == null)
			lDialog = new LoginDialog(context, listener,
						DeviceUtils.getWidth(context) / 2);
		
		return lDialog;
	}
	
	public PasswordDialog getPDialog(Context context, HttpListener listener) {
		if (pDialog == null)
			pDialog = new PasswordDialog(context, listener,
					DeviceUtils.getWidth(context) / 2);
		
		return pDialog;
	}
	
	public static synchronized AlertDialog getTipoVentaDialog(final Context context) {
		
			Builder builder = new Builder(context);
			builder.setTitle(R.string.tipoventa_title)
			.setItems(R.array.array_tipoventa,new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.dismiss();
					
					switch (which) {
					case 0:
						context.startActivity(new Intent(context, RompefilasActivity.class));
						break;
	
					default:
						context.startActivity(new Intent(context, OmnicanalActivity.class));
						break;
					}
				}
			});

		tipoVentaDialog = builder.create();
		return tipoVentaDialog;
	}
	
	@OnClick(R.id.b_cargar)
	void iniciarProcesoActualizacion() {
		if(!ServicesStatus.areServicesRunning()) {
			
			if (UserSession.get().isLoggedIn()) {
				
				if (!AppStatus.get(this).getUpdated()) {
					
					getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
					new AsyncTask<Void, Void, Boolean>() {
						
						private ProgressDialog pDialog;
						
						@Override
						protected void onPreExecute() {
							if (pDialog == null || !pDialog.isShowing()) {
								pDialog = ProgressDialog.show(
										context, "", "Actualización masiva de artículos.",
										true, false);
							}
							
							alarm.cancelAlarm(context);
						};
	
						@Override
						protected Boolean doInBackground(Void... params) {
							// TODO Auto-generated method stub
							String fileName = ChedrauiTextUtil.buildStockFileName(
									AppStatus.get(context).getCodigo(),
									"MAS");
							
							int result = downloadFile(fileName);
							
							switch (result) {
							case 0:
								return insertDataFromFile(fileName);
	
							default:
								return false;
							}
						}
						
						@Override
						protected void onPostExecute(Boolean result) {
							
							if (pDialog != null || pDialog.isShowing()) {
								pDialog.dismiss();
							}
							
							getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
							
							if (result) {
								alarm.setAlarm(context);
								onFileUpdateSuccessful(new DateTime());
							} else {
								onFileUpdateFailed(getString(R.string.error_descarga_masiva));
							}
						};
					}.execute();
				} else {
					Toast.makeText(context, R.string.catalogo_actualizado, Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(context, R.string.error_login_actualizar, Toast.LENGTH_SHORT).show();
			}
		} else {
			Toast.makeText(context, R.string.error_servicio_ejecutando, Toast.LENGTH_SHORT).show();
		}
	}
	
	@OnClick(R.id.b_venta)
	void iniciarNuevaVenta() {
		if (!ServicesStatus.areServicesRunning()) {
			if (UserSession.get().isLoggedIn()) {
				
				if (AppStatus.get(context).getUpdated()) {
					getTipoVentaDialog(context).show();
				} else {
					Toast.makeText(context, R.string.error_actualizacion, Toast.LENGTH_SHORT).show();
				}
				
			} else {
				Toast.makeText(context, R.string.error_login_venta, Toast.LENGTH_SHORT).show();
			}
		} else {
			Toast.makeText(context, R.string.error_servicio_ejecutando, Toast.LENGTH_SHORT).show();
		}
	}
	
	@Subscribe
	public void getVersionFromBus(Version version) {
		if (version != null) {

			VersionStatus status = VersionProcessingServiceFactory.get()
					.processVersion(
							VersionUtil.getDeviceVersionNumber(context,
									getClass()), version);

			switch (status) {
			case SIN_CAMBIO:
				Toast.makeText(context, R.string.version_sin_cambio,
						Toast.LENGTH_LONG).show();
				break;
			case OBLIGATORIO:
				VersionDialogFactory.get(context, R.string.version_obligatoria,
						version.getUrl()).show();
				break;
			case OPCIONAL:
				VersionDialogFactory.get(context, R.string.version_opcional,
						null, 2, version.getUrl());
				break;

			default:

				break;
			}
		}
	}
	
	private int downloadFile(String fileName) {
		FileTransferClient client = SftpTransferClient
				.get("50.57.192.214", "rmuniz", "xZZWj4GFzTZa");
		
		String file = getFilesDir() + "/" + fileName;
		
		if (client.connect())
			return client.getFile(file, Url.DESCARGA);
		
		return -10000;
	}
	
	private boolean insertDataFromFile(String fileName) {
		
		
		
		fileName = getFilesDir() + "/" + fileName;
		InputStream stream = null;
		try {
			
			stream = new FileInputStream(fileName);
			BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
			Articulo.deleteAll();
			
			String line;
			ActiveAndroid.beginTransaction();
			while ((line = reader.readLine()) != null) {
				Articulo a = ArticuloFactory.get(TextUtils.split(line, "\\|")); 
				
				if (a != null) {
					try { 
						a.save();
					} catch (SQLiteConstraintException e) {
						Log.e(TAG, "SQLiteConstraintException Error al insertar UPC " + a.getUpc());
					}
				} else {
					Log.e(TAG, "No se generó articulo con linea: " + line);
				}
			}
			ActiveAndroid.setTransactionSuccessful();
			Log.d(TAG, "Termina insercion masiva: " 
					+ Calendar.getInstance().getTime().toString());
			
			return true;
			
		} catch (FileNotFoundException e0) {
			Log.e(TAG, "No se encontró archivo en dispositivo: "
					+ Calendar.getInstance().getTime().toString(), e0);
		} catch (UnsupportedEncodingException e1) {
			Log.e(TAG, "Error en encoding de archivo: "
					+ Calendar.getInstance().getTime().toString(), e1);
		} catch (Exception e2) {
			Log.e(TAG, "Error en inserción de masivo de artículos: "
					+ Calendar.getInstance().getTime().toString(), e2);
		} finally {
			ActiveAndroid.endTransaction();
			try {
				stream.close();
			} catch (IOException e) {
				Log.e(TAG, "Error al cerrar archivo", e);
			}
		}
		
		return false;
	}
	
	@Subscribe
	public void onFileUpdateSuccessful(DateTime date) {
		AppStatus.get(context).setFechaActualizacion(date);
		AppStatus.get(context).setUpdated(true);
		actualizacionView.setText(
				Phrase.from(context, R.string.actualizacion)
					.put("fecha", date.toString("dd/MM/yyyy hh:mm:ss")).format());
		
		Toast.makeText(context, R.string.exito_actualizacion, Toast.LENGTH_LONG).show();
	}
	
	@Subscribe
	public void onFileUpdateFailed(String error) {
		AppStatus.get(context).setUpdated(true);
		actualizacionView.setText(error);
		
		Toast.makeText(context, error, Toast.LENGTH_LONG).show();
	}
	
	@Override
	public void onBtDevicesObtained(List<BluetoothDevice> devices) {
		// TODO Auto-generated method stub
		if (devices == null || devices.isEmpty())
			Toast.makeText(context, R.string.error_bt, 
					Toast.LENGTH_LONG).show();
		else {
			printConfDialog = new PrinterConfDialog(context, devices,
													DeviceUtils.getWidth(context) / 2);
			printConfDialog.show();
		}
	}
	
	
	
	/**
	 * Implementación de {@code HttpListener} que escucha la petición
	 * correspodiente al servicio <b>login</b>
	 * 
	 * @author carlosgs
	 *
	 */
	private class LoginListener implements HttpListener {

		@Override
		public void onSuccess(String response) {
			// TODO Auto-generated method stub
			
			if (getLDialog(context, listener).isShowing())
				getLDialog(context, listener).dismiss();

			LoginResponse resp = GsonFactory.get().fromJson(response,
					LoginResponse.class);

			switch (resp.getIdError()) {
			case 0:
				if (resp.getStatus() == 1) {
					UserSession.get().setLogin(resp);
					
					AppStatus.get(context).setCodigo(
							resp.getTienda().getCodigo());
					
					Toast.makeText(context, resp.getMensajeError(),
							Toast.LENGTH_LONG).show();
					invalidateOptionsMenu();
				} else if (resp.getStatus() == 99) {
					UserSession.get().setLogin(resp);
					
					AppStatus.get(context).setCodigo(
							UserSession.get().getLogin().getTienda().getCodigo());
					
					getPDialog(context, HttpListenerFactory.get(new PasswordListener())).show();
					
				} else {
					UserSession.get().setLogin(null);
					Toast.makeText(context, resp.getMensajeError(),
							Toast.LENGTH_LONG).show();
				}

				break;

			default:
				UserSession.get().setLogin(null);
				Toast.makeText(context, resp.getMensajeError(),
						Toast.LENGTH_LONG).show();
				break;
			}
		}

		@Override
		public void onError() {
			// TODO Auto-generated method stub
			if (getLDialog(context, listener).isShowing())
				getLDialog(context, listener).dismiss();

			UserSession.get().setLogin(null);
			Toast.makeText(context, ErrorUtil.errorServicio(context, "Login"),
					Toast.LENGTH_LONG).show();
		}

	}
	
	private class PasswordListener implements HttpListener {

		@Override
		public void onSuccess(String response) {
			// TODO Auto-generated method stub

			if (getPDialog(context, listener).isShowing())
				getPDialog(context, listener).dismiss();
			
			DefaultResponse resp = GsonFactory.get().fromJson(response, DefaultResponse.class);
			
			switch (resp.getIdError()) {
			case 0:
				Toast.makeText(context, resp.getMensajeError()
						.concat("\nInicia sesión con tu nueva contraseña") , Toast.LENGTH_LONG).show();
				break;

			default:
				
				Toast.makeText(context, resp.getMensajeError(),
						Toast.LENGTH_LONG).show();
				break;
			}
			
			UserSession.get().logout();
			invalidateOptionsMenu();
			
		}

		@Override
		public void onError() {
			// TODO Auto-generated method stub

			if (getPDialog(context, listener).isShowing())
				getPDialog(context, listener).dismiss();
			
			Toast.makeText(context, ErrorUtil.errorServicio(context, "Actualización contraseña"),
					Toast.LENGTH_LONG).show();
		}
		
	}

}
