package addcel.chedrauipos.ui;

import org.addcel.android.chedrauipos.AppStatus;
import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.ServicesStatus;
import org.joda.time.DateTime;

import addcel.chedrauipos.model.Articulo;
import addcel.chedrauipos.presenter.RompefilasPresenter;
import addcel.chedrauipos.ui.adapter.ProductoAdapter;
import addcel.chedrauipos.ui.callback.ArticuloDeletedCallback;
import addcel.chedrauipos.ui.callback.ViewCallback;
import addcel.chedrauipos.ui.dialog.BarcodeDialog;
import addcel.chedrauipos.vo.DepositoResponse;
import addcel.otto.BusFactory;
import addcel.util.AddcelTextUtil;
import addcel.util.DeviceUtils;
import addcel.version.Version;
import addcel.version.VersionProcessingServiceFactory;
import addcel.version.VersionStatus;
import addcel.version.VersionUtil;
import addcel.version.dialog.VersionDialogFactory;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.method.TextKeyListener;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnTextChanged;
import butterknife.OnTextChanged.Callback;

import com.squareup.otto.Subscribe;
import com.squareup.phrase.Phrase;

public class RompefilasActivity extends Activity implements ViewCallback, ArticuloDeletedCallback, OnDismissListener {

	@InjectView(R.id.view_actualizacion)
	TextView actualizacionView;
	@InjectView(R.id.view_descripcion)
	TextView descripcionView;
	@InjectView(R.id.view_total)
	TextView totalView;
	@InjectView(R.id.view_numart)
	TextView numArticulosView;
	@InjectView(R.id.list_articulos)
	ListView articulosList;
	@InjectView(R.id.text_upc)
	EditText upcText;
	
	private AppStatus status;
	private RompefilasPresenter controller;
	private ProgressDialog pDialog;
	private ProductoAdapter adapter;
	private final Context context = this;
	private BarcodeDialog barcodeDialog;
	
	private static final String TAG = RompefilasActivity.class.getSimpleName();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.inject(this);
		findViewById(R.id.view_cancelados).setVisibility(View.GONE);
		status= AppStatus.get(this);
		controller = new RompefilasPresenter(context);
		adapter = new ProductoAdapter(this, controller.getArticulos(), this);
		articulosList.setAdapter(adapter);
		
		pDialog = new ProgressDialog(this);
		pDialog.setMessage(getText(R.string.msg_pdialog));
		pDialog.setCancelable(false);
		pDialog.setIndeterminate(true);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		BusFactory.get().register(context);
		setActualizacion(status.getFechaActualizacion());
		setTotal(0);
		setNumArticulos(0);
		upcText.requestFocus();
		
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		BusFactory.get().unregister(context);
	}
	
	@OnTextChanged(value = R.id.text_upc, callback = Callback.AFTER_TEXT_CHANGED)
	void queryUpc(final Editable s) {
		
		if (TextUtils.indexOf(s, "\n") == (s.length() - 1)) {
			Log.d("afterTextChanged", "Encuentra char");
			if (s.length() > 0) {
				new AsyncTask<CharSequence, Void, Boolean>() {

					@Override
					protected Boolean doInBackground(CharSequence... params) {
						// TODO Auto-generated method stub
						return controller.searchArticulo(params[0].toString());
					}
					
					protected void onPostExecute(Boolean result) {
						if (result) {
							adapter.notifyDataSetChanged();
							setTotal(Articulo.getTotal(controller.getArticulos()));
							setNumArticulos(controller.getArticulos().size());
							
						} else {
							CharSequence seq = Phrase.from(context,
									R.string.error_upc_noencontrado)
									.put("upc", s).format();
							
							Toast.makeText(context, seq, Toast.LENGTH_SHORT).show();
						}
						
						TextKeyListener.clear(s);
					};
				
				}.execute(s);
			}
		}
	}
	
	@OnItemClick(R.id.list_articulos)
	void showArticuloDescription(int position) {
		descripcionView.setText(
				controller.getArticulos().get(position).toString());
	}
	
	private void setActualizacion(DateTime d) {
		CharSequence dateSequence = Phrase.from(this, R.string.actualizacion).put("fecha", 
				d.toString("dd/MM/yyyy hh:mm:ss")).format();
		actualizacionView.setText(dateSequence);
	}
	
	private void setTotal(double total) {
		CharSequence totalSequence = Phrase.from(this, R.string.total).put("total", 
				AddcelTextUtil.formatCurrency(total)).format();
		totalView.setText(totalSequence);
	}
	
	private void setNumArticulos(int numArticulos) {
		CharSequence numArtSequence = Phrase.from(this, R.string.num_articulos).put("numart", 
				Integer.toString(numArticulos)).format();
		numArticulosView.setText(numArtSequence);
	}
	
	@OnClick(R.id.b_eliminar)
	void deleteAllArticulos() {
		if (controller.getArticulos().size() > 0) {
			Builder builder = new Builder(context)
								.setMessage(R.string.eliminar_message)
								.setPositiveButton(android.R.string.ok, 
										new DialogInterface.OnClickListener() {
									
									@Override
									public void onClick(DialogInterface dialog, int which) {
										// TODO Auto-generated method stub
										controller.deleteAllArticulos();
										adapter.notifyDataSetChanged();
										setTotal(Articulo.getTotal(controller.getArticulos()));
										setNumArticulos(controller.getArticulos().size());
										descripcionView.setText("");
										dialog.dismiss();
									}
								})
								.setNegativeButton(android.R.string.cancel,
										new DialogInterface.OnClickListener() {
									
									@Override
									public void onClick(DialogInterface dialog, int which) {
										// TODO Auto-generated method stub
										dialog.dismiss();
									}
								});
			builder.show();
		} else {
			Toast.makeText(context, R.string.error_orden_vacia, Toast.LENGTH_SHORT).show();
		}
	}
	
	@OnClick(R.id.b_pagar)
	void enviarArchivo() {
		if (!ServicesStatus.areServicesRunning()) {
			if (controller.getArticulos().size() > 0) {
				new AsyncTask<Void, Void, DepositoResponse>() {
					
					@Override
					protected void onPreExecute() {
						// TODO Auto-generated method stub
						if (pDialog == null || !pDialog.isShowing())
							pDialog.show();
					}
					
					@Override
					protected DepositoResponse doInBackground(Void... params) {
						// TODO Auto-generated method stub
						return controller.depositFile();
					}
					
					@Override
					protected void onPostExecute(DepositoResponse result) {
						// TODO Auto-generated method stub
						if (pDialog != null && pDialog.isShowing())
							pDialog.dismiss();
						switch (result.getIdError()) {
						case 0:
							Log.d(TAG, "Barcode ticket: " + result.getTicket().getBarcode());
							
							barcodeDialog = new BarcodeDialog(
									context, result.getTicket(),
									DeviceUtils.getWidth(context) / 2, controller);
							barcodeDialog.show();
							
							Toast.makeText(context, R.string.exito_deposito, Toast.LENGTH_SHORT).show();
							break;
		
						default:
							Toast.makeText(context, result.getMensajeError(), Toast.LENGTH_SHORT).show();
							break;
						}
					}
					
				}.execute();
			} else {
				Toast.makeText(context, R.string.error_orden_vacia, Toast.LENGTH_SHORT).show();
			}
		} else {
			Toast.makeText(context, R.string.error_servicio_ejecutando, Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onArticuloDeleted(int position) {
		// TODO Auto-generated method stub
		controller.getArticulos().remove(position);
		adapter.notifyDataSetChanged();
		setTotal(Articulo.getTotal(controller.getArticulos()));
		setNumArticulos(controller.getArticulos().size());
		descripcionView.setText("");
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		// TODO Auto-generated method stub
		dialog.dismiss();
		controller.getArticulos().clear();
		adapter.notifyDataSetChanged();
		setTotal(Articulo.getTotal(controller.getArticulos()));
		setNumArticulos(controller.getArticulos().size());
		descripcionView.setText("");
	}

	
	@Override
	@Subscribe
	public void onVersionReceived(Version version) {
		// TODO Auto-generated method stub
		if (version != null) {

			VersionStatus status = VersionProcessingServiceFactory.get()
					.processVersion(
							VersionUtil.getDeviceVersionNumber(context,
									getClass()), version);

			switch (status) {
			case SIN_CAMBIO:
				Toast.makeText(context, R.string.version_sin_cambio,
						Toast.LENGTH_LONG).show();
				break;
			case OBLIGATORIO:
				VersionDialogFactory.get(context, R.string.version_obligatoria,
						version.getUrl()).show();
				break;
			case OPCIONAL:
				VersionDialogFactory.get(context, R.string.version_opcional,
						null, 2, version.getUrl());
				break;

			default:

				break;
			}
		}
	}

	
	@Override
	@Subscribe
	public void onCatalogUpdated(DateTime date) {
		// TODO Auto-generated method stub
		status.setFechaActualizacion(date);
		status.setUpdated(true);
		actualizacionView.setText(
				Phrase.from(context, R.string.actualizacion)
					.put("fecha", date.toString("dd/MM/yyyy hh:mm:ss")).format());
		
		Toast.makeText(context, R.string.exito_actualizacion, Toast.LENGTH_LONG).show();
	}
	
	
	@Override
	@Subscribe
	public void onErrorFromBus(String error) {
		// TODO Auto-generated method stub
		status.setUpdated(true);
		actualizacionView.setText(error);
		
		Toast.makeText(context, error, Toast.LENGTH_LONG).show();
	}
}
