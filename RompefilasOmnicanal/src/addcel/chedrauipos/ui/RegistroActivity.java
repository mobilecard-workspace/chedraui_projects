package addcel.chedrauipos.ui;

import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.UserSession;

import addcel.chedrauipos.presenter.RegistroController;
import addcel.chedrauipos.ui.callback.RegistroCallback;
import addcel.chedrauipos.vo.DefaultResponse;
import addcel.chedrauipos.vo.Role;
import addcel.chedrauipos.vo.RolesResponse;
import addcel.chedrauipos.vo.Tienda;
import addcel.chedrauipos.vo.TiendasResponse;
import addcel.chedrauipos.vo.UserInsertRequest;
import addcel.util.ErrorUtil;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class RegistroActivity extends Activity implements RegistroCallback {

	@InjectView(R.id.text_login)
	EditText loginText;
	@InjectView(R.id.text_email)
	EditText emailText;
	@InjectView(R.id.text_nombre)
	EditText nombreText;
	@InjectView(R.id.text_paterno)
	EditText paternoText;
	@InjectView(R.id.text_materno)
	EditText maternoText;
	@InjectView(R.id.spinner_tienda)
	Spinner tiendaSpinner;
	@InjectView(R.id.spinner_role)
	Spinner roleSpinner;

	private RegistroController controller;
	private ArrayAdapter<Tienda> tiendaAdapter;
	private ArrayAdapter<Role> roleAdapter;
	private final Context context = this;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registro);
		ButterKnife.inject(this);

		controller = new RegistroController(this, this);
		tiendaAdapter = new ArrayAdapter<Tienda>(this,
				android.R.layout.simple_spinner_item, controller.getTiendas());
		tiendaAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		tiendaSpinner.setAdapter(tiendaAdapter);
		roleAdapter = new ArrayAdapter<Role>(this,
				android.R.layout.simple_spinner_item, controller.getRoles());
		roleAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		roleSpinner.setAdapter(roleAdapter);

		controller.getTiendasAsync();

	}

	private boolean validar() {
		CharSequence login = loginText.getText();
		CharSequence email = emailText.getText();
		CharSequence nombre = nombreText.getText();
		CharSequence paterno = paternoText.getText();

		if (TextUtils.isEmpty(login)) {
			loginText.setError(ErrorUtil.vacio(this, R.string.hint_login));
			return false;
		}

		if (TextUtils.isEmpty(email)) {
			emailText.setError(ErrorUtil.vacio(this, R.string.hint_email));
			return false;
		}

		if (TextUtils.isEmpty(nombre)) {
			nombreText.setError(ErrorUtil.vacio(this, R.string.hint_nombre));
			return false;
		}

		if (TextUtils.isEmpty(paterno)) {
			paternoText.setError(ErrorUtil.vacio(this, R.string.hint_paterno));
			return false;
		}

		return true;
	}

	@OnClick(R.id.b_registrar)
	void registrar() {
		if (validar()) {
			UserInsertRequest request = new UserInsertRequest();
			request.setApellido(paternoText.getText().toString().trim());
			request.setIdRol(((Role) roleSpinner.getSelectedItem()).getIdRol());
			request.setIdSupervisor(UserSession.get().getLogin().getIdUsuario());
			request.setIdTienda(((Tienda) tiendaSpinner.getSelectedItem())
					.getIdTienda());
			request.setLogin(loginText.getText().toString().trim());
			request.setMail(emailText.getText().toString().trim());
			request.setMaterno(maternoText.getText().toString().trim());
			request.setNombre(nombreText.getText().toString().trim());

			controller.registrar(request);
		}
	}

	@Override
	public void onTiendasReceived(TiendasResponse response) {
		// TODO Auto-generated method stub
		if (response != null) {
			switch (response.getIdError()) {
			case 0:
				UserSession.get().setTiendas(response.getTiendas());
				controller.getTiendas().addAll(UserSession.get().getTiendas());
				tiendaAdapter.notifyDataSetChanged();
				break;

			default:
				Toast.makeText(this, response.getMensajeError(),
						Toast.LENGTH_LONG).show();
				break;
			}
		} else {
			finish();
			Toast.makeText(
					this,
					ErrorUtil.errorServicio(context,
							getString(R.string.prompt_tienda)),
					Toast.LENGTH_LONG).show();
		}

		controller.getRolesAsync();
	}

	@Override
	public void onRolesReceived(RolesResponse response) {
		// TODO Auto-generated method stub
		if (response != null) {
			switch (response.getIdError()) {
			case 2:
				UserSession.get().setRoles(response.getRoles());
				controller.getRoles().addAll(UserSession.get().getRoles());
				roleAdapter.notifyDataSetChanged();
				break;

			default:
				Toast.makeText(this, response.getMensajeError(),
						Toast.LENGTH_LONG).show();
				break;
			}
		} else {
			finish();
			Toast.makeText(
					this,
					ErrorUtil.errorServicio(context,
							getString(R.string.prompt_rol)), Toast.LENGTH_LONG)
					.show();
		}
	}

	@Override
	public void onRegistroFinished(DefaultResponse response) {
		// TODO Auto-generated method stub
		if (response != null) {
			Toast.makeText(this, response.getMensajeError(), Toast.LENGTH_LONG)
					.show();
			if (response.getIdError() == 0)
				finish();
		} else {
			Toast.makeText(this,
					ErrorUtil.errorServicio(context, "Registro de usuario"),
					Toast.LENGTH_LONG).show();
		}
	}

}
