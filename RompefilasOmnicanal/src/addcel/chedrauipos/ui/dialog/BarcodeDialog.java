package addcel.chedrauipos.ui.dialog;

import org.addcel.android.chedrauipos.R;

import addcel.chedrauipos.model.Ticket;
import addcel.chedrauipos.presenter.RompefilasPresenter;
import addcel.chedrauipos.util.BarcodeUtil;
import addcel.util.ProgressDialogFactory;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import com.google.zxing.BarcodeFormat;

public class BarcodeDialog extends Dialog {

	@InjectView(R.id.img_barcode)
	ImageView barcodeImg;
	@InjectView(R.id.view_barcode)
	TextView barcodeView;

	private Context context;
	private Ticket ticket;
	private int width;
	private RompefilasPresenter controller;
	private ProgressDialog pDialog;

	public BarcodeDialog(Context context, Ticket ticket, int width,
			RompefilasPresenter controller) {
		super(context);
		this.context = context;
		this.ticket = ticket;
		this.width = width;
		this.controller = controller;
		this.pDialog = ProgressDialogFactory.get(this.context,R.string.txt_imprimir_msg);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_barcode);
		LayoutParams params = getWindow().getAttributes();
		if (width == 0)
			params.width = LayoutParams.MATCH_PARENT;
		else
			params.width = width;

		setTitle(R.string.txt_barcode_title);
		getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		ButterKnife.inject(this);
		setBarcodeData();
	}

	public void setBarcodeData() {

		Bitmap bcBitmap = BarcodeUtil.generateBarcode(ticket.getBarcode(),
				BarcodeFormat.EAN_13, width / 2);

		if (bcBitmap != null) {
			barcodeImg.setImageBitmap(bcBitmap);
		}

		barcodeView.setText(ticket.getBarcode());
	}

	@OnClick(R.id.b_imprimir)
	void imprimirBarcode() {
		new AsyncTask<Void, Void, Integer>() {
			
			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				if (!pDialog.isShowing())
					pDialog.show();
			}

			@Override
			protected Integer doInBackground(Void... params) {
				// TODO Auto-generated method stub
				return controller.printReceipt(ticket);
			}
			
			protected void onPostExecute(Integer result) {
				if (pDialog.isShowing())
					pDialog.dismiss();
				
				switch (result) {
				case 0:
					dismiss();
					break;

				default:
					Toast.makeText(context, R.string.error_impresion, Toast.LENGTH_LONG).show();
					break;
				}
			};
		}.execute();
	}

	@OnClick(R.id.b_cancelar)
	void cancelar() {
		cancel();
	}

}
