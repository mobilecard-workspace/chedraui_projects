package addcel.chedrauipos.ui.dialog;

import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.Url;
import org.addcel.android.crypto.Cifrados;

import addcel.chedrauipos.util.RequestBuilder;
import addcel.http.Client;
import addcel.http.ClientFactory;
import addcel.http.ClientTaskFactory;
import addcel.http.HttpListener;
import addcel.util.ErrorUtil;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.TextKeyListener;
import android.view.WindowManager.LayoutParams;
import android.widget.EditText;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class LoginDialog extends Dialog {

	@InjectView(R.id.text_login)
	EditText loginText;
	@InjectView(R.id.text_password)
	EditText passwordText;

	private Context context;
	private int width;
	private HttpListener listener;
	private static Client client;
	
	public LoginDialog(Context context, HttpListener listener) {
		this(context, listener, 0);
	}

	public LoginDialog(Context context, HttpListener listener, int width) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.listener = listener;
		this.width = width;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_login);
		LayoutParams params = getWindow().getAttributes();
		if (width == 0)
			params.width = LayoutParams.MATCH_PARENT;
		else 
			params.width = width;
		setTitle(R.string.iniciar_sesion);
		getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		ButterKnife.inject(this);
	}

	private boolean validar() {
		CharSequence login = loginText.getText();
		CharSequence password = passwordText.getText();

		if (TextUtils.isEmpty(login)) {
			loginText.setError(ErrorUtil.vacio(context, R.string.login));
			return false;
		}

		if (login.length() < 4) {
			loginText.setError(ErrorUtil.noValido(context, R.string.login));
			return false;
		}

		if (TextUtils.isEmpty(password)) {
			passwordText.setError(ErrorUtil.vacio(context, R.string.password));
			return false;
		}

		if (password.length() < 8) {
			passwordText.setError(ErrorUtil
					.noValido(context, R.string.password));
			return false;
		}

		return true;
	}

	@OnClick(R.id.btn_login)
	void ingresar() {
		if (validar()) {
			client = ClientFactory.get(
					Cifrados.SENSITIVE,
					Url.USER_GET,
					null,
					RequestBuilder.loginRequest(loginText.getText().toString()
							.trim(), passwordText.getText().toString().trim()));

			ClientTaskFactory.get(client, listener, context).execute();
		}
	}

	@OnClick(R.id.btn_login_cancelar)
	void cancelar() {
		dismiss();
	}

	@Override
	public void dismiss() {
		// TODO Auto-generated method stub
		TextKeyListener.clear(loginText.getEditableText());
		TextKeyListener.clear(passwordText.getEditableText());
		loginText.requestFocus();
		super.dismiss();
	}

	@Override
	public void cancel() {
		// TODO Auto-generated method stub
		TextKeyListener.clear(loginText.getEditableText());
		TextKeyListener.clear(passwordText.getEditableText());
		loginText.requestFocus();
		super.cancel();
	}
}