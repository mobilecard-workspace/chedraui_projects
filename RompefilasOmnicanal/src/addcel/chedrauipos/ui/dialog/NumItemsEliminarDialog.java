package addcel.chedrauipos.ui.dialog;

import org.addcel.android.chedrauipos.R;

import com.squareup.phrase.Phrase;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import addcel.chedrauipos.omnicanal.vo.RespuestaConsultarPedidoItem;
import addcel.chedrauipos.presenter.OmnicanalPresenter;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.WindowManager.LayoutParams;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

public class NumItemsEliminarDialog extends Dialog {
	
	@InjectView(R.id.msg_delete) TextView deleteMsg;
	@InjectView(R.id.picker_articulos) NumberPicker articuloPicker;
	
	private Context context;
	private RespuestaConsultarPedidoItem item;
	private OmnicanalPresenter presenter;
	private int width;

	public NumItemsEliminarDialog(Context context, OmnicanalPresenter presenter, 
			RespuestaConsultarPedidoItem item, int width) {
		super(context);
		this.context = context;
		this.presenter = presenter;
		this.item = item;
		this.width = width;
		// TODO Auto-generated constructor stub
	}

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_eliminar_numitems);
		LayoutParams params = getWindow().getAttributes();
		if (width == 0)
			params.width = LayoutParams.MATCH_PARENT;
		else
			params.width = width;

		setTitle(R.string.txt_cancel_item_title);
		getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		ButterKnife.inject(this);
		deleteMsg.setText(
				Phrase.from(
						context, R.string.txt_cancel_item_msg)
						.put("upc", item.getSku()).format());
		articuloPicker.setMaxValue(item.getTotalArticulos().intValue());
	}
	
	@OnClick(R.id.b_add)
	void agregagAeliminados() {
		int numItems = articuloPicker.getValue();
		for (int i = 0; i < numItems; i++) {
			presenter.getItemsAEliminar().add(item.getSku());
		}
		presenter.alterDeleteSize(presenter.getItemsAEliminar().size());
		dismiss();
	}
	
	@Override
	public void cancel() {
		// TODO Auto-generated method stub
		for (String upc : presenter.getItemsAEliminar()) {
			if (TextUtils.equals(upc, item.getSku()))
				presenter.getItemsAEliminar().remove(upc);
		}
		Toast.makeText(context, "El artículo no se cancelará.", Toast.LENGTH_SHORT).show();
		super.cancel();
	}
}
