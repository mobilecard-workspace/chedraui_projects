package addcel.chedrauipos.ui.dialog;

import java.util.List;

import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.UserSession;

import addcel.chedrauipos.presenter.PrinterConfPresenter;
import addcel.chedrauipos.ui.adapter.BtAdapter;
import android.app.Dialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.Bundle;
import android.view.WindowManager.LayoutParams;
import android.widget.Spinner;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.OnItemSelected.Callback;

public class PrinterConfDialog extends Dialog {
	
	@InjectView(R.id.spinner_bt)
	Spinner btSpinner;
	
	private Context context;
	private PrinterConfPresenter presenter;
	private int width;
	private BtAdapter adapter;

	public PrinterConfDialog(Context context, List<BluetoothDevice> devices, int width) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.presenter = new PrinterConfPresenter(context, devices);
		this.width = width;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_impresora);
		LayoutParams params = getWindow().getAttributes();
		if (width == 0)
			params.width = LayoutParams.MATCH_PARENT;
		else 
			params.width = width;
		setTitle(R.string.txt_impresoras_title);
		getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		ButterKnife.inject(this);
		
		adapter = new BtAdapter(context, presenter.getDevices());
		btSpinner.setAdapter(adapter);
		
		presenter.setInitialDevicePosition(UserSession.get().getImpresora(), btSpinner);
	}
	
	@OnItemSelected(value = R.id.spinner_bt, 
			callback = Callback.ITEM_SELECTED)
	void seleccionaImpresora(int position) {
		UserSession.get().setImpresora(
				presenter.getDevices().get(position));
	}
	
	@OnItemSelected(value = R.id.spinner_bt, 
			callback = Callback.NOTHING_SELECTED)
	void deseleccionaImpresora() {
		UserSession.get().setImpresora(null);
	}
	
	@OnClick(R.id.b_cancelar)
	void cancelar() {
		cancel();
	}
	
	@OnClick(R.id.b_ok)
	void aceptar() {
		if (UserSession.get().getImpresora() == null) {
			UserSession.get().setImpresora(
					(BluetoothDevice) btSpinner.getSelectedItem());
		}

		dismiss();
	}

}
