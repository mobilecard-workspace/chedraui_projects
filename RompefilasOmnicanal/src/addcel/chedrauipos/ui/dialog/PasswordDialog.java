package addcel.chedrauipos.ui.dialog;

import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.Url;
import org.addcel.android.chedrauipos.UserSession;
import org.addcel.android.crypto.Cifrados;

import com.activeandroid.util.Log;

import addcel.chedrauipos.util.RequestBuilder;
import addcel.http.Client;
import addcel.http.ClientFactory;
import addcel.http.ClientTaskFactory;
import addcel.http.HttpListener;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.TextKeyListener;
import android.view.WindowManager.LayoutParams;
import android.widget.EditText;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class PasswordDialog extends Dialog {
	
	@InjectView(R.id.text_pass_actual)
	EditText actualText;
	@InjectView(R.id.text_pass_nuevo)
	EditText nuevoText;
	@InjectView(R.id.text_pass_nuevo_conf)
	EditText nuevoConfText;
	
	private Context context;
	private int width;
	private HttpListener listener;
	private static Client client;
	
	private static final String TAG = PasswordDialog.class.getSimpleName();

	public PasswordDialog(Context context, HttpListener listener) {
		this(context, listener, 0);
		// TODO Auto-generated constructor stub
	}
	
	public PasswordDialog(Context context, HttpListener listener, int width) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.listener = listener;
		this.width = width;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_password);
		LayoutParams params = getWindow().getAttributes();
		if (width == 0)
			params.width = LayoutParams.MATCH_PARENT;
		else 
			params.width = width;
		setTitle(R.string.txt_actualizar_seguridad);
		getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		ButterKnife.inject(this);
	}
	
	private boolean validar() {
		CharSequence pActual = actualText.getText();
		CharSequence pNuevo = nuevoText.getText();
		CharSequence pConf = nuevoConfText.getText();
		
		if (TextUtils.isEmpty(pActual)) {
			actualText.setError("Captura tu contraseña actual");
			return false;
		}
		
		if (TextUtils.isEmpty(pNuevo)) {
			nuevoText.setError("Captura tu nueva contraseña");
			return false;
		}
		
		if (pNuevo.length() < 8) {
			nuevoText.setError("Tu nueva contraseña no es válida.");
			return false;
		}
		
		if (!TextUtils.equals(pNuevo, pConf)) {
			nuevoConfText.setError("La confirmación no ha sido exitosa.");
			return false;
		}
		
		return true;
	}
	
	@OnClick(R.id.b_actualizar)
	void actualizar() {
		if (validar()) {
			String params = RequestBuilder.passwordRequest(
					UserSession.get().getLogin().getIdUsuario(),
					actualText.getText(),
					nuevoText.getText());
			Log.d(TAG, params);
			
			client = ClientFactory.get(
					Cifrados.SENSITIVE,
					Url.PASSWORD_UPDATE,
					null,params);

			ClientTaskFactory.get(client, listener, context).execute();
		}
	}

	@OnClick(R.id.b_cancelar)
	void cancelar() {
		dismiss();
	}

	@Override
	public void dismiss() {
		// TODO Auto-generated method stub
		TextKeyListener.clear(actualText.getText());
		TextKeyListener.clear(nuevoText.getText());
		TextKeyListener.clear(nuevoConfText.getText());
		actualText.requestFocus();
		super.dismiss();
	}

	@Override
	public void cancel() {
		// TODO Auto-generated method stub
		TextKeyListener.clear(actualText.getText());
		TextKeyListener.clear(nuevoText.getText());
		TextKeyListener.clear(nuevoConfText.getText());
		actualText.requestFocus();
		super.cancel();
	}

}
