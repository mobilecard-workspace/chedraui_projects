package addcel.chedrauipos.ui.dialog;

import org.addcel.android.chedrauipos.AppStatus;
import org.addcel.android.chedrauipos.R;

import addcel.chedrauipos.model.Articulo;
import addcel.chedrauipos.model.Ticket;
import addcel.chedrauipos.presenter.OmnicanalPresenter;
import addcel.chedrauipos.util.BarcodeGenerationUtil;
import addcel.chedrauipos.util.BarcodeUtil;
import addcel.util.ProgressDialogFactory;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import com.google.zxing.BarcodeFormat;

public class OmnicanalBarcodeDialog extends Dialog {

	@InjectView(R.id.img_barcode)
	ImageView barcodeImg;
	@InjectView(R.id.view_barcode)
	TextView barcodeView;

	private Context context;
	private int width;
	private OmnicanalPresenter presenter;
	private ProgressDialog pDialog;

	public OmnicanalBarcodeDialog(Context context, int width,
			OmnicanalPresenter presenter) {
		super(context);
		this.context = context;
		this.width = width;
		this.presenter = presenter;
		this.pDialog = ProgressDialogFactory.get(this.context,
				R.string.txt_imprimir_msg);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_barcode);
		LayoutParams params = getWindow().getAttributes();
		if (width == 0)
			params.width = LayoutParams.MATCH_PARENT;
		else
			params.width = width;

		setTitle(R.string.txt_barcode_title);
		getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		ButterKnife.inject(this);
		setBarcodeData();
	}

	public void setBarcodeData() {

		String consecutivo = BarcodeGenerationUtil.get().generaNSTicket(
				context, AppStatus.get(context).getConsecutivo());
		String barcode = BarcodeGenerationUtil.get().generaCodigoBarrasTicket(
				consecutivo,
				String.valueOf(presenter.getPedido().getPedido().getTotal()
						.doubleValue()));

		Bitmap bcBitmap = BarcodeUtil.generateBarcode(barcode,
				BarcodeFormat.EAN_13, width / 2);

		if (bcBitmap != null) {
			barcodeImg.setImageBitmap(bcBitmap);
		}

		barcodeView.setText(barcode);
	}

	@OnClick(R.id.b_imprimir)
	void imprimirBarcode() {
		presenter.printReceiptAsync();
	}

	@OnClick(R.id.b_cancelar)
	void cancelar() {
		cancel();
	}

}
