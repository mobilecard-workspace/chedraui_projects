package addcel.chedrauipos.ui;

import java.util.List;

import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.UserSession;

import addcel.chedrauipos.presenter.DatosPresenter;
import addcel.chedrauipos.ui.callback.DatosCallback;
import addcel.chedrauipos.vo.DefaultResponse;
import addcel.chedrauipos.vo.LoginResponse;
import addcel.chedrauipos.vo.Role;
import addcel.chedrauipos.vo.Tienda;
import addcel.util.AddcelTextUtil;
import addcel.util.ErrorUtil;
import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.ButterKnife.Action;
import butterknife.InjectView;
import butterknife.InjectViews;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.OnTextChanged.Callback;

public class DatosActivity extends Activity implements DatosCallback {
	
	@InjectViews({R.id.view_login, R.id.view_email, R.id.view_nombres, 
		R.id.view_paterno, R.id.view_materno, R.id.view_tienda,
		R.id.view_role,})
	List<TextView> titleViews;
	@InjectView(R.id.text_login) EditText loginText;
	@InjectView(R.id.text_email) EditText emailText;
	@InjectView(R.id.text_nombre) EditText nombreText;
	@InjectView(R.id.text_paterno) EditText paternoText;
	@InjectView(R.id.text_materno) EditText maternoText;
	@InjectView(R.id.spinner_tienda) Spinner tiendaSpinner;
	@InjectView(R.id.spinner_role) Spinner roleSpinner;
	@InjectView(R.id.b_registrar) ImageView actualizarButton;
	
	private boolean loginValido, emailValido, nombreValido, paternoValido;
	
	private DatosPresenter presenter;
	private ArrayAdapter<Tienda> tiendaAdapter;
	private ArrayAdapter<Role> roleAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registro);
		ButterKnife.inject(this);
		ButterKnife.apply(titleViews, VISIBLE);
		
		loginText.setEnabled(false);
		actualizarButton.setImageResource(R.drawable.s_actualizar);
		 
		presenter = new DatosPresenter(this, this);
		tiendaAdapter = new ArrayAdapter<Tienda>(this,
				android.R.layout.simple_spinner_item, presenter.getTiendas());
		tiendaAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		tiendaSpinner.setAdapter(tiendaAdapter);
		
		roleAdapter = new ArrayAdapter<Role>(this,
				android.R.layout.simple_spinner_item, presenter.getRoles());
		roleAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		roleSpinner.setAdapter(roleAdapter);
		
		presenter.loadTiendasAsync();
	}
	
	@OnTextChanged(value = R.id.text_login, callback = Callback.AFTER_TEXT_CHANGED)
	void validaLogin(Editable s) {
		 if (TextUtils.isEmpty(s)) {
			 loginText.setError("Campo obligatorio");
			 loginValido = false;
		 } else if (TextUtils.getTrimmedLength(s) < 4) {
			 loginText.setError("Longitud mínima 4 caracteres");
			 loginValido = false;
		 } else
			 loginValido = true;
		 
			 
	}
	
	@OnTextChanged(value = R.id.text_email, callback = Callback.AFTER_TEXT_CHANGED)
	void validaEmail(Editable s) {
		if (TextUtils.isEmpty(s)) {
			 emailText.setError("Campo obligatorio");
			 emailValido = false;
		 } else if (!AddcelTextUtil.esCorreo(s.toString())) {
			 emailText.setError("Correo electrónico no válido");
			 emailValido = false;
		 } else
			 emailValido = true;
	}
	
	@OnTextChanged(value = R.id.text_nombre, callback = Callback.AFTER_TEXT_CHANGED)
	void validaNombre(Editable s) {
		if (TextUtils.isEmpty(s)) {
			 nombreText.setError("Campo obligatorio");
			 nombreValido = false;
		} else
			nombreValido = true;
	}
	
	@OnTextChanged(value = R.id.text_paterno, callback = Callback.AFTER_TEXT_CHANGED)
	void validaPaterno(Editable s) {
		if (TextUtils.isEmpty(s)) {
			 paternoText.setError("Campo obligatorio");
			 paternoValido = false;
		} else
			paternoValido = true;
	}
	
	private boolean validate() {
		return loginValido && emailValido && nombreValido && paternoValido;
			
	}
	
	
	@OnClick(R.id.b_registrar)
	void actualizar() {
		if (validate()) {
			presenter.actualizarDatosAsync(
					loginText.getText().toString().trim(), 
					emailText.getText().toString().trim(), 
					nombreText.getText().toString().trim(), 
					paternoText.getText().toString().trim(), 
					maternoText.getText().toString().trim(), 
					(Tienda) tiendaSpinner.getSelectedItem(), 
					(Role) roleSpinner.getSelectedItem());
		} else {
			Toast.makeText(this, R.string.error_datos_validos, Toast.LENGTH_LONG).show();
		}
	}
	
	@Override
	public void onTiendasReceived(List<Tienda> tiendas) {
		// TODO Auto-generated method stub
		if (tiendas != null && !tiendas.isEmpty()) {
			presenter.setTiendas(tiendas);
			tiendaAdapter.notifyDataSetChanged();
			presenter.loadRolesAsync();
		
		} else {
			Toast.makeText(this, ErrorUtil.errorLista(this, "sucursales"), Toast.LENGTH_LONG).show();
			finish();
		}
	}
	
	@Override
	public void onRolesReceived(List<Role> roles) {
		// TODO Auto-generated method stub
		if (roles != null && !roles.isEmpty()) {
			presenter.setRoles(roles);
			roleAdapter.notifyDataSetChanged();
			
			loginText.setText(presenter.getData().getLogin());
			emailText.setText(presenter.getData().getMail());
			nombreText.setText(presenter.getData().getNombre());
			paternoText.setText(presenter.getData().getApellido());
			maternoText.setText(presenter.getData().getMaterno());
			tiendaSpinner.setSelection(
					presenter.getSelectedTiendaIndex(
							presenter.getData().getTienda()));
			roleSpinner.setSelection(
					presenter.getSelectedRoleIndex(
							presenter.getData().getRole()));
			
			if (presenter.getData().getRole().getIdRol() == 100)
				roleSpinner.setEnabled(false);
			
		} else {
			Toast.makeText(this, ErrorUtil.errorLista(this, "roles"), Toast.LENGTH_LONG).show();
			finish();
		}
	}
	
	@Override
	public void onDataUpdated(DefaultResponse response, LoginResponse data) {
		// TODO Auto-generated method stub
		if (response == null) {
			Toast.makeText(this, 
					ErrorUtil.errorServicio(this, "actualización"),
					Toast.LENGTH_SHORT)
			.show();
		} else {
			
			switch (response.getIdError()) {
			case 0:
				UserSession.get().setLogin(data);
				
				Toast.makeText(this, response.getMensajeError(),
						Toast.LENGTH_LONG).show();
				finish();
				break;

			default:
				Toast.makeText(this, response.getMensajeError(),
						Toast.LENGTH_LONG).show();
				break;
			}
		}
	}
	
	/**
	 * Asigna {@link View#VISIBLE} a {@code List} de {@code View}
	 */
	static final Action<View> VISIBLE = new Action<View>() {

		@Override
		public void apply(View arg0, int arg1) {
			// TODO Auto-generated method stub
			arg0.setVisibility(View.VISIBLE);
		}
		
	};
}
