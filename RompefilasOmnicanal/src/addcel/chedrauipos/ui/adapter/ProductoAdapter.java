package addcel.chedrauipos.ui.adapter;

import java.util.List;

import org.addcel.android.chedrauipos.R;

import addcel.chedrauipos.model.Articulo;
import addcel.chedrauipos.ui.callback.ArticuloDeletedCallback;
import addcel.otto.BusFactory;
import addcel.util.AddcelTextUtil;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class ProductoAdapter extends BaseAdapter {
	
	private Context context;
	private List<Articulo> data;
	private ArticuloDeletedCallback callback;
	
	public ProductoAdapter(Context context, List<Articulo> data,
			ArticuloDeletedCallback callback) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.data = data;
		this.callback = callback;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.item_articulo, parent, false);
			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		Articulo a = (Articulo) getItem(position);
		viewHolder.precioView.setText(AddcelTextUtil.formatCurrency(a.getMontoTotal()));
		viewHolder.articuloView.setText(a.getDescripcion());
		
		final int pos = position;
		viewHolder.eliminarButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				callback.onArticuloDeleted(pos);
			}
		});
		
		return convertView;
	}
	
	static class ViewHolder {
		@InjectView(R.id.view_precio)
		TextView precioView;
		@InjectView(R.id.view_articulo)
		TextView articuloView;
		@InjectView(R.id.b_eliminar)
		ImageButton eliminarButton;
		
		public ViewHolder(View view) {
			ButterKnife.inject(this, view);
			eliminarButton.setFocusable(false);
			eliminarButton.setFocusableInTouchMode(false);
		}
		
	}

}
