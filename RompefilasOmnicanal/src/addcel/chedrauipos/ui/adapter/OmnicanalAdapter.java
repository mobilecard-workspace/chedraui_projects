package addcel.chedrauipos.ui.adapter;

import java.math.BigDecimal;
import java.util.List;

import org.addcel.android.chedrauipos.R;

import com.squareup.phrase.Phrase;

import addcel.chedrauipos.omnicanal.vo.PromocionVO;
import addcel.chedrauipos.omnicanal.vo.RespuestaConsultarPedidoItem;
import addcel.chedrauipos.ui.callback.OmnicanalCallback;
import addcel.util.AddcelTextUtil;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;

public class OmnicanalAdapter extends BaseAdapter {
	
	private Context context;
	private List<RespuestaConsultarPedidoItem> data;
	private OmnicanalCallback callback;
	
	public OmnicanalAdapter(Context context, List<RespuestaConsultarPedidoItem> data, OmnicanalCallback callback) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.data = data;
		this.callback = callback;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return (long) position;
	}
	

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}
	
	@Override
	public boolean isEnabled(int position) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		
		if (convertView == null) {
			convertView = LayoutInflater.
					from(context).inflate(R.layout.item_omnicanal, parent, false);
			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		final RespuestaConsultarPedidoItem item = (RespuestaConsultarPedidoItem) getItem(position);
		final int pos = position;
		
		viewHolder.existsCheck.setEnabled(false);
		
		if (item.isSeleccionado()) {
			viewHolder.existsCheck.setChecked(true);
		} else {
			viewHolder.existsCheck.setChecked(false);
		}
		
		viewHolder.totalView.setText(
				AddcelTextUtil.formatCurrency(item.getPrecio()));
		viewHolder.articuloView.setText(item.getDescripcion());
		viewHolder.cantidadView.setText(setCantidad(item.getEsPesable(), item.getTotalArticulos()));
		viewHolder.cancelarButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				callback.onItemAddedToDeleteList(item);
			}
		});
		
		return convertView;
	}
	
	
	private CharSequence setCantidad(String esPesable, BigDecimal totalArticulos) {
		if (TextUtils.equals(esPesable, "0")) {
			return Phrase.from(context, R.string.txt_cantidad).put("cantidad",
					String.valueOf(totalArticulos.intValue())).format();
		} else {
			return Phrase.from(context, R.string.txt_cantidad).put("cantidad",
					String.valueOf(totalArticulos.doubleValue())).format();
		}
		
	}
	
	static class ViewHolder {
		@InjectView(R.id.b_item_cancelar)
		ImageButton cancelarButton;
		@InjectView(R.id.view_total)
		TextView totalView;
		@InjectView(R.id.view_articulo)
		TextView articuloView;
		@InjectView(R.id.view_cantidad)
		TextView cantidadView;
		@InjectView(R.id.check_exists)
		CheckBox existsCheck;
		
		public ViewHolder(View _aux) {
			// TODO Auto-generated constructor stub
			ButterKnife.inject(this, _aux);
			cancelarButton.setFocusable(false);
			cancelarButton.setFocusableInTouchMode(false);
		}
	}
	
	

}
