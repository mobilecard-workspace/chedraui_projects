package addcel.chedrauipos.ui.adapter;

import java.util.List;

import org.addcel.android.chedrauipos.R;
import org.joda.time.DateTime;

import com.squareup.picasso.Picasso;

import addcel.chedrauipos.model.Ticket;
import addcel.chedrauipos.ui.callback.DepositarTicketCallback;
import addcel.util.AddcelTextUtil;
import android.content.Context;
import android.opengl.Visibility;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;

public class TicketAdapter extends BaseAdapter {

	private Context context;
	private List<Ticket> data;
	private DepositarTicketCallback callback;

	public TicketAdapter(Context context, List<Ticket> data,
			DepositarTicketCallback callback) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.data = data;
		this.callback = callback;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.item_ticket, parent, false);
			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		Ticket t = (Ticket) getItem(position);
		viewHolder.totalView
				.setText(AddcelTextUtil.formatCurrency(t.getTotal()));
		viewHolder.fechaView.setText(new DateTime(t.getFecha())
				.toString("dd/MM/yyyy HH:mm:ss"));

		if (!t.isProcesado()) {
			final int pos = position;
			Picasso.with(context).load(R.drawable.b_enviar)
					.into(viewHolder.enviarButton);
			
			viewHolder.enviarButton
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							callback.launchDeposito(pos);
						}
					});
		} else {
			
			Picasso.with(context).load(R.drawable.b_success)
					.into(viewHolder.enviarButton);
		}

		return convertView;
	}

	static class ViewHolder {

		@InjectView(R.id.b_enviar)
		ImageButton enviarButton;
		@InjectView(R.id.view_precio)
		TextView totalView;
		@InjectView(R.id.view_fecha)
		TextView fechaView;

		public ViewHolder(View view) {
			ButterKnife.inject(this, view);
			enviarButton.setFocusable(false);
			enviarButton.setFocusableInTouchMode(false);
		}
	}

}
