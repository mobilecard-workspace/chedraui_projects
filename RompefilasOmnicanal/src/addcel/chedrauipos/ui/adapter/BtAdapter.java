package addcel.chedrauipos.ui.adapter;

import java.util.List;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class BtAdapter extends ArrayAdapter<BluetoothDevice> {

	private Context context;

	public BtAdapter(Context context, List<BluetoothDevice> data) {
		// TODO Auto-generated constructor stub
		super(context, android.R.layout.simple_list_item_1, data);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		View view = convertView;

		if (view == null) {
			view = LayoutInflater.from(context).inflate(
					android.R.layout.simple_list_item_1, parent, false);

		}
		
		BluetoothDevice bt = (BluetoothDevice) getItem(position);
		
		if (bt != null)
			((TextView)view.findViewById(android.R.id.text1)).setText(bt.getName());

		return view;
	}
	
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;

		if (view == null) {
			view = LayoutInflater.from(context).inflate(
					android.R.layout.simple_dropdown_item_1line, parent, false);

		}
		
		BluetoothDevice bt = (BluetoothDevice) getItem(position);
		
		if (bt != null)
			((TextView)view.findViewById(android.R.id.text1)).setText(bt.getName());

		return view;
	}

}
