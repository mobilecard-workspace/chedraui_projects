package addcel.chedrauipos.ui;

import java.util.Arrays;

import org.addcel.android.chedrauipos.AppStatus;
import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.ServicesStatus;
import org.joda.time.DateTime;

import addcel.chedrauipos.model.Articulo;
import addcel.chedrauipos.omnicanal.Constantes;
import addcel.chedrauipos.omnicanal.vo.AbstractVO;
import addcel.chedrauipos.omnicanal.vo.DatosTransaccion;
import addcel.chedrauipos.omnicanal.vo.PedidoResponse;
import addcel.chedrauipos.omnicanal.vo.RespuestaConsultarPedidoItem;
import addcel.chedrauipos.presenter.OmnicanalPresenter;
import addcel.chedrauipos.ui.adapter.OmnicanalAdapter;
import addcel.chedrauipos.ui.callback.OmnicanalCallback;
import addcel.chedrauipos.ui.dialog.BarcodeDialog;
import addcel.chedrauipos.ui.dialog.NumItemsEliminarDialog;
import addcel.chedrauipos.ui.dialog.OmnicanalBarcodeDialog;
import addcel.otto.BusFactory;
import addcel.util.AddcelTextUtil;
import addcel.util.DeviceUtils;
import addcel.version.Version;
import addcel.version.VersionProcessingServiceFactory;
import addcel.version.VersionStatus;
import addcel.version.VersionUtil;
import addcel.version.dialog.VersionDialogFactory;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.method.TextKeyListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.OnTextChanged.Callback;

import com.squareup.otto.Subscribe;
import com.squareup.phrase.Phrase;

public class OmnicanalActivity extends Activity implements OmnicanalCallback {
	
	@InjectView(R.id.text_upc) EditText upcText;
	@InjectView(R.id.list_articulos) ListView articulosList;
	@InjectView(R.id.view_total) TextView totalView;
	@InjectView(R.id.view_numart) TextView numArtView;
	@InjectView(R.id.view_actualizacion) TextView actualizacionView;
	@InjectView(R.id.view_descripcion) TextView descripcionView;
	@InjectView(R.id.view_cancelados) TextView canceladosView;
	
	private AlertDialog pedidoDialog, tipoCancelacionDialog;
	private OmnicanalPresenter presenter;
	private OmnicanalAdapter adapter;
	private OmnicanalBarcodeDialog bDialog;
	private NumItemsEliminarDialog cancelarItemsDialog;
	
	private final Context context = this;
	private static final String TAG = OmnicanalActivity.class.getSimpleName();
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.inject(this);
		
		if (presenter == null)
			presenter = new OmnicanalPresenter(context, this);
		
		adapter = new OmnicanalAdapter(context, presenter.getItemsPedido(),
				this);
		articulosList.setAdapter(adapter);
		
		setTotal(0);
		setNumArticulos(0);
		setCancelados(0);
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = new MenuInflater(context);
		inflater.inflate(R.menu.menu_omnicanal, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		int id = item.getItemId();

		switch (id) {
		case R.id.a_pedido:
			if (!ServicesStatus.areServicesRunning())
				getPedidoDialog().show();
			else
				Toast.makeText(context, R.string.error_servicio_ejecutando, 
						Toast.LENGTH_SHORT).show();
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		BusFactory.get().register(context);
		
		if (!getPedidoDialog().isShowing())
			getPedidoDialog().show();
		
		setActualizacion(AppStatus.get(context).getFechaActualizacion());
		setTotal(0);
		setNumArticulos(0);
		
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		BusFactory.get().unregister(context);
	}
	
	@OnTextChanged(value = R.id.text_upc, callback = Callback.AFTER_TEXT_CHANGED)
	void onUpcCaptured(Editable s) {
		if (TextUtils.indexOf(s, "\n") == (s.length() - 1)) {
			Log.d("afterTextChanged", "Encuentra char");
			if (s.length() > 0) {
				String upc = s.toString().trim();
				TextKeyListener.clear(s);
				if(TextUtils.isDigitsOnly(upc)) {
					if (!ServicesStatus.areServicesRunning())
						presenter.consultarArticuloAsync(upc, false);
					else
						Toast.makeText(context, R.string.error_servicio_ejecutando, Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(context, Phrase.from("Upc {upc} no válido").put("upc", upc).format(), Toast.LENGTH_SHORT).show();
				}
			}
		}
	}
	
	private void setActualizacion(DateTime d) {
		CharSequence dateSequence = Phrase.from(this, R.string.actualizacion).put("fecha", 
				d.toString("dd/MM/yyyy hh:mm:ss")).format();
		actualizacionView.setText(dateSequence);
	}
	
	private void setTotal(double total) {
		CharSequence totalSequence = Phrase.from(this, R.string.total).put("total", 
				AddcelTextUtil.formatCurrency(total)).format();
		totalView.setText(totalSequence);
	}
	
	private void setNumArticulos(int numArticulos) {
		CharSequence numArtSequence = Phrase.from(this, R.string.num_articulos).put("numart", 
				Integer.toString(numArticulos)).format();
		numArtView.setText(numArtSequence);
	}
	
	private void setCancelados(int cancelados) {
		CharSequence numCancelados = Phrase.from(this, R.string.total_cancelados).put("cancelados",
				String.valueOf(cancelados)).format();
		canceladosView.setText(numCancelados);
	}
	
	@OnClick(R.id.b_eliminar)
	void cancelarPedido() {
		if (presenter.getPedido() != null) {
			if (!presenter.getItemsAEliminar().isEmpty()) {
				Log.d(TAG, "Seleccionar tipo de cancelacion");
				getTipoCancelacionDialog().show();
				
			} else {
				Log.d(TAG, "Lanza directo cancelación parcial");
				CharSequence s = Phrase.from(context, R.string.txt_msg_cancelar)
						.put("increment_id", 
								presenter.getPedido().getPedido().getNumeroPedido()).format();
				
				presenter.getCancelarDialog().setMessage(s);
				presenter.getCancelarDialog().show();
			}
		} else {
			Toast.makeText(context, R.string.error_pedido_nulo_cancelar, Toast.LENGTH_SHORT).show();
		}
	}
	
	@OnClick(R.id.b_pagar)
	void depositarArchivo() {
		if (!ServicesStatus.areServicesRunning()) {
			
			Log.d(TAG, "Items pedido seleccionados: " + articulosList.getCheckedItemCount());
			Log.d(TAG, "Items pedido : " + presenter.getItemsPedido().size());
			
			if (articulosList.getCheckedItemCount() == presenter.getItemsPedido().size()) {
				presenter.depositarArchivoAsync();
			} else {
				Toast.makeText(context, R.string.error_picking, Toast.LENGTH_SHORT).show();
			}
				
		} else {
			Toast.makeText(context, R.string.error_pedido_nulo_cancelar, Toast.LENGTH_SHORT).show();
		}
	}
	
	
	public AlertDialog getPedidoDialog() {
		if (pedidoDialog == null)
			pedidoDialog = presenter.buildPedidoDialog();
		
		return pedidoDialog;
	}
	
//	Callbacks de interfaz OmnicanalCallback
	public AlertDialog getTipoCancelacionDialog() {
		if (tipoCancelacionDialog == null)
			tipoCancelacionDialog = presenter.buildTipoCancelacionDialog();
		
		return tipoCancelacionDialog;
	}
	

	@Override
	public void onPedidoConsultado(PedidoResponse pedido) {
		// TODO Auto-generated method stub
		if (pedido == null) {
			Toast.makeText(context, R.string.error_pedido, Toast.LENGTH_LONG).show();
		} else {
			
			switch (pedido.getIdError()) {
			case 0:

//				if (TextUtils.equals(pedido.getPedido().getEstado(),
//						Constantes.STATUS_EN_PROCESO_SURTIDO)) {
				
					Toast.makeText(context, pedido.getMsgError(), Toast.LENGTH_SHORT).show();
					
					presenter.setPedido(pedido);
					presenter.setItemsPedido(Arrays.asList(pedido.getPedido().getItemsp()));
					presenter.consultarUpcsPedidoAsync();
//				} else {
//					CharSequence msg = Phrase.from(context, R.string.error_picking_realizado)
//							.put("pedido", pedido.getPedido().getNumeroPedido())
//							.format();
//					Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
//				}
				
				break;

			default:

				Toast.makeText(context, pedido.getMsgError(), Toast.LENGTH_LONG).show();
				break;
			}
			
		}
	}

	@Override
	public void onUpcsConsultados(long[] upc) {
		// TODO Auto-generated method stub
		if (upc == null) {
			Toast.makeText(
					context, R.string.error_pedido_incompleto,
					Toast.LENGTH_SHORT)
					.show();
			finish();
			
		} else {
			for (long l : upc) {
				
				if (l == 0L) {
					Log.d(TAG, "Upc no existe, valor: " + l);
					Toast.makeText(
							context, R.string.error_pedido_incompleto,
							Toast.LENGTH_SHORT)
							.show();
					finish();
					break;
				} 
				
				presenter.setStatusCantidad(new int[presenter.getItemsPedido().size()]);
				setTotal(presenter.getPedido().getPedido().getTotal().doubleValue());
				setNumArticulos(presenter.getPedido().getPedido().getItemsp().length);
			}
			
			adapter.notifyDataSetChanged();
			
		}
	}

	@Override
	public void onArticuloConsultado(Articulo articulo) {
		// TODO Auto-generated method stub
		if (articulo != null) {
			for (int i = 0; i < presenter.getItemsPedido().size(); i++) {
				
				RespuestaConsultarPedidoItem item = presenter.getItemsPedido().get(i);
				
				if (item.getSku().contains(String.valueOf(articulo.getUpc()))) {
					
					if (TextUtils.equals(item.getEsPesable(), "0")){
						/*
						 * En caso de que la consulta sea exitosa y el artículo
						 * no sea un pesable, aumentamos en 1 la cantidad de escaneos
						 * exitosos
						 */
						presenter.getStatusCantidad()[i]++;
						
						/**
						 * Validamos si la cantidad de escaneos exitosos es menor a
						 *  {@link RespuestaConsultarPedidoItem#getTotalArticulos()} en el
						 * {@code RespuestaConsultarPedidoItem} actual.
						 */
						if (presenter.getStatusCantidad()[i] < item.getTotalArticulos().intValue()) {
							Toast.makeText(context,
									presenter.buildMensajeCantidad(presenter.getStatusCantidad()[i],
									item), Toast.LENGTH_SHORT).show();
						} else {
							articulosList.setItemChecked(i, true);
							item.setSeleccionado(true);
							adapter.notifyDataSetChanged();
						}
					} else {
						articulosList.setItemChecked(i, true);
						item.setSeleccionado(true);
						adapter.notifyDataSetChanged();
					}
				}
			}
		} else {
			Toast.makeText(context, R.string.error_upc_noexiste, Toast.LENGTH_SHORT).show();
		}
	}
	
	@Override
	public void onArchivoDepositado(int result) {
		// TODO Auto-generated method stub
		switch (result) {
		case 0:
			
//			DatosTransaccion request = DatosTransaccion.get(300, 
//					presenter.getPedido().getPedido().getNumeroPedido(),
//					AppStatus.get(context).getCodigo());
//			presenter.lanzarOperacion(request);
			onStatusModificado(AbstractVO.getDummySuccess());
			break;

		case 404:
			Toast.makeText(context, R.string.error_pedido_vacio, Toast.LENGTH_LONG).show();
			break;
		
		default:
			Toast.makeText(context, R.string.error_deposito, Toast.LENGTH_LONG).show();
		}
	}
	
	@Override
	public void onStatusModificado(AbstractVO response) {
		// TODO Auto-generated method stub
		if (response == null) {
			Toast.makeText(context, R.string.error_cambio_status, Toast.LENGTH_SHORT).show();
		} else {
			switch (response.getIdError()) {
			case 0:
//				TODO logica para imprimir ticket
				bDialog = new OmnicanalBarcodeDialog(context, 
						DeviceUtils.getWidth(context) /2, presenter);
				bDialog.show();
				break;

			default:
				Toast.makeText(context, response.getMsgError(), Toast.LENGTH_LONG).show();
				break;
			}
		}
	}
	
	@Override
	public void onTicketImpreso(int result) {
		// TODO Auto-generated method stub
		switch (result) {
		case 0:
			
			break;

		default:
			break;
		}
	}
	
	@Override
	public void onStatusCancelado(AbstractVO response) {
		// TODO Auto-generated method stub
		
		if (presenter.getCancelarDialog().isShowing())
			presenter.getCancelarDialog().dismiss();
		
		if (response == null) {
			Toast.makeText(context, R.string.error_cambio_status, Toast.LENGTH_SHORT).show();
		} else {
			switch (response.getIdError()) {
			case 0:
				presenter.getItemsPedido().clear();
				presenter.getItemsAEliminar().clear();
				presenter.setPedido(null);
				finish();
				Toast.makeText(context, response.getMsgError(), Toast.LENGTH_LONG).show();
				break;

			default:
				Toast.makeText(context, response.getMsgError(), Toast.LENGTH_SHORT).show();
				break;
			}
		}
		
	}

	@Override
	public void onItemAddedToDeleteList(final RespuestaConsultarPedidoItem item) {
		// TODO Auto-generated method stub
		if (item != null) {
			if (TextUtils.equals(item.getEsPesable(), "0") 
					&& item.getTotalArticulos().intValue() > 1) {
				cancelarItemsDialog = new NumItemsEliminarDialog(context, presenter, item,
						DeviceUtils.getWidth(context) / 2);
				cancelarItemsDialog.show();
			} else {
				new AlertDialog.Builder(context).setTitle(R.string.txt_cancel_item_title)
				.setMessage(Phrase.from("¿Cancelar item {sku} ?").put("sku", item.getSku()).format())
				.setPositiveButton(R.string.txt_cancel, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						presenter.getItemsAEliminar().add(item.getSku());
						presenter.alterDeleteSize(presenter.getItemsAEliminar().size());
						dialog.dismiss();
					}
				}).show();
			}
		} else {
			CharSequence error = Phrase.from(
					context, R.string.error_servicio)
					.put("servicio", "Cancelación Parcial")
					.format();
			Toast.makeText(context, error, Toast.LENGTH_LONG).show();
		}
			
	}
	
	@Override
	public void onDeleteListModified(int newSize) {
		// TODO Auto-generated method stub
		setCancelados(newSize);
	}
	
	@Override
	public void onCancelacionParcial(AbstractVO response) {
		// TODO Auto-generated method stub
			presenter.getCancelarParcialDialog().dismiss();
		
		if (response == null) {
			Toast.makeText(context, R.string.error_cambio_status, Toast.LENGTH_SHORT).show();
		} else {
			switch (response.getIdError()) {
			case 0:
				Toast.makeText(context, response.getMsgError(), Toast.LENGTH_LONG).show();
				finish();
				break;

			default:
				Toast.makeText(context, response.getMsgError(), Toast.LENGTH_SHORT).show();
				break;
			}
		}
	}

	@Override
	@Subscribe
	public void onVersionReceived(Version version) {
		// TODO Auto-generated method stub
		if (version != null) {
			
			VersionStatus status = VersionProcessingServiceFactory.get()
					.processVersion(
							VersionUtil.getDeviceVersionNumber(context,
									getClass()), version);

			switch (status) {
			case SIN_CAMBIO:
				Toast.makeText(context, R.string.version_sin_cambio,
						Toast.LENGTH_LONG).show();
				break;
			case OBLIGATORIO:
				VersionDialogFactory.get(context, R.string.version_obligatoria,
						version.getUrl()).show();
				break;
			case OPCIONAL:
				VersionDialogFactory.get(context, R.string.version_opcional,
						null, 2, version.getUrl());
				break;

			default:

				break;
			}
		}
	}

	@Override
	@Subscribe
	public void onCatalogUpdated(DateTime date) {
		// TODO Auto-generated method stub
		AppStatus.get(context).setFechaActualizacion(date);
		AppStatus.get(context).setUpdated(true);
		actualizacionView.setText(
				Phrase.from(context, R.string.actualizacion)
					.put("fecha", date.toString("dd/MM/yyyy hh:mm:ss")).format());
		
		Toast.makeText(context, R.string.exito_actualizacion, Toast.LENGTH_LONG).show();
	}

	@Override
	@Subscribe
	public void onErrorFromBus(String error) {
		// TODO Auto-generated method stub
		AppStatus.get(context).setUpdated(true);
		actualizacionView.setText(error);
		
		Toast.makeText(context, error, Toast.LENGTH_LONG).show();
	}
	
	
	
	

}
