package addcel.chedrauipos.ui;

import org.addcel.android.chedrauipos.R;

import addcel.chedrauipos.presenter.TicketController;
import addcel.chedrauipos.ui.adapter.TicketAdapter;
import addcel.chedrauipos.ui.callback.DepositarTicketCallback;
import addcel.chedrauipos.vo.DepositoResponse;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemClick;

public class TicketActivity extends Activity implements DepositarTicketCallback {
	
	@InjectView(R.id.list_tickets)
	ListView ticketList;
	@InjectView(R.id.view_description)
	TextView descriptionView;
	
	private TicketController controller;
	private TicketAdapter adapter;
	private ProgressDialog pDialog;
	private final Context context = this;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ticket);
		ButterKnife.inject(this);
		
		controller = new TicketController();
		adapter = new TicketAdapter(this, controller.getTickets(), this);
		ticketList.setAdapter(adapter);
		
		pDialog = new ProgressDialog(this);
		pDialog.setMessage(getText(R.string.msg_pdialog));
		pDialog.setCancelable(false);
		pDialog.setIndeterminate(true);
		
	}
	
	@OnItemClick(R.id.list_tickets)
	void showTicketData(int position) {
		descriptionView.setText(controller.getTicketDescripcion(context, position));
	}

	@Override
	public void launchDeposito(final int position) {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, DepositoResponse>() {
			
			protected void onPreExecute() {
				if (pDialog == null || !pDialog.isShowing())
					pDialog.show();
			};

			@Override
			protected DepositoResponse doInBackground(Void... params) {
				// TODO Auto-generated method stub
				return controller.depositFile(context, position);
			}
			
			@Override
			protected void onPostExecute(DepositoResponse result) {
				// TODO Auto-generated method stub
				if (pDialog != null && pDialog.isShowing()) 
					pDialog.dismiss();
				
				switch (result.getIdError()) {
				case 0:
					adapter.notifyDataSetChanged();
					Toast.makeText(context, result.getMensajeError(), Toast.LENGTH_SHORT).show();
					break;

				default:
					Toast.makeText(context, result.getMensajeError(), Toast.LENGTH_SHORT).show();
					break;
				}
			}
		}.execute();
	}

}
