package addcel.chedrauipos.ui.callback;

import org.joda.time.DateTime;

import addcel.version.Version;

/**
 * Especifica m&eacute;todos para recibir eventos de Servicios en la
 * aplicaci&oacute;n
 * 
 * @author carlosgs
 *
 */
public interface ViewCallback {

	/**
	 * Recibe evento proveniente de {@code VersionService}
	 * 
	 * @param version
	 */
	public void onVersionReceived(Version version);

	/**
	 * Recibe evento proveniente de {@code ActualizacionPeriodicaService}
	 * 
	 * @param date
	 */
	public void onCatalogUpdated(DateTime date);

	/**
	 * Recibe error proveniente de {@code ActualizacionPeriodicaService}
	 * 
	 * @param error
	 */
	public void onErrorFromBus(String error);

}
