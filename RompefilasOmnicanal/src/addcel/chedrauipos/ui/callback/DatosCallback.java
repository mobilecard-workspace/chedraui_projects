package addcel.chedrauipos.ui.callback;

import java.util.List;

import addcel.chedrauipos.vo.DefaultResponse;
import addcel.chedrauipos.vo.LoginResponse;
import addcel.chedrauipos.vo.Role;
import addcel.chedrauipos.vo.Tienda;

public interface DatosCallback {
	
	public void onTiendasReceived(List<Tienda> tiendas);
	public void onRolesReceived(List<Role> roles);
	public void onDataUpdated(DefaultResponse response, LoginResponse data);
	

}
