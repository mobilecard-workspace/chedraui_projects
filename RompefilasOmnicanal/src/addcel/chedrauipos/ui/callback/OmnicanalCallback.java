package addcel.chedrauipos.ui.callback;

import addcel.chedrauipos.model.Articulo;
import addcel.chedrauipos.omnicanal.vo.AbstractVO;
import addcel.chedrauipos.omnicanal.vo.PedidoResponse;
import addcel.chedrauipos.omnicanal.vo.RespuestaConsultarPedidoItem;

/**
 * Especifica m&eacute;todos para recibir eventos de Servicios y Tasks
 * ejecutadas desde {@code OmnicanalPresenter} en la clase
 * {@code OmnicanalActivity}
 * 
 * @author carlosgs
 *
 */
public interface OmnicanalCallback extends ViewCallback {

	public void onPedidoConsultado(PedidoResponse pedido);

	public void onUpcsConsultados(long[] upc);

	public void onArticuloConsultado(Articulo articulo);

	public void onArchivoDepositado(int result);

	public void onStatusModificado(AbstractVO response);

	public void onTicketImpreso(int result);

	public void onStatusCancelado(AbstractVO response);

	public void onItemAddedToDeleteList(RespuestaConsultarPedidoItem item);
	
	public void onDeleteListModified(int newSize);

	public void onCancelacionParcial(AbstractVO response);

}
