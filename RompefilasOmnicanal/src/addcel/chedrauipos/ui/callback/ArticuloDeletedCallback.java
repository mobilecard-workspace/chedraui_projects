package addcel.chedrauipos.ui.callback;

public interface ArticuloDeletedCallback {

	public void onArticuloDeleted(int position);
}
