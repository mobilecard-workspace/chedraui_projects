package addcel.chedrauipos.ui.callback;

import java.util.List;

import android.bluetooth.BluetoothDevice;

public interface MenuCallback {
	
	public void onBtDevicesObtained(List<BluetoothDevice> devices);

}
