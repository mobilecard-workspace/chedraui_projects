package addcel.chedrauipos.ui.callback;

import addcel.chedrauipos.vo.DefaultResponse;
import addcel.chedrauipos.vo.RolesResponse;
import addcel.chedrauipos.vo.TiendasResponse;

public interface RegistroCallback {
	
	public void onTiendasReceived(TiendasResponse response);
	public void onRolesReceived(RolesResponse response);
	public void onRegistroFinished(DefaultResponse response);

}
