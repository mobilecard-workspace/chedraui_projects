package addcel.receiver;

import com.activeandroid.util.Log;

import addcel.chedrauipos.service.receiver.ArticuloCleanerReceiver;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootReceiver extends BroadcastReceiver {
	
	private ArticuloCleanerReceiver alarm = new ArticuloCleanerReceiver();
	
	private static final String TAG = BootReceiver.class.getSimpleName();

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
        	Log.d(TAG,"inicializando ArticulosCleanerReceiver");
        	alarm.setAlarm(context);
        }
	}

}
