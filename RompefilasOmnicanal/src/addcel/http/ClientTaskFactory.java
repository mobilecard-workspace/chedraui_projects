package addcel.http;

import org.addcel.android.chedrauipos.R;

import addcel.util.ProgressDialogFactory;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

public final class ClientTaskFactory {
	
	private static ProgressDialog pDialog;
	private static final String TAG = ClientTaskFactory.class.getName();
	private static AsyncTask<Void, Void, String> instance;
	
	private ClientTaskFactory() {
		
	}
	
	public static synchronized AsyncTask<Void, Void, String> get(final Client client, 
			final HttpListener listener, final Context context) {
		if (context != null)
			pDialog = ProgressDialogFactory
					.get(context, R.string.msg_pdialog);
		
		instance = new AsyncTask<Void, Void, String>() {
			
			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				if (context != null) {
					if (!pDialog.isShowing())
						pDialog.show();
				}
			}
			
			@Override
			protected String doInBackground(Void... params) {
				// TODO Auto-generated method stub
				return client.getData();
				
			}
			
			@Override
			protected void onPostExecute(String result) {
				// TODO Auto-generated method stub
				
				if (context != null) {
					if (pDialog != null && pDialog.isShowing()) {
						pDialog.dismiss();
					}
				}
				
				if (TextUtils.isEmpty(result))
					listener.onError();
				else
					listener.onSuccess(result);
			}
		};
		
		return instance;
	}
	
	

}
