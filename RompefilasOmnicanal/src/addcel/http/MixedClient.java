package addcel.http;

import java.io.IOException;

import org.addcel.android.crypto.AddcelCrypto;

import android.util.Log;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

public class MixedClient implements Client {
	
	private OkHttpClient client;
	private String url;
	private String[] params;
	
	public MixedClient(OkHttpClient client, String url, String... params) {
		// TODO Auto-generated constructor stub
		this.client = client;
		this.url = url;
		this.params = params;
	}

	@Override
	public String getData() {
		// TODO Auto-generated method stub
		Log.d(MobilecardClient.class.getName(), url);
		Request request;
		RequestBody body = null;
		
		if (params != null && params.length > 0) {
			body = new FormEncodingBuilder()
		        .add("json", AddcelCrypto.encryptSensitive(
		        		AddcelCrypto.getKey(), params[0]))
		        .build(); 
		} else {
			body = RequestBody.create(MediaType.parse("text/plain"), "");
		}
		
		request = new Request.Builder()
		        .url(url)
		        .post(body)
		        .build();
		try {
				
			Response response = client.newCall(request).execute();
			Log.d(MixedClient.class.getName(), "" + response.code());
			if (response.code() == 200) {
				String data = AddcelCrypto.decryptHard(response.body().string());
				return data;
			} else {
				return null;
			}
			
		} catch (IOException e) {
			return null;
		}
	}

}
