package addcel.http;

import java.io.IOException;

import org.addcel.android.crypto.AddcelCrypto;

import android.util.Log;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

public class HardClient implements Client {
	
	private String url;
	private String[] params;
	private OkHttpClient client;
	
	public HardClient(OkHttpClient client, String url, String... params) {
		// TODO Auto-generated constructor stub
		this.client = client;
		this.url = url;
		this.params = params;
	}

	@Override
	public String getData() {
		// TODO Auto-generated method stub
		Log.d(Client.TAG_HARD, url);
		Request request;
		RequestBody body = null;
		
		if (params != null && params.length > 0) {
			Log.d(Client.TAG_HARD, params[0]);
			body = new FormEncodingBuilder()
		        .add("json", AddcelCrypto.encryptHard(params[0]))
		        .build(); 
		} else {
			body = RequestBody.create(MediaType.parse("text/plain"), "");
		}
		
		request = new Request.Builder()
		        .url(url)
		        .post(body)
		        .build();
		try {
				
			Response response = client.newCall(request).execute();
			Log.d(HardClient.class.getSimpleName(), "" + response.code());
			if (response.code() == 200) {
				String data = AddcelCrypto.decryptHard(response.body().string());
				return data;
			}
			
			return null;
				
			
		} catch (IOException e) {
			Log.e(Client.TAG_HARD, "IOException", e);
			return null;
		}
	}

}
