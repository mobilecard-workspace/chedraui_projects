package addcel.http;

import java.util.concurrent.TimeUnit;

import org.addcel.android.crypto.Cifrados;

import com.squareup.okhttp.OkHttpClient;

public final class ClientFactory {
	
	private static OkHttpClient okHttpClient;
	private static Client instance = null;
	
	private ClientFactory() {
		
	}
	
	private static synchronized OkHttpClient getOkHttpClient() {
		if (okHttpClient == null) {
			okHttpClient = new OkHttpClient();
			okHttpClient.setConnectTimeout(15, TimeUnit.SECONDS);
		}
		
		return okHttpClient;
	}
	
	public static synchronized Client get(Cifrados c, String url, String key, String... params) {
		
		switch (c) {
		case LIMPIO:
			instance = new CleanClient(getOkHttpClient(), url, params);
			break;
		case HARD:
			instance = new HardClient(getOkHttpClient(), url, params);
			break;
		case SENSITIVE:
			instance = new SensitiveClient(getOkHttpClient(), url, params);
			break;
		case MIXED:
			instance = new MixedClient(getOkHttpClient(), url, params);
			break;
		case MOBILECARD:
			instance = new MobilecardClient(getOkHttpClient(), url, key, params);
			break;

		default:
			break;
		}
		
		return instance;
	}
 
}
