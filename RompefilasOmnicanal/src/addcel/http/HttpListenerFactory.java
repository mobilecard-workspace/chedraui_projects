package addcel.http;

public final class HttpListenerFactory {
	
	private static HttpListener instance;
	
	public static synchronized HttpListener get(HttpListener listener) {
		set(listener);
		return instance;
	}
	
	private static synchronized void set(HttpListener listener) {
		instance = listener;
	}

}
