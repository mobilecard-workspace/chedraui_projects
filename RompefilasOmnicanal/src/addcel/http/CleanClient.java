package addcel.http;

import java.io.IOException;

import android.util.Log;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

public class CleanClient implements Client {
	
	private String url;
	private String[] params;
	private OkHttpClient client;
	
	public CleanClient(OkHttpClient client, String url, String... params) {
		// TODO Auto-generated constructor stub
		this.url = url;
		this.params = params;
		this.client = client;
	}

	@Override
	public String getData() {
		// TODO Auto-generated method stub
		Log.d(Client.TAG_CLEAN, url);
		Request request;
		RequestBody body = null;
		
		if (params != null && params.length > 0) {
			body = new FormEncodingBuilder()
		        .add("json", params[0])
		        .build(); 
		} else {
			body = RequestBody.create(MediaType.parse("text/plain"), "");
		}
		
		request = new Request.Builder()
		        .url(url)
		        .post(body)
		        .build();
		try {
			Response response = client.newCall(request).execute();
			Log.d(Client.TAG_CLEAN, "" + response.code());
			if (response.code() == 200) {
				String data = response.body().string();
				return data;
			} else {
				return null;
			}
		} catch (IOException e) {
			return null;
		}
	}

}
