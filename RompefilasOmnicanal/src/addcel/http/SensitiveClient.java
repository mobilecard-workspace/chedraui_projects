package addcel.http;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.addcel.android.crypto.AddcelCrypto;

import android.util.Log;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

public class SensitiveClient implements Client {
	
	private OkHttpClient client;
	private String url;
	private String[] params;
	
	public SensitiveClient(OkHttpClient client, String url, String... params) {
		// TODO Auto-generated constructor stub
		this.client = client;
		this.url = url;
		this.params = params;
	}

	@Override
	public String getData() {
		// TODO Auto-generated method stub
		Log.d(Client.TAG_SENSITIVE, url);
		Request request;
		RequestBody body = null;
		
		if (params != null && params.length > 0) {
			Log.d(Client.TAG_SENSITIVE, params[0]);
			body = new FormEncodingBuilder()
		        .add("json", AddcelCrypto.encryptSensitive(
		        		AddcelCrypto.getKey(), params[0]))
		        .build(); 
		} else {
			body = RequestBody.create(MediaType.parse("text/plain"), "");
		}
		
		request = new Request.Builder()
		        .url(url)
		        .post(body)
		        .build();
		try {
				
			Response response = client.newCall(request).execute();
			Log.d(Client.TAG_SENSITIVE, "" + response.code());
			if (response.code() == 200) {
				String data = AddcelCrypto.decryptSensitive(response.body().string());
				return data;
			}
			
			return null;
			
		} catch (IOException e) {
			Log.e(Client.TAG_SENSITIVE, "IOException", e);
			return null;
		}
	}

}
