package addcel.http;

import java.io.IOException;

import org.addcel.android.crypto.AddcelCrypto;
import org.addcel.android.crypto.Crypto;

import addcel.util.AddcelTextUtil;
import android.util.Log;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

public class MobilecardClient implements Client {
	
	private String url;
	private String[] params;
	private OkHttpClient client;
	private String key;
	
	public MobilecardClient(OkHttpClient client, String url, String key, String... params) {
		// TODO Auto-generated constructor stub
		this.client = client;
		this.url = url;
		this.key = key;
		this.params = params;
	}

	@Override
	public String getData() {
		// TODO Auto-generated method stub
		Log.d(Client.TAG_MOBILECARD, url);
		Request request;
		RequestBody body = null;
		
		if (params != null && params.length > 0) {
			
			Log.d(Client.TAG_MOBILECARD, params[0]);
			
			String encrypt = Crypto.aesEncrypt(AddcelTextUtil.parsePass(key), params[0]);
			encrypt = AddcelCrypto.mergeStr(encrypt, key);
			
			body = new FormEncodingBuilder()
		        .add("json", encrypt)
		        .build(); 
		} else {
			body = RequestBody.create(MediaType.parse("text/plain"), "");
		}
		
		request = new Request.Builder()
		        .url(url)
		        .post(body)
		        .build();
		try {
				
			Response response = client.newCall(request).execute();
			//Crypto.aesDecrypt(Text.parsePass(key), response);
			if (response.code() == 200) {
				String data = Crypto.aesDecrypt(AddcelTextUtil.parsePass(key), 
						response.body().string());
				Log.d(Client.TAG_MOBILECARD, data);
				return data;
			} else {
				return null;
			}
			
		} catch (IOException e) {
			Log.e(Client.TAG_MOBILECARD, "IOException", e);
			return null;
		}
	}

}
