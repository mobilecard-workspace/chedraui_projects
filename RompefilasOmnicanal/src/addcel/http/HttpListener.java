package addcel.http;

/**
 * Interfaz que define procesamiento de respuestas Http
 * @author carlosgs
 *
 */
public interface HttpListener {

	public void onSuccess(String response);

	public void onError();

}
