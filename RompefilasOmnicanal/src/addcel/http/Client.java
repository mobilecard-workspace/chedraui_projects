package addcel.http;


public interface Client {
	
	public static final String TAG_CLEAN = CleanClient.class.getName();
	public static final String TAG_HARD = HardClient.class.getName();
	public static final String TAG_SENSITIVE = SensitiveClient.class.getName();
	public static final String TAG_MIXED = MixedClient.class.getName();
	public static final String TAG_MOBILECARD = MobilecardClient.class.getName();
	
	public String getData();

}
