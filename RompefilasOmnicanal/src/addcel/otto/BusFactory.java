package addcel.otto;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

public final class BusFactory {

	private static Bus instance;
	
	private BusFactory() {
		
	}
	
	public static synchronized Bus get() {
		if (instance == null)
			instance = new Bus(ThreadEnforcer.ANY);
		
		return instance;
	}
}
