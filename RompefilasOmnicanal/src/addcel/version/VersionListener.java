package addcel.version;

public interface VersionListener {
	
	public void onCurrentVersion();
	public void onForcedChange(Version version);
	public void onOptionalChange(Version version);
	public void onError();

}
