package addcel.version.service;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import addcel.receiver.BootReceiver;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

public class VersionReceiver extends WakefulBroadcastReceiver {
	
	private AlarmManager aManager;
	private PendingIntent pIntent;
	
	private static final String TAG = VersionReceiver.class.getName();

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		Log.d(TAG, "Recibe señal para lanzar servicio de versión");
		Intent service = new Intent(context, VersionService.class);
		startWakefulService(context, service);
	}
	
	public void setAlarm(Context context) {
		
		Log.d(TAG, "Iniciando Alarma");
		
		aManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(context, VersionReceiver.class);
		pIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
		
		
		aManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
				Calendar.getInstance().getTimeInMillis(), 
//				AlarmManager.INTERVAL_HOUR,
				TimeUnit.HOURS.toMillis(2),
				pIntent);
		
		ComponentName receiver = new ComponentName(context, BootReceiver.class);
		PackageManager pm = context.getPackageManager();
		
		pm.setComponentEnabledSetting(receiver, PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
				PackageManager.DONT_KILL_APP);
	}
	
	public void cancelAlarm(Context context) {
        // If the alarm has been set, cancel it.
        if (aManager!= null) {
            aManager.cancel(pIntent);
        }
        
        // Disable {@code SampleBootReceiver} so that it doesn't automatically restart the 
        // alarm when the device is rebooted.
        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
	}

}
