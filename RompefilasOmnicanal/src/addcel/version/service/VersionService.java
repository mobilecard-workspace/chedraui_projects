package addcel.version.service;

import java.net.SocketTimeoutException;

import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.ServicesStatus;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.RetrofitError;
import retrofit.client.Response;
import addcel.otto.BusFactory;
import addcel.util.NetworkUtil;
import addcel.version.Version;
import addcel.version.VersionRest;
import android.app.IntentService;
import android.content.Intent;
import android.os.Build.VERSION;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.text.TextUtils;
import android.util.Log;

public class VersionService extends IntentService {

	private static VersionRest rest;

	private static final String ENDPOINT = "http://www.mobilecard.mx:8080";
	private static final String TAG = VersionService.class.getName();
	
	public VersionService() {
		super("VersionService");
		// TODO Auto-generated constructor stub
		Log.d(TAG, "Inicializando...");
	}


	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		Log.d(TAG, "Entrando a onHandleIntent");
		
		ServicesStatus.setStatus(ServicesStatus.VERSION, true);
		
		if (NetworkUtil.isOnline(this)) {
			try {
				getVersionRest().getVersionAsync("4", getString(R.string.version_id_app),
						new Callback<Version>() {

							@Override
							public void failure(RetrofitError arg0) {
								// TODO Auto-generated method stub
								if (arg0.getBody() != null)
									Log.e(TAG,arg0.getBody().toString());
								else
									Log.e(TAG, "RetrofitError");
								
								BusFactory.get().post(R.string.version_error);
							}

							@Override
							public void success(Version arg0, Response arg1) {
								// TODO Auto-generated method stub
								Log.d(TAG, arg1.getBody().toString());
								BusFactory.get().post(arg0);
							}
						});
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Log.d(TAG, Exception.class.getSimpleName(), e);
				BusFactory.get().post(R.string.version_error);
			} finally {
				ServicesStatus.setStatus(ServicesStatus.VERSION, false);
				WakefulBroadcastReceiver.completeWakefulIntent(intent);
			}
		}
	}
	
	public static synchronized VersionRest getVersionRest() {
		if (rest == null) {
			RestAdapter.Log log = new RestAdapter.Log() {

				@Override
				public void log(String arg0) {
					// TODO Auto-generated method stub
					Log.d(VersionRest.class.getName(), arg0);
				}
			};

			RestAdapter restAdapter = new RestAdapter.Builder().setLog(log)
					.setLogLevel(LogLevel.FULL).setEndpoint(ENDPOINT).build();

			rest = restAdapter.create(VersionRest.class);
		}

		return rest;
	}

}
