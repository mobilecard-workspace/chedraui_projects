package addcel.version;

import java.net.SocketTimeoutException;

import org.addcel.android.chedrauipos.R;

import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import addcel.util.ProgressDialogFactory;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class VersionTask extends AsyncTask<Void, Void, Version> {

	private Context context;
	private String idPlataforma;
	private String idApp;
	private static VersionRest service;
	private VersionListener listener;
	private ProgressDialog pDialog;

	private static final String ENDPOINT = "http://www.mobilecard.mx:8080";

	public VersionTask(Context con, String idPlataforma, String idApp,
			VersionListener listener) {
		// TODO Auto-generated constructor stub
		this.context = con;
		this.idPlataforma = idPlataforma;
		this.idApp = idApp;
		this.listener = listener;
		this.pDialog = ProgressDialogFactory
				.get(context, R.string.msg_version);
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		if (!pDialog.isShowing())
			pDialog.show();
	}

	@Override
	protected Version doInBackground(Void... params) {
		// TODO Auto-generated method stub
		try {
			return getVersionService().getVersion(idPlataforma, idApp);
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	protected void onPostExecute(Version result) {
		// TODO Auto-generated method stub

		if (pDialog.isShowing())
			pDialog.dismiss();

		if (result == null)
			listener.onError();
		else {

			VersionStatus status = VersionProcessingServiceFactory.get()
					.processVersion(
							VersionUtil.getDeviceVersionNumber(context,
									VersionTask.class), result);

			switch (status) {
			case SIN_CAMBIO:
				listener.onCurrentVersion();
				break;
			case OBLIGATORIO:
				listener.onForcedChange(result);
				break;
			case OPCIONAL:
				listener.onOptionalChange(result);
				break;

			default:
				listener.onError();
				break;
			}
		}
	}

	public static synchronized VersionRest getVersionService() {
		if (service == null) {
			RestAdapter.Log log = new RestAdapter.Log() {

				@Override
				public void log(String arg0) {
					// TODO Auto-generated method stub
					Log.d(VersionRest.class.getName(), arg0);
				}
			};

			RestAdapter restAdapter = new RestAdapter.Builder().setLog(log)
					.setLogLevel(LogLevel.FULL).setEndpoint(ENDPOINT).build();

			service = restAdapter.create(VersionRest.class);
		}

		return service;
	}

}
