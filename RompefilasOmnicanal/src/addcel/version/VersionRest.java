package addcel.version;

import java.net.SocketTimeoutException;

import retrofit.http.GET;
import retrofit.http.Query;

public interface VersionRest {
	
	@GET("/Vitamedica/getVersion")
	Version getVersion(
			@Query("idPlataforma") String idPlataforma,
			@Query("idApp") String idApp) throws Exception;
	
	@GET("/Vitamedica/getVersion")
	void getVersionAsync(@Query("idPlataforma") String idPlataforma, @Query("idApp") String idApp, 
			retrofit.Callback<Version> callback) throws Exception;

}
