package addcel.version;

import android.text.TextUtils;

public class VersionProcessingServiceImpl implements VersionProcessingService {
	

	@Override
	public VersionStatus processVersion(String deviceVersion, Version version) {
		// TODO Auto-generated method stub
		switch (version.getTipo()) {
		case 1:
			if (!evaluate(version.getVersion(), deviceVersion))
				return VersionStatus.OBLIGATORIO;
			else
				return VersionStatus.SIN_CAMBIO;
		case 2:
			if (!evaluate(version.getVersion(), deviceVersion))
				return VersionStatus.OPCIONAL;
			else
				return VersionStatus.SIN_CAMBIO;
		default:
			return VersionStatus.SIN_CAMBIO;
		}
	}
	
	private boolean evaluate(String serverVersion, String deviceVersion) {
		
		String[] serverArr = TextUtils.split(serverVersion, "[.]");
		String[] deviceArr = TextUtils.split(deviceVersion, "[.]");
		
		boolean pasa = true;
		
		for(int i = 0; i < serverArr.length; i++) {
			if (i < deviceArr.length) {
				int serverNum = Integer.parseInt(serverArr[i]);
				int deviceNum = Integer.parseInt(deviceArr[i]);
				
				if (serverNum > deviceNum) {
					pasa = false;
					break;
				} else if (serverNum < deviceNum) {
					return true;
				}
			} else {
				break;
			}
		}
		
		return pasa;
	}

}
