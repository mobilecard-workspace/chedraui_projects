package addcel.version;

public class Version {

	private int id;
	private String version;
	private String url;
	private int tipo;

	public int getId() {
		return id;
	}

	public String getVersion() {
		return version;
	}

	public String getUrl() {
		return url;
	}

	public int getTipo() {
		return tipo;
	}
}
