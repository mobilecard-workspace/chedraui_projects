package addcel.version;

public class VersionListenerFactory {
	
	private static VersionListener instance;
	
	public static synchronized VersionListener get(VersionListener listener) {
		set(listener);
		return instance;
	}
	
	private static synchronized void set(VersionListener listener) {
		instance = listener;
	}

}
