package addcel.version.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

public final class VersionDialogFactory {

	private static AlertDialog.Builder builder;

	public static synchronized AlertDialog.Builder get(final Context con,
			final int mensaje, final Class<?> next, final int tipo,
			final String url) {

		builder = new AlertDialog.Builder(con);
		builder.setMessage(mensaje);
		builder.setCancelable(false);

		switch (tipo) {
		case 1:
			builder.setPositiveButton(android.R.string.ok,
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(Intent.ACTION_VIEW);
							intent.setData(Uri.parse(url));
							con.startActivity(intent);
						}
					});
			break;

		case 2:
			builder.setNegativeButton(android.R.string.no,
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss();
						}
					});
			builder.setPositiveButton(android.R.string.ok,
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(Intent.ACTION_VIEW);
							intent.setData(Uri.parse(url));
							con.startActivity(intent);
						}
					});
			break;
		default:
			break;
		}

		return builder;
	}

	public static synchronized AlertDialog.Builder get(final Context con,
			final int mensaje, final String url) {
		return get(con, mensaje, null, 1, url);
	}

	public static synchronized AlertDialog.Builder get(final Context con,
			final int mensaje, final Class<?> next, final String url) {
		return get(con, mensaje, next, 2, url);
	}

}
