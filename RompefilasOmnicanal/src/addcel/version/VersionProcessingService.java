package addcel.version;

public interface VersionProcessingService {
	
	public VersionStatus processVersion(String deviceVersion, Version version); 

}
