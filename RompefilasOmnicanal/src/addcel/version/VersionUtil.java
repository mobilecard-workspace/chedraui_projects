package addcel.version;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.text.TextUtils;

public final class VersionUtil {
	
	private VersionUtil() {
		
	}
	
	private static String deviceVersion;
	
	public static synchronized String getDeviceVersionNumber(Context c, Class<?> cls) {
		if (TextUtils.isEmpty(deviceVersion)) {
			try {
		        ComponentName comp = new ComponentName(c, cls);
		        PackageInfo pinfo = c.getPackageManager().getPackageInfo(comp.getPackageName(), 0);
		        return pinfo.versionName;
		    } catch (android.content.pm.PackageManager.NameNotFoundException e) {
		        return null;
		    }
		}
	
		return deviceVersion;
	}

}
