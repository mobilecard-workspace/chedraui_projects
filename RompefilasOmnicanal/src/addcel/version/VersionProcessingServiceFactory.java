package addcel.version;

public class VersionProcessingServiceFactory {
	
	private static VersionProcessingService instance;
	
	public static synchronized VersionProcessingService get() {
		if (instance == null)
			instance = new VersionProcessingServiceImpl();
		
		return instance;
	}
	
	public static synchronized void set(VersionProcessingService service) {
		instance = service;
	}

}
