package org.addcel.android.chedrauipos;

import java.util.ArrayList;
import java.util.List;

import addcel.chedrauipos.vo.LoginResponse;
import addcel.chedrauipos.vo.Role;
import addcel.chedrauipos.vo.Tienda;
import android.bluetooth.BluetoothDevice;

public final class UserSession {
	
	private LoginResponse login;
	private List<Tienda> tiendas;
	private List<Role> roles;
	private BluetoothDevice impresora;
	
	private static UserSession instance;
	
	private UserSession() {
		tiendas = new ArrayList<Tienda>();
		roles = new ArrayList<Role>();
	}
	
	public static synchronized UserSession get() {
		if (instance == null)
			instance = new UserSession();
		
		return instance;
	}
	
	public LoginResponse getLogin() {
		return login;
	}
	
	public void setLogin(LoginResponse login) {
		this.login = login;
	}
	
	public List<Tienda> getTiendas() {
		return tiendas;
	}
	
	public void setTiendas(List<Tienda> tiendas) {
		this.tiendas = tiendas;
	}
	
	public List<Role> getRoles() {
		return roles;
	}
	
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	public BluetoothDevice getImpresora() {
		return impresora;
	}
	
	public void setImpresora(BluetoothDevice impresora) {
		this.impresora = impresora;
	}
	
	public boolean isLoggedIn() {
		if (login == null)
			return false;
		
		if (login.getStatus() != 1)
			return false;
		
		if (login.getIdUsuario() <= 0)
			return false;
		
		return true;
	}
	
	public boolean mustChangePassword() {
		if (login.getStatus() == 99)
			return true;
		
		return false;
	}
	
	public void logout() {
		setLogin(null);
	}
}
