package org.addcel.android.chedrauipos;

import java.util.concurrent.TimeUnit;

import addcel.chedrauipos.ui.MenuActivity;
import addcel.version.service.VersionReceiver;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class LaunchActivity extends Activity {
	
	private final Context context = this;
	private static VersionReceiver alarm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_launch);
		
		int consecutivo = AppStatus.get(context).getConsecutivo();
        if (consecutivo == 0) {
        	AppStatus.get(context).setConsecutivo(9001);
        }
		
		alarm = new VersionReceiver();
		alarm.setAlarm(context);
		
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				startActivity(new Intent(context, MenuActivity.class));
			}
		}, TimeUnit.SECONDS.toMillis(3L));
	}
}
