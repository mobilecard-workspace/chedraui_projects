package org.addcel.android.chedrauipos;

public final class ServicesStatus {
	
	private static boolean[] SERVICIOS;
	
	public static final int VERSION = 0;
	public static final int ACTUALIZACION = 1;
	public static final int CLEANER = 2;
	
	public static synchronized boolean[] get() {
		if (SERVICIOS == null)
			SERVICIOS = new boolean[3];
		
		return SERVICIOS;
	}
	
	public static synchronized void setStatus(int servicio, boolean value) {
		if (servicio < 3) {
			get()[servicio] = value;
		}
	}
	
	public static synchronized boolean areServicesRunning() {
		for (boolean b : get()) {
			if (b)
				return true;
		}
		
		return false;
	}

}
