package org.addcel.android.chedrauipos;

import org.joda.time.DateTime;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class AppStatus {
	
	private SharedPreferences pref;
	private Editor editor;
	
	private static final  int PRIVATE_MODE = 0;
	
	private static final String PREF_NAME = "chedrauipos";
	private static final String HAS_ATTRS = "hasAttributes";
	public static final String CODIGO = "codigo";
	public static final String CONSECUTIVO = "consecutivo";
	public static final String FECHA_ACT = "fechaActualizacion";
	public static final String UPDATED = "updated";
	
	private static AppStatus instance;
	
	private AppStatus(Context context) {
		pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}
	
	public static synchronized AppStatus get(Context context) {
		instance = new AppStatus(context);
		return instance;
	}
	
	public int getConsecutivo() {
		return pref.getInt(CONSECUTIVO, 0);
	}

	public void setConsecutivo(int consecutivo) {
		editor.putInt(CONSECUTIVO, consecutivo);
		editor.commit();
	}

	public String getCodigo() {
		return pref.getString(CODIGO, "");
	}
	
	public String getCodigoForOmnicanal() {
		String codigo = pref.getString(CODIGO, "");
		
		switch (codigo.length()) {
		case 1:
			return "000" + codigo;
		case 2:
			return "00" + codigo;
		case 3:
			return "0" + codigo;
		
		default:
			return codigo;
		}
	}

	public void setCodigo(String codigo) {
		editor.putString(CODIGO, codigo);
		editor.commit();
	}

	public DateTime getFechaActualizacion() {
		return new DateTime(pref.getLong(FECHA_ACT, 0));
	}

	public void setFechaActualizacion(DateTime fechaActualizacion) {
		if (fechaActualizacion == null)
			editor.putLong(FECHA_ACT, 0L);
		else
			editor.putLong(FECHA_ACT, fechaActualizacion.getMillis());
		
		editor.commit();
	}

	public boolean getUpdated() {
		return pref.getBoolean(UPDATED, false);
	}

	public void setUpdated(boolean updated) {
		editor.putBoolean(UPDATED, updated);
		editor.commit();
	}

	public void logoutUser() {
		editor.clear();
		editor.commit();
	}

	public boolean arePreferencesSet() {
		return pref.getBoolean(HAS_ATTRS, false);
	}

}
