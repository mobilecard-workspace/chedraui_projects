package org.addcel.widget.factory;

import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class ViewFactory {
	
	public static final int WRAP = LayoutParams.WRAP_CONTENT;
	public static final int MATCH = LayoutParams.MATCH_PARENT;
	
	public static final int NAME = 0;
	public static final int VALUE = 1;
	
	public static final int LEFT = Gravity.LEFT;
	public static final int RIGHT = Gravity.RIGHT;
	

	private static String TAG = "ViewFactory";
	

/**
 * 	
 * @param _con
 * @param _text
 * @param _widthType
 * @param _gravity
 * @param _margin
 * @param _color
 * @param _fontSize
 * @return
 */
	public static TextView TextViewBuilder(Context _con, String _text, int _widthType, int _gravity, int _margin, int _color, int _fontSize) {

		TextView view = new TextView(_con);
		LayoutParams params = null;
		params = new LayoutParams(_widthType, LayoutParams.WRAP_CONTENT);
		params.setMargins(0, 0, 0, _margin);
		view.setLayoutParams(params);
		view.setGravity(_gravity);
		
		if (_color == 0) {
			view.setTextColor(0xCC333333);
		} else {
			view.setTextColor(_color);
		}
		view.setTextSize(_fontSize);
		view.setText(_text);
		view.setSingleLine(false);
		view.setTypeface(Typeface.SANS_SERIF);
		
		return view;
	}
	
/**
 * 	
 * @param _con
 * @param _text
 * @return
 */
	public static TextView TextViewBuilder(Context _con, String _text) {
		
		return TextViewBuilder(_con, _text, WRAP, LEFT, 0, 0, 14);
		
	}
	
/**
 * 
 * @param _con
 * @param _text
 * @param _widthType
 * @return
 */
	public static TextView TextViewBuilder(Context _con, String _text, int _widthType) {
		return TextViewBuilder(_con, _text, _widthType, LEFT, 0, 0, 14);
	}
	
/**
 * 
 * @param _con
 * @param _text
 * @param _widthType
 * @param _gravity
 * @return
 */
	public static TextView TextViewBuilder(Context _con, String _text, int _widthType, int _gravity) {
		return TextViewBuilder(_con, _text, _widthType, _gravity, 0, 0, 14);
	}
	
/**
 * 
 * @param _con
 * @param _text
 * @param _widthType
 * @param _gravity
 * @param _margin
 * @return
 */
	public static TextView TextViewBuilder(Context _con, String _text, int _widthType, int _gravity, int _margin) {
		return TextViewBuilder(_con, _text, _widthType, _gravity, _margin, 0, 14);
	}
	
/**
 * 
 * @param _con
 * @param _text
 * @param _widthType
 * @param _gravity
 * @param _margin
 * @param _color
 * @return
 */
	public static TextView TextViewBuilder(Context _con, String _text, int _widthType, int _gravity, int _margin, int _color) {
		return TextViewBuilder(_con, _text, _widthType, _gravity, _margin, _color, 14);
	}
	
	

}
