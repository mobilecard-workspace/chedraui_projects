package org.addcel.widget.factory;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class LayoutFactory {
	
	private static final String TAG = "LayoutFactory";
	
	public static final LinearLayout buildHorizontalLayout(Context _con, View... _views) {
		
		LinearLayout layout = new LinearLayout(_con);
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		params.setMargins(0, 0, 0, 8);
		
		layout.setLayoutParams(params);
		layout.setBackgroundColor(Color.TRANSPARENT);
		
		for (View view : _views) {
			layout.addView(view);
		}
		
		return layout;
		
	}

}
