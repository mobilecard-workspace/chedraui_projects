package org.addcel.widget.factory;

import org.addcel.android.util.LogUtil;
import org.addcel.android.widget.EditableElement;
import org.addcel.android.widget.FormElement;
import org.addcel.android.widget.VisibleElement;
import org.addcel.android.widget.enums.ElementType;

import android.content.Context;

public class FormElementFactory {
	
	private static final String TAG = "FormElementFactory";
	
	public static FormElement buildElement(Context con, ElementType t, String name, String value) {
		
		FormElement element = null;
		
		switch (t) {
		case VISIBLE:
			
			LogUtil.text(TAG, name);
			
			element = new VisibleElement(con, name);
			element.setTag(name);
			
			break;
			
		case EDITABLE:
			
			LogUtil.text(TAG, name);
			
			element = new EditableElement(con, name, value);
			element.setTag(value);
				
			break;

		default:
			break;
		}
		
		return element;
		
	}

}
