package org.addcel.android.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.addcel.android.util.LogUtil;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class SFTPTransferClient implements FileTransferClient {
	
	private JSch channel;
	private ChannelSftp sftpCh;
	private Session session;
	private String host;
	private String login;
	private String password;
	private int port;
	
	private static final String TAG = "SFTPTransferClient";
	
	public SFTPTransferClient() {
		// TODO Auto-generated constructor stub
		channel = new JSch();
	}
	
	public SFTPTransferClient(String host, String login, String pass) {
		this();
		this.host = host;
		this.login = login;
		this.password = pass;
		this.port = 22;
	}
	
	public boolean connect() {
		try {
			session = channel.getSession(login, host, port);
			session.setPassword(password);
			session.setConfig("StrictHostKeyChecking", "no");
			session.connect();
			if (session.isConnected()) {
				LogUtil.text(TAG, "Conexión Exitosa");
				sftpCh = (ChannelSftp) session.openChannel("sftp");
				sftpCh.connect();
				return sftpCh.isConnected();
			} else {
				return false;
			}
			
		} catch (JSchException e) {
			LogUtil.error(TAG, "connect", e);
			return false;
		}
	}

	@Override
	public int getFile(String myPath, String serverPath) {
		// TODO Auto-generated method stub
		try {
			File file = new File(myPath);
            FileOutputStream srcFileStream = new FileOutputStream(file);
            sftpCh.cd(serverPath);
            sftpCh.get(file.getName(), srcFileStream);
            srcFileStream.close();
            
            return 0;
		} catch (Exception e) {
            LogUtil.error(TAG, "getFile", e);
			return -10000;
		} finally {
			disconnect();
		}
	}

	@Override
	public int sendFile(String myPath, String serverPath) {
		// TODO Auto-generated method stub
		try {  
        	File file = new File(myPath);
            FileInputStream srcFileStream = new FileInputStream(file);  
            sftpCh.cd(serverPath);
            sftpCh.put(srcFileStream, file.getName());
            srcFileStream.close();
            return 0;
             
        } catch (Exception e) {  
             LogUtil.error(TAG, "sendFile", e);
             return -10000;
        } finally {
        	disconnect();
        }
	}

	@Override
	public boolean disconnect() {
		// TODO Auto-generated method stub
		sftpCh.disconnect();
		session.disconnect();
		return true;
	}

}
