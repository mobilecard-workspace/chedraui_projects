package org.addcel.android.ftp;

import org.addcel.android.ftp.callback.FTPResponseListener;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class FileSendTask extends AsyncTask<Void, Void, Integer> {
	
	private Context con;
	private FileTransferClient client;
	private String myPath, serverPath;
	private ProgressDialog pDialog;
	private FTPResponseListener listener;
	
	public FileSendTask(Context con, FileTransferClient client, String myPath, String serverPath, FTPResponseListener listener) {
		this.con = con;
		this.client = client;
		this.myPath = myPath;
		this.serverPath = serverPath;
		this.listener = listener;
	}
	
	public FileTransferClient getClient() {
		return client;
	}
	
	public String getMyPath() {
		return myPath;
	}
	
	public String getServerPath() {
		return serverPath;
	}
	
	public FTPResponseListener getListener() {
		return listener;
	}
	
	public void setClient(FileTransferClient client) {
		this.client = client;
	}
	
	public void setMyPath(String myPath) {
		this.myPath = myPath;
	}
	
	public void setServerPath(String serverPath) {
		this.serverPath = serverPath;
	}
	
	public void setListener(FTPResponseListener listener) {
		this.listener = listener;
	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		if (pDialog == null || (pDialog != null && !pDialog.isShowing())) {
			pDialog = ProgressDialog.show(con, "Enviando a controlador..." , "Espere, por favor.", true, false);
		}
	}

	@Override
	protected Integer doInBackground(Void... params) {
		// TODO Auto-generated method stub
		
		boolean stat = false;
			
		if (client instanceof FTPTransferClient)
			stat = ((FTPTransferClient) client).connect();
		else if (client instanceof SFTPTransferClient)
			stat = ((SFTPTransferClient) client).connect();
		
		if (stat) {
			return client.sendFile(myPath, serverPath);
		} else {
			return -10000;
		}
	}
	
	@Override
	protected void onPostExecute(Integer result) {
		// TODO Auto-generated method stub
		if (pDialog.isShowing()) {
			pDialog.dismiss();
		}
		
		switch (result) {
		case 0:
			listener.onSent();
			break;

		default:
			listener.onError();
			break;
		}
	}

}
