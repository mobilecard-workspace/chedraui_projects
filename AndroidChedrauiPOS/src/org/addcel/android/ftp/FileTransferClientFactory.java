package org.addcel.android.ftp;


public class FileTransferClientFactory {
	
	public enum TipoCanal {FTP, SFTP,};
	
	private static FileTransferClient client;
	
	public static synchronized FileTransferClient get(TipoCanal tipo, 
			String host, String login, String pass) {
		switch (tipo) {
		case FTP:
			client = new FTPTransferClient(host, login, pass);
			break;
		case SFTP:
			client = new SFTPTransferClient(host, login, pass);
			break;
			
		default:
			break;
		}
		
		return client;
	}

}
