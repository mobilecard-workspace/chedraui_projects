package org.addcel.android.ftp.callback;

public interface FTPResponseListener {
	
	public void onSent();
	public void onError();

}
