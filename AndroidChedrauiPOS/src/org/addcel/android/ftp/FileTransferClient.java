package org.addcel.android.ftp;

public interface FileTransferClient {

	public boolean connect();
	public int getFile(String myPath, String serverPath);
	public int sendFile(String myPath, String serverPath);
	public boolean disconnect();
}
