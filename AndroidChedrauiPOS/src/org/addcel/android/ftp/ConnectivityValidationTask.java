package org.addcel.android.ftp;

import org.addcel.android.ftp.callback.ConnectionCallback;
import org.addcel.android.util.LogUtil;
import org.addcel.android.util.NetworkUtil;

import android.content.Context;
import android.os.AsyncTask;

public class ConnectivityValidationTask extends AsyncTask<Void, Void, Boolean> {
	
	private Context con;
	private FileTransferClient client;
	private ConnectionCallback callback;
	
	private static final String TAG = "ConnectivityValidationTask";
	
	public ConnectivityValidationTask(Context con, FileTransferClient client, 
			ConnectionCallback callback) {
		// TODO Auto-generated constructor stub
		this.client = client;
		this.con = con;
		this.callback = callback;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		// TODO Auto-generated method stub
		boolean isActive = false;
		isActive = NetworkUtil.isOnline(con);
		if (isActive) {
			LogUtil.text(TAG, "Sí hay internet.");
			if (params != null) {
				isActive = client.connect();
				
				String status = isActive ? "Controlador activo": "Controlador inactivo";
				LogUtil.text(TAG, status);
				client.disconnect();
			}
		}
		
		return isActive;
	}
	
	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		callback.onConnectionChecked(result);
	}

}
