package org.addcel.android.ftp;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;

import org.addcel.android.util.LogUtil;
import org.apache.commons.net.ftp.FTPClient;

public class FTPTransferClient implements FileTransferClient {

	private String host;
	private String login;
	private String password;
	private int port;
	private FTPClient client;

	private static final String TAG = "FTPTransferClient";

	public FTPTransferClient() {
		client = new FTPClient();
	}

	public FTPTransferClient(String host, String login, String pass) {
		// TODO Auto-generated constructor stub
		this();
		this.host = host;
		this.login = login;
		this.password = pass;
		this.port = 21;
	}

	public String getHost() {
		return host;
	}

	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}

	public int getPort() {
		return port;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public boolean connect() {
		try {
			client.connect(InetAddress.getByName(host), port);
			return client.login(login, password);
		} catch (IOException e) {
			e.printStackTrace();
			LogUtil.error(TAG, "connect", e);
			return false;
		}
	}


	@Override
	public int getFile(String myPath, String serverPath) {
		// TODO Auto-generated method stub

		StringBuilder sb = new StringBuilder();
		sb.append("ftp://").append(login).append(":").append(password)
				.append("@").append(host).append(serverPath);

		String urlStr = sb.toString();
		LogUtil.text(TAG, "El archivo se descargará  de: " + urlStr);

		try {
			URL url = new URL(urlStr);
			URLConnection conn = url.openConnection();
			InputStream inputStream = conn.getInputStream();

			FileOutputStream outputStream = new FileOutputStream(myPath);

			byte[] buffer = new byte[4096];
			int bytesRead = -1;
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, bytesRead);
			}

			outputStream.close();
			inputStream.close();
			LogUtil.text(TAG, "Archivo descargado correctamente");
			return 0;

		} catch (IOException ex) {
			LogUtil.error(TAG, "Error en descarga.", ex);
			return -10000;
		}
	}

	@Override
	public int sendFile(String myPath, String serverPath) {
		// TODO Auto-generated method stub

		try {
			FileInputStream srcFileStream = new FileInputStream(myPath);
			boolean status = client.storeFile(serverPath, srcFileStream);
			LogUtil.text("Status", String.valueOf(status));
			srcFileStream.close();
			return status ? 0 : -10000;

		} catch (Exception e) {
			LogUtil.error(TAG, "sendFile", e);
			return -10000;
		} finally {
			disconnect();
		}
	}

	@Override
	public boolean disconnect() {
		// TODO Auto-generated method stub
		try {
			return client.logout();
		} catch (IOException e) {
			return false;
		}
	}

}
