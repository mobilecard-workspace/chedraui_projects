package org.addcel.android.zebra;

import com.zebra.sdk.comm.BluetoothConnectionInsecure;
import com.zebra.sdk.comm.Connection;

public class ConnectionFactory {

	private static Connection connection;
	
	public static synchronized void set(Connection connection) {
		ConnectionFactory.connection = connection;
	}
	
	public static synchronized Connection get(String deviceAddress) {
		if (connection == null) {
			connection = new BluetoothConnectionInsecure(deviceAddress);
		}
		
		return connection;
	}
}
