package org.addcel.android.zxing.util;

import java.util.EnumMap;
import java.util.Map;

import org.addcel.android.util.LogUtil;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

/**
 * 
 * @author carlosgs
 * Solo incluir paquete si se est� usando la librería ZXING, en cualquier otro
 * caso eliminar del proyecto.
 *
 */
public class BarcodeUtil {
	
	private static final int TRANSPARENT = 0xFFFFFFFF;
	private static final int BLACK = 0xFF000000;
	
	private static final String TAG = BarcodeUtil.class.getName();

	private static Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
		String contentsToEncode = contents;
		
		if (contentsToEncode == null) {
		    return null; 
		} 
		
		Map<EncodeHintType, Object> hints = null;
		String encoding = guessAppropriateEncoding(contentsToEncode);
		
		if (encoding != null) {
		    hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
		    hints.put(EncodeHintType.CHARACTER_SET, encoding);
		} 
		
		MultiFormatWriter writer = new MultiFormatWriter();
		BitMatrix result;
		
		try { 
		    result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
		} catch (IllegalArgumentException iae) {
		    // Unsupported format 
		        return null; 
		} 
		
	    int width = result.getWidth();
	    int height = result.getHeight();
	    int[] pixels = new int[width * height];
	    
	    for (int y = 0; y < height; y++) {
	        int offset = y * width;
	        
	        for (int x = 0; x < width; x++) {
	        	pixels[offset + x] = result.get(x, y) ? BLACK : TRANSPARENT;
	        } 
	    } 
		 
	    Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
		
		return bitmap;
    } 
		 
		    
	public static String guessAppropriateEncoding(CharSequence contents) {
		    // Very crude at the moment 
		for (int i = 0; i < contents.length(); i++) {
			
		    if (contents.charAt(i) > 0xFF) {
		    	return "UTF-8"; 
		    } 
		} 
		
		return null; 
	} 
	
	public static Bitmap generateBarcode(String _barcode, BarcodeFormat _f, int _width) {
		// TODO Auto-generated method stub
		LogUtil.text(TAG, "Intentando generar barcode para String: " + _barcode);
	    try { 
	    	 
	        Bitmap bitmap = encodeAsBitmap(_barcode, _f, _width, _width / 2);
	        LogUtil.text(TAG, "Barcode generado exitosamente");
	        return bitmap;
	 
	    } catch (WriterException e) {
	        LogUtil.error(TAG, "WriterException", e);
	        LogUtil.text(TAG, "Falló generación del barcode para String: " + _barcode);
	        return null;
	    } catch (Exception e) {
	    	LogUtil.error(TAG, "Excepción general", e);
	        LogUtil.text(TAG, "Falló generación del barcode para String: " + _barcode);
	        return null;
	    }
	 
	}
}
