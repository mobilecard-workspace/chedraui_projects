package org.addcel.android.download;

public interface DownloadCallback {

	public void onDownloadFinished(int result);
}
