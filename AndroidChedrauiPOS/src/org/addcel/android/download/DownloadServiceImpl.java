package org.addcel.android.download;

import org.addcel.android.ftp.FileTransferClient;

import android.util.Log;

public class DownloadServiceImpl implements DownloadService {

	private FileTransferClient client;
	
	private static final String TAG = DownloadServiceImpl.class.getSimpleName();

	public DownloadServiceImpl(FileTransferClient client) {
		Log.i(TAG, "Inicializando constructor");
		this.client = client;
	}

	@Override
	public int run(final String myPath, final String serverPath) {
		// TODO Auto-generated method stub
		
		boolean stat = client.connect();

		if (stat)
			return client.getFile(myPath, serverPath);
		else
			return -10000;
	}
}
