package org.addcel.android.version;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.addcel.android.util.ErrorUtil;
import org.addcel.android.ws.listener.WSResponseListener;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;
import android.util.Xml.Encoding;

public class VersionWSClient extends AsyncTask<VersionTO, Void, String> {
	
	private String url;
	private WSResponseListener listener;
	private int connectionTimeout;
	private int responseTimeout;
	private String error;
	
	private static final String TAG = "VERSION_CLIENT";
	
	/**
	 * 
	 * @param listener - Callback que indica que acci�n ejecutar con la respuesta
	 * @param url - Url del server que emite el servicio
	 */
	public VersionWSClient(WSResponseListener listener,String url) {
		this.listener = listener;
		this.url = url;		
		this.connectionTimeout = 10000;
		this.responseTimeout = 30000;
		this.error = "";
	}
	
	@Override
	protected String doInBackground(VersionTO... params) {
		// TODO Auto-generated method stub
		try {
			
			HttpParams connParams = new BasicHttpParams();
			
			HttpConnectionParams.setConnectionTimeout(connParams, connectionTimeout);
			HttpConnectionParams.setSoTimeout(connParams, responseTimeout);
			
			DefaultHttpClient httpClient = new DefaultHttpClient(connParams);
			HttpPost httpPost = new HttpPost(url);
			
			Log.d(TAG, "Intentando conectar con: " + url);
			
			List<BasicNameValuePair> nameValuePairs = new LinkedList<BasicNameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("idPlataforma", params[0].getIdPlataforma()));
			nameValuePairs.add(new BasicNameValuePair("idApp", params[0].getIdApp()));
			
			Log.d(TAG, "Con params: " + Arrays.toString(params));
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			
			HttpResponse httpResponse = httpClient.execute(httpPost);
			
			HttpEntity httpEntity = httpResponse.getEntity();
			
			String response = EntityUtils.toString(httpEntity, Encoding.UTF_8.name());
			Log.d(TAG, response);
			
			return response;
			
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "ClientProtocolException", e);
			return error;
		}  catch (ConnectTimeoutException e) {
			Log.e(TAG, "ConnectTimeoutException", e);
			error = ErrorUtil.connection();
			return ErrorUtil.connection();
		} catch(SocketTimeoutException e) {
			Log.e(TAG, "SocketTimeoutException", e);
			error = "El servidor está tardando demasiado tiempo en responder. Intente de nuevo más tarde";
			return error;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "IOException", e);
			return ErrorUtil.connection();
		} 
	}
	
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		
		try {
			
			JSONObject resultJSON = new JSONObject(result);
			listener.onJsonObjectReceived(resultJSON);
			
		} catch (JSONException obEx) {
			
			Log.e(TAG, "", obEx);
			
			try {
				JSONArray resultJSONArray = new JSONArray(result);
				listener.onJsonArrayReceived(resultJSONArray);	
			} catch (JSONException arrEx) {
				
				Log.e(TAG, "", arrEx);
				
				listener.onStringReceived(result);
			}
			
		} catch (NullPointerException e) {
			
			Log.e(TAG, "", e);
			listener.onStringReceived(result);
		}
	}



}
