package org.addcel.android.version;


public class VersionTO {
	
	private String idPlataforma;
	private String idApp;
	
	private static final String ANDROID = "4";
	
	public VersionTO() {}
	
	public VersionTO(String idApp) {
		this.idPlataforma = ANDROID;
		this.idApp = idApp;
	}

	public String getIdPlataforma() {
		return idPlataforma;
	}

	public void setIdPlataforma(String idPlataforma) {
		this.idPlataforma = idPlataforma;
	}

	public String getIdApp() {
		return idApp;
	}

	public void setIdApp(String idApp) {
		this.idApp = idApp;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "idPlataforma=" + idPlataforma + "&idApp=" + idApp; 
	}
	
	
}
