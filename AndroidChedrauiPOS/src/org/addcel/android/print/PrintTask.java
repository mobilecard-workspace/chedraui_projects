package org.addcel.android.print;

import java.io.IOException;
import java.util.Set;

import org.addcel.android.util.LogUtil;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.zebra.sdk.comm.BluetoothConnectionInsecure;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.graphics.ZebraImageFactory;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;

public class PrintTask extends AsyncTask<Void, Void, Integer> {

	private Context con;
	private boolean isRompeFilas;
	private Bitmap barcode;
	private String barcodeStr;
	private String recibo;
	private ProgressDialog pDialog;
	private PrintActionListener listener;
	
	private String TAG = PrintTask.class.getSimpleName();

	public PrintTask(Context con, PrintActionListener listener, Bitmap barcode) {
		// TODO Auto-generated constructor stub
		this.con = con;
		this.listener = listener;
		this.barcode = barcode;
		isRompeFilas = true;
	}
	
	public PrintTask(Context con, PrintActionListener listener, Bitmap barcode, String barcodeStr, String recibo) {
		// TODO Auto-generated constructor stub
		this.con = con;
		this.listener = listener;
		this.barcode = barcode;
		this.barcodeStr = barcodeStr;
		this.recibo = recibo;
		isRompeFilas = true;
	}

	public PrintTask(Context con, PrintActionListener listener, String recibo) {
		// TODO Auto-generated constructor stub
		this.con = con;
		this.listener = listener;
		this.recibo = recibo;
		isRompeFilas = false;
	}

	public Context getCon() {
		return con;
	}

	public boolean isRompeFilas() {
		return isRompeFilas;
	}

	public Bitmap getBarcode() {
		return barcode;
	}

	public String getRecibo() {
		return recibo;
	}

	public void setCon(Context con) {
		this.con = con;
	}

	public void setRompeFilas(boolean isRompeFilas) {
		this.isRompeFilas = isRompeFilas;
	}

	public void setBarcode(Bitmap barcode) {
		this.barcode = barcode;
	}

	public void setRecibo(String recibo) {
		this.recibo = recibo;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		if (pDialog == null || pDialog != null && !pDialog.isShowing()) {
			if (isRompeFilas)
				pDialog = ProgressDialog.show(con, 
						"Generando ticket", "Espere, por favor...", true, false);
			else
				pDialog = ProgressDialog.show(con, 
						"Generando recibo", "Espere, por favor...", true, false);
				
		}
	}

	@Override
	protected Integer doInBackground(Void... arg0) {
		// TODO Auto-generated method stub
		BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		
		if (bluetoothAdapter == null) {
			return -1;
		}
		
		Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
			
		BluetoothDevice device = null;
		
		if (0 < pairedDevices.size()) {
			for (BluetoothDevice bluetoothDevice : pairedDevices) {
				LogUtil.text("MainActivity", "Device: " + bluetoothDevice.getName());
//					if (bluetoothDevice.getName().contains("Star")||bluetoothDevice.getName().contains("STAR")||bluetoothDevice.getName().contains("star")) {
				if (bluetoothDevice.getName().contains("XXXX")||bluetoothDevice.getName().contains("xxxx")) {
					LogUtil.text("MainActivity", "Impresora encontrada.");
					device = bluetoothDevice;
					LogUtil.text("MainActivity", "Impresora asignada.");
				}
				
			}
			
			if (null != device) {
				try {
	                  // Instantiate connection for given Bluetooth® MAC Address.
					LogUtil.text(TAG, device.getAddress());
					if (isRompeFilas) {
						LogUtil.text(TAG, "Intentando imprimir barcode.");
						return printBarcode(device);
		            } else {
						LogUtil.text(TAG, "Intentando imprimir recibo.");
		            	return printReceipt(device);
		            }
		 
		        } catch (Exception e) {
		              // Handle communications error here.
		          e.printStackTrace();
		          return -1;
		        }
			} else {
				LogUtil.text("setBluetooth", "No se cargó dispositivo BT.");
				return -1;
			}
		} else {
			LogUtil.text("setBluetooth", "No están dispositivos BT pareados.");
			return -1;
		}
	}

	@Override
	protected void onPostExecute(Integer result) {
		// TODO Auto-generated method stub
		if (pDialog != null && pDialog.isShowing()) {
			pDialog.dismiss();
		}
		
		switch (result) {
		case 0:
			listener.onSuccess();
			break;

		default:
			listener.onError();
			break;
		}
		
	}
	
	/*
	 *  public static void main(String[] args) {
         try {
             Connection connection = new TcpConnection("192.168.1.32", TcpConnection.DEFAULT_ZPL_TCP_PORT);
             connection.open();
             ZebraPrinter printer = ZebraPrinterFactory.getInstance(connection);
             int x = 0;
             int y = 0;
             printer.printImage("/sdcard/sample.jpg", x, y);
             connection.close();
         } catch (ConnectionException e) {
             e.printStackTrace();
         } catch (ZebraPrinterLanguageUnknownException e) {
             e.printStackTrace();
         } catch (IOException e) {
             e.printStackTrace();
         }
     }
	 */
	
	private int printBarcode(BluetoothDevice device){
		try {
			 Connection connection = new BluetoothConnectionInsecure(device.getAddress());
//			 Connection connection = ConnectionFactory.get(device.getAddress());
             connection.open();
             ZebraPrinter printer = ZebraPrinterFactory.getInstance(connection);
             int x = 0;
             int y = 0;
             connection.write("! UTILITIES\r\nIN-MILLIMETERS\r\nSETFF 10 2\r\nPRINT\r\n".getBytes());
             printer.printImage(ZebraImageFactory.getImage(barcode), x, y, 0, 0, false);
             String bcStr = barcodeStr + "\n\n";
             connection.write(bcStr.getBytes());
             String importe = "TOTAL M.N.: " + recibo + "\n";
             connection.write(importe.getBytes());
             connection.close();
		     return 0;
		 } catch(ConnectionException e) {
			 return -1;
		 } catch(ZebraPrinterLanguageUnknownException e) {
			 return -1;
		 } catch(IOException e) {
			 return -1;
		 }
	}
	
	private int printReceipt(BluetoothDevice device) {
		try { 
			Connection connection = new BluetoothConnectionInsecure(device.getAddress());
//			Connection connection = ConnectionFactory.get(device.getAddress());
			connection.open();
	         // This example prints "This is a ZPL test." near the top of the label.
	         String zplData = recibo;
	         // Send the data to printer as a byte array.
	         connection.write(zplData.getBytes());
	         // Close the connection to release resources.
	         connection.close();
	         return 0;
		} catch (ConnectionException e) {
		     e.printStackTrace();
		     return -1;
		} 
	}

}
