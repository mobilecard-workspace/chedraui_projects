package org.addcel.android.print;

public interface PrintActionListener {
	
	public boolean onSuccess();
	public boolean onError();

}
