package org.addcel.android.font;

public enum Gotham {

	GOTHAM_BOLD("Bold"), GOTHAM_MEDIUM("Medium"), GOTHAM_BOOK("Book");

	private String path;

	private Gotham(String _p) {
		path = "fonts/Gotham/Gotham-" + _p + ".ttf";
	}

	public String getPath() {
		return path;
	}
}
