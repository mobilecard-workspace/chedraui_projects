package org.addcel.android.interfaces;

import org.json.JSONObject;

public interface JSONable {
	
	public JSONObject writeToJSON();
	
	public void readFromJSON(JSONObject json);

}
