package org.addcel.android.interfaces;

import org.apache.http.message.BasicNameValuePair;

public interface Mapable {
	
	public BasicNameValuePair[] writeToMap();

}
