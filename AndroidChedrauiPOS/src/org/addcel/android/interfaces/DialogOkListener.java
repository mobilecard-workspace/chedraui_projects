package org.addcel.android.interfaces;

import android.content.DialogInterface;

public interface DialogOkListener {
	public void ok(DialogInterface dialog, int id);
	public void cancel(DialogInterface dialog, int id);
}
