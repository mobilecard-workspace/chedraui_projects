package org.addcel.android.upload;

import org.addcel.android.ftp.FileTransferClient;

import android.util.Log;

public class UploadServiceImpl implements UploadService {
	
	private FileTransferClient client;
	
	private static final String TAG = UploadServiceImpl.class.getSimpleName();
	
	public UploadServiceImpl(FileTransferClient client) {
		Log.i(TAG, "Inicializando constructor");
		this.client = client;
	}

	@Override
	public int run(String myPath, String serverPath) {
		// TODO Auto-generated method stub
		
		boolean stat = client.connect();
		
		if (stat)
			return client.sendFile(myPath, serverPath);
		else
			return -10000;
		
	}

}
