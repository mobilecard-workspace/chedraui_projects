package org.addcel.android.upload;

public interface UploadService {

	public int run(String myPath, String serverPath);
}
