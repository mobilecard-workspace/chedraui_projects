package org.addcel.android.upload;

import org.addcel.android.ftp.FileTransferClient;

import android.util.Log;

public class MockUploadServiceImpl implements UploadService {
	
	private FileTransferClient client;
	private static final String PATH = "/home/chedraui/version/";
	
	private static final String TAG = MockUploadServiceImpl.class.getSimpleName();
	
	public MockUploadServiceImpl(FileTransferClient client) {
		// TODO Auto-generated constructor stub
		Log.i(TAG, "Inicializando constructor");
		this.client = client;
	}
	

	@Override
	public int run(String myPath, String serverPath) {
		// TODO Auto-generated method stub
		
		boolean stat = client.connect();
		
		if (stat)
			return client.sendFile(myPath, PATH);
		else
			return -10000;
	}

}
