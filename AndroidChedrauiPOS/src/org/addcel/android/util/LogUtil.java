package org.addcel.android.util;

import java.util.Arrays;

import org.addcel.android.enums.AppContext;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class LogUtil {
	
	private static AppContext c = AppContext.getCurrent();
	
	public static void text(String _tag, String _mensaje) {
		if (c.equals(AppContext.DEV) || c.equals(AppContext.PREPROD)) {
			Log.i(_tag, _mensaje);
		}
	}
	
	public static void error(String _tag, String _mensaje, Throwable _t) {
		
		if (c.equals(AppContext.DEV) || c.equals(AppContext.PREPROD)) {
			
			if (_t != null) {
				Log.e(_tag, _mensaje, _t);
			} else {
				Log.e(_tag, _mensaje);
			}
		}
		
	}
	
	public static void request(String _tag, String _request) {
		
		if (c.equals(AppContext.DEV) || c.equals(AppContext.PREPROD)) {
			
			try {
				JSONObject(_tag, new JSONObject(_request));
			} catch (JSONException e) {
				text(_tag, _request);
			}
			
		}
		
	}
	
	public static void JSONObject(String _tag, JSONObject _json) {
		
		if (c.equals(AppContext.DEV) || c.equals(AppContext.PREPROD)) {
		
			try {
				
				Log.i(_tag, _json.toString(4));
				
			} catch (JSONException e) {
				Log.e(_tag, "JSONObject", e);
			}
		}
	}
	
	public static void JSONArray(String _tag, JSONArray _json) {
		
		if (c.equals(AppContext.DEV) || c.equals(AppContext.PREPROD)) {
		
			try {
				
				Log.i(_tag, _json.toString(4));
				
			} catch (JSONException e) {
				Log.e(_tag, "JSONArray", e);
			}
		}
	}
	
	public static void tuples(String _tag, BasicNameValuePair[] _tuples) {
		
		if (c.equals(AppContext.DEV) || c.equals(AppContext.PREPROD)) {
			Log.i(_tag, Arrays.toString(_tuples));
		}
	}
	
	public static void byteArray(String _tag, byte[] _arr) {
		
		if (c.equals(AppContext.DEV) || c.equals(AppContext.PREPROD)) {
			Log.i(_tag, Arrays.toString(_arr));
		}
	}

}
