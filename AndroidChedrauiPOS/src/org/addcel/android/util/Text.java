package org.addcel.android.util;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

import android.text.TextUtils;
import android.util.Log;

public class Text {
	
	private static Locale MX_LOCALE = new Locale("en", "US");
	
	public static Locale getMexLocale() {
		
		if (null == MX_LOCALE)
			MX_LOCALE = new Locale("en", "US");
		
		return MX_LOCALE;
	}
	 
	 public static boolean esCorreo(String correo){
			return (correo.matches("^[_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@+([_a-zA-Z0-9-]+\\.)*[a-zA-Z0-9-]{2,200}\\.[a-zA-Z]{2,6}$"));
		}
	 
	public static String parsePass(String pass){
		int len = pass.length();
		String key = "";
		
		for (int i =0; i < 32 /len; i++){
			key += pass;
		}
		
		int carry = 0;
		while (key.length() < 32){
			key += pass.charAt(carry);
			carry++;
		}
		return key;
	}
		
	public static String reverse(String chain){
		String nuevo = "";
		
		for (int i = chain.length()-1; i >= 0; i--){
			nuevo += chain.charAt(i);
		}
		
		return nuevo;
	}
	
	public static String mergeStr(String first, String second) {
		String result = "";
		String other = Text.reverse(second);
		
		result += ( Integer.toString(other.length()).length() < 2 ) ? "0"+other.length() : Integer.toString(other.length());
		
		String sub1 = first.substring(0, 19);
		String sub2 = first.substring(19, first.length());
		
		result += sub1;
		
		for (int i =0; i < other.length(); i+=2){
			int offset = ( (i+2) <= other.length() ) ? (i+2) : (i+1);
			String next = other.substring(i, offset);
			
			result += next;
			
			result += sub2.substring(0, 2);
			
			sub2 = sub2.substring(2);
		}
		result += sub2;
				
		return result;
	}
		
	public static String formatDate(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
		return format.format(date);
	}
	
	public static String formatDateNoSpace(Date date) {

		SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy", Locale.US);
		return format.format(date);
	}
	
	public static String formatDateChedraui(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd", Locale.US);
		return format.format(date);
	}
	
	public static String formatDateWithSlash(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd", Locale.US);
		return format.format(date);
		
	}
	
	public static String formatDateForShowing(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
		return format.format(date);
	}
	
	public static String formatDateTimeForShowing(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm", Locale.US);
		return format.format(date);
	}
	
	public static String formatTimeForShowing(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("hh:mm", Locale.US);
		return format.format(date);
	}
	
	public static String formatDateVitamedica(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy", Locale.US);
		return format.format(date);
	}
	
	public static String toVitaDate(String date) {
		Log.i("Text", date);
		Date d = getSimpleDateFromString(date);
		return formatDateVitamedica(d);
	}
	
	public static String formatTimer(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("dd hh:mm:ss", Locale.US);
		return format.format(date);
	}
	

	public static String formatCurrency(double cantidad) {
		
		LogUtil.text("formatCurrency", "" +cantidad);
		
		NumberFormat formatter = NumberFormat.getCurrencyInstance(getMexLocale());
		formatter.setMaximumFractionDigits(2);
		
		String moneyString = formatter.format(cantidad);
		
		return moneyString;
	}
	
	public static Integer[] getYearsArray() {
		Calendar c = Calendar.getInstance();
		Integer[] years = new Integer[10];
		
		int i = 0;
		int year = c.get(Calendar.YEAR);
		
		while (i < 10) {
			
			years[i] = year;
			year++;
			i++;
		}
		
		Log.i("years array", Arrays.toString(years));
		
		return years;
		
	}
	
	public static String trimMonth(int month) {
		month = month + 1;
		
		if (month < 10) {
			return "0" + Integer.toString(month);
		} else {
			return Integer.toString(month);
		}
	}
	
	public static String trimYear(int year) {
		return Integer.toString(year).substring(2, 4);
	}
	
	public static String getVigencia(int month, int year ) {
		return trimMonth(month) + "/" + year;
	}
	
	public static Date getDateFromString(String s) {
		 SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
		 try {
			 Date fecha = format.parse(s);
			 Log.i("getDateFromString", fecha.toString());
			return fecha;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return null;
		}
		 
	 }
	
	public static Date getDateFromStringWithMS(String s) {
		 SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S", Locale.US);
		 try {
			 Date fecha = format.parse(s);
			 Log.i("getDateFromString", fecha.toString());
			return fecha;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return null;
		}
		 
	 }
	
	public static String getStringFromDateWithMS(Date d) {

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S", Locale.US);
		return format.format(d);
	}
	
	public static Date getSimpleDateFromString(String s) {
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
		 
		try {
			 Date fecha = format.parse(s);
			 LogUtil.text("getSimpleDateFromString", fecha.toString());
			return fecha;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return null;
		}
		 
	 }
	
	public static Date getSimpleDateFromStringWithSlash(String s) {
		 SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss", Locale.US);
		 try {
			 Date fecha = format.parse(s);
			 LogUtil.text("getSimpleDateFromStringWithSlash", fecha.toString());
			return fecha;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	
	public static Date getDateFromETNString(String s) {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd", Locale.US);
		 try {
			 Date fecha = format.parse(s);
			 LogUtil.text("getDateFromETNString", fecha.toString());
			return fecha;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	
	public static Date getEtnDateFromStringWithSlash(String s) {
		 SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
		 try {
			 Date fecha = format.parse(s);
			 LogUtil.text("getSimpleDateFromStringWithSlash", fecha.toString());
			return fecha;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	
	public static String addParamsToUrl(String url, List<BasicNameValuePair> params) {
		if (! url.endsWith("?"))
			url += "?";
		
		String paramString = URLEncodedUtils.format(params, "utf-8");
		
		url += paramString;
		
		return url;
	}
	
	public static String getKey() { 
		 return String.valueOf(new Date().getTime()).substring(0, 8);
		 
	}
	
	public static String getPeriodosConPipe(List<String> list) {
		String listString = "";
		
		List<String> subList = list.subList(0, list.size() - 1);
		
		for (String periodo : subList) {
			listString = listString + periodo + "|";
		}
		
		return listString + list.get(list.size() - 1);
	}
	
	private static String maskCard(String card) {
		
		StringBuilder maskBuilder = new StringBuilder("");
		String tag = "maskCard";
		
		int cardLen = card.length();
		
		if (4 < cardLen) {
			
			int maskLen = cardLen - 4;
			
			for (int i = 0; i < cardLen; i++) {
				
				if (i < maskLen)
					maskBuilder.append("*");
				else
					maskBuilder.append(card.charAt(i));
			}
			
			
		} else {
			LogUtil.error(tag, "Longitud de tarjeta no v�lida", null);
		}
		
		return maskBuilder.toString();
		
	}
	
	public static String buildJSPaymentString(String nombre, String tarjeta, String tipo, int mes, int anio) {
		
		String tag = "buildJSPaymentString";
		
		LogUtil.text(tag, nombre);
		LogUtil.text(tag, maskCard(tarjeta));
		LogUtil.text(tag, tipo);
		LogUtil.text(tag, String.valueOf(mes));
		LogUtil.text(tag, String.valueOf(anio));
		
		int formattedMes = mes - 1;
		int formattedAnio = anio - Calendar.getInstance().get(Calendar.YEAR);
		
		StringBuilder builder = new StringBuilder("javascript:document.getElementsByName('cc_name')[0].value='").append(nombre).append("';")
													 .append("document.getElementsByName('cc_number')[0].value='").append(tarjeta).append("';")
												     .append("document.getElementsByName('cc_type')[0].value='").append(tipo).append("';")
												     .append("document.getElementsByName('_cc_expmonth')[0].selectedIndex=").append(formattedMes).append(";")
												     .append("document.getElementsByName('_cc_expyear')[0].selectedIndex=").append(formattedAnio).append(";");
		
		return builder.toString();
		
	}
	
	public static String buildJSAmexPaymentString(String nombre) {
		
		String tag = "buildJSAmexPaymentString";
		LogUtil.text(tag, nombre);
		
		return new StringBuilder("javascript:document.getElementsByName('cc_name')[0].value='").append(nombre).append("';").toString();
	}
	
	public static boolean matchNonPrintChar(String s) {
		boolean match = s.matches("[\\x00\\x08\\x0B\\x0C\\x0E-\\x1F]");
		if (match)
			LogUtil.text("matchNonPrintChar", "Es Match");
		else
			LogUtil.text("matchNonPrintChar", "No Es Match");
		
		return match;
	}
	
	
}
