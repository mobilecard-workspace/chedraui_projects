package org.addcel.android.util;

import java.io.IOException;

import org.addcel.android.ws.client.HttpClientFactory;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.format.DateUtils;

public class NetworkUtil {

	
	private static final String TAG = NetworkUtil.class.getSimpleName();
	
	public static boolean isOnline(Context _con) { 
		 ConnectivityManager connectivity = (ConnectivityManager) 
				 _con.getSystemService(Context.CONNECTIVITY_SERVICE);
		 
         if (connectivity != null) {
             NetworkInfo[] info = connectivity.getAllNetworkInfo();
             if (info != null)
                 for (int i = 0; i < info.length; i++)
                     if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                         return true;
                     }
 
         }
         return false;
	} 
	
	public static boolean checkHost(String url) {
		
		LogUtil.text(TAG, "checkHost - Validando status de host: " + url);
		HttpParams httpParams = new BasicHttpParams();
		int some_reasonable_timeout = (int) (10 * DateUtils.SECOND_IN_MILLIS);
		HttpConnectionParams.setConnectionTimeout(httpParams, some_reasonable_timeout);
		HttpConnectionParams.setSoTimeout(httpParams, some_reasonable_timeout);
		HttpGet request = new HttpGet(url);
		request.setParams(httpParams);

        // get the response 
		HttpResponse response = null;
		try { 
		    response = HttpClientFactory.get().execute(request);
		} catch (ClientProtocolException e) {
		    // TODO Auto-generated catch block 
			LogUtil.error(TAG, "checkHost", e);
		    return false;
		} catch (IOException e) {
		    // TODO Auto-generated catch block 
			LogUtil.error(TAG, "checkHost", e);
			return false;
		} 
 
        StatusLine status = response.getStatusLine();
        LogUtil.text(TAG, "Status is: " + status.toString());
        if (status.getStatusCode() == HttpStatus.SC_OK) { 
        	return true;
        } else { 
        	return false;
        } 
	}
	
	
}
