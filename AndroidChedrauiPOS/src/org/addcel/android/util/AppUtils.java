package org.addcel.android.util;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.util.Log;
import android.widget.ArrayAdapter;

public class AppUtils {
	
	private static String TAG = "AppUtils";
	
	public static String getVersionName(Context context, Class<?> cls) 
    {
      try {
        ComponentName comp = new ComponentName(context, cls);
        PackageInfo pinfo = context.getPackageManager().getPackageInfo(comp.getPackageName(), 0);
        return pinfo.versionName;
      } catch (android.content.pm.PackageManager.NameNotFoundException e) {
        return null;
      }
    }
	
	private static List<Integer> getAniosVigencia() {
		Calendar c = Calendar.getInstance();
		int anio = c.get(Calendar.YEAR);
		
		List<Integer> anios = new LinkedList<Integer>();
		
		int amount = anio + 7;
		
		for (int i = anio; i < amount; i++) {
			anios.add(i);
		}
		
		return anios;
	}
	
	private static List<String> getAniosVigenciaS() {
		Calendar c = Calendar.getInstance();
		int anio = c.get(Calendar.YEAR);
		
		List<String> anios = new LinkedList<String>();
		
		int amount = anio + 7;
		
		for (int i = anio; i < amount; i++) {
			anios.add(String.valueOf(i));
		}
		
		return anios;
	}
	
	public static ArrayAdapter<Integer> getAniosVigenciaAdapter(Context _con) {
		
		ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(
				_con, android.R.layout.simple_spinner_item, 
				getAniosVigencia()
			);
		adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
		
		return adapter;
		
	}
	
	public static ArrayAdapter<String> getAniosVigenciaSAdapter(Context _con) {
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				_con, android.R.layout.simple_spinner_item, 
				getAniosVigenciaS()
			);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		return adapter;
		
	}
	
	
	public static List<String> getAniosConsulta() {
		Calendar c = Calendar.getInstance();
		int anio = c.get(Calendar.YEAR);
		
		List<String> anios = new LinkedList<String>();
		int amount = anio + 6;
		
		for (int i = anio; i < amount; i++) {
			anios.add("" + anio--);
		}
		
		return anios;
	}
	
	public static String getKey() {
		Long seed = new Date().getTime();
		
		String key = String.valueOf(seed).substring(0, 8);
		Log.d(TAG, "SENSITIVE KEY: " + key);
		
		return key;
	}
	
	public static int getAnioVigenciaPosition(String vigenciaAnio) {
		Calendar c = Calendar.getInstance();
		int anio = c.get(Calendar.YEAR);
		
		int fin = Integer.parseInt("20" + vigenciaAnio);
		int diff = fin - anio;
		
		Log.d(TAG, "getAnioVigenciaPosition()" + diff);
		
		return diff;
	}
	
	/**
	 * 
	 * @param vigencia Vigencia a parsear en mes y a�o
	 * @return Arreglo de String donde la primera posici�n es el mes, y la segunda el a�o
	 */
	public static String[] getVigenciaArray(String vigencia) {
		return vigencia.split("/");
	}
}
