/*
 * COPYRIGHT:
 * Licensed Materials - Property of Toshiba Global Commerce Solutions, Inc.
 * Author: Daniel Flores Cruz
 * 
 * Change Activity Log
 * Reason  Date     Who  Description
 * ------- -------- ---  -----------
 * 
 */
package org.addcel.android.util;

public class EAN13 {
	
	public static void main(String args[]){
		
		
		//String UPC = "750300654628";
		//String UPC = "081346301088";
		String UPC = "762350015608";
		
		
		EAN13 ean13 = new EAN13();
		System.out.println(ean13.generateVerDigit(UPC));
		System.out.println(EAN13.checkDigit(UPC));
		
	}
	
	
	public String generateVerDigit(String s){
		
		int odd = 0;
		int even = 0;
		
		int temp = 0;
		
		int result = 0;
		
		//Para los codigos que traen un cero a la izquierda
		if(s.length() == 11)
			s = "0" + s;
		
		
		//Sólo si es EAN13, genera el digito verificador
		if(s.length() == 12){
			
			for(int i = s.length()-1; i >= 0; i-=2){
				odd += Character.getNumericValue(s.charAt(i));
				even += Character.getNumericValue(s.charAt(i-1));
			}
			
			odd *= 3;
			
			result = even + odd;
			
			temp = roundTen(result);
		
		}
		
		return temp - result + "";
		
	}
	
	
	public int roundTen(int val){
		//int x = val;
		for(int i = 0; i < 9; i++) {
			if(val % 10 == 0)
				break;
			else
				val++;
		}
		return val; 
	}
	
	static public String checkDigit(String s){
		int evenPos = 0;
		int oddPos = 0;
		int i = 0;
		for(i=s.length()-1; i>=0; i-=2){
			if(i>=0) evenPos += Character.getNumericValue(s.charAt(i));
			if(i-1>=0) oddPos += Character.getNumericValue(s.charAt(i-1));			
		}
		
		evenPos = 10 -((3*evenPos + oddPos)%10);
		
		return s + evenPos;
	}


}


