package org.addcel.android.util;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class GraphicUtil {

	/**
	 * Valida el build del tel�fono para seleccionar que m�todo setBackgroundUtilizar
	 * @param _v Layout al cual se le modificar&aacute el background
	 * @param _d Drawable que contiene el gr&aacute;fico a utilizar
	 */
	public static void setBackgroundDrawableWithBuildValidation(View _v, Drawable _d) {
		try {
			if (Build.VERSION.SDK_INT >= 16)
				_v.setBackground(_d);
			else
			    _v.setBackgroundDrawable(_d);
		} catch (Exception e) {
			Log.e("GraphicUtil", "setBackgroundDrawableWithBuildValidation", e);
		}
	}
	
	public static void setDrawableTop(TextView _v, Drawable _d) {
	
		int version = Build.VERSION.SDK_INT;
		
		Log.d("setDrawableTop", String.valueOf(version));
		
		if (version >= 16) {
			Log.d("setDrawableTop", "versi�n > 16");
			_v.setCompoundDrawables(null, _d, null, null);
		}
	}
	
	
}
