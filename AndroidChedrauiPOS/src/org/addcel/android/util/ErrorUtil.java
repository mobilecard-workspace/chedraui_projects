
package org.addcel.android.util;

/**
 * 
 * @author carlosgs
 *	Clase utilitaria para crear mensajes de Error configurables.
 */

public class ErrorUtil {
	
	public static String notLogged(String tipo) {
		return "Debes iniciar sesión para acceder a este " + tipo + "."; 
	}
	
	public static String connection() {
		return "Error de conexión. Intente de nuevo más tarde.";
	}
	
	public static String notRetrieved(String datos) {
		return "Error al obtener " + datos;
	}
	
	public static String comission() {
		return "Error al obtener la comisión.";
	}
	
	public static String tags() {
		return "Error al obtener TAGs.";
	}
	
	
	public static String notAvailable(String data) {
		return "No hay " + data + " disponibles";
	}


	public static String isEmpty(String field) {
		
		return "El campo " + field + " no puede ir vacío";
	}
	
	public static String length(String field, int minLength, int maxLength) {
		
		if (minLength == maxLength)
			return "El campo " + field + " debe tener " + minLength + " caracteres."; 
		else
			return "El campo " + field + " debe tener " + minLength + "-" + maxLength + " caracteres."; 
	}
	
	public static String notValid(String field) {
		
		return field + " no válido.";
	}
	
	public static String notAMatch(String field1, String field2) {
		
		return field1 + " y " + field2 + " no coinciden.";
	}
	
	public static String emptyArray(String tipo) {
		return "No existen " + tipo + ".";
	}
	
	public static String spinner(String tipo) {
		return "Seleccione " + tipo;
	}
	
	public static String terminos() {
		return "Acepte términos y condiciones.";
	}
}
