package org.addcel.android.ws.client;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.addcel.android.crypto.AddcelCrypto;
import org.addcel.android.crypto.Cifrados;
import org.addcel.android.crypto.Crypto;
import org.addcel.android.enums.Plataforma;
import org.addcel.android.util.LogUtil;
import org.addcel.android.util.Text;
import org.addcel.android.ws.listener.WSResponseListener;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Xml.Encoding;


/**
 * <p>Clase que extiende de AsyncTask, espera un par�metro String que es
 *	un request a enviar, y devuelve un String que es el response a una petici�n http.
 *	Idealmente esperar�a �nicamente un JSONObject y el resultado ser�a otro JSONObject, pero por 
 *	implementaciones legadas, debe poder recibir Strings.</p>
 *
 * @author carlosgs
 * 
 */
public class WSClient extends AsyncTask<String, Void, String> {

	private Context con;
	private boolean loader;
	private WSResponseListener listener;
	private String url;
	private Cifrados cifrado;
//	private Catalogos catalogo;
	private static final String TAG = "WSClient";
	private ProgressDialog progressDialog;
	private String key;
	private int connectionTimeout;
	private int responseTimeout;
	private boolean cancellable;
	
	/**
	 * 
	 * @param _con AppContext desde el que se ejecuta la clase. El context se usa 
	 * para mostrar un ProgressDialog.
	 */
	public WSClient(Context _con) {
		con = _con;
		connectionTimeout = 10000;
		responseTimeout = 30000;
		cancellable = true;
	}
	
	public WSClient hasLoader(boolean _loader) {
		loader = _loader;
		
		return this;
	}
	
	public WSClient setListener(WSResponseListener _listener) {
		if (null != _listener)
			listener = _listener;
		
		return this;
	}
	
	public WSClient setUrl(String _url) {
		if (!TextUtils.isEmpty(_url))
			url = _url;
		
		return this;
	}
	
	public WSClient setCifrado(Cifrados _cifrado) {
		cifrado = _cifrado;
		
		return this;
	}
	
	public WSClient setKey(String _key) {
		key = _key;
		
		return this;
	}
	
//	public WSClient setCatalogo(Catalogos _catalogo) {
//		catalogo = _catalogo;
//		
//		return this;
//	}
	
	public WSClient setConnectionTimeout(int _timeout) {
		connectionTimeout = _timeout;
		
		return this;
	}
	
	public WSClient setResponseTimeout(int _timeout) {
		responseTimeout =_timeout;
		
		return this;
	}
	
	public WSClient setCancellable(boolean _cancellable) {
		cancellable = _cancellable;
		
		return this;
	}
	

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		if (loader) {
			progressDialog = ProgressDialog.show(con, "Cargando...", "Espere por favor...", cancellable);
		}
	}
	
	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		
		AndroidHttpClient httpClient = AndroidHttpClient.newInstance(Plataforma.ANDROID.getNombre(), con);

		HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), connectionTimeout);
		HttpConnectionParams.setSoTimeout(httpClient.getParams(), responseTimeout);
		
		HttpPost httpPost = new HttpPost(url);
		
		try {

			LogUtil.text(TAG, "Intentando conectar con: " + url);
			
			if (params.length > 0 && params != null) {
				
				LogUtil.request(TAG, params[0]);
				
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
				
				nameValuePairs.add(new BasicNameValuePair("json", encryptRequest(cifrado, params[0])));
		        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		        
		        LogUtil.text(TAG, "Con params: " + nameValuePairs.toString());
			}
			
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			String response = EntityUtils.toString(httpEntity, Encoding.UTF_8.name());
			httpClient.close();
			response = decryptResponse(cifrado, response);
			
			return response;
			
		} catch (ClientProtocolException e) {
			LogUtil.error(TAG, "ClientProtocolException",e);
			httpClient.close();
			return "";
		} catch (ConnectTimeoutException e) {
			LogUtil.error(TAG, "ConnectTimeoutException", e);
			httpClient.close();
			return "";
		} catch(SocketTimeoutException e) {
			LogUtil.error(TAG, "SocketTimeoutException", e);
			httpClient.close();
			return "";
		} catch (IOException e) {
			LogUtil.error(TAG, "IOException", e);
			httpClient.close();
			return "";
		}
	}
	
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		
		if (loader)
			progressDialog.dismiss();
		
		try {
			JSONObject resultJSON = new JSONObject(result);
			
//			if (this.catalogo != null) {
//				
//				JSONArray array = resultJSON.getJSONArray(catalogo.getNombre());
//				List<BasicNameValuePair> catalogo = new ArrayList<BasicNameValuePair>(array.length());
//				
//				for (int i = 0; i < array.length(); i++) {
//					JSONObject item = array.getJSONObject(i);
//					catalogo.add(new BasicNameValuePair(item.getString(this.catalogo.getDescripcion()), item.getString(this.catalogo.getClave())));
//				}
//				
//				listener.onCatalogReceived(catalogo);
//				
//			} else {
				LogUtil.JSONObject(TAG, resultJSON);
				listener.onJsonObjectReceived(resultJSON);
			
//			}
		} catch (JSONException obEx) {
			
			LogUtil.error(TAG, "onPostExecute", obEx);
			
			try {
				JSONArray resultJSONArray = new JSONArray(result);
				LogUtil.JSONArray(TAG, resultJSONArray);
				listener.onJsonArrayReceived(resultJSONArray);	
			
			} catch (JSONException arrEx) {
				
				LogUtil.error(TAG, "onPostExecute", arrEx);
				LogUtil.text(TAG, result);
				
				listener.onStringReceived(result);
			}
			
		} catch (NullPointerException e) {
			
			LogUtil.error(TAG, "onPostExecute", e);
			listener.onStringReceived(result);
		}
	}
	
	/**
	 * 
	 * @param c M�todo de cifrado a utilizar
	 * @param request String a cifrar.
	 * @return String cifrada, usando el m�todo seleccionado.
	 */
	private String encryptRequest(Cifrados c, String request) {
		
		if (null == c)
			return AddcelCrypto.encryptHard(request);
		
		switch (c) {
		case LIMPIO:
			return request;
		case HARD:
			return AddcelCrypto.encryptHard(request);
		case SENSITIVE:
			return AddcelCrypto.encryptSensitive(AddcelCrypto.getKey(), request);
		case MIXED:
			return AddcelCrypto.encryptSensitive(AddcelCrypto.getKey(), request);
		case MOBILECARD:
			String encrypt = Crypto.aesEncrypt(Text.parsePass(key), request);
			return Text.mergeStr(encrypt, key);
		
		default:
			return AddcelCrypto.encryptHard(request);
		}
	}
	
	/**
	 * 
	 * @param c M�todo de cifrado a utilizar
	 * @param response String a descifrar.
	 * @return String limpia, descifrada con el m�todo seleccionado.
	 */
	private String decryptResponse(Cifrados c, String response) {
		
		if (null == c)
			return AddcelCrypto.decryptHard(response);
		
		switch (c) {
		case LIMPIO:
			return response;
		case HARD:
			return AddcelCrypto.decryptHard(response);
		case SENSITIVE:
			return AddcelCrypto.decryptSensitive(response);
		case MIXED:
			return AddcelCrypto.decryptHard(response);
		case MOBILECARD:
			return Crypto.aesDecrypt(Text.parsePass(key), response);

		default:
			return AddcelCrypto.decryptHard(response);
		}
	}

}
