package org.addcel.android.ws.client;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

import android.net.http.AndroidHttpClient;

public class HttpClientFactory {

	private static HttpClient httpClient;
	
	public static synchronized void set(HttpClient httpClient) {
		HttpClientFactory.httpClient = httpClient;
	}
	
	public static synchronized HttpClient get() {
		
		if (httpClient == null) {
			httpClient = new DefaultHttpClient();
		}
		
		return httpClient;
	}
}
