package org.addcel.android.ws.listener;

import org.addcel.android.chedrauipos.listener.LoginListener;

import android.content.Context;

public class ListenerFactory {
	
	private static WSResponseListener listener;
	
	public static synchronized void set(WSResponseListener listener) {
		ListenerFactory.listener = listener;
	}
	
	public static synchronized WSResponseListener get(Context con) {
		if (listener == null) {
			listener = new LoginListener(con);
		}
		
		return listener;
	}

}
