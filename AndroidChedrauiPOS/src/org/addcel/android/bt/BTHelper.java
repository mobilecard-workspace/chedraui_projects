package org.addcel.android.bt;

import android.bluetooth.BluetoothDevice;

public interface BTHelper {
	

	public final static int REQUEST_ENABLE_BT = 1;
	
	public BluetoothDevice getDevice();

}
