package org.addcel.android.bt;

import java.util.Set;

import org.addcel.android.util.LogUtil;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class TecladoHelper implements BTHelper {
	
	private Context con;
	
	private static final String TAG = TecladoHelper.class.getSimpleName();

	@Override
	public BluetoothDevice getDevice() {
		// TODO Auto-generated method stub
		BluetoothDevice device = null;

		BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		
		if (bluetoothAdapter == null) {
			Toast.makeText(con, "Bluetooth no disponible en este dispositivo", Toast.LENGTH_SHORT).show();
		} else {
			
			LogUtil.text(TAG, "Si hay adaptador.");
			
			if (!bluetoothAdapter.isEnabled()) {
				LogUtil.text(TAG, "Bluetooth no está activo. Activando...");
				Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				((Activity) con).startActivityForResult(enableBluetooth, REQUEST_ENABLE_BT);
				LogUtil.text(TAG, "Bluetooth activado.");
			}
			
			Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
			
			if (0 < pairedDevices.size()) {
				for (BluetoothDevice bluetoothDevice : pairedDevices) {
					LogUtil.text(TAG, "Device: " + bluetoothDevice.getName());
					if (bluetoothDevice.getName().contains("cs")||bluetoothDevice.getName().contains("CS")) {
						LogUtil.text(TAG, "Teclado encontrado.");
						device = bluetoothDevice;
						LogUtil.text(TAG, "Teclado asignado.");
					}
					
				}				
			} else {
				LogUtil.text(TAG, "No están dispositivos BT pareados.");
			}
		}
		
		return device;
	}

}
