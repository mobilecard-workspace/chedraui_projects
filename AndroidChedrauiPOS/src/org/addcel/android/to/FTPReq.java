package org.addcel.android.to;

import java.util.Arrays;

import org.addcel.android.util.LogUtil;

import android.text.TextUtils;

public class FTPReq {
	
	private String ip;
	private String user;
	private String pwd;
	private String port;
	
	private static final String TAG = FTPReq.class.getName();
	
	public FTPReq() {
	}
	
	public String getIp() {
		return ip;
	}
	
	public String getUser() {
		return user;
	}
	
	public String getPwd() {
		return pwd;
	}
	
	public String getPort() {
		return port;
	}
	
	public FTPReq setIp(String ip) {
		this.ip = ip;
		return this;
	}
	
	public FTPReq setUser(String user) {
		this.user = user;
		return this;
	}
	
	public FTPReq setPwd(String pwd) {
		this.pwd = pwd;
		return this;
	}
	
	public FTPReq setPort(String port) {
		this.port = port;
		return this;
	}
	
	public byte[] ipToByteArr() {
		
		String[] ipArr = TextUtils.split(ip, "\\.");
		LogUtil.text(TAG, Arrays.toString(ipArr));
		byte[] byteArr = new byte[] {(byte) Integer.parseInt(ipArr[0]),
									(byte) Integer.parseInt(ipArr[1]),
									(byte) Integer.parseInt(ipArr[2]),
									(byte) Integer.parseInt(ipArr[3]),};
		
		return byteArr;
		
		
		
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Usuario -> ").append(user)
		.append("\nPassword -> ").append(pwd)
		.append("\nIp -> ").append(ip)
		.append("\nPort -> ").append(port);
		
		return sb.toString();
	}
}
