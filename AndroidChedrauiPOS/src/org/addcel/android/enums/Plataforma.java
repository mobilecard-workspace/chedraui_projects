package org.addcel.android.enums;

import org.json.JSONException;
import org.json.JSONObject;

public enum Plataforma {

	ANDROID(1, "Android"),
	IOS(2, "IOS"),
	BB(3, "BlackBerry"),
	WP(4, "Windows"),
	WEB(5, "WEB");

	private int id;
	private String nombre;
	
	private Plataforma(int _id, String _nombre) {
		id = _id;
		nombre = _nombre;
	}

	public int getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}
	
	public JSONObject toJSON() {
		
		try {
			
			return new JSONObject().put("idPlataforma", id);
			
		} catch (JSONException e) {
			return null;
		}
	}
	
	
	
}
