package org.addcel.android.enums;

import org.json.JSONException;
import org.json.JSONObject;

public enum TokenData {

	MEGACABLE(27, "userPrueba", "passwordPrueba"),
	EDOMEX(23, "userPrueba", "passwordPrueba"),
	DIESTEL(0, "diestelUsuario", "ym@J!ZHL"),
	CHEDRAUI(36, "userPrueba", "passwordPrueba");
	
	private int producto;
	private String usuario;
	private String password;
	
	private TokenData(int _prod, String _u, String _p) {
		producto = _prod;
		usuario = _u;
		password = _p;
	}
	
	public int getProducto() {
		return producto;
	}

	public String getUsuario() {
		return usuario;
	}

	public String getPassword() {
		return password;
	}
	
	public static String buildRequest(TokenData t) {
		
		try {
			
			JSONObject json = new JSONObject().put("usuario", t.getUsuario())
												.put("password", t.getPassword());
			
			if (t.producto != 0)
				json.put("idProveedor", t.getProducto());
			
			return json.toString();
			
		} catch (JSONException e) {
			
			return null;
		}
		
	}
}
