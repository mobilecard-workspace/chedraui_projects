package org.addcel.android.enums;

public enum AppContext {

	DEV("http://50.57.192.214:8080/"), 
	PREPROD("http://www.mobilecard.mx:8080/"),
	PROD("http://www.mobilecard.mx:8080/"),
	SECURE("https://www.mobilecard.mx/");
	
	private static final AppContext CURRENT = DEV;
	
	private String url;
	
	private AppContext(String _url) {
		url = _url;
	}
	
	public String getUrl() {
		return url;
	}
	
	
	public static AppContext getCurrent() {
		return CURRENT;
	}
	
}
