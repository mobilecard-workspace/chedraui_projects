package org.addcel.android.session;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;

public class SessionManager {
	SharedPreferences pref;
	Editor editor;
	Context _context;
	
	int PRIVATE_MODE = 0;
	
	private static final String PREF_NAME = "chedrauipos";
	private static final String HAS_ATTRS = "hasAttributes";
	public static final String CONSECUTIVO = "consecutivo";
	public static final String FECHA_ACT = "fechaActualizacion";
	public static final String UPDATED = "updated";
	
	public SessionManager(Context context) {
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}
	
	public int getConsecutivo() {
		return pref.getInt(CONSECUTIVO, 0);
	}
	
	public String getActualizacion() {
		return pref.getString(FECHA_ACT, "");
	}
	
	public void setConsecutivo(int consecutivo) {
		editor.putInt(CONSECUTIVO, consecutivo);
		editor.commit();
	}
	
	public void setFechaActualizacion(String fechaActualizacion) {
		editor.putString(FECHA_ACT, fechaActualizacion);
		editor.commit();
	}
	
	public Date getFechaActualizacion() {
		String act = pref.getString(FECHA_ACT, "");
		if (!TextUtils.isEmpty(act)) {
			try {
				SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd");
				return s.parse(act);
			}catch (ParseException e) {
				return null;
			}
		} else {
			return null;
		}
	}
	
	public boolean getUpdated() {
		return pref.getBoolean(UPDATED, false);
	}
	
	public void setUpdated(boolean updated) {
		editor.putBoolean(UPDATED, updated);
		editor.commit();
	}
	
	public void logoutUser() {
		editor.clear();
		editor.commit();
	}
	
	public boolean arePreferencesSet() {
		return pref.getBoolean(HAS_ATTRS, false);
	}
	
}
