package org.addcel.android.chedrauipos.activity;

import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;

import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.adapter.ProductoAdapter;
import org.addcel.android.chedrauipos.constant.Url;
import org.addcel.android.chedrauipos.data.Orden;
import org.addcel.android.chedrauipos.listener.ArticuloQueryListener;
import org.addcel.android.chedrauipos.listener.ProductoListChangeListener;
import org.addcel.android.chedrauipos.service.SessionTimerBroadcastService;
import org.addcel.android.chedrauipos.session.UserSession;
import org.addcel.android.chedrauipos.sql.ArticuloDataSource;
import org.addcel.android.chedrauipos.sql.TicketDataSource;
import org.addcel.android.chedrauipos.sql.task.ArticuloTask;
import org.addcel.android.chedrauipos.to.Articulo;
import org.addcel.android.chedrauipos.to.PagoReq;
import org.addcel.android.chedrauipos.to.Result;
import org.addcel.android.chedrauipos.to.Ticket;
import org.addcel.android.chedrauipos.util.GeneraArchivoMTL;
import org.addcel.android.chedrauipos.util.GeneraCodigoBarras;
import org.addcel.android.chedrauipos.util.GeneraCodigoBarrasFactory;
import org.addcel.android.crypto.Cifrados;
import org.addcel.android.enums.AppContext;
import org.addcel.android.enums.TokenData;
import org.addcel.android.ftp.ConnectivityValidationTask;
import org.addcel.android.ftp.FTPTransferClient;
import org.addcel.android.ftp.FileSendTask;
import org.addcel.android.ftp.FileTransferClient;
import org.addcel.android.ftp.FileTransferClientFactory;
import org.addcel.android.ftp.FileTransferClientFactory.TipoCanal;
import org.addcel.android.ftp.callback.ConnectionCallback;
import org.addcel.android.ftp.callback.FTPResponseListener;
import org.addcel.android.print.PrintActionListener;
import org.addcel.android.print.PrintTask;
import org.addcel.android.session.SessionManager;
import org.addcel.android.upload.MockUploadServiceImpl;
import org.addcel.android.upload.UploadService;
import org.addcel.android.util.ErrorUtil;
import org.addcel.android.util.LogUtil;
import org.addcel.android.util.Text;
import org.addcel.android.widget.SignatureView;
import org.addcel.android.ws.client.WSClient;
import org.addcel.android.ws.listener.WSResponseListener;
import org.addcel.android.zebra.ConnectionFactory;
import org.addcel.android.zxing.util.BarcodeUtil;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.text.method.TextKeyListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.datecs.audioreader.AudioReader;
import com.datecs.audioreader.AudioReader.FinancialCard;
import com.datecs.audioreader.activity.AudioReaderHelper;
import com.datecs.audioreader.activity.AudioReaderTask;
import com.google.zxing.BarcodeFormat;
import com.zebra.sdk.comm.Connection;

public class MainActivity extends Activity implements ConnectionCallback{

	ListView prodList;
	TextView descView;
	TextView totalView;
	TextView numArticulosView;
	EditText barcodeText;
	FrameLayout frame;

	private ProductoAdapter adapter;
	private ArticuloDataSource aDataSource;
	private TicketDataSource tDataSource;
	private FinancialCard card;
	private boolean lectorConectado;
	private boolean impresionExitosa;

	ProgressDialog loader;

	//ActionBar
	EditText txtSearch;
	
	// Ventana auxiliar
	private PopupWindow auxWindow;

	// Elementos vista pago
	EditText nombreText;
	EditText mailText;
	EditText tarjetaText;
	EditText mmText;
	EditText aaText;
	EditText cvvText;
	EditText codeText;
	ImageButton btnDismiss;
	ImageView btnScanCard, btnFinalizar;
	PagoReq request;

	// Elementos vista firma
	TextView montoView;
	SignatureView signatureView;
	
	// Elementos vista confirmacion
	TextView titleView, contentView;

	private final static int REQUEST_ENABLE_BT = 1;

	private final HeadsetReceiver headsetReceiver = new HeadsetReceiver();
	private Stack<View> vistas;
	
	private GeneraArchivoMTL generaArchivo = new GeneraArchivoMTL();
	private GeneraCodigoBarras generaBarcode = GeneraCodigoBarrasFactory.get();
	private DisplayMetrics metrics;
	
	private Timer checkingTimer;
	private TimerTask checkingTask;
	private ArtQueryListener listener = new ArtQueryListener();
	
	private UploadService uService;
	private FileTransferClient uClient = FileTransferClientFactory
			.get(TipoCanal.FTP, UserSession.getInstance().getTienda().getUrlFTP(),
					UserSession.getInstance().getTienda().getUserFTP(), 
					UserSession.getInstance().getTienda().getPassFTP());
	
	private static final String TAG = MainActivity.class.getSimpleName();
	private Context con = MainActivity.this;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		metrics = getResources().getDisplayMetrics();

		getActionBar().setBackgroundDrawable(
				new ColorDrawable(getResources().getColor(R.color.ch_orange)));

		frame = (FrameLayout) findViewById(R.id.frame);

		descView = (TextView) findViewById(R.id.view_desc);
		totalView = (TextView) findViewById(R.id.view_total);
		numArticulosView = (TextView) findViewById(R.id.view_numart);
		barcodeText = (EditText) findViewById(R.id.text_code);
		barcodeText.requestFocus();
		barcodeText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(TextUtils.indexOf(s, "\n") == (s.length() -1)) {
					Log.d("afterTextChanged", "Encuentra char");
					if (s.length() > 0)
						new ArticuloTask(con,
								s.toString().trim(),
								listener, aDataSource).execute();
		        }
			}
		});

		prodList = (ListView) findViewById(R.id.list_productos);
		prodList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				Articulo p = ((Articulo) (((ProductoAdapter) parent
						.getAdapter()).getItem(position)));

				prodList.setItemChecked(position, true);
				descView.setText(p.toString());
			}
		});

		aDataSource = new ArticuloDataSource(con);
		tDataSource = new TicketDataSource(con);
		vistas = new Stack<View>();

		buildAdapter();
		
		if (TextUtils.equals("elmanss",UserSession.getInstance().getUsuario().getLogin())) {
			uService = new MockUploadServiceImpl(uClient);
		}

		AudioManager manager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		int max = manager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		manager.setStreamVolume(AudioManager.STREAM_MUSIC, max, 0);
		
		initializeTimerTask(this);
		
		
	}
	
	public void initializeTimerTask(final ConnectionCallback callback) { 
		
        checkingTimer = new Timer();
		checkingTask = new TimerTask() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				new ConnectivityValidationTask(con, uClient, callback)
				.execute();
			}
		};
		
		checkingTimer.schedule(checkingTask, 2000, (int) 3 * DateUtils.MINUTE_IN_MILLIS);
    } 
	
	private BroadcastReceiver br = new BroadcastReceiver() {
		
		public void onReceive(Context context, Intent intent) {
			boolean logout = intent.getBooleanExtra("logout", false);
			if(logout) {
				UserSession.getInstance().logout();
				Toast.makeText(con, "Se ha vencido sesión de usuario.", Toast.LENGTH_SHORT).show();
				finish();
			} else {
				LogUtil.text(TAG, "Hubo evento que interrumpió el timer.");
				try {
					startService(new Intent(con, SessionTimerBroadcastService.class));
				} catch (Exception e) {
					LogUtil.error(TAG, "Broadcast Timer", e);
				}
			}
		};
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub

		android.view.MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);

		View v = (View) menu.findItem(R.id.a_escanear).getActionView();

		/** Get the edit text from the action view */
		txtSearch = (EditText) v.findViewById(R.id.txt_search);
		txtSearch.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(TextUtils.indexOf(s, "\n") == (s.length() -1)) {
					Log.d("afterTextChanged", "Encuentra char");
					if (s.length() > 0)
						new ArticuloTask(con,
								s.toString().trim(),
								listener, aDataSource).execute();
		        }
				
			}
		});
//		txtSearch.requestFocus();
		
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		SessionTimerBroadcastService.setFlagNoCerrarSesion();
		int id = item.getItemId();
		switch (id) {
		case R.id.a_capturar:
			if (txtSearch != null) {
				if (!TextUtils.isEmpty(txtSearch.getText().toString().trim())) {
					new ArticuloTask(con,
							txtSearch.getText().toString().trim(),
							listener, aDataSource).execute();
				} else {
					txtSearch.setError("Escanea o captura el código del producto.");
				}
			}
			break;

		case R.id.a_escanear:
			if (txtSearch != null) {
				txtSearch.requestFocus();
			}
			break;
		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		registerReceiver(headsetReceiver, new IntentFilter(
				Intent.ACTION_HEADSET_PLUG));
		updateReaderState(false);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		registerReceiver(br, new IntentFilter(SessionTimerBroadcastService.TIMER_BR));
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		unregisterReceiver(headsetReceiver);
		unregisterReceiver(br);
//		unregisterReceiver(checkingReceiver);
		if (checkingTimer != null) {
			checkingTimer.cancel();
			checkingTimer = null;
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Orden.get().clear();
	}
	
	@Override
	public void onConnectionChecked(boolean flag) {
		// TODO Auto-generated method stub
		if (flag) {
			findViewById(R.id.btn_pagar).setEnabled(true);
			findViewById(R.id.btn_rompe).setEnabled(true);
			((TextView) findViewById(R.id.view_isactive))
			.setText(R.string.txt_main_activo);
			((TextView) findViewById(R.id.view_isactive))
			.setTextColor(getResources().getColor(android.R.color.holo_green_dark));
		} else {
			findViewById(R.id.btn_pagar).setEnabled(false);
			findViewById(R.id.btn_rompe).setEnabled(false);
			((TextView) findViewById(R.id.view_isactive))
			.setText(R.string.txt_main_inactivo);
			((TextView) findViewById(R.id.view_isactive))
			.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
		}
	}

	private void buildAdapter() {
		// TODO Auto-generated method stub

		adapter = new ProductoAdapter(con, Orden.get().getArticulos(),
				new ListChangeListener());

		adapter.setTotal(Articulo.getTotal(Orden.get().getArticulos()));
		totalView.setText(Text.formatCurrency(adapter.getTotal()));

		prodList.setAdapter(adapter);
	}

	private void buildPagoView(View v) {

		SessionTimerBroadcastService.setFlagNoCerrarSesion();

		View popupView = null;
		int w = metrics.widthPixels - metrics.widthPixels / 10;
		int h = metrics.heightPixels - metrics.heightPixels / 10;
		
		
		if (v != null) {
			popupView = v;
			auxWindow = new PopupWindow(popupView, w, h, true);
		} else {
			
			LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
			popupView = layoutInflater.inflate(R.layout.view_pago, null);

			auxWindow = new PopupWindow(popupView, w, h, true);
			final Context con = popupView.getContext();

			btnDismiss = (ImageButton) popupView.findViewById(R.id.btn_cerrar);
			btnScanCard = (ImageView) popupView.findViewById(R.id.btn_scantarjeta);
			btnFinalizar = (ImageView) popupView.findViewById(R.id.btn_finalizar);

			nombreText = (EditText) popupView.findViewById(R.id.text_nombre);
			mailText = (EditText) popupView.findViewById(R.id.text_mail);
			tarjetaText = (EditText) popupView.findViewById(R.id.text_pan);
			mmText = (EditText) popupView.findViewById(R.id.text_mes);
			aaText = (EditText) popupView.findViewById(R.id.text_anio);
			cvvText = (EditText) popupView.findViewById(R.id.text_cvv);

			if (lectorConectado) {
				btnScanCard.setEnabled(true);
			} else {
				btnScanCard.setEnabled(false);
			}

			btnDismiss.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					auxWindow.dismiss();
					frame.setForeground(getResources().getDrawable(
							android.R.color.transparent));

					if (card != null)
						card = null;
				}
			});

			btnScanCard.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					SessionTimerBroadcastService.setFlagNoCerrarSesion();
					MyAudioReaderTask task = new MyAudioReaderTask(con, false) {

						@Override
						public void onExecute() throws Exception {
							// TODO Auto-generated method stub
							card = AudioReaderHelper.readAndGetCard(con, this);
						}
					};

					task.start();
				}
			});

			btnFinalizar.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					SessionTimerBroadcastService.setFlagNoCerrarSesion();
					if (validate(con, nombreText, mailText, tarjetaText,mmText, aaText, cvvText)) {

						request = new PagoReq().setCon(con)
								.setAnio(aaText.getText().toString().trim())
								.setArticulos(Orden.get().getArticulos())
								.setCvv2(cvvText.getText().toString().trim())
								.setEmail(mailText.getText().toString().trim())
								.setIdUsuario(1000)
								.setMes(mmText.getText().toString().trim().length() <= 1 ? "0" + mmText.getText().toString().trim(): mmText.getText().toString().trim())
								.setNombre(nombreText.getText().toString().trim())
								.setPassword(UserSession.getInstance().getUsuario().getPassword());
						
						String tarjeta = tarjetaText.getText().toString().trim();

						if (card != null) {
							if (TextUtils.isDigitsOnly(tarjeta)) {
								request.setTarjeta(tarjetaText.getText().toString().trim());
							} else {
								String num = getNumber(card);
								if (TextUtils.isEmpty(num))
									Toast.makeText(con, "Error al procesar los datos de tarjeta.", Toast.LENGTH_SHORT).show();
								else
									request.setTarjeta(getNumber(card));
							}
						} else {
							request.setTarjeta(tarjetaText.getText().toString().trim());
						}

						auxWindow.dismiss();
						buildSignatureView();
					}
				}

			});
		}

		auxWindow.showAtLocation(frame, Gravity.CENTER, 0, 0);
		frame.setForeground(getResources().getDrawable(R.color.foreground_dark));
		vistas.push(popupView);

	}

	private void buildSignatureView() {

		SessionTimerBroadcastService.setFlagNoCerrarSesion();
		LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(R.layout.view_firma, null);

		auxWindow = new PopupWindow(popupView, 768, 450, true);
		final Context con = popupView.getContext();

		ImageButton btnDismiss = (ImageButton) popupView.findViewById(R.id.btn_cerrar);
		Button btnPagar = (Button) popupView.findViewById(R.id.btn_pagar);

		montoView = (TextView) popupView.findViewById(R.id.view_monto);
		montoView.setText(Text.formatCurrency(Articulo.getTotal(request.getArticulos())));
		signatureView = (SignatureView) popupView.findViewById(R.id.view_signature);

		btnDismiss.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SessionTimerBroadcastService.setFlagNoCerrarSesion();
				auxWindow.dismiss();
				frame.setForeground(getResources().getDrawable(
						android.R.color.transparent));

				View vista = vistas.pop();
				buildPagoView(vista);
			}
		});
		
		btnPagar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SessionTimerBroadcastService.setFlagNoCerrarSesion();
				if (request != null) {
					new WSClient(con)
                    .setCancellable(false)
                    .hasLoader(true)
                    .setCifrado(Cifrados.HARD)
                    .setListener(new TokenGetListener(con))
                    .setUrl(Url.TOKEN_GET)
                    .execute(TokenData.buildRequest(TokenData.CHEDRAUI));
				}
			}
		});

		auxWindow.showAtLocation(frame, Gravity.CENTER, 0, 0);
		frame.setForeground(getResources().getDrawable(R.color.foreground_dark));

		// vistas.push(popupView);
	}
	
	private void buildConfirmationView(final Result r, final Context con) {

		SessionTimerBroadcastService.setFlagNoCerrarSesion();
		
		switch (r.getIdError()) {
		case 0:
			auxWindow.dismiss();
			frame.setForeground(getResources().getDrawable(android.R.color.transparent));
			
			LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
			View popupView = layoutInflater.inflate(R.layout.view_confirmacion, null);

			auxWindow = new PopupWindow(popupView, 768, 450, true);

			ImageButton btnDismiss = (ImageButton) popupView.findViewById(R.id.btn_cerrar);
			ImageView btnPrint = (ImageView) popupView.findViewById(R.id.btn_print);

			titleView = (TextView) popupView.findViewById(R.id.view_title);
			titleView.setText(r.getMensajeError());
			
			contentView = (TextView) popupView.findViewById(R.id.view_content);
			contentView.setText(r.toString());

			btnDismiss.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					SessionTimerBroadcastService.setFlagNoCerrarSesion();
					vistas.clear();
					auxWindow.dismiss();
					frame.setForeground(getResources().getDrawable(
							android.R.color.transparent));
					
					Orden.get().clear();
					adapter.notifyDataSetChanged();
					adapter.setTotal(0.0);
					descView.setText("");
					totalView.setText(Text.formatCurrency(adapter
							.getTotal()));
					
					
				}
			});
			
			btnPrint.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					SessionTimerBroadcastService.setFlagNoCerrarSesion();
					new PrintTask(con, new OnReceiptPrintListener(con), r.buildArticleListString()).execute();
				}
			});

			auxWindow.showAtLocation(frame, Gravity.CENTER, 0, 0);
			frame.setForeground(getResources().getDrawable(R.color.foreground_dark));

			
			break;

		default:
			Toast.makeText(con, r.getMensajeError(), Toast.LENGTH_SHORT).show();
			auxWindow.dismiss();
			frame.setForeground(getResources().getDrawable(android.R.color.transparent));
			
			View vista = vistas.pop();
			buildPagoView(vista);
			break;
		}
		
	}
	
	private void buildBarcodeView(final Ticket t, final Context con) {

		SessionTimerBroadcastService.setFlagNoCerrarSesion();
		LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(R.layout.view_barcode, null);

		auxWindow = new PopupWindow(popupView, 768, 450, true);

		ImageButton btnDismiss = (ImageButton) popupView.findViewById(R.id.btn_cerrar);
		ImageButton btnPrint = (ImageButton) popupView.findViewById(R.id.b_print);
		ImageView barcode = (ImageView) popupView.findViewById(R.id.barcode);
		TextView bView = (TextView) popupView.findViewById(R.id.view_barcode);
		
		final Bitmap b = BarcodeUtil.generateBarcode(t.getBarcode(), BarcodeFormat.EAN_13, 300);
		
		if (b != null) {
			barcode.setImageBitmap(b);
		}
		
		bView.setText(t.getBarcode());

		btnDismiss.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SessionTimerBroadcastService.setFlagNoCerrarSesion();
				
				if (impresionExitosa) {
					vistas.clear();
					auxWindow.dismiss();
					frame.setForeground(getResources().getDrawable(
							android.R.color.transparent));
					
					Orden.get().clear();
					adapter.notifyDataSetChanged();
					adapter.setTotal(0.0);
					descView.setText("");
					totalView.setText(Text.formatCurrency(adapter
							.getTotal()));
				} else {
					showCleanWarning();
				}
			}
		});
		
		btnPrint.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SessionTimerBroadcastService.setFlagNoCerrarSesion();
				new PrintTask(con, new OnReceiptPrintListener(con), 
						b,
						t.getBarcode(), Text.formatCurrency(
								Articulo.getTotal(Orden.get().getArticulos())
							)
						).execute();
			}
		});

		auxWindow.showAtLocation(frame, Gravity.CENTER, 0, 0);
		frame.setForeground(getResources().getDrawable(R.color.foreground_dark));
		
	}

	private boolean validate(Context con, EditText... texts) {

		for (EditText editText : texts) {

			if (TextUtils.isEmpty(editText.getText().toString().trim())) {
				// editText.requestFocus();
				// editText.setError("El campo no puede ir vac�o.");
				//
				Toast.makeText(con,
						"Llena la totalidad de campos en la forma.",
						Toast.LENGTH_SHORT).show();
				return false;
			}
		}

		return true;

	}

	public void deleteAllItems(View v) {

		SessionTimerBroadcastService.setFlagNoCerrarSesion();
		if (Orden.get().getArticulos() != null && !Orden.get().getArticulos().isEmpty())
			showDeleteWarning();
		else
			Toast.makeText(con,
					R.string.txt_main_productos_vacio_eliminar,
					Toast.LENGTH_SHORT).show();

	}

	public void executePayment(View v) {

		SessionTimerBroadcastService.setFlagNoCerrarSesion();
		if (Orden.get().getArticulos() != null && !Orden.get().getArticulos().isEmpty()) {
			buildPagoView(null);
		} else {
			Toast.makeText(con,
					R.string.txt_main_productos_vacio, Toast.LENGTH_SHORT)
					.show();
		}

	}
	
	public void runFTP(View v) {
		SessionTimerBroadcastService.setFlagNoCerrarSesion();
		LogUtil.text(TAG, "Entrando a runFTP");
		if (UserSession.getInstance().isLoggedIn()) {
			if (Orden.get().getArticulos() != null && !Orden.get().getArticulos().isEmpty()) {
				LogUtil.text(TAG, "Hay artículos");
				
				SessionManager pref = new SessionManager(con);
				String consecutivo = generaBarcode.generaNSTicket(
						con, pref.getConsecutivo());
				String barcode = generaBarcode.generaCodigoBarrasTicket(
						consecutivo, Articulo.getTotalString(Orden.get().getArticulos()));
				String fileString = Articulo.getFileString(Orden.get().getArticulos());
				LogUtil.text(TAG, "String que contiene al archivo: " + fileString);
				
				if (!TextUtils.isEmpty(fileString)) {
				
					String mtlFileName = generaArchivo.generaArchivoMTL(
							con, barcode, fileString);
					FileSendListener listener = new FileSendListener(
							consecutivo, barcode, fileString);
					String host = "";
					String login = "";
					String pass = "";
					
					if (AppContext.getCurrent().equals(AppContext.DEV)) {
						host = UserSession.getInstance().getUsuario().getTienda().getUrlFTP();
						login = UserSession.getInstance().getUsuario().getTienda().getUserFTP();
						pass = UserSession.getInstance().getUsuario().getTienda().getPassFTP();
					}
					
					FileTransferClient client = new FTPTransferClient(host, login, pass);
					new FileSendTask(con, //CONTEXT
							client, //CLIENTE
							getFilesDir() + "/" + mtlFileName, // MY_PATH
							"C:TOLEDO/" + mtlFileName, // SERVER_PATH
							listener) //LISTENER
					.execute();
					
					
				} else {
					Toast.makeText(con,
							R.string.txt_main_error_archivo, Toast.LENGTH_SHORT)
							.show();
				}
			} else {
				Toast.makeText(con,
						R.string.txt_main_productos_vacio, Toast.LENGTH_SHORT)
						.show();
			}
		} else {
			Toast.makeText(con,
					R.string.txt_main_errors_sesion, Toast.LENGTH_SHORT)
					.show();
		}
		
	}

	private void showDeleteWarning(String... params) {
		// TODO Auto-generated method stub
		String title = "Borrar Artículos";
		String bPositivo = "Borrar";
		if (params.length > 0) {
			title = params[0];
			bPositivo = params[1];
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(con);

		builder.setTitle(title)
				.setMessage(R.string.txt_main_delete_warning)
				.setCancelable(true)
				.setPositiveButton(bPositivo,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

								Orden.get().clear();
								adapter.notifyDataSetChanged();
								adapter.setTotal(0.0);
								descView.setText("");
								totalView.setText(Text.formatCurrency(adapter
										.getTotal()));
								numArticulosView.setText(String.valueOf(Orden.get().getArticulos().size()));

							}
						})
				.setNegativeButton("Cancelar",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								dialog.cancel();
							}
						}).show();

	}
	
	private void showCleanWarning() {
		String mensaje = "¿Refrescar lista de artículos?";
		String bPositivo = "Aceptar";
		AlertDialog.Builder builder = new AlertDialog.Builder(con);

		builder.setMessage(mensaje)
				.setCancelable(true)
				.setPositiveButton(bPositivo,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

								vistas.clear();
								auxWindow.dismiss();
								frame.setForeground(getResources().getDrawable(
										android.R.color.transparent));
								Orden.get().clear();
								adapter.notifyDataSetChanged();
								adapter.setTotal(0.0);
								descView.setText("");
								totalView.setText(Text.formatCurrency(adapter
										.getTotal()));
								numArticulosView.setText(String.valueOf(Orden.get().getArticulos().size()));

							}
						})
				.setNegativeButton("Cancelar",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								dialog.cancel();
							}
						}).show();
	}
	
	private class InsertTicketTask extends AsyncTask<Ticket, Void, Ticket> {

		@Override
		protected Ticket doInBackground(Ticket... params) {
			// TODO Auto-generated method stub
			LogUtil.text("FindArticleTask", "Insertando ticket: " + params[0]);
			
			tDataSource.open();
			Ticket newT = tDataSource.createTicket(params[0]);
			tDataSource.close();
			
			return newT;
		}
		
		@Override
		protected void onPostExecute(Ticket result) {
			// TODO Auto-generated method stub
			if (result != null) {
				if (result.isProcesado()) {
					buildBarcodeView(result, con);
				}
			}
		}
		
	}

	private class ListChangeListener implements ProductoListChangeListener {

		@Override
		public void onTotalChange(double total) {
			// TODO Auto-generated method stub

			totalView.setText(Text.formatCurrency(total));
		}

		@Override
		public void onDelete() {
			// TODO Auto-generated method stub
			descView.setText("");
			numArticulosView.setText(String.valueOf(Orden.get().getArticulos().size()));
		}

	}
	
	/**
	 * 
	 * @author carlosgs
	 * <p>Clase que implementa la interfaz ArticuloQueryListener.</p>
	 */
	private class ArtQueryListener implements ArticuloQueryListener {

		@Override
		public void onQueryFinished(Articulo articulo) {
			// TODO Auto-generated method stub
			SessionTimerBroadcastService.setFlagNoCerrarSesion();
			if (articulo != null) {
				Orden.get().getArticulos().add(articulo);
				adapter.notifyDataSetChanged();
				adapter.setTotal(Articulo.getTotal(Orden.get().getArticulos()));
				totalView.setText(Text.formatCurrency(adapter.getTotal()));
				numArticulosView.setText(String.valueOf(Orden.get().getArticulos().size()));
				TextKeyListener.clear(txtSearch.getText());
				TextKeyListener.clear(barcodeText.getText());
				barcodeText.requestFocus();
			} else {				
				TextKeyListener.clear(txtSearch.getText());
				TextKeyListener.clear(barcodeText.getText());
				barcodeText.requestFocus();
				Toast.makeText(con, "Artículo no encontrado.", Toast.LENGTH_SHORT).show();
			}
		}
		
	}
	
	private class TokenGetListener implements WSResponseListener {
		
		private Context con;
		
		public TokenGetListener(Context _con) {
			con = _con;
		}

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			Toast.makeText(con, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			int idError = response.optInt("idError", -10000);
			String mensajeError = response.optString("mensajeError", ErrorUtil.connection());
			
			switch (idError) {
			case 0:
				
				if (request != null) {
					
					request.setToken(response.optString("token"));
					
					new WSClient(con)
					.hasLoader(true)
					.setCancellable(false)
					.setCifrado(Cifrados.SENSITIVE)
					.setListener(new PagoPostListener(con))
					.setUrl(Url.PAGO_POST)
					.execute(request.toString());
					
				}
				
				break;

			default:
				Toast.makeText(con, mensajeError, Toast.LENGTH_SHORT).show();
				break;
			}
		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			Toast.makeText(con, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			Toast.makeText(con, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}
		
		
	}
	
	private class PagoPostListener implements WSResponseListener {
		
		private Context con;
		
		public PagoPostListener(Context _con) {
			// TODO Auto-generated constructor stub
			con = _con;
		}

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			Toast.makeText(con, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			
			Result res = new Result(response);
			buildConfirmationView(res, con);
		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			Toast.makeText(con, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			Toast.makeText(con, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}
		
		
		
	}
	
	
	/*
	 * INTERACCIÓN CON IMPRESORA
	 */
	public void setBluetooth(Context _con, Result _r , boolean rompeFilas) {

		BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		
		if (bluetoothAdapter == null) {
			Toast.makeText(_con, "Bluetooth no disponible en este dispositivo", Toast.LENGTH_SHORT).show();
		} else {
			
			LogUtil.text("setBluetooth", "Si hay adaptador.");
			
			if (!bluetoothAdapter.isEnabled()) {
				LogUtil.text("setBluetooth", "Bluetooth no est� activo. Activando...");
				
				Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enableBluetooth, REQUEST_ENABLE_BT);

				LogUtil.text("setBluetooth", "Bluetooth activado.");
			}
			
			Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
			
			BluetoothDevice device = null;
			
			if (0 < pairedDevices.size()) {
				for (BluetoothDevice bluetoothDevice : pairedDevices) {
					LogUtil.text("MainActivity", "Device: " + bluetoothDevice.getName());
//					if (bluetoothDevice.getName().contains("Star")||bluetoothDevice.getName().contains("STAR")||bluetoothDevice.getName().contains("star")) {
					if (bluetoothDevice.getName().contains("XXXX")||bluetoothDevice.getName().contains("xxxx")) {
						LogUtil.text("MainActivity", "Star Micronics encontrado.");
						device = bluetoothDevice;
						LogUtil.text("MainActivity", "Star Micronics asignado.");
					}
					
				}
				
				if(null != device) {
					try {
		                  // Instantiate connection for given Bluetooth® MAC Address.
						LogUtil.text(TAG, device.getAddress());
		                  Connection thePrinterConn = ConnectionFactory.get(device.getAddress());
		                  
		                  // Open the connection - physical connection is established here.
		                  thePrinterConn.open();
		     
		                  // This example prints "This is a ZPL test." near the top of the label.
		                  String zplData = _r.buildArticleListString();
		     
		                  // Send the data to printer as a byte array.
		                  thePrinterConn.write(zplData.getBytes());
		     
		                  //Make sure the data got to the printer before closing the connection
		                  Thread.sleep(500);
		     
		                  // Close the connection to release resources.
		                  thePrinterConn.close();
		     
		              } catch (Exception e) {
		                  // Handle communications error here.
		                  e.printStackTrace();
		              }
				} else {
					LogUtil.text("setBluetooth", "No se cargó dispositivo BT.");
				}
			} else {

				LogUtil.text("setBluetooth", "No están dispositivos BT pareados.");
			}
		}
	}
	
	private class OnReceiptPrintListener implements PrintActionListener {
		private Context con;
		
		public OnReceiptPrintListener(Context con) {
			// TODO Auto-generated constructor stub
			this.con = con;
		}

		@Override
		public boolean onSuccess() {
			// TODO Auto-generated method stub
			Toast.makeText(con, "Impresión exitosa.", Toast.LENGTH_SHORT).show();
			impresionExitosa = true;
			return impresionExitosa;
		}

		@Override
		public boolean onError() {
			// TODO Auto-generated method stub
			Toast.makeText(con, "Error en impresión.", Toast.LENGTH_SHORT).show();
			impresionExitosa = false;
			return impresionExitosa;
			
		}
		
	}
	
	/**
	 * 
	 * @author carlosgs
	 *	FTP
	 */
	
	private class FileSendListener implements FTPResponseListener {
		
		private String consecutivo;
		private String barcode;
		private String file;
		
		public FileSendListener(String _consecutivo, String _barcode) {
			consecutivo = _consecutivo;
			barcode = _barcode;
		}
		
		public FileSendListener(String _consecutivo, String _barcode, String _file) {
			this(_consecutivo, _barcode);
			this.file = _file;
		}

		@Override
		public void onSent() {
			// TODO Auto-generated method stub
			Ticket t = new Ticket().setBarcode(barcode)
									.setConsecutivo(consecutivo).setFecha(Calendar.getInstance().getTime())
									.setFileString("")
									.setProcesado(true);
			
			new InsertTicketTask().execute(t);
		}

		@Override
		public void onError() {
			// TODO Auto-generated method stub

			Ticket t = new Ticket().setBarcode(barcode).setConsecutivo(consecutivo).setFecha(Calendar.getInstance().getTime())
									.setFileString(file)
									.setProcesado(false);
			
			Toast.makeText(con, "La conexión con el controlador no ha sido exitosa. Repetir operación.", Toast.LENGTH_LONG).show();
		
			new InsertTicketTask().execute(t);
		}
		
	}
	

	/*
	 * INTERACCI�N CON LECTOR DE TARJETA
	 */

	private class HeadsetReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub

			boolean headsetAvailable = intent.getIntExtra("state", 0) == 1;
			boolean microphoneAvailable = intent.getIntExtra("microphone", 0) == 1;
			boolean isConnected = headsetAvailable && microphoneAvailable;

			updateReaderState(isConnected);
		}
	}

	private void updateReaderState(boolean connected) {
		// TODO Auto-generated method stub

		lectorConectado = connected;

		if (connected) {
			LogUtil.text("Cobro", "Dispositivo Conectado");
		} else {
			LogUtil.text("Cobro", "Dispositivo no Conectado");
		}
	}

	private String getNumber(FinancialCard _card) {

		String track2 = null;
		String num = null;
		int encryptionType = (_card.data[0] >>> 3);
		// Trim extract encrypted block.
		byte[] encryptedData = new byte[_card.data.length - 1];
		System.arraycopy(_card.data, 1, encryptedData, 0, encryptedData.length);

		if (encryptionType == AudioReader.ENCRYPTION_TYPE_OLD_RSA
				|| encryptionType == AudioReader.ENCRYPTION_TYPE_RSA) {
			// a.log("� Decrypt card data\n", true);
			try {
				String[] result = AudioReaderHelper
						.decryptTrackDataRSA(encryptedData);
				// a.log("  Track2: " + result[0] + "\n");
				// almacenar
				track2 = result[0];
			} catch (Exception e) {
				e.printStackTrace();
				// a.logWarning("  Failed to decrypt data: " + e.getMessage() +
				// "\n");
			}
		} else if (encryptionType == AudioReader.ENCRYPTION_TYPE_AES256) {
			try {
				String[] result = AudioReaderHelper
						.decryptAESBlock(encryptedData);

				// a.log("  Random data: " + result[0] + "\n");
				// a.log("  Serial number: " + result[1] + "\n");
				if (result[2] != null) {
					// a.log("  Track1: " + result[2] + "\n");
				}
				if (result[3] != null) {
					// a.log("  Track2: " + result[3] + "\n");
					track2 = result[3];
				}
				if (result[4] != null) {
					// a.log("  Track3: " + result[4] + "\n");
				}
			} catch (Exception e) {
				e.printStackTrace();
				// a.logWarning("  Failed to decrypt data: " + e.getMessage() +
				// "\n");
			}
		} else if (encryptionType == AudioReader.ENCRYPTION_TYPE_IDTECH) {
			try {
				String[] result = AudioReaderHelper
						.decryptIDTECHBlock(encryptedData);

				// a.log("  Card type: " + result[0] + "\n");
				if (result[1] != null) {
					// a.log("  Track1: " + result[1] + "\n");
				}
				if (result[2] != null) {
					// a.log("  Track2: " + result[2] + "\n");
					track2 = result[2];
				}
				if (result[3] != null) {
					// a.log("  Track3: " + result[3] + "\n");
				}
			} catch (Exception e) {
				e.printStackTrace();
				// a.logWarning("  Failed to decrypt data: " + e.getMessage() +
				// "\n");
			}
		} else {
			// a.log("  Encrypted block: " +
			// HexUtil.byteArrayToHexString(fc.data) + "\n");
		}
		
		try {
			num = track2.substring(track2.indexOf(";") + 1, track2.indexOf("="));
			return num;
		} catch (NullPointerException e) {
			Log.e("MainActivity", "getNumber", e);
			return num;
		}

	}

	private void paintCardData(Context _con) {
		// TODO Auto-generated method stub

		if (card != null && !TextUtils.isEmpty(card.number)) {

			String cardStr = card.number;
			String holderStr = TextUtils.isEmpty(card.holder) ? "" : card.holder;

			tarjetaText.setText(cardStr);
			nombreText.setText(holderStr);

			mmText.setText(String.valueOf(card.month));
			aaText.setText(String.valueOf(card.year));
			
		} else {
			Toast.makeText(_con, "No se pudieron obtener datos de tarjeta",Toast.LENGTH_LONG).show();
		}

	}

	private abstract class MyAudioReaderTask extends AudioReaderTask {

		private Context con;

		public MyAudioReaderTask(Context _con, boolean isCancelable) {
			super(_con, isCancelable);
			con = _con;
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onFinish(long ellapse) {
			// TODO Auto-generated method stub
			if (card != null) {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						paintCardData(con);
					}
				});
			}
		}

		@Override
		public void onError(Exception e) {
			// TODO Auto-generated method stub
			LogUtil.error("DiestelPagoActivity", "MyAudioReaderTask", e);
			final String error = TextUtils.isEmpty(e.getMessage()) ? "No se pudieron obtener datos de tarjeta"
					: e.getMessage();

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					Toast.makeText(con, error, Toast.LENGTH_SHORT).show();
				}
			});
		}

	}

}
