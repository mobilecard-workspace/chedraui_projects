package org.addcel.android.chedrauipos.activity;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.constant.Rutas;
import org.addcel.android.chedrauipos.dialog.Dialogs;
import org.addcel.android.chedrauipos.insert.DataInsertCallback;
import org.addcel.android.chedrauipos.insert.DataInsertService;
import org.addcel.android.chedrauipos.insert.MassiveDataInsertServiceImpl;
import org.addcel.android.chedrauipos.insert.MockDataInsertServiceImpl;
import org.addcel.android.chedrauipos.masiva.CargaMasivaSchedulingService;
import org.addcel.android.chedrauipos.periodica.CargaPeriodicaAlarmReceiver;
import org.addcel.android.chedrauipos.service.SessionTimerBroadcastService;
import org.addcel.android.chedrauipos.session.UserSession;
import org.addcel.android.chedrauipos.sql.ArticuloDataSource;
import org.addcel.android.chedrauipos.util.ChedrauiTextUtil;
import org.addcel.android.chedrauipos.util.FileUtils;
import org.addcel.android.chedrauipos.util.HostValidator;
import org.addcel.android.download.DownloadCallback;
import org.addcel.android.download.DownloadService;
import org.addcel.android.download.DownloadServiceImpl;
import org.addcel.android.ftp.FileTransferClient;
import org.addcel.android.ftp.FileTransferClientFactory;
import org.addcel.android.ftp.FileTransferClientFactory.TipoCanal;
import org.addcel.android.session.SessionManager;
import org.addcel.android.util.ErrorUtil;
import org.addcel.android.util.LogUtil;
import org.addcel.android.util.NetworkUtil;
import org.addcel.android.util.Text;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class MenuActivity extends Activity implements DownloadCallback,
	DataInsertCallback{
	
	@InjectView(R.id.view_ultima) TextView ultimaView;
	
	private FileTransferClient client = FileTransferClientFactory.get(TipoCanal.SFTP,
			"50.57.192.214", "rmuniz", "xZZWj4GFzTZa");
	
	private DownloadService dService;
	private ArticuloDataSource dataSource;
	private String downloadedFileName;
	
	private DataInsertService insertService;
	
	private HostValidator validator;
	private Context con = MenuActivity.this;
	private SessionManager preferences;
	
	private ProgressDialog pDialog;
	
	private static final String TAG = MenuActivity.class.getName();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		ButterKnife.inject(this);
		
		getActionBar().setBackgroundDrawable(
				new ColorDrawable(getResources().getColor(R.color.ch_orange)));
		
		dataSource = new ArticuloDataSource(con);
		preferences = new SessionManager(con);
		Date d = preferences.getFechaActualizacion();
		
		if (d != null) {
			LogUtil.text(TAG, d.toString());
			LogUtil.text(TAG, "Lanzando matchTimeStamp para fecha: ");
			LogUtil.text(TAG, d.toString());
			
			if (matchTimeStamp(d)) {
				LogUtil.text(TAG, "Match en fechas");
				preferences.setUpdated(true);
				preferences.setFechaActualizacion(Text.formatDateChedraui(d));
				ultimaView.setText(d.toString());
			} else {
				LogUtil.text(TAG, "No hay match en fechas");
				preferences.setUpdated(false);
			}
		}
	}
	
	private BroadcastReceiver br = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			boolean logout = intent.getBooleanExtra("logout", false);
			if(logout) {
				UserSession.getInstance().logout();
				Toast.makeText(con, "Se ha vencido sesión de usuario.", 
						Toast.LENGTH_SHORT).show();
				invalidateOptionsMenu();
			} else {
				try {
					startService(new Intent(MenuActivity.this,
							SessionTimerBroadcastService.class));
				} catch (Exception e) {
					LogUtil.error(TAG, "Receiver timer", e);
				}
			}
			
		}
	};
	
	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		invalidateOptionsMenu();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		setFechaDeActualizacion();
		dService = new DownloadServiceImpl(client);
		registerReceiver(br, 
				new IntentFilter(SessionTimerBroadcastService.TIMER_BR));
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_inicio, menu);
		
		if (UserSession.getInstance().isLoggedIn()) {
			MenuItem it = menu.findItem(R.id.a_notlogged);
			it.setVisible(false);
			MenuItem logged = menu.findItem(R.id.a_logged);
			Menu m = logged.getSubMenu();
			if (UserSession.getInstance().getUsuario().getRole().getIdRol() == 102) {
				m.findItem(R.id.a_registro).setVisible(false);
				m.findItem(R.id.a_password).setVisible(false);
				m.findItem(R.id.a_datos).setVisible(false);
				m.findItem(R.id.a_settings).setVisible(false);
			}
			
		} else {
			MenuItem it = menu.findItem(R.id.a_logged);
			it.setVisible(false);			
		}
		
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		int id = item.getItemId();
		
		switch (id) {
		case R.id.a_login:
			if (UserSession.getInstance().isLoggedIn()) {
				SessionTimerBroadcastService.setFlagNoCerrarSesion();
			}
			Dialogs.loginDialog(con, null, false);
			invalidateOptionsMenu();
			break;
		case R.id.a_registro:
			if (UserSession.getInstance().isLoggedIn()) {
				SessionTimerBroadcastService.setFlagNoCerrarSesion();
			}
			startActivity(new Intent(con, RegistroActivity.class));
			break;
		case R.id.a_logout:
			UserSession.getInstance().logout();
			SessionTimerBroadcastService.setFlagCerrarSesion();
			invalidateOptionsMenu();
			break;
		case R.id.a_datos:
			if (UserSession.getInstance().isLoggedIn()) {
				SessionTimerBroadcastService.setFlagNoCerrarSesion();
			}
			startActivity(new Intent(con, DatosActivity.class));
			break;
		case R.id.a_password:
			if (UserSession.getInstance().isLoggedIn()) {
				SessionTimerBroadcastService.setFlagNoCerrarSesion();
			}
			Dialogs.passwordDialog(con);
			break;
		case R.id.a_consultar:
			if (UserSession.getInstance().isLoggedIn()) {
				SessionTimerBroadcastService.setFlagNoCerrarSesion();
			}
			startActivity(new Intent(con, ConsultaActivity.class));
			break;
		case R.id.a_tickets:
			if (UserSession.getInstance().isLoggedIn()) {
				SessionTimerBroadcastService.setFlagNoCerrarSesion();
			}
			startActivity(new Intent(con, TicketActivity.class));
			break;
			
		case R.id.a_settings:
			if (UserSession.getInstance().isLoggedIn()) {
				SessionTimerBroadcastService.setFlagNoCerrarSesion();
			}
			startActivity(new Intent(Settings.ACTION_SETTINGS));
			break;
		default:
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		if (UserSession.getInstance().isLoggedIn()) {
			menu.getItem(0).setVisible(false);
		} else {
			menu.getItem(1).setVisible(false);
		}
		
		return super.onPrepareOptionsMenu(menu);
	}
	
	@OnClick(R.id.btn_articulos)
	void updateArticulos() {
		SessionTimerBroadcastService.setFlagNoCerrarSesion();
		if (UserSession.getInstance().getTienda() != null) {
			if (!preferences.getUpdated())
				downloadFile();
			else
				Toast.makeText(con, "El masivo se encuentra actualizado.",
						Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(con, "Da de alta una tienda para actualizar el"
					+ " catálogo.", Toast.LENGTH_SHORT).show();
		}
	}

	@OnClick(R.id.btn_venta)
	void goToMain() {
		SessionTimerBroadcastService.setFlagNoCerrarSesion();
		if (preferences.getUpdated()) {
			if (!CargaMasivaSchedulingService.isRunning) {
				if (UserSession.getInstance().isLoggedIn()) {
					validator = new HostValidator("50.57.192.214", "8080");
					new HostAvailableTask().execute(validator); 
				} else
					Toast.makeText(con, ErrorUtil.notLogged("módulo"),
							Toast.LENGTH_SHORT).show();
			} else
				Toast.makeText(con, "Se está llevando a cabo la "
						+ "actualización masiva de artículos.", 
						Toast.LENGTH_SHORT).show();
		} else
			Toast.makeText(con, "Actualice el masivo de artículos.", 
					Toast.LENGTH_SHORT).show();
	}
	 
	  @Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		  if (UserSession.getInstance().isLoggedIn() &&  
				  UserSession.getInstance().getUsuario().getRole().getIdRol() == 101) {
			  finish();
		  } else {
			  LogUtil.text(TAG, "La aplicación nunca se cierra.");
		  }
	}
	
	void downloadFile() {
		
		if (NetworkUtil.isOnline(con)) {
			LogUtil.text(TAG, "Lanzando Cliente para obtener archivo SFTP.");
			
			downloadedFileName = ChedrauiTextUtil.buildStockFileName(
					UserSession.getInstance().getUsuario().getTienda().getCodigo(),
					"MAS");
			LogUtil.text(TAG, "Archivo a descargar: " + downloadedFileName);
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			
			new AsyncTask<Void, Void, Integer>() {
				
				@Override
				protected void onPreExecute() {
					// TODO Auto-generated method stub
					if (pDialog == null || !pDialog.isShowing()) {
						pDialog = ProgressDialog.show(con, "Actualización. Fase 1.",
								"Descargando masivo de artículos.", true, false);
					}
				}

				@Override
				protected Integer doInBackground(Void... params) {
					// TODO Auto-generated method stub
					return dService.run(getFilesDir() + "/" + downloadedFileName, 
							Rutas.DESCARGA);
				}
				
				@Override
				protected void onPostExecute(Integer result) {
					// TODO Auto-generated method stub
					if (pDialog.isShowing())
						pDialog.dismiss();
					
					onDownloadFinished(result);
				}
			}.execute();
		
		} else {
			Toast.makeText(con, "No se ha lanzado proceso de actualización de inventario. "
								+ "Verificar conexión a internet.", Toast.LENGTH_SHORT).show();
		}
	}
	
	void insertData() {
		
		try {
			final InputStream stream = FileUtils.open(getFilesDir() + "/" + downloadedFileName);
			
			if (TextUtils.equals("elmanss",UserSession.getInstance().getUsuario().getLogin()))
				insertService = new MockDataInsertServiceImpl(dataSource);
			else
				insertService = new MassiveDataInsertServiceImpl(dataSource);
			
			new AsyncTask<Void, Void, Boolean>() {
				
				@Override
				protected void onPreExecute() {
					// TODO Auto-generated method stub
					if (pDialog == null || !pDialog.isShowing()) {
						pDialog = ProgressDialog.show(con, "Actualización. Fase 2.",
								"Insertando articulos actualizados..", true, false);
					}
				}

				@Override
				protected Boolean doInBackground(Void... params) {
					// TODO Auto-generated method stub
					return insertService.run(stream);
				}
				
				@Override
				protected void onPostExecute(Boolean result) {
					// TODO Auto-generated method stub
					if (pDialog.isShowing())
						pDialog.dismiss();
					
					onDataInsertFinished(result);
				}
			}.execute();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			LogUtil.error(TAG, "insertData", e);
			Toast.makeText(con, R.string.txt_menu_error, Toast.LENGTH_SHORT).show();
		}
	}
	
	private void setFechaDeActualizacion() {
		if ( preferences.getFechaActualizacion() == null) {
			ultimaView.setText("Sin actualizar");
		} else {
			ultimaView.setText(preferences.getFechaActualizacion().toString());
		}
	}
	

	private void setStateAfterDataInsert() {
	    final Date terminaActualizacion = Calendar.getInstance().getTime();
	    
	    LogUtil.text(TAG, "Termina actualización en: " + terminaActualizacion.toString());
	    CargaPeriodicaAlarmReceiver pAlarm = new CargaPeriodicaAlarmReceiver();
	    preferences.setFechaActualizacion(new SimpleDateFormat("yyyyMMdd", Locale.US).
	    		format(terminaActualizacion));
	    preferences.setUpdated(true);
	    pAlarm.setAlarm(MenuActivity.this);
	    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		ultimaView.setText(terminaActualizacion.toString());
	}
	 
	  @Override 
	  protected void onPause() { 
		  
	//	    unregisterReceiver(br);
	    super.onPause(); 
	  } 
	  
	  @Override
	  protected void onStop() {
		// TODO Auto-generated method stub
		dataSource.close();
		try { 
		    unregisterReceiver(br);
		} catch (Exception e) {
		    // Receiver was probably already stopped in onPause() 
		} 
		super.onStop();
	  }
	  
	  @Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		  stopService(new Intent(this, SessionTimerBroadcastService.class));
		  LogUtil.text(TAG, "Stopped service");
		super.onDestroy();
	}

	@Override
	public void onDownloadFinished(int result) {
		// TODO Auto-generated method stub
		switch (result) {
		case 0:
			insertData();
			break;

		default:
			Toast.makeText(con, R.string.txt_menu_errordescarga, Toast.LENGTH_LONG).show();
			break;
		}
	}

	@Override
	public void onDataInsertFinished(boolean result) {
		// TODO Auto-generated method stub
		
		if (result) {
			setStateAfterDataInsert();
			Toast.makeText(con, R.string.txt_menu_exito, Toast.LENGTH_LONG).show();
		} else {
			Toast.makeText(con, R.string.txt_menu_error, Toast.LENGTH_LONG).show();
		}
	}
	
	public boolean matchTimeStamp(Date fechaActualizacion) {
		LogUtil.text(TAG, "Entrando matchTimeStamp");
		String fHoyStr = Text.formatDateChedraui(Calendar.getInstance().getTime());
		LogUtil.text(TAG, fHoyStr);
		String fActStr = Text.formatDateChedraui(fechaActualizacion);
		LogUtil.text(TAG, fActStr);
		
		if(TextUtils.equals(fActStr, fHoyStr)) {
			return true;
		} else {
			return false;
		}
	}
	
	private class HostAvailableTask extends AsyncTask<HostValidator, Void, Boolean> {

		@Override
		protected Boolean doInBackground(HostValidator... params) {
			// TODO Auto-generated method stub
			if (params != null && params.length > 0) {
				return params[0].isHostAvailable();
			} else{
				return false;
			}
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			if (result) {
				startActivity(new Intent(con, MainActivity.class));
			} else {
				Toast.makeText(con, "El controlador Chedraui "
						+ "no se encuentra activo.", Toast.LENGTH_SHORT).show();
			}
		}
		
	}
}
