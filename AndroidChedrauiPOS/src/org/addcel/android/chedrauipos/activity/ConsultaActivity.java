package org.addcel.android.chedrauipos.activity;

import java.util.ArrayList;
import java.util.List;

import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.adapter.PagoAdapter;
import org.addcel.android.chedrauipos.constant.Url;
import org.addcel.android.chedrauipos.to.Pago;
import org.addcel.android.chedrauipos.to.Result;
import org.addcel.android.crypto.Cifrados;
import org.addcel.android.interfaces.JSONable;
import org.addcel.android.util.ErrorUtil;
import org.addcel.android.ws.client.WSClient;
import org.addcel.android.ws.listener.WSResponseListener;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class ConsultaActivity extends Activity implements OnClickListener {
	
	FrameLayout frame;
	ListView pagosList;
	List<Pago> pagos;
	PagoAdapter adapter;
	private PopupWindow auxWindow;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_consulta);
		
		getActionBar().setBackgroundDrawable(
				new ColorDrawable(getResources().getColor(R.color.ch_orange)));
		
		frame = (FrameLayout) findViewById(R.id.frame);
		
		(findViewById(R.id.btn_hoy)).setOnClickListener(this);
		(findViewById(R.id.btn_semana)).setOnClickListener(this);
		(findViewById(R.id.btn_mes)).setOnClickListener(this);
		(findViewById(R.id.btn_anterior)).setOnClickListener(this);
		
		pagosList = (ListView) findViewById(R.id.list_pagos);
		
		pagos = new ArrayList<Pago>();
		adapter = new PagoAdapter(ConsultaActivity.this, pagos);
		pagosList.setAdapter(adapter);
		pagosList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				PagoAdapter ad = (PagoAdapter) parent.getAdapter();
				Pago p = (Pago) ad.getItem(position);
				
				if (p != null) {
					buildConfirmationView(p, ConsultaActivity.this);
				}
			}
		});
	}
	
	private void buildConfirmationView(final Pago r, Context con) {
		
		LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(R.layout.view_recibo, null);

		auxWindow = new PopupWindow(popupView, 768, 450, true);
		final Context newCon = popupView.getContext();

		ImageButton btnDismiss = (ImageButton) popupView.findViewById(R.id.btn_cerrar);
		Button btnPrint = (Button) popupView.findViewById(R.id.btn_print);

		TextView titleView = (TextView) popupView.findViewById(R.id.view_title);
		titleView.setText("Venta: #" + r.getReferenciaMC());
		
		TextView contentView = (TextView) popupView.findViewById(R.id.view_content);
		contentView.setText(r.buildReceipt());

		btnDismiss.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				
				auxWindow.dismiss();
				frame.setForeground(getResources().getDrawable(
						android.R.color.transparent));
				
			}
		});
		
		btnPrint.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				new WSClient(newCon)
				.hasLoader(true)
				.setCancellable(false)
				.setCifrado(Cifrados.HARD)
				.setListener(new CorreoPostListener())
				.setUrl(Url.MAIL_POST)
				.execute(r.writeToJSON().toString());
			}
		});

		auxWindow.showAtLocation(frame, Gravity.CENTER, 0, 0);
		frame.setForeground(getResources().getDrawable(R.color.foreground_dark));

			
			
		
	}

	private void getPagos(int _criterio) {
		// TODO Auto-generated method stub
		ConsultaReq param = new ConsultaReq().setIdUsuario(1000).setTipoBusqueda(_criterio);
		
		new WSClient(ConsultaActivity.this)
		.hasLoader(true)
		.setCancellable(false)
		.setCifrado(Cifrados.HARD)
		.setListener(new PagosGetListener())
		.setUrl(Url.PAGOS_GET)
		.execute(param.toString());
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		if (v != null) {

			switch (v.getId()) {
			case R.id.btn_hoy:
				getPagos(1);
				break;
			case R.id.btn_semana:
				getPagos(2);
				break;
			case R.id.btn_mes:
				getPagos(3);
				break;
			case R.id.btn_anterior:
				getPagos(4);
				break;

			default:
				break;
			}

		}

	}
	
	private class ConsultaReq implements JSONable {
		
		private int tipoBusqueda;
		private long idUsuario;
		
		public ConsultaReq() {
			// TODO Auto-generated constructor stub
		}
		
		public ConsultaReq setTipoBusqueda(int tipoBusqueda) {
			this.tipoBusqueda = tipoBusqueda;
			return this;
		}
		
		public ConsultaReq setIdUsuario(long idUsuario) {
			this.idUsuario = idUsuario;
			return this;
		}

		@Override
		public JSONObject writeToJSON() {
			// TODO Auto-generated method stub
			
			try {
				
				return new JSONObject().put("idUsuario", idUsuario).put("tipoBusqueda", tipoBusqueda);
				
			} catch (JSONException e) {
			
				return null;
			}
		}

		@Override
		public void readFromJSON(JSONObject json) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return writeToJSON().toString();
		}
		
	}
	
	private class PagosGetListener implements WSResponseListener {
		
		private static final String TAG = "PagosGetListener";

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			if (!pagos.isEmpty()) {
				pagos.clear();
				adapter.notifyDataSetChanged();
			}
			
			Toast.makeText(ConsultaActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			int idError = response.optInt("idError", -10000);
			String mensajeError = response.optString("mensajeError", ErrorUtil.connection());
			
			switch (idError) {
			case 0:
				
				JSONArray array = response.optJSONArray("pagos");
				
				if (array != null && array.length() > 0) {
					
					if (!pagos.isEmpty())
						pagos.clear();
					
					for (int i = 0; i < array.length(); i++) {
						JSONObject json = array.optJSONObject(i);
						pagos.add(new Pago(json));
						adapter.notifyDataSetChanged();
					}
					
				} else {
					if (!pagos.isEmpty()) {
						pagos.clear();
						adapter.notifyDataSetChanged();
					}
					
					Toast.makeText(ConsultaActivity.this, "No existen pagos en el periodo especificado.", Toast.LENGTH_SHORT).show();
				}
				
				
				break;
			
			default:
				
				if (!pagos.isEmpty()) {
					pagos.clear();
					adapter.notifyDataSetChanged();
				}
				
				Toast.makeText(ConsultaActivity.this, mensajeError, Toast.LENGTH_SHORT).show();
				break;
			}
		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			if (!pagos.isEmpty()) {
				pagos.clear();
				adapter.notifyDataSetChanged();
			}
			
			Toast.makeText(ConsultaActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			if (!pagos.isEmpty()) {
				pagos.clear();
				adapter.notifyDataSetChanged();
			}
			
			Toast.makeText(ConsultaActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}
		
		
	}
	
	private class CorreoPostListener implements WSResponseListener {

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			Toast.makeText(ConsultaActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			Toast.makeText(ConsultaActivity.this, response.optString("mensajeError", ErrorUtil.connection()), Toast.LENGTH_SHORT).show();
			
		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			Toast.makeText(ConsultaActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			Toast.makeText(ConsultaActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}
		
		
		
	}

}
