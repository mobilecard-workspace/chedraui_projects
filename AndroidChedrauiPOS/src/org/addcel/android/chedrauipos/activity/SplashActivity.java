 package org.addcel.android.chedrauipos.activity;

import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.masiva.CargaMasivaAlarmReceiver;
import org.addcel.android.enums.AppContext;
import org.addcel.android.session.SessionManager;
import org.addcel.android.util.LogUtil;
import org.addcel.android.util.NetworkUtil;
import org.addcel.android.version.UrlVersion;
import org.addcel.android.version.VersionTO;
import org.addcel.android.version.VersionWSClient;
import org.addcel.android.version.VersionWSListener;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.view.Display;

public class SplashActivity extends Activity {

	private static final String TAG = SplashActivity.class.getName();
	private final Context con = SplashActivity.this;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
		SessionManager session = new SessionManager(con);
		DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
		 
        float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        
        LogUtil.text("DENSIDAD", Float.toString(displayMetrics.density));
        LogUtil.text("ALTURA PANTALLA", Float.toString(dpHeight));
        LogUtil.text("ANCHURA PANTALLA", Float.toString(dpWidth));
		
        int consecutivo = session.getConsecutivo();
        if (consecutivo == 0) {
        	session.setConsecutivo(9001);
        }
        
		AppContext appCon = AppContext.getCurrent();
		
		switch (appCon) {
		case DEV:
			getVersion();
			break;

		default:
			new Handler().postDelayed(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					startActivity(new Intent(con, MenuActivity.class));
					finish();
				}
			}, 3000);
			break;
			
		}
		
	}
	
	private void getVersion() {
		if(NetworkUtil.isOnline(con)) {
			LogUtil.text(TAG, "HAY internet.");
			VersionTO params = new VersionTO("13");
			
			new VersionWSClient(
					new VersionWSListener(con, MenuActivity.class), UrlVersion.GET).execute(params);
		} else {
			LogUtil.text(TAG, "NO HAY internet.");
			
			try {
				Thread.sleep(5 * DateUtils.SECOND_IN_MILLIS);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			getVersion();
		}
	}
	
}
