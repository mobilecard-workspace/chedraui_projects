package org.addcel.android.chedrauipos.activity;

import java.util.ArrayList;
import java.util.List;

import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.adapter.TicketAdapter;
import org.addcel.android.chedrauipos.service.SessionTimerBroadcastService;
import org.addcel.android.chedrauipos.sql.TicketDataSource;
import org.addcel.android.chedrauipos.sql.enums.Operacion;
import org.addcel.android.chedrauipos.sql.listener.QueryListener;
import org.addcel.android.chedrauipos.sql.task.TicketTask;
import org.addcel.android.chedrauipos.to.Ticket;
import org.addcel.android.session.SessionManager;
import org.addcel.android.zxing.util.BarcodeUtil;

import com.google.zxing.BarcodeFormat;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class TicketActivity extends Activity {
	
	ListView ticketList;
	
	private TicketDataSource source;
	private TicketAdapter adapter;
	private SessionManager prefs;
	private List<Ticket> tickets;
	private Context con = TicketActivity.this;
	private static final String TAG = TicketActivity.class.getName();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tickets);
		getActionBar().setBackgroundDrawable(
				new ColorDrawable(getResources().getColor(R.color.ch_orange)));
		
		tickets = new ArrayList<Ticket>();
		source = new TicketDataSource(con);
		ticketList = (ListView) findViewById(R.id.list_tickets);
		adapter = new TicketAdapter(con, tickets);
		
		ticketList.setAdapter(adapter);
		ticketList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				SessionTimerBroadcastService.setFlagNoCerrarSesion();
				Ticket t = (Ticket) arg0.getAdapter().getItem(arg2);
				if (t.isProcesado()) {
					
					Bitmap b = BarcodeUtil.generateBarcode(t.getBarcode() , BarcodeFormat.EAN_13, 150);
					if (b != null) {
						((ImageView) findViewById(R.id.barcode)).setImageBitmap(b);
					}
					
					((TextView) findViewById(R.id.view_barcode)).setText(t.getBarcode());
					
				} else {
					Toast.makeText(con, "Este ticket no ha sido procesado.", Toast.LENGTH_SHORT).show();
				}
			}
			
			
		});
		
		new TicketTask()
			.setLoader(true)
			.setCancellable(false)
			.setCon(con)
			.setDataSource(source)
			.setListener(new TicketGetListener()).execute(Operacion.GET_ALL);
	}
	
	private class TicketGetListener implements QueryListener<Ticket> {
		
		private static final String TAG = "TicketGetListener";

		@Override
		public void onInsert(Ticket e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onGet(Ticket e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onGetAll(List<Ticket> _list) {
			// TODO Auto-generated method stub
			if (_list != null && !_list.isEmpty()) {
				tickets.addAll(_list);
				adapter.notifyDataSetChanged();
			} else {
				Toast.makeText(con, "No hay tickets procesados", Toast.LENGTH_LONG).show();
				finish();
			}
		}
		
	}
	

}
