package org.addcel.android.chedrauipos.activity;

import java.util.ArrayList;
import java.util.List;

import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.constant.Url;
import org.addcel.android.chedrauipos.session.UserSession;
import org.addcel.android.chedrauipos.to.Role;
import org.addcel.android.chedrauipos.to.Tienda;
import org.addcel.android.chedrauipos.to.Usuario;
import org.addcel.android.crypto.Cifrados;
import org.addcel.android.util.ErrorUtil;
import org.addcel.android.util.LogUtil;
import org.addcel.android.ws.client.WSClient;
import org.addcel.android.ws.listener.WSResponseListener;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

public class RegistroActivity extends Activity {
	
	private Context con = RegistroActivity.this;
	private List<Tienda> tiendas;
	private List<Role> roles;
	private ArrayAdapter<Tienda> tiendaAdapter;
	private ArrayAdapter<Role> roleAdapter;
	
	private EditText login, mail, nombre, apellido, materno;
	private Spinner tiendasSpinner, rolesSpinner;
	
	private String catalogRequest = "{\"idUsuario\":" + UserSession.getInstance().getUsuario().getIdUsuario() + "}";
	private static final String TAG = RegistroActivity.class.getSimpleName();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registro);
		
		getActionBar().setBackgroundDrawable(
				new ColorDrawable(getResources().getColor(R.color.ch_orange)));
		
		tiendas = new ArrayList<Tienda>();
		roles = new ArrayList<Role>();
		tiendaAdapter = new ArrayAdapter<Tienda>(con, android.R.layout.simple_spinner_item, tiendas);
		roleAdapter = new ArrayAdapter<Role>(con, android.R.layout.simple_spinner_item, roles);
		
		login = (EditText) findViewById(R.id.text_login);
		mail = (EditText) findViewById(R.id.text_mail);
		nombre = (EditText) findViewById(R.id.text_nombre);
		apellido = (EditText) findViewById(R.id.text_apellido);
		materno = (EditText) findViewById(R.id.text_materno);
		tiendasSpinner = (Spinner) findViewById(R.id.spinner_tienda);
		rolesSpinner = (Spinner) findViewById(R.id.spinner_rol);
		
		tiendasSpinner.setAdapter(tiendaAdapter);
		tiendasSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Tienda t = ((Tienda)(parent.getAdapter().getItem(position)));
				if (t != null) {
					LogUtil.text(TAG, "Tienda seleccionada: " + t.getCodigo() + "-" + t.getDescripcion());
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		rolesSpinner.setAdapter(roleAdapter);
		
		getTiendas();
		
		
	}
	
	private void getTiendas() {
		if (!tiendas.isEmpty()) {
			tiendaAdapter.notifyDataSetChanged();
			getRoles();
		} else {
			new WSClient(con)
			.hasLoader(true)
			.setCancellable(false)
			.setCifrado(Cifrados.HARD)
			.setListener(new TiendaGetListener())
			.setUrl(Url.TIENDA_GET).execute(catalogRequest);
		}
		
	}
	
	private void getRoles() {
		if (!roles.isEmpty()) {
			roleAdapter.notifyDataSetChanged();
		} else {
			new WSClient(con)
			.hasLoader(true)
			.setCancellable(false)
			.setCifrado(Cifrados.HARD)
			.setListener(new RoleGetListener())
			.setUrl(Url.ROLE_GET).execute(catalogRequest);
		}
	}
	
	private boolean validar() {
		if (TextUtils.isEmpty(login.getText().toString().trim())) {
			login.setError(ErrorUtil.isEmpty("login"));
			return false;
		} 
		
		if (TextUtils.isEmpty(mail.getText().toString().trim())) {
			mail.setError(ErrorUtil.isEmpty("correo electrónico"));
			return false;
		} 
		
		if (TextUtils.isEmpty(nombre.getText().toString().trim())) {
			nombre.setError(ErrorUtil.isEmpty("nombre"));
			return false;
		} 
		
		if (TextUtils.isEmpty(apellido.getText().toString().trim())) {
			apellido.setError(ErrorUtil.isEmpty("apellido paterno"));
			return false;
		}
		
		if (TextUtils.isEmpty(materno.getText().toString().trim())) {
			materno.setError(ErrorUtil.isEmpty("apellido materno"));
			return false;
		} 
		
		return true;
	}
	
	private void registrar() {
		
		String loginStr = login.getText().toString().trim();
		String mailStr = mail.getText().toString().trim();
		String nombreStr = nombre.getText().toString().trim();
		String apellidoStr = apellido.getText().toString().trim();
		String maternoStr = materno.getText().toString().trim();
		Role role = (Role) rolesSpinner.getSelectedItem();
		Tienda tienda = (Tienda) tiendasSpinner.getSelectedItem();
		
		Usuario usuario = new Usuario().setApellido(apellidoStr)
				.setIdSupervisor(UserSession.getInstance().getUsuario().getIdUsuario())
				.setLogin(loginStr)
				.setMail(mailStr)
				.setMaterno(maternoStr)
				.setNombre(nombreStr)
				.setRole(role)
				.setTienda(tienda);
		
		new WSClient(con).hasLoader(true)
							.setCancellable(false)
							.setCifrado(Cifrados.SENSITIVE)
							.setListener(new UserInsertListener())
							.setUrl(Url.USER_INSERT)
							.execute(usuario.toString());
	}

	public void execute(View v) {
		// TODO Auto-generated method stub
		
		if (validar()) {
			registrar();
		}
	}
	
	private class TiendaGetListener implements WSResponseListener {

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			Toast.makeText(con, "Error al obtener tiendas.", Toast.LENGTH_SHORT).show();
			getRoles();
		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			int idError = response.optInt("idError", -10000);
			String mensajeError = response.optString("mensajeError", "Error al obtener tiendas.");
			switch (idError) {
			case 0:
				JSONArray arr = response.optJSONArray("tiendas");
				for (int i = 0; i < arr.length(); i++) {
					tiendas.add(new Tienda(arr.optJSONObject(i)));
				}
				
				tiendaAdapter.notifyDataSetChanged();
				break;

			default:
				Toast.makeText(con, mensajeError, Toast.LENGTH_SHORT).show();
				break;
			}
			
			getRoles();
		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			Toast.makeText(con, "Error al obtener tiendas.", Toast.LENGTH_SHORT).show();
			getRoles();
		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			Toast.makeText(con, "Error al obtener tiendas.", Toast.LENGTH_SHORT).show();
			getRoles();
		}
		
	}
	
	private class RoleGetListener implements WSResponseListener {

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			Toast.makeText(con, "Error al obtener roles.", Toast.LENGTH_SHORT).show();
			
		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			int idError = response.optInt("idError", -10000);
			String mensajeError = response.optString("mensajeError", "Error al obtener roles.");
			switch (idError) {
			case 0:
			case 2:
				JSONArray arr = response.optJSONArray("roles");
				for (int i = 0; i < arr.length(); i++) {
					roles.add(new Role(arr.optJSONObject(i)));
				}
				
				roleAdapter.notifyDataSetChanged();
				break;

			default:
				Toast.makeText(con, mensajeError, Toast.LENGTH_SHORT).show();
				break;
			}
		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			Toast.makeText(con, "Error al obtener roles.", Toast.LENGTH_SHORT).show();
			
		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			Toast.makeText(con, "Error al obtener roles.", Toast.LENGTH_SHORT).show();
			
		}
		
	}
	
	private class UserInsertListener implements WSResponseListener {

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			Toast.makeText(con, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			int idError = response.optInt("idError", -10000);
			String mensajeError = response.optString("mensajeError", ErrorUtil.connection());
			switch (idError) {
			case 0:
				Toast.makeText(con, mensajeError, Toast.LENGTH_LONG).show();
				finish();
				break;

			default:
				Toast.makeText(con, mensajeError, Toast.LENGTH_SHORT).show();
				break;
			}
		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			Toast.makeText(con, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			Toast.makeText(con, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}
		
	}

}
