package org.addcel.android.chedrauipos.sql;

import org.addcel.android.chedrauipos.to.Articulo;
import org.addcel.android.util.LogUtil;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

public class ArticuloDataSource {

	private SQLiteDatabase db;
	private ChedrauiPOSSQLiteHelper chDbHelper;
	
	private String[] articuloColumnas = {
											ChedrauiPOSSQLiteHelper.COL_ID,
											ChedrauiPOSSQLiteHelper.COL_UPC,
											ChedrauiPOSSQLiteHelper.COL_TIENDA,
											ChedrauiPOSSQLiteHelper.COL_SKU,
											ChedrauiPOSSQLiteHelper.COL_UM,
											ChedrauiPOSSQLiteHelper.COL_DESCRIPCION,
											ChedrauiPOSSQLiteHelper.COL_PROV,
											ChedrauiPOSSQLiteHelper.COL_MARCA,
											ChedrauiPOSSQLiteHelper.COL_GRUPO,
											ChedrauiPOSSQLiteHelper.COL_FLAT,
											ChedrauiPOSSQLiteHelper.COL_PRECIO,
											ChedrauiPOSSQLiteHelper.COL_T_IVA,
											ChedrauiPOSSQLiteHelper.COL_T_IEPS ,
											ChedrauiPOSSQLiteHelper.COL_TP_PROMO,
											ChedrauiPOSSQLiteHelper.COL_DESCUENTO,
										};
	
	public ArticuloDataSource(Context _con) {
		chDbHelper = new ChedrauiPOSSQLiteHelper(_con);
	}
	
	public void open() throws SQLException {
		db = chDbHelper.getWritableDatabase();
	}
	
	public void close() {
		chDbHelper.close();
	}
	
	public Articulo createArticulo(String _linea) {
		String[] parsedLinea = _linea.split("\\|");
		
		String upc = TextUtils.isEmpty(parsedLinea[0])? "No Info" : parsedLinea[0];
		String tienda = TextUtils.isEmpty(parsedLinea[1])? "No Info" : parsedLinea[1];
		String articulo = TextUtils.isEmpty(parsedLinea[2])? "No Info" : parsedLinea[2];
		String um = TextUtils.isEmpty(parsedLinea[3])? "No Info" : parsedLinea[3];
		String descripcion = TextUtils.isEmpty(parsedLinea[4])? "No Info" : parsedLinea[4];
		String proveedor = TextUtils.isEmpty(parsedLinea[5])? "No Info" : parsedLinea[5];
		String marca = TextUtils.isEmpty(parsedLinea[6])? "No Info" : parsedLinea[6];
		String departamento = TextUtils.isEmpty(parsedLinea[7])? "No Info" : parsedLinea[7];
		String pesable = TextUtils.isEmpty(parsedLinea[8])? "No Info" : parsedLinea[8];
		String precio = TextUtils.isEmpty(parsedLinea[9])? "No Info" : parsedLinea[9];
		String iva = TextUtils.isEmpty(parsedLinea[10])? "No Info" : parsedLinea[10];
		String ieps = TextUtils.isEmpty(parsedLinea[11])? "No Info" : parsedLinea[11];
		String promo = TextUtils.isEmpty(parsedLinea[12])? "No Info" : parsedLinea[12];
		String descuento = TextUtils.isEmpty(parsedLinea[13])? "No Info" : parsedLinea[13];
		
		StringBuilder sb = new StringBuilder();
		sb.append("upc:").append(" ").append(upc)
		.append(", ").append("tienda:").append(" ").append(tienda)
		.append(", ").append("articulo:").append(" ").append(articulo)
		.append(", ").append("um:").append(" ").append(um)
		.append(", ").append("descripcion:").append(" ").append(descripcion)
		.append(", ").append("proveedor:").append(" ").append(proveedor)
		.append(", ").append("marca:").append(" ").append(marca)
		.append(", ").append("departamento:").append(" ").append(departamento)
		.append(", ").append("pesable:").append(" ").append(pesable)
		.append(", ").append("precio:").append(" ").append(precio)
		.append(", ").append("iva:").append(" ").append(iva)
		.append(", ").append("ieps:").append(" ").append(ieps)
		.append(", ").append("promo:").append(" ").append(promo)
		.append(", ").append("descuento:").append(" ").append(descuento);
		
		LogUtil.text("createArticulo", sb.toString());
		
		ContentValues values = new ContentValues();
		values.put(ChedrauiPOSSQLiteHelper.COL_UPC, upc);
		values.put(ChedrauiPOSSQLiteHelper.COL_TIENDA, tienda);
		values.put(ChedrauiPOSSQLiteHelper.COL_SKU, articulo);
		values.put(ChedrauiPOSSQLiteHelper.COL_UM, um);
		values.put(ChedrauiPOSSQLiteHelper.COL_DESCRIPCION, descripcion);
		values.put(ChedrauiPOSSQLiteHelper.COL_PROV, proveedor);
		values.put(ChedrauiPOSSQLiteHelper.COL_MARCA, marca);
		values.put(ChedrauiPOSSQLiteHelper.COL_GRUPO, departamento);
		values.put(ChedrauiPOSSQLiteHelper.COL_FLAT, pesable);
		values.put(ChedrauiPOSSQLiteHelper.COL_PRECIO, precio);
		values.put(ChedrauiPOSSQLiteHelper.COL_T_IVA, iva);
		values.put(ChedrauiPOSSQLiteHelper.COL_T_IEPS, ieps);
		values.put(ChedrauiPOSSQLiteHelper.COL_TP_PROMO, promo);
		values.put(ChedrauiPOSSQLiteHelper.COL_DESCUENTO, descuento);
		
		long insertId = db.insert(ChedrauiPOSSQLiteHelper.T_ARTICULO, null, 
				values);
		
		Cursor cursor =  db.query(ChedrauiPOSSQLiteHelper.T_ARTICULO,
		        articuloColumnas, ChedrauiPOSSQLiteHelper.COL_ID + " = " + insertId, null,
		        null, null, null); 
		
		cursor.moveToFirst();
		Articulo nuevoArticulo = cursorToArticulo(cursor);
		LogUtil.text("createArticulo", nuevoArticulo.toString());
		cursor.close();
		return nuevoArticulo;
	}
	
	public void updateArticulo(String _linea) {
		String[] parsedLinea = _linea.split("\\|");
		
		String upc = parsedLinea[0];
		String precio = parsedLinea[9];
		
		Articulo a = selectArticulo(upc);
		
		if (a == null) {
			createArticulo(_linea);
		} else {
			ContentValues values = new ContentValues();
			values.put(ChedrauiPOSSQLiteHelper.COL_PRECIO, precio);
			db.update(ChedrauiPOSSQLiteHelper.T_ARTICULO, values, ChedrauiPOSSQLiteHelper.COL_UPC + "=" + upc, null);
		}
	}
	
	public Articulo cursorToArticulo(Cursor cursor) {

		String upc = cursor.getString(1);
		String tienda = cursor.getString(2);
		String articulo = cursor.getString(3);
		String um = cursor.getString(4);
		String descripcion = cursor.getString(5);
		String proveedor = cursor.getString(6);
		String marca = cursor.getString(7);
		String departamento = cursor.getString(8);
		String flat = cursor.getString(9);
		double precio = cursor.getDouble(10);
		String tIva = cursor.getString(11);
		String tIeps = cursor.getString(12);
		String tpPromo = cursor.getString(13);
		double descuento = cursor.getDouble(14);
		
		return new Articulo().setUpc(upc)
								.setTienda(tienda)
								.setSku(articulo)
								.setUm(um)
								.setDescripcion(descripcion)
								.setProveedor(proveedor)
								.setMarca(marca)
								.setDepartamento(departamento)
								.setFlat(flat)
								.setPrecio(precio)
								.settIva(tIva)
								.settIeps(tIeps)
								.setTpPromo(tpPromo)
								.setDescuento(descuento);
	}
	
	public long countArticulos() {
		return DatabaseUtils.queryNumEntries(db, ChedrauiPOSSQLiteHelper.T_ARTICULO, null, null);
	}
	
	public int deleteAllArticulos() {
		if (countArticulos() > 0) {
			return db.delete(ChedrauiPOSSQLiteHelper.T_ARTICULO, "1", null);
		} else {
			return 0;
		}
	}
	
	public Articulo selectArticulo(String _upc) {
		
		Articulo articulo = null;
		String[] args = new String[] {_upc};
		 
		StringBuilder query = new StringBuilder();
		query.append("SELECT * FROM ").append(ChedrauiPOSSQLiteHelper.T_ARTICULO).append(" WHERE ").append(
				ChedrauiPOSSQLiteHelper.COL_UPC).append(" = ?");
		
		Cursor cursor = db.rawQuery(query.toString(), args);
		
		if (cursor.moveToFirst())
			 articulo = cursorToArticulo(cursor);
		
		cursor.close();
		
		return articulo;
	}
}
