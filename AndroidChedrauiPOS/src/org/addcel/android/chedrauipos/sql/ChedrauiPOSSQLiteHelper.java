package org.addcel.android.chedrauipos.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.SyncStateContract.Columns;
import android.util.Log;

public class ChedrauiPOSSQLiteHelper extends SQLiteOpenHelper {
	
	public static final String T_ARTICULO = "articulo";	
	public static final String COL_ID = Columns._ID;
	
//     1 UPC OBLIG, F1
	   public static final String COL_UPC = "upc";
//		2 TDA
		public static final String COL_TIENDA = "tienda";
//		3 SKU 
		public static final String COL_SKU = "sku";
//		4 UM (UNIDAD DE MEDIDA) OBLIG, F1
		public static final String COL_UM = "um";
//		5 DESC CORTA OBLIG, F1
		public static final String COL_DESCRIPCION = "descripcion";
//		6 PROV (PRINCIPAL)
		public static final String COL_PROV = "prov";
//		7 MARCA
		public static final String COL_MARCA = "marca";
//		8 GRUPO DE ARTICULO
		public static final String COL_GRUPO = "grupo";
//		9 FLAT_PESABLE
		public static final String COL_FLAT = "flat";
//		10 PRECIO
		public static final String COL_PRECIO = "precio";
//		11 T.IVA
		public static final String COL_T_IVA = "t_iva";
//		12 T.IEPS
		public static final String COL_T_IEPS = "t_ieps";
//		13 TP_PROMO
		public static final String COL_TP_PROMO = "tp_promo";
//		14 MONTO_DESC
		public static final String COL_DESCUENTO = "descuento";
		
	public static final String T_TICKET = "ticket";
	public static final String COL_CONSECUTIVO = "consecutivo";
	public static final String COL_BARCODE = "barcode";
	public static final String COL_FILE = "file";
	public static final String COL_FECHA = "fecha";
	public static final String COL_PROCESADO = "procesado";
	
	private static final String DB_NOMBRE = "chedraui_pos.db";
	private static final int DB_VERSION = 7;
	
	  // Database creation sql statement 
	private static final String CREATE_T_ARTICULO = "create table "+ T_ARTICULO + " (" 
														  + COL_ID + " integer primary key autoincrement, " 
														  + COL_UPC + " text not null, "
														  + COL_TIENDA + " text not null, " 
													      + COL_SKU + " text not null, "
													      + COL_UM + " text not null, "
														  + COL_DESCRIPCION + " text not null, "
														  + COL_PROV + " text not null, "
														  + COL_MARCA + " text not null, "
														  + COL_GRUPO + " text not null, "
														  + COL_FLAT + " text not null, "
														  + COL_PRECIO + " real, "
														  + COL_T_IVA + " text not null, "
													      + COL_T_IEPS + " text not null, "
														  + COL_TP_PROMO + " text not null, "
														  + COL_DESCUENTO + " real"
											      + ");"; 
	
	private static final String CREATE_T_TICKET = "create table "+ T_TICKET + " (" 
														  + COL_ID + " integer primary key autoincrement, " 
														  + COL_CONSECUTIVO + " text not null, " 
													      + COL_BARCODE + " text not null, "
													      + COL_FILE + " text not null, "
														  + COL_FECHA + " integer, "
														  + COL_PROCESADO + " integer"
											      + ");"; 


	
	public ChedrauiPOSSQLiteHelper(Context context) {
		super(context, DB_NOMBRE, null, DB_VERSION);
		// TODO Auto-generated constructor stub
	}


	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATE_T_ARTICULO);
		db.execSQL(CREATE_T_TICKET);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		 Log.w(ChedrauiPOSSQLiteHelper.class.getName(),
			        "Upgrading database from version " + oldVersion + " to "
			            + newVersion + ", which will destroy all old data");
			    db.execSQL("DROP TABLE IF EXISTS " + T_ARTICULO);
			    db.execSQL("DROP TABLE IF EXISTS " + T_TICKET);
	    onCreate(db);
	}

}
