package org.addcel.android.chedrauipos.sql;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.addcel.android.chedrauipos.to.Ticket;
import org.addcel.android.util.LogUtil;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class TicketDataSource {
	
	private SQLiteDatabase db;
	private ChedrauiPOSSQLiteHelper chDbHelper;
	
	private String[] ticketColumnas = {
											ChedrauiPOSSQLiteHelper.COL_ID,
											ChedrauiPOSSQLiteHelper.COL_CONSECUTIVO,
											ChedrauiPOSSQLiteHelper.COL_BARCODE,
											ChedrauiPOSSQLiteHelper.COL_FILE,
											ChedrauiPOSSQLiteHelper.COL_FECHA,
											ChedrauiPOSSQLiteHelper.COL_PROCESADO,
										};
	
	private static final String TAG = TicketDataSource.class.getName();
	
	public TicketDataSource(Context _con) {
		// TODO Auto-generated constructor stub
		chDbHelper = new ChedrauiPOSSQLiteHelper(_con);
	}
	
	public void open() throws SQLException {
		db = chDbHelper.getWritableDatabase();
	}
	
	public void close() {
		chDbHelper.close();
	}
	
	public Ticket createTicket(Ticket _t) {
		
		ContentValues values = new ContentValues();
		values.put(ChedrauiPOSSQLiteHelper.COL_CONSECUTIVO, _t.getConsecutivo());
		values.put(ChedrauiPOSSQLiteHelper.COL_BARCODE, _t.getBarcode());
		values.put(ChedrauiPOSSQLiteHelper.COL_FILE, _t.getFileString());
		values.put(ChedrauiPOSSQLiteHelper.COL_FECHA, _t.getFecha().getTime());
		values.put(ChedrauiPOSSQLiteHelper.COL_PROCESADO, _t.isProcesado());
		
		long insertId = db.insert(ChedrauiPOSSQLiteHelper.T_TICKET, null, 
				values);
		
		Cursor cursor =  db.query(ChedrauiPOSSQLiteHelper.T_TICKET,
		        ticketColumnas, ChedrauiPOSSQLiteHelper.COL_ID + " = " + insertId, null,
		        null, null, null); 
		
		cursor.moveToFirst();
		Ticket nuevoTicket = cursorToTicket(cursor);
		cursor.close();
		
		if (nuevoTicket != null) {
			LogUtil.text(TAG, "Ticket Insertado exitosamente.");
		} else {
			LogUtil.text(TAG, "Error al insertar ticket.");
		}
		
		return nuevoTicket;
	}
	
	public List<Ticket> getAllTickets() {
		List<Ticket> tickets = new ArrayList<Ticket>();
		
		Cursor cursor = db.query(ChedrauiPOSSQLiteHelper.T_TICKET,
		        ticketColumnas, null, null, null, null, null);
		
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			Ticket t = cursorToTicket(cursor);
			tickets.add(t);
			cursor.moveToNext();
		}
		
		cursor.close();
		return tickets;
	}
	
	public Ticket cursorToTicket(Cursor cursor) {
		
		return new Ticket().set_id(cursor.getInt(0))
		.setConsecutivo(cursor.getString(1))
		.setBarcode(cursor.getString(2))
		.setFileString(cursor.getString(3))
		.setFecha(new Date(cursor.getLong(4)))
		.setProcesado(cursor.getInt(5) == 1 ? true : false);
		
	}
}
