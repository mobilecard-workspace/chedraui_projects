package org.addcel.android.chedrauipos.sql.listener;

import java.util.List;

public interface QueryListener<E> {
	
	public void onInsert(E e);
	public void onGet(E e);
	public void onGetAll(List<E> _list);

}
