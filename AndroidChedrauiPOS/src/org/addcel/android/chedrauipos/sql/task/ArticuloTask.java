package org.addcel.android.chedrauipos.sql.task;

import org.addcel.android.chedrauipos.listener.ArticuloQueryListener;
import org.addcel.android.chedrauipos.sql.ArticuloDataSource;
import org.addcel.android.chedrauipos.to.Articulo;
import org.addcel.android.chedrauipos.util.BarcodeNotProcessor;
import org.addcel.android.chedrauipos.util.BarcodeProcessor;
import org.addcel.android.chedrauipos.util.BarcodeProcessorFactory;
import org.addcel.android.chedrauipos.util.BarcodeProcessorFactory.ProcessorType;
import org.addcel.android.util.LogUtil;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

public class ArticuloTask extends AsyncTask<Void, Void, Articulo> {
	
	private Context con;
	private String barcode;
	private BarcodeProcessor processor;
	private ArticuloQueryListener listener;
	private ArticuloDataSource dSource;
	private ProgressDialog pDialog;
	
	private static final String TAG = "ArticuloTask";
	
	public ArticuloTask(Context con, String barcode, ArticuloQueryListener listener, ArticuloDataSource dSource) {
		// TODO Auto-generated constructor stub
		this.con = con;
		this.barcode = barcode;
		this.listener = listener;
		this.dSource = dSource;
		
		int bCodeLen = barcode.length();
		switch (bCodeLen) {
		case 18:
			if (TextUtils.indexOf(barcode, "23") == 0 || TextUtils.indexOf(barcode, "25") == 0) {
				processor = BarcodeProcessorFactory.get(ProcessorType.PESABLE);
			} else {
				processor = BarcodeProcessorFactory.get(ProcessorType.DEFAULT);
			}
			break;

		default:
			processor = BarcodeProcessorFactory.get(ProcessorType.DEFAULT);
			break;
		}
	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		if (pDialog == null) {
			pDialog = ProgressDialog.show(con, "Buscando artículo", "Espere, por favor.", true, false);
		}
	}

	@Override
	protected Articulo doInBackground(Void... params) {
		// TODO Auto-generated method stub
		String upc = processor.getUpc(barcode);
		Articulo a = null;
		dSource.open();
		LogUtil.text(TAG, "Se ejecuta query para: " + upc);
		a = dSource.selectArticulo(upc);
		
		return a;
	}
	
	@Override
	protected void onPostExecute(Articulo result) {
		// TODO Auto-generated method stub
		if (pDialog != null && pDialog.isShowing()) {
			pDialog.dismiss();
		}
		if (processor instanceof BarcodeNotProcessor) {
			listener.onQueryFinished(result);
		} else {
			dSource.close();
			Articulo a = processor.processArticulo(barcode, result);
			listener.onQueryFinished(a);
		}
	}


	
	

}
