package org.addcel.android.chedrauipos.sql.task;

import java.util.List;

import org.addcel.android.chedrauipos.sql.TicketDataSource;
import org.addcel.android.chedrauipos.sql.enums.Operacion;
import org.addcel.android.chedrauipos.sql.listener.QueryListener;
import org.addcel.android.chedrauipos.to.Ticket;
import org.addcel.android.util.LogUtil;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class TicketTask extends AsyncTask<Operacion, Void, Operacion> {

	private TicketDataSource dataSource;
	private QueryListener<Ticket> listener;

	private Context con;
	private boolean loader;
	private ProgressDialog progressDialog;
	private boolean cancellable;
	private Ticket ticket;

	//RESPUESTAS
	private Ticket insertResp;
	private Ticket getResp;
	private List<Ticket> getAllResp;
	
	private static final String TAG = TicketTask.class.getName();

	public TicketTask() {
		// TODO Auto-generated constructor stub
	}

	public TicketDataSource getDataSource() {
		return dataSource;
	}

	public QueryListener<Ticket> getListener() {
		return listener;
	}

	public Context getCon() {
		return con;
	}

	public boolean hasLoader() {
		return loader;
	}

	public ProgressDialog getProgressDialog() {
		return progressDialog;
	}

	public boolean isCancellable() {
		return cancellable;
	}
	
	public Ticket getTicket() {
		return ticket;
	}

	public TicketTask setDataSource(TicketDataSource dataSource) {
		this.dataSource = dataSource;
		return this;
	}

	public TicketTask setListener(QueryListener<Ticket> listener) {
		this.listener = listener;
		return this;
	}

	public TicketTask setCon(Context con) {
		this.con = con;
		return this;
	}

	public TicketTask setLoader(boolean loader) {
		this.loader = loader;
		return this;
	}

	public TicketTask setProgressDialog(ProgressDialog progressDialog) {
		this.progressDialog = progressDialog;
		return this;
	}

	public TicketTask setCancellable(boolean cancellable) {
		this.cancellable = cancellable;
		return this;
	}
	
	public TicketTask setTicket(Ticket ticket) {
		this.ticket = ticket;
		return this;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		if (hasLoader()) {
			progressDialog = ProgressDialog.show(con, "Consulta a Base de datos", "Espere un momento...", true);
		}
	}
	
	@Override
	protected Operacion doInBackground(Operacion... params) {
		// TODO Auto-generated method stub
		
		dataSource.open();
		
		switch (params[0]) {
		case INSERT:
			insertResp = dataSource.createTicket(ticket);
			dataSource.close();
			return Operacion.INSERT;
		case GET:
			return Operacion.GET;
		case GET_ALL:
			getAllResp = dataSource.getAllTickets();
			dataSource.close();
			return Operacion.GET_ALL;

		default:
			return null;
		}
	}
	
	@Override
	protected void onPostExecute(Operacion result) {
		// TODO Auto-generated method stub
		
		if (loader) {
			progressDialog.dismiss();
		}
		
		switch (result) {
		case INSERT:
			listener.onInsert(insertResp);
			break;
		case GET_ALL:
			LogUtil.text(TAG, getAllResp.toString());
			listener.onGetAll(getAllResp);
		default:
			break;
		}
	}

}
