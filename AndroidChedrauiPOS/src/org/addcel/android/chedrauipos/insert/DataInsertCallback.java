package org.addcel.android.chedrauipos.insert;

public interface DataInsertCallback {

	public void onDataInsertFinished(boolean result);
}
