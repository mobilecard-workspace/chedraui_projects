package org.addcel.android.chedrauipos.insert;

import java.io.InputStream;

public interface DataInsertService {

	public boolean run(InputStream inputStream);
}
