package org.addcel.android.chedrauipos.insert;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.addcel.android.chedrauipos.sql.ArticuloDataSource;
import org.addcel.android.chedrauipos.to.Articulo;
import org.addcel.android.util.LogUtil;

import android.util.Log;

public class MassiveDataInsertServiceImpl implements DataInsertService {

	private ArticuloDataSource dSource;
	private BufferedReader bReader;

	private static final String TAG = MassiveDataInsertServiceImpl.class
			.getSimpleName();

	public MassiveDataInsertServiceImpl(ArticuloDataSource dSource) {
		Log.i(TAG, "Inicializando constructor");
		this.dSource = dSource;
	}

	@Override
	public boolean run(InputStream inputStream) {
		// TODO Auto-generated method stub
		try {
			bReader = new BufferedReader(new InputStreamReader(inputStream,
					"UTF8"));

			dSource.open();
			dSource.deleteAllArticulos();

			String line;
			while ((line = bReader.readLine()) != null) {

				Articulo a = dSource.createArticulo(line);
				if (a != null) {
					LogUtil.text(TAG, a.getUpc() + " Insertado exitosamente.");
				}
			}
			return true;

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			return false;
		} finally {
			dSource.close();
			try {
				inputStream.close();
			} catch (IOException e) {
				LogUtil.error(TAG, "run", e);
			}
		}
	}

}
