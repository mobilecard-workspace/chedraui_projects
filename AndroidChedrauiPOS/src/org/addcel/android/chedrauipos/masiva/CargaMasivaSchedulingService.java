package org.addcel.android.chedrauipos.masiva;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;

import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.activity.MenuActivity;
import org.addcel.android.chedrauipos.constant.Rutas;
import org.addcel.android.chedrauipos.periodica.CargaPeriodicaAlarmReceiver;
import org.addcel.android.chedrauipos.session.UserSession;
import org.addcel.android.chedrauipos.sql.ArticuloDataSource;
import org.addcel.android.chedrauipos.to.Articulo;
import org.addcel.android.ftp.FileTransferClient;
import org.addcel.android.ftp.SFTPTransferClient;
import org.addcel.android.session.SessionManager;
import org.addcel.android.util.LogUtil;
import org.addcel.android.util.NetworkUtil;
import org.addcel.android.util.Text;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

public class CargaMasivaSchedulingService extends IntentService {
	
	private FileTransferClient client;
	private ArticuloDataSource source;
	private String myPath;
	private String serverPath;
	private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;
	private SessionManager preferences;
	
//	Receiver para el lanzamiento de las actualizaciones periódicas.
	private CargaPeriodicaAlarmReceiver pAlarm = new CargaPeriodicaAlarmReceiver();

    public static final int NOTIFICATION_ID = 1;
    public static boolean isRunning = false;
    private static final String TAG = CargaMasivaSchedulingService.class.getSimpleName();
	
	public CargaMasivaSchedulingService() {
		// TODO Auto-generated constructor stub
		super("CargaMasivaSchedulingService");
		LogUtil.text(TAG, "Inicializando service.");
	}
	
	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		LogUtil.text(TAG, "Entrando a onHandleIntent");
		preferences = new SessionManager(this);
		isRunning = true;
		 //La base de datos no se encuentra actualizada
		preferences.setUpdated(false);
		//Cancelamos lanzamiento de actualizaciones periódicas.
		pAlarm.cancelAlarm(this); 
		if (NetworkUtil.isOnline(this)) {
			//Inicializamos objetos con datos de prueba, se modificarán una vez que el backend se encuentre terminado.
			if (UserSession.getInstance().getTienda() != null) {
				this.client = new SFTPTransferClient("50.57.192.214", "rmuniz", "xZZWj4GFzTZa");
				this.source = new ArticuloDataSource(this);
				this.myPath = getFilesDir() + "/" + buildFileName(UserSession.getInstance().getTienda().getCodigo());
				this.serverPath = Rutas.DESCARGA;
				//Buscamos descargar última versión del masivo de artículos
				int actualizado = get();
				LogUtil.text(TAG, "Termino proceso de descarga de archivo");
				//Si archivo se actualizó correctamente, lanzamos inserción en base de datos.
				if (actualizado == 0) {
					int insert = insertArticles(myPath);
					switch (insert) {
					case 0:
						/*
						 * Si la inserción de datos se llevó a cabo correctamente:
						 *  - asignamos timestamp con la hora de la actualización
						 *  - Marcamos que la base de datos se encuentra actualizada
						 *  - Activamos la alarma de actualizaciones periódicas.
						 *  - Enviamos notificación a la pantalla.
						 */
						Date actualizacion = Calendar.getInstance().getTime();
						preferences.setUpdated(true);
						preferences.setFechaActualizacion(Text.formatDateChedraui(actualizacion));
				        pAlarm.setAlarm(this);
						sendNotification("Base de datos actualizada con éxito.");
						break;
		
					default:
						/*
						 * Si hubo error en la inserción de datos:
						 *  - Dejamos status como al inicio y lanzamos notificación de error.
						 */
						LogUtil.error(TAG, "Error al actualizar base de datos.", null);
						sendNotification("Error en actualización base de datos.");
						break;
					}
				} else {
					/*
					 * Si hubo error en la descarga de archivo:
					 *  - Dejamos status como al inicio y lanzamos notificación de error.
					 */
					LogUtil.error(TAG, "Error al descargar archivo.", null);
					sendNotification("Error en descarga de archivo.");
				}
			} else {
				LogUtil.error(TAG, "Sucursal no cargada.", null);
				sendNotification("Da de alta una sucursal para sincronizar artículos.");
			}
		} else {
			/*
			 * Si no hay internet.
			 *  - Dejamos status como al inicio y lanzamos notificación de error.
			 */
			LogUtil.error(TAG, "Sin conexión a internet.", null);
			sendNotification("Sin conexión a internet, no se pueden actualizar artículos.");
		}
		
		/*
		 * Marcamos que el servicio ya no se encuentra activo y
		 * señalamos que se terminó de ejecutar el servicio
		 */
		isRunning = false;
		CargaMasivaAlarmReceiver.completeWakefulIntent(intent);
	}
	
	private int get() {
		if (((SFTPTransferClient) client).connect()) {
			return client.getFile(myPath, serverPath);
		} else {
			return -10000;
		}
	}
	
	private int insertArticles(String myPath) {
		
		 FileInputStream _ims = null;
		 BufferedReader reader = null;
		    try { 
		    	_ims = new FileInputStream(myPath);
		    	source.open();
		    	
		    	if (source.deleteAllArticulos() > 0)
		    		LogUtil.text(TAG, "Se ha a eliminado inventario previo.");
		    	else 
		    		LogUtil.text(TAG, "NO se ha a eliminado inventario previo.");
		    	
		    	reader = new BufferedReader(new InputStreamReader(_ims, "UTF8"));
		    	String line;
		        while ((line = reader.readLine()) != null) {
		           Articulo a = source.createArticulo(line);
		        	if (a != null) {
				        LogUtil.text(TAG, a.getSku() + " Insertado exitosamente.");
		        	}
		        } 
		        return 0;
		        
		    } catch (UnsupportedEncodingException e) {
		    	LogUtil.error(TAG, "insertArticles", e);
		    	source.deleteAllArticulos();
		    	source.close();
		    	return -10000;
		    } catch (IOException ex) {
		        // handle exception 
	            LogUtil.error(TAG, "insertArticles", ex);
	            source.deleteAllArticulos();
		    	source.close();
		    	return -10000;
		    } 
		    
		    finally { 
		    	
		        try { 
		            _ims.close(); 
		            source.close();
		        } 
		        catch (IOException e) {
		            source.deleteAllArticulos();
		            LogUtil.error(TAG, "insertArticles", e);
		        } 
		    } 
	}
	
	
	
	private void sendNotification(String msg) {
        mNotificationManager = (NotificationManager)
               this.getSystemService(Context.NOTIFICATION_SERVICE);
    
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
            new Intent(this, MenuActivity.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
        .setSmallIcon(R.drawable.ic_launcher)
        .setContentTitle("Proceso actualización")
        .setStyle(new NotificationCompat.BigTextStyle()
        .bigText(msg))
        .setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
	
	private String buildFileName(String tienda) {
		int tiendaLen = tienda.length();
		switch (tiendaLen) {
		case 2:
			return "MAS00" + tienda + ".txt";
		
		case 3:
			return "MAS0" + tienda + ".txt";
			
		default:
			return "MAS" + tienda + ".txt";
		}
	}

}
