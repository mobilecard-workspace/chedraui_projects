package org.addcel.android.chedrauipos.masiva;

import java.util.Calendar;

import org.addcel.android.chedrauipos.boot.BootReceiver;
import org.addcel.android.util.LogUtil;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.content.WakefulBroadcastReceiver;

public class CargaMasivaAlarmReceiver extends WakefulBroadcastReceiver {
	
	private AlarmManager aManager;
	private PendingIntent aIntent;

	private static final String TAG = "CargaMasivaAlarmReceiver";
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		
		LogUtil.text(TAG, "Recibe señal para lanzar servicio.");
		Intent service = new Intent(context, CargaMasivaSchedulingService.class);
		service.putExtra("dir", context.getFilesDir());
		startWakefulService(context, service);
	}
	
	public void setAlarm(Context context) {
		// TODO Auto-generated method stub
		aManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(context, CargaMasivaAlarmReceiver.class);
		aIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		LogUtil.text("CargaMasivaAlarmReceiver", calendar.getTime().toString());
		calendar.set(Calendar.HOUR_OF_DAY, 3);
		calendar.set(Calendar.MINUTE, 0);
		
		
		aManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
				AlarmManager.INTERVAL_DAY, aIntent);
//		aManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, 
//				calendar.getTimeInMillis(), AlarmManager.INTERVAL_HALF_HOUR, aIntent);
//		
		ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();
        
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);    
		
	}
	
	 public void cancelAlarm(Context context) {
	        // If the alarm has been set, cancel it.
	        if (aManager!= null) {
	            aManager.cancel(aIntent);
	        }
	        
	        // Disable {@code SampleBootReceiver} so that it doesn't automatically restart the 
	        // alarm when the device is rebooted.
	        ComponentName receiver = new ComponentName(context, BootReceiver.class);
	        PackageManager pm = context.getPackageManager();

	        pm.setComponentEnabledSetting(receiver,
	                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
	                PackageManager.DONT_KILL_APP);
	    }
	
	

}
