package org.addcel.android.chedrauipos.service;

import org.addcel.android.util.LogUtil;

import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.text.format.DateUtils;

public class SessionTimerBroadcastService extends Service {
	
	private static boolean FINISH = true;
	private final static String TAG = "SessionTimerBroadcastService";
	public final static String TIMER_BR = "org.addcel.android.chedrauipos.service.timer_br";
	Intent ti = new Intent(TIMER_BR);
	
	CountDownTimer timer = null;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		setFlagCerrarSesion();
		
	}
	

    @Override 
    public void onDestroy() { 

        timer.cancel();
        LogUtil.text(TAG, "Timer cancelled");
        super.onDestroy(); 
    } 

    @Override 
    public int onStartCommand(Intent intent, int flags, int startId) {      
    	setFlagCerrarSesion();
    	LogUtil.text(TAG, "Iniciando SessionTimer...");
//		1800000
		timer = new CountDownTimer(30 * DateUtils.MINUTE_IN_MILLIS, 1000) {
			
			@Override
			public void onTick(long millisUntilFinished) {
				// TODO Auto-generated method stub
				LogUtil.text(TAG, millisUntilFinished / DateUtils.MINUTE_IN_MILLIS + " minutos.");
			}
			
			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				if (FINISH) {
					ti.putExtra("logout", true);
					LogUtil.text(TAG, "Cerrar sesión en Context que recibe...");
					sendBroadcast(ti);
				} else {
					ti.putExtra("logout", false);
					LogUtil.text(TAG, "Reiniciar Timer...");
					sendBroadcast(ti);
				}
				
			}
		};
		
		timer.start();
        return START_STICKY;
    } 

    public static void setFlagNoCerrarSesion() {
    	if (FINISH) {
    		LogUtil.text(TAG, "Lanza bandera no cerrar sesión");
    		FINISH = false;
    	}
    }
    
    public static void setFlagCerrarSesion() {
		LogUtil.text(TAG, "Lanza bandera cerrar sesión");
    	FINISH = true;
    }
    
}
