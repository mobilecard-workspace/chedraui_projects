package org.addcel.android.chedrauipos.data;

import java.util.ArrayList;
import java.util.List;

import org.addcel.android.chedrauipos.to.Articulo;

public class Orden {

	private static Orden instance = null;
	
	private List<Articulo> articulos;
	
	private Orden() {
		articulos = new ArrayList<Articulo>();
	}
	
	public static synchronized Orden get() {
		if (instance == null) {
			instance = new Orden();
		}
		
		return instance;
	}
	
	public List<Articulo> getArticulos() {
		return articulos;
	}
	
	public void setArticulos(List<Articulo> articulos) {
		this.articulos = articulos;
	}
	
	public void clear() {
		articulos.clear();
	}
	
}
