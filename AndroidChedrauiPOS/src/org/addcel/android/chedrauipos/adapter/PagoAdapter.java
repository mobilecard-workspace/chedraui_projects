package org.addcel.android.chedrauipos.adapter;

import java.util.List;

import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.to.Pago;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PagoAdapter extends BaseAdapter {
	
	private Context con;
	private List<Pago> data;
	private LayoutInflater inflater;
	
	public PagoAdapter(Context _con, List<Pago> _data) {
		// TODO Auto-generated constructor stub
		con = _con;
		data = _data;
		inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		ViewHolder viewHolder;
		
		if (null == convertView) {
			view = inflater.inflate(R.layout.item_pago, parent, false);
			viewHolder = new ViewHolder(view);
			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) view.getTag();
		}
		
		Pago p = (Pago) data.get(position);
		if (null != p) {
			viewHolder.pagoView.setText(p.toString());
		}
		
		return view;
	}
	
	private class ViewHolder {

		TextView pagoView;
		
		
		public ViewHolder(View _aux) {
			// TODO Auto-generated constructor stub
			
			pagoView = (TextView) _aux.findViewById(R.id.view_pago);
		}

	}

}
