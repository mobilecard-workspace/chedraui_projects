package org.addcel.android.chedrauipos.adapter;

import java.util.List;

import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.listener.ProductoListChangeListener;
import org.addcel.android.chedrauipos.to.Articulo;
import org.addcel.android.util.Text;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

public class ProductoAdapter extends BaseAdapter {

	private Context con;
	private List<Articulo> data;
	private LayoutInflater inflater;
	private double total;
	private ProductoListChangeListener listener;

	public ProductoAdapter(Context _con, List<Articulo> _data, ProductoListChangeListener _listener) {
		// TODO Auto-generated constructor stub
		con = _con;
		data = _data;
		inflater = (LayoutInflater) con
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		listener = _listener;
		
		total = Articulo.getTotal(data);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		ViewHolder viewHolder;
		
		if (null == convertView) {
			
			view = inflater.inflate(R.layout.item_producto, parent, false);
			viewHolder = new ViewHolder(view);
			view.setTag(viewHolder);
			
		} else {
			viewHolder = (ViewHolder) view.getTag();
		}
		
		final Articulo p = (Articulo) data.get(position);
		
		if (null != p) {
			
			final int pos = position;
			viewHolder.precioView.setText(Text.formatCurrency(p.getMontoTotal()));
			String label = p.getDescripcion();
			viewHolder.productoView.setText(label);
			viewHolder.eliminarButton.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					setTotal(Articulo.getTotal(data) - (p.getMontoTotal()));
					listener.onTotalChange(getTotal());
					data.remove(pos);
					listener.onDelete();
					notifyDataSetChanged();
				}
			});

		}
		
		return view;
	}
	
	private class ViewHolder {

		ImageButton eliminarButton;
		TextView precioView;
		TextView productoView;
		
		
		public ViewHolder(View _aux) {
			// TODO Auto-generated constructor stub
			
			eliminarButton = (ImageButton) _aux.findViewById(R.id.btn_eliminar);
			eliminarButton.setFocusable(false);
			eliminarButton.setFocusableInTouchMode(false);
			precioView = (TextView) _aux.findViewById(R.id.view_precio);
			productoView = (TextView) _aux.findViewById(R.id.view_producto);
		}

	}

}
