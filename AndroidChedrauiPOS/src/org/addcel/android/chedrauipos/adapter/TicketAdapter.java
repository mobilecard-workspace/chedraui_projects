package org.addcel.android.chedrauipos.adapter;

import java.util.List;
import java.util.Map;

import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.service.SessionTimerBroadcastService;
import org.addcel.android.chedrauipos.to.Ticket;
import org.addcel.android.chedrauipos.util.GeneraArchivoMTL;
import org.addcel.android.session.SessionManager;
import org.addcel.android.to.FTPReq;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

public class TicketAdapter extends BaseAdapter {
	
	private Context con;
	private List<Ticket> data;
	private LayoutInflater inflater;
	
	public TicketAdapter(Context _con, List<Ticket> _data) {
		con = _con;
		data = _data;
		inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return data.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		View view = arg1;
		ViewHolder viewHolder;
		
		if (null == arg1) {
			
			view = inflater.inflate(R.layout.item_ticket, arg2, false);
			viewHolder = new ViewHolder(view);
			view.setTag(viewHolder);
			
		} else {
			viewHolder = (ViewHolder) view.getTag();
		}
		
		final Ticket p = (Ticket) data.get(arg0);
		
		if (null != p) {
			
			final int pos = arg0;
			
			viewHolder.ticketView.setText(p.toString());
			viewHolder.enviarButton.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					SessionTimerBroadcastService.setFlagNoCerrarSesion();
				}
			});
			
			if (p.isProcesado()) {
				viewHolder.enviarButton.setVisibility(View.GONE);
			}

		}
		
		return view;
	}
	
	private void runFTP(Ticket p) {
			
		SessionManager pref = new SessionManager(con);
		GeneraArchivoMTL generaArchivo = new GeneraArchivoMTL();
		
		if (pref.arePreferencesSet()) {
			
			String mtlFileName = generaArchivo.generaArchivoMTL(con, p.getBarcode(), p.getFileString());
			
//			FileSendListener listener = new FileSendListener(consepcutivo, barcode);
//			
//			new AddcelFtpClient(con).hasLoader(true).setCancellable(false).setFile(mtlFileName).setListener(listener).execute(req);
//			
		}
	}
	
	private class ViewHolder {
		
		TextView ticketView;
		ImageButton enviarButton;
		
		public ViewHolder(View _v) {
			ticketView = (TextView) _v.findViewById(R.id.view_ticket);
			enviarButton = (ImageButton) _v.findViewById(R.id.b_enviar);
		}
	}

}
