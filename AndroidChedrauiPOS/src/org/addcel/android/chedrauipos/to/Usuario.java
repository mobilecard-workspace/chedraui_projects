package org.addcel.android.chedrauipos.to;

import org.addcel.android.interfaces.JSONable;
import org.json.JSONException;
import org.json.JSONObject;

public class Usuario implements JSONable {

	private int idUsuario;
	private int idSupervisor;
	private String login;
	private String password;
	private String nombre;
	private String apellido;
	private String materno;
	private String mail;
	private int status;
	private int loginCount;
	private Tienda tienda;
	private Role role;

	public Usuario() {
		// TODO Auto-generated constructor stub
	}

	public Usuario(JSONObject json) {
		readFromJSON(json);
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public int getIdSupervisor() {
		return idSupervisor;
	}

	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getMaterno() {
		return materno;
	}

	public String getMail() {
		return mail;
	}
	
	public int getStatus() {
		return status;
	}
	
	public int getLoginCount() {
		return loginCount;
	}

	public Tienda getTienda() {
		return tienda;
	}

	public Role getRole() {
		return role;
	}

	public Usuario setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
		return this;
	}

	public Usuario setIdSupervisor(int idSupervisor) {
		this.idSupervisor = idSupervisor;
		return this;
	}

	public Usuario setLogin(String login) {
		this.login = login;
		return this;
	}

	public Usuario setPassword(String password) {
		this.password = password;
		return this;
	}

	public Usuario setNombre(String nombre) {
		this.nombre = nombre;
		return this;
	}

	public Usuario setApellido(String apellido) {
		this.apellido = apellido;
		return this;
	}

	public Usuario setMaterno(String materno) {
		this.materno = materno;
		return this;
	}
	
	public Usuario setMail(String mail) {
		this.mail = mail;
		return this;
	}

	public Usuario setStatus(int status) {
		this.status = status;
		return this;
	}
	
	public Usuario setLoginCount(int loginCount) {
		this.loginCount = loginCount;
		return this;
	}

	public Usuario setTienda(Tienda tienda) {
		this.tienda = tienda;
		return this;
	}

	public Usuario setRole(Role role) {
		this.role = role;
		return this;
	}

	@Override
	public JSONObject writeToJSON() {
		// TODO Auto-generated method stub
		
		try { 
			return new JSONObject()
			.putOpt("idUsuario", idUsuario)
			.put("idSupervisor", idSupervisor)
			.put("login", login)
			.put("password", password)
			.put("nombre", nombre)
			.put("apellido", apellido)
			.put("materno", materno)
			.put("mail", mail)
			.put("idTienda", tienda.getIdTienda())
			.put("idRol", role.getIdRol());
		} catch (JSONException e) {
			return null;
		}
	}

	@Override
	public void readFromJSON(JSONObject json) {
		// TODO Auto-generated method stub
		idUsuario = json.optInt("idUsuario");
		idSupervisor = json.optInt("idSupervisor");
		login = json.optString("login");
		password = json.optString("password");
		nombre = json.optString("nombre");
		apellido = json.optString("apellido");
		materno = json.optString("materno");
		mail = json.optString("mail");
		status = json.optInt("status");
		loginCount = json.optInt("loginCount");
		tienda = new Tienda(json.optJSONObject("tienda"));
		role = new Role(json.optJSONObject("role"));
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return writeToJSON().toString();
	}

}
