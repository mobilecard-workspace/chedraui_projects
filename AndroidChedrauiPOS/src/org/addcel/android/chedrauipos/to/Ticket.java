package org.addcel.android.chedrauipos.to;

import java.util.Date;

import org.addcel.android.util.Text;

public class Ticket {

	private int _id;
	private String consecutivo;
	private String barcode;
	private String fileString;
	private Date fecha;
	private boolean procesado;
	
	public Ticket() {
	}

	public int get_id() {
		return _id;
	}
	
	public String getConsecutivo() {
		return consecutivo;
	}

	public String getBarcode() {
		return barcode;
	}
	
	public String getFileString() {
		return fileString;
	}

	public Date getFecha() {
		return fecha;
	}

	public boolean isProcesado() {
		return procesado;
	}

	public Ticket set_id(int _id) {
		this._id = _id;
		return this;
	}
	
	public Ticket setConsecutivo(String consecutivo) {
		this.consecutivo = consecutivo;
		return this;
	}

	public Ticket setBarcode(String barcode) {
		this.barcode = barcode;
		return this;
	}
	
	public Ticket setFileString(String fileString) {
		this.fileString = fileString;
		return this;
	}

	public Ticket setFecha(Date fecha) {
		this.fecha = fecha;
		return this;
	}

	public Ticket setProcesado(boolean procesado) {
		this.procesado = procesado;
		return this;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		sb.append("Barcode: ").append(barcode)
			.append("\nFecha: ").append(Text.formatDateForShowing(fecha));
		
		return sb.toString();
	}

}
