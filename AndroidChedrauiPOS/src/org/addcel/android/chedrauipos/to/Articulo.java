package org.addcel.android.chedrauipos.to;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.addcel.android.chedrauipos.enums.UnidadMedida;
import org.addcel.android.interfaces.JSONable;
import org.addcel.android.util.LogUtil;
import org.addcel.android.util.Text;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class Articulo implements JSONable, Parcelable {

	private String upc;
	private String tienda;
	private String sku;
	private UnidadMedida um; //únidad de medida, nuevo.
	private String descripcion;
	private String proveedor;
	private String marca;
	private String departamento;
	private String flat;
	private double precio;
	private String tIva;
	private String tIeps;
	private String tpPromo;
	private double descuento; //monto descuento, nuevo.

	private double peso;
	
	/**
	 * Precio unitario x cantidad
	 */
	private double montoTotal;
	
	public Articulo() {
		// TODO Auto-generated constructor stub
	}

	public Articulo(Parcel source) {
		readFromParcel(source);
	}

	public Articulo(JSONObject json) {
		readFromJSON(json);
	}
	
	public String getSku() {
		return sku;
	}

	public String getTienda() {
		return tienda;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getUpc() {
		return upc;
	}

	public double getPrecio() {
		return precio;
	}

	public String getDepartamento() {
		return departamento;
	}
	
	//NUEVOS GET

	public UnidadMedida getUm() {
		return um;
	}

	public String getProveedor() {
		return proveedor;
	}

	public String getMarca() {
		return marca;
	}

	public String getFlat() {
		return flat;
	}

	public String gettIva() {
		return tIva;
	}

	public String gettIeps() {
		return tIeps;
	}

	public String getTpPromo() {
		return tpPromo;
	}

	public double getDescuento() {
		return descuento;
	}
	
	public double getPeso() {
		return peso;
	}
	
	public double getMontoTotal() {
		return montoTotal;
	}
	
	public Articulo setSku(String sku) {
		this.sku = sku;
		return this;
	}
	public Articulo setTienda(String tienda) {
		this.tienda = tienda;
		return this;
	}

	public Articulo setDescripcion(String descripcion) {
		this.descripcion = descripcion;
		return this;
	}

	public Articulo setUpc(String upc) {
		this.upc = upc;
		return this;
	}

	public Articulo setPrecio(double precio) {
		this.precio = precio;
		return this;
	}

	public Articulo setDepartamento(String departamento) {
		this.departamento = departamento;
		return this;
	}
	
	public Articulo setUm(String um) {
		if (TextUtils.equals(UnidadMedida.KG.name(), um)) {
			this.um = UnidadMedida.KG;
		}
		
		if (TextUtils.equals(UnidadMedida.L.name(), um)) {
			this.um = UnidadMedida.L;
		}
		
		if (TextUtils.equals(UnidadMedida.M.name(), um)) {
			this.um = UnidadMedida.M;
		}
		
		if (TextUtils.equals(UnidadMedida.ST.name(), um)) {
			this.um = UnidadMedida.ST;
		}
		
		return this;
	}

	public Articulo setProveedor(String proveedor) {
		this.proveedor = proveedor;
		return this;
	}

	public Articulo setMarca(String marca) {
		this.marca = marca;
		return this;
	}

	public Articulo setFlat(String flat) {
		this.flat = flat;
		return this;
	}

	public Articulo settIva(String tIva) {
		this.tIva = tIva;
		return this;
	}

	public Articulo settIeps(String tIeps) {
		this.tIeps = tIeps;
		return this;
	}

	public Articulo setTpPromo(String tpPromo) {
		this.tpPromo = tpPromo;
		return this;
	}

	public Articulo setDescuento(double descuento) {
		this.descuento = descuento;
		return this;
	}
	
	public Articulo setPeso(double peso) {
		this.peso = peso;
		return this;
	}
	
	public Articulo setMontoTotal(double montoTotal) {
		this.montoTotal = montoTotal;
		return this;
	}

	public static double getTotal(List<Articulo> prodList) {
		double total = 0.0;
		for (Articulo producto : prodList)
			total += (producto.getMontoTotal());
		
		return total;
	}
	
	public static String getTotalString(List<Articulo> prodList) {
		return Text.formatCurrency(getTotal(prodList)).replace("$", "");
	}
	
	public static String getFileString(List<Articulo> prodList) {
		Date hoy = new Date();
		
		DecimalFormat df = new DecimalFormat("0.00");
		DecimalFormat df1 = new DecimalFormat("0.000");
		
		StringBuffer sb = new StringBuffer();
		
		for (Articulo articulo : prodList) {
			
			//Departamento
			sb.append(articulo.getDepartamento() + ";");
			//Código de barras a 12 digitos
			if(articulo.getUpc().length() < 12) {
				sb.append(StringUtils.leftPad(articulo.getUpc(), 12, "0") + ";");
			} else {
				sb.append(articulo.getUpc() + ";");
			}
			//Tipo de producto unitario = 0
			if (articulo.getUm().equals(UnidadMedida.ST))
				sb.append("0;");
			else
				sb.append("1;");
			
			String precio = df.format(Double.valueOf(articulo.getPrecio()));
			precio = precio.replace(".", ""); //PRECIO SIN PUNTO DECIMAL
			sb.append(precio + ";");
			if (articulo.getUm().equals(UnidadMedida.ST)) {
				int cantidad = 1; //AGREGAR ATRIBUTO CANTIDAD
				sb.append(cantidad * 1000 + ";");
			} else {
				String pesoStr = df1.format(Double.valueOf(articulo.getPeso())); //FORMATEAR PESO A 0.000
				pesoStr = pesoStr.replace(".", ""); //ELIMINAR PUNTO DECIMAL
				sb.append(pesoStr + ";");
			}
			
			String subTotal = df.format(Double.valueOf(articulo.getMontoTotal()));
			subTotal = subTotal.replace(".", "");
			sb.append(subTotal + ";");
			
			SimpleDateFormat sf1 = new SimpleDateFormat("hh:mm:ss", Locale.getDefault());
			sb.append(sf1.format(new Date()) + ";");
			
			SimpleDateFormat sf2 = new SimpleDateFormat("dd/MM/yy", Locale.getDefault());
			sb.append(sf2.format(hoy) + "\r\n");
		}
		
		return sb.toString();
		
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(sku);
		dest.writeString(tienda);
		dest.writeString(descripcion);
		dest.writeString(upc);
		dest.writeDouble(precio);
		dest.writeString(departamento);
	}

	public void readFromParcel(Parcel source) {
		sku = source.readString();
		tienda = source.readString();
		descripcion = source.readString();
		upc = source.readString();
		precio = source.readDouble();
		departamento = source.readString();
	}

	@Override
	public JSONObject writeToJSON() {
		// TODO Auto-generated method stub
		
		try {
			
			return new JSONObject().put("marca", marca)
								   .put("modeloProd", sku)
								   .put("idProducto", upc)
								   .put("descripcion", descripcion)
								   .put("monto", montoTotal);
			
		} catch (JSONException e) {
		
			return null;
		}
	}

	@Override
	public void readFromJSON(JSONObject json) {
		// TODO Auto-generated method stub
		sku = json.optString("idProducto");
		descripcion = json.optString("descripcion");
		precio = json.optDouble("monto");
		departamento = json.optString("marca");
		sku = json.optString("modelo");
		upc = json.optString("idProducto");
//		SUCURSAL SE AGREGA CON set
	}
	
	
	public static JSONArray buildJSONArray(List<Articulo> articulos) {
		
		JSONArray array = new JSONArray();
		
		for (Articulo articulo : articulos) {
			array.put(articulo.writeToJSON());
		}
		
		return array;
		
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(descripcion)
			.append("\nArtículo: ").append(upc)
			.append("\nTienda: ").append(tienda)
		  	.append("\nDepartamento: ").append(departamento)
		  	.append("\nSubtotal: ").append(Text.formatCurrency(montoTotal));
		
		return sb.toString();
	}
	
	public static final Parcelable.Creator<Articulo> CREATOR = new Creator<Articulo>() {
		
		@Override
		public Articulo[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Articulo[size];
		}
		
		@Override
		public Articulo createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new Articulo(source);
		}
	};

}
