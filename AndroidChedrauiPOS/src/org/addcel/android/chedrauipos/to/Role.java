package org.addcel.android.chedrauipos.to;

import org.addcel.android.interfaces.JSONable;
import org.json.JSONException;
import org.json.JSONObject;

public class Role implements JSONable {

	private int idRol;
	private String descripcion;

	public Role() {
		// TODO Auto-generated constructor stub
	}

	public Role(JSONObject json) {
		readFromJSON(json);
	}
	
	public Role(int idRol, String descripcion) {
		setIdRol(idRol);
		setDescripcion(descripcion);
	}

	public int getIdRol() {
		return idRol;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public JSONObject writeToJSON() {
		// TODO Auto-generated method stub
		try { 
			return new JSONObject().put("idRol", idRol).put("descripcion", descripcion);
		} catch (JSONException e) {
			return null;
		}
	}

	@Override
	public void readFromJSON(JSONObject json) {
		// TODO Auto-generated method stub
		idRol = json.optInt("idRol");
		descripcion = json.optString("descripcion");
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return descripcion;
	}

}
