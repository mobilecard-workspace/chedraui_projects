package org.addcel.android.chedrauipos.to;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.addcel.android.interfaces.JSONable;
import org.addcel.android.util.ErrorUtil;
import org.addcel.android.util.Text;
import org.json.JSONArray;
import org.json.JSONObject;

import android.text.TextUtils;

public class Result implements JSONable {

	private int idError;
	private String mensajeError;
	private String referenciaMC;
	private String numAutorizacion;
	private Date fecha;
	private double monto;
	private double comision;
	private double total;
	private String sucursal;
	private List<Articulo> productos;
	
	public Result() {
		// TODO Auto-generated constructor stub
	}

	public Result(JSONObject json) {
		readFromJSON(json);
	}

	public int getIdError() {
		return idError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public String getReferenciaMC() {
		return referenciaMC;
	}

	public String getNumAutorizacion() {
		return numAutorizacion;
	}

	public Date getFecha() {
		return fecha;
	}

	public double getMonto() {
		return monto;
	}

	public double getComision() {
		return comision;
	}
	
	public double getTotal() {
		return total;
	}
	
	public List<Articulo> getProductos() {
		return productos;
	}
	
	public String getSucursal() {
		return sucursal;
	}

	public Result setIdError(int idError) {
		this.idError = idError;
		return this;
	}

	public Result setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
		return this;
	}

	public Result setReferenciaMC(String referenciaMC) {
		this.referenciaMC = referenciaMC;
		return this;
	}

	public Result setNumAutorizacion(String numAutorizaion) {
		this.numAutorizacion = numAutorizaion;
		return this;
	}

	public Result setFecha(Date fecha) {
		this.fecha = fecha;
		return this;
	}

	public Result setMonto(double monto) {
		this.monto = monto;
		return this;
	}

	public Result setComision(double comision) {
		this.comision = comision;
		return this;
	}
	
	public Result setTotal(double total) {
		this.total = total;
		return this;
	}
	
	public Result setProductos(List<Articulo> productos) {
		this.productos = productos;
		return this;
	}
	
	public Result setSucursal(String sucursal) {
		this.sucursal = sucursal;
		return this;
	}

	@Override
	public JSONObject writeToJSON() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void readFromJSON(JSONObject json) {
		// TODO Auto-generated method stub

		idError = json.optInt("idError", -10000);
		mensajeError = json.optString("mensajeError", ErrorUtil.connection());
		referenciaMC = json.optString("referenciaMC");
		numAutorizacion = json.optString("numAutorizacion");
		fecha = Text.getSimpleDateFromStringWithSlash(json.optString("fecha"));
		monto = json.optDouble("monto");
		comision = json.optDouble("comision");
		total = json.optDouble("total");
		sucursal = json.optString("sucursal");
		
		JSONArray array = json.optJSONArray("productos");
		
		if (null != array && array.length() > 0) {
			
			productos = new ArrayList<Articulo>(array.length());

			for (int i = 0; i < array.length(); i++) {
				
				productos.add(new Articulo(array.optJSONObject(i)).setTienda(sucursal));
			}
		}
		

	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		sb.append("Monto: ").append(Text.formatCurrency(monto))
		.append("\nAutorización Bancaria: ").append(numAutorizacion)
		.append("\nFecha y Hora: ").append(Text.formatDateTimeForShowing(fecha))
		.append("\nReferencia Mobilecard: ").append(referenciaMC);
		
		
		if(comision > 0.0) {
			sb.append("\nComisión").append("    ").append(Text.formatCurrency(comision));
			sb.append("\nTotal (monto + comisión)").append("    ").append(Text.formatCurrency(total));
		}
		
		return sb.toString();
	}
	
	public String buildArticleListString() {
		StringBuilder sb = new StringBuilder();
		
		for (Articulo a : productos) {
			sb.append("\n");
			
			if (a.getDescripcion().length() > 20)
				sb.append(TextUtils.substring(a.getDescripcion(), 0, 20));
			else
				sb.append(a.getDescripcion());
			
			sb.append("  ").append(Text.formatCurrency(a.getPrecio()));
		}
		
		
		
		return sb.toString();
	}

}
