package org.addcel.android.chedrauipos.to;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.addcel.android.interfaces.JSONable;
import org.addcel.android.util.Text;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.text.TextUtils;

public class Pago implements JSONable {

	private String sucursal;
	private double total;
	private String nombre;
	private String numAutorizacion;
	private String idUsuario;
	private Date fecha;
	private String email;
	private String referenciaMC;
	private int tipoTarjeta;
	private List<Articulo> productos;
	private String tarjeta;
	private double comision;
	private double monto;

	public Pago() {

	}

	public Pago(JSONObject json) {
		readFromJSON(json);
	}

	public String getSucursal() {
		return sucursal;
	}

	public double getTotal() {
		return total;
	}

	public String getNombre() {
		return nombre;
	}

	public String getNumAutorizacion() {
		return numAutorizacion;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public Date getFecha() {
		return fecha;
	}

	public String getEmail() {
		return email;
	}

	public String getReferenciaMC() {
		return referenciaMC;
	}

	public int getTipoTarjeta() {
		return tipoTarjeta;
	}

	public List<Articulo> getProductos() {
		return productos;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public double getComision() {
		return comision;
	}

	public double getMonto() {
		return monto;
	}

	public Pago setSucursal(String sucursal) {
		this.sucursal = sucursal;
		return this;
	}

	public Pago setTotal(double total) {
		this.total = total;
		return this;
	}

	public Pago setNombre(String nombre) {
		this.nombre = nombre;
		return this;
	}

	public Pago setNumAutorizacion(String numAutorizacion) {
		this.numAutorizacion = numAutorizacion;
		return this;
	}

	public Pago setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
		return this;
	}

	public Pago setFecha(Date fecha) {
		this.fecha = fecha;
		return this;
	}

	public Pago setEmail(String email) {
		this.email = email;
		return this;
	}

	public Pago setReferenciaMC(String referenciaMC) {
		this.referenciaMC = referenciaMC;
		return this;
	}

	public Pago setTipoTarjeta(int tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
		return this;
	}

	public Pago setProductos(List<Articulo> productos) {
		this.productos = productos;
		return this;
	}

	public Pago setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
		return this;
	}

	public Pago setComision(double comision) {
		this.comision = comision;
		return this;
	}

	public Pago setMonto(double monto) {
		this.monto = monto;
		return this;
	}

	@Override
	public JSONObject writeToJSON() {
		// TODO Auto-generated method stub

		try {
			return new JSONObject().put("idUsuario", idUsuario).put(
					"referenciaMC", referenciaMC); // De momento construye
													// request de reenvío.
		} catch (JSONException e) {
			return null;
		}

	}

	@Override
	public void readFromJSON(JSONObject json) {
		// TODO Auto-generated method stub

		sucursal = json.optString("sucursal");
		total = json.optDouble("total");
		nombre = json.optString("nombre");
		numAutorizacion = json.optString("numAutorizacion");
		idUsuario = json.optString("idUsuario");
		fecha = Text.getSimpleDateFromStringWithSlash(json.optString("fecha"));
		email = json.optString("email");
		referenciaMC = json.optString("referenciaMC");
		tipoTarjeta = json.optInt("tipoTarjeta");

		JSONArray array = json.optJSONArray("productos");
		if (array != null && array.length() > 0) {
			productos = new ArrayList<Articulo>(array.length());

			for (int i = 0; i < array.length(); i++) {
				productos.add(new Articulo(array.optJSONObject(i)).setTienda(
						sucursal));
			}
		}

		tarjeta = json.optString("tarjeta");
		comision = json.optDouble("comision");
		monto = json.optDouble("monto");

	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(Text.formatDateTimeForShowing(fecha))
			.append(" - ")
			.append(Text.formatCurrency(total));
		
		return sb.toString();
	}
	
	public String buildReceipt() {
		StringBuilder sb = new StringBuilder();
		sb.append("Tienda: " + sucursal);
		sb.append("\nFecha de venta: ").append(Text.formatDateForShowing(fecha));
		sb.append("\n\nArtículo").append("                             ").append("Precio");
		
		for (Articulo a : productos) {
			sb.append("\n");
			
			if (a.getDescripcion().length() > 20)
				sb.append(TextUtils.substring(a.getDescripcion(), 0, 19));
			else
				sb.append(a.getDescripcion());
			
			sb.append("    ").append(Text.formatCurrency(a.getPrecio()));
		}
		
		sb.append("\n\nTarjeta").append("   ").append(tarjeta);
		sb.append("\nMonto").append("   ").append(Text.formatCurrency(monto));
		
		if(comision > 0.0) {
			sb.append("\nComisión").append("    ").append(Text.formatCurrency(comision));
			sb.append("\nTotal (monto + comisión)").append("    ").append(Text.formatCurrency(total));
		}
		
		return sb.toString();
	}

}
