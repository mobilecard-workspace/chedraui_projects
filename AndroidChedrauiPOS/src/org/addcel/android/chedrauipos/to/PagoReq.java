package org.addcel.android.chedrauipos.to;

import java.util.List;

import org.addcel.android.enums.Plataforma;
import org.addcel.android.interfaces.JSONable;
import org.addcel.android.util.DeviceUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.text.TextUtils;

public class PagoReq implements JSONable {

	private long idUsuario;
	private String password;
	private String nombre;
	private String email;
	private String tarjeta;
	private String mes;
	private String anio;
	private String cvv2;
	private List<Articulo> articulos;
	private String token;
	private Context con;

	public PagoReq() {
		// TODO Auto-generated constructor stub
	}

	public long getIdUsuario() {
		return idUsuario;
	}

	public String getPassword() {
		return password;
	}

	public String getNombre() {
		return nombre;
	}
	
	public String getEmail() {
		return email;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public String getMes() {
		return mes;
	}

	public String getAnio() {
		return anio;
	}

	public String getCvv2() {
		return cvv2;
	}

	public List<Articulo> getArticulos() {
		return articulos;
	}

	public String getToken() {
		return token;
	}

	public Context getCon() {
		return con;
	}
	
	public PagoReq setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
		return this;
	}

	public PagoReq setPassword(String password) {
		this.password = password;
		return this;
	}

	public PagoReq setNombre(String nombre) {
		this.nombre = nombre;
		return this;
	}
	
	public PagoReq setEmail(String email) {
		this.email = email;
		return this;
	}

	public PagoReq setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
		return this;
	}

	public PagoReq setMes(String mes) {
		this.mes = mes;
		return this;
	}

	public PagoReq setAnio(String anio) {
		this.anio = anio;
		return this;
	}

	public PagoReq setCvv2(String cvv2) {
		this.cvv2 = cvv2;
		return this;
	}

	public PagoReq setArticulos(List<Articulo> articulos) {
		this.articulos = articulos;
		return this;
	}

	public PagoReq setToken(String token) {
		this.token = token;
		return this;
	}
	
	public PagoReq setCon(Context con) {
		this.con = con;
		return this;
	}

	@Override
	public JSONObject writeToJSON() {
		// TODO Auto-generated method stub
		
		int tipoTarjeta = 0;
		
		if (TextUtils.indexOf(tarjeta, '4') == 0) {
			tipoTarjeta = 1;
		}
		
		if (TextUtils.indexOf(tarjeta, '5') == 0) {
			tipoTarjeta = 2;
		}
		
		try {
			return new JSONObject().put("idUsuario", idUsuario)
									.put("password", password)
									.put("nombre", nombre)
									.put("email",email)
									.put("tipoTarjeta", tipoTarjeta)
									.put("tarjeta", tarjeta)
									.put("vigencia", mes + "/" + anio)
									.put("cvv2", cvv2)
									.put("sucursal", articulos.get(0).getTienda())
									.put("total", Articulo.getTotal(articulos))
									.put("productos", Articulo.buildJSONArray(articulos))
									.put("token", token)
									.put("tipo", DeviceUtils.getTipo())
									.put("modelo", DeviceUtils.getModel())
									.put("software", DeviceUtils.getSWVersion())
									.put("imei", DeviceUtils.getIMEI(con))
									.put("cy", 0)
									.put("cx", 0);
					
			
		} catch (JSONException e) {

			return null;
		}
	}

	@Override
	public void readFromJSON(JSONObject json) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return writeToJSON().toString();
	}

}
