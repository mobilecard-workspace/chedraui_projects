package org.addcel.android.chedrauipos.to;

import org.addcel.android.interfaces.JSONable;
import org.json.JSONException;
import org.json.JSONObject;

public class Tienda implements JSONable {

	private String codigo;
	private int status;
	private String urlFTP;
	private int posMobile;
	private String descripcion;
	private String userFTP;
	private int idTienda;
	private int rompeFilas;
	private String passFTP;

	public Tienda() {
		// TODO Auto-generated constructor stub
	}

	public Tienda(JSONObject json) {
		readFromJSON(json);
	}

	public int getIdTienda() {
		return idTienda;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getUrlFTP() {
		return urlFTP;
	}

	public String getUserFTP() {
		return userFTP;
	}

	public String getPassFTP() {
		return passFTP;
	}

	public int getStatus() {
		return status;
	}
	
	public int getPosMobile() {
		return posMobile;
	}

	public int getRompeFilas() {
		return rompeFilas;
	}

	public Tienda setIdTienda(int idTienda) {
		this.idTienda = idTienda;
		return this;
	}

	public Tienda setCodigo(String codigo) {
		this.codigo = codigo;
		return this;
	}

	public Tienda setDescripcion(String descripcion) {
		this.descripcion = descripcion;
		return this;
	}

	public Tienda setUrlFTP(String urlFTP) {
		this.urlFTP = urlFTP;
		return this;
	}

	public Tienda setUserFTP(String userFTP) {
		this.userFTP = userFTP;
		return this;
	}

	public Tienda setPassFTP(String passFTP) {
		this.passFTP = passFTP;
		return this;
	}

	public Tienda setStatus(int status) {
		this.status = status;
		return this;
	}

	public Tienda setPosMobile(int posMobile) {
		this.posMobile = posMobile;
		return this;
	}

	public Tienda setRompeFilas(int rompeFilas) {
		this.rompeFilas = rompeFilas;
		return this;
	}

	@Override
	public JSONObject writeToJSON() {
		// TODO Auto-generated method stub
		try {
			return new JSONObject().put("idTienda", idTienda)
									.put("codigo", codigo)
									.put("descripcion", descripcion)
									.put("urlFTP", urlFTP)
									.put("userFTP", userFTP)
									.put("passFTP", passFTP)
									.put("status", status)
									.put("posMobile", posMobile)
									.put("rompeFilas", rompeFilas);
		} catch (JSONException e) {
			return null;
		}
	}

	@Override
	public void readFromJSON(JSONObject json) {
		// TODO Auto-generated method stub
		idTienda = json.optInt("idTienda");
		codigo = json.optString("codigo");
		descripcion = json.optString("descripcion");
		urlFTP = json.optString("urlFTP");
		userFTP = json.optString("userFTP");
		passFTP = json.optString("passFTP");
		status = json.optInt("status");
		posMobile = json.optInt("posMobile");
		rompeFilas = json.optInt("rompeFilas");
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return descripcion;
	}

}
