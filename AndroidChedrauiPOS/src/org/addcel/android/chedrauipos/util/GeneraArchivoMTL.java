/*
 * COPYRIGHT:
 * Licensed Materials - Property of Toshiba Global Commerce Solutions, Inc.
 * Author: Daniel Flores Cruz
 * 
 * Change Activity Log
 * Reason  Date     Who  Description
 * ------- -------- ---  -----------
 * 
 */
package org.addcel.android.chedrauipos.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.addcel.android.chedrauipos.to.Articulo;
import org.addcel.android.util.LogUtil;

import android.content.Context;

//Esta clase genera el archivo .dat que ser� enviado a el controlador con el detalle de la actual transacci�n
public class GeneraArchivoMTL {
	
	private static final String TAG = "GeneraArchivoMTL";
	
	public String generaArchivoMTL(Context con, String barCode, List<Articulo> modelCarrito){
		
		//A partir de la posici�n 2 es donde empieza el consecutivo y tomas 1 digito del precio
		String consecutivoArchivo = barCode.substring(2, 7);
		
		String archivo = "MTL" + consecutivoArchivo + ".dat";
		
		LogUtil.text(TAG, "Generando archivo MTL: " + archivo);
		
		File mtlFile = new File(con.getFilesDir(), archivo);
		
		//Para el caso en que ya se reinicio el consecutivo de tickets (9999 a 9001) y pudiera generarse un mtl ya existente
		if(mtlFile.exists()){
			LogUtil.text(TAG, "El archivo: " + archivo + " ya existia, por lo que sera borrado para no afectar la transaccion actual");
			mtlFile.delete();
		}
		
		Date hoy = new Date();
		
		try {
			
			DecimalFormat df = new DecimalFormat("0.00");
			FileWriter fw = new FileWriter(mtlFile);
			BufferedWriter bw = new BufferedWriter(fw);
			
			StringBuffer sb = new StringBuffer();
			
			for (Articulo articulo : modelCarrito) {
				
				//Departamento
				sb.append(articulo.getDepartamento() + ";");
				//C�digo de barras a 12 digitos
				sb.append(org.apache.commons.lang3.StringUtils.leftPad(articulo.getUpc(), 12, "0") + ";");
				//Tipo de producto unitario = 0 |1 º
				sb.append("0;");
				String precio = df.format(Double.valueOf(articulo.getPrecio()));
				precio = precio.replace(".", "");
				//Precio sin puntos decimales
				sb.append(precio + ";");
				int cantidad = 1; //AGREGAR ATRIBUTO CANTIDAD
				sb.append(cantidad*1000 + ";");
//				String subTotal = df.format(Double.valueOf(Articulo.getTotal(modelCarrito)));
				String subTotal = df.format(Double.valueOf(articulo.getPrecio()));
				subTotal = subTotal.replace(".", "");
				sb.append(subTotal + ";");
				
				SimpleDateFormat sf1 = new SimpleDateFormat("hh:mm:ss");
				sb.append(sf1.format(new Date()) + ";");
				
				SimpleDateFormat sf2 = new SimpleDateFormat("dd/MM/yy");
				sb.append(sf2.format(hoy) + "\r\n");
			}
			
			LogUtil.text(TAG, "Archivo: ");
			LogUtil.text(TAG, sb.toString());
			
			bw.write(sb.toString());
			
			bw.flush();
			bw.close();
			
			fw.close();
			
			
		} catch (IOException e) {
			LogUtil.error(TAG, "IOException, ", e);
		} catch(Exception e){
			LogUtil.error(TAG, "Exception, ", e);
		}
		
		return archivo;
	}
	
	public String generaArchivoMTL(Context con, String barCode, String fileString) {
		
		//A partir de la posición 2 es donde empieza el consecutivo y tomas 1 digito del precio
		String consecutivoArchivo = barCode.substring(2, 7);
		
		String archivo = "MTL" + consecutivoArchivo + ".dat";
		
		LogUtil.text(TAG, "Generando archivo MTL: " + archivo);
		
		File mtlFile = new File(con.getFilesDir(), archivo);
		
		//Para el caso en que ya se reinicio el consecutivo de tickets (9999 a 9001) y pudiera generarse un mtl ya existente
		if(mtlFile.exists()){
			LogUtil.text(TAG, "El archivo: " + archivo + " ya existia, por lo que sera borrado para no afectar la transaccion actual");
			mtlFile.delete();
		}
		
		try {
			
			FileWriter fw = new FileWriter(mtlFile);
			BufferedWriter bw = new BufferedWriter(fw);
			
			
			
			bw.write(fileString);
			
			bw.flush();
			bw.close();
			
			fw.close();
			
			
		} catch (IOException e) {
			LogUtil.error(TAG, "IOException, ", e);
		} catch(Exception e){
			LogUtil.error(TAG, "Exception, ", e);
		}
		
		return archivo;
	}
	
	

}
