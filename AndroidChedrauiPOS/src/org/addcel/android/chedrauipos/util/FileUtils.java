package org.addcel.android.chedrauipos.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;


public class FileUtils {
	
	public static InputStream open(String path) throws IOException {
		File file = new File(path);
		InputStream ims = new FileInputStream(file);
		return ims;
		
	}
}
