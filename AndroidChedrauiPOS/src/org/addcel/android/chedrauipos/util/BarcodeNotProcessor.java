package org.addcel.android.chedrauipos.util;

import org.addcel.android.chedrauipos.to.Articulo;

import android.text.TextUtils;

public class BarcodeNotProcessor implements BarcodeProcessor {

	@Override
	public String getUpc(String barcode) {
		// TODO Auto-generated method stub
			
		return barcode;
		
	}

	@Override
	public Articulo processArticulo(String barcode, Articulo articulo) {
		// TODO Auto-generated method stub
		String precioStr = TextUtils.substring(barcode, 7, 12);
		double precio = Double.parseDouble(precioStr) / 100;
		if (barcode.length() == 13) {
			articulo = new Articulo();
			articulo.setDepartamento("00")
			.setDescripcion("Pan")
			.setDescuento(0)
			.setMontoTotal(precio * 1)
			.setPrecio(precio)
			.setUm("ST")
			.setUpc(barcode);
		} else {
			if (articulo != null) {
				articulo.setMontoTotal(articulo.getPrecio() * 1);
			}
		}
		
		return articulo;
	}

}
