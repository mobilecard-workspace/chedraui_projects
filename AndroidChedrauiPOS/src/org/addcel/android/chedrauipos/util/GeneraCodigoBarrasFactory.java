package org.addcel.android.chedrauipos.util;

public class GeneraCodigoBarrasFactory {

	private static GeneraCodigoBarras genBarcode;
	
	public static synchronized void set(GeneraCodigoBarras gb) {
		GeneraCodigoBarrasFactory.genBarcode = gb;
	}
	
	public static synchronized GeneraCodigoBarras get() {
		if (genBarcode == null) {
			genBarcode = new GeneraCodigoBarrasPuros();
		}
		
		return genBarcode;
	}
	
}
