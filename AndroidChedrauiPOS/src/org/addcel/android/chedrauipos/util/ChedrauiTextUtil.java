package org.addcel.android.chedrauipos.util;

public class ChedrauiTextUtil {
	
	public static String buildStockFileName(String tienda, String prefix) {
		int tiendaLen = tienda.length();
		switch (tiendaLen) {
		case 2:
			return prefix + "00" + tienda + ".txt";
	
		case 3:
			return prefix + "0" + tienda + ".txt";
			
		default:
			return prefix + tienda + ".txt";
		}
	}
}
