package org.addcel.android.chedrauipos.util;

import java.net.HttpURLConnection;
import java.net.URL;

public class HostValidator {
	
	private String url;
	private String port;

	public HostValidator() {
		this("", "80");
	}
	
	public HostValidator(String url, String port) {
		this.url = url;
		this.port = port;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getPort() {
		return port;
	}
	
	public void setPort(String port) {
		this.port = port;
	}
	
	public boolean isHostAvailable() {
		try { 
            URL url = new URL("http://" + this.url + ":" + port + "/");
            final HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
            urlc.setRequestProperty("User-Agent", "Android Application");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(10 * 1000);
            urlc.connect();
            if (urlc.getResponseCode() == 200) {
                return true; 
            } 
        } catch (Throwable e) {
            e.printStackTrace();
        } 
        return false; 
	}
	
	
	

}
