package org.addcel.android.chedrauipos.util;

import org.addcel.android.chedrauipos.to.Articulo;

public interface BarcodeProcessor {
	
	public String getUpc(String barcode);
	public Articulo processArticulo(String barcode, Articulo articulo);

}
