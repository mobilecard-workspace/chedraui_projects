/*
 * COPYRIGHT:
 * Licensed Materials - Property of Toshiba Global Commerce Solutions, Inc.
 * Author: Daniel Flores Cruz
 * 
 * Change Activity Log
 * Reason  Date     Who  Description
 * ------- -------- ---  -----------
 * 
 */
package org.addcel.android.chedrauipos.util;

import org.addcel.android.session.SessionManager;
import org.addcel.android.util.EAN13;
import org.addcel.android.util.LogUtil;

import android.content.Context;
import android.text.TextUtils;

public class GeneraCodigoBarrasPuros implements GeneraCodigoBarras{
	
	public static final int MAXIMO_CONSECUTIVO = 9999;
	public static final int INICIO_CONSECUTIVO = 9001;
	private static final String[] REPLACABLE = {".", ",",};
	private static final String[] REPLACES = {"", ""};
	
	private static final String TAG = GeneraCodigoBarrasPuros.class.getSimpleName();
	
	EAN13 ean13 = new EAN13();
	
	//el argumento monto es la cantidad de la transacción sin punto decimal
	public String generaCodigoBarrasTicket(String consecutivo, String monto){
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("28");
		
		sb.append(consecutivo);
		monto = TextUtils.replace(monto, REPLACABLE, REPLACES).toString();
//				monto.replace(".", "");
		LogUtil.text(TAG, "La longitud del monto: " + monto + " es: " + monto.length());
		//El monto esta formado por 6 caracteres, por lo que si tienes un precio de 55.52, tienes que agregar dos ceros al inicio
		if(monto.length() == 4){
			monto = "00" + monto;
		}
		//El monto esta formado por 6 caracteres, por lo que si tienes un precio de 555.52, tienes que agregar un cero al inicio
		else if(monto.length() == 5){
			monto = "0" + monto;
		}
		
		sb.append(monto);
		
		sb.append(ean13.generateVerDigit(sb.toString()));
		
		
		return sb.toString();
		
	}
	
	//M�todo para generar el n�mero de serie correspondiente al ticket en turno
	public String generaNSTicket(Context _con, int _consecutivo){
		
//		File consecutivoFile = new File(pathPuros + "App//consecutivo.txt");
		
//		try {
//			
//			FileReader fr = new FileReader(consecutivoFile);
//			BufferedReader br = new BufferedReader(fr);
//			
//			
//			String registro = br.readLine();
			
			
			Integer consecutivo = Integer.valueOf(_consecutivo);
			consecutivo++;
			
			LogUtil.text(TAG,"Número de consecutivo para el ticket actual: " + consecutivo);
			
			if(consecutivo > MAXIMO_CONSECUTIVO){
				consecutivo = INICIO_CONSECUTIVO;
				LogUtil.text(TAG,"Reiniciando consecutivo de tickets a: " + consecutivo);
			}
			
			/*
			 * AUMENTAMOS CONSECUTIVO EN PREFERENCIAS
			 */
			SessionManager session = new SessionManager(_con);
			session.setConsecutivo(consecutivo);
			
//			FileWriter fw = new FileWriter(consecutivoFile);
//			
//			fw.write(consecutivo.toString());
//			
//			fw.flush();
//			fw.close();
//			
//			fr.close();
//			br.close();
			
			return consecutivo.toString();
			
			
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			LogUtil.error(TAG, "IOException, ", e);
//		} catch(Exception e){
//			LogUtil.error(TAG, "Exception, ", e);
//		}
		
//		return null;
		
		
		
	}

}
