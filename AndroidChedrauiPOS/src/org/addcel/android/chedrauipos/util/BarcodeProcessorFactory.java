package org.addcel.android.chedrauipos.util;

public class BarcodeProcessorFactory {
	
	public enum ProcessorType {
		DEFAULT, PESABLE, NO_PROCESS;
	}

	private static BarcodeProcessor processor;
	
	public static synchronized void set(BarcodeProcessor p) {
		BarcodeProcessorFactory.processor = p;
	}
	
	public static synchronized BarcodeProcessor get(ProcessorType t) {
		switch (t) {
		case DEFAULT:
			processor = new DefaultBarcodeProcessor();
			break;

		case PESABLE:
			processor = new PesableBarcodeProcessor();
			break;
		case NO_PROCESS:
			processor = new BarcodeNotProcessor();
			break;
		default:
			break;
		}
		
		return processor;
	}
	

}
