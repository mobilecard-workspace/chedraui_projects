package org.addcel.android.chedrauipos.util;

import android.content.Context;

public interface GeneraCodigoBarras {
	public String generaCodigoBarrasTicket(String consecutivo, String monto);
	public String generaNSTicket(Context _con, int _consecutivo);
}
