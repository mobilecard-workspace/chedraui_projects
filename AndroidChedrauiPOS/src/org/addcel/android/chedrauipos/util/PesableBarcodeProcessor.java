package org.addcel.android.chedrauipos.util;

import org.addcel.android.chedrauipos.to.Articulo;

import android.text.TextUtils;

public class PesableBarcodeProcessor implements BarcodeProcessor {

	@Override
	public String getUpc(String barcode) {
		// TODO Auto-generated method stub
		
		String tipoProducto = TextUtils.substring(barcode, 0, 2);
		String codeProducto = TextUtils.substring(barcode, 3, 8);
		
		return tipoProducto + codeProducto;
	}

	@Override
	public Articulo processArticulo(String barcode, Articulo articulo) {
		// TODO Auto-generated method stub
	
		if (articulo != null) {
			String pesoStr = TextUtils.substring(barcode, 13, 18);
			double peso = Double.parseDouble(pesoStr) / 1000;
			articulo.setPeso(peso);
			double total = peso * articulo.getPrecio();
			double finalValue = Math.round(total * 100.0 ) / 100.0;
			articulo.setMontoTotal(finalValue);
		}
		/*
		 * 
			PARA PREFIJOS 23 y 25
			
			codigo de barras
			25 0 00500 05990 00360
			
			Posiciones            Valor                                Ejemplo
			1 al 2                     tipo producto                 25
			3                            relleno                             0
			4 al 8                     codigo producto            00500
			9  al 13                  precio por unidad          05990
			14  al 18               gramos                             00360
			
			Para el archivo
			
			codigo del producto: posiciones del 9 al 13 en formato pesos 59.90
			Tipo producto: 1 
			Precio unitario: posiciones del 9 al 13, en formato pesos 59.90
			Peso: posiciones del 14 al 18, .360
			Monto: Importe=pesoxprecio.
		 */
		
		
		return articulo;
	}

}
