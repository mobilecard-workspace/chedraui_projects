package org.addcel.android.chedrauipos.listener;

import java.util.List;

import org.addcel.android.chedrauipos.activity.MenuActivity;
import org.addcel.android.chedrauipos.dialog.Dialogs;
import org.addcel.android.chedrauipos.periodica.CargaPeriodicaAlarmReceiver;
import org.addcel.android.chedrauipos.service.SessionTimerBroadcastService;
import org.addcel.android.chedrauipos.session.UserSession;
import org.addcel.android.chedrauipos.to.Usuario;
import org.addcel.android.session.SessionManager;
import org.addcel.android.util.ErrorUtil;
import org.addcel.android.util.LogUtil;
import org.addcel.android.ws.listener.WSResponseListener;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.widget.Toast;

public class LoginListener implements WSResponseListener {

	private Context con;
	private String login, password;
	private SessionManager session;
	private Intent intent;

	public LoginListener(Context _con) {
		// TODO Auto-generated constructor stub
		con = _con;
		session = new SessionManager(_con);
	}
	
	public Context getCon() {
		return con;
	}

	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}

	public SessionManager getSession() {
		return session;
	}

	public Intent getIntent() {
		return intent;
	}
	
	public LoginListener setCon(Context con) {
		this.con = con;
		return this;
	}

	public LoginListener setLogin(String login) {
		this.login = login;
		return this;
	}

	public LoginListener setPassword(String password) {
		this.password = password;
		return this;
	}

	public LoginListener setSession(SessionManager session) {
		this.session = session;
		return this;
	}

	public LoginListener setIntent(Intent intent) {
		this.intent = intent;
		return this;
	}

	@Override
	public void onStringReceived(String response) {
		// TODO Auto-generated method stub
		Toast.makeText(con, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onJsonObjectReceived(JSONObject response) {
		// TODO Auto-generated method stub
		int idError = response.optInt("idError", -10000);
		String mensajeError = response.optString("mensajeError", ErrorUtil.connection());
		if (idError == 0) {
			int status = response.optInt("status", -10000);
			Usuario usuario = new Usuario(response);
			switch (status) {
			case 1:
				UserSession.getInstance().setUsuario(usuario);
				
				if (TextUtils.equals(login, "elmanss")) {
					UserSession.getInstance().getUsuario().getTienda().setPassFTP("8dntseBoBo");
					UserSession.getInstance().getUsuario().getTienda().setUrlFTP("50.57.192.214");
					UserSession.getInstance().getUsuario().getTienda().setUserFTP("chedraui");
				}
				
				UserSession.getInstance().setTienda(usuario.getTienda());
				((Activity) con).invalidateOptionsMenu();
				try {
					con.startService(new Intent(con, SessionTimerBroadcastService.class));
					SessionManager s = new SessionManager(con);
					if (s.getUpdated()) {
						CargaPeriodicaAlarmReceiver pAlarm = new CargaPeriodicaAlarmReceiver();
						pAlarm.setAlarm(con);
					}
				} catch (Exception e) {
					LogUtil.error("LoginListener", "onJsonObjectReceived", e);
				}
				Toast.makeText(con, mensajeError, Toast.LENGTH_SHORT).show();
				break;
			case 99:
				UserSession.getInstance().setUsuario(usuario);
				UserSession.getInstance().setTienda(usuario.getTienda());
				((Activity) con).invalidateOptionsMenu();
				con.startService(new Intent(con, SessionTimerBroadcastService.class));
				if (!((Activity) con).isFinishing()) {
					Dialogs.passwordDialog(con);
				} else {
					Toast.makeText(con, "Hay varios procesos ejecutándose. Intenta ingresar en un momento.", Toast.LENGTH_SHORT).show();
				}
				break;

			default:
				Toast.makeText(con, mensajeError, Toast.LENGTH_SHORT).show();
				break;
			}
		} else {
			if (UserSession.getInstance().isLoggedIn()) {
				UserSession.getInstance().logout();
			}
			
			Toast.makeText(con, mensajeError, Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onJsonArrayReceived(JSONArray response) {
		// TODO Auto-generated method stub
		Toast.makeText(con, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onCatalogReceived(List<BasicNameValuePair> catalog) {
		// TODO Auto-generated method stub
		Toast.makeText(con, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
	}

}
