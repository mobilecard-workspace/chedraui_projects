package org.addcel.android.chedrauipos.listener;

public interface ProductoListChangeListener {
	
	public void onTotalChange(double total);
	public void onDelete();

}
