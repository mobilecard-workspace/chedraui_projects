package org.addcel.android.chedrauipos.listener;

import java.util.List;

import org.addcel.android.chedrauipos.session.UserSession;
import org.addcel.android.util.ErrorUtil;
import org.addcel.android.ws.listener.WSResponseListener;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.widget.Toast;

public class PasswordListener implements WSResponseListener {
	
	private Context con;
	private String password;
	
	public PasswordListener(Context con, String password) {
		this.con = con;
		this.password = password;
	}
	
	@Override
	public void onStringReceived(String response) {
		// TODO Auto-generated method stub
		Toast.makeText(con, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();

	}

	@Override
	public void onJsonObjectReceived(JSONObject response) {
		// TODO Auto-generated method stub
		int idError = response.optInt("idError", -10000);
		String mensajeError = response.optString("mensajeError", ErrorUtil.connection());
		switch (idError) {
		case 0:
			UserSession.getInstance().getUsuario().setPassword(password);
			UserSession.getInstance().getUsuario().setStatus(1);
			Toast.makeText(con, mensajeError, Toast.LENGTH_SHORT).show();
			break;

		default:
			Toast.makeText(con, mensajeError, Toast.LENGTH_SHORT).show();
			break;
		}
	}

	@Override
	public void onJsonArrayReceived(JSONArray response) {
		// TODO Auto-generated method stub
		Toast.makeText(con, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();

	}

	@Override
	public void onCatalogReceived(List<BasicNameValuePair> catalog) {
		// TODO Auto-generated method stub
		Toast.makeText(con, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();

	}

}
