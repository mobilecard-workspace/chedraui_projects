package org.addcel.android.chedrauipos.listener;

import org.addcel.android.chedrauipos.to.Articulo;

public interface ArticuloQueryListener {

	public void onQueryFinished(Articulo articulo);
}
