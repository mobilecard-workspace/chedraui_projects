package org.addcel.android.chedrauipos.dummy;

import org.addcel.android.chedrauipos.to.Role;
import org.addcel.android.chedrauipos.to.Tienda;
import org.addcel.android.chedrauipos.to.Usuario;

public class DummyObjectFactory {

	
	public static Usuario buildDummyUsuario() {
		
		return new Usuario().setApellido("Apellido")
				.setIdSupervisor(10000)
				.setIdUsuario(1000)
				.setLogin("login")
				.setMaterno("Materno")
				.setNombre("Nombre")
				.setPassword("")
				.setRole(new Role(1000, "Vendedor"))
				.setStatus(1)
				.setTienda(buildDummyTienda());
		
	}
	
	public static Tienda buildDummyTienda() {
		return new Tienda().setCodigo("0098")
				.setDescripcion("Tienda 0098")
				.setIdTienda(10)
				.setPassFTP("12345678")
				.setStatus(1)
				.setUrlFTP("http://192.168.0.1")
				.setUserFTP("login");
	}
}
