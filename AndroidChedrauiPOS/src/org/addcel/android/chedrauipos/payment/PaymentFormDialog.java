package org.addcel.android.chedrauipos.payment;

import org.addcel.android.chedrauipos.R;

import android.app.Dialog;
import android.content.Context;
import android.widget.EditText;
import butterknife.InjectView;

public class PaymentFormDialog extends Dialog {

	@InjectView(R.id.text_nombre) EditText nombreText;
	@InjectView(R.id.text_mail) EditText mailText;
	@InjectView(R.id.text_pan) EditText panText;
	@InjectView(R.id.text_mes) EditText mesText;
	@InjectView(R.id.text_anio) EditText anioText;
	@InjectView(R.id.text_cvv) EditText cvvText;
	
	private Context context;

	public PaymentFormDialog(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
	}

}
