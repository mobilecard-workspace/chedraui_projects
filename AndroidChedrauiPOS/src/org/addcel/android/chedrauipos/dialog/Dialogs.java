package org.addcel.android.chedrauipos.dialog;

import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.constant.Url;
import org.addcel.android.chedrauipos.listener.LoginListener;
import org.addcel.android.chedrauipos.listener.PasswordListener;
import org.addcel.android.chedrauipos.session.UserSession;
import org.addcel.android.crypto.Cifrados;
import org.addcel.android.util.ErrorUtil;
import org.addcel.android.ws.client.WSClient;
import org.addcel.android.ws.listener.ListenerFactory;
import org.addcel.android.ws.listener.WSResponseListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Dialogs {
	/**
	 * 
	 * @param _con AppContext desde el cual se muestra el AlertDialog.
	 * @param _intent Intent con la Activity que se abrir� al loguearse exitosamente, si es null no se inicia nada.
	 * @param _closeCurrent Bandera que indica si la actividad actual debe cerrarse al loguearse.
	 */
	public static void loginDialog(final Context _con, final Intent _intent, final boolean _closeCurrent) {
		AlertDialog.Builder builder = new AlertDialog.Builder(_con)
		.setTitle(R.string.txt_login_title)
		.setMessage(R.string.txt_login_mensaje);
		
		LayoutInflater inflater = (LayoutInflater) _con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		final View loginView = inflater.inflate(R.layout.view_login, null);
		
		final EditText loginText = (EditText) loginView.findViewById(R.id.text_login);
		final EditText passwordText = (EditText) loginView.findViewById(R.id.text_password);
		
		builder.setView(loginView)
				.setCancelable(true)
				.setPositiveButton(R.string.txt_login_iniciar, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
						String login = loginText.getText().toString().trim();
						String password = passwordText.getText().toString().trim();
						
						if (TextUtils.isEmpty(login)) {
							Toast.makeText(_con, ErrorUtil.isEmpty("Nombre de usuario"), Toast.LENGTH_LONG).show();
						} else if (login.length() < 3) {
							Toast.makeText(_con, ErrorUtil.length("Nombre de usuario", 3, 16), Toast.LENGTH_LONG).show();
						} else if (TextUtils.isEmpty(password)) {
							Toast.makeText(_con, ErrorUtil.isEmpty("Contraseña"), Toast.LENGTH_LONG).show();
						} else if (password.length() < 8) {
							Toast.makeText(_con, ErrorUtil.length("Contraseña", 8, 12), Toast.LENGTH_LONG).show();
						} else {
							StringBuilder sb = new StringBuilder();
							sb.append("{")
								.append("\"login\":").append("\"").append(login).append("\"").append(",")
								.append("\"password\":").append("\"").append(password).append("\"")
							.append("}");
							
							dialog.dismiss();
							
							LoginListener listener = (LoginListener) ListenerFactory.get(_con);
							
							
							new WSClient(_con)
								.hasLoader(true)
								.setCifrado(Cifrados.SENSITIVE)
								.setKey(password)
								.setListener(listener.setLogin(login).setPassword(password).setIntent(_intent))
								.setUrl(Url.USER_GET)
								.execute(sb.toString());
						}
					}
				})
				.setNegativeButton(R.string.txt_login_cancelar, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.cancel();
						
						if (_closeCurrent)
							((Activity) _con).finish();
					}
				})
				.show();
	}
	
	public static void passwordDialog(final Context _con) {
		AlertDialog.Builder builder = new AlertDialog.Builder(_con)
		.setTitle(R.string.txt_pass_title)
		.setMessage(R.string.txt_pass_mensaje);
		
		LayoutInflater inflater = (LayoutInflater) _con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		final View loginView = inflater.inflate(R.layout.view_password, null);
		
		final EditText actualText = (EditText) loginView.findViewById(R.id.text_actual);
		final EditText nuevoText = (EditText) loginView.findViewById(R.id.text_nueva);
		final EditText confText = (EditText) loginView.findViewById(R.id.text_conf);
		
		builder.setView(loginView)
				.setCancelable(true)
				.setPositiveButton(R.string.txt_pass_actualizar, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
						String actual = actualText.getText().toString().trim();
						String nuevo = nuevoText.getText().toString().trim();
						String conf = confText.getText().toString().trim();
						
						if (TextUtils.isEmpty(actual)) {
							Toast.makeText(_con, ErrorUtil.isEmpty("Contraseña actual"), Toast.LENGTH_LONG).show();
						} else if (TextUtils.isEmpty(nuevo)) {
							Toast.makeText(_con, ErrorUtil.isEmpty("Contraseña nueva"), Toast.LENGTH_LONG).show();
						} else if (!TextUtils.equals(nuevo, conf)) {
							Toast.makeText(_con, ErrorUtil.notAMatch("Contraseña nueva", "Confirmación"), Toast.LENGTH_LONG).show();
						} else {
							
							StringBuilder sb = new StringBuilder();
							sb.append("{")
								.append("\"idUsuario\":").append(UserSession.getInstance().getUsuario().getIdUsuario()).append(",")
								.append("\"password\":").append("\"").append(actual).append("\"").append(",")
								.append("\"passwordNuevo\":").append("\"").append(nuevo).append("\"")
							.append("}");
							
//							dialog.dismiss();
							
							new WSClient(_con)
								.hasLoader(true)
								.setCifrado(Cifrados.SENSITIVE)
								.setListener(new PasswordListener(_con, nuevo))
								.setUrl(Url.PASSWORD_UPDATE)
								.execute(sb.toString());
						}
					}
				})
				.setNegativeButton(R.string.txt_login_cancelar, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				}).create().show();
	}
}
