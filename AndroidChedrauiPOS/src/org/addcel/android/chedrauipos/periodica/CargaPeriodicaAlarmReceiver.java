package org.addcel.android.chedrauipos.periodica;

import org.addcel.android.util.LogUtil;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

public class CargaPeriodicaAlarmReceiver extends WakefulBroadcastReceiver {
	
	private AlarmManager aManager;
	private PendingIntent aIntent;
	
	private static final String TAG = CargaPeriodicaAlarmReceiver.class.getSimpleName();

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		LogUtil.text(TAG, "Recibe señal para lanzar servicio.");
		Intent service = new Intent(context, CargaPeriodicaSchedulingService.class);
		startWakefulService(context, service);
	}
	
	public void setAlarm(Context context) {
		
		aManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(context, CargaPeriodicaAlarmReceiver.class);
		aIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

		/*         
		 *  // Wake up the device to fire the alarm in 30 minutes, and every 30 minutes
         * // after that.
         */
        aManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 
                 AlarmManager.INTERVAL_HALF_HOUR, //REGRESAR A MEDIA HR
                 AlarmManager.INTERVAL_HALF_HOUR, aIntent);
    }

	 public void cancelAlarm(Context context) {
	        // If the alarm has been set, cancel it.
	        if (aManager!= null) {
	            aManager.cancel(aIntent);
	        }
	    }
	
}
