package org.addcel.android.chedrauipos.periodica;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;

import org.addcel.android.chedrauipos.R;
import org.addcel.android.chedrauipos.activity.MenuActivity;
import org.addcel.android.chedrauipos.constant.Rutas;
import org.addcel.android.chedrauipos.session.UserSession;
import org.addcel.android.chedrauipos.sql.ArticuloDataSource;
import org.addcel.android.chedrauipos.util.ChedrauiTextUtil;
import org.addcel.android.ftp.FileTransferClient;
import org.addcel.android.ftp.SFTPTransferClient;
import org.addcel.android.session.SessionManager;
import org.addcel.android.util.LogUtil;
import org.addcel.android.util.NetworkUtil;
import org.addcel.android.util.Text;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

public class CargaPeriodicaSchedulingService extends IntentService {
	
	private FileTransferClient client;
	private ArticuloDataSource source;
	private String myPath;
	private String serverPath;
	private NotificationManager mNotificationManager;
	private SessionManager preferences;
	NotificationCompat.Builder builder;
	
	public static final int NOTIFICATION_ID = 1;
	public static boolean isRunning = false;
    private static final String TAG = CargaPeriodicaSchedulingService.class.getSimpleName();
	
	
    public CargaPeriodicaSchedulingService() {
    	super("CargaPeriodicaSchedulingService");
    	LogUtil.text(TAG, "Inicializando service");
    }
    

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		LogUtil.text(TAG, "Entrando a onHandleIntent");
		preferences = new SessionManager(this);
		isRunning = true;
		if (NetworkUtil.isOnline(this)) {
			if (UserSession.getInstance().getTienda() != null) {
				this.client = new SFTPTransferClient("50.57.192.214", "rmuniz", "xZZWj4GFzTZa");
				this.source = new ArticuloDataSource(this);
				this.myPath = getFilesDir() + "/" + ChedrauiTextUtil.buildStockFileName(
						UserSession.getInstance().getTienda().getCodigo(), 
						"UPD");
				this.serverPath = Rutas.DESCARGA;
				int actualizado = get();
				LogUtil.text(TAG, "Termino proceso de descarga de archivo");
				if (actualizado == 0) {
					int update = update(myPath);
					switch (update) {
					case 0:
						Date actualizacion = Calendar.getInstance().getTime();
						preferences.setUpdated(true);
				        preferences.setFechaActualizacion(Text.formatDateChedraui(actualizacion));
						sendNotification("Base de datos actualizada con éxito.");
						break;
		
					default:
						sendNotification("Error en actualización base de datos.");
						break;
					}
				} else {
					sendNotification("Error en descarga de archivo.");
				}	
			} else {
				sendNotification("Inicie sesión para correr el ciclo de actualización de artículos.");
			}
		} else {
			sendNotification("Sin conexión a internet, no se pueden actualizar artículos.");
		}
	}
	
	private int get() {
		if (((SFTPTransferClient) client).connect()) {
			return client.getFile(myPath, serverPath);
		} else {
			return -10000;
		}
	}
	
	private int update(String myPath) {
		
		 FileInputStream _ims = null;
		 BufferedReader reader = null;
		    try { 
		    	_ims = new FileInputStream(myPath);
		    	source.open();
		    	
		    	reader = new BufferedReader(new InputStreamReader(_ims, "UTF8"));
		    	String line;
		        while ((line = reader.readLine()) != null) {
		           source.updateArticulo(line);
		        } 
		        return 0;
		        
		    } catch (UnsupportedEncodingException e) {
		    	LogUtil.error(TAG, "update", e);
		    	source.close();
		    	return -10000;
		    } catch (IOException ex) {
		        // handle exception 
	            LogUtil.error(TAG, "update", ex);
		    	source.close();
		    	return -10000;
		    } 
		    
		    finally { 
		    	
		        try { 
		            _ims.close(); 
		            source.close();
		        } 
		        catch (IOException e) {
		            LogUtil.error(TAG, "update", e);
		        } 
		    } 
	}
	
	private void sendNotification(String msg) {
        mNotificationManager = (NotificationManager)
               this.getSystemService(Context.NOTIFICATION_SERVICE);
    
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
            new Intent(this, MenuActivity.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
        .setSmallIcon(R.drawable.ic_launcher)
        .setContentTitle("Refrescado de artículos")
        .setStyle(new NotificationCompat.BigTextStyle()
        .bigText(msg))
        .setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

}
