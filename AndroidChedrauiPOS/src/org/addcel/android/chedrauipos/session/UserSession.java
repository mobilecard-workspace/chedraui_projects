package org.addcel.android.chedrauipos.session;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.addcel.android.chedrauipos.to.Tienda;
import org.addcel.android.chedrauipos.to.Usuario;

import android.text.TextUtils;

public class UserSession {

	private static UserSession instance = null;
	private Usuario usuario;
	private Tienda tienda;

	private UserSession() {

	}

	public static UserSession getInstance() {
		if (null == instance) {
			instance = new UserSession();
		}

		return instance;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Tienda getTienda() {
		return tienda;
	}
	
	public void setTienda(Tienda tienda) {
		this.tienda = tienda;
	}
	public void logout() {
		usuario = null;
	}

	public boolean isLoggedIn() {
		if (usuario != null) {
			return true;
		} else {
			return false;
		}
	}
	

}
