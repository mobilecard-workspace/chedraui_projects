package org.addcel.android.chedrauipos.constant;

import org.addcel.android.enums.AppContext;

public class Url {
	
	private static final String BASE = AppContext.getCurrent().getUrl() + "ChedrauiServicios/";
	
	public static final String TOKEN_GET = BASE + "getToken";
	public static final String PAGO_POST = BASE + "pagoProductos";
	public static final String PAGOS_GET = BASE + "busquedaPagos";
	public static final String MAIL_POST = BASE + "reenvioRecibo";
	
	public static final String USER_INSERT = BASE + "altaUsuario";
	public static final String USER_UPDATE = BASE + "cambioUsuario";
	public static final String USER_GET = BASE + "login";
	public static final String ROLE_GET = BASE + "getRoles";
	public static final String TIENDA_GET = BASE + "getTiendas";
	public static final String PASSWORD_UPDATE = BASE + "cambioPassword";

}
