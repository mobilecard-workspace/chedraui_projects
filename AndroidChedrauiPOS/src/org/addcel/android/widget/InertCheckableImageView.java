package org.addcel.android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;

public class InertCheckableImageView extends CheckableImageView {

	public InertCheckableImageView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public InertCheckableImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
	
	@Override
    public boolean onTouchEvent(MotionEvent event) {
            // Make the checkbox not respond to any user event
            return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
            // Make the checkbox not respond to any user event
            return false;
    }

    @Override
    public boolean onKeyMultiple(int keyCode, int repeatCount, KeyEvent event) {
            // Make the checkbox not respond to any user event
            return false;
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
            // Make the checkbox not respond to any user event
            return false;
    }

    @Override
    public boolean onKeyShortcut(int keyCode, KeyEvent event) {
            // Make the checkbox not respond to any user event
            return false;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
            // Make the checkbox not respond to any user event
            return false;
    }

    @Override
    public boolean onTrackballEvent(MotionEvent event) {
            // Make the checkbox not respond to any user event
            return false;
    }

}
