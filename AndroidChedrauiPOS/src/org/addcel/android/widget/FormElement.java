package org.addcel.android.widget;

import org.addcel.android.font.Roboto;
import org.addcel.android.widget.enums.ElementType;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.widget.LinearLayout;

public abstract class FormElement extends LinearLayout {
	
	private ElementType type;
	private LayoutParams params;
	private static Typeface t;
	
	
	public FormElement(Context context, ElementType type) {
		// TODO Auto-generated constructor stub
		super(context);
		this.type = type;
		create();
	}
	
	public ElementType getType() {
		return type;
	}
	
	public void setType(ElementType type) {
		this.type = type;
	}
	
	private void create() {
		this.setLayoutParams(getParams(true));
		this.setOrientation(VERTICAL);
		this.setGravity(Gravity.CENTER_HORIZONTAL);
		this.setBackgroundColor(Color.TRANSPARENT);
	}
	
	public LayoutParams getParams(boolean ultimaView) {
		if (null == params) {
			params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		}
		
		if (ultimaView){
			params.setMargins(0, 0, 0, 8);
		} else {
			params.setMargins(0, 0, 0, 0);
		}
		
		return params;
	}
	
	protected abstract void format(Context c);
	
	protected static Typeface getTypeface(Context c) {
		
		if (null == t) {
			t = Typeface.createFromAsset(c.getAssets(), Roboto.ROBOTO_LIGHT.getPath());
		}
		
		return t;
		
	}
}
