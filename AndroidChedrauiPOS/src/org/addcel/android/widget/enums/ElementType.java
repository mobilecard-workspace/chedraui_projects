package org.addcel.android.widget.enums;

public enum ElementType {
	VISIBLE, EDITABLE, PICKABLE, CHECKABLE;
}
