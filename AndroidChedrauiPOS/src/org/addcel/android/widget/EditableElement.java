package org.addcel.android.widget;

import org.addcel.android.chedrauipos.R;
import org.addcel.android.widget.enums.ElementType;

import android.content.Context;
import android.text.InputFilter;
import android.text.TextUtils.TruncateAt;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.TextView;

public class EditableElement extends FormElement {
	
	private TextView label;
	private String labelString;
	private EditText value;
	private String valueString;
	private static int MAX = 200;
	
	public EditableElement(Context context) {
		// TODO Auto-generated constructor stub
		super(context, ElementType.EDITABLE);
	}
	
	public EditableElement(Context context, String _labelStr, String _valueStr) {
		this(context);
		labelString = _labelStr;
		valueString = _valueStr;
		format(context);
	}

	@Override
	protected void format(Context c) {
		// TODO Auto-generated method stub
		
		label = new TextView(c);
		label.setLayoutParams(super.getParams(false));
		label.setGravity(Gravity.CENTER_HORIZONTAL);
//		label.setTextSize(14, TypedValue.COMPLEX_UNIT_SP);
//		label.setTextColor(c.getResources().getColor(R.color.abs__bright_foreground_holo_dark));
		label.setText(labelString);
		label.setTypeface(super.getTypeface(c));
		addView(label);
		
		value = new EditText(c);
		
		InputFilter[] fArray = new InputFilter[1];
		fArray[0] = new InputFilter.LengthFilter(MAX);
		value.setFilters(fArray);
//		value.setTextSize(12, TypedValue.COMPLEX_UNIT_SP);
		value.setLayoutParams(super.getParams(true));
		value.setGravity(Gravity.CENTER_HORIZONTAL);
		value.setEllipsize(TruncateAt.START);
		value.setText(valueString);
		value.setTypeface(super.getTypeface(c));
		addView(value);
	}
	
	public TextView getLabel() {
		return label;
	}
	
	public EditText getValue() {
		return value;
	}

}
