package org.addcel.android.widget;

import org.addcel.android.widget.enums.ElementType;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.TextView;

public class VisibleElement extends FormElement {
	
	private TextView label;
	private String text;
	

	public VisibleElement(Context context) {
		super(context, ElementType.VISIBLE);
	}
	
	public VisibleElement(Context context, String _text) {
		this(context);
		text = _text;
		format(context);
	}

	@Override
	protected void format(Context c) {
		// TODO Auto-generated method stub
		label = new TextView(c);
		label.setLayoutParams(super.getParams(false));
		label.setGravity(Gravity.CENTER_HORIZONTAL);
		label.setTextSize(14);
		label.setTextColor(Color.WHITE);
		label.setText(text);
		label.setTypeface(super.getTypeface(c));
		addView(label);
		
		refreshDrawableState();
	}
	
	public TextView getLabel() {
		return label;
	}
	
	public void setLabel(TextView label) {
		this.label = label;
	}

}
